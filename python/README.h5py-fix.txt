incorrect semantics when slicing data set with len=1ndarray of indices :
shape is not preserved (dimension with len=1 is removed)

* disagrees from behavior with >1 element in the index list
* disagrees from numpy behavior
* disagrees from slicing with a list

example : slicing a data set with lists and ndarrays len=4,1

import h5py, numpy as np
h5f=h5py.File('tmp.h5', 'w')
h5f['x'] = np.random.rand(10,2,6,16)
h5f.flush()
h5d=h5f['x']

print h5d.shape
#> (10, 2, 6, 16)
print h5d[:, :, [0,1,2,3] ].shape	    # slice with list, len=4 OK
#> (10, 2, 4, 16)
print h5d[:, :, [0]].shape		    # slice with list, len=1 OK
#> (10, 2, 1, 16)
print h5d[:, :, np.array([0,1,2,3])].shape  # slice with ndarray, len=4 OK
#> (10, 2, 4, 16)
print h5d[:, :, np.array([0])].shape	    # slice with ndarray, len=1 FAIL
#> (10, 2, 16)

numpy preserves sliced shape:
example : slicing a data set with lists and ndarrays len=4,1
print h5d.value.shape
#> (10, 2, 6, 16)
print h5d.value[:, :, [0,1,2,3] ].shape		  # slice with list, len=4 OK
#> (10, 2, 4, 16)
print h5d.value[:, :, [0]].shape		  # slice with list, len=1 OK
#> (10, 2, 1, 16)
print h5d.value[:, :, np.array([0,1,2,3])].shape  # slice with ndarray, len=4 OK
#> (10, 2, 4, 16)
print h5d.value[:, :, np.array([0])].shape	  # slice with ndarray,len=1 OK
#> (10, 2, 1, 16)


apparently fixed on 2016/06/18 (#722), closing issue #425 submitted on
  2014/03/28, should appear in >h5py-2.6.0
