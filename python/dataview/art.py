import lhpd
import itertools as it
from lhpd.fitter import f_wrap, fr_wrap
from .config_mpl import style_group, make_std_axes
import numpy as np

stg  = [
    style_group((0,0,1),   '-', 'o', alpha=.7, alpha_shade=.3),
    style_group((.7,0,0),  '-', 's', alpha=.7, alpha_shade=.3),
    style_group((0,.4,0),  '-', 'd', alpha=.7, alpha_shade=.3),
    style_group((0,.7,.7), '-', 'x', alpha=.7, alpha_shade=.3),
    style_group('#808000', '-', '+', alpha=.7, alpha_shade=.3),
    style_group('#e000d0', '-', 'h', alpha=.7, alpha_shade=.3),

    style_group('#ff6600', '-', 'v', alpha=.7, alpha_shade=.3),
    style_group('#7d26cd', '-', '^', alpha=.7, alpha_shade=.3),
    style_group('#b7410e', '-', '>', alpha=.7, alpha_shade=.3),
    style_group('#990066', '-', '<', alpha=.7, alpha_shade=.3),

    style_group('#000000', '-', '*', alpha_shade=.2)    # default black star
    ]


def make_range(xr, n, margin=0.10,
               min_or=None, max_or=None,
               min_and=None, max_and=None):
    """ compute range: n points in [min(xr) ... max(xr)], 
        extended to range_or, shrunk to range_and(takes precedence)
    """
    x0, x1  = xr.min(), xr.max()
    dx      = abs(x1 - x0)
    x0, x1  = x0 - dx*margin, x1 + dx*margin
    if not None is  min_or and min_or < x0: x0 = min_or
    if not None is  max_or and x1 < max_or: x1 = max_or
    if not None is  min_and and x0 < min_and: x0 = min_and
    if not None is  max_and and max_and < x1: x1 = max_and

    if not None is  max and max < x1: x1 = max
    return np.r_[x0 : x1 : 1j*n ]

def do_fit_rs(model_func, xr, y_rs, rsplan, reparam=None, p0=None):
    """
        RETURN  (p_rs, chi2_rs)
    """
    yavg, yerr, yncov = lhpd.calc_avg_err_ncov(y_rs, rsplan)
    fitter  = lhpd.fitter.Fitter(model_func, xr, yavg, yerr, yncov)
    pavg, chi2  = fitter.fit(p0, method='lma_nodiff')
    n_rs    = len(y_rs)
    n_p     = len(pavg)
    p_rs    = np.empty((n_rs, n_p), pavg.dtype)
    chi2_rs = np.empty((n_rs,), chi2.dtype)
    for i, y in enumerate(y_rs):
        p_rs[i], chi2_rs[i] = fitter.fit_savestate(y, method='lma_nodiff')
    return p_rs, chi2_rs

def plotval_broadcast_(yr, xr) :
    """ broadcast non-array or len=1 array to 1-d array ~ xr 
        (aid in plotting various bands etc) 
        affects only 1st dimension """
    xr  = np.atleast_1d(xr)
    yr  = np.atleast_1d(yr)
    return yr * np.ones_like(xr) # will catch any broadcast issues

def plot_data_rs(xr, y_rs, rsplan, ax, stg, label=None,
                 xtransform=lambda x:x):
    y_avg, y_err    = lhpd.calc_avg_err(y_rs, rsplan)
    ax.errorbar(xtransform(xr), y_avg, yerr=y_err, label=label, **stg.edot())

def plot_vminmax_band(xr, yavg, ymin, ymax, ax, stg, label=None) :
    """ yerr = errorbars
        if yminmax is not None, use yminmax as upper/lower """
    xr      = np.atleast_1d(xr)
    yavg    = plotval_broadcast_(yavg, xr)
    ymin    = plotval_broadcast_(ymin, xr)
    ymax    = plotval_broadcast_(ymax, xr)
    ax.fill_between(xr, ymin, yavg, label=label, **stg.band())
    ax.fill_between(xr, yavg, ymax, **stg.band())
    ax.plot(xr, yavg, **stg.line())

def plot_ve_band(xr, yavg, yerr, ax, stg, label=None) :
    """ yerr = errorbars
        if yminmax is not None, use yminmax as upper/lower """
    yavg = np.asarray(yavg)
    yerr = np.asarray(yerr)
    ymin = yavg - yerr
    ymax = yavg + yerr
    plot_vminmax_band(xr, yavg, ymin, ymax, ax, stg, label=label)

def plot_func_band_rs(func, xr, p_rs, rsplan, ax, stg, 
                quant=None, label=None, xsh=0,
                xtransform=lambda x:x):
    """ plot bands: y, y\pm Dy 
        func    function(xr, p
        xr      x range
        p_rs    parameter (resampled) ensemble
        rsplan  rsplan
        ax      axis
        stg     style group
        quant   quantile; plot bands for (q, 0.5, 1-q) regions
                if None, use mean, mean\pm1\sigma
    """
    xr  = np.sort(xr)
    yr_rs = [ np.array([ func(x, p) for x in xr ]) for p in p_rs ]
    if None is quant:
        yr_avg, yr_err = lhpd.calc_avg_err(yr_rs, rsplan)
        yr0  = yr_avg - yr_err
        yr1  = yr_avg
        yr2  = yr_avg + yr_err
    else:
        if None is rsplan or 'av' == rsplan[0]:
            if q < 0 or .5 < q: 
                raise ValueError("quantile=%f out of range [0, 0.5]" % q)
            yr0, yr1, yr2 = lhpd.weighted_quantile(yr_rs, 
                                    (quant, .5, 1.-quant), axis=0)
        else:
            raise ValueError("cannot use quantile with rsplan=%s" % str(rsplan))
    xr_t    = xtransform(xr + xsh)
    ax.fill_between(xr_t, yr0, yr1, **stg.band())
    ax.fill_between(xr_t, yr1, yr2, **stg.band())
    ax.plot(xr_t, yr1, label=label, **stg.line())
#ax=plt.figure().add_axes([.1,.1,.8,.8]); plot_func_band_rs(lambda x,p: p[0] + p[1]*x, np.r_[0:1:101j], np.random.normal(size=(100,2)), ('av',), ax, stg[0])

def plot_func_qbands_rs(func, xr, p_rs, rsplan, ax, xtransform=lambda x:x, *qq_s):
    """ plot bands: y, y\pm Dy 
        TODO test
        func    function(xr, p
        xr      x range
        p_rs    parameter (resampled) ensemble
        rsplan  rsplan
        ax      axis
        qq_s    [(q1, q2, stg), ... ] plot quantile bands with stg
    """
    xr  = np.sort(xr)
    yr_rs = [ np.array([ func(x, p) for x in xr ]) for p in p_rs ]

    xr_t    = xtransform(xr)
    for q1, q2, stg in qq_s:
        # FIXME optimize : call weighted_quantile (sort inside) only once
        yr1, yr2    = lhpd.weighted_quantile(yr_rs, (q1, q2), axis=0)
        ax.fill_between(xr_t, yr1, yr2, **stg.band())

def cdf_xrange(v) :
    """ make data range for CDF plotting: each data point is doubled """
    v   = np.array(v)
    v.sort(0)
    return np.r_[ [v, v] ].T.flatten()
    
def cdf_yrange(n, normed=True) : 
    """ make count range for CDF plotting """

    x   = np.r_[ 0 : 1. - 1./n : n * 1j]
    x1  = x + 1./n
    if not normed : 
        x   *= n
        x1  *= n
    return np.r_[ [x, x1] ].T.flatten()

def plot_dist(v, ax, stg, **kw) :
    label   = kw.get('label')
    quant   = kw.get('quant')#, [.25, .50, .75])
    stat    = kw.get('stat', [])
    v       = np.array(v)    
    n       = len(v)
    v.sort()
    ax.plot(cdf_xrange(v), cdf_yrange(n), label=label, **stg.line())

    if not None is quant :
        vq      = lhpd.weighted_quantile(v, quant)
        for q, vq in zip(quant, vq) :
            lw = 1 - 2*abs(.5 - q)
            ax.axvline(x = vq, **stg.cp(ls='-', lw=2*lw).line())

    if 'avg' in stat :
        vae      = v.mean(), v.std()
        vlo, vhi = lhpd.ae2lohi(vae)
        ax.axvspan(vlo, vhi, **stg.band())

def demo_plot_linear_fit(ax=None, stg=stg[0],
                 xtransform=lambda x:x):
    # gen data 
    xr  = np.r_[0.05:1:.1] 
    a,b = .3, .5 
    dy  = 0.2 
    n_data  = 100 
    rsplan  = ('jk', 1) 
    y   = a + b * xr + np.random.normal(scale=dy, size=(100, len(xr))) 
    y_rs= lhpd.resample(y, rsplan) 
    if None is ax: ax = make_std_axes() 
    f_lin   = lambda x,p:(p[0]+p[1]*x) 
    p_rs, chi2_rs   = do_fit_rs(f_wrap(f_lin),  
                                xr, y_rs, rsplan, p0=[0,0]) 
 
    plot_func_band_rs(f_lin, xr, p_rs, rsplan, ax, stg, xtransform=xtransform)
    plot_data_rs(xr, y_rs, rsplan, ax, stg, xtransform=xtransform) 
    return ax 


# from latcorr/runme/ff/nde/analysis_ff.py
def latexize(s):
    s   = s.replace('_', r'\_')
    s   = s.replace(r'<', r'$\langle$')
    s   = s.replace(r'>', r'$\rangle$')
    s   = s.replace(r'|', r'$|$')
    return s
def latexize_math(s):
    s   = s.replace(r'<', r'\langle')
    s   = s.replace(r'>', r'\rangle')
    return s
