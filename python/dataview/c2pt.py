from __future__ import print_function
import numpy
import matplotlib as mpl
import matplotlib as plt

from .config_mpl import *
from .aux_math import *


def calc_opt_axis_symlog(a_min, a_max, a_lth=None, linear=0.1, base=10, max_margin=0.1):
    """ return optimal (y_lo, y_hi, y_linthresh) 
            a_min, a_max
            a_lth       linear threshold, supersedes "linear"
            linear      portion of graph to be linear
            max_margin  maximal margin around points
    """
    if a_max < a_min:
        a_min, a_max = a_max, a_min
    if (base <= 0) or (1 == base) : 
        raise ValueError("invalid base=%e" % base)
    elif base < 1. :
        base = 1. / base
    if (max_margin <= 0.) or (1. <= max_margin) :
        raise ValueError("invalid max_margin=%f" % max_margin)

    # adjust a_min, a_max if any of them is zero
    if (0. == a_min) and (0. == a_max) :
        raise ValueError("empty range [%f : %f]" % (a_min, a_max))
    elif (0. == a_min) :  
        assert 0. < a_max
        a_min = -math.exp(1 - 1. / linear + math.log(a_max))
        # FIXME define a_lo, a_hi instead?
    elif a_max == 0 :  
        assert a_min < 0.
        a_max = math.exp(1 - 1. / linear + math.log(-a_min))
        # FIXME define a_lo, a_hi instead?
    elif 0. < a_min * a_max :     # same sign
        l_width = math.fabs(math.log(a_max / a_min))  # 0 < l_width
        if l_width < (1. / max_margin - 1.) * math.log(base):
            # narrow log range compared to base: extend by margin factor
            fact = math.exp(l_width * max_margin * .5)
            if 0 < a_min :   # positive
                if None is a_lth: a_lth = a_min / fact / 10.
                return a_min / fact, a_max * fact, a_lth
            else :          # negative
                if None is a_lth: a_lth = a_max / fact / 10.
                return a_min * fact, a_max / fact, a_lth
        else:
            # wide log range comp. to base: extend limits to base^n
            a_lo, a_hi = log_floor(a_min, base), log_ceil(a_max, base)
            if None is a_lth:
                a_lth = min(math.fabs(a_lo), math.fabs(a_hi)) / 10.
            return a_lo, a_hi, a_lth

    assert a_min < 0. and 0. < a_max      # other cases have been done

    # FIXME move to case selector above?
    a_lo, a_hi = log_floor(a_min, base), log_ceil(a_max, base)
    if None is a_lth:
        a_lth = math.exp(1 - 1./linear + 0.5 * math.log(math.fabs(a_lo * a_hi)))
        if math.fabs(a_hi) < a_lth: 
            a_lth = math.exp(2 - 2. / linear + math.fabs(a_lo))
        elif math.fabs(a_lo) < a_lth:
            a_lth = math.exp(2 - 2. / linear + math.fabs(a_hi))
    return a_lo, a_hi, a_lth

def make_ticks_symlog(a_lo, a_hi, a_lth, base=10, tc_density=10):
    """ make a list of tick values 
            a_lo, a_hi  graph limits
            a_lth       linear threshold
            base        log base
            tc_density  approximate number of ticks
    """
    def log_range(a0, a_end, base):
        # make sure the seq stops
        if (base < 0) or (1 == base):
            raise ValueError("invalid base = %e" % base)
        if (a0 * a_end < 0) :
            raise ValueError("invalid limits (%e, %e)" % (a0, a_end))
        res = []
        while ((a0 < a_end) ^ (base < 1) ^ (a0 < 0)) :
            res.append(a0)
            a0 *= base
        return res

    def uniq(t):
        t.sort()
        n = len(t)
        assert 0 < n
        last = t[0]
        lasti = i = 1
        while i < n:
            if t[i] != last:
                t[lasti] = last = t[i]
                lasti += 1
            i += 1
        return t[:lasti]

    if a_hi < a_lo:
        a_hi, a_lo = a_lo, a_hi
    if a_lth < 0:
        raise ValueError("invalid linthresh = %e" % a_lth)

    a_lo, a_hi = log_floor(a_lo, base), log_ceil(a_hi, base)

    tc_hi = []
    tc_lo = []
    tc_lin= []
    if (a_lth < a_hi) :
        if (a_lo < a_lth): 
            tc_hi = log_range(a_hi, a_lth, 1./base)
        else: 
            tc_hi = log_range(a_lo, a_hi, base)
    if (a_lo < -a_lth):
        if (-a_lth < a_hi):
            tc_lo = log_range(a_lo, -a_lth, 1./base)
        else:
            tc_lo = log_range(a_lo, a_hi, 1./base)

    a_l_lo  = min(a_lo, -a_lth)
    a_l_hi  = min(a_lth, a_hi)
    a_l_step= log_floor(max(math.fabs(a_l_lo), math.fabs(a_l_hi)), base)
    print(a_l_lo, a_l_hi, a_l_step)
    print(base_ceil(a_l_lo, a_l_step), base_ceil(a_l_hi, a_l_step), a_l_step)
    tc_l    = list(numpy.arange(base_ceil(a_l_lo, a_l_step), base_ceil(a_l_hi, a_l_step), 
                                a_l_step))
    print(tc_l)

    return uniq(tc_lo + tc_l + tc_hi)
        

def plot_stat_2d(a, tr=None, quant=(.0015, .0230, .1587), ranks=3, is_sorted=False,
                 ax=None, fmt=fmt_line_robust, fmt_shade=fmt_shade_robust):
    """ Show median and quantiles around median
        a           2D array [i_sample, i_obsv]
        tr          parameter t_obsv
        quant       thresholds to show; must be <=0.5, made symmetric
        ranks       min and max ## to show as plots
            None        do not show min/max
            'rest'      plot values outside the smallest quantile
            (int)       number of values
            (list)      list of ranks to show
        sorted      whether a is already sorted along 0 axis
        ax          axes to draw in
        fmt_shade   format for filled curves; made symmetric
    """
    from lhpd import orderstats, weighted_quantile
    
    
    if None is tr: tr = numpy.arange(0, a.shape[-1])
    else: tr = numpy.asarray(tr)

    if is_sorted:   a_s = numpy.asarray(a)
    else:           a_s = numpy.sort(a, axis=0)

    if len(quant) < 1:
        raise ValueError("bad quantiles (%s)" % str(quant))
    quant   = sorted(quant)
    if quant[-1] < 0.4999:
        quantX  = quant + [ 0.5 ] + [ (1. - q) for q in reversed(quant) ]
        n_q     = len(quant)
    elif 0.5001 < quant[-1]:
        raise ValueError("bad quantile %e > 0.5" % quant[-1])
    else:
        quantX  = quant + [ (1. - q) for q in reversed(quant[:-1]) ]
        n_q     = len(quant) - 1

    a_all= weighted_quantile(
                    a_s, 
                    quant=quantX + [ 0.5000, .1587, .8413 ],
                    is_sorted=True,
                    axis=0)
    a_q, a_med, a_lo_1s, a_hi_1s = \
                a_all[0 : 2*n_q+1], \
                a_all[2*n_q+1], a_all[2*n_q+2], a_all[2*n_q+3]

    if None is ax:
        ax = plt.figure().add_subplot(1,1,1)

    # plot quantiles
    for i_q in range(n_q):
        fmt_i = ls_rotate(fmt_shade, i_q)
        ax.fill_between(tr, a_q[i_q], a_q[i_q+1], **fmt_i)
        ax.fill_between(tr, a_q[2*n_q - i_q], a_q[2*n_q - i_q - 1], **fmt_i)

    # plot min/max
    if None is ranks: 
        ranks = []
    elif list == type(ranks) or tuple == type(ranks): pass
    elif float == type(ranks):
        ranks = range(int(ranks * a_s.shape[0]))
    elif 'rest' == ranks:
        ranks = range(int(quant[0] * a_s.shape[0]))
    elif int == type(ranks):
        assert 0 <= ranks
        ranks = range(ranks)
    else:
        raise ValueError("bad ranks='%s'" % str(ranks))

    for i_r, r in enumerate(ranks):
        fmt_i = ls_rotate(fmt, i_r)
        ax.plot(tr, a_s[r], **fmt_i)
        ax.plot(tr, a_s[-r-1], **fmt_i)

    return ax


def show_2pt(x):
    fig = plt.figure()
    x_opt = calc_opt_axis_symlog(x.min(), x.max())

    ax_all  = fig.add_subplot(2,1,1)
    ax_all.plot(x.T)
    ax_all.set_yscale('symlog', linthreshy=x_opt[2])
    ax_all.set_ylim(x_opt[0], x_opt[1])
    #ax_all.set_yticks(make_ticks_symlog(x_opt[0], x_opt[1], x_opt[2]))

    ax_rng  = fig.add_subplot(2,1,2)
    plot_stat_2d(x, ax=ax_rng)
    ax_rng.set_yscale('symlog', linthreshy=x_opt[2])
    ax_rng.set_ylim(x_opt[0], x_opt[1])
    #ax_rng.set_yticks(make_ticks_symlog(x_opt[0], x_opt[1], x_opt[2]))
    
    return fig
