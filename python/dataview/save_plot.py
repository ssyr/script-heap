from __future__ import print_function
from future.utils import iteritems
from past.builtins import basestring
import types
import sys
import numpy as np
import pprint
import matplotlib as mpl
from .config_mpl import *

class class_logger:
    """ log all calls to `obj' methods as python code 
        
        * pass all method calls to obj methods
        * if no exception is raised, record calls: method name, parameters
        
        FIXME remove meaningless calls from iPython: 
            e.g. trait_names, _getAttributeNames called when Tab-expanding
    """
    def __init__(self, obj):
        self.class_logger_obj       = obj
        self.class_logger_code      = []

    def __getattr__(self, name):
        a   = getattr(self.class_logger_obj, name)
        #print('class_logger: ', type(a), ' : ', name)
        if type(a) in [ types.MethodType, types.FunctionType ]:
            def hf(*args, **kwargs):
                res = a(*args, **kwargs)
                self.class_logger_add_call(name, *args, **kwargs)
                return res
            return hf
        else: return a

    def class_logger_add_call(self, name, *args, **kwargs):
        """ TODO copy arrays to avoid overwriting? """
        self.class_logger_code.append((name, args, kwargs))
    def class_logger_get_code(self):
        return self.class_logger_code

    def class_logger_clean(self):       
        self.class_logger_code = []
    def class_logger_del(self, line):
        if int == type(line): self.class_logger_code.pop(line)
        if list == type(line): 
            for i in line: self.class_logger_code[i] = None
            self.class_logger_code = filter(lambda x: (not x is None), 
                                            self.class_logger_code)

    def class_logger_repeat(self, newobj):
        for c in self.class_logger_code:
            name, args, kwargs  = c
            getattr(newobj, name)(*args, **kwargs)

# external alias for `class_logger_repeat' method
def repeat(cx, ax): cx.class_logger_repeat(ax)


# alias for `class_logger' with meaningful name
axes_log = class_logger

"""
# test class
class X:
    def __init__(self): pass
    def X_id(self): return ("X:0x%08x" %(id(self),))
    def go(self): print("%s\n" % self.X_id())
    def __getattr__(self, name):
        if name in ['trait_names', '_getAttributeNames']:
            raise AttributeError
        def handler(*args, **kwargs):
            print("%s:(call %s (%s, %s))" % \
                    (self.X_id(), name, str(args), str(kwargs)))
        return handler
"""


def ndarray2str(a, sep = ['\n\n\n', '\n\n', '\n', ' ']):    
    # sic! cannot use '\t' in sep: iPython gets confused when c&p code with '\t'
    a   = np.asarray(a)
    dim = len(a.shape)
    assert dim <= 4

    def nd2str_step(a, sep):
        if len(a.shape) <= 0: return repr(a)
        assert len(sep) == len(a.shape)
        if 1 == len(a.shape): return sep[0].join([ repr(x) for x in a ])
        else: return sep[0].join([ nd2str_step(x, sep[1:]) for x in a ])
    return nd2str_step(a, sep[len(sep) - dim:])
    
def str2ndarray(a, maxdim=4, sep = ['\n\n\n', '\n\n', '\n', None]):
    # sic! any whitespace on the lowest level
    """

        a       string to convert
        maxdim  dimension of the array will be equal to maxdim
                with shape padded with 1 on the left if necessary;
                exc.raised if the string has separators of higher order
    """
    def str2nd_step(a, sep):
        if len(sep) <= 0: return float(a)
        return [ str2nd_step(x, sep[1:]) 
                 for x in filter(lambda y: 0 < len(y), a.split(sep[0])) ]

    a   = a.strip()     # crucial if maxdim=1 and `a' has '\n's
    for s in sep[:len(sep) - maxdim]:
        if 0 < a.count(s): 
            raise ValueError('string represents an array of dim > maxdim')
    return np.array(str2nd_step(a, sep[len(sep) - maxdim:]))

func_str2ndarray_str = """
def str2ndarray(a, maxdim=4, sep = ['\\n\\n\\n', '\\n\\n', '\\n', None]):
    def str2nd_step(a, sep):
        if len(sep) <= 0: return float(a)
        return [ str2nd_step(x, sep[1:]) 
                 for x in filter(lambda y: 0 < len(y), a.split(sep[0])) ]

    a   = a.strip()  
    for s in sep[:len(sep) - maxdim]:
        if 0 < a.count(s): 
            raise ValueError('string represents an array of dim > maxdim')
    return np.array(str2nd_step(a, sep[len(sep) - maxdim:]))
"""

def save_code(cx, obj_name, indent='', dindent='    ', lineno=None, fo=None):
    """ print code: 
        each parameter on a newline 
        * print on request
        * special treatment of ndarray objects: converted to tsv strings
    """
    def a2str(a):
        if np.ndarray == type(a):
            return 'str2ndarray("""\n%s\n""", maxdim=%d)' % \
                    (ndarray2str(a), len(a.shape))
        else: return repr(a)

    if None is fo: fo = sys.stdout
    def get_lineno_str(n_line, n_subline):
        if (not lineno) and (0 != lineno): return ''
        if None is n_line: 
            if None is n_subline: return ''
            else: return '   .%-3d ' % n_subline
        else:
            if None is n_subline: return '%3d     ' % (n_line + lineno)
            else: return ('%3d.%-3d ' % (n_line + lineno, n_subline))
    
    for n_line, c in enumerate(cx.class_logger_get_code()):
        name, args, kwargs = c
        n_args  = len(args) + len(kwargs)
        #print(name, args, kwargs, n_args)
        if (n_args <= 0):
            fo.write("%s%s%s.%s()\n" % \
                        (get_lineno_str(n_line, None), 
                         indent, obj_name, name))
        else:
            fo.write("%s%s%s.%s(\n" % \
                        (get_lineno_str(n_line, None), 
                         indent, obj_name, name))

            n_subline = 0
            for a in args:
                n_subline += 1
                fo.write("%s%s%s%s%s\n" % \
                            (get_lineno_str(n_line, n_subline), 
                             indent, dindent, a2str(a),
                             ',' if n_subline < n_args else ')'))
                
            for k,a in iteritems(kwargs):
                n_subline += 1
                fo.write("%s%s%s%s=%s%s\n" % \
                            (get_lineno_str(n_line, n_subline), 
                             indent, dindent, k, a2str(a),
                             ',' if n_subline < n_args else ')'))

def save_axdraw(cx, func_name, axes_name=None, indent='', dindent='    ', 
                fo=sys.stdout):

    if None is axes_name: axes_name = 'ax'
    fo.write('%sdef %s(%s):\n' % (indent, func_name, axes_name))
    save_code(cx, axes_name, indent=indent + dindent, dindent=dindent, 
              lineno=None, fo=fo)

def save_aux_func(fo):
    fo.write("\n%s\n\n" % func_str2ndarray_str)

def save_figdraw(fo,  fig_name='fig',  
                 indent='', dindent='    ', **cx_dict):
    if isinstance(fo, basestring) : 
        fo = open(fo, 'w')
    elif isinstance(fo, file) :
        assert ('w' in fo.mode)
    else: raise TypeError(fo)

    fo.write('%simport matplotlib as mpl\n' % indent)
    fo.write('%simport matplotlib.pyplot as plt\n' % indent)
    fo.write('%simport numpy as np\n' % indent)
    #fo.write("%smpl.rcParams.update(**%s)\n" % (indent, pprint.pformat(mpl.rcParams)))
    fo.write("%smpl.rcParams.update(**%s)\n" % (indent, pprint.pformat(
                dict(iteritems(mpl.rcParams)))))
    save_aux_func(fo)

    fo.write('%s%s = plt.figure()\n' % \
             (indent, fig_name))

    for cx_name, cx in iteritems(cx_dict):
        func_name   = 'draw_%s' % cx_name
        axes_name   = 'axes_%s' % cx_name
        save_axdraw(cx, func_name, axes_name, indent=indent, dindent=dindent,
                    fo=fo)
        axes_bounds = cx.class_logger_obj.get_position().bounds
        fo.write('%s%s = %s.add_axes(%s)\n' % (indent, axes_name, fig_name, 
                                               repr(axes_bounds)))
        fo.write('%s%s(%s)\n' % (indent, func_name, axes_name))
        """
        fo.write('%sdef %s(%s):\n' % (indent, func_axdraw_name, ax_name))
        save_code(cx, ax_name, indent=indent + dindent, dindent=dindent, 
                  lineno=None, fo=fo)
        fo.write('%s%s = %s.add_axes(%s)\n' % (indent, ax_name, fig_name, 
                                               repr(cx.get_position().bounds))
        fo.write('%s%s(%s)\n' % (indent, func_axdraw_name, ax_name))
        """

def save_rcparams(indent='', fo=sys.stdout): 
    fo.write("%smpl.rcParams.update(**%s)\n" % \
             (indent, pprint.pformat(mpl.rcParams)))

class axes_savefig(class_logger) :
    def __init__(self, *p, **d) :
        class_logger.__init__(self, make_std_axes(*p, **d))
    def savefig(self, fo) : self.class_logger_obj.figure.savefig(fo)
    def save_figdraw(self, fo): save_figdraw(fo, ax=self)
        

def make_savefig_axes(*p, **d):
    return axes_savefig(*p, **d)

