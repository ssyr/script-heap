from __future__ import print_function
from future.utils import iteritems
from past.builtins import basestring
import numpy as np
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import pprint
import itertools as it

mpl.rc('text', usetex=True)     # tex 
mpl.rc('font', family='serif')  # tex
mpl.rcParams.update({
    #'xtick.labelsize'   : 'medium',
    'xtick.color'       : 'k',
    #'ytick.labelsize'   : 'medium',
    'ytick.color'       : 'k',
    #'axes.labelsize'    : 'medium',
    #'legend.fontsize'   : 'xx-small',


    'text.usetex'       : True,
    'font.family'       : 'serif',
    'font.weight'       : 'normal',
    'font.size'         : 16,
    #'font.color'        : 'k'
})

# marker types
fmt_marker_default = [
    { 'c': 'r', 'marker': '+', 'ls':' ' },
    { 'c': 'g', 'marker': 'x', 'ls':' ' }, 
    { 'c': 'b', 'marker': 's', 'ls':' ' }, 
    { 'c': 'c', 'marker': 'o', 'ls':' ' }, 
    { 'c': 'm', 'marker': 'd', 'ls':' ' }, 
    { 'c': 'y', 'marker': 'h', 'ls':' ' },
    { 'c': 'r', 'marker': 'v', 'ls':' ' }, 
    { 'c': 'g', 'marker': '^', 'ls':' ' }, 
    { 'c': 'b', 'marker': '>', 'ls':' ' }, 
    { 'c': 'c', 'marker': '<', 'ls':' ' }, 
    { 'c': 'm', 'marker': 'D', 'ls':' ' }, 
    { 'c': 'y', 'marker': 'p', 'ls':' ' },
    { 'c': 'r', 'marker': '1', 'ls':' ' }, 
    { 'c': 'g', 'marker': '2', 'ls':' ' }, 
    { 'c': 'b', 'marker': '3', 'ls':' ' }, 
    { 'c': 'c', 'marker': '4', 'ls':' ' }, 
    { 'c': 'm', 'marker': 'p', 'ls':' ' }, 
    { 'c': 'y', 'marker': 'h', 'ls':' ' }
   ]

# colors suitable for presentations
# TODO develop robust colors with multiple palettes
fmt_marker_robust = [
    { 'c': (.7,0,0),    'marker':'+',   'ls':' ' },
    { 'c': (0,.4,0),    'marker':'x',   'ls':' ' },
    { 'c': (0,0,1),     'marker':'s',   'ls':' ' },
    { 'c': (0,.7,.7),   'marker':'o',   'ls':' ' },
    { 'c': '#808000',   'marker':'d',   'ls':' ' },
    { 'c': '#e000d0',   'marker':'h',   'ls':' ' },

    { 'c': '#ff6600',   'marker':'v',   'ls':' ' },
    { 'c': '#7d26cd',   'marker':'^',   'ls':' ' },
    { 'c': '#b7410e',   'marker':'>',   'ls':' ' },
    { 'c': '#990066',   'marker':'<',   'ls':' ' },
    ]
fmt_line_robust = [
    { 'c': (.7,0,0),    'ls':'-' },
    { 'c': (0,.4,0),    'ls':'-' },
    { 'c': (0,0,1),     'ls':'-' },
    { 'c': (0,.7,.7),   'ls':'-' },
    { 'c': '#808000',   'ls':'-' },
    { 'c': '#e000d0',   'ls':'-' },

    { 'c': '#ff6600',   'ls':'-' },
    { 'c': '#7d26cd',   'ls':'-' },
    { 'c': '#b7410e',   'ls':'-' },
    { 'c': '#990066',   'ls':'-' },
    ]
def mkfmt_shade(f, alpha, **kwargs):
    if list == type(alpha): a = alpha[i]
    else: a = alpha
    if dict == type(f):
        res = dict(f)
        c = mpl.colors.colorConverter.to_rgb(f['c'])
    else: 
        res = {}
        c = mpl.colors.colorConverter.to_rgb(f)
    # for EPS (no transparency)
    #res['facecolor'] = tuple([ 1. - a * (1. - ci) for ci in c ])
    res['facecolor'] = mpl.colors.colorConverter.to_rgba(c, a)
    res['edgecolor'] = c
    #res['linestyle'] = ' '
    #res['linewidth'] = 0
    for k,v in iteritems(kwargs): res[k] = v
    return res

def ls_rotate(ls, i):
    """ return ls or , (i%len(ls))-th elem of ls if ls is a list """
    if list == type(ls): return ls[ i % len(ls) ]
    else: return ls

def first_not_none(*a) :
    for x in a :
        if not None is x : return x
    return None

class style_group:
    def mkcol_(self, c, a) :
        # XXX this function replaces alpha; combine alphas instead?
        if None is c or 'none' == c : return c
        else : return mpl.colors.colorConverter.to_rgba(c, a)

    def copy(self, **new) :
        newd    = dict(self.extra_)
        for k in ['mec', 'mfc', 'edgecolor', 'facecolor' ] :
            if k in newd : del newd[k]
        newd.update(new)
        lc      = newd.pop('lc', self.lc)
        ls      = newd.pop('ls', self.ls)
        marker  = newd.pop('marker', self.marker)
        #print(lc, ls, marker, newd)
        return style_group(lc, ls, marker, **newd)
    def cp(self, **new) : return self.copy(**new)

    def mk_colors_(self, lc, alpha, alpha_shade) :
        return { 
            'mec'       : self.mkcol_(lc, alpha),
            'mfc'       : self.mkcol_(lc, alpha_shade),
            'edgecolor' : self.mkcol_(lc, alpha),
            'facecolor' : self.mkcol_(lc, alpha_shade) }

    def __init__(self, lc, ls, marker,  
                #alpha=.8, alpha_shade=.3, 
                alpha=None, alpha_shade=None, 
                alpha_mec=None, alpha_mfc=None, 
                alpha_ec=None, alpha_fc=None, 
                **extra_):
        # TODO: select lc, marker from cycle over in-class value lists 

        assert (not lc is None) and (not ls is None) and (not marker is None)



        if None is alpha: self.lc = lc
        else: self.lc = self.mkcol_(lc, alpha)

        self.ls     = ls
        self.marker = marker

        self.extra_ = dict(extra_)
        self.extra_['alpha']        = alpha
        self.extra_['alpha_shade']  = alpha_shade
        self.extra_['ls']           = ls
        self.extra_['marker']       = marker

        #alpha_mec = first_not_none(alpha_mec, alpha)
        #alpha_mfc = first_not_none(alpha_mfc, alpha_shade, alpha)
        #alpha_ec  = first_not_none(alpha_ec, alpha)
        #alpha_fc = first_not_none(alpha_fc, alpha_shade, alpha)
        #self.to_def('mec', self.mkcol_(lc, alpha_mec))
        #self.to_def('mfc', self.mkcol_(lc, alpha_mfc))
        #self.to_def('edgecolor',  self.mkcol_(lc, alpha_ec))
        #self.to_def('facecolor',  self.mkcol_(lc, alpha_fc))

        def upd_color(d, k, alpha=None) :
            c = d.get(k, lc)
            if not None is alpha : c = self.mkcol_(c, alpha)
            d[k] = c
        upd_color(self.extra_, 'mec', alpha_mec or alpha_shade)
        upd_color(self.extra_, 'mfc', alpha_mfc or alpha_shade)
        upd_color(self.extra_, 'edgecolor', alpha_ec or alpha_shade)
        upd_color(self.extra_, 'facecolor', alpha_fc or alpha_shade)

        #for k,v in iteritems(self.mk_colors_(lc, alpha, alpha_shade)) :
            #self.to_def(k, v)


    def set_(self, *list_k, **ini):
        for k in list_k:
            if k in self.extra_: 
                ini[k] = self.extra_[k]
        return ini
    def or_(*l):
        for x in l:
            if not None is  x: return x
        return None
    def to_def(self, k, val=None):
        if not k in self.extra_:  self.extra_[k] = val
        elif None is self.extra_[k]:    self.extra_[k] = val
    def eor_(*l):
        for x in l:
            if x in self.extra_: return self.extra_[x]
        return None
    def line(self):     return self.set_('alpha', 
                                'lw',
                                color=self.lc,
                                ls=self.ls, marker='')
    def dot(self):      return self.set_('alpha',
                                'ms', 'mew', 'mec', 'mfc', 
                                color=self.lc, 
                                ls='', marker=self.marker)
    def edot(self):     return self.set_('alpha', 
                                'ms', 'mew', 'mec', 'mfc', 'elinewidth',
                                color=self.lc, 
                                ls='', marker=self.marker)
    def dotline(self):  return self.set_('alpha',
                                'ms', 'mew', 'mec', 'mfc', 
                                color=self.lc, 
                                ls=self.ls, marker=self.marker)
    def edotline(self): return self.set_('alpha',
                                'ms', 'mew', 'mec', 'mfc', 'elinewidth',
                                color=self.lc, 
                                ls=self.ls, marker=self.marker)
    def band(self):     return self.set_('edgecolor', 'facecolor', 'linewidth',
                                linestyle=self.ls)

# empty style group : return empty kwarg for any plot option
class style_group_None(style_group) :
    def __init__(self) : pass
    def set_(self) : return {}

#class style_group_cycle(style_group):
    #""" a cyclic list of style groups; each call to 'line', 'dot', ... functions switches to the next entry on the list """
    #def __init__(self, stg_list):
        #self.stg_list = stg_list
        #assert 0 < len(stg_list)
        #self.cnt = 0
    #def __getattr__(self, attr):
        #if attr in [ 'line', 'dot', 'edot', 'dotline', 'edotline', 'band']:
            #if len(self.stg_list) <= self.cnt: self.cnt = 0
            #return getattr(self.stg_list[self.cnt], attr)
        #else 
    #def next(self, attr):
        #self.cnt += 1
        #if len(self.stg_list) <= self.cnt: self.cnt = 0

# select sufficiently different markers and colors
marker_list_default = [
    'o', 's', 'd', 
    'v', '^', '>', '<', 
    '+', 'x', '*'] #10
color_list_default = [
    (0,0,1),
    (.7,0,0),
    (0,.4,0),
    (0.5,0,.5),
    (0,.5,.5),
    (.5,.5,.0),
#    '#808000',
    '#e000d0',
    '#ff6600',
    '#7d26cd',
    '#b7410e',
    '#990066',
    '#ff6600',
    '#7d26cd',
    '#b7410e',
    '#990066',
] #14

#style_group_default = [
    #style_group((0,0,1),   '-', 'o'),
    #style_group((.7,0,0),  '-', 's'),
    #style_group((0,.4,0),  '-', 'd'),
    #style_group((0,.7,.7), '-', 'x'),
    #style_group('#808000', '-', '+'),
    #style_group('#e000d0', '-', 'h'),
#
    #style_group('#ff6600', '-', 'v'),
    #style_group('#7d26cd', '-', '^'),
    #style_group('#b7410e', '-', '>'),
    #style_group('#990066', '-', '<'),
    #
    #style_group('#ff6600', '-', 'o'),
    #style_group('#7d26cd', '-', 's'),
    #style_group('#b7410e', '-', 'd'),
    #style_group('#990066', '-', 'x'),
#]
# all combinations
style_group_default = [
        style_group(c, '-', m, alpha=0.8, alpha_shade=0.3) 
        for c, m in it.islice(
            zip(it.cycle(color_list_default), it.cycle(marker_list_default)), 
            9*14) ]
style_group_black = style_group((0,0,0), '-', 'o', alpha=0.8, alpha_shade=0.3)

# default set
fmt_color_default = [
    (1., 0., 0.),
    (0., 1., 0.),
    (0., 0., 1.),
    (0., 1., 1.),
    (1., 0., 1.),
    (1., 1., 0.)
    ]
fmt_shade_default = [ mkfmt_shade(c, 0.3) for c in fmt_color_default ]

# robust set
fmt_color_robust = [
    (.7,0,0), 
    (0,.4,0), 
    (0,0,1),  
    (0,.7,.7),
    '#808000',
    '#e000d0',
              
    '#ff6600',
    '#7d26cd',
    '#b7410e',
    '#990066'
]
fmt_shade_robust = [ mkfmt_shade(c, 0.3) for c in fmt_color_robust ]


def modify_format(f, **kwargs):
    """ replace kwargs in a list of dicts 
        usage: fmt=modify_format(fmt_marker_robust, ls='--')    
        # (change linestyle to dashed)
    """
    if dict == type(f):
        new_l = type(f)(f)      # copy
        for k,v in iteritems(kwargs):
            new_l[k] = v
        return new_l
    elif list == type(f):
        new_f = []
        for l in f:
            new_f.append(modify_format(l, **kwargs))
        return new_f
    else: raise RuntimeError('cannot modify type(fmt)=%s' % str(type(fmt)))

def calc_window(x1, x2, m=0, scale='linear'):
    """ calculate window centered on (min+max)/2 wrt scale
        x1, x2  range
        m       fractional margin
        scale   type of axis scale
    """
    def shift(x1, x2, m):
        from math import log, exp
        if scale == 'linear':   return (1. + m) * x1 - m * x2
        elif scale == 'log' :
            assert 0 < x1 and 0 < x2
            return exp((1. + m) * log(x1) - m * log(x2))
    if tuple == type(m): return (shift(x1, x2, m[0]), shift(x2, x1, m[1]))
    else: return (shift(x1, x2, m), shift(x2, x1, m))

def set_xlim(ax, amin=None, amax=None, **kw):
    if 'xmargin' in kw : m = kw['xmargin']
    else: m = .1
    if (amin is None) or (amax is None) : ax.set_autoscalex_on()
    elif ('xmin' in kw) and ('xmax' in kw) :
        ax.set_xlim(kw['xmin'], kw['xmax'])
    else:
        ax.set_xlim(*calc_window(amin, amax, m, ax.get_xscale()))
def set_ylim(ax, amin=None, amax=None, **kw):
    m = kw.get('ymargin', 1)
    if amin is None or amax is None: ax.set_autoscaley_on()
    elif ('ymin' in kw) and ('ymax' in kw) :
        ax.set_ylim(kw['ymin'], kw['ymax'])
    else:
        ax.set_ylim(*calc_window(amin, amax, m, ax.get_yscale()))

def adjust_xlim(ax, amin=None, bmin=None, amax=None, bmax=None) :
    """ include(a, exclude(b, current)) """
    xmin, xmax = ax.get_xlim()

    if None is xmin : xmin = bmin
    elif not (None is bmin) : xmin = max(xmin, bmin)

    if None is xmin : xmin = amin
    elif not (None is amin) : xmin = min(xmin, amin)

    if None is xmax : xmax = bmax
    elif not (None is bmax) : xmax = min(xmax, bmax)

    if None is xmax : xmax = amax
    elif not (None is amax) : xmax = max(xmax, amax)

    ax.set_xlim(xmin, xmax)

def adjust_ylim(ax, amin=None, bmin=None, amax=None, bmax=None) :
    """ include(a, exclude(b, current)) """
    ymin, ymax = ax.get_ylim()

    if None is ymin : ymin = bmin
    elif not (None is bmin) : ymin = max(ymin, bmin)

    if None is ymin : ymin = amin
    elif not (None is amin) : ymin = min(ymin, amin)

    if None is ymax : ymax = bmax
    elif not (None is bmax) : ymax = min(ymax, bmax)

    if None is ymax : ymax = amax
    elif not (None is amax) : ymax = max(ymax, amax)

    ax.set_ylim(ymin, ymax)

def plot_data2D(ax, i_data, x, y, err=None, label=None,
                   fmt=fmt_marker_default, fmt_line=None):
    """ Plot N data groups with specific format and tracing line(s)
        useful to have it in one place not avoid cluttering of the code
        ax      "axes"
        i_data  index
        x       N-array
        y       N-array
        err     N-array
        label   string
        fmt     marker format
        fmt_line    line style
    """
    def get_fmt(i_data):
        i_fmt = fmt
        if   isinstance(i_fmt, list) : i_fmt = fmt[ i_data % len(i_fmt) ]
        elif isinstance(i_fmt, basestring)  : return { 'fmt' : i_fmt }
        elif isinstance(i_fmt, dict) : return i_fmt
        else: raise ValueError('bad fmt=%s' % str(i_fmt))

    if not None is  err: ax.errorbar(x, y, err, label=label, **get_fmt(i_data))
    else: ax.plot(x, y, label=label, **get_fmt(i_data))
    #if not None is  fmt_line:
    #    i_fmt_l = fmt_line
    #    if list == type(fmt_l): fmt_l = fmt_l[ i_data % len(fmt_l) ]
    #    ax.plot(x, y, err, label=label, fmt=fmt_l)


def set_ticks_closex_(nx, ix, ax):
    assert 0 <= ix and ix < nx
    if 0 == ix:         ax.tick_params(axis='y', labelleft='on',  labelright='off')
    elif nx - 1 == ix:  ax.tick_params(axis='y', labelleft='off', labelright='on')
    else:               ax.tick_params(axis='y', labelleft='off', labelright='off')
def set_ticks_closey_(ny, iy, ax):
    assert 0 <= iy and iy < ny
    if ny - 1 == iy:    ax.tick_params(axis='x', labeltop='off', labelbottom='on')
    elif 0 == iy:       ax.tick_params(axis='x', labeltop='on',  labelbottom='off')
    else:               ax.tick_params(axis='x', labeltop='off', labelbottom='off')

def make_ysubplot(ny, fig=None, size=None,
                  closey=False, xlabel=None, ylabel=None):
    """ arrange axes vertically """
    if None == fig: fig = plt.figure()
    if None != size: fig.set_size_inches(size, forward=True)
    res = [ fig.add_subplot(ny, 1, 1+i) for i in range(ny) ]
    if closey :
        fig.subplots_adjust(hspace=0.)
        for j in range(ny):
            set_ticks_closey_(ny, j, res[j])
    if not None is xlabel :
            res[0].set_xlabel(xlabel)
    if not None is ylabel :
        for j in range(ny) :
            res[j].set_ylabel(ylabel)
    return res

def make_xsubplot(nx, fig=None, size=None,
                  closex=False, xlabel=None, ylabel=None):
    """ arrange axes horizontally """
    if None == fig: fig = plt.figure()
    if None != size: fig.set_size_inches(size, forward=True)
    res = [ fig.add_subplot(1, nx, 1+i) for i in range(nx) ]
    if closex:
        fig.subplots_adjust(wspace=0.)
        for i in range(nx): set_ticks_closex_(nx, i, res[i])
    if not None is xlabel :
        for i in range(nx) : 
            res[i].set_xlabel(xlabel)
    if not None is ylabel :
        res[0].set_ylabel(ylabel)
    return res

def make_xysubplot(nx, ny, fig=None, size=None, flat=True, 
                   closex=False, closey=False,
                   xlabel=None, ylabel=None):
    """ arrange axes horizontally (closest neighbors), then vertically """
    if None is fig: fig = plt.figure()
    if not None is size: fig.set_size_inches(size, forward=True)
    res = [ [ fig.add_subplot(ny, nx, 1 + i + nx * j) for i in range(nx) ] for j in range(ny) ]
    if closex:
        fig.subplots_adjust(wspace=0.)
        for j in range(ny):
            for i in range(nx): set_ticks_closex_(nx, i, res[j][i])
    if closey:
        fig.subplots_adjust(hspace=0.)
        for j in range(ny):
            for i in range(nx): set_ticks_closey_(ny, j, res[j][i])
    if not None is xlabel :
        for i in range(nx) : 
            res[-1][i].set_xlabel(xlabel)
    if not None is ylabel :
        for j in range(ny) :
            res[j][0].set_ylabel(ylabel)
    if flat: return reduce(lambda x,y:x+y, res, [])
    else: return res
def make_yxsubplot(nx, ny, fig=None, size=None, flat=True, 
                   closex=False, closey=False, 
                   xlabel=None, ylabel=None):
    """ arrange axes vertically (closest neighbors), then horizontally """
    if None is fig: fig = plt.figure()
    if not None is size: fig.set_size_inches(size, forward=True)
    res = [ [ fig.add_subplot(ny, nx, 1 + i + nx * j) for j in range(ny) ] for i in range(nx) ]
    if closex:
        fig.subplots_adjust(wspace=0.)
        for j in range(ny):
            for i in range(nx): set_ticks_closex_(nx, i, res[i][j])
    if closey:
        fig.subplots_adjust(hspace=0.)
        for j in range(ny):
            for i in range(nx): set_ticks_closey_(ny, j, res[i][j])
    if not None is xlabel :
        for i in range(nx) : 
            res[i][-1].set_xlabel(xlabel)
    if not None is ylabel :
        for j in range(ny) :
            res[0][j].set_ylabel(ylabel)
    if flat: return reduce(lambda x,y:x+y, res, [])
    else: return res

def adjust_multiplot_xlim(ax_list, inc_min=0, inc_max=0):
    xlim_r = []
    for ax in ax_list: xlim_r.extend(ax.get_xlim())
    xlim_r = np.asarray(xlim_r)
    xlim_new = [ xlim_r.min() + inc_min, xlim_r.max() + inc_max ]
    for ax in ax_list: ax.set_xlim(xlim_new)
def adjust_multiplot_ylim(ax_list, inc_min=0, inc_max=0):
    ylim_r = []
    for ax in ax_list: ylim_r.extend(ax.get_ylim())
    ylim_r = np.asarray(ylim_r)
    ylim_new = [ ylim_r.min() + inc_min, ylim_r.max() + inc_max ]
    for ax in ax_list: ax.set_ylim(ylim_new)

def set_labels_xyplot_(res, xlabel=None, ylabel=None) :
    nx, ny = len(res[0]), len(res)
    if not (None is xlabel) :
        if not isinstance(xlabel, list) : xlabel = [xlabel] * nx
        for ix in range(nx) : res[-1][ix].set_xlabel(xlabel[ix])
    if not (None is ylabel) :
        if not isinstance(ylabel, list) : ylabel = [ylabel] * ny
        for iy in range(ny) : res[iy][0].set_ylabel(ylabel[iy])

def set_labels_yxplot_(res, xlabel=None, ylabel=None) :
    nx, ny = len(res), len(res[0])
    if not (None is xlabel) :
        if not isinstance(xlabel, list) : xlabel = [xlabel] * nx
        for ix in range(nx) : res[ix][-1].set_xlabel(xlabel[ix])
    if not (None is ylabel) :
        if not isinstance(ylabel, list) : ylabel = [ylabel] * ny
        for iy in range(ny) : res[0][iy].set_ylabel(ylabel[iy])

# TODO add set_labels_yxplot

def make_xysplitplot(spx, spy, fig=None, rect=None, w_pad=0, h_pad=0, **kw) :
    """ make xy grid of axes with dimensions proportional to xsp, ysp
        xsp, ysp are normalized (all must be positive)
    """
    def norm_split(sp, pad, w) :
        if isinstance(sp, int) : sp = [1.] * sp
        sp  = np.asarray(sp)
        nsp = len(sp)
        assert((0 < sp).all())
        tsp = sp.sum() + pad * (nsp - 1)
        sp  = sp / float(tsp)
        csp = 1. - np.cumsum(sp) - np.r_[:nsp] * pad / float(tsp)
        return w * sp, w * csp
    
    x0, y0, dx, dy = rect
    spx, cspx   = norm_split(spx, w_pad, dx)
    spy, cspy   = norm_split(spy, h_pad, dy)
    nx          = len(spx)
    ny          = len(spy)
    res  = [ [ fig.add_axes((x0 + cspx[ix], y0 + cspy[iy], spx[ix], spy[iy]))
               for ix in range(nx) ]
             for iy in range(ny) ] 
    for iy in range(ny) : 
        for ix in range(nx): 
            set_ticks_closex_(nx, ix, res[iy][ix])
            set_ticks_closey_(ny, iy, res[iy][ix])
    set_labels_xyplot_(res, **kw)
    if 1 == nx : res = [ res[iy][0] for iy in range(ny) ]
    if 1 == ny : res = res[0]
    return res


def make_std_axes():
    return plt.figure().add_axes([.16, .16, .8, .8])
        
def set_std_axes(ax, **kw):
    #print("set_std_axes: kw.keys=%s" % kw.keys())
    for k, v in iteritems(kw) :
        if   ("xlabel" == k or "xl" == k) and (not None is v) : ax.set_xlabel(v)
        elif ("ylabel" == k or "yl" == k) and (not None is v) : ax.set_ylabel(v)
        elif ("xlim" == k or 'xr' == k) and (not None is v) : ax.set_xlim(v)
        elif ("ylim" == k or 'yr' == k) and (not None is v) : ax.set_ylim(v)
        elif ("title" == k) and (not None is v)   : ax.set_title(v)

def close_fig(fig):
    fig.clf()
    plt.close(fig)

def calc_symlog_linthresh(vmin, vmax, base=10., linfrac_max=.2, stops=None):
    """ compute appropriate linear threshold for a symlog scale 
        if interval [vmin, vmax] contains zero. The result is (b^a) such that 
        the linear fraction the plot is in (b^{a-1}, b^a] range
        stops   if not None, consider intervals separated by {stops * b^a} 
                instead of {b^a}; 1 <=stops[i] < base
        linfrac  (lin) / (lin + log); 0 < linfrac <=1

        XXX if vmin, vmax have the same sign, force using only log scale 
            by returning min(abs(vmin), abs(vmax))
        TODO separate case if |vmin| << |vmax| (or v/v) so that resulting linthresh > |vmin|
    """
    lb  = math.log(base)
    if vmax < vmin: vmin, vmax = vmax, vmin
    if 0 < vmin: return vmin
    elif vmax < 0: return -vmax
    elif 0 == vmin: l_vr = math.log(abs(vmax)) / lb
    elif 0 == vmax: l_vr = math.log(abs(vmin)) / lb
    else: l_vr = (math.log(abs(vmax)) + math.log(abs(vmin))) / lb / 2.

    l_vr = l_vr - (1. / linfrac_max - 1.)
    a       = int(math.floor(l_vr))
    if not None is  stops:
        s_max  = base ** (l_vr - a)
        s =  max(filter(lambda s: s <= s_max, stops))
        return s * base**a
    else: return base**a

