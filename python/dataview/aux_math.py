import math

def base_floor(x, base=1): return math.floor(x / base) * base
def base_ceil (x, base=1): return math.ceil (x / base) * base

def log_floor(x, base=10):
    if 0 == x: return 0
    elif x < 0: return -log_ceil(-x, base)
    else:
        lb  = math.log(base)
        return math.exp(lb * math.floor(math.log(x) / lb))
        
def log_ceil(x, base=10):
    if 0 == x: return 0
    elif x < 0: return -log_floor(-x, base)
    else:
        lb  = math.log(base)
        return math.exp(lb * math.ceil(math.log(x) / lb))
