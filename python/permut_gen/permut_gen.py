from __future__ import print_function
def factorial(k):
    res = 1
    for s in range(2,k+1):
        res *= s
    return res

def permut_gen(k, s_):
    s = list(s_)
    l = len(s)
    assert 0 <= k and k < factorial(l)
    for j in range(1,l):
        s[k % (j+1)], s[j] = s[j], s[k % (j+1)]
        k /= (j + 1)
    return s

def permut_lex(k, s_):
    s = list(s_)
    l = len(s)
    assert 0 < l
    assert 0 <= k and k < factorial(l)
    f = factorial(l - 1)
    for j in range(l-1):
        dj = (k / f) % (l - j)
        aux = s[j + dj]
        s[j+1 : j+dj+1] = s[j: j+dj]
        s[j] = aux
        f /= (l - j - 1)
    return s



def test_permut(l):
    assert 0 < l
    s0 = range(l)
    k = 0
    for sp in permut_lex_iter(s0):
        print(sp, "\t", permut_lex(k, s0), "\t", permut_gen(k, s0))
        k += 1
