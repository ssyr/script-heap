# some examples


# 1. sort blocks by one entry (sb,l,w):
b = numpy.array(range(720)).reshape((12,3,4,5))     # generate 12x3x4x5
b[::-1,0,0,0] = b[:,0,0,0].copy()                   # inverse the order of [:,0,0,0]
# XXX equiv to b[(slice(None,None,-1), 0, 0, 0)] = ...
b_sorted = b[b.argsort(0)[:,0,0,0]].copy()          # rearrange so that [0,0,0] is in order
b_sorted - b[b.argsort(0)[::-1,0,0,1]]              # test: compare to descending of [0,0,1]

# 2. sort lines in subblocks
b = numpy.array(range(720)).reshape((12,3,4,5))     # generate 12x3x4x5
b[:,:,::-1,0] = b[:,:,:,0].copy()                   # inverse the order of [..,:,0]
b[...,:,:] = b[...,b.argsort(2),:].copy()

def sort_by_item(a,ind):
    """ 
        sort a[:] by comparing (:,)+ind elem
        a   n-dim array
        ind (n-1)-tuple
    """
    a[...] = a[a.argsort(0)[(slice(None),)+ind]].copy()

def apply_at_nd(a,n,func,*arg):
    for i in numpy.ndindex(a.shape[:n]): func(a[i],*arg)
