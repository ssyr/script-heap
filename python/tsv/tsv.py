import sys
import numpy
import math


def array_as4d(a):
    if (type(a) != numpy.ndarray): return numpy.array([[[[a]]]])
    if (len(a.shape) >= 4):     return a
    elif (len(a.shape) == 0):   return a.reshape((1,1,1,1))
    elif (len(a.shape) == 1):   return a.reshape((1,1,1) + a.shape)
    elif (len(a.shape) == 2):   return a.reshape((1,1) + a.shape)
    elif (len(a.shape) == 3):   return a.reshape((1,) + a.shape)


def write_format(fo, tab, format='%.16e'):
    tab = array_as4d(tab)
    for i_tb, tb in enumerate(tab):
        for i_ts, ts in enumerate(tb):
            for i_tl, tl in enumerate(ts):
                for i_tr, tr in enumerate(tl):
                    fo.write((format % tr) +'\t')
                fo.write('\n')
            if (1+i_ts < len(tb)): fo.write('\n')
        if (1+i_tb < len(tab)): fo.write('\n\n')


def write_tex(fo, tab, format='%.16f'):
    tab = array_as4d(tab)
    for i_tb, tb in enumerate(tab):
        for i_ts, ts in enumerate(tb):
            for i_tl, tl in enumerate(ts):
                for i_tr, tr in enumerate(tl):
                    if (0 < i_tr): fo.write(' &  ')
                    fo.write((format % tr))
                if (1+i_tl < len(ts)): fo.write('  \\\\\n')
            if (1+i_ts < len(tb)): fo.write('  \\\\\n%\n\\hline\n')
        if (1+i_tb < len(tab)): fo.write('  \\\\\n%\n%\n\\hline\\hline\n')
    fo.write("\n")

def write_tex_val_err(fo, tab_val, tab_err, errmax=100, precmax=8):
    tab_val = array_as4d(tab_val)
    tab_err = array_as4d(tab_err)
    for i_tb, tb in enumerate(tab_val):
        for i_ts, ts in enumerate(tb):
            for i_tl, tl in enumerate(ts):
                for i_tr, tr in enumerate(tl):
                    if (0 < i_tr): fo.write(' &  ')
                    fo.write(spprint_ve(tr, tab_err[i_tb,i_ts,i_tl,i_tr], 
                                errmax, precmax) +'\t')
                if (1+i_tl < len(ts)): fo.write('  \\\\\n')
            if (1+i_ts < len(tb)): fo.write('  \\\\\n%\n\\hline\n')
        if (1+i_tb < len(tab_val)): fo.write('  \\\\\n%\n%\n\\hline\\hline\n')
    fo.write("\n")

def write_math(fo, tab, format='%.16f'):
    tab = array_as4d(tab)
    for i_tb, tb in enumerate(tab):
        fo.write('{')
        for i_ts, ts in enumerate(tb):
            fo.write('{')
            for i_tl, tl in enumerate(ts):
                fo.write('{')
                for i_tr, tr in enumerate(tl):
                    if (0 < i_tr): fo.write(',')
                    fo.write((format % tr))
                fo.write('}')
                if (1+i_tl < len(ts)): fo.write(',\n')
            fo.write('}')
            if (1+i_ts < len(tb)): fo.write(',\n')
        fo.write('}')
        if (1+i_tb < len(tab)): fo.write(',\n')
    fo.write("\n")


def exp10(pwr):
    abspwr = abs(pwr)
    res = 1
    a = 10
    while 0 < abspwr:
        if abspwr % 2 == 1: res *= a
        abspwr /= 2
        a *= a
    if (0 <= pwr): return res
    else: return 1. / res


def spprint_ve(val, err, errmax=100, precmax=8):
    val_norm, pwr_val = 0,0
    if numpy.isnan(val): return 'nan'
    if numpy.isnan(err): err = 0.
    assert 0 <= err
    if val != 0:
        pwr_val = int(math.floor(math.log10(abs(val))))
        val_norm= val * exp10(-pwr_val)
        if err != 0:
            pwr_err = int(math.ceil(math.log10(float(err) / errmax)))
            err_norm= int(err * exp10(-pwr_err))
            prec_val= pwr_val - pwr_err
            if prec_val < 0:
                err_norm *= exp10(-prec_val)
                prec_val = 0
        else:
            err_norm = 0
            prec_val = precmax
    else:
        val_norm = 0
        if err != 0: 
            pwr_err = int(math.ceil(math.log10(float(err) / errmax)))
            err_norm= int(err * exp10(-pwr_err))
            pwr_val = pwr_err
            prec_val= 0
        else: return '0(0)'
    return ('%%.%df(%%d)e%%d' % prec_val) % (val_norm, err_norm, pwr_val)


def write_val_err(fo, tab_val, tab_err, errmax=100, precmax=8):
    #assert tab_val.shape == tab_err.shape and len(tab_val.shape)==4
    tab_val = array_as4d(tab_val)
    tab_err = array_as4d(tab_err)
    for i_tb, tb in enumerate(tab_val):
        for i_ts, ts in enumerate(tb):
            for i_tl, tl in enumerate(ts):
                for i_tr, tr in enumerate(tl):
                    fo.write(spprint_ve(tr, tab_err[i_tb,i_ts,i_tl,i_tr], 
                                errmax, precmax) +'\t')
                fo.write('\n')
            if (1+i_ts < len(tb)): fo.write('\n')
        if (1+i_tb < len(tab_val)): fo.write('\n\n')


def read_float(fo):
    """ read TSV d=4 data 
        generate list [i_block][i_subblock][i_line][i_elem]
        TODO: support comments in the end of lines
    """
    newline_cnt = 0
    blocks = []
    subblocks = []
    lines = []
    for str in fo:
        if (str[0]=='#'): continue
        line = str.split()
        if (not line):
            newline_cnt += 1
            continue
        # not a newline or comment
        if (0 < newline_cnt):
            if (lines): subblocks.append(lines)
            lines = []
        if (1 < newline_cnt):
            if (subblocks): blocks.append(subblocks)
            subblocks = []
        newline_cnt = 0
        lines.append(numpy.array(map(float, line)))
    if (lines): subblocks.append(lines)
    if (subblocks): blocks.append(subblocks)
    return blocks


def shape(b, list_types=[list, numpy.ndarray]): 
    """ calculate the shape of nested list in the form 
        [(#repeats, [<nested-shape>...] or None), <next-shape>...]
    """
    if type(b) in list_types:
        shape = []
        cnt_prev = 0
        res_prev = None
        for i in b:
            res = shape(i)
            if res == res_prev: cnt_prev += 1
            else:
                if (0 < cnt_prev): shape.append((cnt_prev, res_prev))
                res_prev = res
                cnt_prev = 1
        if (0 < cnt_prev): shape.append((cnt_prev, res_prev))
        return shape
    else: return None
    

def check_rect(shape):
    """ check if the nested list shape is rectangular and return its dimensions
        otherwise, return None
        shape must be a list or None
    """
    if (shape is None): return ()
    assert type(shape) == list
    if (len(shape) != 1): return None
    res = check_rect(shape[0][1])
    if (None is res): return None
    return (shape[0][0],) + res
    

def print_shape_elem(shape_elem, next_indent='\t', delta_indent='\t', fo=sys.stdout):
    """ print head of shape without indent (assume it was indented outside)
        the rest is printed with indent, and transferred (indent=indent+delta_indent)
    """
    assert type(shape_elem) == tuple
    fo.write('%d' % shape_elem[0])
    if (not shape_elem[1] is None):
        assert type(shape_elem[1]) == list and 0 < len(shape_elem[1])
        fo.write(delta_indent)
        print_shape_elem(shape_elem[1][0], 
                             next_indent=next_indent+delta_indent, 
                             delta_indent=delta_indent, 
                             fo=fo)
        for i in range(1, len(shape_elem[1])):
            fo.write(next_indent)
            print_shape_elem(shape_elem[1][i], 
                                 next_indent=next_indent+delta_indent, 
                                 delta_indent=delta_indent, 
                                 fo=fo)
    else: fo.write('\n')


def print_shape(shape, indent='', delta_indent='\t', fo=sys.stdout):
    for t in shape:
        fo.write(indent)
        print_shape_elem(t, 
                             next_indent=indent+delta_indent,
                             delta_indent=delta_indent,
                             fo=fo)
