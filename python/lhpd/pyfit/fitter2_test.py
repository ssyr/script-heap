from __future__ import print_function
import math 
import itertools as it
import numpy as np
import h5py
import lhpd
from lhpd.pyfit import Nexp_ladder_c2pt
from lhpd.stat import gen_ens_mvar as gen_multivar_ens
from lhpd.fitter import datafit, prior_simple, datafit_join, fitter2 
def gen_c2pt(c, e, tr):
    return np.dot(
            np.exp(-np.asarray(e)) ** np.asarray(tr)[:,None],
                  np.asarray(c))
def gen_c2pt_err(cerr, merr, t_r):
    return cerr * np.exp(-merr * t_r)
def gen_c2pt_cov(mc, t_r) :
    return np.exp(-mc*np.abs(t_r[:, None] - t_r))

def gen_c2pt_ens(nsamp, t_r, c, e, cerr, merr, mcov=None) :
    xav     = gen_c2pt(c, e, t_r)
    xerr    = gen_c2pt_err(cerr, merr, t_r)
    xcov    = None
    if not None is mcov : xcov = gen_c2pt_cov(mcov, t_r)
    return gen_multivar_ens(nsamp, xav, xerr, xcov=xcov)


# test of constrained fitter with 2-exp meff


def test_nexp_c2pt_avg(tr, c2_a, c2_e, c2_cov,
        nexp_fit=2,
        method='nlm',
        h5g_out=None,
        opt_kw={}) :

    c2exp_mf= lhpd.fitter.legacy2modelfunc(Nexp_ladder_c2pt(nexp_fit))
    c2exp_fv= datafit(c2exp_mf, tr, c2_a, c2_e, c2_cov)

    c2exp_fit=fitter2(c2exp_fv, reparam=lhpd.fitter.reparam(
            *([('range', 0., None), ('range', 0., None) ] * nexp_fit) ))
    p0  = np.asarray([1., 1.] * nexp_fit)
    res = c2exp_fit.fit(p0, method=method, opt_kw=opt_kw)
    print(res)
    popt, pcov, chi2 = res
    chi2_2 = c2exp_fit.chi2(popt)
    print("popt = ", popt)
    print("chi2/dof = %e /%d" % (chi2, c2exp_fit.ndof()))
    if not None is h5g_out :
        lhpd.h5_io.h5_clean_group(h5g_out)
        h5g_out['fit_data_a'] = c2_a
        h5g_out['fit_data_e'] = c2_e
        h5g_out['xr']   = tr
        h5g_out['popt'] = popt

def test_nexp_c2pt_ens(tr, c2_ens, rsplan,
        nexp_fit=2,
        method='nlm',
        h5g_out=None,
        hack_data_slice=None,
        hack_rnd_seed=None,
        opt_kw={} ) :

    if not None is hack_rnd_seed : np.random.seed(seed=hack_rnd_seed)
    if None is hack_data_slice : hack_data_slice = np.r_[:len(c2_ens)]

    n_data = len(c2_ens)
    c2_a, c2_e, c2_cov = lhpd.calc_avg_err_ncov(c2_ens, rsplan)

    c2exp_mf    = lhpd.fitter.legacy2modelfunc(Nexp_ladder_c2pt(nexp_fit))
    c2exp_fv    = datafit(c2exp_mf, tr, c2_a, c2_e, c2_cov)
    c2exp_fit   = fitter2(c2exp_fv, reparam=lhpd.fitter.reparam(
                    *([('range', 0., None), ('range', 0., None) ] * nexp_fit) ))

    p0  = np.asarray([1., 1.] * nexp_fit)
    res = c2exp_fit.fit(p0, method=method, opt_kw=opt_kw)
    print(res)
    popt, pcov, chi2 = res
    chi2_2 = c2exp_fit.chi2(popt)
    print("popt = ", popt)
    print("chi2/dof = %e /%d" % (chi2, c2exp_fit.ndof()))

    p_r     = np.empty((n_data, c2exp_mf.plen()), np.float64)
    chi2_r  = np.empty((n_data,), np.float64)
    for i_data, c2 in enumerate(c2_ens[hack_data_slice]) :
        c2exp_fv.set_all(xv=tr, ya=c2, ys=c2_e, ycov=c2_cov)    # FIXME vary c2_e, c2_cov with sample
        opt_kw1         = dict(opt_kw)
        opt_kw1['disp'] = 0
        p_i, pcov_i, chi2_i = c2exp_fit.fit_savestate(p0, 
                    method=method, opt_kw=opt_kw1) # FIXME remove disp=2
        p_r[i_data]     = p_i
        chi2_r[i_data]  = chi2_i
        print("### i_data=%d  chi2=%f (ndof=%d)" % (i_data, chi2_i, c2exp_fit.ndof()))
    p_a, p_e, p_cov = lhpd.calc_avg_err_ncov(p_r, rsplan)
    if not None is h5g_out :
        lhpd.h5_io.h5_clean_group(h5g_out)
        h5g_out['xr']           = tr
        h5g_out['fit_data']     = c2_ens
        h5g_out['fit_data_a']   = c2_a
        h5g_out['fit_data_e']   = c2_e
        h5g_out['popt']         = popt
        h5g_out['p']            = p_r
        h5g_out['p_a']          = p_a
        h5g_out['p_e']          = p_e
        h5g_out['p_cov']        = p_cov
        h5g_out['chi2']         = chi2_r

        h5g_out.attrs['rsplan']     = rsplan
        h5g_out.attrs['ndof']       = c2exp_fit.ndof()
        h5g_out.attrs['pname']  = c2exp_fv.pname

def test_nexp_c2pt_priors_ens(tr, c2_ens, rsplan,
        nexp_fit=2,
        method='nlm',
        h5g_out=None,
        hack_data_slice=None,
        hack_rnd_seed=None,
        opt_kw={} ) :

    if not None is hack_rnd_seed : np.random.seed(seed=hack_rnd_seed)
    if None is hack_data_slice : hack_data_slice = np.r_[:len(c2_ens)]

    n_data = len(c2_ens)
    c2_a, c2_e, c2_cov = lhpd.calc_avg_err_ncov(c2_ens, rsplan)

    c2exp_mf    = lhpd.fitter.legacy2modelfunc(Nexp_ladder_c2pt(nexp_fit))
    c2exp_fv    = datafit_join([],   # pname : no renaming/reordering
                    datafit(c2exp_mf, tr, c2_a, c2_e, c2_cov))
    c2exp_fv.add(prior_simple(  # this way, parameters for priors must already be in 
                    ("c2pt_c0", "lognormal", c[0], math.log(2)),
                    ("c2pt_e0", "lognormal", e[0], math.log(2)),
                    ("c2pt_c1", "lognormal", c[1], math.log(2)),
                    ("c2pt_de1_0", "lognormal", e[1]-e[0], math.log(2)) ),
                 newfitp=False)
    c2exp_fit   = fitter2(c2exp_fv)

    p0  = np.asarray([1., 1.] * nexp_fit)
    res = c2exp_fit.fit(p0, method=method, opt_kw=opt_kw)
    print(res)
    popt, pcov, chi2 = res
    chi2_2 = c2exp_fit.chi2(popt)
    print("popt = ", popt)
    print("chi2/dof = %e /%d" % (chi2, c2exp_fit.ndof()))
    print("chi2[model]=%e  chi2[prior]=%e" % (
            c2exp_fv.m[0].chi2(popt), c2exp_fv.m[1].chi2(popt)))
    
    return None

    p_r     = np.empty((n_data, c2exp_mf.plen()), np.float64)
    chi2_r  = np.empty((n_data,), np.float64)
    if None is hack_data_slice : hack_data_slice = np.r_[:n_data]
    for i_data, c2 in enumerate(c2_ens[hack_data_slice]) :
        c2exp_fv.set_all(xv=tr, ya=c2, ys=c2_e, ycov=c2_cov)    # FIXME vary c2_e, c2_cov with sample
        opt_kw1         = dict(opt_kw)
        opt_kw1['disp'] = 0
        p_i, pcov_i, chi2_i = c2exp_fit.fit_savestate(p0, 
                    method=method, opt_kw=opt_kw1) # FIXME remove disp=2
        p_r[i_data]     = p_i
        chi2_r[i_data]  = chi2_i
        print("### i_data=%d  chi2=%f (ndof=%d)" % (i_data, chi2_i, c2exp_fit.ndof()))
    p_a, p_e, p_cov = lhpd.calc_avg_err_ncov(p_r, rsplan)
    if not None is h5g_out :
        lhpd.h5_io.h5_clean_group(h5g_out)
        h5g_out['xr']           = tr
        h5g_out['fit_data']     = c2_ens
        h5g_out['fit_data_a']   = c2_a
        h5g_out['fit_data_e']   = c2_e
        h5g_out['popt']         = popt
        h5g_out['p']            = p_r
        h5g_out['p_a']          = p_a
        h5g_out['p_e']          = p_e
        h5g_out['p_cov']        = p_cov
        h5g_out['chi2']         = chi2_r

        h5g_out.attrs['rsplan']     = rsplan
        h5g_out.attrs['ndof']       = c2exp_fit.ndof()
        h5g_out.attrs['pname']  = c2exp_fv.pname

if __name__ == '__main__':
    tr=np.r_[1:20]
    c=np.r_[ 1.0, 2.0, 4.0, 8.0 ]
    e=np.r_[ 0.5, 1.0, 1.5, 2.0 ]
    cerr, merr, mc = 2e-2, 0.3, 0.1
    nexp_fit = 2
    h5f = h5py.File('zzz.h5', 'a')
    method  = 'nlm'
    n_data=300
    hack_rnd_seed   = 19842042
    hack_data_slice = np.s_[:]
    
    rsplan  = ('bs',)
    c2_ens  = gen_c2pt_ens(n_data, tr, c, e, cerr, merr, mc)
    c2_a    = gen_c2pt(c, e, tr)
    c2_e    = gen_c2pt_err(cerr, merr, tr)
    c2_cov  = gen_c2pt_cov(mc, tr)
    print("c2_a", c2_a)
    print("c2_e", c2_e)
    print("cov eigv=", np.linalg.eigvalsh(c2_cov))

    if True :
    #if False :
        test_nexp_c2pt_avg(tr, c2_a, c2_e, c2_cov,
                nexp_fit=nexp_fit,
                method=method,
                h5g_out=h5f.require_group('/testfit_avg'),
                opt_kw=dict(disp=2))
    #if True :
    if False :
        test_nexp_c2pt_ens(tr, c2_ens, rsplan,
                nexp_fit=nexp_fit,
                method=method,
                h5g_out=h5f.require_group('/testfit_ens'),
                #hack_rnd_seed=hack_rnd_seed,
                #hack_data_slice=hack_data_slice,
                opt_kw=dict(disp=2))
    if True :
    #if False :
        test_nexp_c2pt_priors_ens(tr, c2_ens, rsplan,
                nexp_fit=nexp_fit,
                method=method,
                h5g_out=h5f.require_group('/testfit_ens'),
                #hack_rnd_seed=hack_rnd_seed,
                #hack_data_slice=hack_data_slice,
                opt_kw=dict(disp=2))

