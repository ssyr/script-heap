# interpreter?
import math
import sys
import numpy as np
import lhpd
import getopt
import tsv
from ..fitter import Model_function_Base, Fitter

class fit_poly(Model_function_Base) :
    def __init__(self, npoly, pname_prefix='') :
        self.npoly = np.array(npoly)
        assert(1 == len(self.npoly.shape))
        self.pname = [ "%sc%d" % (pname_prefix, n) for n in npoly ]
    def par_names(self): return list(self.par_names) # copy
    def fv(self, xv, p) :    # [i_x]
        return (xv[..., None] ** self.npoly * p).sum(-1)
    def dfv(self, xv, p) :   # [i_x, i_p]
        return xv[..., None] ** self.npoly
    def __str__(self) : return "Poly[%s]" % ','.join(self.npoly)
    def __repr__(self): return self.__str__()
    def desc(self) : return self.__str__()


class fit_linear(Model_function_Base):
    """ linear fit A+B*x """
    def __init__(self): pass
    def f(self, q2, p):
        #print("f:\t", p)
        return p[0] + p[1] * q2
    def df(self, q2, p):
        #print("df:\t", p)
        x = np.empty(shape=(2,), dtype=np.float64)
        x[0] = 1 
        x[1] = q2
        return x
    def ddf(self, q2, p):
        #print("ddf:\t", p)
        x = np.empty(shape=(2,2), dtype=np.float64)
        x[0,0] = 0
        x[1,0] = x[0,1] = 0
        x[1,1] = 0
        return x
    def default_start_val(self): return np.array([1., 1.])
    def par_names(self): return [ 'A', 'B' ]
    def desc(self): return "linear A + B*x"

class fit_monopole(Model_function_Base):
    """ monopole fit A/(1+B*x) """
    def __init__(self): pass
    def f(self, q2, p):
        #print("f:\t", p)
        return p[0] / (1 + p[1] * q2)
    def df(self, q2, p):
        #print("df:\t", p)
        x = np.empty(shape=(2,), dtype=np.float64)
        x[0] = 1 / (1 + p[1] * q2)
        x[1] = -p[0] * q2 / (1 + p[1] * q2)**2
        return x
    def ddf(self, q2, p):
        #print("ddf:\t", p)
        x = np.empty(shape=(2,2), dtype=np.float64)
        x[0,0] = 0
        x[1,0] = x[0,1] = -q2 / (1 + p[1] * q2)**2
        x[1,1] = 2 * q2 * q2 * p[0] / (1 + p[1] * q2)**3
        return x
    def default_start_val(self): return np.array([1., 1.])
    def par_names(self): return [ 'A', 'B' ]
    def desc(self): return "monopole A/(1+B*x)"

class fit_monopole_plus_const(Model_function_Base):
    """ monopole fit A/(1+B*x) + C """
    def __init__(self): pass
    def f(self, q2, p):
        #print("f:\t", p)
        return p[0] / (1 + p[1] * q2) + p[2]
    def df(self, q2, p):
        #print("df:\t", p)
        x = np.empty(shape=(3,), dtype=np.float64)
        x[0] = 1 / (1 + p[1] * q2)
        x[1] = -p[0] * q2 / (1 + p[1] * q2)**2
        x[2] = 1
        return x
    def ddf(self, q2, p):
        #print("ddf:\t", p)
        x = np.empty(shape=(3,3), dtype=np.float64)
        x[0,0] = 0
        x[1,0] = x[0,1] = -q2 / (1 + p[1] * q2)**2
        x[1,1] = 2 * q2 * q2 * p[0] / (1 + p[1] * q2)**3
        x[:,3] = x[3,:] = 0
        return x
    def default_start_val(self): return np.array([1., 1., 0.])
    def par_names(self): return [ 'A', 'B', 'C' ]
    def desc(self): return "monopole+const A/(1+B*x) + C"

class fit_dipole(Model_function_Base):
    """ dipole fit A/(1+B*x)**2 """
    def __init__(self): pass
    def f(self, q2, p):
        #print("f:\t", p)
        return p[0] / (1 + p[1] * q2)**2
    def df(self, q2, p):
        #print("df:\t", p)
        x = np.empty(shape=(2,), dtype=np.float64)
        x[0] = 1 / (1 + p[1] * q2)**2
        x[1] = -2 * p[0] * q2 / (1 + p[1] * q2)**3
        return x
    def ddf(self, q2, p):
        #print("ddf:\t", p)
        x = np.empty(shape=(2,2), dtype=np.float64)
        x[0,0] = 0
        x[1,0] = x[0,1] = -2 * q2 / (1 + p[1] * q2)**3
        x[1,1] = 6 * q2 * q2 * p[0] / (1 + p[1] * q2)**4
        return x
    def default_start_val(self): return np.array([1., 1.])
    def par_names(self): return [ 'A', 'B' ]
    def desc(self): return "dipole A/(1+B*x)**2"

class fit_tripole(Model_function_Base):
    """ tripole fit A/(1+B*x)**3 """
    def __init__(self): pass
    def f(self, q2, p):
        #print("f:\t", p)
        return p[0] / (1 + p[1] * q2)**3
    def df(self, q2, p):
        #print("df:\t", p)
        x = np.empty(shape=(2,), dtype=np.float64)
        x[0] = 1 / (1 + p[1] * q2)**3
        x[1] = -3 * p[0] * q2 / (1 + p[1] * q2)**4
        return x
    def ddf(self, q2, p):
        #print("ddf:\t", p)
        x = np.empty(shape=(2,2), dtype=np.float64)
        x[0,0] = 0
        x[1,0] = x[0,1] = -3 * q2 / (1 + p[1] * q2)**4
        x[1,1] = 12 * q2 * q2 * p[0] / (1 + p[1] * q2)**5
        return x
    def default_start_val(self): return np.array([1., 1.])
    def par_names(self): return [ 'A', 'B' ]

def fit_Xpole(q2list, data_rs, rsplan, fit_func, cov_diag=False, xslice=np.s_[:]):
    """

        q2list      [i_q2]
        data_rs     [i_data][i_q2] (resampled) data
        rsplan
        xslice      select q2 values

        return:     sol[i_data][i_var], chi2[i_data]
                    where i_var = {F(0), F'(0)} uniform for all models
    """
    q2list  = np.asarray(q2list)[xslice]
    data_rs = np.asarray(data_rs)[:, xslice]
    n_data  = data_rs.shape[0]
    n_q2    = len(q2list)
    assert (data_rs.shape[1] == n_q2)

    # filter out zero-variation points (FIXME switch on/off in options?)
    data_err= lhpd.calc_err(data_rs, rsplan=rsplan)
    nzv_slice   = filter(lambda i : data_err[i] > 0., range(n_q2))
    q2list  = q2list[nzv_slice]
    data_rs = data_rs[:, nzv_slice]
    n_q2    = len(q2list)
    
    data_avg, data_err, data_ncov = lhpd.calc_avg_err_ncov(data_rs, rsplan=rsplan)
    if cov_diag: data_ncov = np.identity(n_q2, dtype=float)

    fit_func_class = {
        'linear' :      fit_linear,
        'monopole' :    fit_monopole,
        'monopole+c' :  fit_monopole_plus_const,
        'dipole' :      fit_dipole,
        'tripole' :     fit_tripole
        }[fit_func]()

    fitter = Fitter(fit_func_class,
            q2list, data_avg, data_err, data_ncov, None)
    fitter.fit(np.array(fit_func_class.default_start_val()))
    sol = []
    chi2= []
    for i in range(n_data):
        a, b = fitter.fit_savestate(data_rs[i])
        sol.append(a)
        chi2.append(b)
    
    sol     = np.asarray(sol)
    chi2    = np.asarray(chi2)
    if 'linear' == fit_func:            pass
    elif 'monopole' == fit_func:     sol[:,1] *= -sol[:,0]
    elif 'monopole+c' == fit_func:   
        sol = np.r_['1', sol[:,0:1], -sol[:,0:1]*sol[:,1:2]]
    elif 'dipole' == fit_func:       sol[:,1] *= -2 * sol[:,0]
    elif 'tripole' == fit_func:      sol[:,1] *= -3 * sol[:,0]
    else: raise ValueError('unknown fit_func="%s"' % fit_func)

    # TODO add drawing to spec.axes
    return sol, chi2, fitter.get_ndf()
    
def print_fit_Xpole(q2list, data_rs, rsplan, fit_func, cov_diag=False, xslice=np.s_[:], fo=sys.stdout):
    """ print from 1,2,3-pole fits 
            forward value F(0)
            F(0) * mean.sq.radius
            mean.sq.radius
    """

    sol, chi2, ndf      = fit_Xpole(q2list, data_rs, rsplan, fit_func, 
                                    cov_diag=cov_diag, xslice=xslice)
    sol_avg, sol_err    = lhpd.calc_avg_err(sol, rsplan=rsplan)
    frd_avg, frd_err    = lhpd.calc_avg_err(sol[:,1] / sol[:,0], rsplan=rsplan)
    chi2_avg, chi2_err  = lhpd.calc_avg_err(chi2, rsplan=rsplan)

    fo.write('# stat=%d ndf=%d rsplan=%s\n' % (len(sol), ndf, str(rsplan)))

    fo.write("%e\t%e\t# F(0)      %s\n" % \
             (sol_avg[0], sol_err[0], lhpd.spprint_ve(sol_avg[0], sol_err[0])))
    fo.write("%e\t%e\t# F(0)*<r2> %s\n" % \
            (-6. * sol_avg[1], 6. * sol_err[1], 
             lhpd.spprint_ve(-6. * sol_avg[1], 6. * sol_err[1])))
    fo.write("%e\t%e\t# <r2>      %s\n" % \
            (-6. * frd_avg, 6. * frd_err, 
             lhpd.spprint_ve(-6. * frd_avg, 6. * frd_err)))
    fo.write("%e\t%e\t# chi2      %s\n" % \
            (chi2_avg, chi2_err, lhpd.spprint_ve(chi2_avg, chi2_err)))
    # TODO add Qvalue


def print_usage(name):
    import sys
    sys.stderr.write('Usage:\n%s' % name)
    sys.stderr.write(
"""
                --fit-func=[linear|monopole|dipole|tripole]
                --with-chi2=[plain|reduced]
                [--xslice=<slice>]
                [--cov-diag]
                <q2_list>  <ff-data>
""")

# only dipole is used currently
if (__name__ == '__main__'):
    from numpy import r_, s_, ix_
    if len(sys.argv) < 3: 
        print_usage(sys.argv[0])
        exit(1)

    opt_list, rest_arg = getopt.getopt(sys.argv[1:], '', 
                        [ 'fit-func=',
                          'cov-diag',
                          'xslice=',
                          'with-chi2=' ])
    if len(rest_arg) != 2:
        print_usage(sys.argv[0])
        exit(1)

    param = { 'fit_func'        : 'dipole', 
              'cov_diag'        : False,
              'xslice'          : ':',
              'with_chi2'       : '' }
    for opt, val in opt_list:
        if   opt == '--fit-func':   param['fit_func'] = val
        elif opt == '--cov-diag':   param['cov_diag'] = True
        elif opt == '--with-chi2':  param['with_chi2'] = val
        elif opt == '--xslice':     param['xslice'] = val
        else: raise RuntimeError('unknown option %s' % opt)
    
    q2_list = np.array(tsv.read_float(open(rest_arg[0], 'r'))[0][0], 
                          dtype=float)[:,0]
    q2_list = eval("q2_list[%s]" % param['xslice'])

    assert len(q2_list.shape) == 1
    n_q2 = q2_list.shape[0]
    assert (2 < n_q2)
    
    ff_data = None
    if rest_arg[1] == '-': ff_data = np.array(tsv.read_float(sys.stdin), dtype=float)
    else: ff_data = np.array(tsv.read_float(open(rest_arg[1], 'r')), dtype=float)
    ff_data = eval("ff_data[:,:,%s,:]" % param['xslice'])

    n_data = ff_data.shape[0]
    n_var = ff_data.shape[3]
    assert (ff_data.shape[1] == 1 and ff_data.shape[2] == n_q2)

    #fit_res = np.empty(shape=(n_data, 2, n_var), dtype=np.float64)
    #fit_chi2= np.empty(shape=(n_data, n_var), dtype=np.float64)
    fit_res = []
    fit_chi2 = []
    for i_var in range(n_var):
        fit_res.append([])
        fit_chi2.append([])

        ff_var = ff_data[:,0,:,i_var].var(axis=0)
        i_q2_list = []
        for i,v in enumerate(ff_var):
            if (v > 0.): i_q2_list.append(i)
        #print("selected i_q2:\t", i_q2_list)
        ff          = ff_data[:,0,i_q2_list,i_var]
        q2r         = q2_list[i_q2_list]
        ff_avg      = ff.mean(axis=0)
        ff_cov      = np.cov(ff, rowvar=0)
        ff_sigma    = np.sqrt(ff_cov.diagonal())
        ff_cov     /= np.outer(ff_sigma, ff_sigma)
        if param['cov_diag']: ff_cov = np.identity(len(ff_sigma), np.float)
        ff_sigma   *= math.sqrt(n_data - 1)
        #print(q2r.shape, q2r)
        #print(ff_avg)
        #print(ff_sigma)
        #print(ff_cov)
        fit_func = None
        fit_func = {
            'linear' :      fit_linear,
            'monopole' :    fit_monopole,
            'monopole+c' :  fit_monopole_plus_const,
            'dipole' :      fit_dipole,
            'tripole' :     fit_tripole
            }[param['fit_func']]()
        fitter = Fitter(fit_func,
                q2r, ff_avg, ff_sigma, ff_cov, None)

        fitter.fit(np.array(fit_func.default_start_val()))
        for i in range(n_data):
            a, b = fitter.fit_savestate(ff_data[i,0,i_q2_list,i_var])
            fit_res[i_var].append(a)
            fit_chi2[i_var].append(b)
    fit_res = np.array(fit_res, dtype=np.float64).transpose((1, 2, 0))
    fit_chi2= np.array(fit_chi2, dtype=np.float64).transpose((1, 0))

    if param['with_chi2'] == '':
        tsv.write_format(sys.stdout, fit_res[:,None], format="%.16e")
    else:
        out = np.empty(shape=(n_data, fit_res.shape[1]+1, n_var), dtype=np.float64)
        out[:,0:-1]  = fit_res
        if param['with_chi2'] == 'plain':       out[:,-1] = fit_chi2
        elif param['with_chi2'] == 'reduced':   out[:,-1] = fit_chi2 / (n_q2 - 2)
        else: raise RuntimeError('unknown chi2 mode %s' % param['with_chi2'])
        tsv.write_format(sys.stdout, out[:, None], format="%.16e")

