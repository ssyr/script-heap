import numpy as np
import math
from math import pi

from ..fitter import Model_function_Base, modelfunc_Base

class Nexp_ladder_c2pt(Model_function_Base):
    """ Two-point correlator function for spatial-momentum projected states
        (only t-dependent) including excited states contamination. 
        "Ladder" stands for explicit ordering of energy states:
            param = c2pt_[c0,e0, c1,de1_0, c2, de2_1...]
            f(t) = X_0(c_0 + X_1(c_1 + ... X_N c_N)...),
            where X_I = e^{-de{I}_{I-1}*t}, 
            or
            f(t) = c_0 e^{-E_0 t} + c_1 e^{-E_1 t} + ... + c_N e^{-E_N t},
            where E_0 = e0,
                  E_1 = e0 + de1_0,
                  ...
                  E_N = e0 + ... + de{N}_{N-1}
        When one puts range limits on energy differences de{I}_{I-1} > 0, fit should give
        stable ordering of the distinct energy levels.
    """
    def __init__(self, n=1, pname_prefix=''):
        assert 0 < n
        self.n = n
        self.par_names_list = ['%sc0' %pname_prefix, '%se0' %pname_prefix]
        for i in range(1, self.n): 
            self.par_names_list.append('%sc%d' %(pname_prefix,i))
            self.par_names_list.append('%sde%d' %(pname_prefix,i))
    def plen(self) : return 2*self.n
    def f(self, t, param_):
        param = np.asarray(self.select_param(param_), dtype=np.float64)
        assert(len(param)  == 2 * self.n)
        Np1 = self.n
        func = 0
        for i in reversed(range(Np1)):
            # here f = \sum(c_i exp(-t*\sum_{k=0..i} de_k)
            func = math.exp(-t * param[2*i+1]) * (param[2*i] + func)
        return func
    def df(self, t, param_) :
        param = self.select_param(param_)
        assert(len(param)  == 2 * self.n)
        Np1     = self.n
        Carr    = np.array([ param[2*i]   for i in range(Np1) ])
        dEarr   = np.array([ param[2*i+1] for i in range(Np1) ])
        Xarr    = np.array([ math.exp(-t * dE) for dE in dEarr ])
        func    = 0.
        dfunc   = np.zeros(shape=(2*Np1,), dtype=np.float64)
        for i in reversed(range(Np1)):
            dfunc[2*i : 2*(i+1)] = [ Xarr[i], Carr[i] + func ]
            dfunc[2*(i+1) : 2*Np1] *= Xarr[i]
            func = Xarr[i] * (Carr[i] + func)
        for i in range(Np1):
            dfunc[2*i+1] *= Xarr[i] * (-t)
        return dfunc
    def ddf(self, t, param_):
        param = self.select_param(param_)
        assert(len(param) == 2 * self.n)
        Np1 = self.n
        Carr    = np.array([ param[2*i]   for i in range(Np1) ])
        dEarr   = np.array([ param[2*i+1] for i in range(Np1) ])
        Xarr    = np.array([ math.exp(-t * dE) for dE in dEarr ])
        func    = 0.
        dfunc   = np.zeros(shape=(2*Np1,), dtype=np.float64)
        ddfunc  = np.zeros(shape=(2*Np1, 2*Np1), dtype=np.float64)
        for i in reversed(range(Np1)):
            ddfunc[2*i:2*(i+1), 2*i:2*(i+1)] = [[0., 1.], [1., 0.]]
            for j in range(i+1,Np1):
                ddfunc[2*i,   2*j : 2*(j+1)] = [0., 0.]                 # row d2/dc_i ...
                ddfunc[2*i+1, 2*j : 2*(j+1)] = dfunc[2*j : 2*(j+1)]     # row d2/dX_i ...
                ddfunc[2*j : 2*(j+1), 2*i  ] = [0., 0.]                 # col d2/dc_i ...
                ddfunc[2*j : 2*(j+1), 2*i+1] = dfunc[2*j : 2*(j+1)]     # col d2/dX_i ...
            ddfunc[2*(i+1) : , 2*(i+1) : ] *= Xarr[i]                   # update previous ddfunc
            dfunc[2*i : 2*(i+1)] = [ Xarr[i], Carr[i] + func ]          # new dfunc
            dfunc[2*(i+1) : ] *= Xarr[i]                                # update previous dfunc
            func = Xarr[i] * (Carr[i] + func)
        fact = np.array([ [ 1., Xarr[i] * (-t) ] for i in range(Np1) ],
                     dtype=np.float64, shape=(2*Np1,))
        ddfunc = ddfunc * fact.reshape((1,2*Np1)) * fact.reshape((2*Np1,1))
        for i in range(Np1):
            ddfunc[2*i+1, 2*i+1] += t * t * Xarr[i] * dfunc[2*i+1]
        return ddfunc
    def __str__(self) :
        pn = self.par_names_list
        return "EXP{-t*%s}*(%s+%s)" % (pn[1], pn[0],
                '+'.join([ "%s*EXP{-t*%s}" % (pn[2*i], pn[1+2*i]) for i in range(1, self.n) ]))
    def __repr__(self): return str(self)
        
# TODO finish rewriting in efficient NumPy
class mfunc_Nexp_ladder_c2pt(modelfunc_Base) :
    def __init__(self, n=1, pname_prefix=''):
        assert 0 < n
        self.n = n
        self.pname = ['%sc0' %pname_prefix, '%se0' %pname_prefix]
        for i in range(1, self.n): 
            self.pname.extend([ '%sc%d' % (pname_prefix, i), 
                                '%sde%d' % (pname_prefix, i) ])
    def plen(self) : return 2*self.n
    def f(self, t, p) :
        assert(self.plen() == len(p))
        return (np.exp(-t * p[1::2].cumsum(-1)) * p[::2]).sum(-1)
    def fv(self, tr, p):
        assert(self.plen() == len(p))
        return (np.exp(-tr[:,None] * p[1::2].cumsum(-1)) * p[::2]).sum(-1)
    #def fpart(self, t, p, nexp=None) :
        #""" calculate contributions to """
        #if None is nexp : nexp = self.nexp
        #return (np.exp(-t * p[1:1+2*nexp:2].cumsum(-1)) * p[::2]).sum(-1)

    def df(self, t, p) :
        assert(len(p)  == 2 * self.n)
        dfunc   = np.zeros(shape=(self.plen()), dtype=np.float64)
        pXarr   = np.exp(-t * p[1::2].cumsum(-1))
        CpXarr  = p[::2] * pXarr[::1]
        CpXarr_rcs  = (CpXarr[::-1]).cumsum(-1)
        dfunc[::2]  = pXarr
        dfunc[1::2] = -t * CpXarr_rcs[::-1]
        return dfunc
    def dfv(self, tr, p) :
        assert(len(p)  == 2 * self.n)
        tr      = tr[:,None]
        dfv     = np.zeros(shape=(len(tr), self.plen()), dtype=np.float64)
        pXarr   = np.exp(-tr * p[1::2].cumsum(-1))
        CpXarr  = p[::2] * pXarr[:, ::1]
        CpXarr_rcs  = (CpXarr[:, ::-1]).cumsum(-1) # rev.cum.sum
        dfv[:, ::2] = pXarr
        #dfv[:, 1::2]= -tr * CpXarr_rcs[:, ::-1]
        dfv[:, -1::-2]= -tr * CpXarr_rcs
        return dfv

    def __str__(self) :
        pn = self.pname
        return "EXP{-t*%s}*(%s+%s)" % (pn[1], pn[0],
                '+'.join([ "%s*EXP{-t*%s}" % (pn[2*i], pn[1+2*i]) for i in range(1, self.n) ]))
    def __repr__(self): return str(self)
    
class Nexp_osc_ladder_c2pt(Model_function_Base):
    """ Two-point correlator function for spatial-momentum projected states
        (only t-dependent) including excited states contamination AND 
        one oscillating state. 
            param = [c0,e0,c1,de1_0,....cN,de{N}_{N-1}, cosc,deosc_0]
            f(t)  = Nexp_ladder_c2pt(t,[c0,e0,c1,de1_0....cN,de{N}_{N-1}]) 
                    + (-1)^t * cosc * exp(-t*(e0+deosc_0))
        TODO: change Nexp_ladder_c2pt field -> base class
    """
    def __init__(self, n=1, pname_prefix=''):
        assert 0 < n
        self.nexp = Nexp_ladder_c2pt(n)
        self.n = n
        self.par_names_list = [ '%sc0' % pname_prefix, 
                                '%se0' % pname_prefix ]
        for i in range(1, self.n) :
            self.par_names_list.append('%sc%d' % (pname_prefix, i))
            self.par_names_list.append('%sde%d' % (pname_prefix, i))
        self.par_names_list.extend([                        
                                '%scosc' % pname_prefix, 
                                '%sdeosc0' % pname_prefix])
    def f(self, t, param_):
        param = self.select_param(param_)
        assert(len(param) == 2 * (self.n + 1))
        return self.nexp.f(t, param[0:-2]) + \
                param[-2] * math.cos(pi * t) * math.exp(-t * (param[1] + param[-1]))
    def df(self, t, param_):
        param = self.select_param(param_)
        assert(len(param) == 2 * (self.n + 1))
        df = np.zeros(dtype=np.float64, shape=(len(param),))
        df[0:-2] = self.nexp.df(t, param[0:-2])
        c_osc   = param[-2]
        exp_osc = math.cos(pi * t) * math.exp(-t * (param[1] + param[-1]))
        df[-2] = exp_osc
        df[-1] = c_osc * exp_osc * (-t)
        df[1] += df[-1]
        return df
    def ddf(self, t, param_):
        param = self.select_param(param_)
        assert(len(param) == 2 * (self.n + 1))
        ddf = np.zeros(dtype=np.float64, shape=(len(param),len(param)))
        ddf[0:-2, 0:-2] = self.nexp.ddf(t, param[0:-2])
        exp_osc = math.cos(pi * t) * math.exp(-t * (param[1] + param[-1]))
        f_dc_de = (-t) * exp_osc
        # [-2] is cneg, [-1] in eneg, [1] is e0
        ddf[-2,-1] = ddf[-1,-2] = ddf[1,-2] = ddf[-2,1] = f_dc_de
        f_de2   = t * t * exp_osc * param[-2]
        ddf[-1,-1] = ddf[1,-1] = ddf[-1,1] = f_de2
        ddf[1,1]  += f_de2
        return ddf
    def __str__(self) :
        pn = self.par_names_list
        return "EXP{-t*%s}*(%s+%s+%s*OSC{-t*%s}}" % (pn[1], pn[0],
                '+'.join([ "%s*EXP{-t*%s}" % (pn[2*i], pn[1+2*i]) for i in range(1, self.n) ]),
                pn[-2], pn[-1])
    def __repr__(self): return str(self)

class Twoexp_osc_ladder_c3pt_symm(Model_function_Base):
    """ Three-pt amputated correlator function ("plateau ratio") fully projected on
        symmetric momentum states (only t-dependent) including contamination 
        from excited states AND one oscillating state. The excited- to excited-state 
        contamination is not included since it cannot be determined from fit with fixed
        source-sink separation. Energy levels are fixed and should be taken from 
        two-point correlator fit.
            param   = c3pt_[A00, A10, Aosc0]
            c3pt(t) = A00 + A10 * (exp(-t*de1) + << t --> T-t >>) 
                      + Aosc0 *(cos(pi*t) * exp(-t*deosc0) + << t --> T-t >>)
    """
    def __init__(self, src_snk_dt, de):
        """ c2pt        two-point model
            src_snk_dt  separation
            de          fitted delta-energies E1-E0, Eneg - E0
        """
        self.src_snk_dt = src_snk_dt
        assert 2 == len(de)
        self.de     = de
        self.par_names_list = ['c3pt_A00', 'c3pt_A10', 'c3pt_Aosc0']
    def f(self, t, param_):
        param = self.select_param(param_)
        assert len(param) == 3
        np = len(param)
        T = self.src_snk_dt
        res =  + param[0] \
               + param[1] * (math.exp(-t*self.de[0]) + math.exp(-(T-t)*self.de[0])) \
               + param[2] * (math.cos(pi*t)*math.exp(-t*(self.de[1])) \
                             + math.cos(pi*(T-t))*math.exp(-(T-t)*self.de[1]))
        return res
    def df(self, t, param_):
        param = self.select_param(param_)
        assert len(param) == 3
        np = 3
        T = self.src_snk_dt
        res = np.zeros(dtype=np.float64, shape=(np,))
        res[0] = 1.
        res[1] = math.exp(-t*self.de[0]) + math.exp(-(T-t)*self.de[0])
        res[2] = math.cos(pi*t)*math.exp(-t*(self.de[1])) \
                 + math.cos(pi*(T-t))*math.exp(-(T-t)*self.de[1])
        return res
    def ddf(self, t, param_):
        param = self.select_param(param_)
        assert len(param) == 3
        np = 3
        return np.zeros(dtype=np.float64, shape=(np,np))
    

class Meff(Model_function_Base):
    """ Effective mass model function for meff(t) = log(|c2pt(t)/c2pt(t+1)|). 
        Since c0 is excluded from meff at t->inf, we fix it to 1 and cN correspond
        to cN/c0 from 2pt correlator fit
            param = [e0, c1, de1_0, ... cN, de{N}_{N-1})
            f(t)  = log(abs(c2pt(t) / c2pt(t+1)))
            in effect, c0 is excluded from this composition
        FIXME: each c2pt(t) is evaluated twice; can be changed if there are 
               [f,df,ddf]_range functions
    """
    def __init__(self, c2pt):
        c2pt_parm   = c2pt.par_names()
        assert (1 < len(c2pt_parm) and 
                c2pt_parm[0] == 'c0')        # sanity check 
        self.c2pt = c2pt
        self.par_names_list = c2pt_parm[1:]
    def f(self, t, param_):
        param = self.select_param(param_)
        x = np.zeros(shape=(1+len(param),), dtype=np.float64)
        x[0]    = 1.
        x[1:]   = param[:]
        f0  = self.c2pt.f(t,x)
        f1  = self.c2pt.f(t+1,x)
        return math.log(abs(f0 / f1))
    def f_range(self, trange, param_):
        param = self.select_param(param_)
        x   = np.zeros(shape=(1+len(param),), dtype=np.float64)
        x[0]    = 1.
        x[1:]   = param[:]
        tlast = None
        f1  = None
        res = np.zeros(shape=(len(trange),), dtype=np.float64)
        for i,t in enumerate(trange):
            if (t-1 == tlast):    f0, f1  = f1, self.c2pt.f(t+1, x)
            else:   f0, f1  = self.c2pt.f(t, x), self.c2pt.f(t+1, x)
            res[i] = math.log(abs(f0 / f1))
            tlast = t
        return res
        
    def df(self, t, param_):
        param = self.select_param(param_)
        x = np.zeros(shape=(1+len(param),), dtype=np.float64)
        x[0]    = 1.
        x[1:]   = param[:]
        f0  = self.c2pt.f(t, x)
        df0 = self.c2pt.df(t, x)[1:]
        f1  = self.c2pt.f(t+1, x)
        df1 = self.c2pt.df(t+1, x)[1:]
        return df0 / f0 - df1 / f1
    def df_range(self, trange, param_):
        param = self.select_param(param_)
        x   = np.zeros(shape=(1+len(param),), dtype=np.float64)
        x[0]    = 1.
        x[1:]   = param[:]
        df1, f1 = None, None
        res = np.zeros(shape=(len(trange), len(param)), dtype=np.float64)
        tlast   = None
        f1, df1 = None, None
        for i,t in enumerate(trange):
            if (t-1 == tlast):
                f0, f1  = f1, self.c2pt.f(t+1, x)
                df0, df1= df1, self.c2pt.df(t+1, x)[1:]
            else:
                f0, f1  = self.c2pt.f(t, x), self.c2pt.f(t+1, x)
                df0, df1= self.c2pt.df(t, x)[1:], self.c2pt.df(t+1, x)[1:]
            res[i] = df0 / f0 - df1 / f1
            tlast = t
        return res

    def ddf(self, t, param_):
        param = self.select_param(param_)
        x = np.zeros(shape=(1+len(param),), dtype=np.float64)
        x[0]    = 1.
        x[1:]   = param[:]
        f0  = self.c2pt.f(t, x)
        df0 = self.c2pt.df(t, x)[1:]
        ddf0= self.c2pt.ddf(t, x)[1:,1:]
        f1  = self.c2pt.f(t+1, x)
        df1 = self.c2pt.df(t+1, x)[1:]
        ddf1= self.c2pt.ddf(t+1, x)[1:,1:]
        return - outerproduct(df0,df0) / f0**2 \
               + ddf0 / f0 \
               + outerproduct(df1,df1) / f1**2 \
               - ddf1 / f1

class Delta_meff(Model_function_Base):
    """ evaluate \Delta meff(t) = log(|c2pt(t) c2pt(t+2) / c2pt(t+1)**2|)
        FIXME: (almost) each c2pt is evaluated twice
        param = (c1/c0, e1-e0, ... cN/c0, eN-e0) or equivalent
        in effect, c0 and e0 are excluded from this composition
    """
    def __init__(self, c2pt):
        c2pt_parm   = c2pt.par_names()
        assert (2 < len(c2pt_parm) and
                c2pt_parm[0] == 'c0' and 
                c2pt_parm[1] == 'e0')
        self.c2pt = c2pt
        self.par_names_list = c2pt_parm[2:]
    def f(self, t, param_):
        param = self.select_param(param_)
        x = np.zeros(shape=(2+len(param),), dtype=np.float64)
        x[0:2]    = [ 1., 0. ]
        x[2:]   = param[:]
        f0  = self.c2pt.f(t, x)
        f1  = self.c2pt.f(t+1, x)
        f2  = self.c2pt.f(t+2, x)
        return math.log(abs(f0 * f2 / f1 **2))
    def df(self, t, param_):
        param = self.select_param(param_)
        x = np.zeros(shape=(2+len(param),), dtype=np.float64)
        x[0:2]    = [ 1., 0. ]
        x[2:]   = param[:]
        f0  = self.c2pt.f(t, x)
        df0 = self.c2pt.df(t, x)[2:]
        f1  = self.c2pt.f(t+1, x)
        df1 = self.c2pt.df(t+1, x)[2:]
        f2  = self.c2pt.f(t+2, x)
        df2 = self.c2pt.df(t+2, x)[2:]
        return df0 / f0 + df2 / f2 - 2. * df1 / f1
    def ddf(self, t, param_):
        param = self.select_param(param_)
        x = np.zeros(shape=(2+len(param),), dtype=np.float64)
        x[0:2]    = [ 1., 0. ]
        x[2:]   = param[:]
        f0  = self.c2pt.f(t, x)
        df0 = self.c2pt.df(t, x)[2:]
        ddf0= self.c2pt.ddf(t, x)[2:,2:]
        f1  = self.c2pt.f(t+1, x)
        df1 = self.c2pt.df(t+1, x)[2:]
        ddf1= self.c2pt.ddf(t+1, x)[2:,2:]
        f2  = self.c2pt.f(t+2, x)
        df2 = self.c2pt.df(t+2, x)[2:]
        ddf2= self.c2pt.ddf(t+2, x)[2:,2:]
        return - outerproduct(df0, df0) / f0**2 \
               + ddf0 / f0 \
               - outerproduct(df2, df2) / f2**2 \
               + ddf2 / f2 \
               + 2. * outerproduct(df1, df1) / f1**2 \
               - 2. * ddf1 / f1
   




class Twoexp_osc_ladder_c3pt_symm_contam10(Model_function_Base):
    """ compute contamination coeff from exc states to symmetric amputated 3pt func
            param = [c1,de1_0]
            f(t)  = sqrt(c1) * (exp(-t*de1_0) + exp(-(T-t)*de1_0)

    """
    def __init__(self, src_snk_dt):
        assert 0 < src_snk_dt
        self.src_snk_dt = 0
        self.par_names_list = ['c1', 'de1']
    def f(self, t, param_):
        param = self.select_param(param_)
        return math.sqrt(param[0]) \
                * (exp(-t * param[1]) + exp(-(self.src_snk_dt - t)*param[1]))

class Twoexp_osc_ladder_c3pt_symm_contam11(Model_function_Base):
    """ compute contamination coeff from exc states to symmetric amputated 3pt func
            param = [c1,de1_0]
            f(t)  = c1 * exp(-T*de1_0)

    """
    def __init__(self, src_snk_dt):
        assert 0 < src_snk_dt
        self.src_snk_dt = 0
        self.par_names_list = ['c1', 'de1']
    def f(self, t, param_):
        param = self.select_param(param_)
        return param[0] * exp(-T * param[1])




### the rest is old stuff 

class deltameff_contamination_11:
    """ input: [ C1, E1, C2, E2 ] (same as meff)
        output: contamination term  = C1 exp(-t*(dE1)) + C2 exp(-t*(dE1+dE2)) + ...
    """
    def __init__(self, c2pt, drop=0):
        self.c2pt = c2pt
        assert 0 <= drop
        self.drop = drop
    def f(self, T, param):
        assert 2*self.drop < len(param)
        new_len = len(param) - 2*self.drop
        return self.c2pt.f(T, np.array((0., 0.) + tuple(param[:new_len])))
    def df(self, T, param):
        assert 2*self.drop < len(param)
        new_len = len(param) - 2*self.drop
        res = np.zeros(dtype=np.float64, shape=(len(param),))
        res[:new_len] = self.c2pt.df(T, np.array((0., 0.) + tuple(param[:new_len])))[2:]
        return res

class meff_contamination_11:
    """ input: [ C1, E1, C2, E2 ] (same as meff)
        output: contamination term  = C1 exp(-t*(dE1)) + C2 exp(-t*(dE1+dE2)) + ...
    """
    def __init__(self, c2pt, drop=0):
        self.dmc11 = deltameff_contamination_11(c2pt,drop)
    def f (self, T, param): return self.dmc11.f(T, param[1:])
    def df(self, T, param): return np.array((0,) + tuple(self.dmc11.df(T, param[1:])))

class deltameff_contamination_10:
    """ input: [ C1, dE1, C2, dE2 ] (same as meff)
        output: contamination term  = C1 exp(-t*(dE1)) + C2 exp(-t*(dE1+dE2)) + ...
                                      + << same with t -> T-t >>
    """
    def __init__(self, c2pt, drop=0):
        """ c2pt    2pt function model to use
            drop    number of terms to drop from the end
        """
        self.c2pt = c2pt
        assert 0 <= drop
        self.drop = drop
    def f(self, T, t, param):
        assert 2*self.drop < len(param)
        new_len = len(param) - 2*self.drop
        x       = np.array((0., 0.) + tuple(param[:new_len]))
        for i in range(1, len(x)/2): x[2*i] = sqrt(abs(x[2*i]))
        return self.c2pt.f(t, x) + self.c2pt.f(T-t, x)
    def df(self, T, t, param):
        assert 2*self.drop < len(param)
        new_len = len(param) - 2*self.drop
        x       = np.array((0., 0.) + tuple(param[:new_len]))
        y       = x 
        for i in range(1, len(y)/2): y[2*i] = sqrt(abs(y[2*i]))
        res     = self.c2pt.df(t, y) + self.c2pt.df(T-t, y)
        for i in range(1, len(y)/2): 
            res[2*i] /= 2.* y[2*i]
            if x[2*i] < 0: res[2*i] *= -1
        res2 = np.zeros(dtype=np.float64, shape=(len(param),))
        res2[:new_len] = res[2:]
        return res2
        
class meff_contamination_10:
    """ input: [ E0, C1, dE1, C2, dE2 ] (same as meff)
        output: contamination term  = C1 exp(-t*(dE1)) + C2 exp(-t*(dE1+dE2)) + ...
    """
    def __init__(self, c2pt, drop=0):
        self.dmc10= deltameff_contamination_10(c2pt, drop)
    def f (self, T, t, param): return self.dmc10.f(T, t, param[1:])
    def df(self, T, t, param): return np.array((0.,) + tuple(self.dmc10.df(T, t, param[1:])))
       

def make_param_dict(par_names, values):
    """
        create the dictionary relating parameter names to their current values
        lists must be of equal size
    """
    assert(len(par_names) == len(values))
    return dict([ (par_names[i], values[i]) for i in range(len(par_names)) ]) 


def c2fit_exp_sysmatr(t, d_en, d_en_osc=None) :
    """ return [...,i_en,i_t] = exp(-en*t) [ + osc*exp(-en_osc*t) ]
        NOTE if t is not an array, the shape is still en.shape+(1,)
    """
    en = np.asarray(d_en).cumsum(-1)
    exp_en  = np.exp(-en[...,:,None] * t)

    if None is d_en_osc : return exp_en
    else :
        en_osc = np.asarray(d_en_osc) + en[...,0:1]
        exp_en_osc  = np.exp(-en_osc[...,:,None] * t) * np.cos(np.pi * t)
        return np.r_['-2', exp_en, exp_en_osc]

def c3fit_exp_sysmatr(ttau, d_en_src, d_en_snk, d_en_osc_src=None, d_en_osc_snk=None) :
    """ make a system matrix for fitting 3pt functions 
        [..., i_src, j_snk, i_ttau] = exp(-Ei*tau - Ej*(tsep-tau))
        ttau = [(tsep, tau),...]
    """
    ttau        = np.asarray(ttau)
    exp_en_src  = c2fit_exp_sysmatr(ttau[:,1], d_en_src, d_en_osc=d_en_osc_src)
    exp_en_snk  = c2fit_exp_sysmatr(ttau[:,0] - ttau[:,1], d_en_snk, d_en_osc=d_en_osc_snk)
    return exp_en_src[...,:,None,:] * exp_en_snk[...,None,:,:]


def tsep_tau_list(tsep_list, tskip) :
    ttau = []
    for tsep in tsep_list :
        ttau.extend([ (tsep, tau) for tau in np.r_[tskip : tsep - tskip + 1]])
    return ttau
