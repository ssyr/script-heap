from config_mpl import *
import numpy
import math
import lhpd

def calc_meff_ratio(c2pt, tr, dt=1):
    assert 0 < dt < c2pt.shape[-1]
    return c2pt[...,tr] / c2pt[...,numpy.array(tr)+dt]
def calc_meff(c2pt, tr, dt=1):
    assert 0 < dt < c2pt.shape[-1]
    return numpy.log(c2pt[...,tr] / c2pt[...,numpy.array(tr)+dt]) / dt

        
        

def show_meff(ens, key_list, mom3, tr, dt=1, rsplan=('jk',1),
            xshift=0, fmt=fmt_marker_default, fmt_line=None, 
            x_margin=0.1, y_margin=0.1):
    """
        ens         dict: key->ens
        key_list    list of keys to show
        mom3        momentum
        tr          t-range
        dt          eff.mass interval
        xshift      shift each group in x direction for readability
        fmt         formats of markers
    """
    from lhpd import resample
    # XXX real part of c2pt is taken here
    tr      = numpy.array(tr)
    c2pt    = get_c2pt(ens, key_list, mom3).real
    c2pt_r  = resample(c2pt, rsplan)
    meff_r    = calc_meff(c2pt_r, tr)
    meff_avg  = lhpd.calc_avg(meff_r)
    meff_err  = lhpd.calc_err(meff_r, rsplan)

    fig = plt.figure()
    ax  = fig.add_axes([.1, .1, .8, .8])
    for i_k, k in enumerate(key_list):
        plot_data2D(ax, i_k, numpy.array(tr) + i_k * xshift,
                    meff_avg[i_k],
                    meff_err[i_k],
                    label=str(k),
                    fmt=fmt, fmt_line=fmt_line)
    ax.set_xlim(*calc_window(tr.min(), tr.max(), 
                x_margin, ax.get_xscale()))
    ax.set_ylim(*calc_window(meff_avg.min(), meff_avg.max(), 
                y_margin, ax.get_yscale()))
    ax.set_xlabel('$t$')
    ax.set_ylabel(r'$m^{eff}$')
    ax.set_xticks(tr)
    ax.legend()
    try:
        plt.show()
    except KeyboardInterrupt: pass
    return fig

def show_snr(ens, key_list, mom3, tr,
            xshift=0, fmt=fmt_marker_default, fmt_line=None,
            x_margin=0.1, y_margin=0.1):
    """
        ens         dict: key->ens
        key_list    list of keys to show
        mom3        momentum
        tr          t-range
        dt          eff.mass interval
        xshift      shift each group in x direction for readability
        fmt         formats of markers

    """
    from lhpd import resample
    # XXX real part of c2pt is taken here
    tr      = numpy.array(tr)
    c2pt    = get_c2pt(ens, key_list, mom3).real[..., tr]
    c2pt_avg, c2pt_err = lhpd.calc_avg_err(c2pt)
    snr     = numpy.fabs(c2pt_avg) / c2pt_err

    fig = plt.figure()
    ax  = fig.add_axes([.1, .1, .8, .8])
    for i_k, k in enumerate(key_list):
        plot_data2D(ax, i_k, numpy.array(tr) + i_k * xshift,
                    numpy.fabs(c2pt_avg[i_k]) / c2pt_err[i_k],
                    None,
                    label=str(k),
                    fmt=fmt, fmt_line=fmt_line)
    ax.set_yscale('log')
    ax.set_xlim(*calc_window(tr.min(), tr.max(), 
                x_margin, ax.get_xscale()))
    ax.set_ylim(*calc_window(snr.min(), snr.max(), 
                y_margin, ax.get_yscale()))
    ax.set_xlabel('$t$')
    ax.set_ylabel(r'$m^{eff}$')
    ax.set_xticks(tr)
    ax.legend()
    try:
        plt.show()
    except KeyboardInterrupt: pass
    return fig




