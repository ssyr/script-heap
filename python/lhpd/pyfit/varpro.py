import lhpd
import numpy as np

class varpro_ladder_c2pt(lhpd.fitter.Model_function_Base):
    """ Two-point correlator function for spatial-momentum projected states
        (only t-dependent) including excited states contamination. 
        "Ladder" stands for explicit ordering of energy states:
            param = c2pt_[e0, de1_0, de2_1...]
            f(t) = X_0(c_0 + X_1(c_1 + ... X_N c_N)...) at min_{c_i} (chi2},
            where X_I = e^{-de{I}_{I-1}*t}, 
            or
            f(t) = c_0 e^{-E_0 t} + c_1 e^{-E_1 t} + ... + c_N e^{-E_N t},
            where E_0 = e0,
                  E_1 = e0 + de1_0,
                  ...
                  E_N = e0 + ... + de{N}_{N-1}
        here, chi2=||f(t)-y||^2_{corr^-1}
        When one puts range limits on energy differences de{I}_{I-1} > 0, fit should give
        stable ordering of the distinct energy levels.
    """
    def __init__(self, n, y, y_sigma, y_ncov=None):
        """ 
            y       2pt correlator c2[t] for all t
            y_sigma uncertainty in y[t]
            y_ncov  normalized cov.matrix [t1,t2]
        """
        assert 0 < n
        self.n = n
        self.par_names_list = ['e0']
        for i in range(1, self.n): 
            self.par_names_list.append('de%d%d' %(i, i-1))

        assert(len(y) == len(y_sigma))
        self.y = y
        self.y_sigma = y_sigma
        
        if not None is  y_ncov: 
            self.y_rcov = np.linalg.pinv(y_ncov)
        else: self.y_rcov  = np.identity(len(y), dtype=np.float64)
        self.y_rcov /= y_sigma[None,:] * y_sigma[:,None] 

    def sysmatrix(self, tr, e_n):
        #assert (len(e_n) == self.n)
        res = np.empty((len(tr), len(e_n)))
        e_i = tr * e_n[0]
        res[:,0] = np.exp(-e_i)
        for i in range(1, len(e_n)):
            e_i += tr * e_n[i]
            res[:,i] = np.exp(-e_i)
        return res
    def varpro_solve(self, tr, e_n):
        y       = self.y[tr]
        y_rcov  = self.y_rcov[tr,:][:,tr]
        
        sysm    = self.sysmatrix(tr, e_n)
        rC_A    = np.dot(y_rcov, sysm)
        A_rC_A  = np.dot(sysm.T, rC_A) 
        r_A_rC_A= np.linalg.pinv(A_rC_A)
        return np.dot(r_A_rC_A, np.dot(rC_A.T, y))
    def varpro_approx(self, tr, e_n):
        """ return y_p = sysmatrix . solve(y) """
        y       = self.y[tr]
        y_rcov  = self.y_rcov[tr,:][:,tr]
        
        sysm    = self.sysmatrix(tr, e_n)
        rC_A    = np.dot(y_rcov, sysm)
        A_rC_A  = np.dot(sysm.T, rC_A) 
        r_A_rC_A= np.linalg.pinv(A_rC_A)
        return np.dot(sysm, np.dot(r_A_rC_A, np.dot(rC_A.T, y)))

    def f_range(self, tr, param_):
        e_n     = self.select_param(param_)
        return self.y[tr] - self.varpro_approx(tr, e_n)
    def varpro_chi2(self, tr, param_):
        f_vec   = self.f_range(tr, param_)
        return np.dot(f_vec, np.dot(self.y_rcov[tr,:][:,tr], f_vec))
    def get_ndf(self):
        return lhpd.fitter.Model_function_Base.get_ndf() - self.n 
