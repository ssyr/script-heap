import numpy as np
import lhpd
from lhpd.fitter import modelfunc_inline, datafit, fitter2
import dataview as dv

import matplotlib as mpl ; import matplotlib.pyplot as plt

def calc_plot_model(mfi, xv, yv, dyv, ycov=None, 
                    ax=None, stg=dv.style_group_default[0], 
                    label=None,
                    xv_plot=None, xv_min=None, xv_max=None, 
                    xsh_plot=0, p0=None) :
    df  = datafit(mfi, xv, yv, dyv, ycov=ycov)
    if None is p0 : p0 = np.zeros((mfi.plen(),), np.float)
    else : 
        p0 = np.asarray(p0, np.float)
        assert(len(p0) == mfi.plen())
    p   = fitter2(df).fit(p0=p0)
    pcov= df.pcov(p[0])
    pprint_ve_cov(p[0], pcov, title=mfi.pname)
    if not None is ax : 
        if None is xv_plot:
            xv_plot = dv.make_range(xv, 100,
                    min_or=xv_min, max_or=xv_max)
        yv_plot = mfi.fv(xv_plot, p[0])
        ax.plot(xv_plot + xsh_plot, yv_plot)
        ax.errorbar(xv + xsh_plot, yv, yerr=dyv, 
                    label=label, **stg.edotline())
def calc_plot_model_bs(mfi, xv, yv, dyv, ycov=None, 
                    ax=None, stg=dv.style_group_default[0], 
                    label=None,
                    xv_plot=None, xv_min=None, xv_max=None, 
                    xsh_plot=0, nbs=1000, p0=None) :
    df    = datafit(mfi, xv, yv, dyv, ycov=ycov)
    rsplan= ('bs',1)
    fit   = fitter2(df)
    if None is p0 : p0 = np.zeros((mfi.plen(),), np.float)
    else : 
        p0 = np.asarray(p0, np.float)
        assert(len(p0) == mfi.plen())
    p     = fit.fit(p0=p0)
    pcov  = df.pcov(p[0])
    #pprint_ve_cov(p[0], pcov, title=mfi.pname)

    yv_bs = lhpd.stat.gen_ens_mvar(nbs, yv, dyv, ycov)
    p_rs  = []
    for i_bs, yv_bs_i in enumerate(yv_bs):
        df.set_all(ya=yv_bs_i)
        p_rs.append(fit.fit(p0=p0)[0])
    p_rs  = np.array(p_rs)
    p_ac  = lhpd.calc_avg_cov(p_rs, rsplan)
    pprint_ve_cov(p_ac[0], p_ac[1], title=mfi.pname)
    
    if not None is ax : 
        if None is xv_plot:
            xv_plot = dv.make_range(xv, 100,
                    min_or=xv_min, max_or=xv_max)
        yv_plot = mfi.fv(xv_plot, p[0])
        #ax.plot(xv_plot + xsh_plot, yv_plot)
        #ax.errorbar(xv + xsh_plot, yv, yerr=dyv, 
        #            label=label, **stg.edotline())
        dv.plot_func_band_rs(lambda x,p:mfi.f(x,p), xv_plot, p_rs, rsplan, 
                             ax, stg, xsh=xsh_plot) 

mfi_linear  = modelfunc_inline(['a0', 'a1'],
        lambda x,p : p[0] + p[1] * x,
        lambda x,p : [1,  x])
mfi_dipole  = modelfunc_inline(['A', 'B'],
        lambda x,p : p[0] / (1. + p[1] * x)**2,
        lambda x,p : [1./(1. + p[1] * x)**2, 
                      (-2.*p[0]*x) / (1. + p[1] * x)**3])
mfi_lincub  = modelfunc_inline(['a1', 'a3'],
        lambda x,p : p[0] * x + p[1] * x**3,
        lambda x,p : [x,  x**3])
