from __future__ import print_function
import numpy
import math
#from config_mpl import *

def make_gpof(matr, t0, m1, m2, dt=1):
    """ build gpof matrix
        matr    correlator matrix [shape_data, n1, n2, t]
                n1, n2 = the number of sources and sinks
                regularly, shape_data = (n_data,); function works any shape_data
        t0      starting time
        m1, m2  gpof dimensions(t-shifts) for the source and sink sides

        return: gpof_matr[shape_data, n1*m1, n2*m2], m1*m2 matrix 
                built from t=t0 ... t0 + m1+m2
    """
    assert(3 <= len(matr.shape))
    sh      = matr.shape[:-3]
    n1, n2, lt = matr.shape[-3:]
    assert(0 <= t0 and t0 + dt * (m1 + m2 - 2) < lt)
    gpof    = numpy.empty(sh + (n1*m1, n2*m2),
                          matr.dtype)
    gpof_rs = gpof.reshape(sh + (n1, m1, n2, m2))
    for i1 in range(m1):
        for i2 in range(m2):
            gpof_rs[..., :, i1, :, i2] = matr[..., :, :, t0 + dt * (i1 + i2)]
    return gpof

# TODO un-gpof: extract correlator data from GPoF matrix 
#def make
#   s[i1,i2][t] = mean_i { gpof[n1*i+i1, n2*(t-i) + i2] }

# TODO examine contributions to S/N ratio:
# var(s[i1,i2][t] * (contrib(i-th s.v., s[i1,i2][t]) / s[i1,i2][t])**2


def reduce_svd(a, tol):
    from numpy.linalg import svd
    u, s, v = svd(a, 0, 1)
    i = 1
    while i < len(s) and s[0] * tol < s[i]: i += 1
    sp  = s[:i]
    up  = u[:, :i]
    vp  = v[:i, :]
    return up, sp, vp

def estim_tol(avg, sigma):
    return (sigma / avg).max()

def diag_gpof(c2pt, t0, tr, m1, m2, tol):
    """ c2pt[shape_data, n1, n2, t]
        return ev[i_t, i_ev], sorted desc. in i_ev
    """
    from numpy import dot
    from numpy.linalg import svd, eigvals
    gp0     = make_gpof(c2pt, t0, m1, m2)
    u, s, v = reduce_svd(gp0, tol)
    spX = 1. / numpy.sqrt(s)
    upH = spX[:, None] * u.T.conj()
    vpH = v.T.conj() * spX[None,:]

    n_ev= len(s)
    ev  = numpy.zeros((len(tr), n_ev), numpy.complex128)

    for i_t, t in enumerate(tr):
        gp  = make_gpof(c2pt, t, m1, m2)
        sysmat  = dot(upH, dot(gp, vpH))
        ev[i_t] = eigvals(sysmat)
        if t < t0: ev[i_t].sort()
        else: ev[i_t, ::-1].sort()
    return ev

def show_gpof(ens, key_ll, mom3, t0_list, tr, m1, m2, tol, 
            symm_src_snk=False, rsplan=('jk',1),
            xshift=0, fmt=fmt_marker_default, fmt_color=fmt_color_default,
            show=True, **kwargs):
    """
        ens         dict: key->ens
        key_ll      double list of keys, [i_src][i_snk]
        mom3        momentum
        t0_list     list of t0's
        tr          t-range
        m1,m2       shifts of source, sink
        dt          eff.mass interval
        xshift      shift each t0 group in x direction for readability
        fmt         formats of markers
    """
    from lhpd import resample
    fig = plt.figure()
    ax  = fig.add_axes([.1, .1, .8, .8])

    # XXX real part of c2pt is taken here
    tr      = numpy.array(tr)
    tr_enc  = numpy.array(range(tr.max() + 2))
    c2pt    = []
    for kl in key_ll: 
        c2pt.append(get_c2pt(ens, kl, mom3).real)
    c2pt    = numpy.array(c2pt)     # shape: [i_src, i_data, i_snk, t]
    c2pt    = numpy.array(c2pt.transpose(1, 0, 2, 3))
    if symm_src_snk and c2pt.shape:
        print('Averaging c2pt -> (c2pt + c2pt.H) / 2')
        c2pt = (c2pt + c2pt.transpose(0,2,1,3).conj()) / 2.

    c2pt_rs = resample(c2pt, rsplan)
    for i_t0, t0 in enumerate(t0_list):
        l_ev = []
        n_ev_min = -1
        for x in c2pt_rs:
            cur_ev = diag_gpof(x, t0, tr_enc, m1, m2, tol)
            if n_ev_min < 0 or cur_ev.shape[-1] < n_ev_min: 
                n_ev_min = cur_ev.shape[-1]
            l_ev.append(cur_ev)
        for i in range(len(l_ev)): 
            l_ev[i] = l_ev[i][:,:n_ev_min]    # drop extra ev's
        l_ev = numpy.array(l_ev).real
        #print(l_ev.mean(0))
        meff = numpy.log(l_ev[:, tr, :] / l_ev[:, tr + 1, :])
        meff_avg, meff_err = calc_avg_err(meff, rsplan)
        #print(('# t0=%d\n' % t0), meff_avg, '\n', meff_err)
        #tsv.write_val_err(sys.stdout, meff_avg, meff_err+0)
        
        cur_ymin, cur_ymax = meff_avg.min(), meff_avg.max()

        for i_ev in range(n_ev_min):
            cur_fmt=modify_format(fmt[i_t0], c=fmt_color[i_ev])
            plot_data2D(ax, i_t0, numpy.array(tr) + i_t0 * xshift,
                        meff_avg[:, i_ev],
                        meff_err[:, i_ev],
                        label=(r'$t_0=%d, n=%d$' % (t0, i_ev)),
                        fmt=cur_fmt)
            
    set_xlim(ax, tr.min(), tr.max(), **kwargs)
    #ax.set_ylim(*calc_window(meff_avg.min(), meff_avg.max(), 
    #            y_margin, ax.get_yscale()))
    #ax.set_autoscaley_on(True)
    set_ylim(ax, cur_ymin, cur_ymax, **kwargs)

    ax.set_xlabel(r'$t+%.2f*t_0$' % (xshift,))
    ax.set_ylabel(r'$m^{eff}$')

    ax.set_xticks(tr)
    ax.legend()
    if show:
        try:
            plt.show()
        except KeyboardInterrupt: pass
    return fig
