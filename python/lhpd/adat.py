import numpy
import re
import math

def read_adat(fd):
    """ read ADAT file and return cplx/real 2d array 
        file must be rewound to the beginning with
        > fd.seek(0, 0)
    """
    line_cnt = 0
    l, line_cnt   = fd.readline(), line_cnt + 1
    if '' == l :
        raise ValueError(("%s: empty ADAT file" % fd.name))
    lre = re.match('^(\d+)\s+(\d+)\s+([01])\s+(\d+)\s+(\d+)\s*$', l)
    if None is lre:
        raise ValueError("%s: syntax error at line %d: '%s'" 
                        % (fd.name, line_cnt, l))
    n_data, n_t, is_cplx, x_a, x_b = [ int(x) for x in lre.groups() ]

    def parse_real(l):
        lre = re.match('^(\d+)\s+([-+\.\d+eE]+)\s*$', l)
        if None is lre:
            raise ValueError("%s: syntax error at line %d: '%s'" 
                            % (fd.name, line_cnt, l))
        l = l[:-1]      # chop off '\n'
        try:
            t   = int(lre.groups()[0])
            x   = float(lre.groups()[1])
        except ValueError, errstr: 
            raise (("%s: syntax error at line %d: '%s'" % (fd.name, line_cnt, l)) 
                        + errstr)
        return t, x

    def parse_cplx(l):
        lre = re.match('^(\d+)\s+([-+\.\d+eE]+)\s+([-+\.\d+eE]+)\s*$', l)
        if None is lre:
            raise ValueError("%s: syntax error at line %d: '%s'" 
                            % (fd.name, line_cnt, l))
        try:
            t   = int(lre.groups()[0])
            x   = float(lre.groups()[1])
            y   = float(lre.groups()[2])
        except ValueError, errstr: 
            raise ValueError("%s: syntax error at line %d: '%s'" % (fd.name, line_cnt, l)
                        + errstr))
        return t, complex(x, y)

    if is_cplx:
        data = numpy.empty((n_data, n_t), dtype=numpy.complex128)
        parse_line  = parse_cplx
    else:
        data = numpy.empty((n_data, n_t), dtype=numpy.float64)
        parse_line  = parse_real

    for i_data in range(n_data):
        for i_t in range(n_t):
            l, line_cnt   = fd.readline(), line_cnt + 1
            if '' == l :
                raise ValueError("unexpected end of file at , line empty file '%s'" % fd.name)
            l = l[:-1]       # chop off '\n'
            t, x = parse_line(l)
            if i_t != t:
                raise ValueError("%s: mismatch of line number and time index at line %d: '%s'" 
                                    % (fd.name, line_cnt, l))
            data[i_data, i_t] = x

    return data
