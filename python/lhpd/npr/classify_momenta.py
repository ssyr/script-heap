from __future__ import print_function
import math
import numpy as np
import h5py
import itertools as it

import lhpd
from lhpd.npr.npr_common import *
from lhpd.h5_io import h5_purge_keys
from lhpd.misc.lat_geom import inv_wavevec, cmp_wavevec, std_inv_wavevec, std_rot3d_wavevec




"""
# USAGE & DEBUG
h5npr=h5py.File('tmp-npr.h5', 'w')
#wvlist=make_wavevec_list(ens_cfg_48c96pt, 10*np.r_[1,1,1,2], 10., h5grp=h5npr.require_group('/mom2_lin'))
wvlist=h5npr['']

# list all wavevec combinations of specific geometry
mom_uniq=find_wvcomb3_hex2d(ens_cfg_48c96pt, h5npr['/mom2_lin/wavevec'][slice(*h5npr['/mom2_lin/momsq_range'][200])], cos_tol=1e-3, VERBOSE=True)

# search + classification
#wvcomb_list=make_wvcomb_d4u2_list(ens_cfg_48c96pt, 'simp3d', wvlist[0], wvlist[2][200:202], h5grp=h5npr.require_group('/mom2_lin/simp3d'), VERBOSE=True)
#wvc_list=make_wvcomb_d4u2_list(ens_cfg_48c96pt, 'simp3d', wvlist[0], wvlist[2][200:202], VERBOSE=True)


print(wvc_list[0][slice(*wvc_list[4][-1])])


    
def UTEST_runme() :
    h5npr=h5py.File('tmp-npr.h5', 'a')
    #wvlist=make_wavevec_list(ens_cfg_48c96pt, np.r_[-12,-12,-12,-24], np.r_[11, 11, 11, 23], 100., h5grp=h5npr.require_group('/mom2_lin'))
    wvlist=[h5npr['/mom2_lin'][s].value for s in ['wavevec', 'momsq', 'eqb_momsq_wavevec', 'momsq_uniq' ]]

    print_prop_components(ens_cfg_48c96pt, 'exonly', wvlist[0], h5grp=h5npr.require_group('/prop_avg'))


    #wvcomb_list=make_wvcomb_d4u2_list('simp3d', wvlist[0], wvlist[2][200:210], ens_cfg_48c96pt.latsize, ens_cfg_48c96pt.ft_twist, h5grp=h5npr.require_group('/mom2_lin/simp3d-select'), VERBOSE=True)
    #wvcomb_list=make_wvcomb_d4u2_list('hex2d', wvlist[0], wvlist[2][200:210], ens_cfg_48c96pt.latsize, ens_cfg_48c96pt.ft_twist, h5grp=h5npr.require_group('/mom2_lin/hex2d-select'), VERBOSE=True)
    wvcomb_list = [h5npr['/mom2_lin/simp3d-select'][s] for s in ['wvcomb', 'wvcclass', 'eqb_momsq_wvcomb', 'eqb_momsq_wvcclass', 'eqb_wvcclass_wvcomb']]

    calc_save_nnbar_npr(ens_cfg_48c96pt, 'exact', wvcomb_list[0], 
                #meas_slice=np.s_[:3],
                VERBOSE=True)

"""



def make_wavevec_list(ensc, mom_min, mom_max, momsq_max=-1, momsq_tol=1e-8, 
            h5grp=None, VERBOSE=False) :
    """ generate a list of momenta satisfying sum(2*pi*k/L)**2 <= momsq_max 
        sort by momsq, categorize equivalent mom and store to tmp_file
        momsq_

        list of wavevec is ordered according to p2; momsq_range

        dumb walkthrough

        RETURN list of wvec, list of momsq, list of ranges with equal momsq
    """
    ls      = ensc.latsize
    tw      = ensc.ft_twist
    nd      = len(ensc.latsize)
    #mom_max = np.abs(mom_max)
    #mom_ind = np.r_[ [ ensc.latsize, 2*mom_max + 1 ] ].min(axis=0)
    wvec    = coord_meshgrid([ np.r_[mom_min[k] : mom_max[k] + 1]
                               for k in range(nd) ]).reshape(-1, nd)
    momsq   = calc_momsq_lin(wvec, ls, tw)
    isort   = momsq.argsort()
    momsq   = momsq[isort]
    wvec    = wvec[isort]
    if 0 < momsq_max :
        imax    = momsq.searchsorted(momsq_max)
        momsq   = momsq[:imax]
        wvec    = wvec[:imax]

    eqb_momsq= np.array([ (il, iu) for il, iu, v 
                         in equal_bound(momsq, make_cmp_tol(momsq_tol)) ])
    momsq_uniq= np.array([ momsq[il] for il, iu in eqb_momsq ])

    if (not None is  h5grp) :
        h5_purge_keys(h5grp, ['momsq', 'wavevec', 'eqb_momsq_wavevec', 'momsq_uniq'])
        h5grp['wavevec']    = wvec
        h5grp['momsq']      = momsq
        h5grp['eqb_momsq_wavevec']= eqb_momsq
        h5grp['momsq_uniq'] = momsq_uniq
    
    # momsq_uniq is redundant and may be removed later : 
    # momsq_uniq[:] = np.r_[ eqb_momsq[:,0] ] == np.r_[ eqb_momsq[:,1] ]
    return wvec, momsq, eqb_momsq, momsq_uniq

def std_d4u2_wvcomb(wvcomb, ls, tw) :
    """ project onto a CoE repr: 
            * a,b,c <- std_inv(a),std_inv(b),std_inv(c)
            * a<=b
        this function selects a representative from a class of wv combinations
        that produce the same element for contraction 
            < D[a] D[abar] D[b] D[bbar] U[c] U[cbar] Op-6quark>
        wavevectors "(a,b,c)bar" := "-(a,b,c)" taking into account lattice BC
    """
    a, b, c = [ tuple(std_inv_wavevec(x, ls, tw)) for x in wvcomb ]
    if b < a : a, b = b, a
    return (a, b, c)

def std_rot3d_permut_wvcomb(wvcomb, ls, tw) : 
    """ select repr of CoE for a class of wavevec comb.
        generated from wvcomb by rotations in 3d and permutations 
        of wavevec
    """
    a, b, c = [ tuple(std_rot3d_wavevec(x, ls, tw)) for x in wvcomb ]
    # sort to a<b<c
    if b < a : a, b = b, a
    if b < c : pass
    elif a < c : a, b, c = a, c, b
    else : a, b, c = c, a, b
    return (a, b, c)


def find_wvcomb3_hex2d(wvec, ls, tw, cos_tol=1e-3, VERBOSE=False) :
    """ find all combinations of 3 wavevecs (a,b,a-b) such that angle(a,b)=pi/3
        FIXME? momsq(a-b) is not necessarily ==momsq(a),momsq(b)
    """
    mom         = calc_mom_lin(wvec, ls, tw)
    momsq       = calc_momsq_lin(wvec, ls, tw)
    momsq_sqrt  = np.sqrt(momsq)
    mom_cos     = ( np.einsum('ik,jk->ij', mom, mom) 
                    / momsq_sqrt / momsq_sqrt[:, None] )
    wvcomb_list = []
    cnt = 0
    for i, j in np.ndindex((len(mom), len(mom))) : #it.combinations(range(len(mom)), 2) :
        if abs(mom_cos[i,j] - .5) <= cos_tol :
            # build standard three vec
            if False : 
                a = std_inv_wavevec(wvec[i], ls, tw)
                b = std_inv_wavevec(wvec[j], ls, tw)
                c = std_inv_wavevec(wvec[i] 
                            + inv_wavevec(wvec[j], ls, tw), ls, tw)        # c = "a - b"
                if cmp_wavevec(b, a) < 0 : a, b = b, a 
                # order (a,b,c)
                if cmp_wavevec(b, c) < 0 : pass
                elif cmp_wavevec(a, c) < 0 : a, b, c = a, c, b
                else : a, b, c = c, a, b
            else : 
                a, b, c = ( tuple(wvec[i]), tuple(wvec[j]), 
                            tuple(wvec[i] + inv_wavevec(wvec[j], ls, tw)) )
            if True : #not abc in wvcomb_list :
                # DEBUG
                cnt += 1
                mabc = np.array([ calc_mom_lin(k, ls, tw) for k in a, b, c ])
                mabcsq_sqrt = np.sqrt(np.array([ calc_momsq_lin(k, ls, tw) 
                                                 for k in a, b, c ]))
                mabc_cos = (np.einsum('ik,jk->ij', mabc, mabc)  
                                    / mabcsq_sqrt / mabcsq_sqrt[:, None] )
                if VERBOSE : print(cnt, a,b,c, mabc_cos[[0,0,1], [1,2,2]])
            wvcomb_list.append((tuple(a), tuple(b), tuple(c)))

    return wvcomb_list

def find_wvcomb3_simp3d(wvec, ls, tw, cos_tol=1e-3, VERBOSE=False) :
    """ find all combinations of wavevecs (a,b,c) such that 
        angle(a,b) = angle(b,c) = angle(a,c) approx pi/2
    """
    mom         = calc_mom_lin(wvec, ls, tw)
    momsq       = calc_momsq_lin(wvec, ls, tw)
    momsq_sqrt  = np.sqrt(momsq)
    mom_cos     = ( np.einsum('ik,jk->ij', mom, mom) 
                    / momsq_sqrt / momsq_sqrt[:, None] )
    wvcomb_list = []
    cnt = 0
    for i, j in np.ndindex((len(mom), len(mom))) : #it.combinations(range(len(mom)), 2) :
        if cos_tol < abs(mom_cos[i,j]) : 
            continue
        for k in range(len(mom)) :
            if cos_tol < abs(mom_cos[i, k]) or cos_tol < abs(mom_cos[j, k]) :
                continue
            a, b, c = (tuple(wvec[i]), tuple(wvec[j]), tuple(wvec[k]))

            if True:
                # DEBUG
                cnt += 1
                mabc = np.array([ calc_mom_lin(k, ls, tw) for k in a, b, c ])
                mabcsq_sqrt = np.sqrt(np.array([ calc_momsq_lin(k, ls, tw) 
                                                 for k in a, b, c ]))
                mabc_cos = (np.einsum('ik,jk->ij', mabc, mabc)  
                                    / mabcsq_sqrt / mabcsq_sqrt[:, None] )
                if VERBOSE : print(cnt, a,b,c, mabc_cos[[0,0,1], [1,2,2]])

            wvcomb_list.append((a, b, c))
    return wvcomb_list

def filter_wvcomb3_d4u2(wvcomb_list, wvcomb_tostd_func) :
    """ OBSOLETE, should not be used; all functionality is atomized in 
        `classify_list' and appropriate mappings elem->class(elem)

        filter wv combinations (a,b,c) according to antisymmetric structure of 
        contraction < D[a] D[abar] D[b] D[bbar] U[c] U[cbar] >
    """
    s = list(set([wvcomb_tostd_func(wvc) for wvc in wvcomb_list]))
    s.sort
    return s
    """
    for a, b, c in wvcomb_list :
        a = tuple(std_inv_wavevec(a, ls, tw))
        b = tuple(std_inv_wavevec(b, ls, tw))
        if b < a : continue # drop
        c = tuple(std_inv_wavevec(c, ls, tw))
        res.add((a,b,c))
    res = list(res)
    res.sort()
    return res
    """

    
def classify_wvcomb(wv_comb_list, wv_tostd_func) : 
    """ OBSOLETE, should not be used; all functionality is atomized in
        `classify_list' and appropriate mappings elem->class(elem)

        classify wavevector combinations into equivalence classes 
            * wavevectors are equiv within orbit projector wv_tostd_func
            * permutation of wavevectors within a wv_comb
        INPUT:
        wv_comb_list = [ [wv0, wv1, ...], ... ]

        RETURN: 
            ( list : [i_wv_comb_equiv] -> [ i_wv_comb_0, ... ] 
              list : [i_wv_comb_equiv] -> (i_wvstd_0, ...)
              list : [i_wvstd] -> wvstd )

        notations: 
            wv          wavevector
            wvstd       "standard" wavevector == representative in the orbit
            olist       ordered list (e.g. used for mapping)
    """
    # TODO rewrite using `classify_list'
    # build equiv map of wv
    wvstd_comb_list = [ [ tuple(wv_tostd_func(wv))
                          for wv in wv_comb_i ] 
                        for wv_comb_i in wv_comb_list ]
    wvstd_olist  = list(set(wvstd for wvstd_comb_i in wvstd_comb_list 
                                  for wvstd in wvstd_comb_i))
    wvstd_olist.sort()           # FIXME shorter version using ordered set iterator?
    wvstd_map   = dict([ (v, i) for i, v in enumerate(wvstd_olist) ])  # inverse map
   
    # build equiv map of wv_comb
    wvo_comb_list   = [ tuple(sorted(wvstd_map[wvstd]
                                     for wvstd in wvstd_comb_i))
                        for wvstd_comb_i in wvstd_comb_list ]
    wvo_comb_olist  = list(set(wvo_comb_list))
    wvo_comb_olist.sort()       # FIXME shorter version using ordered set iterator?
    wvo_comb_map    = dict([ (v, i) for i, v in enumerate(wvo_comb_olist) ])
    
    # build lists of equiv elements from wv_comb_list
    wvo_comb_equiv  = [ [] for i in range(len(wvo_comb_olist)) ]    # different lists
    for i, wvoc in enumerate(wvo_comb_list) :
        wvo_comb_equiv[wvo_comb_map[wvoc]].append(i)

    return wvo_comb_equiv, wvo_comb_olist, wvstd_olist


def make_wvcomb_d4u2_list(wvcomb_type, wvec, eqb_momsq, ls, tw, 
            h5grp=None, VERBOSE=False) :
    """
        h5grp       RW h5 group to read wavevecs and save wvcomb's
        
        return : map
    """
    

    assert(wvcomb_type in ['hex2d', 'simp3d'])
    wvec_map = dict([ (tuple(wv), i) for i, wv in enumerate(wvec) ])
    def wvcomb_map(wvc) :
        return tuple(wvec_map[tuple(wv)] for wv in wvc)

    def wvec_exist(wv) :
        return tuple(wv) in wvec_map

    def wvcomb_exist(wvc) :
        return all([ (  tuple(wv) in wvec_map
                        and tuple(inv_wavevec(wv, ls, tw)) in wvec_map)
                     for wv in wvc ])

    wvcomb_list         = []        # [wvc]
    wvcclass_list       = []        # [wvcclass]
    # "eqb_X_Y" : equal_bound of X in Y
    eqb_momsq_wvcomb    = []        # [ (i_wvc_lo, i_wvc_hi) for class[:] ]
    eqb_momsq_wvcclass  = []        # [ (i_wvcclass_lo, i_wvcclass_hi) for momsq[:] ]
    eqb_wvcclass_wvcomb = []        # [ (i_wvc_lo, i_wvc_hi) for wvcclass[:] ]

    for i_eqb, (eqb_lo, eqb_hi) in enumerate(eqb_momsq) :
        wvcomb_list_full = { 'hex2d' : find_wvcomb3_hex2d, 
                             'simp3d': find_wvcomb3_simp3d }[wvcomb_type] (
                wvec[eqb_lo : eqb_hi], ls, tw)
        wvcomb_list_full = filter(wvcomb_exist, wvcomb_list_full)
        # make a list of distinct wvcomb's for d4u2 contractions
        ind_d4u2_wvcomb, wvcomb_list_cur = classify_list(
                wvcomb_list_full, 
                lambda wvc: std_d4u2_wvcomb(wvc, ls, tw))
        # make a list of wvcomb equivalence classes under rot3d & permut(wva,wvb,wvc)
        ind_wvcomb_wvcclass, wvcclass_list_cur = classify_list(
                wvcomb_list_cur,
                lambda wvc : std_rot3d_permut_wvcomb(wvc, ls, tw))

        # sort so that wvcomb_class counter is ascending
        c_isort             = np.argsort(ind_wvcomb_wvcclass)
        ind_wvcomb_wvcclass = [ ind_wvcomb_wvcclass[i] for i in c_isort ]
        wvcomb_list_cur     = [ wvcomb_list_cur[i] for i in c_isort ]
        
        # store wvcomb, wvcomb_class
        n_wvcomb            = len(wvcomb_list)
        n_wvcclass          = len(wvcclass_list)
        if True:
            wvcomb_list.extend(wvcomb_list_cur)
            wvcclass_list.extend(wvcclass_list_cur)
        else :
            # mapped version: do I really need to index everything?
            wvcomb_list.extend([ wvcomb_map(wvc) for wvc in wvcomb_list_cur ])
            wvcclass_list.extend([ wvcomb_map(wvc) for wvc in wvcclass_list_cur ])
        n_wvcomb_new        = len(wvcomb_list)
        n_wvcclass_new      = len(wvcclass_list)
        
        # store ranges
        eqb_wvcclass_wvcomb.extend([ 
                (n_wvcomb + j_lo, n_wvcomb + j_hi)
                for j_lo, j_hi, v in equal_bound(ind_wvcomb_wvcclass) ])
        eqb_momsq_wvcomb.append( (n_wvcomb, n_wvcomb_new) )
        eqb_momsq_wvcclass.append( (n_wvcclass, n_wvcclass_new) )
        
        print(i_eqb, (eqb_hi - eqb_lo), n_wvcomb_new, n_wvcclass_new, 
                [wvcomb_map(wvc) for wvc in wvcclass_list_cur])

    wvcomb_list         = np.asarray(wvcomb_list)
    wvcclass_list       = np.asarray(wvcclass_list)
    eqb_momsq_wvcomb    = np.asarray(eqb_momsq_wvcomb)
    eqb_momsq_wvcclass  = np.asarray(eqb_momsq_wvcclass)
    eqb_wvcclass_wvcomb = np.asarray(eqb_wvcclass_wvcomb)
    if not None is  h5grp :
        h5_purge_keys(h5grp, [ 'wvcomb', 'wvcclass',
                    'eqb_momsq_wvcomb', 'eqb_momsq_wvcclass',
                    'eqb_wvcclass_wvcomb' ])
        h5grp['wvcomb']             = wvcomb_list
        h5grp['wvcclass']           = wvcclass_list
        h5grp['eqb_momsq_wvcomb']   = eqb_momsq_wvcomb
        h5grp['eqb_momsq_wvcclass'] = eqb_momsq_wvcclass
        h5grp['eqb_wvcclass_wvcomb']= eqb_wvcclass_wvcomb
        h5grp.file.flush()
        
    return (wvcomb_list, wvcclass_list, 
            eqb_momsq_wvcomb, eqb_momsq_wvcclass, eqb_wvcclass_wvcomb)


