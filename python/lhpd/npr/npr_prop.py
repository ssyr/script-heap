from __future__ import print_function
import math
import numpy as np
import h5py
import itertools as it

import os
import sys
import time
import pickle

# this will go to "prod", so leep all module abs.paths
import lhpd
from lhpd import np_vec_cmp

import lhpd.npr
from lhpd.npr.npr_common import *
from lhpd.npr import nnbar
from lhpd.pymath.gamma_matr import Gamma16_dgr as Gamma16

import tsv
import matplotlib as mpl
#mpl.use('Agg')       
import matplotlib.pyplot as plt
import dataview as dv


def show_propsq_axis(ensc, ama_mode,
                     meas_slice=slice(None), VERBOSE=False,
                     ax=None, stg_list=iter(dv.style_group_default),
                     h5grp=None) :
    latdim      = len(ensc.latsize)
    stg_list    = iter(stg_list)

    if None is ax : ax = dv.make_std_axes()
    ax.set_xlabel(r'$k_\mu*\xi_\mu$')
    ax.set_ylabel(r'$|prop|^2$')
    for mu in range(latdim) :
        mom_list = np.zeros((ensc.latsize_save[mu], latdim), int)
        mom_list[:, mu] = np.r_[ : ensc.latsize_save[mu]] - ensc.lat_save_max[mu]
        prop    = ensc.get_frwprop_ft(mom_list, ama_mode,
                    meas_slice=meas_slice, VERBOSE=VERBOSE)
        stg     = next(stg_list)
    
        p2      = (prop*prop.conj()).reshape(prop.shape[:-4] + (12**2,)).sum(-1).real
        mom_mu  = mom_list[:,mu] * (2*math.pi / ensc.latsize[mu] * ensc.lat_aniso[mu])
        p2_a, p2_e = lhpd.calc_avg_err(p2)
        ax.errorbar(mom_mu, p2_a, yerr=p2_e, 
                label=(r'$\mu=%d$' % mu), **stg.edotline())

    return ax

####################################################################################################
# EXPLORE QUARK AND GLUON PROPAGATORS
####################################################################################################

def print_prop_components(ensc, ama_mode, wvlist, 
            meas_slice=slice(None), rsplan=('jk', 1),
            h5grp=None, fo=sys.stdout, d_wvlist=1) :
    ls          = ensc.latsize
    tw          = ensc.ft_twist
    pb  = lhpd.progbar(len(wvlist), prefix=('%d mom' % len(wvlist)))

    n_mom           = len(wvlist)

    res_TrSsq_a     = np.empty((n_mom,), np.float)
    res_TrSsq_e     = np.empty_like(res_TrSsq_a)
    res_TrSinvsq_a  = np.empty((n_mom,), np.float)
    res_TrSinvsq_e  = np.empty_like(res_TrSinvsq_a)
    res_TrGammaSinv = np.empty((n_mom,), np.float)
    for i_wvlist in range(0, n_mom, d_wvlist) :
        sl_wv       = slice(i_wvlist, i_wvlist + d_wvlist)
        prop_cs     = ensc.get_frwprop_ft(wvlist[sl_wv], ama_mode, meas_slice)
        prop_cs_r   = lhpd.resample(prop_cs, rsplan)
        sh_r_base   = prop_cs_r.shape[:2]
        prop_r      = prop_cs_r.reshape(sh_r_base + (NCOLSPIN, NCOLSPIN))
        
        prop_sq = (np.abs(prop_r)**2).reshape(sh_r_base + (-1,)).sum(-1) / NCOLSPIN
        res_TrSsq_a[sl_wv], res_TrSsq_e[sl_wv] = lhpd.calc_avg_err(prop_sq, rsplan)
        
        prop_inv= np.linalg.inv(prop_r)
        prop_inv_sq = (np.abs(prop_inv)**2).reshape(sh_r_base + (-1,)).sum(-1) / NCOLSPIN
        res_TrSinvsq_a[sl_wv], res_TrSinvsq_e[sl_wv] = lhpd.calc_avg_err(prop_inv_sq, rsplan)
        # TODO Tr[GammaSinv]

        pb.step(d_wvlist)
        pb.redraw()
    pb.finish()

    k_lin       = calc_mom_lin(wvlist, ls, tw)
    k_lin_sq    = calc_momsq_lin(wvlist, ls, tw)
    k_sin       = calc_mom_sin(wvlist, ls, tw)
    k_sin_sq    = calc_momsq_sin(wvlist, ls, tw)

    if not None is  h5grp :
        h5_purge_keys(h5grp, [ 'TrSsq_a', 'TrSsq_e',
                   'TrSinvsq_a', 'TrSinvsq_e',
                    # TODO Tr[GammaSinv]
                   'k_lin', 'k_lin_sq',
                   'k_sin', 'k_sin_sq' ])
        h5grp['k_lin']      = k_lin
        h5grp['k_sin']      = k_sin
        h5grp['k_lin_sq']   = k_lin_sq
        h5grp['k_sin_sq']   = k_sin_sq
        h5grp['TrSsq_a']    = res_TrSsq_a
        h5grp['TrSsq_e']    = res_TrSsq_e
        h5grp['TrSinvsq_a'] = res_TrSinvsq_a
        h5grp['TrSinvsq_e'] = res_TrSinvsq_e
        h5grp.file.flush()

    t2d = np.s_[..., None]
    out = np.r_['-1', 
                k_lin, k_sin, k_lin_sq[t2d], k_sin_sq[t2d], 
                res_TrSsq_a[t2d], res_TrSsq_e[t2d],
                res_TrSinvsq_a[t2d], res_TrSinvsq_e[t2d]
                ]
    tsv.write_format(fo, out)
        



def UTEST_check_frwprop(mom_list=[[0,0,0,0], [2,3,4,8]], ama_mode='exact', VERBOSE=False) :
    x_cs=ens_cfg_48c96pt.get_frwprop_ft(mom_list, ama_mode, VERBOSE=VERBOSE)
    rsplan=('jk',1)
    x_cs_r=lhpd.resample(x_cs, rsplan)
    x_sh_base = x_cs.shape[:2]
    x_r=x_cs_r.reshape(x_sh_base + (12,12))
    x2tr=(x_r*x_r.conj()).real.sum(-1).sum(-1)
    x2tr_a,x2tr_e=lhpd.calc_avg_err(x2tr, rsplan)
    #print(x2tr_a, x2tr_e)

    xinv_r=np.linalg.inv(x_r)
    xinv2tr=(xinv_r*xinv_r.conj()).real.sum(-1).sum(-1)
    xinv2tr_a,xinv2tr_e=lhpd.calc_avg_err(xinv2tr, rsplan)
    #print(xinv2tr_a, xinv2tr_e)
    
    for m,x1a,x1e,x2a,x2e in zip(mom_list, x2tr_a, x2tr_e, xinv2tr_a, xinv2tr_e) :
        print('%s\t%e\t%e\t%e\t%e' % (str(tuple(m)), x1a, x1e, x2a, x2e))

# TODO move to lib

from lhpd.pymath.tensor import proj_vec, proj_vec_comp

def calc_invprop_comp(ensc, mom_list, ama_mode='exact', 
            rsplan=('jk',1), VERBOSE=False, h5grp=None, meas_slice=slice(None)):

    ls          = ensc.latsize
    tw          = ensc.ft_twist
    
    x_cs    = ensc.get_frwprop_ft(mom_list, ama_mode, VERBOSE=VERBOSE, meas_slice=meas_slice)
    x_cs_r  = lhpd.resample(x_cs, rsplan)
    a_r     = np.einsum("abcicj,nji->abn", x_cs_r, Gamma16 / NCOLSPIN) # Tr[S.Gamma(n)]

    x_sh_base = x_cs.shape[:2]
    x_r     = x_cs_r.reshape(x_sh_base + (NCOLSPIN, NCOLSPIN))

    xinv_r  = np.linalg.inv(x_r)
    xinv_cs_r = xinv_r.reshape(x_sh_base + (NCOLOR, NSPIN, NCOLOR, NSPIN))
    ainv_r  = np.einsum("abcicj,nji->abn", xinv_cs_r, Gamma16 / float(NCOLSPIN)) # Tr[S.Gamma(n)]

    """ project inv.propagator onto momenta """
    n_data, n_mom = x_r.shape[0:2]
    # prop : "mass"; projections on lin-mom, sin-mom ; extra
    prop_mp         = np.empty((n_data, n_mom), np.complex)
    prop_proj_plin  = np.empty((n_data, n_mom), np.complex)
    prop_perp_plin  = np.empty((n_data, n_mom), np.float)
    prop_proj_psin  = np.empty((n_data, n_mom), np.complex)
    prop_perp_psin  = np.empty((n_data, n_mom), np.float)
    prop_perp       = np.empty((n_data, n_mom), np.float)
    # prop inv : "mass"; projections on lin-mom, sin-mom ; extra
    pinv_mp         = np.empty((n_data, n_mom), np.complex)
    pinv_proj_plin  = np.empty((n_data, n_mom), np.complex)
    pinv_perp_plin  = np.empty((n_data, n_mom), np.float)
    pinv_proj_psin  = np.empty((n_data, n_mom), np.complex)
    pinv_perp_psin  = np.empty((n_data, n_mom), np.float)
    pinv_perp       = np.empty((n_data, n_mom), np.float)

    # vec-cmp
    pinv_plin_rho   = np.empty((n_data, n_mom), np.float)
    pinv_plin_cos   = np.empty((n_data, n_mom), np.float)
    pinv_plin_phi   = np.empty((n_data, n_mom), np.float)
    pinv_psin_rho   = np.empty((n_data, n_mom), np.float)
    pinv_psin_cos   = np.empty((n_data, n_mom), np.float)
    pinv_psin_phi   = np.empty((n_data, n_mom), np.float)
    pinv_psym_rho   = np.empty((n_data, n_mom), np.float)
    pinv_psym_cos   = np.empty((n_data, n_mom), np.float)
    pinv_psym_phi   = np.empty((n_data, n_mom), np.float)

    def proj_prop(prop_G16, pvec) :
        """ prop_G16 [..., NSPIN**2] - prop in Gamma16-basis ; pvec - vector of 4-momentum """
        prop_mass   = prop_G16[..., 0]
        prop_mom    = prop_G16[ np.s_[..., [1,2,4,8]] ]
        prop_perp   = prop_G16[ np.s_[..., [3,5,6,7,9,10,11,12,13,14,15]] ]
        # XXX here is the imag.factor that should match the definition of cont.limit AND ft in the code
        # XXX ft in the code = \sum_x exp(i*p*x) y(x), p=momentum(lattice site after ft), (up to twist!)
        # XXX free cont.limit of Dslash = ( +\slashed\partial + m)  [agrees with earlier notes]
        # XXX Results here correspond to
        # XXX \sum_{xy} exp(ipx) (Dslash^{-1})_{xy} delta(y) 
        # XXX   -> \sum_{xy} exp(ipx) ((+\slashed\partial+m)^{-1})_{xy} delta(y)
        # XXX   -> \sum_{xy} \delta(y) ((-\slashed\partial+m)^{-1})_{yx} exp(ipx) 
        # XXX   -> (-i*p + m)
        # XXX if LAT == CONTINUUM ideally, in INVERSE propagator=(-ip+m) expect pl==1+0j, plp==0 
        pl, plp = proj_vec_comp(prop_mom / (-1j), pvec)
        return prop_mass, pl, plp, np.sqrt((prop_perp.conj() * prop_perp).real.sum(-1))
    


    for i_m, mom in enumerate(mom_list) :
        plin = calc_mom_lin(mom, ls, tw)
        psin = calc_mom_sin(mom, ls, tw)
        psym = calc_mom_sym(mom, ls, tw)

        (prop_mp[:, i_m], prop_proj_plin[:, i_m], prop_perp_plin[:, i_m], 
            prop_perp[:, i_m]) = proj_prop(a_r[:, i_m], plin)

        (prop_mp[:, i_m], prop_proj_psin[:, i_m], prop_perp_psin[:, i_m], 
            prop_perp[:, i_m]) = proj_prop(a_r[:, i_m], psin)

        (pinv_mp[:, i_m], pinv_proj_plin[:, i_m], pinv_perp_plin[:, i_m], 
            pinv_perp[:, i_m]) = proj_prop(ainv_r[:, i_m], plin)

        (pinv_mp[:, i_m], pinv_proj_psin[:, i_m], pinv_perp_psin[:, i_m], 
            pinv_perp[:, i_m]) = proj_prop(ainv_r[:, i_m], psin)
        

        prop_mom    = ainv_r[ np.s_[:, i_m, [1,2,4,8]] ] / (-1j)
        (pinv_plin_rho[:, i_m], pinv_plin_cos[:, i_m], 
            pinv_plin_phi[:, i_m]) = np_vec_cmp(prop_mom, plin)
        (pinv_psin_rho[:, i_m], pinv_psin_cos[:, i_m], 
            pinv_psin_phi[:, i_m]) = np_vec_cmp(prop_mom, psin)
        (pinv_psym_rho[:, i_m], pinv_psym_cos[:, i_m], 
            pinv_psym_phi[:, i_m]) = np_vec_cmp(prop_mom, psym)

    if not None is  h5grp :
        h5_purge_keys(h5grp, [  'mom_list', 
                    'k_lin', 'k_sin', 'k_lin_sq', 'k_sin_sq',
                    'prop_mp', 'prop_perp',
                    'prop_proj_plin', 'prop_perp_plin', 
                    'prop_proj_psin', 'prop_perp_psin',
                    'pinv_mp', 'pinv_perp',
                    'pinv_proj_plin', 'pinv_perp_plin', 
                    'pinv_proj_psin', 'pinv_perp_psin',

                    # vec-cmp
                    'pinv_plin_rho', 'pinv_plin_cos', 'pinv_plin_phi',
                    'pinv_psin_rho', 'pinv_psin_cos', 'pinv_psin_phi',
                    'pinv_psym_rho', 'pinv_psym_cos', 'pinv_psym_phi'
                ])
        h5grp.attrs['rsplan']   = pickle.dumps(rsplan)
        h5grp.attrs['ama_mode'] = pickle.dumps(ama_mode)

        h5grp['mom_list']       = mom_list
        h5grp['k_lin']          = calc_mom_lin(mom_list, ls, tw)
        h5grp['k_lin_sq']       = calc_momsq_lin(mom_list, ls, tw)
        h5grp['k_sin']          = calc_mom_sin(mom_list, ls, tw)
        h5grp['k_sin_sq']       = calc_momsq_sin(mom_list, ls, tw)
        h5grp['prop_mp']        = prop_mp
        h5grp['prop_proj_plin'] = prop_proj_plin
        h5grp['prop_perp_plin'] = prop_perp_plin
        h5grp['prop_proj_psin'] = prop_proj_psin
        h5grp['prop_perp_psin'] = prop_perp_psin
        h5grp['prop_perp']      = prop_perp
        h5grp['pinv_mp']        = pinv_mp
        h5grp['pinv_proj_plin'] = pinv_proj_plin
        h5grp['pinv_perp_plin'] = pinv_perp_plin
        h5grp['pinv_proj_psin'] = pinv_proj_psin
        h5grp['pinv_perp_psin'] = pinv_perp_psin
        h5grp['pinv_perp']      = pinv_perp

        # vec-cmp
        h5grp['pinv_plin_rho']  = pinv_plin_rho
        h5grp['pinv_plin_cos']  = pinv_plin_cos
        h5grp['pinv_plin_phi']  = pinv_plin_phi
        h5grp['pinv_psin_rho']  = pinv_psin_rho
        h5grp['pinv_psin_cos']  = pinv_psin_cos
        h5grp['pinv_psin_phi']  = pinv_psin_phi
        h5grp['pinv_psym_rho']  = pinv_psym_rho
        h5grp['pinv_psym_cos']  = pinv_psym_cos
        h5grp['pinv_psym_phi']  = pinv_psym_phi
        h5grp.file.flush()


def show_invprop_comp(ensc, h5grp, 
            ax=None, stg_list=None) :
    if None is stg_list: stg_list = it.cycle(dv.style_group_default)
    if None is ax : ax = dv.make_std_axes()
    
    rsplan = pickle.loads(h5grp.attrs['rsplan'])

    def edotline_x_y_h5(x_key, y_key, stg, label=None, imag=False) :
        x       = h5grp[x_key].value
        y       = h5grp[y_key].value
        if imag :   y = y.imag
        else :      y = y.real
        ya, ye  = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(x, ya, yerr=ye, label=label, **stg.edotline())
    
    def edotline_x_xy_h5(x_key, y_key, stg, label=None, imag=False) :
        x       = h5grp[x_key].value
        y       = h5grp[y_key].value
        if imag :   y = y.imag
        else :      y = y.real
        ya, ye  = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(x, x*ya, yerr=x*ye, label=label, **stg.edotline())
    
    edotline_x_xy_h5('k_lin_sq', 'prop_mp',         next(stg_list), r'\verb|prop_mp|')
    edotline_x_xy_h5('k_lin_sq', 'prop_mp',         next(stg_list), r'\verb|Im prop_mp|', imag=True)
    edotline_x_xy_h5('k_lin_sq', 'prop_proj_plin',  next(stg_list), r'\verb|prop_proj_plin|')
    edotline_x_xy_h5('k_lin_sq', 'prop_proj_plin',  next(stg_list), r'\verb|Im prop_proj_plin|', imag=True)
    edotline_x_xy_h5('k_lin_sq', 'prop_perp_plin',  next(stg_list), r'\verb|prop_perp_plin|')
    edotline_x_xy_h5('k_lin_sq', 'prop_perp',       next(stg_list), r'\verb|prop_perp|')

    edotline_x_y_h5 ('k_lin_sq', 'pinv_mp',         next(stg_list), r'\verb|pinv_mp|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_mp',         next(stg_list), r'\verb|Im pinv_mp|', imag=True)
    edotline_x_y_h5 ('k_lin_sq', 'pinv_proj_plin',  next(stg_list), r'\verb|pinv_proj_plin|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_proj_plin',  next(stg_list), r'\verb|Im pinv_proj_plin|', imag=True)
    edotline_x_y_h5 ('k_lin_sq', 'pinv_perp_plin',  next(stg_list), r'\verb|pinv_perp_plin|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_perp',       next(stg_list), r'\verb|pinv_perp|')

    # vec-cmp
    edotline_x_y_h5 ('k_lin_sq', 'pinv_plin_rho',   next(stg_list), r'\verb|pinv_plin_rho|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_plin_cos',   next(stg_list), r'\verb|pinv_plin_cos|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_plin_phi',   next(stg_list), r'\verb|pinv_plin_phi|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_psin_rho',   next(stg_list), r'\verb|pinv_psin_rho|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_psin_cos',   next(stg_list), r'\verb|pinv_psin_cos|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_psin_phi',   next(stg_list), r'\verb|pinv_psin_phi|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_psym_rho',   next(stg_list), r'\verb|pinv_psym_rho|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_psym_cos',   next(stg_list), r'\verb|pinv_psym_cos|')
    edotline_x_y_h5 ('k_lin_sq', 'pinv_psym_phi',   next(stg_list), r'\verb|pinv_psym_phi|')
    
    ax.legend(numpoints=1)

    return ax

def show_invprop_cmp_axes(ensc, axis_list_k, what_k, h5grp, 
            ax=None, stg_list=None, imag=False) :
    if None is stg_list: stg_list = it.cycle(dv.style_group_default)
    if None is ax : ax = dv.make_std_axes()

    def edotline_x_y_h5(h5grp, x_key, y_key, stg, label=None) :
        rsplan  = pickle.loads(h5grp.attrs['rsplan'])
        x       = h5grp[x_key].value
        y       = h5grp[y_key].value
        if imag :   y = y.imag
        else :      y = y.real
        ya, ye  = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(x, ya, yerr=ye, label=label, **stg.edotline())
    
    def edotline_x_xy_h5(h5grp, x_key, y_key, stg, label=None) :
        rsplan  = pickle.loads(h5grp.attrs['rsplan'])
        x       = h5grp[x_key].value
        y       = h5grp[y_key].value
        if imag :   y = y.imag
        else :      y = y.real
        ya, ye  = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(x, x*ya, yerr=x*ye, label=label, **stg.edotline())
    
    for axis_k in axis_list_k :
        h5g = h5grp[axis_k]
        if 'prop' == what_k[:4] :
            edotline_x_xy_h5(h5grp[axis_k], 'k_lin_sq', what_k, next(stg_list), r'\verb|%s|' % axis_k)
        elif 'pinv' == what_k[:4] :
            edotline_x_y_h5(h5grp[axis_k], 'k_lin_sq', what_k, next(stg_list), r'\verb|%s|' % axis_k)
        else:
            raise ValueError(what_k)

    ax.legend(numpoints=1)

    return ax

def runme_show_invprop_comp(ensc, ama_mode_list, h5g_top, 
            pmax=12, rsplan=('jk',1), VERBOSE=True, 
            do_calc=True, do_cmp_comp=True, do_cmp_axes=True,
            meas_slice=slice(None),
            out_dir='.') :

    # TODO separate do_calc from do_cmp_comp
    """ print all useful combinations of ama_mode(s) and mom_list(s) """
    def do_show_cmp_comp(mom_list, mom_name, ama_mode) :
        
        h5g = h5g_top.require_group('%s/%s' % (ama_mode, mom_name))
        out_file = '%s/prop-comp_%s_%s' % (out_dir, ama_mode, mom_name)
        if do_calc :
            calc_invprop_comp(ensc, mom_list, ama_mode=ama_mode, 
                        h5grp=h5g, VERBOSE=VERBOSE, rsplan=rsplan, meas_slice=meas_slice)
        if do_cmp_comp : 
            ax = show_invprop_comp(ensc, h5g, ax=dv.class_logger(dv.make_std_axes()))
            ax.figure.savefig(out_file + '.pdf')
            dv.save_figdraw(open(out_file + '.plt.py', 'w'), ax=ax)
            dv.close_fig(ax.figure)
            if (VERBOSE) :
                print('%s  %s  ->  %s' % (ama_mode, mom_name, out_file))


    if do_cmp_comp or do_calc : 
        for ama_mode in ama_mode_list :
            do_show_cmp_comp([[x,0,0,  0] for x in np.r_[-pmax:pmax]], 'axis-x',     ama_mode)
            do_show_cmp_comp([[0,x,0,  0] for x in np.r_[-pmax:pmax]], 'axis-y',     ama_mode)
            do_show_cmp_comp([[0,0,x,  0] for x in np.r_[-pmax:pmax]], 'axis-z',     ama_mode)
            do_show_cmp_comp([[0,0,0,2*x] for x in np.r_[-pmax:pmax]], 'axis-t',     ama_mode)
            do_show_cmp_comp([[x,x,0,  0] for x in np.r_[-pmax:pmax]], 'diag-xy',    ama_mode)
            do_show_cmp_comp([[x,0,x,  0] for x in np.r_[-pmax:pmax]], 'diag-xz',    ama_mode)
            do_show_cmp_comp([[x,0,0,2*x] for x in np.r_[-pmax:pmax]], 'diag-xt',    ama_mode)
            do_show_cmp_comp([[0,x,x,  0] for x in np.r_[-pmax:pmax]], 'diag-yz',    ama_mode)
            do_show_cmp_comp([[0,x,0,2*x] for x in np.r_[-pmax:pmax]], 'diag-yt',    ama_mode)
            do_show_cmp_comp([[0,0,x,2*x] for x in np.r_[-pmax:pmax]], 'diag-zt',    ama_mode)
            do_show_cmp_comp([[x,x,x,  0] for x in np.r_[-pmax:pmax]], 'diag-xyz',   ama_mode)
            do_show_cmp_comp([[x,x,0,2*x] for x in np.r_[-pmax:pmax]], 'diag-xyt',   ama_mode)
            do_show_cmp_comp([[x,0,x,2*x] for x in np.r_[-pmax:pmax]], 'diag-xzt',   ama_mode)
            do_show_cmp_comp([[0,x,x,2*x] for x in np.r_[-pmax:pmax]], 'diag-yzt',   ama_mode)
            do_show_cmp_comp([[x,x,x,2*x] for x in np.r_[-pmax:pmax]], 'diag-xyzt',  ama_mode)

    def do_show_cmp_axes(axis_list_k, what_k, ama_mode, imag=False) :
        if imag : out_file = '%s/prop-axes-cmp_%s_%s_im' % (out_dir, ama_mode, what_k)
        else    : out_file = '%s/prop-axes-cmp_%s_%s' % (out_dir, ama_mode, what_k)
        ax = show_invprop_cmp_axes(ensc, axis_list_k, what_k, h5g_top[ama_mode], 
                    ax=dv.class_logger(dv.make_std_axes()), imag=imag)
        ax.figure.savefig(out_file + '.pdf')
        dv.save_figdraw(open(out_file + '.plt.py', 'w'), ax=ax)
        dv.close_fig(ax.figure)
        if (VERBOSE) :
            print('%s  %s  ->  %s' % (ama_mode, what_k, out_file))


    # TODO make axis(&name) generation DRY
    axis_list_k = [ 'axis-x',   'axis-y',   'axis-z',   'axis-t',   
                    'diag-xy',  'diag-xz',  'diag-xt',  'diag-yz',  
                    'diag-yt',  'diag-zt',  'diag-xyz', 'diag-xyt', 
                    'diag-xzt', 'diag-yzt', 'diag-xyzt' ]
    if do_cmp_axes :
        for ama_mode in ama_mode_list :

            do_show_cmp_axes(axis_list_k, 'prop_mp',        ama_mode)
            do_show_cmp_axes(axis_list_k, 'prop_mp',        ama_mode, imag=True)
            do_show_cmp_axes(axis_list_k, 'prop_proj_plin', ama_mode)
            do_show_cmp_axes(axis_list_k, 'prop_proj_plin', ama_mode, imag=True)
            do_show_cmp_axes(axis_list_k, 'prop_perp_plin', ama_mode)
            do_show_cmp_axes(axis_list_k, 'prop_perp',      ama_mode)

            do_show_cmp_axes(axis_list_k, 'pinv_mp',        ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_mp',        ama_mode, imag=True)
            do_show_cmp_axes(axis_list_k, 'pinv_proj_plin', ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_proj_plin', ama_mode, imag=True)
            do_show_cmp_axes(axis_list_k, 'pinv_perp_plin', ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_perp',      ama_mode)
            
            do_show_cmp_axes(axis_list_k, 'pinv_plin_rho',  ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_plin_cos',  ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_plin_phi',  ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_psin_rho',  ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_psin_cos',  ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_psin_phi',  ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_psym_rho',  ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_psym_cos',  ama_mode)
            do_show_cmp_axes(axis_list_k, 'pinv_psym_phi',  ama_mode)


"""Usage example
runme_show_invprop_comp(ens_cfg_48c96pt, ['slonly', 'unbias', 'exonly'], h5npr.require_group('prop_comp'), rsplan=('jk', 1))
runme_show_invprop_comp(ens_cfg_48c96pt, ['unbias'], h5npr.require_group('prop_comp'), rsplan=('jk', 1), VERBOS
E=True, out_dir='prop.st20-redoneBC')
"""

def calc_gaugeprop_comp(ensc, mom_list, 
        rsplan=('jk',1), VERBOSE=False, h5grp=None, meas_slice=slice(None),
        avg_equal_momsq=False) :
    ls          = ensc.latsize
    tw          = ensc.gauge_twist
    
    ls_half     = ls / 2
    def mom_center(c) : return ((c + ls_half) % ls - ls_half)

    sqrt3inv = math.sqrt(1./3)
    gm_matr = np.array([
        [ [  0,  1,  0 ], [  1,  0,  0 ], [  0,  0,  0 ] ],
        [ [  0,-1j,  0 ], [ 1j,  0,  0 ], [  0,  0,  0 ] ],
        [ [  1,  0,  0 ], [  0, -1,  0 ], [  0,  0,  0 ] ],
        [ [  0,  0,  1 ], [  0,  0,  0 ], [  1,  0,  0 ] ],
        [ [  0,  0,-1j ], [  0,  0,  0 ], [ 1j,  0,  0 ] ],
        [ [  0,  0,  0 ], [  0,  0,  1 ], [  0,  1,  0 ] ],
        [ [  0,  0,  0 ], [  0,  0,-1j ], [  0, 1j,  0 ] ],
        [ [ sqrt3inv, 0, 0 ], [ 0, sqrt3inv, 0 ], [ 0, 0, -2*sqrt3inv ] ] ], 
        dtype=np.complex)
    
    # [i_mom, mu]
    mom_list    = np.asarray(mom_list)
    mom_list_r  = np.array([ inv_wavevec(wv, ls, tw) for wv in mom_list ])
    n_mom       = len(mom_list)
    
    # build proj matr wrt mom
    # [i_mom, mu]
    # XXX sic! replaced lin with sin; have to do both separately
    klin    = calc_mom_sin(mom_list, ls, tw)           
    klin_r  = calc_mom_sin(mom_list_r, ls, tw)
    klin_sum    = klin + klin_r
    lat_d   = len(ls)
    klinsq  = (klin*klin).sum(-1)
    klinsq_nonzero  = np.where(0 < klinsq, klinsq, 1.)
    k_n_lin = klin / np.sqrt(klinsq_nonzero)[..., None]
    
    # [i_data, i_mom, mu, i, j]
    # XXX cannot resample because there is variation due to residual global symmetry
    gft_33  = ensc.get_gauge_ft(mom_list, VERBOSE=VERBOSE, meas_slice=meas_slice)   
    gft_33_r= ensc.get_gauge_ft(mom_list_r, VERBOSE=VERBOSE, meas_slice=meas_slice) 

    
    # TODO fix qlua gauge_ft : remove 1/2-link phase ; do all phase adjustments in Python
    # XXX hotfix for 1/2-link phase : remove the phase adjustment done in Qlua
    # XXX commented out because gauge links are now saved without half-integer coordinate phase
    #ph_half     = np.exp(1j*math.pi*np.array(mom_list % ls + tw, float) / ls)
    #gft_33      = np.einsum('abcij,bc->abcij', gft_33, ph_half.conj()) 
    #ph_half_r   = np.exp(1j*math.pi*np.array(mom_list_r % ls + tw, float) / ls)
    #gft_33_r    = np.einsum('abcij,bc->abcij', gft_33_r, ph_half_r.conj())

    # 1/2-link phase adjustment 
    # XXX the range half-momentum p/2 must be "centered" around (0,0,0,0) 
    # to avoid discontinuity at p=0(===2pi)
    ph_ch       = np.exp(1j * math.pi * np.array(mom_center(mom_list) + tw, float) / ls)
    gft_33      = np.einsum('abcij,bc->abcij', gft_33, ph_ch) 
    ph_ch_r     = np.exp(1j * math.pi * np.array(mom_center(mom_list_r) + tw, float) / ls)
    gft_33_r    = np.einsum('abcij,bc->abcij', gft_33_r, ph_ch_r) 


    # [i_data, i_mom, mu, a] projectons on Gell-Mann matrices TODO enforce Tr=0?
    gft_8   = np.einsum('abcij,nji->abcn', gft_33, gm_matr)
    gft_8_r = np.einsum('abcij,nji->abcn', gft_33_r, gm_matr)
    # [i_data, i_mom, a] longitudinal proj
    gft_8_L = np.einsum('abcn,bc->abn', gft_8, k_n_lin)
    gft_8_r_L   = np.einsum('abcn,bc->abn', gft_8_r, k_n_lin)
    # [i_data, i_mom, mu, a] transversal proj
    gft_8_T = gft_8 - np.einsum('abn,bc->abcn', gft_8_L, k_n_lin)
    gft_8_r_T   = gft_8_r - np.einsum('abn,bc->abcn', gft_8_r_L, k_n_lin)

    # longitudinal prop
    gprop_long  = np.einsum('abm,abn->abmn', gft_8_L, gft_8_r_L) / ensc.latvolume
    gprop_long  = lhpd.resample(gprop_long, rsplan)
    # transverse prop
    gprop_trans = np.einsum('abcm,abcn->abmn', gft_8_T, gft_8_r_T) / (lat_d - 1) / ensc.latvolume
    gprop_trans = lhpd.resample(gprop_trans, rsplan)
    # everything else
    gprop_resid = ( np.einsum('abcm,abn,be->abcemn', gft_8_T, gft_8_r_L, k_n_lin)
                  + np.einsum('abm,aben,bc->abcemn', gft_8_L, gft_8_r_T, k_n_lin) 
                  ) / ensc.latvolume
    gprop_resid = lhpd.resample(gprop_resid, rsplan)

    if avg_equal_momsq :
        # build equivalence matrix
        eq_ind, eq_list = classify_list(klinsq, 
                cmp=cmp_with_tol(adiff=1e-6))
        n_mom_new = len(eq_list)
        mom_proj = np.zeros((n_mom_new, n_mom), float)
        for i, j in enumerate(eq_ind) :
            mom_proj[j, i] = 1.
        
        # apply the projector
        mom_proj /= mom_proj.sum(-1)[:, None]   # normalize
        gprop_long  = np.einsum('bx,axmn->abmn', mom_proj, gprop_long)
        gprop_trans = np.einsum('bx,axmn->abmn', mom_proj, gprop_trans)
        gprop_resid = np.einsum('bx,axcemn->abcemn', mom_proj, gprop_resid)

        # select mom_list representatives
        mom_list    = np.array([ mom_list[eq_ind.index(i)]
                                 for i in range(n_mom_new) ])
        mom_list_r  = np.array([ inv_wavevec(wv, ls, tw) for wv in mom_list ])
        n_mom       = len(mom_list)
    else :
        mom_i       = klinsq.argsort()
        mom_list    = mom_list[mom_i]
        mom_list_r  = mom_list_r[mom_i]
        gprop_long  = gprop_long[:, mom_i]
        gprop_trans = gprop_trans[:, mom_i]
        gprop_resid = gprop_resid[:, mom_i]


    def col_proj(a) :
        """ project on trace and |rest|, using the last two indices """
        a       = np.asarray(a)
        a_sh    = a.shape
        a_d     = a_sh[-1]
        assert(a_sh[-2] == a_d)
        a_tr    = np.einsum('...aa', a) / a_d
        a_rest  = a - a_tr[..., None, None] * np.identity(a_d)
        a_rest  = a_rest.reshape(a_sh[:-2] + (a_d*a_d,))
        a_rest  = np.sqrt((a_rest * a_rest.conj()).real.sum(-1))
        return a_tr, a_rest

    # [i_data, i_mom]
    gprop_trans_cdiag, gprop_trans_coffd    = col_proj(gprop_trans)
    # [i_data, i_mom]
    gprop_long_cdiag, gprop_long_coffd      = col_proj(gprop_long)
    # [i_data, i_mom, mu]
    gprop_resid_cdiag, gprop_resid_coffd    = col_proj(gprop_resid)
    gprop_resid_cdiag   = np.sqrt((gprop_resid_cdiag * gprop_resid_cdiag.conj()).real.sum(-1).sum(-1))
    gprop_resid_coffd   = np.sqrt((gprop_resid_coffd * gprop_resid_coffd.conj()).real.sum(-1).sum(-1))
    
    if not None is  h5grp :
        h5_purge_keys(h5grp, [  'mom_list', 
                    'k_lin', 'k_lin_sq', 
                    'k_sin', 'k_sin_sq',
                    'prop_trans_cdiag', 'prop_trans_coffd',
                    'prop_long_cdiag',  'prop_long_coffd', 
                    'prop_resid_cdiag', 'prop_resid_coffd',
                ])
        h5grp.attrs['rsplan']   = pickle.dumps(rsplan)

        h5grp['mom_list']       = mom_list
        h5grp['k_lin']          = calc_mom_lin(mom_list, ls, tw)
        h5grp['k_lin_sq']       = calc_momsq_lin(mom_list, ls, tw)
        h5grp['k_sin']          = calc_mom_sin(mom_list, ls, tw)
        h5grp['k_sin_sq']       = calc_momsq_sin(mom_list, ls, tw)
        h5grp['prop_trans_cdiag']   = gprop_trans_cdiag
        h5grp['prop_trans_coffd']   = gprop_trans_coffd
        h5grp['prop_long_cdiag']    = gprop_long_cdiag
        h5grp['prop_long_coffd']    = gprop_long_coffd
        h5grp['prop_resid_cdiag']   = gprop_resid_cdiag
        h5grp['prop_resid_coffd']   = gprop_resid_coffd

"""Usage
calc_gaugeprop_comp(ens_cfg_48c96pt, [[x,0,0,0] for x in np.r_[-5:5]], h5grp=h5npr.require_group('/glueprop_comp'), VERBOSE=True, meas_slice=np.s_[:10])
"""



def show_gaugeprop_comp(ensc, h5grp, x_k, ax=None, stg_list=None) :
    if None is stg_list: stg_list = it.cycle(dv.style_group_default)
    if None is ax : ax = dv.make_std_axes()
    
    rsplan = pickle.loads(h5grp.attrs['rsplan'])
    
    def edotline_x_y_h5(x_key, y_key, stg, label=None, imag=False) :
        x       = h5grp[x_key].value
        y       = h5grp[y_key].value
        if imag :   y = y.imag
        else :      y = y.real
        ya, ye  = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(x, ya, yerr=ye, label=label, **stg.edotline())
    
    def edotline_x_xy_h5(x_key, y_key, stg, label=None, imag=False) :
        x       = h5grp[x_key].value
        y       = h5grp[y_key].value
        if imag :   y = y.imag
        else :      y = y.real
        ya, ye  = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(x, x*ya, yerr=x*ye, label=label, **stg.edotline())
    
    edotline_x_xy_h5(x_k, 'prop_trans_cdiag', next(stg_list), r'\verb|prop_trans_cdiag|')
    edotline_x_xy_h5(x_k, 'prop_trans_cdiag', next(stg_list), r'\verb|Im prop_trans_cdiag|', imag=True)
    edotline_x_xy_h5(x_k, 'prop_trans_coffd', next(stg_list), r'\verb|prop_trans_coffd|')
    edotline_x_xy_h5(x_k, 'prop_long_cdiag', next(stg_list), r'\verb|prop_long_cdiag|')
    edotline_x_xy_h5(x_k, 'prop_long_cdiag', next(stg_list), r'\verb|Im prop_long_cdiag|', imag=True)
    edotline_x_xy_h5(x_k, 'prop_long_coffd', next(stg_list), r'\verb|prop_long_coffd|')
    edotline_x_xy_h5(x_k, 'prop_resid_cdiag', next(stg_list), r'\verb|prop_resid_cdiag|')
    edotline_x_xy_h5(x_k, 'prop_resid_coffd', next(stg_list), r'\verb|prop_resid_coffd|')

    ax.legend(numpoints=1)
    ax.set_xlabel(r'\verb|%s|' % x_k)

    return ax

def show_gaugeprop_cmp_axes(ensc, axis_list_k, x_k, what_k, h5grp,
            ax=None, stg_list=None, imag=False) :
    if None is stg_list: stg_list = it.cycle(dv.style_group_default)
    if None is ax : ax = dv.make_std_axes()

    def edotline_x_y_h5(h5grp, x_key, y_key, stg, label=None) :
        rsplan  = pickle.loads(h5grp.attrs['rsplan'])
        x       = h5grp[x_key].value
        y       = h5grp[y_key].value
        if imag :   y = y.imag
        else :      y = y.real
        ya, ye  = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(x, ya, yerr=ye, label=label, **stg.edotline())
    
    def edotline_x_xy_h5(h5grp, x_key, y_key, stg, label=None) :
        rsplan  = pickle.loads(h5grp.attrs['rsplan'])
        x       = h5grp[x_key].value
        y       = h5grp[y_key].value
        if imag :   y = y.imag
        else :      y = y.real
        ya, ye  = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(x, x*ya, yerr=x*ye, label=label, **stg.edotline())
    
    for axis_k in axis_list_k :
        h5g = h5grp[axis_k]
        edotline_x_xy_h5(h5grp[axis_k], x_k, what_k, next(stg_list), r'\verb|%s|' % axis_k)

    ax.legend(numpoints=1)
    ax.set_xlabel(r'\verb|%s|' % x_k)

    return ax


def runme_show_gaugeprop_comp(ensc, h5g_top, 
            pmax=11, rsplan=('jk',1), VERBOSE=True, 
            do_calc=True, do_cmp_comp=True, do_cmp_axes=True,
            avg_equal_momsq=False,
            meas_slice=slice(None),
            out_dir='.') :

    # TODO separate do_calc from do_cmp_comp
    """ print all useful combinations of ama_mode(s) and mom_list(s) """
    def do_show_cmp_comp(mom_list, mom_name) :
        
        h5g = h5g_top.require_group(mom_name)
        out_file = '%s/gaugeprop-comp_%s' % (out_dir, mom_name)
        if do_calc :
            calc_gaugeprop_comp(ensc, mom_list, 
                        h5grp=h5g, VERBOSE=VERBOSE, rsplan=rsplan, meas_slice=meas_slice,
                        avg_equal_momsq=avg_equal_momsq)
        if do_cmp_comp :
            x_k = 'k_sin_sq'
            ax = show_gaugeprop_comp(ensc, h5g, x_k, ax=dv.class_logger(dv.make_std_axes()))
            ax.figure.savefig(out_file + '.pdf')
            dv.save_figdraw(open(out_file + '.plt.py', 'w'), ax=ax)
            dv.close_fig(ax.figure)
            if (VERBOSE) :
                print('%s  ->  %s' % (mom_name, out_file))


    # TODO make axis(&name) generation DRY
    if do_cmp_comp or do_calc : 
        
        # complete reflection of momenta does not add to statistics, since G(p) = <A(p)A(-p)>
        pp  = np.array([ [x,x,x,2*x] for x in range(pmax+1) ])
        N   = None

        do_show_cmp_comp(pp * [1,0,0,0], 'axis-x')
        do_show_cmp_comp(pp * [0,1,0,0], 'axis-y')
        do_show_cmp_comp(pp * [0,0,1,0], 'axis-z')
        do_show_cmp_comp(pp * [0,0,0,1], 'axis-t')
        
        xy  = 1 - 2 * np.array(list(np.ndindex((2,1,1,1)))) 
        xy  = xy[:,N,:] * [1,1,0,0]
        do_show_cmp_comp((pp * xy[..., [0,1,2,3]]).reshape(-1,4), 'diag-xy')
        do_show_cmp_comp((pp * xy[..., [0,2,1,3]]).reshape(-1,4), 'diag-xz')
        do_show_cmp_comp((pp * xy[..., [0,2,3,1]]).reshape(-1,4), 'diag-xt')
        do_show_cmp_comp((pp * xy[..., [2,0,1,3]]).reshape(-1,4), 'diag-yz')
        do_show_cmp_comp((pp * xy[..., [2,0,3,1]]).reshape(-1,4), 'diag-yt')
        do_show_cmp_comp((pp * xy[..., [2,3,0,1]]).reshape(-1,4), 'diag-zt')
        

        xyz = 1 - 2 * np.array(list(np.ndindex((2,2,1,1))))
        xyz = xyz[:,None,:] * [1,1,1,0] 
        do_show_cmp_comp((pp * xyz[..., [0,1,2,3]]).reshape(-1,4), 'diag-xyz')
        do_show_cmp_comp((pp * xyz[..., [0,1,3,2]]).reshape(-1,4), 'diag-xyt')
        do_show_cmp_comp((pp * xyz[..., [0,3,1,2]]).reshape(-1,4), 'diag-xzt')
        do_show_cmp_comp((pp * xyz[..., [3,0,1,2]]).reshape(-1,4), 'diag-yzt')
        

        xyzt= 1 - 2 * np.array(list(np.ndindex((2,2,2,1))))
        xyzt= xyzt[:,None,:]
        do_show_cmp_comp((pp * xyzt).reshape(-1,4), 'diag-xyzt')
        
    
    def do_show_cmp_axes(axis_list_k, what_k, imag=False) :
        if imag : out_file = '%s/gaugeprop-axes-cmp_%s_im' % (out_dir, what_k,)
        else    : out_file = '%s/gaugeprop-axes-cmp_%s' % (out_dir, what_k,)
        ax = show_gaugeprop_cmp_axes(ensc, axis_list_k, 
                    'k_sin_sq', what_k, h5g_top, 
                    ax=dv.class_logger(dv.make_std_axes()), imag=imag)
        ax.figure.savefig(out_file + '.pdf')
        dv.save_figdraw(open(out_file + '.plt.py', 'w'), ax=ax)
        dv.close_fig(ax.figure)
        if (VERBOSE) :
            print('%s  ->  %s' % (what_k, out_file))


    axis_list_k = [ 'axis-x',   'axis-y',   'axis-z',   'axis-t',   
                    'diag-xy',  'diag-xz',  'diag-xt',  'diag-yz',  
                    'diag-yt',  'diag-zt',  
                    'diag-xyz', 'diag-xyt', 'diag-xzt', 'diag-yzt', 
                    'diag-xyzt',
                    ]
    if do_cmp_axes :
        do_show_cmp_axes(axis_list_k, 'prop_trans_cdiag')
        do_show_cmp_axes(axis_list_k, 'prop_trans_cdiag', imag=True)
        do_show_cmp_axes(axis_list_k, 'prop_trans_coffd')
        do_show_cmp_axes(axis_list_k, 'prop_long_cdiag')
        do_show_cmp_axes(axis_list_k, 'prop_long_cdiag', imag=True)
        do_show_cmp_axes(axis_list_k, 'prop_long_coffd')
        do_show_cmp_axes(axis_list_k, 'prop_resid_cdiag')
        do_show_cmp_axes(axis_list_k, 'prop_resid_coffd')

def make_alldiag_vec(pmax) :
    """ all true diagonals """

    res = []

    pp  = np.array([ [x,x,x,x] for x in range(-pmax, pmax+1) ])
    N   = None

    res.extend(pp * [1,0,0,0])
    res.extend(pp * [0,1,0,0])
    res.extend(pp * [0,0,1,0])
    res.extend(pp * [0,0,0,1])
    
    xy  = 1 - 2 * np.array(list(np.ndindex((2,1,1,1)))) 
    xy  = xy[:,N,:] * [1,1,0,0]
    res.extend((pp * xy[..., [0,1,2,3]]).reshape(-1,4)) #xy
    res.extend((pp * xy[..., [0,2,1,3]]).reshape(-1,4)) #xz
    res.extend((pp * xy[..., [0,2,3,1]]).reshape(-1,4)) #xt
    res.extend((pp * xy[..., [2,0,1,3]]).reshape(-1,4)) #yz
    res.extend((pp * xy[..., [2,0,3,1]]).reshape(-1,4)) #xt
    res.extend((pp * xy[..., [2,3,0,1]]).reshape(-1,4)) #zt

    xyz = 1 - 2 * np.array(list(np.ndindex((2,2,1,1)))) 
    xyz = xyz[:,None,:] * [1,1,1,0]
    res.extend((pp * xyz[..., [0,1,2,3]]).reshape(-1,4)) #xyz
    res.extend((pp * xyz[..., [0,1,3,2]]).reshape(-1,4)) #xyt
    res.extend((pp * xyz[..., [0,3,1,2]]).reshape(-1,4)) #xzt
    res.extend((pp * xyz[..., [3,0,1,2]]).reshape(-1,4)) #yzt
    
    xyzt= 1 - 2 * np.array(list(np.ndindex((2,2,2,1))))
    xyzt= xyzt[:,None,:]
    res.extend((pp * xyzt).reshape(-1,4)) #xyzt
    
    res = list(set([ tuple(x) for x in res ]))
    res.sort()
    return np.array(res)


