from .npr_common import *

import aff
import numpy as np
import math
from ..pymath.gamma_matr import Gamma16_dgr
from ..pymath import H4_tensor as h4t
from ..misc import rdiff

def aff_lpath2str(lpath, cancel=True, 
              frw_link=['x', 'y', 'z', 't'], 
              bkw_link=['X','Y','Z','T']):
    """ convert an array of links `lpath' into a string
        lpath   1-D sequence: [0; LDIM) -> frw_link, [LDIM; 2*LDIM) -> bkw_link
        cancel  whether to remove adjacent opposite links
        frw_link, bkw_link: link characters
    """
    print("WARNING obsolete? look at misc/strkey.py")
    assert ((len(frw_link) == LDIM) and (len(bkw_link) == LDIM))
    if len(lpath) <= 0: return ''
    s_link  = frw_link + bkw_link
    l       = lpath[0]
    assert (0 <= l and l < 2*LDIM)
    res = s_link[l]
    for i in range(1, len(lpath)):
        l   = lpath[i]
        assert (0 <= l and l < 2*LDIM)
        if (cancel):
            lprev = s_link.index(res[-1])
            if (abs(l - lprev) == LDIM):
                res = res[:-1]
                continue
        res += s_link[l]
    return res



def aff_read_npr_2qvertex(aff_r, aff_kpath, rank):
    """ read NPR 2-quark vertex building blocks 
        return: array(shape=[NSPIN**2, 2*LDIM x rank, NCOLOR, NSPIN, NCOLOR, NSPIN], 
                      dtype=complex) 
                [i_Gamma, 
                 {forward link(xyzt): 0 .. LDIM-1 
                  + backward link(XYZT): LDIM + 2*LDIM-1} x rank,
                 ic, is, jc, js]
        
    """

    res = np.empty(shape=(NSPIN*NSPIN,) + (2*LDIM,)*rank + (NCOLOR,NSPIN,NCOLOR,NSPIN), 
                   dtype=np.complex128)
    for dim_idx in np.ndindex((2*LDIM,)*rank):
        lpath_str   = aff_lpath2str(dim_idx)
        for iG in range(NSPIN*NSPIN):
            res[(iG,) + dim_idx].flat = aff_r.read(
                        aff_kpath + ('/l%d_%s/g%d' %(len(lpath_str), lpath_str, iG)))

    return res

def aff_read_npr_prop(aff_r, aff_kpath):
    """ read npr propagator
        return: array(shape=[NCOLOR, NSPIN, NCOLOR, NSPIN], 
                      dtype=complex)
                [ic, is, jc, js]
    """
    res = np.empty(shape=(NCOLOR,NSPIN,NCOLOR,NSPIN), dtype=np.complex128)
    res.flat = aff_r.read(aff_kpath)
    return res

def calc_bb2dsymm_momproj(v, idx_list, mom, latsize):
    """ 
        v       momentum-projected building blocks == v(mom) in the next line
        mom     momentum used in mom.projection (sic: note the minus sign!): 
                v(mom) = \sum_x exp(-i*mom.x) v(x)
        latsize (hyper)cubic lattice size
    """

    sh_len = len(v.shape)
    exp_ip1 = np.exp(2j * math.pi * np.asarray(mom, dtype=float) / latsize)
    for idx in idx_list:
        assert ((0 <= idx) and (v.shape[idx] == 2*LDIM))
        # init correct slices
        sl      = [np.s_[:]] * sh_len
        sl_frw  = list(sl);             sl_frw[idx] = np.s_[LDIM:2*LDIM]
        sl_bkw  = list(sl);             sl_bkw[idx] = np.s_[0:LDIM]
        sl_mom  = [None] * sh_len;      sl_mom[idx] = np.s_[:]
        v1      = (v[sl_frw] * (1. + exp_ip1.conj()) - v[sl_bkw] * (1. + exp_ip1)) / 4.
        v       = v1
    return v

# TODO read all meas for vertices
# TODO calc all derivs
# TODO read all meas for props
# TODO resample vertices and props
# TODO calc Gamma = S^-1 . G. S^-1
# TODO extract representations: 1) select gamma(s); 2) calc tensors
# TODO project onto correct gamma structure & print

def tensor_linsolver(rhs, sigma):
    """ calculate least-squares decomposition 
            rhs[{I}] = \sum_i x[i] * sigma[i, {I}] + C[{i}]
        solution for x is x = [sigma[j]^\dag . sigma[i]]^{-1} . sigma[j] . rhs
        return:     (x[i], C[{I}])
    """
    rhs     = np.asarray(rhs)
    sigma   = np.asarray(sigma)

    assert (sigma.shape[1:] == rhs.shape)
    len_I   = len(rhs.shape)
    rhs_I   = range(len_I)
    sigma_I = range(1, 1 + len_I)
    sys = np.tensordot(sigma.conj(), sigma, [sigma_I, sigma_I])
    inv_sys = np.linalg.inv(sys)
    sigma_rhs = np.tensordot(sigma.conj(), rhs, [sigma_I, rhs_I])
    x       = np.dot(inv_sys, sigma_rhs)
    resid   = rhs - np.tensordot(sigma, x, (0,0))
    return x, resid

def norm_tr_pinv(a, tol=1e-8):
    """ compute pseudo-inv using SVD and divide it by the number 
        of non-zero elements (rank) to normalize the trace
    """
    a   = np.asarray(a)
    m   = a.shape[0]
    assert(a.shape == (m,m))
    u,s,v   = np.linalg.svd(a)
    #print(s)
    nz_cnt  = 0
    for i in range(m):
        if tol <= abs(s[i]): 
            s[i] = 1. / s[i]
            nz_cnt +=1
        else: s[i] = 0.
    if 0 == nz_cnt: return np.zeros((m,m), a.dtype), 0
    else: return np.dot(v.conj().T * s, u.conj().T), nz_cnt
    
    
def vec_avg_linsolver(rhs, sigma):
    rhs     = np.asarray(rhs)
    sigma   = np.asarray(sigma)
    

    assert(rhs.shape == sigma.shape)
    assert(rhs.shape[1:] == (NSPIN, NSPIN))
    n_elem  = rhs.shape[0]
    
    #if True:
    if False:
        x = np.array([ np.dot(rhs[i_elem], 
                              np.linalg.pinv(sigma[i_elem])).trace() / NSPIN
                       for i_elem in range(n_elem) ])
        #print("sum(nzc)=", n_elem*NSPIN)
        return x.sum() / n_elem
    else:
        xsum = 0.
        nz_cnt = 0
        for i_elem in range(n_elem):
            s_pinv, nzc = norm_tr_pinv(sigma[i_elem])
            xsum    += np.dot(rhs[i_elem], s_pinv).trace()
            nz_cnt  += nzc
            #print("nzc=", nzc, "  rdiff=", rdiff(s_pinv, np.linalg.pinv(sigma[i_elem])))
        #print("sum(nzc)=", nz_cnt)
        return xsum / nz_cnt

def vec_avg_linsolver_arr(rhs, sigma):
    rhs     = np.asarray(rhs)
    sigma   = np.asarray(sigma)
    x       = np.array([ vec_avg_linsolver(rhs, s)
                         for s in sigma ])
    resid   = rhs - np.tensordot(sigma, x, (0,0))
    return x, resid

def mom_linear(mom, latsize):
    return 2 * math.pi * np.array(mom, dtype=float) / latsize
def momsq_linear(mom, latsize): return (mom_linear(mom, latsize)**2).sum()

def mom_sine(mom, latsize):
    return np.sin(2 * math.pi * np.array(mom, dtype=float) / latsize)
def momsq_sine(mom, latsize): return (mom_sine(mom, latsize)**2).sum()

def inv_dirprop(v):
    assert (v.shape == (NCOLOR, NSPIN, NCOLOR, NSPIN))
    return np.linalg.inv(v.reshape((NCOLOR*NSPIN, NCOLOR*NSPIN))).reshape(
                                (NCOLOR, NSPIN, NCOLOR, NSPIN))

def amp_q2vertex_useinv(v, inv_frw, inv_bkw):
    """ 
        v       [..., c, s, c, s]
        inv_frw, inv_bkw
                [c, s, c, s]
    """
    assert (inv_frw.shape == (NCOLOR, NSPIN, NCOLOR, NSPIN))
    assert (inv_bkw.shape == (NCOLOR, NSPIN, NCOLOR, NSPIN))
    assert (v.shape[-4:] == (NCOLOR, NSPIN, NCOLOR, NSPIN))
    # sic! watch index juggling
    return np.tensordot(np.tensordot(v, inv_bkw, ((-4,-3),(2,3))), 
                        inv_frw, ((-4,-3),(0,1)))
    
def amp_q2vertex(v, frw_prop, bkw_prop):
    """ 
        d2      [..., c, s, c, s]
        frw_prop, bkw_prop
                [c, s, c, s]
    """
    return amp_q2vertex_useinv(v, inv_dirprop(frw_prop), inv_dirprop(bkw_prop))

def vertex_dirmat_g1d1(latmom, gamma):
    """ calculate dirac structures for 2-quark vertex with 1 deriv and 1 gamma matrix
        latmom  [mu] lattice momentum
        gamma   [mu, s1,s2] gamma matrices
        return  list of v[mu, nu, s1, s2]: dirac matrices

        NOTE    no symmetrization, trace subtraction, etc is done; result contains
                all possible representations
    """
    # gamma[mu][s1,s2] -> [mu, s1, s2]
    #gamma       = np.asarray([ Gamma16_dgr[1], Gamma16_dgr[2], Gamma16_dgr[4], Gamma16_dgr[8]])
    # p[mu] gamma[nu][s1,s2] -> [mu, nu, s1, s2]
    gamma       = np.asarray(gamma)
    mom_gamma   = latmom[:,N,N,N] * gamma[N,:,:,:]
    # p[mu] gamma[mu][s1,s2] -> [s1, s2]
    mom_gamma_dot = np.tensordot(latmom, gamma, (0, 0))
    # p^2
    latmom2     = (latmom**2).sum()
    return [ mom_gamma, 
             latmom[:,N,N,N] * latmom[N,:,N,N] / latmom2 * mom_gamma_dot[N,N,:,:] ]

# transform the first index from Gamma16 to a Lorentz tensor
def ith_(v, axis, k):
    v = np.asarray(v)
    sl = [np.s_[:]] * len(v.shape)
    sl[axis] = k
    return v[sl]
def G16_to_scalar(v, i=0): return ith_(v,i,0)
def G16_to_pseudoscalar(v, i=0): return ith_(v,i,15)
def G16_to_vec(v, i=0): 
    return np.array([ ith_(v,i,k) for k in (1,2,4,8) ])
def G16_to_axialvec(v, i=0): 
    return np.array([ +ith_(v,i,14), -ith_(v,i,13), +ith_(v,i,11), -ith_(v,i,7) ])
def G16_to_sigma(v, i=0): 
    res = np.empty((LDIM,LDIM) + v.shape[:i] + v.shape[i+1:], dtype=v.dtype)
    res[0,0], res[0,1], res[0,2], res[0,3] = (             0, +ith_(v,i, 3), +ith_(v,i, 5), +ith_(v,i, 9) )
    res[1,0], res[1,1], res[1,2], res[1,3] = ( -ith_(v,i, 3),             0, +ith_(v,i, 6), +ith_(v,i,10) )
    res[2,0], res[2,1], res[2,2], res[2,3] = ( -ith_(v,i, 5), -ith_(v,i, 6),             0, +ith_(v,i,12) )
    res[3,0], res[3,1], res[3,2], res[3,3] = ( -ith_(v,i, 9), -ith_(v,i,10), -ith_(v,i,12),             0 )
    return 1j*res

#def solve_2qvertex_T2sym(v2, h4repr, latmom, is_axial=False):
#    """ 
#        v2          [iG, mu, nu, c, s, c, s] amputated vertex
#        latmom      lattice momentum
#        is_axial    switch whether gamma5 is present
#    """
#    if not is_axial:
#        v2x     = np.asarray([+v2[1], +v2[2], +v2[4], +v2[8] ])
#        gamma   = np.asarray([+Gamma16_dgr[1], +Gamma16_dgr[2], +Gamma16_dgr[4], +Gamma16_dgr[8] ])
#    else:
#        v2x     = np.asarray([+v2[14], -v2[13], +v2[11], -v2[7] ])
#        gamma   = np.asarray([+Gamma16_dgr[1], -Gamma16_dgr[2], +Gamma16_dgr[4], -Gamma16_dgr[8] ])
#    
#    dirmat      = [ h4repr(d) for d in vertex_dirmat_g1d1(latmom, gamma) ]
#    rhs         = h4repr(v2)
#    return tensor_linsolver(rhs, dirmat)



####################################################################################################
# NEW CODE FOR ANALYZING NPR
####################################################################################################
def calc_qbarq_npr(qqbar_i, qqbar_f, gamma_list) :
    """ 
        qqbar_i, qqbar_f initial (incomping), final(outgoing) quark [i_data, ic,is,jc,js]
        
        qqbar_f . Gamma . gamma5 . qqbar_i^\dag . gamma5
    """
    gamma_list  = np.asarray(gamma_list)
    n_gamma     = len(gamma_list)
    assert(gamma_list.shape == (n_gamma, NSPIN, NSPIN))
    # Gamma[a] . gamma5 . qqbari[...]^dag . gamma5
    qqbar_i_rev = qqbar_rev(qqbar_i)
    # FIXME 3-matrix einsum is slow
    qqbar_gamma_qqbar = np.einsum('...abcd,...ide,...cefg->...iabfg', 
        qqbar_f, gamma_list, qqbar_i)
    return qqbar_gamma_qqbar

