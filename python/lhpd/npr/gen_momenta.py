import numpy as np

def gen_momenta_diag4d_tpos(klist, diag_factor) :
    """ treat t-direction differently : only positive 
        diag_factor : for anisotropic diags
    """
    ml  = []
    diag_factor = np.asarray(diag_factor)
    for k in klist :
        for i_s in np.ndindex(2,2,2) :   # alternate signs of x,y,z components
            mom_a   = diag_factor * k
            s       = 1 - 2*np.array(i_s)
            ml.append(mom_a * [s[0],s[1],s[2],1])
    return np.array(ml)

def gen_momenta_diag3d_tpos(klist, diag_factor=np.r_[1,1,1,1]) :
    """ treat t-direction differently : only positive 
        diag_factor : for anisotropic diags
    """
    ml  = []
    diag_factor = np.asarray(diag_factor)
    for k in klist :
        for i_s in np.ndindex(2,2) :   # alternate signs of x,y,z components
            mom_a   = diag_factor * k
            s       = 1 - 2*np.array(i_s)
            ml.extend([ mom_a * [s[0], s[1],    0, 1],
                        mom_a * [s[0],    0, s[1], 1], 
                        mom_a * [   0, s[0], s[1], 1]  ])
    return np.array(ml)

def gen_momenta_diag3to4d_tpos(klist, diag_factor=np.r_[1,1,1,1], dkmin=None) :
    """ 
        dkmin   max.deviation of components from the 4d diagonal
                dkmin=0 : only 4d diagonal
                dkmin=large number : full 4d->3d diag
    """
    ml  = []
    diag_factor = np.asarray(diag_factor)
    for k in klist :
        m   = diag_factor * k
        for i_s in np.ndindex(2,2) :   # alternate signs of x,y,z components
            s   = 1 - 2*np.array(i_s)

            if not None is dkmin : 
                # run from kmin>=0 to k  ++ negative values
                kmin1   = min(k, max(0, k - dkmin))
                kxxlist1= range(kmin1, k + 1)
                kxxlist = kxxlist1 + [-kxx for kxx in kxxlist1 ]
            else : kxxlist = range(-k, k+1)
            for kxx in kxxlist :
                mxx = diag_factor * kxx
                mk  = np.array([ 
                        [mxx[0],    m[1]*s[0], m[2]*s[1], m[3]],
                        [m[0]*s[0],    mxx[1], m[2]*s[1], m[3]],
                        [m[0]*s[0], m[1]*s[1],    mxx[2], m[3]],
                        ])
                ml.extend(mk)

    ml  = list(set([ tuple(m) for m in ml ]))
    ml.sort()
    return np.array(ml)

def gen_momenta_diag2d_tpos(klist, diag_factor=np.r_[1,1,1,1]) :
    """ treat t-direction differently : only positive 
        diag_factor : for anisotropic diags
    """
    ml  = []
    diag_factor = np.asarray(diag_factor)
    for k in klist :
        for i_s in np.ndindex(2,) :   # alternate signs of x,y,z components
            mom_a   = diag_factor * k
            s       = 1 - 2*np.array(i_s)
            ml.extend([ mom_a * [s[0],    0,    0, 1],
                        mom_a * [   0, s[0],    0, 1], 
                        mom_a * [   0,    0, s[0], 1] ])
    return np.array(ml)

# generate combinations of 6 momenta for NNbar NPR in 2-mom scheme 
def gen_mc6_nnbar_twomom(a_list, ls, tw) :
    ml  = []
    ls  = np.asarray(ls)
    tw  = np.asarray(tw)
    for mom_a in a_list :
        mom_b   = inv_wavevec(mom_a, ls, tw)
        ml.append([ mom_a, mom_a, mom_a, mom_b, mom_b, mom_b ])
        ml.append([ mom_a, mom_b, mom_a, mom_a, mom_b, mom_b ])
        ml.append([ mom_b, mom_b, mom_a, mom_a, mom_a, mom_b ])
    return np.array(ml)
gen_momenta_nnbar = gen_mc6_nnbar_twomom       # old alias

def gen_momenta_add_inv(momlist_tpos, ls, tw) :
    momlist_tneg = np.array([ inv_wavevec(m, ls, tw) for m in momlist_tpos ])
    return np.concatenate((momlist_tpos, momlist_tneg))

def gen_momenta_trapez(a0, a1, kmax, kmin=0) :
    """ generate all-positive trapez interpolating between km*a0 and km*a1 for km="""
    a0  = np.asarray(a0)
    a1  = np.asarray(a1)
    ml  = []
    kmin=min(kmax, kmin)
    for k0 in range(kmin, 1+kmax) :
        for k1 in range(kmin, 1+k0) :
            ml.append(k0 * a0 + k1 * (a1 - a0))     # kmin<=k1<=k0
    return ml

def intvec_uniq(vec_list) :
    """ return list of uniq vectors """
    vec_s   = list(set([ tuple(v) for v in vec_list ]))
    vec_s.sort()
    return [ np.array(v) for v in vec_s ]

def gen_momenta_trapez_adjdiag(kmax, diag_factor, kmin=0, diag_begin=[0]) :
    """ generate a joint list of (kmax,kmin)-trapez 
        between pairs of adjacent k-diag and (k+1)-diags 
        starting from b-masks of diags in diag_begin list """
    df      = np.asarray(diag_factor)
    dim     = df.shape[0]
    assert(df.shape == (dim,))

    # dim-vector factors to select d=0(1),1(4),2(6),3(4),4(1)-d diagonals
    bm_mul  = 2**np.r_[:dim]
    bm_basis= np.where(np.bitwise_and(np.r_[:2**dim][:,None], bm_mul), 1, 0)
    # proceed in a queue from start vector (minimum diags) to full d-diag
    bm_queue= list(diag_begin)
    if 0 == len(bm_queue) : bm_queue = [0]
    vec_list= []
    while 0 < len(bm_queue) :
        bm_i    = bm_queue.pop(0)
        vec_i   = df * bm_basis[bm_i]
        for bm_m in bm_mul :
            bm_j    = bm_i | bm_m
            if bm_i < bm_j :            # new diag
                if 0 == bm_queue.count(bm_j) : bm_queue.append(bm_j)    # add to set
                vec_j   = df * bm_basis[bm_j]
                vec_tr = gen_momenta_trapez(vec_i, vec_j, kmax, kmin=kmin)
                vec_list.extend(vec_tr) 

    return intvec_uniq(vec_list)

def gen_momenta_trapez_alldiag(kmax, diag_factor, kmin=0, diag_begin=[0]) :
    """ generate a joint list of (kmax,kmin)-trapez 
        between pairs of adjacent k-diag and l-diags, All k<l
        starting from b-masks of diags in diag_begin list """
    df      = np.asarray(diag_factor)
    dim     = df.shape[0]
    assert(df.shape == (dim,))

    # dim-vector factors to select d=0(1),1(4),2(6),3(4),4(1)-d diagonals
    bm_mul  = 2**np.r_[:dim]
    bm_max  = 2**dim
    bm_basis= np.where(np.bitwise_and(np.r_[:2**dim][:,None], bm_mul), 1, 0)
    # proceed in a queue from start vector (minimum diags) to full d-diag
    bm_queue= list(diag_begin)
    if 0 == len(bm_queue) : bm_queue = [0]
    vec_list= []

    for bm_j in range(bm_max) :
        vec_j   = df * bm_basis[bm_j]
        for bm_i in range(bm_j) :
            if bm_i & bm_j == bm_i : # i.e. if bm_i is subset of bm_j
                vec_i   = df * bm_basis[bm_i]
                vec_tr = gen_momenta_trapez(vec_i, vec_j, kmax, kmin=kmin)
                vec_list.extend(vec_tr) 

    return intvec_uniq(vec_list)

def make_all_reflect(vec_list) :
    """ produce vectors with all possible reflections of components """
    vec_list= np.asarray(vec_list)
    dim     = vec_list.shape[1]
    bm_mul  = 2**np.r_[:dim]
    s_basis = np.where(np.bitwise_and(np.r_[:2**dim][:,None], bm_mul), 1, -1)
    
    res = []
    for s in s_basis :
        res.extend(vec_list * s)
    return intvec_uniq(res)


""" generate momenta and save to h5:
h5f_ml = h5py.File('momlist_diag.h5', 'a')

def select_x(vl) : return [ v for v in make_all_reflect(vl) if 0<v[-1]]

h5f_ml['alldiag_trapez_4_11']   = np.array(select_x(gen_momenta_trapez_alldiag(11, [1,1,1,2], diag_begin=[0], kmin=4)))
h5f_ml['adjdiag_trapez_4_11']   = np.array(select_x(gen_momenta_trapez_adjdiag(11, [1,1,1,2], diag_begin=[0], kmin=4)))
h5f_ml['alldiag_trapez_4_11_t'] = np.array(select_x(gen_momenta_trapez_alldiag(11, [1,1,1,2], diag_begin=[8], kmin=4)))
h5f_ml['adjdiag_trapez_4_11_t'] = np.array(select_x(gen_momenta_trapez_adjdiag(11, [1,1,1,2], diag_begin=[8], kmin=4)))
h5f_ml['alldiag_trapez_0_11']   = np.array(select_x(gen_momenta_trapez_alldiag(11, [1,1,1,2], diag_begin=[0], kmin=0)))
h5f_ml['adjdiag_trapez_0_11']   = np.array(select_x(gen_momenta_trapez_adjdiag(11, [1,1,1,2], diag_begin=[0], kmin=0)))
h5f_ml['alldiag_trapez_0_11_t'] = np.array(select_x(gen_momenta_trapez_alldiag(11, [1,1,1,2], diag_begin=[8], kmin=0)))
h5f_ml['adjdiag_trapez_0_11_t'] = np.array(select_x(gen_momenta_trapez_adjdiag(11, [1,1,1,2], diag_begin=[8], kmin=0)))

h5f_ml.flush() ; h5f_ml.close()
"""
