import math
import numpy as np
from ..misc.numpy_extra import *
from ..pymath.gamma_matr import Gamma16_dgr as Gamma16


# constants
NCOLOR  = 3
NSPIN   = 4
NCOLSPIN= NCOLOR*NSPIN
LDIM    = 4
dirprop_shape = (NCOLOR, NSPIN, NCOLOR, NSPIN)

###############################################################################
# manipulating quark propagators
# the last four indices are either sc:(ic,is,jc,is) or flat:(ics,ics)
###############################################################################
def qqbar_is_flat(qqbar) :
    """ check the shape of the (?)propagator to be in color*spin representation """
    return (2 <= len(qqbar.shape) and qqbar.shape[-2:] == (NCOLSPIN, NCOLSPIN))

def qqbar_is_cs(qqbar) :
    """ check the shape of the (?)propagator to bi in [color,spin] representation """
    return (4 <= len(qqbar.shape) and qqbar.shape[-4:] == (NCOLOR, NSPIN, NCOLOR, NSPIN))

def qqbar_to_flat(qqbar_cs) :
    assert (qqbar_is_cs(qqbar_cs))
    return qqbar_cs.reshape(qqbar_cs.shape[:-4] + (NCOLSPIN, NCOLSPIN))

def qqbar_to_cs(qqbar_flat) :
    assert (qqbar_is_flat(qqbar_flat))
    return qqbar_flat.reshape(qqbar_flat.shape[:-2] + (NCOLOR, NSPIN, NCOLOR, NSPIN))

# 
def qqbar_inv(qqbar) :
    assert(qqbar_is_cs(qqbar))
    return  qqbar_to_cs(np.linalg.inv(qqbar_to_flat(qqbar)))

def qqbar_rev(qqbar) :
    """ "reverse" qqbar[..., ic,is,jc,js] ->  gamma5 . qqbar^dag . gamma5 
        keep the index convention of qqbar
    """
    assert(qqbar_is_cs(qqbar))
    return np.einsum('...ab,...edcb,...df->...caef', 
                     Gamma16[15], qqbar.conj(), Gamma16[15])


###############################################################################
# lattice momenta
###############################################################################
def calc_mom_lin(c, latsize, twist) :
    """ linear def. of momentum : p_mu = plin_mu = 2*pi*n_mu / L_mu """
    if None is twist : twist = 1
    latsize = np.asarray(latsize)
    twist   = np.asarray(twist)
    return (2*math.pi * (c + twist)) / latsize
def calc_momsq_lin(c, latsize, twist) :
    """ the last axis of 'c' is 'mu' """
    if None is twist : twist = 0
    latsize = np.asarray(latsize)
    twist   = np.asarray(twist)
    return (( (2*math.pi * (c + twist)) 
                / latsize)**2).sum(axis=-1)

def calc_mom_center(c, ls) :
    hls = ls / 2
    return (c + hls) % ls - hls

def calc_mom_sin(c, ls, tw) : 
    """ half-sine def. of momentum : p_mu = 2*sin(plin_mu/2) ; 
        note that it depends on the selection of the Brillouin zone 
        (0-centered in this case) """
    if None is tw : tw = 0
    ls = np.asarray(ls)
    tw = np.asarray(tw)
    return 2 * np.sin((math.pi * (calc_mom_center(c, ls) + tw)) / ls)
def calc_momsq_sin(c, ls, tw) :
    """ the last axis of 'c' is 'mu' """
    if None is tw : tw = 0
    ls = np.asarray(ls)
    tw = np.asarray(tw)
    return 4 * (np.sin( (math.pi * (c + tw)) / ls)**2).sum(axis=-1)

def calc_mom_sym(c, ls, tw) :
    """ momentum def. from symm.derivative : p_mu=sin(plin_mu) """
    if None is tw : tw = 0
    ls = np.asarray(ls)
    tw = np.asarray(tw)
    return np.sin((2 * math.pi * (c + tw)) / ls)
def calc_momsq_sym(c, ls, tw) :
    """ momentum def. from symm.derivative : p_mu=sin(plin_mu) """
    if None is tw : tw = 0
    ls = np.asarray(ls)
    tw = np.asarray(tw)
    return (np.sin((2 * math.pi * (c + tw)) / ls)**2).sum(axis=-1)
def H4_invariants(mom4, ls, ft) :
    """ compute hypercubic invariants for 4-vector """
    mom4    = np.asarray(mom4)
    assert(mom4.shape[-1] == len(ls))    
    k_lin_sq = calc_mom_lin(mom4, ls, ft)**2
    return np.r_['-1', 
        k_lin_sq.sum(-1)[..., None], 
        (k_lin_sq**2).sum(-1)[..., None], 
        (k_lin_sq**3).sum(-1)[..., None], 
        (k_lin_sq**4).sum(-1)[..., None] ]


###############################################################################
# 4d lattice vectors ; TODO move to latgeometry?
###############################################################################
def inv_wavevec(wv, ls, tw) :
    """ transformation (roughly) preserving momsq """
    wv  = np.asarray(wv)
    tw  = np.asarray(tw)
    return np.array(np.round(-wv - 2*tw), wv.dtype)

def std_inv_wavevec(wv, ls, tw) :
    """ make wave vector "standard" wrt (Nd)-dim inversions
        std == the first non-zero component is >0
        transformation preserving momsq """
    if cmp_wavevec(wv, (0,)*len(wv)) < 0 :
        return inv_wavevec(wv, ls, tw)
    else :
        return wv
def std_rot3d_wavevec(wv, ls, tw) :
    """ make wave vector "standard" wrt (Nd-1)-dim rotations and (Nd)-dim reflections
        std == all components >=0, first 3 are ordered 
        transformation preserving momsq """
    wv = np.asarray(wv)
    res = np.array(np.maximum(wv, inv_wavevec(wv, ls, tw)), wv.dtype)
    res[:-1].sort()
    return res
    
def cmp_wavevec(x, y) :
    return cmp(tuple(x), tuple(y))

def coord_meshgrid(xr_list) :
    """ xr - coordinate "ranges" """
    xr_list = list([ np.r_[xr] for xr in xr_list ])
    sh  = tuple(len(xr) for xr in xr_list)
    nd  = len(sh)
    dt  = np.find_common_type([], [ xr.dtype for xr in xr_list])
    mg  = np.meshgrid(*xr_list, indexing='ij')
    res = np.empty(sh + (nd,), dt)
    for mu in range(len(xr_list)) :
        res[..., mu] = mg[mu]
    return res


###############################################################################
# auxiliary functions for classification
# TODO move to misc?
###############################################################################
def coord_meshgrid(xr_list) :
    """ xr - coordinate "ranges" """
    xr_list = list([ np.r_[xr] for xr in xr_list ])
    sh  = tuple(len(xr) for xr in xr_list)
    nd  = len(sh)
    dt  = np.find_common_type([], [ xr.dtype for xr in xr_list])
    mg  = np.meshgrid(*xr_list, indexing='ij')
    res = np.empty(sh + (nd,), dt)
    for mu in range(len(xr_list)) :
        res[..., mu] = mg[mu]
    return res


def make_cmp_tol(tol) :
    """ cmp function with tolerance 
        obsolete? replace with cmp_with_tol """
    def cmp_tol(x, y) :
        if abs(x - y) <= tol : return 0
        else : return cmp(x, y)
    return cmp_tol
def cmp_tol(adiff=1e-8, rdiff=1e-8) :
    """ cmp function treating elements with abs. or rel. difference
        below threshold as equal
    """
    def c(x, y) :
        if abs(x - y) < adiff or lhpd.rdiff(x, y) < rdiff : return 0
        else : return cmp(x,y)
    return c
cmp_with_tol=cmp_tol
equal_bound = np_equal_bound

def classify_list(l, elem_to_std_func=lambda x:x, cmp=cmp) :
    """
        build & enumerate equivalence classes
        resulting enumerated class list is sorted according to `cmp'
        RETURN [i_l]->i_class,  [i_class]->elem_std
    """
    l_std = [elem_to_std_func(x) for x in l]
    class_list = list(set(l_std))
    class_list.sort(cmp=cmp)
    class_map = dict((v, i) for i, v in enumerate(class_list))
    l_class_ind = [ class_map[x] for x in l_std ]
    return l_class_ind, class_list

def dict_incr(d, k, v=1):  
    """ increment a dictionary element, start with 0 if no key """
    d[k] = d.get(k, 0) + v


# unify with np_find_first
def find_first(v, l_v, cmp=cmp) :
    """ find first [i] element in l_v, cmp(v, l_v[i])==0 """
    for i, v_i in enumerate(l_v) :
        if 0 == cmp(v, v_i) : 
            return i
    return None

def find_first_arr_tol(v, l_v, tol=1e-8) :
    """ find first [i] element in l_v, cmp(v, l_v[i])==0 """
    for i, v_i in enumerate(l_v) :
        if (np.abs(np.asarray(v_i) - v)**2).sum() < tol : 
            return i
    return None


