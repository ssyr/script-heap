from .npr_common import *

import numpy as np
import math
from ..pymath.gamma_matr import Gamma16_dgr
from ..pymath import H4_tensor as h4t
from ..misc import rdiff
from ..pymath import tensor
from ..pymath import combi



def make_nnbar_tensor(chi1, chi2, chi3) :
    """ 
        chi1,2,3        (chiral) projectors
        colmat          'symm'|'asym'
        type            1|2 == uu dd dd, ud ud dd
    """
    def m_trans(a, tr) :
        """ mathematica-style transpose : a[abcd..f] <- a[tr(abcd..f)] """
        tr  = np.asarray(tr)
        return np.transpose(a, np.argsort(tr))

    def count_nonzero(a): return (np.where(0 == a, 0, 1).sum())
    
    def asym_u2d4(a):
        """ in : tensor [i_u1, i_u2, i_d1, i_d2, i_d3, i_d4]
            out: tensor [i_u12, i_d1234]
            NOTE : fermion combinatorial factors are included 
            XXX are they correct ???
        """
        a   = tensor.tensor_asym_flat_group(a, 12, [1]*2, a_axes=range(2)) * 2
        a   = tensor.tensor_asym_flat_group(a, 12, [1]*4, a_axes=range(4)) * 24
        return a

    eps3d   = tensor.tensor_asym_flat_ungroup(np.array([math.sqrt(6)]), 3, [1]*3)
    eps3d3d = tensor.tensor_outer(eps3d, eps3d)


    coltens_s = eps3d3d.transpose((1,4,2,5,0,3))                # t[abcdef] <- eps[eac]eps[fbd]
    coltens_s = coltens_s + coltens_s.transpose((1,0,2,3,4,5))  # t[abcdef] : (ab)
    coltens_s = coltens_s + coltens_s.transpose((0,1,2,3,5,4))  # t[abcdef] : (ef)

    coltens_a = eps3d3d.transpose((1,2,4,5,0,3))                # t[abcdef] <- eps[eab]eps[fcd]
    coltens_a = coltens_a + coltens_a.transpose((0,1,2,3,5,4))  # t[abcdef] : (ef)

    nnbartens_symm= (coltens_s.reshape((3,1,3,1,3,1,3,1,3,1,3,1)) 
                    * tensor.tensor_outer(chi1, chi2, chi3).reshape(
                                       (1,4,1,4,1,4,1,4,1,4,1,4))).reshape(12,12,12,12,12,12)
    nnbartens_asym= (coltens_a.reshape((3,1,3,1,3,1,3,1,3,1,3,1)) 
                    * tensor.tensor_outer(chi1, chi2, chi3).reshape(
                                       (1,4,1,4,1,4,1,4,1,4,1,4))).reshape(12,12,12,12,12,12)

    # all tensors are indexed as [i_u1, i_u2, i_d1, i_d2, i_d3, i_d4]
    # Op1: uu dd dd : same order
    nnbartens_s_1 = asym_u2d4(nnbartens_symm)
    # Op2, Op3 : ud ud dd : transpose i_u2 and d_i1 indices
    # x(-1) from permuting (u,d) in Wick contractions
    nnbartens_s_2 = asym_u2d4(-nnbartens_symm.transpose((0,2,1,3,4,5))) 
    nnbartens_a   = asym_u2d4(-nnbartens_asym.transpose((0,2,1,3,4,5))) # x(-1) from permuting (u,d)

    return nnbartens_s_1, nnbartens_s_2, nnbartens_a

def init_npr_nnbar() :
    nnbar_cnct_matr = np.empty(
            ( 3, 2, 2, 2, 
              combi.binom_coeff(NCOLSPIN, 2), 
              combi.binom_coeff(NCOLSPIN, 4) ),
            np.complex)

    G   = Gamma16_dgr
    cc = 1j * np.dot(G[2], G[8])    # charge conjugation matrix
    p_cc = [ # PR.C, PL.C
        np.dot(0.5 * (G[0] + G[15]), cc),
        np.dot(0.5 * (G[0] - G[15]), cc) ]

    for i_chi1, i_chi2, i_chi3 in np.ndindex(2,2,2) :
        t0, t1, t2, = make_nnbar_tensor(
                p_cc[i_chi1], p_cc[i_chi2], p_cc[i_chi3])
        nnbar_cnct_matr[:,i_chi1, i_chi2, i_chi3] = t0, t1, t2

    globals()['nnbar_cnct_matr_internal_'] = nnbar_cnct_matr

def get_nnbar_cnct_all() :
    """ calculation of contractions matrix is expensive 
        compute on demand and cache
        return [i_op, i_chi1, i_chi2, i_chi3, i_u1u2, id3d4d5d6] [3,2,2,2, ...]
    """
    if not 'nnbar_cnct_matr_internal_' in globals() :
        print('INIT nnbar contractions...')
        init_npr_nnbar()
        print('INIT nnbar contractions DONE')
    return nnbar_cnct_matr_internal_

def get_nnbar_cnct_basis() :
    """ basis of 14 relevant operators is defined here """
    nnb = get_nnbar_cnct_all()
    # XXX 2015/06/15: fixed so that res[7:14] is P-image of res[0:7]
    return np.asarray([
                # RRR
                nnb[0,0,0,0] + 4*nnb[1,0,0,0],      # O1RRR + 4*O2RRR   (3Rx0L)
                nnb[1,0,0,0] - nnb[0,0,0,0],        # O2RRR - O1RRR     (1Rx0L)
                # RRL
                nnb[0,1,0,0],                       # O1LRR             (2Rx1L)
                nnb[1,1,0,0],                       # O2LRR             (2Rx1L)
                nnb[0,0,0,1] + 2*nnb[1,0,0,1],      # O1RRL + 2*O2RRL   (2Rx1L)
                nnb[1,0,0,1] - nnb[0,0,0,1],        # O2RRL - O1RRL     (0Rx1L)
                nnb[2,1,0,0],                       # O3LRR             (1Rx0L)
                # LLL
                nnb[0,1,1,1] + 4*nnb[1,1,1,1],      # O1LLL + 4*O2LLL   (3Lx0R)
                nnb[1,1,1,1] - nnb[0,1,1,1],        # O2LLL - O1LLL     (1Lx0R)
                # LLR
                nnb[0,0,1,1],                       # O1RLL             (2Lx1R)
                nnb[1,0,1,1],                       # O2RLL             (2Lx1R)
                nnb[0,1,1,0] + 2*nnb[1,1,1,0],      # O1LLR + 2*O2LLR   (2Lx1R)
                nnb[1,1,1,0] - nnb[0,1,1,0],        # O2LLR - O1LLR     (0Lx1R)
                nnb[2,0,1,1],                       # O3RLL             (1Lx0R) 
    ])


def UTEST_npr_nnbar_gold(op_type, chi1, chi2, chi3, q6) :
    """ q6 [i_q][i_c][i_s] : u,u, d,d d,d 
        op_type : { 0 -> Op1, 1 -> Op2, 2 -> Op3 }
    """
    import itertools as it
    eps3d = np.array([ 
            [ [ 0, 0, 0], 
              [ 0, 0, 1], 
              [ 0,-1, 0] ], 
            [ [ 0, 0,-1], 
              [ 0, 0, 0], 
              [ 1, 0, 0] ], 
            [ [ 0, 1, 0], 
              [-1, 0, 0], 
              [ 0, 0, 0] ] ], 
              float) 
    def coltens(op_type, a,b,c,d,r,s) :
        if 0 == op_type or 1 == op_type :
            return  ( eps3d[r,a,c]*eps3d[s,b,d] + eps3d[s,a,c]*eps3d[r,b,d] 
                    + eps3d[r,b,c]*eps3d[s,a,d] + eps3d[s,b,c]*eps3d[r,a,d] )
        elif 2 == op_type :
            return  ( eps3d[r,a,b]*eps3d[s,c,d] + eps3d[s,a,b]*eps3d[r,c,d] )
        else : raise ValueError

    def get_u2d4_ind(op_type, ind) :
        if 0 == op_type : 
            return [ind[k] for k in [0,1]], [ind[k] for k in [2,3,4,5]]
        elif 1 == op_type or 2 == op_type :
            return [ind[k] for k in [0,2]], [ind[k] for k in [1,3,4,5]]
        else : raise ValueError

    def put_u2d4_ind(op_type, i_u2, i_d4) :
        if 0 == op_type : 
            return [i_u2[0], i_u2[1], i_d4[0], i_d4[1], i_d4[2], i_d4[3]]
        elif 1 == op_type or 2 == op_type :
            return [i_u2[0], i_d4[0], i_u2[1], i_d4[1], i_d4[2], i_d4[3]]
        else : raise ValueError

    fact0 = {0 : 1, 1 : -1, 2 : -1}[op_type] #contract with D3 D2 D1 D0 U1 U0
    res = 0
    for i_s in np.ndindex((NSPIN,)*6) :
        fact1 = (fact0 * chi1[i_s[0], i_s[1]] 
                       * chi2[i_s[2], i_s[3]] 
                       * chi3[i_s[4], i_s[5]])
        if 0 == fact1 : 
            continue

        for i_c in np.ndindex((NCOLOR,)*6) :
            fact2 = fact1 * coltens(op_type, *i_c)

            if 0 == fact2 : 
                continue

            #print(i_s, i_c)
            for p_u in it.permutations(range(2)) :
                for p_d in it.permutations(range(4)) :

                    i_c_u, i_c_d = get_u2d4_ind(op_type, i_c)
                    i_c_u = [ i_c_u[p] for p in p_u ]
                    i_c_d = [ i_c_d[p] for p in p_d ]
                    
                    i_s_u, i_s_d = get_u2d4_ind(op_type, i_s)
                    i_s_u = [ i_s_u[p] for p in p_u ]
                    i_s_d = [ i_s_d[p] for p in p_d ]

                    fact3 = (fact2 * (1 - combi.permut_count(p_u) % 2 * 2) 
                                   * (1 - combi.permut_count(p_d) % 2 * 2) )
                   
                    res = res + fact3 *( 
                                  q6[0][i_c_u[0]][i_s_u[0]] 
                                * q6[1][i_c_u[1]][i_s_u[1]]
                                * q6[2][i_c_d[0]][i_s_d[0]]
                                * q6[3][i_c_d[1]][i_s_d[1]]
                                * q6[4][i_c_d[2]][i_s_d[2]]
                                * q6[5][i_c_d[3]][i_s_d[3]] )
    return res

def UTEST_npr_nnbar_check(q6=None) :

    if None is q6 :
        q6 = np.random.rand(6, NCOLOR, NSPIN) + 1j*np.random.rand(6, NCOLOR, NSPIN)
    else : 
        q6 = np.asarray(q6)

    q6_sc = q6.reshape(6, NCOLSPIN)
    uubar2      = tensor.tensor_asym_flat_outer( q6_sc[0:2], NCOLSPIN, [1]*2)
    ddbar2_0    = tensor.tensor_asym_flat_outer( q6_sc[2:4], NCOLSPIN, [1]*2)
    ddbar2_1    = tensor.tensor_asym_flat_outer( q6_sc[4:6], NCOLSPIN, [1]*2)
    ddbar4      = tensor.tensor_asym_flat_outer( q6_sc[2:6], NCOLSPIN, [1]*4)

    G           = Gamma16_dgr
    chi         = [
        .5j * np.dot(np.dot(G[2], G[8]), G[0] + G[15]),
        .5j * np.dot(np.dot(G[2], G[8]), G[0] - G[15]) ]

    print(uubar2.shape, ddbar2_0.shape, ddbar2_1.shape)
    res = np.empty((3, 2, 2, 2, 4), q6.dtype)
    for op_type in range(3) :
        for i_chi in np.ndindex(2,2,2) :
            nnmat_2x4   = get_nnbar_cnct_all()[op_type, i_chi[0], i_chi[1], i_chi[2]]
            nnmat_2x2x2 = tensor.tensor_asym_flat_ungroup(nnmat_2x4, NCOLSPIN, [2, 2],
                            a_axis=-1, out_axes=[-2,-1])

            res_2x4     = np.einsum('ij,i,j', nnmat_2x4, uubar2, ddbar4)
            res_2x2x2   = np.einsum('ijk,i,j,k', nnmat_2x2x2, uubar2, ddbar2_0, ddbar2_1)
            res_2x2x2_r = np.einsum('ijk,i,j,k', nnmat_2x2x2, uubar2, ddbar2_1, ddbar2_0)

            res_gold    = UTEST_npr_nnbar_gold(op_type, 
                                chi[i_chi[0]], chi[i_chi[1]], chi[i_chi[2]], q6)
    
            res[op_type, i_chi[0], i_chi[1], i_chi[2]] = res_2x4, res_2x2x2, res_2x2x2_r, res_gold

    print(np.allclose(1, res[..., 1:] / res[..., 0:1]))
    return res
            

def UTEST_nnbar_cnct_matrix_complete() :
    nnbar_cnct_list = get_nnbar_cnct_basis()
    n_op    = len(nnbar_cnct_list)
    nnbl    = np.asarray(nnbar_cnct_list)
    nnbl_g  = np.einsum('ikl,jkl->ij', nnbl, nnbl.conj())
    nnbl_gnorm  = nnbl_g / nnbl_g.diagonal()
    # have a diagonal matrix :
    print(np.allclose(np.identity(n_op), nnbl_gnorm))


def UTEST_check_q6colorspin():
    nnbar_cnct = get_nnbar_cnct_basis()
    n_op        = len(nnbar_cnct)
    nnbar_cnct  = tensor.tensor_asym_flat_ungroup(nnbar_cnct, NCOLSPIN, [1,1,1,1], a_axis=-1)
    nnbar_cnct  = tensor.tensor_asym_flat_ungroup(nnbar_cnct, NCOLSPIN, [1,1], a_axis=-5, out_axes=(-6,-5))
    assert(nnbar_cnct.shape == (n_op,) + (NCOLSPIN,)*6)
    nnbar_cnct  = nnbar_cnct.reshape((n_op,) + (NCOLOR, NSPIN)*6)

    if True : #passed
        from ..pymath.gamma_matr import Gamma16_dgr as Gamma16
        gamma_vec   = [ Gamma16[i] for i in [1, 2, 4, 8] ]
        sigma_list  = [ np.dot(gamma_vec[i], gamma_vec[j])
                        for i in range(4) for j in range(i) ]
        for i_nnb, nnb in enumerate(nnbar_cnct) :
            nnb_norm2 = (nnb * nnb.conj()).real.sum()
            for i_s, s in enumerate(sigma_list) :
                spinrot_nnb    = np.zeros_like(nnb)
                for i in range(6) :
                    spinrot_nnb += tensor.tensor_matrix_dot(nnb, s, 2*i+1)
                spinrot_nnb_norm2 = (spinrot_nnb * spinrot_nnb.conj()).real.sum()
                print('SpinRot', i_nnb, i_s, spinrot_nnb_norm2 / nnb_norm2 < 1e-6, spinrot_nnb_norm2 / nnb_norm2)

    if True : #passed
        from ..pymath.gellmann import gellmann_matr
        for i_nnb, nnb in enumerate(nnbar_cnct) :
            nnb_norm2 = (nnb * nnb.conj()).real.sum()
            for i_g, g in enumerate(gellmann_matr) :
                colrot_nnb    = np.zeros_like(nnb)
                for i in range(6) :
                    colrot_nnb += tensor.tensor_matrix_dot(nnb, g, 2*i)
                colrot_nnb_norm2 = (colrot_nnb * colrot_nnb.conj()).real.sum()
                print('colrot', i_nnb, i_g, colrot_nnb_norm2 / nnb_norm2 < 1e-6, colrot_nnb_norm2 / nnb_norm2)



def UTEST_nnbar_count_nonzero() :
    def count_nonzero(a): 
        return (np.where(0 == a, 0, 1).sum())
    
    nnb = get_nnbar_cnct_all()
    for i_chi1, i_chi2, i_chi3 in np.ndindex(2,2,2) :
        t0, t1, t2, = nnb[:, i_chi1, i_chi2, i_chi3]
        print('[%d %d %d] : %d %d %d' % (i_chi2, i_chi2, i_chi3,
                    count_nonzero(t0), count_nonzero(t1), count_nonzero(t2)))
