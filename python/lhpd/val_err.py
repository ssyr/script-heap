import math
import numpy as np
from .misc import spprint_ve

np_spprint_ve = np.vectorize(spprint_ve)

def tab2str(t) :
    lts = len(t.shape)
    if 1 == lts : return '\t'.join(t)
    else : return ('\n' * (lts - 1)).join([ tab2str(x) for x in t])

def np_spprint_ve_tab(v,e) : return tab2str(np_spprint_ve(v,e))

class val_err:
    """ (value, error) pair implementing basic arithmetic to accumulate errors
        in quadrature
        error can be a numpy array; all elements are treated as independent errors 
            (e.g. like stat and sys)
        TODO test support for numpy arrays in sin,cos,...
        TODO prettyprint
        XXX  only real-valued numbers are supported; otherwise unclear meaning of abs
    """
    def __init__(self, v, e):
        self.v  = v         # value
        self.e  = np.abs(e) # error
        if isinstance(v, np.ndarray):
            assert(e.shape[-len(v.shape):] == v.shape)
            self.sin, self.cos, self.log, self.exp, self.sqrt, self.spprint = (
                np.sin, np.cos, np.log, np.exp, np.sqrt, np_spprint_ve_tab)
        else:
            self.sin, self.cos, self.log, self.exp, self.sqrt, self.spprint = (
                math.sin, math.cos, math.log, math.exp, math.sqrt, spprint_ve)
            self.spprint
    
    def __neg__(a): return val_err(-a.v, a.e)
    def __pos__(a, o): return val_err(+a.v, a.e)
    def __abs__(a): return val_err(abs(a.v), a.e)
    
    def __add__(a, b):
        self    = a
        if isinstance(b, val_err):
            return val_err(a.v + b.v, self.sqrt(a.e**2 + b.e**2))
        else: return val_err(a.v + b, a.e)
    def __radd__(b, a):
        self    = b
        if isinstance(a, val_err):  
            return val_err(a.v + b.v, self.sqrt(a.e**2 + b.e**2))
        else: return val_err(a + b.v, b.e)

    def __sub__(a, b):
        self    = a
        if isinstance(b, val_err):  
            return val_err(a.v - b.v, self.sqrt(a.e**2 + b.e**2))
        else: return val_err(a.v - b, a.e)
    def __rsub__(b, a):
        self    = b
        if isinstance(a, val_err):  
            return val_err(a.v - b.v, self.sqrt(a.e**2 + b.e**2))
        else: return val_err(a - b.v, b.e)
        
    def __mul__(a, b):
        self    = a
        if isinstance(b, val_err):
            r = a.v * b.v
            fe= self.sqrt((a.e / np.abs(a.v))**2 + (b.e / np.abs(b.v))**2)
            return val_err(r, np.abs(r) * fe)
        else: return val_err(a.v * b, a.e * abs(b))
    def __rmul__(b, a):
        self    = b
        if isinstance(a, val_err):
            r = a.v * b.v
            fe= self.sqrt((a.e / np.abs(a.v))**2 + (b.e / np.abs(b.v))**2)
            return val_err(r, np.abs(r) * fe)
        else: return val_err(a * b.v, abs(a) * b.e)

    def __truediv__(a, b):
        self    = a
        if isinstance(b, val_err):
            r = a.v / b.v
            fe= self.sqrt((a.e / np.abs(a.v))**2 + (b.e / np.abs(b.v))**2)
            return val_err(r, np.abs(r) * fe)
        else: return val_err(a.v / b, a.e / abs(b))
    def __rdiv__(b, a):
        self    = b
        if isinstance(a, val_err):
            r = a.v / b.v
            fe= self.sqrt((a.e / np.abs(a.v))**2 + (b.e / np.abs(b.v))**2)
            return val_err(r, np.abs(r) * fe)
        else: return val_err(a / b.v, abs(a / b.v**2) * b.e)

    def __pow__(a, b):
        self    = a
        if isinstance(b, val_err):
            r = a.v ** b.v
            fe= self.sqrt((np.abs(b.v / a.v) * a.e)**2 + (np.abs(self.log(a.v)) * b.e)**2)
            return val_err(r, np.abs(r) * fe)
        else: return val_err(a.v**b, np.abs(b * a.v**(b-1.)) * a.e)
    def __rpow__(b, a):
        self    = b
        if isinstance(a, val_err):
            r = a.v ** b.v
            fe= self.sqrt((b.v / a.v * a.e)**2 + (self.log(a.v) * b.e)**2)
            return val_err(r, r * fe)
        else: return val_err(a**b.v, a**b.v * math.log(a) * b.e)
    def __getitem__(a, s):      return val_err(a.v[s], a.e[s])
    def __setitem__(a, s, b):   
        a.v.__setitem__(s, b.v)
        a.e.__setitem__(s, b.e)

    def log(a): return val_err(a.log(a.v), a.e / abs(a.v))
    def exp(a): return val_err(a.exp(a.v), a.exp(a.v) * a.e)
    def sin(a): return val_err(a.sin(a.v), abs(a.cos(a.v)) * a.e)
    def cos(a): return val_err(a.cos(a.v), abs(a.sin(a.v)) * a.e)
    def sqrt(a):return val_err(a.sqrt(a.v), a.e / 2. / a.sqrt(np.abs(a.v)))
    
    def val(self): return self.v
    def err(self): return self.e
    def relerr(self): return self.e / self.v

    def __repr__(self): return str((self.v, self.e))
    def __str__(self): return self.spprint(self.v, self.e)
