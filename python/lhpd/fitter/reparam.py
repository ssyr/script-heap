import numpy
import math

class reparam_id:
    """ identity reparameterization class 
        to be used as a stub
    """
    def __init__(self): pass
    def dim_fixed(self):return 0
    def f(self, x):     return x
    def df(self, x):    return None
    def inv_f(self, x): return x
    

""" reparameterization and function composition
    support up to second derivative (maximum required by minimization algorithms)
    vector functions should return
    f(x)    -> f[i] = f_i(x)
    df(x)   -> df[j,i] = {\partial f_i(x)\over\partial x_j}
    ddf(x)  -> ddf[j,k,i] = {\partial^2 f_i(x)\over\partial x_j\partial x_k}
"""
def scale_param(sc):
    """ scale the parameter: y = sc * x
        may be useful for better convergence/condition number 
    """
    def f(x):       return sc*x
    def df(x):      return sc
    def ddf(x):     return 0.
    def inv_f(y):   return y / sc
    return (f,df,ddf,inv_f)

def range_param(lo, up):
    """ put the parameter into the range lo <= y <= up
        if lo or up is None, assume infinity 
    """
    if (lo is None and up is None):
        raise ValueError("both range boundaries are undefined")
    elif up is None:
        """ ( lo ; \infty ): y = lo + exp x """
        def l_f(x):     return lo + math.exp(x)
        def l_df(x):    return math.exp(x)
        def l_inv_f(y): 
            if y <= lo : raise OverflowError
            return math.log(y - lo)
        return (l_f, l_df, l_df, l_inv_f)    # ddf === df
    elif lo is None:
        """ ( -\infty ; up ): y = up - exp x """
        def u_f(x):     return up - math.exp(x)
        def u_df(x):    return -math.exp(x)
        def u_inv_f(y): 
            if up <= y : raise OverflowError
            return math.log(up - y)
        return (u_f, u_df, u_df, u_inv_f)
    else:
        """ [ lo ; up ]: y = (lo + up + (lo-up)sin x)/2 """
        if up <= lo: raise ValueError("incorrect range boundaries")
        a, b = (up + lo)/2., (up - lo)/2.
        def lu_f(x):    return a + b * math.sin(x)
        def lu_df(x):   return b * math.cos(x)
        def lu_ddf(x):  return -b * math.sin(x)
        def lu_inv_f(y):
            if (y < lo or up < y) : raise OverflowError
            return math.asin((y - a) / b)
        return (lu_f, lu_df, lu_ddf, lu_inv_f)
    
class reparam:
    """ y = f(x): y[a] <- fixed[a] ; y[arg_index[i]] = f[i][*](x[i])  
        dim x = m, dim y = n
    """
    def __init__(self,*argv):
        self.n = len(argv)
        self.m = self.n
        self.fixed = [ None for i in range(self.n) ]         # unfixed params have 'None'
        self.arg_index  = []
        self.fcn = []
        for ip, p in enumerate(argv):
            if p is None:
                self.fcn.append(scale_param(1.))
                self.arg_index.append(ip)
            elif p[0]=='fixed':
                assert not None is  p[1]
                self.fixed[ip] = p[1]
                self.m -= 1
            elif p[0]=='scale':
                self.fcn.append(scale_param(p[1]))
                self.arg_index.append(ip)
            elif p[0]=='range':
                self.fcn.append(range_param(p[1],p[2]))
                self.arg_index.append(ip)
            # add "log-range" : with wide ranges, stick to one side
            else:
                raise ValueError("unknown parameter transformation %s" % (p[0]))
    def dim_fixed(self): return self.n - self.m
    def set_fixed_values(self,y):
        assert len(y) == self.n
        for i in filter(lambda i: (not self.fixed[i] is None), range(self.n)):
            assert not y[i] is None
            self.fixed[i] = y[i]
    def f(self,x):
        assert len(x) == self.m
        res = numpy.array(self.fixed, dtype=numpy.float64)
        for i,fcn in enumerate(self.fcn): 
            res[self.arg_index[i]] = fcn[0](x[i])
        return res
    def df(self,x):
        # XXX 2016/05/25 jacobian df is transposed : order is changed 
        #     from [jint, iext] = d p[iext] / d q[jint]
        #     to   [iext, jint] = d p[iext] / d q[jint]
        #     to agree with (modfunc,fitvec).df([xv],p) [i_x, i_p] = d f(xv[i_x], p) / d p[i_p]
        # FIXME need to change jacobian in lhpd.fitting.fitter
        assert len(x) == self.m
        res = numpy.zeros(shape=(self.n, self.m), dtype=numpy.float64)
        for i,fcn in enumerate(self.fcn): 
            res[self.arg_index[i], i] = fcn[1](x[i])
        return res
    def ddf(self,x):
        assert len(x) == self.m
        res = numpy.zeros(dtype=numpy.float64, shape=(self.n, self.m, self.m))
        for i,fcn in enumerate(self.fcn): 
            res[self.arg_index[i], i, i] = fcn[2](x[i])
        return res
    def inv_f(self,y):
        assert len(y) == self.n
        self.set_fixed_values(y)
        return numpy.array([ fcn[3](y[self.arg_index[i]])
                        for i,fcn in enumerate(self.fcn) ], dtype=numpy.float64)

