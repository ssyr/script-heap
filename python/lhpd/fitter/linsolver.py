import numpy as np
from ..misc import *

#FIXME duplicates fitter/cov_matr.py:recip_cov_matrix
def cov_invert(y_cov, rmin=1e-6, y_var_min=1e-8) :
    """ invert covariance matrix;
        pseudo-inverse: if some y_cov has variance < y_var_min * y_var_avg, 
        set its inv_sigma=0, effectively dropping it from chisquare
        
        y_cov       [i_data, i_var]

        TODO implement "broadcast" : 
            cov_invert(y_cov[i_data, (ind), i_var]) -> y_cov_inv[(ind), i_var, j_var]
        also, implement broadcast for computing covariance itself
    """
    dim = y_cov.shape[0]
    assert (y_cov.shape == (dim, dim))
    y_cov_diag = y_cov.flat[::dim+1]
    y_cov_diag_avg = y_cov_diag.mean()
    if (0. == y_cov_diag_avg).all() : 
        return np.zeros_like(y_cov)

    assert (-y_var_min * y_cov_diag_avg < y_cov_diag).all()
    y_sigma_zero = np.where(y_cov_diag < y_var_min * y_cov_diag_avg)
    y_cov_diag[y_sigma_zero] = np.nan
    y_sigma_inv = 1. / np.sqrt(y_cov_diag)
    y_sigma_inv[y_sigma_zero] = 0.

    y_cov_r = y_cov * y_sigma_inv * y_sigma_inv[:, None]
    assert np.allclose(y_cov, y_cov.T)
    assert np.allclose(y_cov_r, y_cov_r.T)
    y_ev, y_w = np.linalg.eigh(y_cov_r)
    assert((-rmin < y_ev).all())
    y_ev_norm = np.where(rmin < y_ev, y_ev, rmin)
    return np.dot(y_w / y_ev_norm, y_w.T) * y_sigma_inv * y_sigma_inv[:, None]

def linsolver_od(eqn, y, y_cov_inv, sv_min_r=1e-8) :
    """ find solution to a linear chi2 fit : min (eqn.x - y)^{-1}.y_cov_inv.(eqn.x-y)
        return (x, x_undef_matr) where eqn.x_undef_matr=0 (more precisely, where singular value sv<sv_min)
    """
    mat1    = np.dot(eqn.T, y_cov_inv)
    mat2    = np.dot(mat1, eqn)
    mat1_y  = np.dot(mat1, y)

    sv, w   = np.linalg.eigh(mat2)          # mat2 = w.diag{sv}.w^T
    sv_max  = sv.max()
    if (0. == sv).all() : # special case: zero matrix
        return (np.zeros((eqn.shape[1]), np.float64), 
                np.identity(eqn.shape[1], np.float64))
    assert (-sv_min_r * sv_max < sv).all()
    sv_zero = np.where(sv < sv_min_r * sv_max)
    
    sv[sv_zero]     = np.nan
    sv_inv          = 1. / sv
    sv_inv[sv_zero] = 0.

    x       = np.dot(w * sv_inv, np.dot(w.T, mat1_y))
    x_undef = w.T[sv_zero].T
    return x, x_undef

def linsolver_od_matr(eqn, y_cov_inv, sv_min_r=1e-8, VERBOSE=False) :
    """ find solution to a linear chi2 fit : min (eqn.x - y)^{-1}.y_cov_inv.(eqn.x-y)
        return (x, x_undef_matr) where eqn.x_undef_matr=0 (more precisely, where singular value sv<sv_min)
    """
    mat1    = np.dot(eqn.T, y_cov_inv)
    mat2    = np.dot(mat1, eqn)
    
    sv, w   = np.linalg.eigh(mat2)          # mat2 = w.diag{sv}.w^T
    if VERBOSE : 
        sv1 = np.array(sv)
        sv1.sort()
        print(sv1)
    sv_max  = sv.max()
    if (0. == sv).all() : # special case : zero matrix
        return (np.zeros((eqn.shape[1], eqn.shape[0]), np.float64), 
                np.identity(eqn.shape[1], np.float64))
    assert (-sv_min_r * sv_max < sv).all()
    sv_zero = np.where(sv < sv_min_r * sv_max)

    sv[sv_zero]     = np.nan
    sv_inv          = 1. / sv
    sv_inv[sv_zero] = 0.

    x_matr  = np.dot(w * sv_inv, np.dot(w.T, mat1))
    x_undef = w.T[sv_zero].T
    return x_matr, x_undef

def linsolver_od_matr_constrained(m_A, m_B, m_Cinv, sv_min_r=1e-8) :
    """ find p: chi2->min, chi2 = (A.p-y)^T . C^{-1} . (A.p-y), 
        with p satisfying constraint B.p = b
        p = W . S^{-1} . U^T + W' . a, 
        a = [(A . W')^T . C^{-1} . (A . W')]^{-1} . (A . W')^T . C^{-1} . [y - A . W . S{-1} . U^T . b]
        where B = U . S . W^T, and W' is complement to W : W^T . W' = 0
        
        return A1, B1, X1: 
        solution p = A1.y + B1.b + X1.x, where x is random
    """
    # reduce B to rank
    n_eqn, n_p = m_A.shape
    assert(m_B.shape[1] == n_p)
    m_Bpinv, m_Bcomp = pinv_complement(m_B, sv_min_r=sv_min_r)
    
    n_c = n_p - m_Bcomp.shape[1]
    if n_c <= 0 :  # fall back to no-constraint
        print("fallback to lhpd.fitter.linsolver_od_matr")
        m_A1, m_X1 = linsolver_od_matr(m_A, m_Cinv, sv_min_r=sv_min_r)
        m_B1 = np.zeros_like(m_B.T)
    else :
        m_A_Bcomp       = np.dot(m_A, m_Bcomp)
        m_ABcompT_Cinv  = np.dot(m_A_Bcomp.T, m_Cinv)
        m_S             = np.dot(m_ABcompT_Cinv, m_A_Bcomp)  # "system" matrix
        m_Spinv, m_Scomp= pinv_complement(m_S, sv_min_r=sv_min_r)
        m_A1            = np.dot(m_Bcomp, np.dot(m_Spinv, m_ABcompT_Cinv))
        m_B1            = np.dot(np.identity(n_p) -  np.dot(m_A1, m_A), m_Bpinv)
        m_X1            = np.dot(m_Bcomp, m_Scomp)
        
    return m_A1, m_B1, m_X1
