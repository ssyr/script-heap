import math
import numpy as np
import lhpd
from lhpd.fitter.fmin_nlm import fmin_leastsq_lm
from lhpd.fitter import Model_function_Base, reparam_id 



###############################################################################
class modelfunc_Base :
    """ base class for any fit model f(x,p)->y
        provides automatic "vectorized" functions {f,df,ddf}v
        * f, fv         values of function at x, xv[i]
        * df, dfv       values of derivatives with respect to parameter p[j]
                            f(x,p)[j], dfv(xv,p)[i,j]
        * ddf, ddfv     values of Hessians (optional) with respect to param j,k
                            ddf(x,p)[j,k], ddfv(xv,p)[i,j,k]
    """
    def __init__(self)    : self.pname = []
    def plen(self)        : return len(self.pname)
    def f   (self, x,  p) : raise NotImplemented    # []
    def df  (self, x,  p) : raise NotImplemented    # [i_p]
    def ddf (self, x,  p) : raise NotImplemented    # [i_p, i_p]
    def fv  (self, xv, p) : return np.array([ self.f  (x, p) for x in xv ]) # [i_x]
    def dfv (self, xv, p) : return np.array([ self.df (x, p) for x in xv ]) # [i_x, i_p]
    def ddfv(self, xv, p) : return np.array([ self.ddf(x, p) for x in xv ]) # [i_x, i_p, i_p]
    def __repr__(self): return str(self)

###############################################################################
class legacy2modelfunc(modelfunc_Base) :
    def __init__(self, mf_old) : 
        self.mf_old = mf_old
        self.pname = mf_old.par_names_list
    def f   (self, x,  p) : return self.mf_old.f  (x, p)
    def df  (self, x,  p) : return self.mf_old.df (x, p)
    def ddf (self, x,  p) : return self.mf_old.ddf(x, p)
    def fv  (self, xv, p) : return self.mf_old.f_range  (xv, p)
    def dfv (self, xv, p) : return self.mf_old.df_range (xv, p)
    def ddfv(self, xv, p) : return self.mf_old.ddf_range(xv, p)
    def __str__(self)     : return self.mf_old.__str__()

###############################################################################
class modelfunc2legacy(Model_function_Base) :
    def __init__(self, mf) : 
        self.mf = mf_old
        self.par_names_list = mf.pname
    def f        (self, x,  p) : return self.mf.f   (x, p)
    def df       (self, x,  p) : return self.mf.df  (x, p)
    def ddf      (self, x,  p) : return self.mf.ddf (x, p)
    def f_range  (self, xv, p) : return self.mf.fv  (xv, p)
    def df_range (self, xv, p) : return self.mf.dfv (xv, p)
    def ddf_range(self, xv, p) : return self.mf.ddfv(xv, p)

class modelfunc_inline(modelfunc_Base) :
    def __init__(self, pname, func, dfunc, ddfunc=None) :
        self.pname  = list(pname)
        self.func   = func
        self.dfunc  = dfunc
    def f(self, x, p) :     return self.func(x, p)
    def df(self, x, p) :    return self.dfunc(x, p)
    def ddf(self, x, p) :
        if None is self.ddfunc : raise ValueError("ddfunc not set")
        return self.ddfunc(x, p)

###############################################################################
class datafit :
    """ likelihood functionals for modelfunc and data (x, y, {yerr,ycov}) :
        * least-squares vector for LM-type optimizers
        * chi-square for Newton-type optimizers
        TODO extensions
        * other types of likelihood functional
        * scan for confidence regions (chi-square contours, etc)

        fv          least-squares vector
        dfv         p[j]-derivatives of least-squares vector : dfv[i,j]
        ddfv        p[j,k]-Hessian of least-squares vector(optional) : ddfv[i,j,k]
        chi2        chi2=|fv|^2
        dchi2       p[j]-derivative of chi2
        ddchi2      p[j,k]-Hessian of chi2 (optional)
    """
    def __init__(self, 
            modelfunc,          # modelfunc_Base 
            xv,                 # range of (generalized) x-values; must be iterable
            ya, ys,             # y-values and their errors
            ycov=None           # y-values normalized covariance matrix; if None, do uncorrelated fit
            ) :
        """ cov = normalized covariance """
        self.modelfunc    = modelfunc
        self.pname  = modelfunc.pname   # copy ref
        self.xv     = None
        self.ya     = None
        self.ys     = None
        self.ycov   = None
        self.set_all(xv=xv, ya=ya, ys=ys, ycov=ycov)

    def plen(self) : return len(self.pname)
    def vlen(self) : return len(self.xv)
    def ndof(self) : return self.vlen() - self.plen()


    def set_all(self, xv=None, ya=None, ys=None, ycov=None) :
        if None is xv : 
            vlen_ = len(self.xv)
        else : 
            vlen_ = len(xv)
        if not None is ya and len(ya) != vlen_ : raise ValueError
        if not None is ys and len(ys) != vlen_ : raise ValueError
        if not None is ycov and ycov.shape != (vlen_, vlen_) : raise ValueError
        
        self.vlen_  = vlen_
        if not None is xv : self.xv = np.asarray(xv)
        if not None is ya : self.ya = np.asarray(ya)
        if not None is ys : self.ys = np.asarray(ys)

        if not None is ycov : 
            self.ycov = np.asarray(ycov)
            self.ycov_invchol = np.linalg.pinv(np.linalg.cholesky(self.ycov))
        else : 
            self.ycov = None
            self.ycov_invchol = None

    def get_all(self) : 
        return dict(xr=self.xr, ya=self.ya, ys=self.ys, ycov=self.ycov)

    # leastsq vectors
    def fv(self, p) : 
        yv    = (self.modelfunc.fv(self.xv, p) - self.ya) / self.ys
        if None is self.ycov_invchol : return yv
        else : return np.dot(self.ycov_invchol, yv)
    def dfv(self, p) :
        dyv   = self.modelfunc.dfv(self.xv, p) / self.ys[:,None]
        if None is self.ycov_invchol : return dyv
        else : return np.dot(self.ycov_invchol, dyv)
    def ddfv(self, p) :
        ddyv   = self.modelfunc.ddfv(self.xv, p) / self.ys[:,None,None]
        if None is self.ycov_invchol : return ddyv
        else : return np.tensordot(self.ycov_invchol, ddyv, axes=(1,0))

    # chi2 functions
    def chi2(self, p) : 
        yv  = self.fv(p)
        return (yv**2).sum()
    def chi2_m(self, p) : return [ self.chi2(p) ]
    def dchi2(self, p) :
        yv  = self.fv(p)
        dyv = self.dfv(p)
        return 2. * np.dot(dyv.T, yv)
    def ddchi2(self, p) :
        # sic! neglecting second derivative ddv.v
        dyv = self.dfv(p)
        return 2. * np.tensordot(dyv, dyv, axes=(0,0))
    def pcov(self, p) :
        return np.linalg.inv(self.ddchi2(p))

    # wrappers for functions that take args
    def fv_a(self, p, *args)    : return self.fv(p)      
    def dfv_a(self, p, *args)   : return self.dfv(p)     
    def ddfv_a(self, p, *args)  : return self.ddfv(p)
    def chi2_a(self, p, *args)  : return self.chi2(p)
    def dchi2_a(self, p, *args) : return self.dchi2(p)
    def ddchi2_a(self, p, *args): return self.ddchi2(p)

    def __str__(self) : return self.modelfunc.__str__()
    def __repr__(self): return str(self)

###############################################################################
class prior_simple(datafit) :
    """ log-likelihood prior functions
    """
    def pf_normal(self, a, s) : 
        a = float(a)
        s = float(s)
        if s <= 0.  : raise ValueError
        def f(x)    : return (x - a) / s   # least-squares vector component
        def df(x)   : return 1./s
        def ddf(x)  : return 0.
        return (f, df, ddf)
    def pf_lognormal(self, a, s) :
        a = float(a)
        s = float(s)
        if 0. == a  : raise ValueError
        if s  <= 0. : raise ValueError
        def f(x)    : 
            if x * a <= 0 : raise OverflowError  # out of range for prior
            return math.log(x / a) / s
        def df(x)   : 
            if x * a <= 0 : raise OverflowError  # out of range for prior
            return 1. / x / s
        def ddf(x) : 
            if x * a <= 0 : raise OverflowError  # out of range for prior
            return -1. / x**2 / s
        return (f, df, ddf)

    def vlen(self) : return len(self.prior_f)
    def plen(self) : return len(self.prior_f)
    def add(self, pname, prior_type, *param) :
        """
            prior_types
            normal,N    (center,width) P[X]~ exp[-(X-center)^2 / (2*width^2)]
            lognormal,logN   
                        (center,logwidth) P[ln(X)]~ exp[-(ln(X) - ln(center))^2 / (2*logwidth^2]
                        NOTE E[X]   = center*exp[logwidth^2/2], 
                             mode[X]= center*exp[-logwidth]
                             var[X] = center^2 * exp[logwidth^2] * (exp[logwidth^2] - 1)
            TODO        TODO
            lognormal_shift(shift,a,s) : X ~ lognormal(X-shift, a, s)
        """
        if 0 != self.pname.count(pname) :
            raise KeyError("%s exists" % pname)
        self.pname.append(pname)
        self.pdict[pname] = (prior_type, param)
        if    "normal" == prior_type or "N" == prior_type :
            prior_f = self.pf_normal(*param)       # center, width
        elif  "lognormal" == prior_type or "logN" == prior_type :
            prior_f = self.pf_lognormal(*param)    # center, logwidth
        else : raise ValueError("unknown prior type '%s'" % prior_type)
        self.prior_f.append(prior_f)
        self.vlen_ += 1
            

    def __init__(self, *spec) :
        self.pname  = []
        self.pdict  = {}
        self.prior_f= []
        self.vlen_  = 0
        for param in spec :
            pname, prior_type = param[:2]
            self.add(pname, prior_type, *param[2:])

    def fv(self, p) :
        res = np.zeros((self.vlen(),), np.float64)
        for i, f in enumerate(self.prior_f) :
            res[i] = f[0](p[i])
        return res
    def dfv(self, p) :
        res = np.zeros((self.vlen(), self.vlen()), np.float64)
        for i, f in enumerate(self.prior_f) :
            res[i, i] = f[1](p[i])
        return res
    def ddfv(self, p) :
        res = np.zeros((self.vlen(), self.vlen(), self.vlen()), np.float64)
        for i, f in enumerate(self.prior_f) :
            res[i, i, i] = f[2](p[i])
        return res
    def param_str(self, p) :
        if isinstance(p, tuple) :
            return ', '.join([ '%.3e' % x for x in p ])
        else : return str(p)
    def __str__(self) : 
        return "[ %s ]" % (
                    ' && '.join([ "%s=%s(%s)" % (pn, self.pdict[pn][0], self.param_str(self.pdict[pn][1]))
                                for pn in self.pname ]) )

# specialized priors for groups of parameters
# TODO add cprior_lambda as array
class prior_lnorm_rel0(datafit):
    """ lognormal prior for ratio of parameters to param[0] """
    def __init__(self, pname, cprior_lambda) :    
        self.pname = pname
        self.cp_l  = cprior_lambda
    def vlen(self) : return self.plen()-1
    def fv(self, p): 
        assert(self.plen() == len(p))
        pc  = np.array(p)
        if (pc <= 0).any(): 
            raise OverflowError
        return np.log(pc[1:] / pc[0]) / self.cp_l
    def dfv(self, p): 
        assert(self.plen() == len(p))
        pc  = np.array(p)
        if (pc <= 0).any(): 
            raise OverflowError
        rpc = 1. / (pc * self.cp_l)
        dpc = np.empty((self.vlen(), self.plen()), pc.dtype)
        dpc[:,0]= - rpc[0] 
        dpc[:,1:] = np.diag(rpc[1:])
        return dpc
    def __str__(self) :
        return "{%s}/%s=lnorm(1,%s)" % (','.join(self.pname[1:]), self.pname[0], self.cp_l)

# TODO add cprior_lambda as array
class prior_lnorm_rel0_abs0(datafit):
    """ lognormal prior for ratio of parameters to param[0] && param[0]/prior """
    def __init__(self, pname, cprior, cprior_lambda) :
        self.pname = pname
        self.cp    = cprior
        self.cp_l  = cprior_lambda
    def vlen(self) : return self.plen()  
    def fv(self, p): 
        assert(self.plen() == len(p))
        pc  = np.array(p)
        if (pc <= 0).any(): 
            raise OverflowError
        pc[1:] /= pc[0]
        pc[0]  /= self.cp
        return np.log(pc) / self.cp_l
    def dfv(self, p): 
        assert(self.plen() == len(p))
        pc  = np.array(p)
        if (pc <= 0).any(): 
            raise OverflowError
        rpc = 1. / (pc * self.cp_l)
        dpc = np.diag(rpc)
        dpc[1:,0]= - rpc[0]
        return dpc
    def __str__(self) :
        return "{%s/%s,{%s}/%s}=lnorm(1,%s)" % (
            self.pname[0], self.cp, ','.join(self.pname[1:]), self.pname[0], self.cp_l)


###############################################################################
# wrapper with pmap
# TODO init with pname list
class datafit_pmap(datafit) :
    def __init__(self, fv_, pmap) :
        self.fv_    = fv_
        self.pmap   = pmap
        assert(len(pmap) == fv_.plen())


    def plen(self) : return self.fv_.plen()
    def vlen(self) : return self.fv_.vlen()
    def ndof(self) : return self.fv_.ndof()
    

    def set_all(self, xv=None, ya=None, ys=None, ycov=None) :
        self.fv_.set_all(xv=None, ya=None, ys=None, ycov=None)
    def get_all(self) : 
        return self.fv_.get_all()

    def fv(self, p) :       return self.fv_.fv(p[self.pmap])
    def dfv(self, p) :      return self.fv_.dfv(p[self.pmap])
    def ddfv(self, p) :     return self.fv_.ddfv(p[self.pmap])
    def chi2(self, p) :     return self.fv_.chi2(p[self.pmap])
    def dchi2(self, p) :    return self.fv_.dchi2(p[self.pmap])
    def ddchi2(self, p) :   return self.fv_.ddchi2(p[self.pmap])

    def pcov(self, p) :     return self.fv_.pcov(p[self.pmap])

    def fv_a(self, p, *args)    : return self.fv_.fv(    p[self.pmap], *args)
    def dfv_a(self, p, *args)   : return self.fv_.dfv(   p[self.pmap], *args)
    def ddfv_a(self, p, *args)  : return self.fv_.ddfv(  p[self.pmap], *args)
    def chi2_a(self, p, *args)  : return self.fv_.chi2(  p[self.pmap], *args)
    def dchi2_a(self, p, *args) : return self.fv_.dchi2( p[self.pmap], *args)
    def ddchi2_a(self, p, *args): return self.fv_.ddchi2(p[self.pmap], *args)

    def __str__(self) :     return self.fv_.__str__()
    def __repr__(self) :     return self.fv_.__repr__()


###############################################################################
class datafit_join(datafit) :
    """ wrapper for joint likelihood funcionals of several datafit instances
        * joint fits
        * priors
    """
    def add_fitp(self, new_pname) :
        for k in new_pname :
            if self.pname.count(k) <= 0 : 
                self.pname.append(k)

    def add(self, mf, pname=None, newfitp=False) :
        """ 
                newfitp=True : 
                        update pname(+join)=mf_pname
                        useful for accumulating parameters
                mf_pname
                        if None, take from mf.pname
        """
        i = len(self.m)
        self.m.append(mf)

        if None is pname : 
            pname = mf.pname
        else : assert(len(pname) == mf.plen()) # 1to1 map mf_pname <-> mf.pname

        if newfitp :
            self.add_fitp(pname)

        self.mlo.append(self.vlen_)
        self.vlen_  += mf.vlen()
        self.mhi.append(self.vlen_)

        pmap = np.empty((mf.plen(),), int)
        for k, n in enumerate(pname) :
            if 1 != self.pname.count(n) : raise KeyError(n)
            pmap[k] = self.pname.index(n)
        self.pmap.append(pmap)

    def set_all(self) : raise NotImplementedError   # implement for sub-objects
    def get_all(self) : raise NotImplementedError   # implement for sub-objects
    def __init__(self, pname, *modelfunc_spec) :
        """ 
                pname       preferred order for parameters
                            if set to None or [], must be populated from 
                            modelfunc_spec, otherwise KeyError's will happen
                modelfunc_spec list of either 
                            * modelfunc | (modelfunc,) : self.pname is populated 
                              with modelfunc.pname
                            * (modelfunc, pname) : pname is populated with pname;
                              useful for renaming/reordering parameters
        """
        self.pname = pname or []
        self.m   = []
        self.vlen_= 0
        self.mlo = []
        self.mhi = []
        self.pmap= []
        for mf_spec in modelfunc_spec :
            if (isinstance(mf_spec, tuple)) :
                if (1 == len(mf_spec)) :
                    self.add(mf_spec[0], pname=None, newfitp=True)
                elif (2 == len(mf_spec)) :
                    self.add(mf_spec[0], pname=mf_spec[1], newfitp=False)
                else : raise ValueError("bad modelfunc spec")
            else : 
                self.add(mf_spec, pname=None, newfitp=True)
    
    def vlen(self) : return self.vlen_

    # overload leastsq and chi2 functions to group models together
    def get_elem(self, i) :
        return datafit_pmap(self.m[i], self.pmap[i])
    def fv(self, p) :
        p   = np.asarray(p)
        assert(self.plen() == len(p))
        yv  = np.zeros((self.vlen(),), np.float64)
        for i, m in enumerate(self.m) :
            yv[self.mlo[i] : self.mhi[i]] = m.fv(p[self.pmap[i]])
        return yv
    def dfv(self, p) :
        p   = np.asarray(p)
        assert(self.plen() == len(p))
        dyv = np.zeros((self.vlen(), self.plen()), np.float64)
        for i, m in enumerate(self.m) :
            dyv[self.mlo[i] : self.mhi[i], self.pmap[i] ] = m.dfv(p[self.pmap[i]])
        return dyv
    def ddfv(self, p) :
        p   = np.asarray(p)
        assert(self.plen() == len(p))
        ddyv = np.zeros((self.vlen(), self.plen(), self.plen()), np.float64)
        for i, m in enumerate(self.m) :
            ddyv[self.mlo[i] : self.mhi[i], self.pmap[i], self.pmap[i] ] = m.ddfv(p[self.pmap[i]])
        return dyv
    def chi2_m(self, p) :
        p   = np.asarray(p)
        assert(self.plen() == len(p))
        return [ m.chi2(p[self.pmap[i]]) for i, m in enumerate(self.m) ]
    def chi2(self, p) :
        return sum(self.chi2_m(p))
    def dchi2(self, p) :
        p   = np.asarray(p)
        assert(self.plen() == len(p))
        dchi2 = np.zeros((self.plen(),), np.float64)
        for i, m in enumerate(self.m) :
            dchi2[self.pmap[i]] += m.dchi2(p[self.pmap[i]])
        return dchi2
    def ddchi2(self, p) :
        p   = np.asarray(p)
        assert(self.plen() == len(p))
        ddchi2 = np.zeros((self.plen(), self.plen()), np.float64)
        for i, m in enumerate(self.m) :
            ddchi2[self.pmap[i], self.pmap[i]] += m.ddchi2(p[self.pmap[i]])
        return ddchi2
    def __str__(self) : 
        return "[ %s ]" % (' && '.join([ str(df) for df in self.m]) )
        

###############################################################################
class fitter2 : 
    """ rehash of lhpd.fitter.fitter
        functionality:
        * common gateway to different fitting methods
        * reparameterization
        * likelihood functions
        * iteration over ensembles
    """
    def __init__(self, mfv, 
                 reparam=reparam_id(),  # initialize as a dictionary by parameter name
                 prior=None) :  # TODO implement priors 
                                #      OR have them as a submodel in datafit_join ?
        """ 
            mfv     class with methods 
                    chi2(p)=function to optimize, dchi2(p) = d chi2 / d p = gradient[i_p]
                    fv(p)=vector for leastsq, dfv=d fv / d p = gradient[i_vec, i_p]
            reparam class performing the reparameterization
            prior   priors in the form ... TODO
        """
        self.mfv    = mfv
        if (not None is reparam): self.reparam = reparam
        else: self.reparam = reparam_id()
        self.last_res   = None
        self.last_method= None
        # fit stat must have "niter" keyword
        self.fit_stat   = {}

    def reset_stat(self) : self.fit_stat = {}
    def print_stat(self) : 
        kprev = set()
        res_str = ""
        if 0 == len(self.fit_stat.keys()) : 
            print("FIT_STAT EMPTY")
            return
        for k in ['niter'] :
            res_str = "%s=%s" % (k, lhpd.stat_str(self.fit_stat[k], fmt='%d'))
            kprev.add(k)
        for k in ['fcalls', 'gcalls'] : 
            if k in self.fit_stat :
                res_str = "%s  %s=%s" % (res_str, k, lhpd.stat_str(self.fit_stat[k], fmt='%d'))
                kprev.add(k)
        for k in self.fit_stat.keys() : 
            if not k in kprev and not k.startswith('hist_') :
                res_str = "%s  %s=%s" % (res_str, k, lhpd.stat_str(self.fit_stat[k], fmt='%d'))
                kprev.add(k)
        print(res_str)
    def fv_(self, ext_p) : 
        # TODO add priors here
        return self.mfv.fv(ext_p)
    def func_dfv_(self, ext_p) : 
        # TODO add priors here
        return self.mfv.dfv(ext_p)   
    def chi2(self, ext_p) : 
        # TODO add priors here
        return (self.fv_(ext_p)**2).sum() 

    def vlen(self) : return self.mfv.vlen()
    def plen(self) : return self.mfv.plen()
    def ndof(self) : return (self.vlen() - self.plen() + self.reparam.dim_fixed())

    # TODO add / copy minuit
    def lma_scipy(self, ext_p0, opt_kw={}) :
        """ use scipy.optimize.leastsq """
        def f_vec(int_p) :
            ext_p = self.reparam.f(int_p)
            return self.fv_(ext_p)

        def Df_matr(int_p) :
            ext_p = self.reparam.f(int_p)
            int_dfv = self.func_dfv_(ext_p)
            jac = self.reparam.df(int_p) # [iext, jint] = d p[iext] / d q[jint]
            if not None is jac : return df
            else : return np.dot(int_dfv, jac.T)

        int_p0  = self.reparam.inv_f(ext_p0).copy()
        lma_res = optimize.leastsq(f_vec, int_p0, Dfun=Df_matr, **opt_kw)
        int_popt= lma_res[0]
        ext_popt= self.reparam.f(int_popt)
        jac     = self.reparam.df(int_popt)
        if (jac is None)            : ext_cov = lma_res[1]
        elif (None is lma_res[1])   : ext_cov = None
        else : ext_cov  = np.dot(jac, np.dot(lma_res[1], jac.T))
        self.last_res = (ext_popt, ext_cov) + lma_res[2:]
        self.last_method = 'lma_scipy'
        return self.last_res

    def lma_scipy_nodiff(self, ext_p0, opt_kw={}) :
        """ returns the same as scipy.optimize.leastsq """
        def f_vec(int_p):
            ext_p = self.reparam.f(int_p)
            return self.fv_(ext_p)
        int_p0  = self.reparam.inv_f(ext_p0).copy()
        lma_res = optimize.leastsq(f_vec, int_p0, **opt_kw)
        int_popt= lma_res[0]
        ext_popt= self.reparam.f(int_popt)
        jac     = self.reparam.df(int_popt)
        if (jac is None)            : ext_cov = lma_res[1]
        elif (None is lma_res[1])   : ext_cov = None
        else: ext_cov  = np.dot(jac, np.dot(lma_res[1], jac.T))
        self.last_res = (ext_popt, ext_cov) + lma_res[2:]
        self.last_method = 'lma_nodiff'
        return self.last_res
    
    def nlm(self, ext_p0, opt_kw={}) :
        """ use Ethan's optimizer """
        def f_vec_a(int_p, *arg) :
            ext_p = self.reparam.f(int_p)
            return self.fv_(ext_p)

        def Df_matr_a(int_p, *arg) :
            ext_p = self.reparam.f(int_p)
            jac = self.reparam.df(int_p) # [iext, jint] = d p[iext] / d q[jint]

            int_dfv = self.func_dfv_(ext_p)
            if None is jac : return int_dfv
            else : return np.dot(int_dfv, jac.T)

        int_p0  = self.reparam.inv_f(ext_p0).copy()
        int_popt, fit_stat = fmin_leastsq_lm(f_vec_a, int_p0, Df_matr_a, **opt_kw)
        lhpd.dictlist_update(self.fit_stat, fit_stat)
        ext_popt= self.reparam.f(int_popt)
        ext_cov = None
        self.last_res = (ext_popt, ext_cov)
        self.last_method = 'nlm'
        return self.last_res


    def fit(self, p0=None, method='nlm', opt_kw={}):
        if not None is method :  cur_method = method
        elif not None is self.last_method: cur_method = self.last_method
        else: raise ValueError('optimizer method not set')
        if not None is p0 : cur_p0 = np.asarray(p0)
        elif not None is self.last_res[0]: cur_p0 = np.asarray(self.last_res[0])
        else: raise ValueError('starting value not set')

        if   cur_method == 'nlm'        : res = self.nlm(cur_p0, opt_kw=opt_kw)
        elif cur_method == 'lma_scipy'  : res = self.lma_scipy(cur_p0, opt_kw=opt_kw)
        elif cur_method == 'lma_nodiff' : res = self.lma_scipy_nodiff(cur_p0, opt_kw=opt_kw)
        #elif cur_method == 'minuit'     : res = self.minuit2_migrad(cur_p0, opt_kw=opt_kw)
        #elif cur_method == 'cg' : res   = self.cg(cur_p0, opt_kw=opt_kw)
        #elif cur_method == 'ncg': res   = self.ncg(cur_p0, opt_kw=opt_kw)
        else: raise ValueError('unknown method %s' % cur_method)
        res_chi2 = self.chi2(res[0])
        return (res[0], res[1], res_chi2)

    # XXX this does not change the underlying model! 
    # call self.mfv.set_all to modify the underlying functions
    def fit_savestate(self, p0=None, method='nlm', opt_kw={}) :
        """ perform a series of fitting using given ensemble
            y_avg_ens       list of data avg.
            optim_method    optimization method to use
            returns list of parameter vectors
        """
        last_res, last_method = self.last_res, self.last_method
        cur_res = self.fit(p0, method, opt_kw=opt_kw)
        self.last_res, self.last_method = last_res, last_method
        return cur_res


    # XXX cannot do fit_ensemble - iterate externally
    # XXX cannot do def fit_savestate_iter - iterate externally

