from past.builtins import basestring
import numpy
import math
import sys

"""
    func                function to apply to data when adding
    rsplan              resampling plan
    title               list of names to prepend when printing results
    head                first line to print (usually comment on the values)
methods
    add(parm_dict)      add to accumulator
    estimate()          return (avg = sum / cnt , cov = sqsum / cnt - avg * avg)
    clear()             reset state of accumulator to zero
    print([fo=],[titles=])  print accumulated statistics
"""
class gauss_estimator:
    """ accumulator with gaussian statistics estimators """
    def __init__(self, func=None, rsplan=None, title=None, head=None):
        self.rsplan  = rsplan
        self.func   = func
        self.title  = title
        self.head   = head
        self.cnt    = 0
        self.sum    = None
        self.sqsum  = None

    def add(self, parm):
        if (not self.func is None): 
            vec = numpy.array(self.func(parm))
        else: vec = parm
        if (self.cnt <= 0): 
            self.cnt    = 1
            self.sum    = vec.copy()
            self.sqsum  = numpy.outer(vec, vec)
        else:
            self.cnt    += 1
            self.sum    += vec
            self.sqsum  += numpy.outer(vec, vec)

    def get_avg(self):  
        assert 0 < self.cnt
        return self.sum / self.cnt
    def get_err_coeff(self, rsplan_name):
        if   (rsplan_name == 'av'): return 1. / math.sqrt(self.cnt - 1)
        elif (rsplan_name == 'jk'): return math.sqrt(self.cnt - 1)
        elif (rsplan_name == 'bs'): return 1.
        else: raise ValueError('unknown rsplan "%s"' % rsplan_name)
    def get_err(self):  
        assert 1 < self.cnt
        err = numpy.array([ math.sqrt(self.sqsum[i,i] / self.cnt - (self.sum[i] / self.cnt)**2)
                    for i in range(len(self.sum)) ], dtype=numpy.float64)
        return err * self.get_err_coeff(self.rsplan[0])
    def estimate(self):
        cnt = self.cnt
        if (cnt <= 0): raise ValueError('empty accumulator')
        avg = self.sum / cnt
        n   = len(avg)
        if (cnt <= 1): return (avg, numpy.zeros(shape=(n,n), dtype=float64))
        cov = (self.sqsum / cnt - numpy.outer(avg, avg))
        return (avg, cov * self.get_err_coeff(self.rsplan[0])**2)

    def clear(self):
        self.cnt    = 0
        self.sum    = None
        self.sqsum  = None

    def write(self, fo=None, print_cov=True, prettyprint=True):
        """ print   [title[i]]   avg[i]   stdev[i]   norm_cov[i,j<=i] """
        if fo is None: fo = sys.stdout
        avg, cov = self.estimate()
        err = [ math.sqrt(cov[i,i]) for i in range(len(avg)) ]
        assert len(avg) == len(self.title)
        if isinstance(self.head, basestring): fo.write('%s\n' % self.head)
        for i in range(len(avg)):
            if (not self.title is None): fo.write('%-15s\t' % self.title[i])
            if (prettyprint): val_str = lhpd.spprint_ve(val[i], err[i], errmax=316)
            else:   val_str = '%e\t%e' % (avg[i], err[i])
            fo.write('%-31s' % val_str)
            if (print_cov):
                for j in range(i): 
                    fo.write('\t%f' % (cov[i,j] / err[i] / err[j]))
                fo.write('\t1.')
            fo.write('\n')


# TODO add gauss_estimator_nocov : estimate avg, stdev without full cov matrix
# TODO add accumulator for median/CL statistics
# TODO add accumulator 'collect' to remember all resampled values, 
#       to use in subsequent resampled fits

def select(kwords):
    return lambda p: numpy.array([p[name] for name in kwords])


class model_func_range:
    def __init__(self, func, xrange):
        self.xrange = xrange
        self.func = func
        self.par_list = func.par_names()
    def func(parm_dict):
        parm = numpy.array([parm_pict[name] for name in self.par_list])
        return numpy.array([ self.func.f(x, parm) for x in xrange ])
