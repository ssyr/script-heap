from scipy import linalg,optimize

import numpy as np
from .reparam import * 
from ..misc import calc_avg_err
from ..misc import pprint_ve_cov
#TODO std.print_ after each fit sample ala `fit_echo'

#TODO introduce common `data holder' to which all models will address to extract their data
#   it will, eg, hold opened aff descriptors or inclusive data tables in the memory

#TODO for all fitting scripts:
#   1) include `reparam_default' into model: argument to reparam.__init__
#   2) make variant of f_range without flattening over range; 
#       ? its size may be different if e.g. for c2pt f_range is normalized by some c2pt(t0)
#         or there is complex freedom
#   3) `make_data_range' method: standard data holder -> selected data for this fit 
#     to give to fitter, [i_ens][i]
#   4) `size_data_range': number of elements in the sample (everything is real)

#TODO rename:
#   1) all size functions to `size_XXX'
#   2) gaussian_estimate to something shorter: `val_cov'?

#TODO move: 
#   1) fitting functions to separate modules within `fit_proc'
#   2) all estimating functions to 'lhpd.estimate'
#   3) all echo,output,prettyprint,etc... functions to 'lhpd.output'

#TODO introduce constrained fits to fitter:
#   1) takes model vector from model
#   2) adds elements to the vector: deviations of parameters from their constraint central values
#   3) iterates over a sequence of constraint weights
#
#TODO set reparam for existing class; set reparam for a specific parameter?
#     take fixed parameters from reparam


class Model_function_Base:
    """ 
    standard interface for all model functions: class creates an object
    which must have the following methods
        par_names()     list of parameter names, in order
        f(x,param)      value of function at point x with param as parameters
        df(x,param)     same as f, but compute the vector of derivatives wrt parameters
        ddf(x,param)    same as f, but compute the matrix of 2nd derivatives wrt parameters

    """
    """ Contains basic functionality of a model function:
            par_names()         get list of parameter names
            select_param()      return array of parameters, selected from a dictionary
            f(x, param)         value of model function (stub)
            df(x, param)        (analytic) partial derivatives (stub)
            ddf(x, param)       (analytic) hessian (stub)
        Methods df, ddf are optional and required only for congrad-type optimizers
        Also important primitives for parallel computation of model function values
        and derivatives:
            f_df(x, param)      return tuple (f, df) for x
            f_df_ddf(x, param)  return tuple (f, df, ddf) for x
            [f_,
             df_,
             ddf_]range(xrange, param)
                                return array of f, df or ddf values for each x in xrange
            [f_df_,
             f_df_ddf_]range(xrange, param)
                                return tuples of arrays ([f],[df]) or ([f],[df],[ddf])
                                for each x in xrange
        User may want to overload the above group of methods with efficient functions 
        to boost performance
    """
    def __init__(self):             self.par_names_list = []
    def par_names(self):            return self.par_names_list
    def n_param(self):              return len(self.par_names())
    def reparam_default(self):      return [ None for i in range(self.n_param()) ]
    def select_param(self, param_):
        if isinstance(param_, dict) :
            return np.array([ param_[name] for name in self.par_names() ])
        else: return np.asarray(param_)
    def f(self, x, param):          return None
    def df(self, x, param):         return None
    def ddf(self, x, param):        return None
    def f_df(self, x, param):       
        return (self.f(x, param), 
                self.df(x, param))
    def f_df_ddf(self, x, param):   
        return (self.f(x, param), 
                self.df(x, param), 
                self.ddf(x, param))
    def f_range(self, xrange, param):
        #return np.array([ self.f(x, param) for x in xrange ])
        res = np.zeros(shape=(len(xrange),),
                          dtype=np.float64)
        for i,x in enumerate(xrange): res[i] = self.f(x, param)
        return res
    def df_range(self, xrange, param):
        #return np.array([ self.df(x, param) for x in xrange ])
        res = np.zeros(shape=(len(xrange), len(param)), 
                          dtype=np.float64)
        for i,x in enumerate(xrange): res[i] = self.df(x, param)
        return res
    def ddf_range(self, xrange, param):
        #return np.array([ self.ddf(x, param) for x in xrange ])
        res = np.zeros(shape=(len(xrange), len(param), len(param)),
                          dtype=np.float64)
        for i,x in enumerate(xrange): res[i] = self.ddf(x, param)
        return res

    def f_df_range(self, xrange, param):
        return (self.f_range(xrange, param), 
                self.df_range(xrange, param))
    def f_df_ddf_range(self, xrange, param):
        return (self.f_range(xrange, param), 
                self.df_range(xrange, param),
                self.ddf_range(xrange, param))
    def desc(self): return "(no desc)"

class f_wrap(Model_function_Base):
    """ create a model func class from scalar function """
    def __init__(self, func, n_p=-1, p_name=None):
        self.func   = func
        self.n_p    = n_p
        if not None is  p_name : self.par_names_list = list(p_name)
        else: self.par_names_list = None
    def f(self, x, p):          return self.func(x, p)

class fr_wrap(Model_function_Base):
    """ create a model func class from vector function """
    def __init__(self, func, n_p=-1, p_name=None):
        self.func   = func
        self.n_p    = n_p
        if not None is  p_name : self.par_names_list = list(p_name)
        else: self.par_names_list = None
    def f_range(self, xr, p):   return self.func(xr, p)


class Fitter:
    """
    Class performs fitting of a range of data points to a specified function; 
    can use a reparameterization of variables to keep them in range
    """
    # TODO make sure that len(x_range) != len(y_avg) works correctly
    # TODO add fixing parameters in the subordinate reparam
    # TODO add iteration over varying data points: e.g. JK ensemble
    # TODO implement vector mode to function:
    #        range_f(x_range, p) -> [ f(x,p) for x in range(p) ]
    # to save c2pt evaluation in meff, dmeff ('range')
    # TODO implement simultaneous eval. of f, df[, ddf] ('simderiv'): 
    #        f,df = [range_]f_df(x[_range],p) instead of f(x,p), df(x,p)
    # to save additional evaluation of f if it is done each time df is evaluated
    # NB one can stub required 'range' and 'simderiv' by generic wrapping classes
    # which add the methods (study class inheritance, btw)
    #       f_range, df_range, f_df_range,f_df_ddf_range
    # range_capable(c): relying on f, f_df, f_df_ddf
    # simderiv_capable(c): relying on f_range,df_range,ddf_range
    # simderiv_range_capable(c): relying on f, df, ddf
    # truly efficient func-classes may provide efficient versions of the functions
    # f_range, f_df_range, f_df_ddf_range
    
    def __init__(self, func, x_range, y_avg, y_sigma, y_cov=None, 
                 reparam=reparam_id()):
        """ 
            func    class with methods f(x,p)=function, df(x,p)=gradient, ddf(x,p)=hessian 
                    where x is a fit point, p is a fit parameter array
            x_range set of fit points
            y_avg   average values at the fit points, flattened 1d array
            y_sigma std of average values
            y_cov   normalized covariance; if None, set to diag{1,..1}
            reparam class performing the reparameterization
        """
        n = len(y_avg)
        assert (y_avg.shape == (n,) and 
                y_sigma.shape == (n,) and 
                (y_cov is None or y_cov.shape == (n,n)))
        self.func = func
        self.x_range    = np.asarray(x_range)
        self.y_avg      = np.asarray(y_avg)
        self.y_sigma    = np.asarray(y_sigma)
        if (y_cov is None): self.y_cov = np.identity(n, dtype=np.float64)
        else:               
            self.y_cov  = np.asarray(y_cov)
            n_var       = len(self.y_avg)
            assert self.y_cov.shape == (n_var, n_var)
        if (not None is  reparam): self.reparam = reparam
        else: self.reparam = reparam_id()
        self.last_res = None
        self.last_method = None
    
    """ int[ernal] refers to vars transferred to/from the minimizing algorithm
        ext[ernal] refers to vars given by user, transferred to the function, returned to user
        u = ext = self.reparam.f(int=v)         
        v = self.reparam.inv_f(u)
        J[i, j] = \partial u_i / \partial v_j = self.reparam.df(v)[j,i]
        \partial^2 u_i / \partial v_j / \partial v_k = self.reparam.ddf(v)[i,j,k]
    """

    def minuit2_migrad(self, ext_p0):
        err_mat = np.linalg.inv(self.y_cov)
        def f_chi2(int_p):
            ext_p = self.reparam.f(int_p)
            vec     = (self.func.f_range(self.x_range, ext_p) - self.y_avg) / self.y_sigma
            return np.dot(vec, np.dot(err_mat, vec))
        def f_wrapper(n, f):
            if n < 0 or 10 < n: raise ValueError
            elif 0 == n: return lambda :                        f(np.array([]))
            elif 1 == n: return lambda v0:                      f(np.array([v0]))
            elif 2 == n: return lambda v0,v1:                   f(np.array([v0,v1]))
            elif 3 == n: return lambda v0,v1,v2:                f(np.array([v0,v1,v2]))
            elif 4 == n: return lambda v0,v1,v2,v3:             f(np.array([v0,v1,v2,v3]))
            elif 5 == n: return lambda v0,v1,v2,v3,v4:          f(np.array([v0,v1,v2,v3,v4]))
            elif 6 == n: return lambda v0,v1,v2,v3,v4,v5:       f(np.array([v0,v1,v2,v3,v4,v5]))
            elif 7 == n: return lambda v0,v1,v2,v3,v4,v5,v6:    f(np.array([v0,v1,v2,v3,v4,v5,v6]))
            elif 8 == n: return lambda v0,v1,v2,v3,v4,v5,v6,v7:         f(np.array([v0,v1,v2,v3,v4,v5,v6,v7]))
            elif 9 == n: return lambda v0,v1,v2,v3,v4,v5,v6,v7,v8:      f(np.array([v0,v1,v2,v3,v4,v5,v6,v7,v8]))
            elif 10== n: return lambda v0,v1,v2,v3,v4,v5,v6,v7,v8,v9:   f(np.array([v0,v1,v2,v3,v4,v5,v6,v7,v8,v9]))
        def mat_dict2np(n_p, d):
            res     = np.empty((n_p, n_p), dtype=np.float64)
            for i in range(n_p):
                for j in range(n_p):
                    res[i, j] = d[('v%d' % i, 'v%d' % j)]
            return res
        def vec_np2dict(v):
            return dict([ ( 'v%d' % i, int_p0[i]) for i in range(n_p) ])

        import minuit2
        int_p0  = self.reparam.inv_f(ext_p0).copy()
        n_p     = len(int_p0)
        assert n_p <= 10
        m       = minuit2.Minuit2(f_wrapper(n_p, f_chi2), **vec_np2dict(int_p0))
        m.migrad()
        m.hesse()
        int_p   = np.array(m.args)
        ext_p   = self.reparam.f(int_p)
        jac     = self.reparam.df(int_p).T      # sic! after jacobian fix 2016/05/25
        ext_cov = mat_dict2np(n_p, m.covariance)
        if not None is  jac: ext_cov = np.tensordot(jac, 
                               np.tensordot(jac, ext_cov, axes=(0,0)),
                               axes=(0,1)) 
        return (ext_p, ext_cov, m.fval)

    def lma_nodiff(self, ext_p0):
        """ returns the same as scipy.optimize.leastsq """
        chol_cov = linalg.inv(linalg.cholesky(self.y_cov))
        def f_vec(int_p):
            ext_p = self.reparam.f(int_p)
            vec = (self.func.f_range(self.x_range, ext_p) - self.y_avg) / self.y_sigma
            return np.dot(chol_cov.transpose(), vec)
        int_p0  = self.reparam.inv_f(ext_p0).copy()
        lma_res = optimize.leastsq(f_vec, int_p0, full_output=1)
        ext_popt= self.reparam.f(lma_res[0])
        jac     = self.reparam.df(lma_res[0]) 
        if (jac is None): ext_cov = lma_res[1]
        elif (None is lma_res[1]): ext_cov = None
        else: ext_cov  = np.tensordot(jac.T, np.tensordot(jac.T, # sic! T after jacobian fix 2016/05/25
                                   lma_res[1], axes=(0,0)),
                                   axes=(0,1))
        self.last_res = (ext_popt, ext_cov) + lma_res[2:]
        self.last_method = 'lma_nodiff'
        return self.last_res
    def lma(self, ext_p0):
        """ returns the same as scipy.optimize.leastsq """
        chol_cov = linalg.inv(linalg.cholesky(self.y_cov))
        def f_vec(int_p):
            ext_p = self.reparam.f(int_p)
            vec = (self.func.f_range(self.x_range, ext_p) - self.y_avg) / self.y_sigma
            return np.dot(chol_cov.transpose(), vec)
        def Df_matr(int_p):
            ext_p = self.reparam.f(int_p)
            jac = self.reparam.df(int_p).T # sic! after jacobian fix 2016/05/25
            int_df_range = self.func.df_range(self.x_range, ext_p)
            df  = np.zeros(dtype=np.float64, shape=(len(self.y_avg), len(int_p)))
            for i in range(len(self.y_avg)):
                ext_df = int_df_range[i] / self.y_sigma[i]
                if (jac is None): df[i] = ext_df
                else: df[i] = np.dot(jac, ext_df)
            return np.dot(chol_cov.transpose(), df)
        int_p0  = self.reparam.inv_f(ext_p0).copy()
        lma_res = optimize.leastsq(f_vec, int_p0, Dfun=Df_matr,full_output=1)
        ext_popt= self.reparam.f(lma_res[0])
        jac     = self.reparam.df(lma_res[0]).T # sic! after jacobian fix 2016/05/25
        if (jac is None): ext_cov = lma_res[1]
        elif (None is lma_res[1]): ext_cov = None
        else: ext_cov  = np.tensordot(jac, 
                                   np.tensordot(jac, lma_res[1], axes=(0,0)),
                                   axes=(0,1))
        self.last_res = (ext_popt, ext_cov) + lma_res[2:]
        self.last_method = 'lma'
        return self.last_res
    def get_ndf(self):
        return len(self.y_cov) - self.func.n_param() + self.reparam.dim_fixed()
    def get_rcov(self):
        n = len(self.y_avg)
        if (None is self.y_cov):    return np.identity(shape=(n,n), dtype=np.float64)
        else: return linalg.inv(self.y_cov)

    def chi2(self, ext_p, rcov=None):
        """ calculate the chi2 for the set of internal parameters 
            with the inverse data normalized correlation matrix rcov
            return \chi^2(u)
        """
        if rcov is None: rcov = self.get_rcov()
        f = (self.func.f_range(self.x_range, ext_p) - self.y_avg) / self.y_sigma
        return np.dot(f, np.dot(rcov, f))
    def int_chi2(self, int_p, rcov=None): 
        """ return \chi^2(v) """
        return self.chi2(self.reparam.f(int_p), rcov=None)
    def chi2_grad(self, ext_p, rcov=None):
        """ calculate the chi2 gradient for the set of parameters 
            return array[i] = \partial\chi^2(u) / \partial u_i
        """
        # TODO use f_df_range
        f   = (self.func.f_range(self.x_range, ext_p) - self.y_avg) / self.y_sigma
        df  = self.func.df_range(self.x_range, ext_p) / self.y_sigma
        return 2. * np.dot(f, np.dot(rcov, df))
    def int_chi2_grad(self, int_p, rcov=None):
        """ return array[i] = J . chi2_grad """
        ext_p = self.reparam.f(int_p)
        jac = self.reparam.df(int_p).T      # sic! after jacobian fix 2016/05/25
        if (jac is None): return self.chi2_grad(ext_p, rcov)
        else: return np.dot(jac, self.chi2_grad(ext_p, rcov))
    def chi2_hess(self, ext_p, rcov=None):
        """ calculate the chi2 hessian for the set of parameters 
            return 2C_u^{-1}[i,j] = \partial^2\chi^2(u) / \partial u_i / partial u_j
        """
        df  = self.func.df_range(self.x_range, ext_p) / self.y_sigma
        #f  = (self.func.f_range(self.x_range, ext_p) - self.y_avg) / self.y_sigma
        #ddf= self.func.ddf_range(self.x_range, ext_p) / self.y_sigma
        return 2. * np.dot(df.transpose(), 
                              np.dot(rcov, df)) 
        # + 2. * tensordot(dot(f,rcov), ddf, axes=(0,0))
    def int_chi2_hess(self, int_p, rcov=None):  
        """ return 2C_v^{-1} = J . C_v^{-1} . J^T """
        ext_p  = self.reparam.f(int_p)
        jac = self.reparam.df(int_p).T      # sic! after jacobian fix 2016/05/25
        # hess = self.reparam.ddf(int_p)
        if (jac is None): return self.chi2_hess(ext_p, rcov)
        else: return np.dot(jac, 
                               np.dot(self.chi2_hess(ext_p, rcov), 
                                         jac.transpose())) 
        # + tensordot(self.chi2_grad(rcov, ext_p), hess, axes=(0,-1))
    def cg(self, ext_p0):
        """ returns the same as scipy.optimize.leastsq """
        int_p0 = self.reparam.inv_f(ext_p0)
        rcov = self.get_rcov()
        cg_res = optimize.fmin_cg(lambda p: self.int_chi2(p, rcov),
                                  int_p0,
                                  lambda p: self.int_chi2_grad(p, rcov),
                                  full_output=1,
                                  disp=1)
        self.last_res =  (self.reparam.f(cg_res[0]), None, 
                {'fvec': np.array([sqrt(cg_res[1])]), 
                 'nfev': cg_res[2],
                 'njev': cg_res[3]},
                 '<empty>',
                 cg_res[4])
        self.last_method = 'cg'
        return self.last_res
    def ncg(self, ext_p0, avextol=1e-9, disp=0):
        """ returns the same as scipy.optimize.leastsq """
        int_p0 = self.reparam.inv_f(ext_p0)
        rcov = linalg.inv(self.y_cov)
        cg_res =  optimize.fmin_ncg(lambda p: self.int_chi2(rcov, p),
                                    int_p0,
                                    lambda p: self.int_chi2_grad(rcov, p),
                                    fhess = lambda p: self.int_chi2_hess(rcov,p),
                                    avextol=avextol,
                                    full_output=1,
                                    disp=disp)
        self.last_res = (self.reparam.f(cg_res[0]), None, 
                {'fvec': np.array([math.sqrt(cg_res[1])]), 
                 'nfev': cg_res[2],
                 'njev': cg_res[3]},
                 '<empty>',
                 cg_res[5])
        self.last_method = 'ncg'
        return self.last_res
    def chi2_level(self, xi, lev):
        """ find a such that chi^2(p_0 + a xi) = lev+chi^2(p_0)
            this corresponds to the confidence region boundary along xi direction
            p_0 is taken from last_res and assumed to be the point of chi^2 minimum
            TODO: the fixed parameters are kept at their values
        """
        assert (0. < lev) and (0. < (xi**2).sum())
        assert not None is  self.last_res 
        p0      = self.last_res[0] 
        # first approximation from hessian
        rcov    = linalg.inv(self.y_cov)
        c2      = self.chi2(rcov, p0)
        c2hess  = self.chi2_hess(rcov, p0)
        a0      = sqrt(2. * lev / np.dot(xi, np.dot(c2hess, xi)))
        c2_up   = self.chi2(rcov, p0 + a0 * xi)
        while c2_up < c2 + lev:
            a0 *= 2     # that should be a reasonable step; give as a parameter with def. value?
            c2_up   = self.chi2(rcov, p0 + a0 * xi)
        return optimize.brentq(lambda a: self.chi2(rcov, p0 + a * xi) - c2 - lev, 0., a0)
    def minmax(self, lev, func, func_grad):
        """ estimate tuple (min,max) F(param) at chi2 = chi2(p0) + lev 
            estimation relies on the gradient of the function at p0
            return (F_min, F_max) at chi2 = chi2(p0) + lev, F_min < F_max
        """
        assert 0. < lev
        assert not None is  self.last_res 
        p0      = self.last_res[0] 
        rcov    = linalg.inv(self.y_cov)
        inv_c2hess = linalg.inv(self.chi2_hess(rcov, p0))
        iHgrad  = np.dot(inv_c2hess, func_grad(p0))
        assert 0. < (iHgrad**2).sum()
        func1   = func(p0 + iHgrad * self.chi2_level(iHgrad, lev))
        func2   = func(p0 - iHgrad * self.chi2_level((-1)*iHgrad, lev))
        if func1 < func2: return (func1, func2)
        else: return (func2, func1)
    def minmax_func(self, lev, x):
        """ estimate tuple (min,max) of the fitting function at 'x': f(x) 
            at p: chi2(p) = chi2(p0) + lev 
            return (f_min, f_max), f_min < f_max
        """
        return self.minmax(lev, 
                           lambda p: self.func.f(x,p), 
                           lambda p: self.func.df(x,p))
    def minmax_param(self, lev):
        """ estimate range for fitting parameters for chi2 level = 'lev' """
        assert not None is  self.last_res 
        res = []
        np  = len(self.last_res[0])
        for i in range(np):
            i_grad = np.zeros(dtype=np.float64, shape=(np,))
            i_grad[i] = 1.
            res.append(self.minmax(lev,
                                   lambda p: p[i],
                                   lambda p: i_grad))
        return res

    def fit(self, p0=None, method='lma'):
        if not method is None:  cur_method = method
        elif not self.last_method is None: cur_method = self.last_method
        else: raise ValueError('optimizer method not set')
        if not p0 is None: cur_p0 = np.asarray(p0)
        elif not self.last_res[0] is None: cur_p0 = np.asarray(self.last_res[0])
        else: raise ValueError('starting value not set')

        if cur_method == 'lma'  : res   = self.lma(cur_p0)
        elif cur_method == 'cg' : res   = self.cg(cur_p0)
        elif cur_method == 'ncg': res   = self.ncg(cur_p0)
        elif cur_method == 'lma_nodiff' : res = self.lma_nodiff(cur_p0)
        elif cur_method == 'minuit'     : res = self.minuit2_migrad(cur_p0)
        else: raise ValueError('unknown method %s' % cur_method)
        res_chi2 = self.chi2(res[0])
        return res[0], res_chi2

        
    def fit_savestate(self, y, p0 = None, method='lma'):
        """ perform a series of fitting using given ensemble
            y_avg_ens       list of data avg.
            optim_method    optimization method to use
            returns list of parameter vectors
        """
        save_last_res, save_y_avg   = self.last_res, self.y_avg
        self.y_avg  = y
        cur_res, cur_chi2 = self.fit(p0, method)
        self.last_res, self.y_avg   = save_last_res, save_y_avg
        return cur_res, cur_chi2

    def fit_ensemble(self, y_rs, p0=None, method='lma'):
        """ cycle over an ensemble, return n_data x np array """
        save_last_res, save_y_avg   = self.last_res, self.y_avg

        n_data = len(y_rs)
        chi2_rs = np.empty((n_data,), float)
        p_rs    = np.empty((n_data, len(save_last_res[0])), save_last_res[0].dtype)
        for i, y in enumerate(y_rs):
            self.y_avg  = y
            p_rs[i], chi2_rs[i] = self.fit(p0, method)

        self.last_res, self.y_avg   = save_last_res, save_y_avg
        return p_rs, chi2_rs

    def fit_savestate_iter(self, y_ens, p0=None, method='lma'):
        """ iterator over ensemble, similar to fit_savestate """
        save_last_res, save_y_avg   = self.last_res, self.y_avg
        for y in y_ens:
            # restore state, fit and yield
            self.y_avg, self.last_res = y, save_last_res
            yield self.fit(p0, method)
        self.y_avg, self.last_res = save_y_avg, save_last_res

    def band_xrange(self, n_x): 
        return np.r_[self.x_range.min() : self.x_range.max() : n_x * 1j]

    def band(self, p_avg, p_err, p_ncov=None, n_x=100, x_band=None):
        """ calculate band: return (y_lo, y_hi) varying parameters 
            along e.vectors of cov.matrix (probe axes of the "errorbar" hyperellipse
            n_x         number of (evenly spaced)samples in band
            x_band      if set, overrides n_x
            FIXME
        """
        if not None is  x_band:  n_x = len(x_band)
        else: x_band = self.band_xrange(n_x)
        n_p = len(p_avg)

        if None is p_ncov: p_ncov = np.identity(len(n_p), float)
        w,v = np.linalg.eigh(p_ncov)

        y   = np.empty((2 * n_p, n_x), np.float64)
        for i in range(n_p):
            dp  = p_err * v[:,i] * math.sqrt(w)
            y[2 * i]    = self.func.f_range(x_band, p_avg - dp)
            y[2 * i + 1]= self.func.f_range(x_band, p_avg + dp)
        return y.min(0), y.max(0)
    
    def band_ensemble(self, p_rs, rsplan, n_x=100, x_band=None):
        if not None is  x_band:  n_x = len(x_band)
        else: x_band = self.band_xrange(n_x)
        n_data, n_p = p_rs.shape

        y   = np.r_[ [ self.func.f_range(x_band, p) for p in p_rs ] ]
        y_avg, y_err = calc_avg_err(y, rsplan)
        return y_avg - y_err, y_avg + y_err

    def print_res(self): 
        lhpd.pprint_ve_cov(self.last_res[0], self.last_res[1], self.func.par_names())


    
import sys
def fit_echo(cnt, p, chi2):
    str = ('%+.4e' % p[0])
    for pi in p[1:]: str += (' %+.4e' % pi)
    return ('%05d' % cnt) + ' [ ' + str + ' ] ' + ('%.5f' % chi2)

def exp10(pwr):
    abspwr = abs(pwr)
    res = 1
    a = 10
    while 0 < abspwr:
        if abspwr % 2 == 1: res *= a
        abspwr /= 2
        a *= a
    if (0 <= pwr): return res
    else: return 1. / res

def print_lma_res(lma_res, cov=False):
    """ debug print_ function """
    sys.stdout.write('[%d]: %s\n' % (lma_res[4], lma_res[3]))
    sys.stdout.write('fcn\t= %f\n' % ((lma_res[2]['fvec']**2).sum(),))
    sys.stdout.write('nfev / njev\t= %d / %d\n' % 
                     (lma_res[2]['nfev'],lma_res[2]['njev']))
    if (None is lma_res[1]): cov = False
    if (cov):
        cmatr = lma_res[1].copy()
        sigma = norm_cov_matrix(cmatr)
    for i,p in enumerate(lma_res[0]):
        sys.stdout.write('(%d)\t%14.7e' % (i, p))
        if (not None is  lma_res[1]): 
            sys.stdout.write('\t(%14.9e)' % sqrt(lma_res[1][i,i]))
        if (cov):
            sys.stdout.write('\t')
            for j in range(1+i): sys.stdout.write('%f\t' % (cmatr[i,j]))
        sys.stdout.write('\n')
        
def tsv_lma_res(lma_res):
    """ debug print_ function """
    n = len(lma_res[0])
    for i in range(n): sys.stdout.write('%14.7e\t' % lma_res[0][i])
    sys.stdout.write('%14.7e\t' % (lma_res[2]['fvec']**2).sum())
    if (None is lma_res[1]):
        for i in range(n): sys.stdout.write('0.\t')
    else:
        for i in range(n): sys.stdout.write('%14.7e\t' % sqrt(lma_res[1][i,i]))
    sys.stdout.write('0.\n')
        
        
