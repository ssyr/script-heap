import math
import numpy as np
import itertools as it
import lhpd

# TODO needs testing
# one idea is to generate many permutations of fixed size N and compare
# the distribution of p[i] to uniform via KS statistics
def permut_to_seqdraw(p):
    """ "sequential draws": given a permutation p[i], assume that p[i]-th element 
            is removed on step i; before being removed, it is in d[i]-th position
            of the remaining sequence
        convert permutation `p' to "sequential draws" `d'
        p   permutation, len(p)=N, p[i] are unique numbers from [0;N)
        RETURN
        d   "sequential draws", len(d)=N, d[i] is a number from [0;N-i-1)
    """
    N = len(p)
    d = p.copy()
    for i in range(N):
        assert 0 <= p[i] and p[i] < N
        for j in range(i):
            if p[j] < p[i]: d[i] -= 1

    return d

def seqdraw_to_permut(d):
    """ "sequential draws": given a permutation p[i], assume that p[i]-th element 
            is removed on step i; before being removed, it is in d[i]-th position
            of the remaining sequence
        convert "sequential draws" `d' to permutation `p'
        d   "sequential draws", len(d)=N, d[i] is a number from [0;N-i-1)
        RETURN
        p   permutation, len(p)=N, p[i] are unique numbers from [0;N)
    """
    N = len(d)
    p = d.copy()
    dc= d.copy()
    for i in range(N):
        assert 0 <= dc[i] and dc[i] < N - i
        for j in range(i):
            if dc[j] <= dc[i]: p[i] += 1
            else: dc[j] -= 1
    return p
def gen_random_permut(N):
    """ generate a random permutation p[i], len(p)=N, p[i] are unique numbers from [0;N)
    """
    draws = (np.random.rand(N) * range(N,0,-1)).astype(int)
    return seqdraw_to_permut(draws)




def binom_coeff(n,t):
    if n < t : return 0
    if n//2 < t: t = n - t
    a = 1
    for i in range(t):
        b = a * (n - i)
        a = b // (1 + i)
        assert 0 == b % (1 + i)
    return a

def multinom_coeff(k_list) :
    """ multinomial coefficient 
        n! / (k[0]!  ... k[-1]!), where n = sum(k_list) 
    """
    m   = len(k_list)
    if m < 1 : 
        return 0
    elif 1 == m : return 1
    elif 2 == m : return binom_coeff(k_list[0] + k_list[1], k_list[0])

    n   = sum(k_list)
    if n < m : return 0
    return binom_coeff(n, k_list[0]) * multinom_coeff(k_list[1:])


def permut_next(m):
    """ [Dijkstra]
        update permutation `m' in-place to the next permutation 
        m should be a permutation; no checks are performed
    """
    N   = len(m)
    # find i: rightmost local maximum(?)
    i   = N - 1
    while ((0 < i) and (m[i] < m[i-1])): i -= 1 
    if 0 == i: raise StopIteration
    
    # find j: rightmost j, (j<N ? m[j] : None) < m[i-1] <= m[j-1]
    j   = N
    while (m[j-1] <= m[i-1]): j -= 1;
  
    m[i-1], m[j-1] = m[j-1], m[i-1]

    i += 1; j = N;
    while (i < j):
        m[i-1], m[j-1] = m[j-1], m[i-1]
        i += 1
        j -= 1

def permut_iter(n):
    m   = range(n)
    yield m
    while True: 
        permut_next(m)
        yield m

def permut_count(m):
    """ count the number of swaps required(?) to order the permutation """
    n   = len(m)
    cnt = 0
    for i in range(n - 1):
        for j in range(i + 1, n):
            if m[j] < m[i]: cnt += 1
    return cnt

def permut_parity(m): return permut_count(m) % 2

def permut_count_bubble(m):
    """ count permutations in a bubble sort """
    m = list(m)
    n = len(m)
    cnt = 0
    for i in range(n - 1):
        for j in range(n - 1, i, -1):
            if m[j] < m[j-1]:
                m[j-1], m[j] = m[j], m[j-1]
                cnt += 1
    is_sorted = True
    for i in range(n-1):
        if m[i + 1] < m[i]: is_sorted = False

    assert is_sorted

    return cnt


def permut_lex_iter(s_):
    s = list(s_)
    s.sort()
    l = len(s)
    assert 0 < l
    yield s
    if l < 1: raise StopIteration
    while True:
        i = l - 2
        while s[i+1] <= s[i]:
            i -= 1
            if (i < 0): raise StopIteration
        j = l - 1
        while s[j] <= s[i]:
            j -= 1
            assert 0 <= j
        s[i], s[j] = s[j], s[i]
        s[i+1:l] = reversed(s[i+1:l])
        yield s

def factbase_tonumber(m):
    """ decompose a number in factorial system 
        a:  number
        n:  required rank: the tail is padded with zeros or discarded; ignored if None
    """
    a   = 0
    f   = 1
    for i, d in enumerate(m):
        a += d * f
        f *= (i + 1)
    return a
def combination_to_rlex_rank(c):
    """ return rank of permutation 0<=c[0]<c[1]<...<c[-1] 
        the ordered combination [0,1,2,...] corresponds to rank=0
    """
    res = 0
    for i, c in enumerate(c): 
        res += binom_coeff(c, i+1)
    return res
def combination_from_rlex_rank(n, ind):
    """ greedy algorithm to compute combination of length n
        corresponding to rank=ind
    """
    if n <= 0 : return []
    if 1 == n : return [ind]
    c = [None]*n

    i = n - 1
    while 0 <= i and 0 < ind :
        bc = 1
        ci = i + 1
        while ( bc * (ci + 1) <= (ci - i) * ind) :
            bc = (bc * (ci + 1)) // (ci - i)
            ci += 1
        c[i] = ci
        
        ind -= bc
        i -= 1

    c[:i+1] = range(i+1)
    #assert(is_combination(c))
    return c

def factbase_todigits(a, n=None):
    """ decompose a number in factorial system 
        a:  number
        n:  required rank: the tail is padded with zeros or discarded; ignored if None
    """
    m = [ 0 ]   # the first (the least significant) is always zero (a % 1)
    i = 1       
    while 0 < a:
        i += 1
        if not None is  n and n < i: break
        m.append(a % i)
        a //= i
    if not None is  n and i < n:
        m = m + [ 0 ] * (n - i)
    return m
def permut_lex_index_to_parity(ind):
    """ compute the sign of permutation with lex.order ind """
    n = 2
    sum = 0
    while (0 < ind):
        sum += ind % n
        ind//= n
        n   += 1
    return sum

def permut_from_number(a, n):
    """ Dijkstra's order n -> m[] """
    # a-> factorial base:
    # d[0] slowest, d[-2] fastest, d[-1] === 0 (the last remaining choice)
    d   = factbase_todigits(a, n)
    d.reverse()
    # d[i], i=[0..n) selects elements with elimination, from left to right
    for i in range(n - 1, -1, -1):
        for j in range(i + 1, n):
            if d[i] <= d[j]: d[j] += 1  
            # all previous (i<j) occurrences(d[i]) <= current (d[j]) shift 
            # the values of tailing indices by +1
    return d

def permut_to_number(m):
    """ Dijkstra's order m[]->n """
    d   = list(m)
    n   = len(d)
    for j in range(n - 1, -1, -1):
        k   = 0
        for i in range(j):
            if d[i] < d[j]: k += 1
        d[j] -= k
    d.reverse()
    return factbase_tonumber(d)




def combination_rlex_next(c, cmax=None):
    """ update combination c in-place to the next lexicographic:
        0 <= c[0] < c[1] < c[2] < ... < c[-1] < cmax
        generate StopIteration if c is the last one 
        (e.g. no lexicographic-next such that c[-1] < cmax) 
        if cmax is None, ignore stopping conditions and proceed with ever higher c[-1]
    """
    n   = len(c)
    if 0 == n: raise StopIteration
    ic  = 0
    while (ic < n - 1) and (c[ic + 1] - c[ic] == 1):
        c[ic] = ic
        ic += 1
    if None is cmax or c[ic] < cmax - 1:
        c[ic] += 1
    else:
        raise StopIteration

def combination_rlex_iter(n, cmax):
    """ combinations iterator using combination_rlex_next """
    if cmax < n: 
        raise StopIteration
    c = range(n)
    yield c
    while True:
        combination_rlex_next(c, cmax)
        yield c
def combination_rlex_iter_2(n, cmax):
    """ more efficient implementation of combinations iterator 
        [Knuth TAOCP.4:algo"T"] 
    """
    if cmax < n:
        raise StopIteration
    # special case n == 0
    if 0 == n: 
        yield []
        raise StopIteration
    # special case n == 1
    if 1 == n:
        for c in range(cmax): yield [ c ] 
        raise StopIteration
    # general case
    c   = range(n)
    ic  = n - 1
    x   = None
    while ic < n:
        # return a copy since cannot make it read-only: 
        #   external function may change it break the flow
        yield list(c)   
        if 0 == ic:
            if c[0] + 1 < c[1]:
                c[0] += 1
                continue
            ic  = 1
            c[0]= 0
            x   = c[1] + 1
            while (ic < n - 1) and (x == c[ic + 1]):
                ic  += 1
                c[ic-1] = ic - 1
                x       = c[ic] + 1
        else: x = ic + 1
        if (ic == n - 1) and (cmax - 1 == c[-1]):
            raise StopIteration
        c[ic] = x
        ic -=1

def is_combination(c, cmax=None):
    """ check if ascending and below cmax """
    if 0 == len(c): return True
    return (all([ c[i] < c[1+i] for i in range(len(c) - 1) ]) 
            and (0 <= c[0]) and ((None is cmax) or c[-1] < cmax))

def is_rlex_combinations(c_list, cmax=None):
    """ check c_list is a list of combinations in lexicographic order """
    if 0 == len(c_list) : return True
    return ( all([ ( (len(c_list[i]) == len(c_list[1+i])) 
                     and (c_list[i][::-1] < c_list[1+i][::-1])
                     and is_combination(c_list[i], cmax) )  
                   for i in range(len(c_list) - 1) ]) 
             and is_combination(c_list[-1], cmax) )


def it_combinations_rlex(p, r):
    """ using it.combinations, iterate through rev.lexicographic sequence of p
    """
    n = len(p)
    rp = p[::-1]
    it_rlc = reversed(list(it.combinations(range(n), r)))
    for i in reversed(list(it.combinations(range(n), r))) :
        yield tuple([ rp[k] for k in i[::-1] ])
