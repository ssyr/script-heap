import numpy as np
import itertools as it

def map_piecelinear(xystop, a_lo, a_hi) :
    """ make piecewise-linear map passing through points xstop, ystop
        and for x < min(xstop), y = a_lo * (x - xmin) + y[xmin],
                x > max(xstop), y = a_hi * (x - xmax) + y[xmax]
    """
    xstop   = np.asarray([xy[0] for xy in xystop])
    ystop   = np.asarray([xy[1] for xy in xystop])
    n       = len(xstop)
    assert(len(ystop) == n)

    xsort_i = xstop.argsort()
    xstop   = xstop[xsort_i]
    ystop   = ystop[xsort_i]

    dxstop  = xstop[1:] - xstop[:-1]
    assert((0 < dxstop).all())
    dystop  = ystop[1:] - ystop[:-1]
    
    slope   = np.empty(n + 1, float)
    slope[0]    = a_lo
    slope[-1]   = a_hi
    slope[1:-1] = dystop / dxstop
    c_abs       = (slope[1:] - slope[:-1]) / 2. # coefficient of abs val
    c_sum       = c_abs.sum()

    y0      = 0.
    def func(x) :
        x   = np.asarray(x)
        y   = c_sum * x
        for xc, c in zip(xstop, c_abs) :
            y = y + c * np.abs(x - xc)
        return y + y0

    y0  = ystop[0] - func(xstop[0])
    return func
