import numpy as np
import math
is3 = 1./math.sqrt(3.)
gellmann_matr = np.array([
                [[  0,  1,  0], [  1,  0,  0], [  0,  0,  0]],      # lambda1
                [[  0,-1j,  0], [ 1j,  0,  0], [  0,  0,  0]],      # lambda2
                [[  1,  0,  0], [  0, -1,  0], [  0,  0,  0]],      # lambda3
                [[  0,  0,  1], [  0,  0,  0], [  1,  0,  0]],      # lambda4
                [[  0,  0,-1j], [  0,  0,  0], [ 1j,  0,  0]],      # lambda5
                [[  0,  0,  0], [  0,  0,  1], [  0,  1,  0]],      # lambda6
                [[  0,  0,  0], [  0,  0,-1j], [  0, 1j,  0]],      # lambda7
                [[is3,  0,  0], [  0,is3,  0], [  0,  0,-2*is3]]])  # lambda8
del is3
