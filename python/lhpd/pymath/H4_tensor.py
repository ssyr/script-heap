from __future__ import print_function
from future.utils import iteritems

import math
import numpy as np
#from lhpd.pymath.H4_tensor import *

""" functions to convert n-rank tensors into representations of H4 (SH4? parity?)
    general info
    representations: d<dimension>r<repr.id>; numbering follows Gockeler:1996mu
        d1r1, d1r2, d1r3, d1r4
        d2r1, d2r2
        d3r1, d3r2, d3r3, d3r4
        d4r1, d4r2, d4r3, d4r4
        d6r1, d6r2, d6r3, d6r4
        d8r1, d8r2
    return: array, shape[0] = repr.dim 
    all tensors are assumed to have parity P = rank (e.g. for a vector P=-1)
"""
def H4_T0_d1r1(x): 
    return np.array(x)[None]

H4_T0_repr_func = { 'H4_T0_d1r1'    : H4_T0_d1r1 }
H4_T0_repr_comp = { 'H4_T0_d1r1'    : ['a'] }

def H4_T1_d4r1(x):                      # eq.(4.1)
    assert (x.shape[0] == 4)
    return np.array(x)

H4_T1_repr_func = { 'H4_T1_d4r1'    : H4_T1_d4r1 }
H4_T1_repr_comp = { 'H4_T1_d4r1'    : ['x', 'y', 'z', 't'] }


def H4_T2_d1r1(x):
    assert (x.shape[0:2] == (4,4))      # eq.(4.2)
    return (x[0,0] + x[1,1] + x[2,2] + x[3,3]) *0.5
def H4_T2_d3r1(x):                      # eq.(4.3)
    assert (x.shape[0:2] == (4,4))
    sqrt1d2 = 0.70710678118654757
    return np.array([
        (x[0,0] + x[1,1] - x[2,2] - x[3,3]) *0.5,
        (x[2,2] - x[3,3]) *sqrt1d2,
        (x[0,0] - x[1,1]) *sqrt1d2 ])
def H4_T2_d6r3(x):                      # eq.(4.4)
    assert (x.shape[0:2] == (4,4))
    sqrt1d2 = 0.70710678118654757
    return np.array([
        (x[0,1] + x[1,0]) *sqrt1d2,
        (x[0,2] + x[2,0]) *sqrt1d2,
        (x[0,3] + x[3,0]) *sqrt1d2,
        (x[1,2] + x[2,1]) *sqrt1d2,
        (x[1,3] + x[3,1]) *sqrt1d2,
        (x[2,3] + x[3,2]) *sqrt1d2 ])
def H4_T2_d6r1(x):                      # eq.(4.5)
    assert (x.shape[0:2] == (4,4))
    sqrt1d2 = 0.70710678118654757
    return np.array([
        (x[0,1] - x[1,0]) *sqrt1d2,
        (x[0,2] - x[2,0]) *sqrt1d2,
        (x[0,3] - x[3,0]) *sqrt1d2,
        (x[1,2] - x[2,1]) *sqrt1d2,
        (x[1,3] - x[3,1]) *sqrt1d2,
        (x[2,3] - x[3,2]) *sqrt1d2 ])

H4_T2_repr_func= {  'H4_T2_d1r1'    : H4_T2_d1r1, 
                    'H4_T2_d3r1'    : H4_T2_d3r1, 
                    'H4_T2_d6r3'    : H4_T2_d6r3, 
                    'H4_T2_d6r1'    : H4_T2_d6r1 }
H4_T2_repr_comp= {  'H4_T2_d1r1'    : ['a'],
                    'H4_T2_d3r1'    : ['o1', 'o2', 'o3'], 
                    'H4_T2_d6r3'    : ['xy', 'xz', 'xt', 'yz', 'yt', 'zt'],
                    'H4_T2_d6r1'    : ['xy', 'xz', 'xt', 'yz', 'yt', 'zt'] }
#H4_T2_repr_name= sorted(H4_T2_repr_func.keys())
#H4_T2_repr_list= [ H4_T2_repr_func[k] for k in H4_T2_repr_name ]

def H4_T3_sym(x,i1,i2,i3):              # eq.(3.3)
    return (+x[i1,i2,i3] +x[i1,i3,i2] +x[i2,i1,i3] +x[i2,i3,i1]
            +x[i3,i1,i2] +x[i3,i2,i1]) / 6.
def H4_T3_asym(x,i1,i2,i3):              
    return (+x[i1,i2,i3] -x[i1,i3,i2] -x[i2,i1,i3] +x[i2,i3,i1]
            +x[i3,i1,i2] -x[i3,i2,i1]) / 6.
def H4_T3_b1(x,i1,i2,i3):               # eq.(4.6)
    return (+x[i1,i2,i3] -x[i1,i3,i2] -x[i3,i1,i2] +x[i3,i2,i1])
def H4_T3_b2(x,i1,i2,i3):               # eq.(4.7)
    return (+x[i1,i2,i3] -x[i1,i3,i2] +x[i3,i1,i2] -x[i3,i2,i1] 
            -2*x[i2,i3,i1] +2*x[i2,i1,i3])
def H4_T3_a1(x,i1,i2,i3):               # eq.(4.8)
    return (+x[i1,i2,i3] +x[i1,i3,i2] +x[i3,i1,i2] +x[i3,i2,i1] 
            -2*x[i2,i3,i1] -2*x[i2,i1,i3])
def H4_T3_a2(x,i1,i2,i3):               # eq.(4.9)
    return (+x[i1,i2,i3] +x[i1,i3,i2] -x[i3,i1,i2] -x[i3,i2,i1])

def H4_T3_d4r1_1(x):                    # eq.(4.10)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d2 = 0.70710678118654757
    return np.array([
    (+H4_T3_sym(x,0,0,0) +H4_T3_sym(x,0,1,1) +H4_T3_sym(x,0,2,2) +H4_T3_sym(x,0,3,3)) *sqrt1d2,
    (+H4_T3_sym(x,1,0,0) +H4_T3_sym(x,1,1,1) +H4_T3_sym(x,1,2,2) +H4_T3_sym(x,1,3,3)) *sqrt1d2,
    (+H4_T3_sym(x,2,0,0) +H4_T3_sym(x,2,1,1) +H4_T3_sym(x,2,2,2) +H4_T3_sym(x,2,3,3)) *sqrt1d2,
    (+H4_T3_sym(x,3,0,0) +H4_T3_sym(x,3,1,1) +H4_T3_sym(x,3,2,2) +H4_T3_sym(x,3,3,3)) *sqrt1d2 ])

def H4_T3_d4r1_2(x):                    # eq.(4.11)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d2 = 0.70710678118654757
    return np.array([
    (+H4_T3_sym(x,0,0,0) -H4_T3_sym(x,0,1,1) -H4_T3_sym(x,0,2,2) -H4_T3_sym(x,0,3,3)) *sqrt1d2,
    (-H4_T3_sym(x,1,0,0) +H4_T3_sym(x,1,1,1) -H4_T3_sym(x,1,2,2) -H4_T3_sym(x,1,3,3)) *sqrt1d2,
    (-H4_T3_sym(x,2,0,0) -H4_T3_sym(x,2,1,1) +H4_T3_sym(x,2,2,2) -H4_T3_sym(x,2,3,3)) *sqrt1d2,
    (-H4_T3_sym(x,3,0,0) -H4_T3_sym(x,3,1,1) -H4_T3_sym(x,3,2,2) +H4_T3_sym(x,3,3,3)) *sqrt1d2 ])
def H4_T3_d8r1_1(x):                    # eq.(4.12)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d2 = 0.70710678118654757
    sqrt3d2 = 1.2247448713915889
    return np.array([
        (+H4_T3_sym(x,0,1,1) -H4_T3_sym(x,0,2,2)) *sqrt3d2,
        (+H4_T3_sym(x,0,1,1) +H4_T3_sym(x,0,2,2) - 2*H4_T3_sym(x,0,3,3)) *sqrt1d2,
        (+H4_T3_sym(x,1,0,0) -H4_T3_sym(x,1,2,2)) *sqrt3d2,
        (+H4_T3_sym(x,1,0,0) +H4_T3_sym(x,1,2,2) - 2*H4_T3_sym(x,1,3,3)) *sqrt1d2,
        (+H4_T3_sym(x,2,0,0) -H4_T3_sym(x,2,1,1)) *sqrt3d2,
        (+H4_T3_sym(x,2,0,0) +H4_T3_sym(x,2,1,1) - 2*H4_T3_sym(x,2,3,3)) *sqrt1d2,
        (+H4_T3_sym(x,3,0,0) -H4_T3_sym(x,3,1,1)) *sqrt3d2,
        (+H4_T3_sym(x,3,0,0) +H4_T3_sym(x,3,1,1) - 2*H4_T3_sym(x,3,2,2)) *sqrt1d2 ])
def H4_T3_d4r2(x):                      # eq.(4.13)
    assert (x.shape[0:3] == (4,4,4))
    sqrt6   = 2.4494897427831779
    return np.array([ 
        H4_T3_sym(x,1,2,3) *sqrt6,
        H4_T3_sym(x,0,2,3) *sqrt6,
        H4_T3_sym(x,0,1,3) *sqrt6,
        H4_T3_sym(x,0,1,2) *sqrt6 ])
def H4_T3_d4r4(x):                      # eq.(4.14)
    assert (x.shape[0:3] == (4,4,4))
    sqrt6   = 2.4494897427831779
    return np.array([
        H4_T3_asym(x,1,2,3) *sqrt6,
        H4_T3_asym(x,0,2,3) *sqrt6,
        H4_T3_asym(x,0,1,3) *sqrt6,
        H4_T3_asym(x,0,1,2) *sqrt6 ])
def H4_T3_d8r2_1(x):                    # eq.(4.15)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d12= 0.28867513459481287
    return np.array([
        H4_T3_a2(x,1,2,3) *0.5,
        H4_T3_a1(x,1,2,3) *sqrt1d12,
        H4_T3_a2(x,0,2,3) *0.5,
        H4_T3_a1(x,0,2,3) *sqrt1d12,
        H4_T3_a2(x,0,1,3) *0.5,
        H4_T3_a1(x,0,1,3) *sqrt1d12,
        H4_T3_a2(x,0,1,2) *0.5,
        H4_T3_a1(x,0,1,2) *sqrt1d12 ])
def H4_T3_d8r2_2(x):                    # eq.(4.16)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d12= 0.28867513459481287
    return np.array([
        H4_T3_b2(x,1,2,3) *sqrt1d12,
        H4_T3_b1(x,1,2,3) *-0.5,
        H4_T3_b2(x,0,2,3) *sqrt1d12,
        H4_T3_b1(x,0,2,3) *-0.5,
        H4_T3_b2(x,0,1,3) *sqrt1d12,
        H4_T3_b1(x,0,1,3) *-0.5,
        H4_T3_b2(x,0,1,2) *sqrt1d12,
        H4_T3_b1(x,0,1,2) *-0.5 ])
def H4_T3_d4r1_3(x):                    # eq.(4.17)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d18= 0.23570226039551584
    return np.array([
        (+H4_T3_a2(x,0,1,1) +H4_T3_a2(x,0,2,2) +H4_T3_a2(x,0,3,3)) *sqrt1d18,
        (+H4_T3_a2(x,1,0,0) +H4_T3_a2(x,1,2,2) +H4_T3_a2(x,1,3,3)) *sqrt1d18,
        (+H4_T3_a2(x,2,0,0) +H4_T3_a2(x,2,1,1) +H4_T3_a2(x,2,3,3)) *sqrt1d18,
        (+H4_T3_a2(x,3,0,0) +H4_T3_a2(x,3,1,1) +H4_T3_a2(x,3,2,2)) *sqrt1d18 ])
def H4_T3_d4r1_4(x):                    # eq.(4.18)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d54= 0.13608276348795434
    return np.array([
        (+H4_T3_b2(x,0,1,1) +H4_T3_b2(x,0,2,2) +H4_T3_b2(x,0,3,3)) *sqrt1d54,
        (+H4_T3_b2(x,1,0,0) +H4_T3_b2(x,1,2,2) +H4_T3_b2(x,1,3,3)) *sqrt1d54,
        (+H4_T3_b2(x,2,0,0) +H4_T3_b2(x,2,1,1) +H4_T3_b2(x,2,3,3)) *sqrt1d54,
        (+H4_T3_b2(x,3,0,0) +H4_T3_b2(x,3,1,1) +H4_T3_b2(x,3,2,2)) *sqrt1d54 ])
def H4_T3_d8r1_2(x):                    # eq.(4.19)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d12= 0.28867513459481287
    return np.array([
        (+H4_T3_a2(x,0,1,1) -H4_T3_a2(x,0,2,2)) *sqrt1d12,
        (+H4_T3_a2(x,0,1,1) +H4_T3_a2(x,0,2,2) - 2*H4_T3_a2(x,0,3,3)) *(1./6),
        (+H4_T3_a2(x,1,0,0) -H4_T3_a2(x,1,2,2)) *sqrt1d12,
        (+H4_T3_a2(x,1,0,0) +H4_T3_a2(x,1,2,2) - 2*H4_T3_a2(x,1,3,3)) *(1./6),
        (+H4_T3_a2(x,2,0,0) -H4_T3_a2(x,2,1,1)) *sqrt1d12,
        (+H4_T3_a2(x,2,0,0) +H4_T3_a2(x,2,1,1) - 2*H4_T3_a2(x,2,3,3)) *(1./6),
        (+H4_T3_a2(x,3,0,0) -H4_T3_a2(x,3,1,1)) *sqrt1d12,
        (+H4_T3_a2(x,3,0,0) +H4_T3_a2(x,3,1,1) - 2*H4_T3_a2(x,3,2,2)) *(1./6) ])
def H4_T3_d8r1_3(x):                    # eq.(4.20)
    assert (x.shape[0:3] == (4,4,4))
    sqrt1d108   = 0.096225044864937631
    return np.array([
        (+H4_T3_b2(x,0,1,1) -H4_T3_b2(x,0,2,2)) *(1./6),
        (+H4_T3_b2(x,0,1,1) +H4_T3_b2(x,0,2,2) - 2*H4_T3_b2(x,0,3,3)) *sqrt1d108,
        (+H4_T3_b2(x,1,0,0) -H4_T3_b2(x,1,2,2)) *(1./6),
        (+H4_T3_b2(x,1,0,0) +H4_T3_b2(x,1,2,2) - 2*H4_T3_b2(x,1,3,3)) *sqrt1d108,
        (+H4_T3_b2(x,2,0,0) -H4_T3_b2(x,2,1,1)) *(1./6),
        (+H4_T3_b2(x,2,0,0) +H4_T3_b2(x,2,1,1) - 2*H4_T3_b2(x,2,3,3)) *sqrt1d108,
        (+H4_T3_b2(x,3,0,0) -H4_T3_b2(x,3,1,1)) *(1./6),
        (+H4_T3_b2(x,3,0,0) +H4_T3_b2(x,3,1,1) - 2*H4_T3_b2(x,3,2,2)) *sqrt1d108 ])

H4_T3_repr_func = { 'H4_T3_d4r1_1' : H4_T3_d4r1_1,
                    'H4_T3_d4r1_2' : H4_T3_d4r1_2,
                    'H4_T3_d4r1_3' : H4_T3_d4r1_3,
                    'H4_T3_d4r1_4' : H4_T3_d4r1_4,
                    'H4_T3_d4r2'   : H4_T3_d4r2, 
                    'H4_T3_d4r4'   : H4_T3_d4r4, 
                    'H4_T3_d8r1_1' : H4_T3_d8r1_1,
                    'H4_T3_d8r1_2' : H4_T3_d8r1_2,
                    'H4_T3_d8r1_3' : H4_T3_d8r1_3,
                    'H4_T3_d8r2_1' : H4_T3_d8r2_1,
                    'H4_T3_d8r2_2' : H4_T3_d8r2_2 }
H4_T3_repr_comp = { 'H4_T3_d4r1_1' : ['x', 'y', 'z', 't'],
                    'H4_T3_d4r1_2' : ['x', 'y', 'z', 't'],
                    'H4_T3_d4r1_3' : ['x', 'y', 'z', 't'],
                    'H4_T3_d4r1_4' : ['x', 'y', 'z', 't'],
                    'H4_T3_d4r2'   : ['yzt', 'xzt', 'xyt', 'xyz'],
                    'H4_T3_d4r4'   : ['yzt', 'xzt', 'xyt', 'xyz'],
                    'H4_T3_d8r1_1' : ['o1', 'o2', 'o3', 'o4', 'o5', 'o6', 'o7', 'o8'],
                    'H4_T3_d8r1_2' : ['o1', 'o2', 'o3', 'o4', 'o5', 'o6', 'o7', 'o8'],
                    'H4_T3_d8r1_3' : ['o1', 'o2', 'o3', 'o4', 'o5', 'o6', 'o7', 'o8'],
                    'H4_T3_d8r2_1' : ['o1', 'o2', 'o3', 'o4', 'o5', 'o6', 'o7', 'o8'],
                    'H4_T3_d8r2_2' : ['o1', 'o2', 'o3', 'o4', 'o5', 'o6', 'o7', 'o8']}
#H4_T3_repr_name = sorted(H4_T3_repr_func.keys())
#H4_T3_repr_list = [ H4_T3_repr_func[k] for k in H4_T3_repr_name ]


#  test
# 1) create basis x[i1,j1,k1,i2,j2,k2] = \delta({i1,j1,k1}, {i2,j2,k2}}
# 2) build representations
# 3) check mult. table
def build_T_basis(dim, rank, dtype=int):
    """ create 2*rank tensor [{ind1_rank}, {ind2_rank}] = \delta({ind1_rank}, {ind2_rank}}
    """
    res = np.zeros((dim,)* (2*rank), dtype=dtype)
    for ind in np.ndindex((dim,)* rank):
        res[ind + ind] = 1
    return res

def tensor_scal_prod(a,b):
    return (a.conj() * b).sum()

def test_repr_basis(dim, rank, repr_list, scal_prod=tensor_scal_prod):
    basis = build_T_basis(dim, rank)
    r_basis = [ r(basis) for r in repr_list ]
    n_r = len(repr_list)
    for i_r1 in range(n_r):
        r1 = r_basis[i_r1]
        for i_r2 in range(i_r1, n_r):
            r2 = r_basis[i_r2]
            r1_d_r2 = np.array([ [ scal_prod(r1a, r2a) for r2a in r2 ] for r1a in r1 ])
            p = (r1_d_r2.conj() * r1_d_r2 / len(r1)).sum()
            val_str = '--'
            if abs(p) < 1e-10: pass
            elif abs(p - 1) < 1e-10:
                if np.allclose(r1_d_r2, np.identity(len(r1))): val_str = '1'
                else: val_str = '??'
            else:
                val_str = '!!'
            print("%d\t%d\t%e\t%s" % (i_r1, i_r2, p, val_str))

H4_repr_func = {}
H4_repr_func.update(H4_T0_repr_func)
H4_repr_func.update(H4_T1_repr_func)
H4_repr_func.update(H4_T2_repr_func)
H4_repr_func.update(H4_T3_repr_func)

H4_repr_comp = {}
H4_repr_comp.update(H4_T0_repr_comp)
H4_repr_comp.update(H4_T1_repr_comp)
H4_repr_comp.update(H4_T2_repr_comp)
H4_repr_comp.update(H4_T3_repr_comp)

if False :
    H4_repr_dim     = { 
                    'H4_T0_d1r1'    : 1,
                    'H4_T1_d4r1'    : 4,
                    'H4_T2_d1r1'    : 1,
                    'H4_T2_d3r1'    : 3,
                    'H4_T2_d6r3'    : 6,
                    'H4_T2_d6r1'    : 6,
                    'H4_T3_d4r1_1'  : 4,
                    'H4_T3_d4r1_2'  : 4,
                    'H4_T3_d4r1_3'  : 4,
                    'H4_T3_d4r1_4'  : 4,
                    'H4_T3_d4r2'    : 4,
                    'H4_T3_d4r4'    : 4,
                    'H4_T3_d8r1_1'  : 8,
                    'H4_T3_d8r1_2'  : 8,
                    'H4_T3_d8r1_3'  : 8,
                    'H4_T3_d8r2_1'  : 8,
                    'H4_T3_d8r2_2'  : 8,
    }
else:
    H4_repr_dim = dict([ (k, len(v)) for k,v in iteritems(H4_repr_comp) ])


def H4_stdvec(vec) :
    """ make std vector (last index=Lorentz) """
    vec = np.abs(vec)
    vec.sort(-1)
    return vec
