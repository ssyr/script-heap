# implementation of symmetric & antisymmetric tensors
from __future__ import print_function
import math
import collections

import numpy as np
from ..misc import *
from .combi import *

def proj_vec(x, v) :
    """ decompose x into (a*v + b), return (a, b) 
        v       1-dim vector, len(v) == x.shape[-1]
    """
    v   = np.asarray(v)
    assert (1 == len(v.shape))
    v2  = ((v.conj() * v).real.sum())

    x = np.asarray(x)
    if v2 <= 0 :
        x_v = np.zeros(x.shape[:-1], x.dtype)
    else :
        x_v = np.einsum('...i,...i', x, v) / v2

    x_pv = x - x_v[..., None] * v 
    
    return x_v, x_pv
    
def proj_vec_comp(x,v): 
    x_v, x_pv = proj_vec(x, v)
    return x_v, np.sqrt((x_pv.conj() * x_pv).real.sum(-1))


def tensor_matrix_dot(t, m, axis_t):
    """ right-multiply tensor . matrix, using 'axis_t'th index of the tensor 
        preserving the meaning of all the other tensor indices
    """
    assert 2 == len(m.shape)
    return np.rollaxis(np.tensordot(t, m, axes=(axis_t, 0)), -1, axis_t)
# FIXME is it equivalent to 'tensor_matrix_dot(t, m.T, axis_t)' ?
def matrix_tensor_dot(m, t, axis_t):
    """ left-multiply tensor . matrix, using 'axis_t'th index of the tensor 
        preserving the meaning of all the other tensor indices
    """
    assert 2 == len(m.shape)
    return np.rollaxis(np.tensordot(t, m, axes=(axis_t, 1)), -1, axis_t)

def tensor_outer(*a_list) :
    """ full outer product of tensors """
    if len(a_list) <= 0 : return None
    a_list      = [ np.asarray(a) for a in a_list ]
    res_n       = sum([ len(a.shape) for a in a_list ])
    a_sl_list   = [ [None] * res_n for a in a_list ]
    cnt = 0
    for i, a in enumerate(a_list) :
        l = len(a.shape)
        a_sl_list[i][cnt : cnt + l] = [slice(None)] * l
        cnt += l
    
    out = (a_list[0][tuple(a_sl_list[0])]).copy()
    for i in range(1, len(a_list)) :
        out = out * a_list[i][tuple(a_sl_list[i])]

    return out

def tensor_swap_axes(a, ij_list) :
    """ version of transposition : pairs of axes to switch 
        should work if axes encounter >1 time in ij_list, but 
        it's up to you to figure out what the result will mean
    """
    tr = range(len(a.shape))
    for i, j in ij_list:
        tr[i], tr[j] = tr[j], tr[i]
    #print(a.shape, tr)
    return a.transpose(tr)

def tensor_place_axes(a, ij_list) :
    """ move axes (transpose) into specified locations, 
        without changing the relative order of other axes
        ij_list = [(out_axis, in_axis), ...]
            out_axis, in_axis may be negative,
            may not refer to the same position twice
    """
    n   = len(a.shape)
    return a.transpose(list_place(range(len(a.shape)), ij_list))

def tensor_s_(shape_len, ind, axes=None) :
    """ take subtensor at `ind' along `axes' 
        `axes' = list of int ; `ind' = list of int or slice
        useful to make l-value slices
    """
    sl = [slice(None)] * shape_len
    if isinstance(ind, list) or isinstance(ind, tuple) :
        n_ind   = len(ind)
        if None is axes : 
            axes = range(n_ind)
        assert(len(axes) == n_ind)
        for i, ax in enumerate(axes) :
            sl[ax] = ind[i]
    else : 
        if None is axes : axes = 0
        sl[axes] = ind
    return tuple(sl)

def tensor_take(a, ind, axes) :
    sl = [slice(None)] * len(a.shape)
    for i, ax in enumerate(axes) :
        sl[ax] = ind[i]
    return a[tensor_s_(len(a.shape), ind, axes)]

def tensor_take_slice(a, ind, axes) :
    """ similar to tensor_take, but keep the axes
        `axes' = list of int ; `ind' = list of int
    """
    return tensor_take(a, [ np.s_[i:i+1] for i in ind ], axes)

def tensor_sum(a, axes) :
    """ sum tensor along specified directions
    """
    sh = list(a.shape)
    res_tr = tuple(list_del(range(len(a.shape)), axes))
    res_sh = tuple([ a.shape[t] for t in res_tr ])
    return a.transpose(res_tr + axes).reshape(res_sh + (-1,)).sum(-1)

def tensor_sum_slice(a, axes) :
    """ sum tensor along specified directions and return 
        a form broadcastable to the original shape
    """
    res_sh  = list(a.shape)
    for ax in axes : 
        res_sh[ax] = 1
    return tensor_sum(a, axes).reshape(res_sh)

def UTEST_tensor_aux(l, pos_l) :
    res = True
    a=np.random.rand(2,3,4,5)
    res = res and np.allclose(tensor_sum(a, (1,3)), a.sum(3).sum(1))


# description: 
# * support for reduced tensors having certain index symmetry(symmetric, 
#   antisymmetric, general YT)
# * reduced tensors must, upon contraction, produce the same result without
#   any additional factors ; therefore include additional sqrt(n!) factor
#   TODO may want to rethink that because dtype=int arrays are impossible
# * functions to build up symmetric tensors through direct product (done)
# * TODO integrate with einsum?
# XXX mapping between a group of antisymmetric indices and reduced index is 
#   _reverse_lexicographic_ : counting according to sort where the last element 
#   is the most significant in comparison; 
#   e.g. for n=2 : 0:(0,1), 1:(0,2), 2:(1,2), ...
# * performance is not great as of now ; need to speed up the main iterator 
#   `tensor_asym_flat_index_iter' ; perhaps, Cython'ized version will be better?
#   currently, appropriate only for large additional dimensions


# TODO perhaps 
# SYMMETRIC TENSORS 


# ANTISYMMETRIC TENSORS 
            

# TODO for the next group of functions to be extended to symmetric (or arbitrary 
# symmetry) tensors, one has to take multiplicity into account
# ??? combine with sign?

def tensor_asym_index_iter(ind, k) :
    """ iterate over tensor antisymmetric indices: partition `ind' into k[i] groups
        n = len(ind) ; len(k) = m ; sum(k) = n;
        yield each partiiton of `ind' once for each multinomial combination;
        within each partition element, the order is the same as in ind
        
        RETURN: sign, [ ( i_{C(1)}, ..., i_{C(k_1)} ), ( i_{C(k_1+1)}, ...), ... ]
    """
    n   = len(ind)
    m   = len(k)
    s   = []
    for i, k_i in enumerate(k) : s.extend([i] * k_i)
    assert(len(s) == n)
    for p in permut_lex_iter(s) :
        p   = np.asarray(p)
        #print(p, p==0, p==1)
        ix  = [ tuple(it.compress(ind, p==i)) for i in range(len(k)) ]
        sign= 1 - ((permut_count(p)) % 2) * 2
        yield sign, ix


def tensor_asym_flat_index_iter(rank, i_flat, k) :
    """ essentially a wrapper for `tensor_asym_index_iter' : 
        take flat index, return sign and a list of flat indices
    """
    ind = combination_from_rlex_rank(rank, i_flat)  # sorted
    for s, ix_list in tensor_asym_index_iter(ind, k) :
        yield s, [ combination_to_rlex_rank(ix) for ix in ix_list ]
            

def tensor_asym_flat_dual(a, dim, a_rank, a_axis=-1, 
            out=None, out_axis=-1) :
    """ compute a dual tensor 
            aDual[i1...iK] = eps{dim=N}[i1...iN] a[i{K+1}...iN], K=a_rank, N=dim
        fast, as no computation is necessary : only change order and signs
        NOTE    other dual definition, 
                aDual[i{N-K+1}...iN] = eps{dim=N}[i1...iN] a[i1...iK]
                is related to the one above by an overall sign (-1)^{K*(N-K)}
        a           ndarray
        dim         dimension of asym-flat
        a_rank      rank of asym-flat index
        a_axis      asym-flat index axis
        out         output ndarray (optional)
        out_axis    asym-flat axis for output array
    
        RETURN transformed array or `out' if specified
    """
    if a_rank <= 0 or dim < a_rank :
        raise ValueError("bad rank specification")
    a_len       = binom_coeff(dim, a_rank)
    assert(a.shape[a_axis] == a_len)

    out_rank    = dim - a_rank
    out_len     = binom_coeff(dim, out_rank) 
    # fill with zeros : some index combinations will not be visited
    if None is out : 
        out_sh  = tuple(list_insert( list_del(a.shape, (a_axis,)), 
                                     ((out_axis, out_len),) ))
        out     = np.zeros(out_sh, a.dtype)
    else: 
        assert (out.shape[out_axis] == out_len)
        out.fill(0)
    
    for out_sign, (iflat_a, iflat_out) in tensor_asym_flat_index_iter(
                                        dim, 0, [a_rank, out_rank]) :
        a_sl = tensor_s_(len(a.shape), iflat_a, a_axis)
        out_sl = tensor_s_(len(out.shape), iflat_out, out_axis)

        if 0 < out_sign : 
            out[out_sl] =  a[a_sl]
        else : 
            out[out_sl] = -a[a_sl]

    return out


def tensor_asym_flat_calc_inplace(a, dim, a_ranks, a_axes=None) :
    """ antisymmetrize a tensor over asym-flat axes
        the shape is not changed, calculation is done in-place
        a           ndarray
        dim         dimension of axes
        a_ranks     ranks of asym-flat indices
        a_axes      axes of asym-flat indices
        
        RETURN None
    """
    a_ranks     = list(a_ranks)
    m           = len(a_ranks)
    a_rank_full = sum(a_ranks)
    if a_rank_full <= 0 or dim < a_rank_full :
        raise ValueError("bad rank specification")
    a_len_full  = binom_coeff(dim, a_rank_full)

    if None is a_axes : 
        a_axes  = range(-m, 0)
    else : 
        a_axes  = list(a_axes)
    a_lens      = [ binom_coeff(dim, r) for r in a_ranks ]
    assert ([ a.shape[ax] for ax in a_axes ] == a_lens )
    
    # keep track of zero indices
    zero_ind  = set(np.ndindex(*a_lens))
    
    combi_factor = 1. / multinom_coeff(a_ranks)
    for iflat_a_full in range(a_len_full) :
        # sum over index combis
        a_sl_0, a_sign_0 = None, None
        for a_sign, iflat_a_list in tensor_asym_flat_index_iter(
                                            a_rank_full, iflat_a_full, a_ranks) :
            a_sl = tensor_s_(len(a.shape), iflat_a_list, a_axes)
            if None is a_sl_0 :
                a_sl_0, a_sign_0 = a_sl, a_sign
                continue
            if 0 < (a_sign * a_sign_0)  : 
                a[a_sl_0] += a[a_sl]
            else : 
                a[a_sl_0] -= a[a_sl]

        a[a_sl_0] *= combi_factor

        # assign to all index combis
        a_sl_0, a_sign_0 = None, None
        for a_sign, iflat_a_list in tensor_asym_flat_index_iter(
                                            a_rank_full, iflat_a_full, a_ranks) :
            zero_ind.remove(tuple(iflat_a_list))
            a_sl = tensor_s_(len(a.shape), iflat_a_list, a_axes)
            if None is a_sl_0 :
                a_sl_0, a_sign_0 = a_sl, a_sign
                continue
            if 0 < (a_sign * a_sign_0)  : 
                a[a_sl]     =  a[a_sl_0]
            else : 
                a[a_sl]     = -a[a_sl_0]

    for iflat_a_list in zero_ind :
        a[tensor_s_(len(a.shape), iflat_a_list, a_axes)] = 0


def tensor_asym_flat_group(
                a, dim, a_ranks, 
                a_axes=None, out=None, out_axis=-1, skip_calc=False) :
    # TODO implement out_axis, skip_calc
    """ flatten a group of antisymmetric flat indices (antisymmetrize by default)
        a           ndarray-like
        dim         dimension of antisymmetric indices
        a_ranks     list of ranks of axes of `a' to apply the transformation
        a_axes      list of axes; default = last `len(a_ranks)' last axes of `a'
        out         output array, if necessary
        out_axis    axis position for the new flat index in the result
        skip_calc   skip calculation if the tensor in `a'is already antisymmetric

        the rest of the shape of `a' is transported into the result,
        maintaining the relative order of the axes

        RETURN transformed array or `out' if specified
    """
    a           = np.asarray(a)
    a_ranks     = list(a_ranks)     # may be supplied as an iterator
    m           = len(a_ranks)
    assert 0 < m
    
    out_rank    = sum(a_ranks)
    if out_rank <= 0 or dim < out_rank : 
        raise ValueError("bad rank specification")
    out_len = binom_coeff(dim, out_rank)

    if None is a_axes : 
        a_axes  = range(-m, 0)
    else : 
        a_axes  = list(a_axes)
    a_lens      = [ binom_coeff(dim, r) for r in a_ranks ]
    assert ([ a.shape[ax] for ax in a_axes ] == a_lens )

    if None is out : 
        out_sh  = tuple(list_insert( list_del(a.shape, a_axes), 
                                    ((out_axis, out_len),) ))
        out     = np.zeros(out_sh, np.find_common_type([], [a.dtype, float]))
    else :
        assert(out.shape[out_axis] == out_len)
        out.fill(0)

    combi_factor = 1. / math.sqrt(multinom_coeff(a_ranks))
    for iflat_out in range(out_len) : 
        out_sl = tensor_s_(len(out.shape), iflat_out, out_axis)
        for a_sign, iflat_a_list in tensor_asym_flat_index_iter(
                                            out_rank, iflat_out, a_ranks) :
            a_sl = tensor_s_(len(a.shape), iflat_a_list, a_axes)
            if 0 < a_sign  : 
                out[out_sl] += a[a_sl]
            else : 
                out[out_sl] -= a[a_sl]
    
    out *= combi_factor
    return out


def tensor_asym_flat_ungroup(
                a, dim, out_ranks, 
                a_axis=-1, out=None, out_axes=None, DEBUG=False) :
    """ split an asym-flat index into a number of asym-flat indices
        a           ndarray-like
        dim         dimension of antisymmetric indices
        out_ranks   ranks of resulting asym-flat indices
        a_axis      position of the asym-flat index in `a', defaults to (-1)
        out         output ndarray, if specified
        out_axes    positions of new asym-flat indices in the result,
                    defaults to the last `len(out_ranks)' axes

        the rest of the shape of `a' is transported into the result,
        maintaining the relative order of the axes

        RETURN transformed array or `out', if specified
    """
    out_ranks   = list(out_ranks)
    a_rank      = sum(out_ranks)
    m           = len(out_ranks)
    assert 0 < m

    if None is out_axes :
        out_axes = range(-m, 0)
    else :
        out_axes = list(out_axes)
    
    a_len       = binom_coeff(dim, a_rank)
    assert(a.shape[a_axis] == a_len)

    out_lens    = [ binom_coeff(dim, r) for r in out_ranks ]
    # fill with zeros : some index combinations will not be visited
    if None is out : 
        out_sh  = tuple(list_insert( list_del(a.shape, (a_axis,)), 
                                     zip(out_axes, out_lens) ))
        out     = np.zeros(out_sh, np.find_common_type([], [a.dtype, float]))
    else: 
        assert( [ out.shape[ax] for ax in out_axes ] == out_lens)
        out.fill(0)
    
    combi_factor = 1. / math.sqrt(multinom_coeff(out_ranks))  # sic! no summation as in `tensor_asym_flat_group'
    for iflat_a in range(a_len) :
        a_sl = tensor_s_(len(a.shape), iflat_a, a_axis)
        for out_sign, iflat_out_list in tensor_asym_flat_index_iter(
                                            a_rank, iflat_a, out_ranks) :
            out_sl = tensor_s_(len(out.shape), iflat_out_list, out_axes)
            if 0 < out_sign : 
                out[out_sl] = combi_factor * a[a_sl]
            else : 
                out[out_sl] = -combi_factor * a[a_sl]
            if DEBUG:
                print('%+2d' % out_sign, iflat_a, iflat_out_list, " # ", out_sl, a_sl)

    return out


def tensor_asym_flat_einsum(
            einstr, a_s, dim, a_ranks, 
            a_axes=None, out=None, out_axis=-1) :
    """ compute einsum-style contraction/summation, with direct product 
        and antisymmetrization over asym-flat indices, which are flattened into 
        a single asym-flat index in the result
        einstr      einsum specification string; asym-flat indices must be omitted
        a_s         list of ndarray-like's; each must have an asym-flat index (TODO relax this?)
        dim         dimension for asym-flat indices
        a_ranks     ranks of respective asym-flat indices
        a_axes      axis positions of respective asym-flat indices

        the rest of the shape of the einsum result is kept in the resulting array
        maintaining the relative order of the axes

        RESULT einsum with additional single asym-flat index
    """

    n           = len(a_s)
    a_ranks     = list(a_ranks)
    assert(len(a_ranks) == n)
    a_lens      = [ binom_coeff(dim, r) for r in a_ranks ]

    if None is a_axes :
        a_axes  = [-1] * n
    assert(len(a_axes) == n)
    assert ([ a.shape[ax] for a, ax in zip(a_s, a_axes) ] == a_lens)
    
    out_rank    = sum(a_ranks)
    if out_rank <= 0 or dim < out_rank :
        raise ValueError("bad rank specification")
    out_len     = binom_coeff(dim, out_rank)

    if not None is  out :
        assert out.shape[out_axis] == out_len
        out.fill(0)
    else :
        out_sh, out_sl = None, None
        out_dtype   = np.find_common_type([], [float] + [a.dtype for a in a_s])

    combi_factor = 1. / math.sqrt(multinom_coeff(a_ranks))
    for iflat_out in range(out_len) : 
        if not None is  out :
            out_sl = tensor_s_(len(out.shape), iflat_out, out_axis)
        for a_sign, iflat_a_list in tensor_asym_flat_index_iter(
                                            out_rank, iflat_out, a_ranks) :
            einsum_ops  = [ a[tensor_s_(len(a.shape), iflat_a, ax)]
                            for a, iflat_a, ax in zip(a_s, iflat_a_list, a_axes) ]
            x           = np.einsum(einstr, *einsum_ops)

            # `out' not specified ; generate out, out_sl in the first iteration
            if None is out: #
                out_sh  = tuple(list_insert( x.shape, ((out_axis, out_len),) ))
                out     = np.zeros(out_sh, out_dtype)
                out_sl  = tensor_s_(len(out.shape), iflat_out, out_axis)

            if 0 < a_sign  : 
                out[out_sl] += x
            else : 
                out[out_sl] -= x

    out *= combi_factor
    return out

def tensor_asym_flat_mult(
            a_s, dim, a_ranks, a_axes=None, 
            out=None, out_axis=-1) :
    """ asym-flat version of traditional NumPy broadcast multiplication
    """
    n           = len(a_s)
    a_ranks     = list(a_ranks)
    assert(len(a_ranks) == n)
    a_lens      = [ binom_coeff(dim, r) for r in a_ranks ]

    if None is a_axes :
        a_axes  = [-1] * n
    assert(len(a_axes) == n)
    assert ([ a.shape[ax] for a, ax in zip(a_s, a_axes) ] == a_lens)
    
    out_rank    = sum(a_ranks)
    if out_rank <= 0 or dim < out_rank :
        raise ValueError("bad rank specification")
    out_len     = binom_coeff(dim, out_rank)

    if not None is  out :
        assert out.shape[out_axis] == out_len
        out.fill(0)
    else :
        out_sh, out_sl = None, None
        out_dtype   = np.find_common_type([], [float] + [a.dtype for a in a_s])
    
    combi_factor = 1. / math.sqrt(multinom_coeff(a_ranks))
    for iflat_out in range(out_len) : 
        if not None is  out :
            out_sl = tensor_s_(len(out.shape), iflat_out, out_axis)
        for a_sign, iflat_a_list in tensor_asym_flat_index_iter(
                                            out_rank, iflat_out, a_ranks) :

            x = 1   # computing separately and placing into `out' requires <= multiplications
            for a, iflat, ax in zip(a_s, iflat_a_list, a_axes) :
                a_sl = tensor_s_(len(a.shape), iflat, ax)
                x = x * a[a_sl]

            if None is out :
                out_sh = tuple(list_insert( x.shape, ((out_axis, out_len),) ))
                out     = np.zeros(out_sh, out_dtype)
                out_sl = tensor_s_(len(out.shape), iflat_out, out_axis)

            if 0 < a_sign :
                out[out_sl] += x
            else :
                out[out_sl] -= x

    out *= combi_factor
    return out


def tensor_asym_flat_outer(
            a_s, dim, a_ranks, a_axes=None,
            out=None, out_axis=-1) :
    """
    """
    n           = len(a_s)
    a_ranks     = list(a_ranks)
    assert(len(a_ranks) == n)
    a_lens      = [ binom_coeff(dim, r) for r in a_ranks ]

    if None is a_axes :
        a_axes  = [-1] * n
    assert (len(a_axes) == n)
    assert ([ a.shape[ax] for a, ax in zip(a_s, a_axes) ] == a_lens)

    out_rank    = sum(a_ranks)
    if out_rank <= 0 or dim < out_rank :
        raise ValueError("bad rank specification")
    out_len     = binom_coeff(dim, out_rank)
    
    a_sh_reg    = [ tuple(list_del( a.shape, (ax,) ))   # remove asym flat
                    for a, ax in zip(a_s, a_axes) ]
    out_sh_reg  = sum(a_sh_reg, ())                     # combine
    a_sh_outer  = [ [1] * len(out_sh_reg) for i in range(n) ]
    cnt         = 0
    for i_a, a_sh in enumerate(a_sh_reg) : 
        l = len(a_sh_reg[i_a])
        a_sh_outer[i_a][cnt : cnt + l] = a_sh_reg[i_a]
        cnt += l

    if not None is  out :
        assert out.shape[out_axis] == out_len
        out.fill(0)
    else :
        out_sh      = tuple(list_insert( out_sh_reg, ((out_axis, out_len),) ))
        out_dtype   = np.find_common_type([], [float] + [a.dtype for a in a_s])
        out         = np.zeros(out_sh, out_dtype)
    
    combi_factor = 1. / math.sqrt(multinom_coeff(a_ranks))
    for iflat_out in range(out_len) : 
        out_sl = tensor_s_(len(out.shape), iflat_out, out_axis)
        for a_sign, iflat_a_list in tensor_asym_flat_index_iter(
                                            out_rank, iflat_out, a_ranks) :
            x = 1
            for a, iflat, ax, a_sh in zip(a_s, iflat_a_list, a_axes, a_sh_outer) :
                a_sl    = tensor_s_(len(a.shape), iflat, ax)
                x = x * a[a_sl].reshape(a_sh)
            if 0 < a_sign : 
                out[out_sl] += x
            else : 
                out[out_sl] -= x

    out *= combi_factor
    return out


# function for tests
# TODO make in-place version?
def UTEST_tensor_asym_gold(a, axes=(-2,-1), out=None) :
    n = len(axes)
    if n <= 1: return a
    dim = a.shape[axes[0]]
    if not all(a.shape[ax] == dim for ax in axes[1:]) :
        raise IndexError("must have equal len along %s axes" % str(axes))
    
    res_dtype = np.find_common_type([], [a.dtype, float])
    if None is out : 
        out = np.zeros(a.shape, res_dtype)
    else : 
        out.fill(0)
    transp = range(len(a.shape))
    for k in it.permutations(range(n)) :
        sign_a = 1 - 2 * (permut_count(k) % 2)
        for i, ax in enumerate(axes) :
            transp[ax] = axes[k[i]]
        out += sign_a * a.transpose(*transp)
    return out / math.factorial(n)
def UTEST_mk_test_tensor(sh_base, dim, rank, axes=None, use_values=None) :
    """ create test tensor with `axes' axes being [dim], 
        other axes being `sh_base'
    """
    a_orig_sh = list_insert(sh_base, zip(axes, (dim,) * rank))
    if None is use_values :
        return np.random.rand(*a_orig_sh)
    else :
        cnt = np.prod(a_orig_sh)
        return np.fromiter(it.cycle(use_values), float, count=cnt).reshape(a_orig_sh)

def UTEST_mk_test_tensor_asym_flat(sh_base, dim, ranks, axes=None, use_values=None) :
    """ create test tensor with `axes' axes being asym-flat with 
        `ranks' and 'dim' ranks, respectively, other axes being `sh_base'
        NOTE : there is no antisymmetrization with respect to all indices
        encompassed in `ranks'
    """
    test_rank_all = sum(ranks)
    # tensor with first `test_rank_all' axes = [dim]
    a = UTEST_mk_test_tensor(sh_base, dim, test_rank_all, 
                axes=range(test_rank_all), use_values=use_values)
    for r in ranks :
        # new asym-flat axis will be put to the end of a.shape
        a = tensor_asym_flat_group(a, dim, [1]*r, a_axes=range(r))
    # a = tensor with tailing asym-flat indices of rank `ranks'
    if not None is  axes : # with default axes, no change
        a_tr_pairs = zip(axes, range(-len(ranks), 0))
        print(a_tr_pairs)
        a   = tensor_place_axes(a, a_tr_pairs)
    return a

def UTEST_tensor_asym_flat_group_ungroup(
        sh_base=(4,5), dim=8, asym_ranks=(2,3),
        axes_full=None, axes_group=None, axis_flat=-1, use_values=None):
    """ test functions : group, ungroup, calc_inplace
        functionality : out, out_axes, a_axes
    """

    print(sh_base, dim, asym_ranks)

    
    asym_rank_full  = sum(asym_ranks)
    asym_rank_cnt   = len(asym_ranks)

    if None is axes_full :
        axes_full = range(-asym_rank_full, 0)
    if None is axes_group :
        axes_group = range(-asym_rank_cnt, 0)
    
    # random/repeated tensor
    a           = UTEST_mk_test_tensor(sh_base, dim, asym_rank_full, 
                        axes=axes_full, use_values=use_values)
    # "golden" full antisymmetric tensor
    a_s_gold    = UTEST_tensor_asym_gold(a, axes=axes_full)   # antisym in the last `asym_rank_full' axes
    # build asym tensors in two ways (A,B) :
    # A : (i1...iN) -> (iflatAll) -> (iflat1 ...iflatN)
    aA_s_s      = tensor_asym_flat_group(a, dim, [1] * asym_rank_full, 
                        a_axes=axes_full, out_axis=axis_flat)
    # make full antisym tensor
    # FIXME compute aA_full from aA_s by ungrouping individual groups of indices
    aA_full     = np.empty_like(a_s_gold)       # test `out' in ungroup
    tensor_asym_flat_ungroup(aA_s_s, dim, [1] * asym_rank_full,
                        a_axis=axis_flat, out_axes=axes_full,
                        out=aA_full)
    # split axes into asym groups
    aA_s        = tensor_asym_flat_ungroup(aA_s_s, dim, asym_ranks, 
                        a_axis=axis_flat, out_axes=axes_group) 


    # B : (i1...iN) -> (iflat1...iflatN) -> (iflatAll)
    # start with either with the original or fully symmetric tensor
    # note that aA_s is fully symmetric; aB_s will be symmetrized by the 'inplace' function below
    if True :
        aB_s        = a
    else :
        aB_s        = a_s_gold 
    # transpose to 'canonical', trailing asym-flat axes for simplicity, otherwise 
    # multi-index transform will be too cumbersome; arbitrary axes functionality
    # may need separate testing
    aB_s        = tensor_place_axes(aB_s, zip(range(-asym_rank_full, 0), axes_full))
    ax_asym_start = -asym_rank_full   # first axis from group, negative indexing
    for i, r in enumerate(asym_ranks) :
        # new "group" axes are put to the end of a_s.shape
        asym_ranks_axes = list(zip([1] * r, range(ax_asym_start, ax_asym_start + r)))
        aB_s    = tensor_asym_flat_group(aB_s, dim, [1]*r, 
                        a_axes=range(ax_asym_start, ax_asym_start + r))
        ax_asym_start += r - 1  # negative indexing: remove r axes, add 1 from end of aB_s.shape
    assert(-asym_rank_cnt == ax_asym_start)
    # transpose back
    aB_s        = tensor_place_axes(aB_s, zip(axes_group, range(-asym_rank_cnt, 0)))
    # symmetrize wrt remaining index permutations
    aB_s1_copy  = aB_s.copy()   # no antisymmetrization, to check contraction with aA_s
    tensor_asym_flat_calc_inplace(aB_s, dim, asym_ranks, a_axes=axes_group)
    aB_s_s      = np.empty_like(aA_s_s) # test `out' param in group
    tensor_asym_flat_group(aB_s, dim, asym_ranks, 
                        a_axes=axes_group, out_axis=axis_flat,
                        out=aB_s_s)
    aB_full     = tensor_asym_flat_ungroup(aB_s_s, dim, [1] * asym_rank_full,
                        a_axis=axis_flat, out_axes=axes_full)

    # verbose print
    print_list = [
            'a.shape, a_s_gold.shape',
            'aA_s.shape, aA_s_s.shape, aA_full.shape',
            'aB_s.shape, aB_s_s.shape, aB_full.shape', 
            '(a_s_gold**2).sum(), (aA_full**2).sum(), (aB_full**2).sum()',
            '(aA_s**2).sum(), (aA_s_s**2).sum()',
            '(aB_s**2).sum(), (aB_s_s**2).sum()',
            '(aA_s*aB_s).sum(), (aA_s * aB_s1_copy).sum(), (aA_s_s*aB_s_s).sum()' 
            ]
    for p in print_list : 
        print(eval(p), " # ", p)

    # checks
    test_list = [   
            'np.allclose(a_s_gold, aA_full)',
            'np.allclose(a_s_gold, aB_full)',
            'np.allclose(aA_s, aB_s)',
            'np.allclose(aA_s_s, aB_s_s)', 
            ]
    res = True
    for t in test_list :
        is_t = eval(t)
        res = res and is_t
        print(is_t, " # ", t)
    return res


def UTEST_tensor_asym_flat_products() :
    """ test einsum, mult, outer """
    dim = 6
    r_a, r_b, r_c = 1,2,1
    a   = UTEST_mk_test_tensor_asym_flat((2,3), dim, [r_a])
    b   = UTEST_mk_test_tensor_asym_flat((4,3), dim, [r_b])
    c   = UTEST_mk_test_tensor_asym_flat((4,),  dim, [r_c])
    
    ab_o= tensor_asym_flat_outer([a,b], dim, [r_a, r_b])
    ab_e=tensor_asym_flat_einsum("ij,kl->ijkl", [a,b], dim, [r_a, r_b])
    print(np.allclose(ab_o, ab_e))

    ab_m= tensor_asym_flat_mult([a.reshape(2,3,1,1, -1), b.reshape(1,1,4,3, -1)], dim, [r_a, r_b])
    print(np.allclose(ab_m, ab_e))

    ab_sum= np.einsum('ikjk...->ij...', ab_o)
    print(np.allclose(ab_sum, 
                      tensor_asym_flat_einsum('ik...,jk...->ij...', 
                                              [a,b], dim, [r_a, r_b])))

    abc_sum = 0
    for k in range(len(c)) :
        abc_sum = abc_sum + tensor_asym_flat_mult(
                                [ab_sum[:,k], c[k]], dim, [r_a+r_b, r_c])
    print(np.allclose(abc_sum, 
                      tensor_asym_flat_einsum('ik...,jk...,j...->i...', 
                                              [a,b,c], dim, [r_a, r_b, r_c])))




######################## OLD TESTS

# TODO standardise language to be applicable to any symmetry type
# 'nd' : multidim tensor with some symmetry
# 'rd' : tensor with symmetric indices reduced into 1 index ('reduced-dim')


def UTEST_antisymm() :
    def cmp_arr_asymindex(n, dim, a_nd, nd_axes, a_rd, rd_axis) :
        """ compare asym multidim array with asym-indexed array 
            n       # of asym axes
            dim     dimension of asym axes:
            a_nd, nd_axes     multidim array, asymm along nd_asymaxes
            a_rd, rd_axis     asym reduced array; asymnd_axis - combined asym axis
            the following holds:
                a_nd.shape[i]==dim for i in nd_axes
                a_rd.shape[rd_axis] = binom_coeff(dim, n)
                a_nd, a_rd have equal shape after asym axes are deleted from both
        """
        return all([ np.allclose((tensor_take(a_rd, [i_a], [rd_axis]) 
                                    / math.sqrt(math.factorial(n))), 
                                 tensor_take(a_nd, ind_a, nd_axes)) 
                     for i_a, ind_a in enumerate(it_combinations_rlex(range(dim), n)) ])
    res = True

    dim = 4
    n   = 2
    sh_base = (2, 3, 5)     # other axes
    sym_axes= (2, -1)       # axes where symmetrization must be done

    a_orig_sh = (dim,) * n + sh_base
    a_tr_pairs = list(enumerate(sym_axes))
    print(a_orig_sh, a_tr_pairs)
    a   = tensor_transpose(np.random.rand(*a_orig_sh), a_tr_pairs)
    print(a.shape)

    a_as_gold   = UTEST_tensor_asym_gold(a, axes=sym_axes)
    a_as        = tensor_asym(a, axes=sym_axes)

    res = res and cmp_arr_asymindex(n, dim, a_as_gold, sym_axes, a_as, -1)



    a_as_cnct = (a_as * a_as).sum(-1)
    a_as_gold_cnct = tensor_sum(a_as_gold * a_as_gold, sym_axes)
    res = res and np.allclose(a_as_cnct, a_as_gold_cnct)
    print(res)

    # build antisymm array from 2d arrays
    #sh_base = (2,3)
    sh_base = (2,3,5)
    n_a = len(sh_base)
    a_list  = [ np.random.rand(l, dim) for l in sh_base ]
    a = np.einsum(','.join([ 'abcdefghijklmnopqrstvuwxyz'[2*i: 2*(i+1)] 
                             for i in range(n_a)]),
                  *a_list)
    nd_axes    = tuple([ 2*i-1 for i in range(len(sh_base)) ])
    a_nd_gold = UTEST_tensor_asym_gold(a, axes=nd_axes)
    
    # build at once from direct product
    a_rd = tensor_asym(a, nd_axes)
    res = res and cmp_arr_asymindex(n_a, dim, a_nd_gold, nd_axes, a_rd, -1)
    print(res)
    a_rd = None

    # build up antisymm tensor by outer products
    if n_a <= 0 : a_rd = None
    elif n_a == 1 : a_rd = a_list[0]
    else : 
        a_rd = tensor_outer_asym(dim, 1, a_list[0], 1, a_list[1])
        cnt = 2
        for x in a_list[2:] :
            a_rd = tensor_outer_asym(dim, cnt, a_rd, 1, x)
            cnt += 1
    #return a_nd_gold, a_rd # FIXME remove debug
    #print(a_nd_gold.shape, a_rd.shape)
    # compare
    res = res and cmp_arr_asymindex(n_a, dim, a_nd_gold, nd_axes, a_rd, -1)

    return res


def UTEST_tensor_outer_antisymm():
    N  = None       # for broadcast slices
    a, b, c = np.r_[1,3,5,0], np.r_[5,1,3,0], np.r_[3,5,1,0]
    expr = [
        # n=2
        'UTEST_tensor_asym_gold(a[:,N]*b[N,:], axes=(0,1))',
        'tensor_asym(a[:,N]*b[N,:], axes=(0,1)) / math.sqrt(2)',
        'tensor_outer_asym(4, 1, a, 1, b) / math.sqrt(2)',
        # n=3
        'UTEST_tensor_asym_gold(a[:,N,N]*b[N,:,N]*c[N,N,:], axes=(0,1,2))',
        'tensor_asym(a[:,N,N]*b[N,:,N]*c[N,N,:], axes=(0,1,2)) / math.sqrt(6)',
        'tensor_outer_asym(4, 2, tensor_outer_asym(4, 1, a, 1, b), 1, c) / math.sqrt(6)',
        ]
    for s in expr:
        print('#', s)
        print(eval(s))


"""
#UTEST for both combination_rlex_iter_2, combination_rlex_iter
for n,cmax in [ (n,cmax) for cmax in range(1,11) for n in range(0, cmax+2) ]:
    cl = [ list(c) for c in combination_rlex_iter_2(n,cmax) ]
    print("%3d\t%3d\t%3d\t%3d\t%s\t%s" % (n, cmax, binom_coeff(cmax,n), len(cl), binom_coeff(cmax,n) == len(cl), is_rlex_combinations(cl, cmax)))

# invoke utest:
lhpd.pymath.UTEST_tensor_asym_flat_group_ungroup(sh_base=(3,4), dim=8, asym_ranks=(2,3) )

# make a Levi-Civita Symbol : rank=dim=4 asym-flat tensor is a len=1 array
eps4d = tensor_asym_flat_ungroup(np.array([math.sqrt(24)]), 4, [1]*4)



"""

if False :      # attic 

    def tensor_flat_parse_operands(arr_list, **kwargs) :
        # FIXME change into (a, rank, [axis]) with def axis =-1
        """ parse operand parameters for asym_flat functions
            arr     list of array specs, can be
                    * ndarray, rank defaults to 1, axis defaults to axes[i]
                    * tuple = (n_a, ndarray, [axis]), 
                        * n_a = rank of antisymmetric index
                        * ndarray
            param   axes : defaults for all arrays
                    * int   : the same for all arrays
                    * tuple : may be different ; len must be the same as arr
            all arrays are transformed into ndarrays
            return same-len tuples (n_a, ...), (array, ...), (axis, ...)
        """
        n       = len(arr)
        if 'axes' in param : 
            axes = param['axes']
        else : 
            axes    = -1

        if isinstance(axes, collections.Iterable) : 
            axes = list(axes)
        else : 
            axes = [int(axes)] * n
        assert(len(axes) == n)

        for i, a in enumerate(arr) : 
            if isinstance(a, collections.Iterable) :
                a   = tuple(a)
                if len(a) < 2 or 3 < len(a) : 
                    raise ValueError(a)
                elif 3 == len(a) :
                    axes[i] = a[2]
                n_a_i, a_i = a[:2]
            else : 
                n_a_i, a_i = 1, a

            n_a.append(n_a_i)
            arr.append(np.asarray(a_i))

        return tuple(n_a), tuple(arr), tuple(axes)


    # TODO rewrite with `tensor_asym_flat_index_iter' iterator
    def tensor_asym_flat_outer(dim, n_a, a, n_b, b, axis1=-1, axis2=-1, out=None) :
        """ the resulting antisymmetric axis will be (-1)
        """
        if dim < n_a or dim < n_b : 
            raise IndexError
        len_a = binom_coeff(dim, n_a)
        len_b = binom_coeff(dim, n_b)
        if a.shape[axis1] != len_a or b.shape[axis2] != len_b :
            raise IndexError

        n_ab = n_a + n_b
        if (dim < n_ab) : 
            return 0
        len_ab = binom_coeff(dim, n_ab)
        combi_factor = 1. / math.sqrt(binom_coeff(n_ab, n_a)) # sqrt(n_ab! / n_a! / n_b!)

        # shape of the resulting array
        sh_a = list(a.shape)
        del sh_a[axis1]
        sh_b = list(b.shape)
        del sh_b[axis2]
        sh_ab = tuple(sh_a + sh_b + [len_ab])
        dtype_ab = np.find_common_type([], [a.dtype, b.dtype, float])
        if None is out : out = np.zeros(sh_ab, dtype=dtype_ab)
        else : out.fill(0)

        # slices for direct product broadcast
        sl_a = (slice(None),) * len(sh_a) + (None,) * len(sh_b)
        sl_b = (None,) * len(sh_a) + (slice(None),) * len(sh_b)

        for i_ab, idx_ab_ in enumerate(it_combinations_rlex(range(dim), n_ab)) :
            idx_ab = tuple(idx_ab_)
            #print(' ==', i_ab, list(idx_ab))
            for k_a in it_combinations_rlex(range(n_ab), n_a) :
                # FIXME replace with ndarrays?
                m_a, m_b = [0]*n_ab, [1]*n_ab
                for l in k_a :
                    m_a[l], m_b[l] = 1, 0
                idx_a   = tuple(it.compress(idx_ab, m_a))
                i_a     = combination_to_rlex_rank(idx_a)
                idx_b   = tuple(it.compress(idx_ab, m_b))
                i_b = combination_to_rlex_rank(idx_b)
                sign_ab = 1 - 2 * (permut_count(idx_a + idx_b) % 2) #FIXME simpler method?
                #print(i_a, list(idx_a), i_b, list(idx_b), sign_ab)
                #print(idx_ab, i_a, list(it.compress(idx_ab, m_a)), i_b, list(it.compress(idx_ab, m_b)), sign_ab)
                # TODO optimize (+/-) multiplication
                # TODO may be more optimal to construct full slices in sl_a, sl_b instead of using 'take'
                out[..., i_ab] += (sign_ab 
                                    * np.asarray(a.take(i_a, axis1))[sl_a]
                                    * np.asarray(b.take(i_b, axis2))[sl_b])
            out[..., i_ab] *= combi_factor

        return out


