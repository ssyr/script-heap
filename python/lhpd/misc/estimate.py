import os
import numpy as np
import itertools as it
import zlib
import math
import random
import h5py
from .numpy_extra import np_at_slice
from .cov_matr import norm_cov_matrix

"""
        --- Random thoughts on fit data organization ---
data should carry the following indices:
  [i_data][i_x][i_internal]
where
    i_data      the sample index, over which one must "accumulate" 
                to get average and variation
    i_x         the range index;
                it is relevant for SJK-ensembles when different (independent!)
                ensembles correspond to different subsets {i_x}
                Example: fitting form factors as functions of (mpi, q2): 
                  * ensembles with different mpi's are independent
                  * q2 values from these ensembles are not equal, so we cannot 
                    treat the data as [i_mpi][i_q2] array with simple i_mpi->mpi
                    and i_q2->q2 relations
                Important: this is the range to be used to dump the model function
    i_internal  internal data index: must have the same meaning for all the ensembles
                which is FIXED once the model_func object is created
                Example: form factor type
                Example: time `t' in c2pt(t) fits or its equivalent; especially 
                    when analyzing "normalized" correlator
                        x_{ij}(t) = c2pt_{ij}(t)/c2pt_{00}(t0)
                    so that i_internal may be some complicated numbering scheme
                    in such case, it must be passed to model_func.__init__
                    OR
                    time may be encoded in i_x, does not have to be passed to __init__
                Example: real/imag part of a complex number


Standard methods:

model_func.f_range(xr, p)
    fitter interface
    must return an 1d vector flattened over [i_x][i_internal]->[i]

model_func.f_range_pretty(xr, p, ...)   ??? better name?
    specifically for reporting model functions over some range
    must return np.ndarray with len(shape) <=3
    shape is not directly related to fit_func.f_range 
    may take additional parameters (dictionary?), but must be able to set 
        their default values
    ??? incorporate arguments to the returned value?

??? fit_func.make_data must return 2d vector[i_data, i] OR one should iterate 
    over i_data outside?
"""


def cov_elem(vec): return np.outer(vec, vec)

def jk_gaussian_estimate(ens, binsize=1, diag=False):
    """ 
    a cycle over JK ensemble of resampled values
    return (avg, cov)
    OBSOLETE use resample_jk_iter
    """
    if len(ens) <= 2 or binsize < 1: raise StopIteration
    n = len(ens)
    n_bins = n / binsize
    if n_bins <= 1: raise StopIteration
    n = n_bins * binsize
    sum = reduce(lambda x,y: x + y, ens, 0)
    avg = sum / n
    sum_cov = reduce(lambda x,y: x + cov_elem(y - avg), ens, 0)
    n_sub_ens = (n_bins - 1) * binsize
    for i_bin in range(n_bins):
        sub_ens = ens[binsize*i_bin:binsize*(i_bin+1)]
        avg_sample = (sum - reduce(lambda x,y: x + y, sub_ens, 0)) / n_sub_ens
        # this estimates the variance of avg_sample; change to estimator of the variance of average.
#        cov_sample = (reduce(lambda x,y: x + cov_elem(y - avg_sample), ens, 0) - 
#                        reduce(lambda x,y: x + cov_elem(y - avg_sample), sub_ens, 0)) / n_sub_ens \
#                        * binsize / n / (n_sub_ens - 1)
        # with this method of evaluating the covariance, 
        # E(1st line) = (1-1/n)Dx
        # D(avg_sample) = 1/(n-binsize)Dx
        cov_sample = (sum_cov - reduce(lambda x,y: x + cov_elem(y - avg), sub_ens,0)) / n_sub_ens \
                            * n / n_sub_ens / (n - 1)
        if diag:
            for i in range(cov_sample.shape[0]):
                for j in range(i): 
                    cov_sample[i,j] = 0.
                    cov_sample[j,i] = 0.
        yield (avg_sample, cov_sample)

def resample_var_factor(rsplan, n_data):
    """ the factor one has to multiply data variance with to get 
        the true mean uncertainty 
        rsplan  resample plan specification; None is equivalent to ('av',)
        n_data  the number of data points
    """
    if (None is rsplan 
            or 'av' == rsplan[0]
            or 'ama' == rsplan[0]):   
        return 1. / max(1, n_data - 1)
    elif ('jk' == rsplan[0]):               return float(max(1, n_data - 1))
    elif ('bs' == rsplan[0]):               return 1.
    else : raise ValueError("unknown rsplan='%s'" % rsplan[0])

# the following functions apply resample_var_factor={1/sqrt(n-1), sqrt(n-1), 1}
# because var, cov are computed with ddof=0, ie
#   var(a) = \sum_i^n (a_i - mean(a))^2 / n
#   cov(a)^{ab} = \sum_i^n (a^a_i a^b_i - mean(a^a) mean(a^b)) / n
def calc_err(a, rsplan=None, axis=0):
    a   = np.asarray(a)
    return np.sqrt(a.var(axis, ddof=0) * resample_var_factor(rsplan, a.shape[axis]))
def calc_avg(a, axis=0):
    a   = np.asarray(a)
    return a.mean(axis)
def calc_avg_err(a, rsplan=None, axis=0):
    return calc_avg(a, axis=axis), calc_err(a, rsplan=rsplan, axis=axis)
def ae2lohi(ae) : 
    return (ae[0] - ae[1], ae[0] + ae[1])
def calc_avg_cov(a, rsplan=None, axis=0):
    a   = np.asarray(a)
    a_cov   = np.cov(a, rowvar=axis, ddof=0) * \
               resample_var_factor(rsplan, a.shape[axis])
    return calc_avg(a, axis=axis), np.atleast_2d(a_cov)

def calc_cov(a, rsplan=None, axis=0) :    
    a_cov   = np.cov(a, rowvar=axis, ddof=0) * \
               resample_var_factor(rsplan, a.shape[axis])
    return np.atleast_2d(a_cov)

def calc_avg_err_ncov(a, rsplan=None, axis=0):
    a_avg, a_cov = calc_avg_cov(a, rsplan=rsplan, axis=axis)
    a_err = norm_cov_matrix(a_cov)
    return a_avg, a_err, a_cov


def resample_chi2_factor(rsplan, n_data):
    """ the factor to recover the correct value of chi2 """
    if (None is rsplan or 'av'==rsplan[0]):   return 1. / max(1, n_data - 1)
    elif ('jk' == rsplan[0]):               return 1.
    elif ('bs' == rsplan[0]):               return 1.   # TODO: check

def binit(ens, binsize=1) :
    """ 'cause just bin() is not convincing enough
            binsize     
                int: bin size; 
                tuple: 
                    ('ama', n_apprx, n_exact) bin with bias corrected
                        data in each bin must be in the following form:
                        bin size = n_apprx + 2*n_exact
                        approx samples [0:n_apprx]; approx samples for bias
                        correction[+0: +n_exact]; exact samples [+0; +n_exact]

        bins are computed in a simple loop once over the ensemble 
        must not be a problem: binning is not supposed to be called O(Ndata) times

        ens may be any collection: np.array, list, tuple, etc; useful to make lists 
        of array slices without copying

        return is always a np.array
    """
    if 1 == binsize : 
        
        return np.asarray(ens)

    if isinstance(binsize, (int, long)) : 
        
        if binsize <= 0 :
            raise ValueError('incorrect binsize=%d' % binsize)
        n_data  = len(ens)
        if n_data <= 0 :
            raise ValueError('empty data')
        n_bins  = n_data / binsize
        n_data_bins = n_bins * binsize
        if n_data_bins != n_data :
            import warnings
            warnings.warn('last %d samples are ignored because of binning' % (
                                n_data - n_data_bins), RuntimeWarning)

        # be careful not to create a numpy-copy of the data
        # every element must already be an np.array
        res_sh  = (n_bins,) + ens[0].shape
        res_dt  = np.find_common_type([], [np.float_, ens[0].dtype])
        res     = np.zeros(res_sh, res_dt)
        for i_bin in range(n_bins) :
            for i in range(binsize) : 
                res[i_bin] += ens[i_bin * binsize + i]
        res /= binsize
        return res

    if isinstance(binsize, tuple) :
        if 'ama' == binsize[0] :
            assert(len(binsize) == 3)
            n_apprx, n_exact = binsize[1:3]
            assert(0 < n_apprx)
            assert(0 < n_exact)
            bsize   = n_apprx + 2 * n_exact
            n_data  = len(ens)
            if n_data <= 0 :
                raise ValueError('empty data')
            n_bins  = n_data // bsize
            n_data_bins = n_bins * bsize
            if n_data_bins != n_data :
                raise ValueError('datasize=%d inconsistent with AMA(%d,%d)->%d entries/bin' %(
                                    n_data, n_apprx, n_exact, bsize))
            x_ap = np.zeros_like(ens[0])
            x_ex = np.zeros_like(ens[0])
            x_ap0= np.zeros_like(ens[0])
            res_sh  = (n_bins,) + ens[0].shape
            res_dt  = np.find_common_type([], [np.float_, ens[0].dtype])
            res     = np.empty(res_sh, res_dt)
            for i_bin in range(n_bins) :
                x_ap.fill(0), x_ex.fill(0), x_ap0.fill(0)
                ir  = i_bin * bsize
                for i in range(n_apprx) :   x_ap += ens[ir + i]
                ir += n_apprx
                for i in range(n_exact) :   x_ap0+= ens[ir + i]
                ir += n_exact
                for i in range(n_exact) :   x_ex += ens[ir + i]
                res[i_bin] = x_ap / n_apprx + (x_ex - x_ap0) / n_exact

            return res
            
        else :
            raise ValueError('incorrect bin option "%s"' % binsize[0])

def get_binstride(binsize) :
    if isinstance(binsize, (int, long)) :
        return binsize
    elif isinstance(binsize, tuple) :
        if binsize[0] == 'ama' :
            assert(len(binsize) == 3)
            return binsize[1] + 2 * binsize[2]
        else:
            raise ValueError('unknown bintype "%s"' % binsize[0])
    else :
        raise ValueError('unknown binsize type "%s"' % str(type(binsize)))

def rsplan_nobin(rsplan) :
    return (rsplan[0], 1) + rsplan[2:]



# accumulators : resample without storint intermediate results
# instead, accumulate bin average and return result when a bin is complete
# XXX self.binstride >0 is flag: ?active
class onebin_accumulate :
    def __init__(self, binsize, shape, dtype) :
        self.binsize    = binsize
        self.binstride  = get_binstride(binsize)
        self.count      = 0
        self.res        = np.zeros(shape, dtype)
        if isinstance(binsize, (int, long)) : pass
        elif isinstance(binsize, tuple) :
            if 'ama' == binsize[0] :
                assert(len(binsize) == 3)
                self.n_apprx, self.n_exact = binsize[1:3]   
                self.res_ap0    = np.zeros_like(self.res)
                self.res_ex     = np.zeros_like(self.res)
            else :
                raise ValueError('unknown bintype "%s"' % binsize[0])
        else :
            raise ValueError('unknown binsize type "%s"' % str(type(binsize)))

    def get_binstride(self) : return self.binstride

    def complete(self) :
        assert (0 < self.binstride)
        return (self.binstride == self.count)

    def update(self, x) :
        assert(not self.complete())
        if isinstance(self.binsize, (int, long)) :
            self.res += x
        elif isinstance(self.binsize, tuple) :
            if 'ama' == self.binsize[0] :
                if self.count < self.n_apprx :
                    self.res    += x
                elif self.count < self.n_apprx + self.n_exact :
                    self.res_ap0+= x
                else :
                    self.res_ex += x
            else :
                raise ValueError('unknown bintype "%s"' % binsize[0])
        else :
            raise ValueError('unknown binsize type "%s"' % str(type(binsize)))
        self.count += 1

    def result(self) :
        assert(self.complete())
        if isinstance(self.binsize, (int, long)) :
            res = self.res / self.binsize
        elif isinstance(self.binsize, tuple) :
            if 'ama' == self.binsize[0] :
                res = self.res / self.n_apprx + (self.res_ex - self.res_ap0) / self.n_exact
            else :
                raise ValueError('unknown bintype "%s"' % binsize[0])
        else :
            raise ValueError('unknown binsize type "%s"' % str(type(binsize)))

        del self.res        # prohibit further use
        self.binstride = 0
        return res


class binit_accumulate :
    def __init__(self, binsize, shape, dtype) :
        self.binsize    = binsize
        self.shape      = shape
        self.dtype      = dtype
        self.binstride  = get_binstride(binsize)
        self.res        = []
        self.count      = 0
    def get_binstride(self) : return self.binstride
    def complete(self) :
        assert (0 < self.binstride)
        return (0 == self.count % self.binstride)

    def update(self, x) :
        if self.complete() :
            self.onebin_a = onebin_accumulate(self.binsize, self.shape, self.dtype)
        self.onebin_a.update(x)
        self.count += 1
        if self.onebin_a.complete() :
            self.res.append(self.onebin_a.result())
            del self.onebin_a

    def result(self):
        assert(self.complete())
        res = np.array(self.res)
        del self.res
        self.binstride = 0
        return res

# FIXME make a child of binit_accumulate? overload __init__, result
class resample_accumulate :
    def __init__(self, rsplan, shape, dtype) :
        self.rsplan = rsplan
        self.shape  = shape
        self.dtype  = dtype
        self.binit_a= binit_accumulate(rsplan[1], shape, dtype)
    def get_binstride(self) : return self.binit_a.get_binstride()
    def complete(self) : return self.binit_a.complete()
    def update(self, x) : self.binit_a.update(x)
    def result(self) :
        assert(self.complete())
        res = resample(self.binit_a.result(), rsplan_nobin(self.rsplan))
        del self.binit_a
        return res

def make_ama_bins_simple(n_apprx, n_exact, ens_ap, ens_ap0, ens_ex) :
    """ assume ordered sequence 
        blocks of n_apprx in ens_ap <=> blocks of n_exact in ens_ap0 <=> in ens_ex
    """
    n_bins  = len(ens_ap) // n_apprx
    assert(0 < n_bins)
    if n_bins * n_apprx != len(ens_ap) :
        raise ValueError('approx samples count mismatch')
    if n_bins * n_exact != len(ens_ap0) :
        raise ValueError('approx0 samples count mismatch')
    if n_bins * n_exact != len(ens_ex) :
        raise ValueError('exact samples count mismatch')
    x_sh= ens_ap[0].shape
    bsize   = n_apprx + 2 * n_exact
    res = np.empty((n_bins * bsize,) + x_sh, np.find_common_type([], 
                [ens_ap[0].dtype, ens_ap0[0].dtype, ens_ex[0].dtype]))
    for i_bin in range(n_bins) :
        ia, ie  = i_bin * n_apprx, i_bin * n_exact
        ir  = i_bin * (n_apprx + 2 * n_exact)
        res[ir : ir + n_apprx] = ens_ap[ia : ia + n_apprx]
        ir += n_apprx
        res[ir : ir + n_exact] = ens_ap0[ie : ie + n_exact]
        ir += n_exact
        res[ir : ir + n_exact] = ens_ex[ie : ie + n_exact]
    return res



def UTEST_binit_resample() :
    def cmp_rs_func_class(x, rsplan) :
        rsplan1 = rsplan_nobin(rsplan)
        x_b     = binit(x, binsz)
        x_rs    = resample(x, rsplan)
        x_rs2   = resample(x_b, rsplan1)


        # binit_accumulate
        binit_a = binit_accumulate(binsz, x.shape[1:], x.dtype)
        for xi in x : binit_a.update(xi)
        x2_b    = binit_a.result()
        resmp_a = resample_accumulate(rsplan, x.shape[1:], x.dtype)
        for xi in x : resmp_a.update(xi)
        x2_rs   = resmp_a.result()
        res = (np.allclose(x_rs, x_rs2),
               np.allclose(x_rs, x2_rs),
               np.allclose(x_b, x2_b))
        assert all(res) # DEBUG hook
        print(res)

    ndata   = 1000
    ndata_ex= 100
    nsloppy2ex = ndata / ndata_ex
    shape   = (4,3)
    x       = np.random.rand(*((ndata,) + shape))
    x_ex    = np.random.rand(*((ndata_ex,) + shape))

    # no ama
    binsz   = 1
    rsplan  = ('jk', binsz)
    cmp_rs_func_class(x, rsplan)

    # no ama
    binsz   = 10
    rsplan  = ('jk', binsz)
    cmp_rs_func_class(x, rsplan)
    
    # ama
    binsz   = ('ama', 10, 1)
    rsplan  = ('jk', binsz)
    cmp_rs_func_class(make_ama_bins_simple(binsz[1], binsz[2], 
                x, x[::nsloppy2ex], x_ex), rsplan)

    # ama
    binsz   = ('ama', 20, 2)
    rsplan  = ('jk', binsz)
    cmp_rs_func_class(make_ama_bins_simple(binsz[1], binsz[2], 
                x, x[::nsloppy2ex], x_ex), rsplan)

def gaussian_estimate(ens, rsplan=None):
    """
    DEPRECATED
    estimate average and its covariance matrix
        ens     vector of samples
        diag    whether set off-diag elements to zero
        rsplan   resampling plan used; cov is adjusted accordingly
    returns:    (avg, cov)
        avg     average
        cov     covariance of average estimate
    """
    n = len(ens)
    if n <= 1: return None
    avg = reduce(lambda x,y: x + y, ens, 0) / n
    cov = reduce(lambda x,y: x + cov_elem(y - avg), ens, 0) / n
    if (None is rsplan or 'av'==rsplan[0]):   cov /= n - 1
    elif ('jk' == rsplan[0]):               cov *= n - 1
    elif ('bs' == rsplan[0]):               pass
    else: raise ValueError('unknown resampling %s' % rsplan[0])
    return (avg, cov)



def meff_estimate(ens, p, t_range, rsplan=None):
    """
    add resampling factor to cov depending on resampling plan (default='av')
    """
    c2pt = [ x.get_avg_equiv(p) for x in ens ]
    meff = [ np.array([ math.log(c2[t] / c2 [t+1]) for t in t_range ])
             for c2 in resample_iter(c2pt, rsplan=rsplan) ]
    return lhpd.calc_avg_cov(meff, rsplan=rsplan)

def meff_diff_estimate(ens, p1, p2, t_range, rsplan=None):
    """
    compute plateau for E(p2) - E(p1)
    """
    c1  = [ x for x in resample_iter([ x.get_avg_equiv(p1) for x in ens ], rsplan=rsplan) ]
    c2  = [ x for x in resample_iter([ x.get_avg_equiv(p2) for x in ens ], rsplan=rsplan) ]
    meff= [ np.array([ math.log(c2[i][t] / c2[i][t+1] / c1[i][t] * c1[i][t+1]) 
                                for t in t_range ])
                  for i in range(len(c1)) ]
    return lhpd.calc_avg_cov(meff, rsplan=rsplan)

def deltameff_estimate(ens, p, t_range, rsplan=None):
    """
    add resampling factor to cov depending on resampling plan (default='av')
    """
    c2pt = [ x.get_avg_equiv(p) for x in ens ]
    dmeff = [ np.array([ math.log(c2[t] * c2[t+2] / c2 [t+1]**2) for t in t_range ])
              for c2 in resample_iter(c2pt, rsplan=rsplan) ]
    return lhpd.calc_avg_cov(dmeff, rsplan=rsplan)


"""
new functions
"""
def resample_jk_iter(ens, binsize=1):
    """ 
    iterate over single-eliminated (JK resampling plan) ensemble averages
    return average
    """
    ens = binit(ens, binsize)
    n = len(ens)
    assert(2 <= n)
    sum = ens.sum(0)
    for x in ens : 
        yield (sum - x) / (n - 1)

def resample_cov_jk_iter(ens, binsize=1):
    """ 
    iterate over single-eliminated (JK resampling plan) ensemble averages
    return jk sample mean, covariance
    """
    ens = binit(ens, binsize)
    n = len(ens)
    assert(2 < n)
    ens     = ens.reshape(n, -1)
    xsum    = ens.sum(0)
    dens    = ens - xsum / n
    xcov    = np.tensordot(dens, dens, (0,0))
    for x, dx in zip(ens, dens) :
        yield ( 
            (xsum - x) / (n - 1),                           
            (xcov - n * np.outer(dx, dx) / (n - 1)) / (n - 1) / (n - 2) )

bsmap_cache = {}
def resample_get_bsmap(n, nboot, bsmap) :
    if None is bsmap : 
        try : bsmap = bsmap_cache[None]
        except :
            print("WARN[resample_bs_iter] generating bsmap(nboot=%d, n=%d)" % (n, nboot))
            bsmap   = np.random.randint(0, high=n, size=(nboot, n))
            bsmap_cache[None] = bsmap
    elif isinstance(bsmap, str) :
        bsmap_uuid = bsmap
        try : bsmap = bsmap_cache[bsmap_uuid]
        except KeyError :
            h5f = h5py.File("%s/DATA/testfiles/bsmap_%s.h5" % (os.environ['HOME'], bsmap_uuid), 'r')
            bsmap = np.asarray(h5f['/cc'][()])
            bsmap_cache[bsmap_uuid] = bsmap
    else : raise TypeError
    
    return bsmap

def resample_bs_iter(ens, binsize=1, nboot=1024, bsmap=None) :
    """ draw nboot bootstrap samples from binsize-binned ensemble
        binsize     bin data
        nboot       number of bootstrap
        bsmap       bootstrap map (needs to be constant for consistency)
    """
    ens_binned  = binit(ens, binsize)
    n           = len(ens_binned)
    bsmap       = resample_get_bsmap(n, nboot, bsmap)

    assert(2 == len(bsmap.shape))
    assert(nboot <= bsmap.shape[0]) # TODO otherwise, generate larger bsmap
    assert(n <= bsmap.shape[1])     # TODO otherwise, generate larger bsmap or bin data

    for s in bsmap[:nboot] :
        yield ens_binned[s[:n] % n].mean(0)

def resample_cov_bs_iter(ens, binsize=1, nboot=1024, bsmap=None) :
    """ draw nboot bootstrap samples from binsize-binned ensemble
        binsize     bin data
        nboot       number of bootstrap
        bsmap       bootstrap map (needs to be constant for consistency)
    """
    ens_binned  = binit(ens, binsize)
    n           = len(ens_binned)
    bsmap       = resample_get_bsmap(n, nboot, bsmap)

    assert(2 == len(bsmap.shape))
    assert(nboot <= bsmap.shape[0]) # TODO otherwise, generate larger bsmap
    assert(n <= bsmap.shape[1])     # TODO otherwise, generate larger bsmap or bin data

    for s in bsmap[:nboot] :
        x   = ens_binned[s[:n] % n]
        xa  = x.mean(0)
        dx  = x - xa
        yield ( x.mean(0), 
                np.tensordot(dx, dx, (0,0)) / float(n * (n - 1)) )



def resample_iter(ens, rsplan=None):
    """
    iterate over resampled ensemble with specified method
        rsplan       'av'|None   no resampling
                        'jk'        Jackknife
                        'bs'        NOT IMPLEMENTED
        binsize         how to bin data
    """
    if (None is rsplan):  return ens.__iter__()
    assert isinstance(rsplan, tuple)
    if ('av'==rsplan[0]):
        return iter(binit(ens, rsplan[1]))
    elif ('jk' == rsplan[0]):
        return resample_jk_iter(ens, rsplan[1])
    elif ('bs' == rsplan[0]):
        binsize, nboot, bsmap = 1, 1024, None
        if 2 <= len(rsplan) : binsize = rsplan[1]
        if 3 <= len(rsplan) : nboot   = rsplan[2]
        if 4 <= len(rsplan) : bsmap   = rsplan[3]
        return resample_bs_iter(ens, binsize=binsize, nboot=nboot, bsmap=bsmap)
    else: raise ValueError('unknown resampling %s' % rsplan[0])

def resample_cov_iter(ens, rsplan=None):
    """
    iterate over resampled ensemble with specified method
        rsplan       'av'|None   no resampling
                        'jk'        Jackknife
                        'bs'        NOT IMPLEMENTED
        binsize         how to bin data
    """
    if (None is rsplan):  return ens.__iter__()
    assert isinstance(rsplan, tuple)
    if ('av'==rsplan[0]) : raise NotImplementedError
    elif ('jk' == rsplan[0]):
        return resample_cov_jk_iter(ens, rsplan[1])
    elif ('bs' == rsplan[0]):
        binsize, nboot, bsmap = 1, 1024, None
        if 2 <= len(rsplan) : binsize = rsplan[1]
        if 3 <= len(rsplan) : nboot   = rsplan[2]
        if 4 <= len(rsplan) : bsmap   = rsplan[3]
        return resample_cov_bs_iter(ens, binsize=binsize, nboot=nboot, bsmap=bsmap)
    else: raise ValueError('unknown resampling %s' % rsplan[0])

def rsplan_str(rsplan) :
    if (None is rsplan):  return 'av_bin1'
    assert isinstance(rsplan, tuple)
    if   ('av' == rsplan[0]) : return 'av_bin%d' % rsplan[1]
    elif ('jk' == rsplan[0]) : return 'jk_bin%d' % rsplan[1]
    elif ('bs' == rsplan[0]) : 
        binsize, samples = rsplan[1:3]
        if isinstance(samples, int) or isinstance(samples, long) :
            return 'bs_bin%d_samples%d' % (binsize, samples)
        else :
            m,n = samples.shape[:2]
            samp_crc32 = np.uint32(zlib.crc32(samples.astype('>i4')))
            return 'bs_bin%d_samples%dx%d_crc%X' % (binsize, m, n, samp_crc32)
    else: raise ValueError('unknown resampling %s' % rsplan[0])

def resample(ens, rsplan=None):
    return np.array([ x for x in resample_iter(ens, rsplan=rsplan) ])

def resample_cov(ens, rsplan=None):
    res = [ x for x in resample_cov_iter(ens, rsplan=rsplan) ]
    return np.array([ x[0] for x in res ]), np.array([ x[1] for x in res ])

def rebin(a, binsize, rsplan) :
    """ (re)bin resampled ensemble preserving the size of the errorbar
    
        NOTE no default for rsplan - must specify """
    n_data  = len(a)
    n_bins  = n_data // binsize
    n_data_bins = n_bins * binsize
    n_cut   = n_data - n_data_bins
    if (0 != n_cut) :
        import warnings
        warnings.warn('last %d samples are ignored because of binning' % (n_cut), 
                    RuntimeWarning)

    # (re)bin
    a_sh    = (n_bins, binsize) + a.shape[1:]
    a_b     = a[:n_data_bins].reshape(a_sh).mean(1)

    # scale distribution around central to preserve error size
    if (None is rsplan or 'av' == rsplan[0] or 'ama' == rsplan[0]): k = 1.
    elif ('jk' == rsplan[0]): k = float(binsize)
    elif ('bs' == rsplan[0]): k = np.sqrt(binsize)
    else : raise ValueError("unknown rsplan='%s'" % rsplan[0])
    return k * a_b + (1. - k) * a_b.mean(0)


def make_sjk(xr_list, data_list):
    """
    make a super-Jackknife ensemble from lists of datasets `data_list' 
    and x-ranges `xr_list', len(data_list) == len(xr_list)

    xr_list     [ [ x^a_1, x^a_2, ..., x^a_ra ], [ x^b_1, x^b_2, ..., x^b_rb ], ... ]
        where all x.shape==X (parameter type is always the same)
        * the second level of nesting may be a list as well
        * x^a_n may be np.ndarray OR any type appropriate as np.dtype
        FIXME tried to treat xr_list=[ x_1, x_2, ...] as [ [x1], [x2], ...]
              testing needed
              to be safe, wrap it explicitly
    data_list   [ data^a, data^b, ... ]
        where data^a.shape == (Na, ra)+Y
            with Na = #samples in {a}, 
                 ra = #range of arg of {a}
                 Y = internal structure of data
        * data^a must be np.ndarray
        * the dimension data^a.shape[1] must be xrange, even if it is trivial and ==1
    create data_sjk = [N, r, Y]
    create xr_sjk   = [r, X]
    """
    def xr_category(xr):
        """ return len, type, shape of xr """
        xr_len, Xtype, Xshape   = None, None, None
        if isinstance(xr, list) :
            assert (0 < len(xr))
            xr_len = len(xr)
            if isinstance(xr[0], np.ndarray):
                Xtype = xr[0].dtype
                Xshape= xr[0].shape
            else:
                Xtype = type(xr[0])
                Xshape= ()
        elif isinstance(xr, np.ndarray):
            xr_len= xr.shape[0]
            Xtype = xr.dtype
            Xshape= xr.shape[1:]
        else:
            xr_len=1
            Xtype = type(xr)
            Xshape=()
            # TODO verify that the rest will work with the above and remove Exc.
            #raise ValueError("bad xrange '%s'" % str(xr))
        return xr_len, Xtype, Xshape

    assert(0 < len(xr_list) and len(xr_list) == len(data_list))

    xr_len, Xtype, Xshape = xr_category(xr_list[0])
    n_x = 0
    for i_xr, xr in enumerate(xr_list):
        xr_len_, Xtype_, Xshape_ = xr_category(xr)
        assert ((Xtype_, Xshape_) == (Xtype, Xshape))
        n_x += xr_len_
    xr_sjk = np.empty((n_x,)+Xshape, dtype=Xtype)

    assert (isinstance(data_list[0], np.ndarray))
    Ytype   = data_list[0].dtype
    Yshape  = data_list[0].shape[2:]
    n_data  = 0
    for i_data, data in enumerate(data_list):
        xr_len_, Xtype_, Xshape_ = xr_category(xr_list[i_data])
        assert (isinstance(data, np.ndarray))
        assert ((data.shape[1], data.shape[2:], data.dtype) == (xr_len_, Yshape, Ytype))
        n_data += data.shape[0]

    data_sjk = np.empty((n_data, n_x) + Yshape, dtype=Ytype)

    i_x = 0
    for i_xr, xr in enumerate(xr_list):
        xr_len_ = xr_category(xr)[0]
        xr_sjk[i_x : i_x + xr_len_] = xr
        data_cur_avg = data_list[i_xr].mean(0)
        i_y = 0
        for i_data, data in enumerate(data_list):
            data_len = data.shape[0]
            if (i_data == i_xr): 
                data_sjk[i_y : i_y + data_len, i_x : i_x + xr_len_] = data
            else: 
                data_sjk[i_y : i_y + data_len, i_x : i_x + xr_len_] = data_cur_avg[None]
            i_y += len(data)
        assert (i_y == n_data)
        i_x += xr_len_
    assert(i_x == n_x)

    return xr_sjk, data_sjk


def make_fixgauss(n):
    """ create n-size Gaussian distributed fixed sample, erf^-1(even) """
    from scipy.stats import norm
    return norm.ppf(np.r_[ .5/n : 1. - .5/n : n*1j ])

def fit_average(avg, stdev):
    """
        return average of independent measurements of the same value
        equivalent to trivial fit f_i=avg
        return (avg, err): 
            avg = \sum{avg_i / err_i**2} / \sum{1 / err_i**2}
            err = 1 / sqrt{\sum{1 / err_i**2}}
        TODO support rsplan?
    """
    assert (len(avg.shape) == 1 and 
            len(stdev.shape) == 1 and
            avg.shape == stdev.shape)
    stdev_rcsq_sum  = (1. / stdev**2).sum()
    sum = (avg / stdev**2).sum()
    return sum / stdev_rcsq_sum, math.sqrt(1. / stdev_rcsq_sum)

def mean_weighted_ve(x_avg, x_stdev, axis=-1):
    """ average over n samples
        x_avg.shape=([i_data,] [x_sh,] n) 
        x_stdev.shape=([x_sh,] n)
        where x_sh is the variable index
        axis = axis for n in x_avg (and x_stdev broadcast to x_avg.shape)

        RETURN: avg, err weighed with stdev, shape=([i_data, ] [x_sh])
        return average of independent measurements of the same value
        note : no averaging over i_data because it may be later used to resample or compute cov
    """
    assert (1 <= len(x_avg.shape) and     
            1 <= len(x_stdev.shape))
    stdev_rcsq      = 1. / x_stdev**2
    stdev_rcsq_sum  = stdev_rcsq.sum(axis=axis)
    avg_sum         = (x_avg * stdev_rcsq).sum(axis=axis)
    return avg_sum / stdev_rcsq_sum, np.sqrt(1. / stdev_rcsq_sum)


#def weighted_quantile(arr, quant, axis=-1):
#    """ arr must be sorted in asc order """
#    if len(arr) <= 0: return None
#    if len(arr) == 1: return arr[0]
#    assert (0. <= quant and quant <= 1.)
#    if quant == 1. : return arr[-1]
#    x   = (len(arr) - 1)  * quant
#    i   = int(math.floor(x))
#    g   = x - i
#    return arr[i] + g * (arr[i+1] - arr[i])
def weighted_quantile(arr, quant, is_sorted=False, axis=-1):
    """ return quantile(s) of data along specified axis
        if `quant' is an array, resp.quantiles are stacked along 0-axis
        array       at least 1d array of reals
        is_sorted   True if sorted in asc.order
        quant       scalar or 1-d array (list,tuple) of quantiles 0<=q<= 1
    """
    if is_sorted:
        arr = np.asarray(arr)
    else:
        arr = np.array(arr)  # copy
        arr.sort(axis=axis)

    a_len = arr.shape[axis]
    if -1 == axis: res_shape = arr.shape[0:-1]
    else: res_shape = arr.shape[0:axis] + arr.shape[axis+1:]

    def get_quantile(q):
        if a_len <= 0: return None
        if a_len == 1: return arr.reshape(res_shape)
        assert (0. <= q and q <= 1.)
        if q == 1. : return arr[-1]
        x   = (a_len - 1)  * q
        i   = int(math.floor(x))
        g   = x - i
        return (arr.take([i], axis=axis) + g * (arr.take([i+1], axis=axis) 
                    - arr.take([i], axis=axis))).reshape(res_shape)

    if (isinstance(quant, (tuple, list))) :
        return np.r_[ list(get_quantile(q) for q in quant) ]

    else: return get_quantile(quant)

def orderstats(arr, is_sorted=False, axis=-1):
    """ calculate (median, median-1sigma, median+1sigma) quantiles along specified axis
        arr         at least 1d array of reals
        is_sorted   True if sorted in asc.order
        axis        axis along which to take the quantiles
        RETURN      [ quantile(50%), quantile(16%), quantile(84%)]
    """
    return weighted_quantile(arr, (.5000, .1587, .8413), 
                             is_sorted=is_sorted, 
                             axis=axis),

def ordered_cdf(v, vcdf, weighted=True) :
    """ compute empirical cdf using weighted """
    vcdf    = np.array(vcdf)
    vcdf.sort()
    assert(1 == len(vcdf.shape))
    n       = vcdf.shape[0]
    iv = np.searchsorted(vcdf, v) 
    if   n == iv : return 1.
    elif 0 == iv : return 0.
    elif weighted and (v < vcdf[iv]) : # interpolate
        iv1 = iv - float(v - vcdf[iv]) / float(vcdf[iv-1] - vcdf[iv]) 
    else : iv1 = iv
    return iv1 / float(n)

# examining for outliers
def calc_med_mdev(v, axis=0) :
    """ compute median and deviation(with sign) from median ; mostly useful in functions below
        axis    data axis
    """
    v       = np.asarray(v)
    if np.iscomplexobj(v) :
        urm, urd = calc_med_mdev(v.real, axis=axis)
        uim, uid = calc_med_mdev(v.imag, axis=axis)
        return urm + 1j*uim, urd + 1j*uid
    bc_slice= np_at_slice(len(v.shape), (axis, None))
    v_m     = np.median(v, axis=axis)
    return v_m, v - v_m[bc_slice]

def calc_med_dev(v, axis=0) :
    """ compute median and median abs. deviation 
        axis    data axis
    """
    v       = np.asarray(v)
    v_m, v_d= calc_med_mdev(v, axis=axis)
    if np.iscomplexobj(v) : v_ad = np.abs(v_d.real) + 1j*np.abs(v_d.imag)
    else : v_ad   = np.abs(v_d)
    return v_m, np.median(v_ad, axis=axis) 
calc_med_mad = calc_med_dev # synonym

# TODO add to documentation
def rank_adfm(v, axis=0) :
    """ adfm = "absolude deviation from median" 
        return indices (along axis) sorted in decreasing adfm along axis
        adfm(v) [{I}, rank_adfm(v), {J}] is decreasing along axis for any fixed I,J
    """
    v       = np.asarray(v)
    if np.iscomplexobj(v) : raise TypeError("cannot rank complex obj")
    m, d    = calc_med_dev(v, axis=axis)
    return np.argsort(d, axis=axis)[::-1]
def calc_rdfm(v, axis=0) :
    """ rdfm = "relative deviation from median"  = "deviation from median(with sign)" / mad
        if mad is zero (all ==constant), rdfm=0
    """
    v       = np.asarray(v)
    if np.iscomplexobj(v) : 
        return calc_rdfm(v.real, axis=axis) + 1j * calc_rdfm(v.imag, axis=axis)
    v_m, v_d= calc_med_mdev(v, axis=axis)
    v_mad   = np.median(np.abs(v_d), axis=axis) # real
    v_mad1  = np.where(0 == v_mad, 1., v_mad)
    bc_slice= np_at_slice(len(v.shape), (axis, None))
    return v_d / v_mad1[bc_slice]

def rank_rdfm(v, axis=0) :
    v       = np.asarray(v)
    if np.iscomplexobj(v) : raise TypeError("cannot rank complex obj")
    return np.argsort(calc_rdfm(v, axis=axis), axis=axis)[::-1]

def calc_rdfm_max(v, axis=0) :
    """ for each sample, calc max rdfm over obsv.set 
        OUT[k] for IN[{I},k,{J}]
    """
    v_rdfm  = calc_rdfm(v, axis=axis)
    vlen    = v_rdfm.shape[axis]
    v_rdfm1 = np.moveaxis(v_rdfm, axis, 0).reshape((vlen, -1))
    return v_rdfm1.max(axis=1)

def rank_rdfm_max(v, axis=0, threshold=0) :
    """ adfm = "absolude deviation from median" 
        return indices (along axis) sorted in decreasing max(adfm/med(adfm)) along axis;
        adfm_max(v) [k] = max_{I,J} adfm(v)[{I}, k, {j}]
    """
    v       = np.asarray(v)
    if np.iscomplexobj(v) : raise TypeError("cannot rank complex obj")
    return np.argsort(calc_rdfm_max(v, axis=axis), axis=axis)[::-1]

def calc_normabsdev(v, axis=0) :
    """ compute mad-normalized abs.dev 
        axis    data axis
    """
    sh      = v.shape
    bc_slice= np_at_slice(len(sh), (axis, None))
    v_m, v_mad = calc_med_dev(v, axis=axis)
    return np.abs(v - v_m[bc_slice]) / v_mad[bc_slice]

def calc_devstat(v, axis=0) :
    """ compute outlier deviation statistics
        axis    data axis
    """
    sh      = v.shape
    v_nad   = calc_normabsdev(v, axis=axis)
    sum_axes= range(len(sh))
    sum_axes.pop(axis)
    return (v_nad**2).sum(tuple(sum_axes))

