from future.utils import iteritems

import numpy as np
import scipy as sp
import scipy.special
import sys
import math
import types
import itertools as it
from .numpy_extra import np_uniq

ldir_3d = dict(
    x = np.r_[ 1, 0, 0], y = np.r_[ 0, 1, 0], z = np.r_[ 0, 0, 1],
    q = np.r_[ 1, 1, 0], s = np.r_[ 0, 1, 1], v = np.r_[ 1, 0, 1], 
    r = np.r_[-1, 1, 0], u = np.r_[ 0,-1, 1], w = np.r_[ 1, 0,-1])
for k, v in iteritems(dict(ldir_3d)) : ldir_3d[k.upper()] = -v
ldir_inv = dict(
    x='X', X='x',
    y='Y', Y='y',
    z='Z', Z='z',
    t='T', T='t',
    q='Q', Q='q',
    r='R', R='r',
    s='S', S='s',
    u='U', U='u',
    v='V', V='v',
    w='W', W='w',
)

def space2full(c, t_axis, c_t=0) :
    c           = np.asarray(c)
    cnew_sh     = list(c.shape)
    cnew_sh[-1] += 1
    cnew        = np.empty(tuple(cnew_sh), c.dtype)
    cnew[..., : t_axis]     = c[..., : t_axis]
    cnew[..., t_axis + 1 :] = c[..., t_axis :]
    cnew[..., t_axis]       = c_t
    return cnew

def full2space(cfull, t_axis) :
    c           = np.asarray(c)
    cnew_sh     = list(c.shape)
    cnew_sh[-1] -= 1
    cnew        = np.empty(tuple(cnew_sh), c.dtype)
    cnew[..., : t_axis] = c[..., : t_axis]
    cnew[..., t_axis :] = c[..., t_axis + 1 :]





def gen_orbit(v, gen_list):
    res = [ np.asarray(v) ]
    cur = 0

    while cur < len(res):
        for g in gen_list:
            g_v = np.dot(g, res[cur])
            if not any(tensor_equal(g_v, v1) for v1 in res):
                res.append(g_v)
        cur += 1
    return res


h4_gen  = []

""" 
    group of functions to generate N-vectors of integers which are 
    representatives of classes wrt reflections and hypercubic rotations;
    vectors are generated so that c0>=c1>=...>=c{N-1}>=0
"""
def normsorted(x) :
    x    = np.asarray(x)
    iarg = np.argsort((x**2).sum(-1))
    return x[iarg]

def std_int_vectors_maxcomp(dim, comp_max):
    """ standard vectors filling a cube , c_i<=comp_max 
        "standard" means 0<=c_i, c_i>=c_{i+1}
    """
    if comp_max < 0 or dim < 0: raise StopIteration
    elif dim == 0: yield []
    else:
        for c0 in range(1+comp_max):
            for v in std_int_vectors_maxcomp(dim - 1, c0):
                yield [c0] + v

def int_vectors_maxcomp(dim, comp_max):
    """ vectors filling a cube , |c_i|<=comp_max """
    if comp_max < 0 or dim < 0: raise StopIteration
    elif dim == 0: yield []
    else:
        for v in int_vectors_maxcomp(dim - 1, comp_max):
            for c0 in range(-comp_max, 1+comp_max):
                yield v + [c0]

def std_int_vectors_fixnorm(dim, norm2, comp_max=-1):
    """ standard vectors forming a "shpere", \sum_i c_i^2==norm2;
        * if comp_max>=0, additionally c_i <= comp_max
        "standard" means 0<=c_i, c_i>=c_{i+1}
    """
    if norm2 < 0 or dim < 0: raise StopIteration
    elif dim == 0: 
        if 0 == norm2: yield []
    else:
        c0_max = int(math.sqrt(norm2))
        if 0 <= comp_max: 
            c0_max = min(c0_max, comp_max)
        for c0 in range(1+c0_max):
            for v in std_int_vectors_fixnorm(dim - 1, norm2 - c0 * c0, c0):
                yield [c0] + v

def int_vectors_fixnorm(dim, norm2, comp_max=-1):
    """ vectors forming a "shpere", \sum_i c_i^2==norm2;
        * if comp_max>=0, additionally c_i <= comp_max
    """
    if norm2 < 0 or dim < 0: raise StopIteration
    elif dim == 0: 
        if 0 == norm2: yield []
    else:
        c0_max = int(math.sqrt(norm2))
        if 0 <= comp_max: 
            c0_max = min(c0_max, comp_max)
        for c0 in range(-c0_max, 1+c0_max):
            for v in int_vectors_fixnorm(dim - 1, norm2 - c0 * c0, comp_max):
                yield v + [c0]

def std_int_vectors_maxnorm(dim, norm2, comp_max=-1, norm2_min=0):
    """ standard vectors forming a "shpere", \sum_i c_i^2==norm2;
        * if comp_max>=0, additionally c_i <= comp_max
        "standard" means 0<=c_i, c_i>=c_{i+1}
    """
    if norm2 < 0 or dim < 0: raise StopIteration
    elif dim == 0: 
        if 0 <= norm2: yield np.array([], dtype=int)
    else:
        c0_max = int(math.sqrt(norm2))
        if 0 <= comp_max: 
            c0_max = int(min(c0_max, comp_max))
        for c0 in range(1+c0_max):
            for v in std_int_vectors_maxnorm(dim - 1, norm2 - c0 * c0, c0):
                vv = np.r_[v, [c0]]
                if norm2_min <= (vv**2).sum() :
                    yield vv

def int_vectors_maxnorm(dim, norm2, comp_max=-1):
    """ vectors forming a "shpere", \sum_i c_i^2==norm2;
        * if comp_max>=0, additionally c_i <= comp_max
    """
    if norm2 < 0 or dim < 0: raise StopIteration
    elif dim == 0: 
        if 0 <= norm2: yield []
    else:
        c0_max = int(math.sqrt(norm2))
        if 0 <= comp_max: 
            c0_max = min(c0_max, comp_max)
        for c0 in range(-c0_max, 1+c0_max) :
            for v in int_vectors_maxnorm(dim - 1, norm2 - c0 * c0, comp_max):
                yield [c0] + v

def mom2_cont(latsize, mom): 
    return ((2*math.pi * np.asarray(mom, dtype=float) / latsize)**2).sum(-1)
def mom2_sine(latsize, mom): 
    return 4*(np.sin(math.pi * np.asarray(mom, dtype=float) / latsize)**2).sum(-1)

def list2str(l, fmt='%s', beg='[', end=']', sep=', '):
    if 0 == len(l) : return '[]'
    s = beg + (fmt % l[0])
    for x in l[1:]: s += sep + (fmt % x)
    return s + end

def gen_mom_cubic(dim, lx, lt, mom2_max):
    mom_t = 0
    mom_list = []
    while True:
        mom2t = mom2_cont([lt], [mom_t]) 
        if mom2_max < mom2t: break
        mom2x_max = mom2_max - mom2t
        for v in std_int_vectors_maxnorm(dim - 1, mom2x_max * (lx / 2 / math.pi)**2):
            yield v + [mom_t]
        mom_t += 1

def print_mom_cubic(dim, lx, lt, mom2_max, fo=sys.stdout, filt=None):
    ls = [lx] * (dim-1) + [lt]
    mom     = []
    mom_sq  = []
    if None is filt: filt = ()
    elif isinstance(filt, types.FunctionType): filt = (filt,)
    else: # check that it is iterable
        if not hasattr(filt, '__iter__'):
            raise ValueError('filt must be a (list of) function(s)')
        
    for v in gen_mom_cubic(dim, lx, lt, mom2_max):
        if not all([ f(dim, lx, lt, v) for f in filt ]) : continue
        mom.append(v)
        mom_sq.append(mom2_cont(ls, v))

    ord         = np.argsort(mom_sq)
    uniq_i      = -1
    mom_sq_prev = -1
    mom_sq_tol  = 1e-6
    for i in range(len(mom)):
        if mom_sq_tol < math.fabs(mom_sq[ord[i]] - mom_sq_prev):
            uniq_i      += 1
            mom_sq_prev = mom_sq[ord[i]]
        fo.write("%d\t%d\t%7.5f\t%7.5f\t# %s\t%s\t%f\t%s\n" % (i, uniq_i,
                mom2_cont(ls, mom[ord[i]]), mom2_sine(ls, mom[ord[i]]),
                list2str(mom[ord[i]], '%2d'),
                list2str(mom[ord[i]] / np.asarray(ls, dtype=float), '%5.3f'),
                ((mom[ord[i]] / np.asarray(ls, dtype=float))**4).sum(),
                str([ f(dim, lx, lt, mom[ord[i]]) for f in filt ]) ))

def mk_filter_diag(maxdiff=2*math.pi):
    """ make filter: return True if all components k_i,j 
        |k_i - k_j| < maxdiff 
    """
    def f(dim, lx, lt, v):
        assert (len(v) == dim)
        k = 2*math.pi * np.asarray(v, dtype=float) / ([lx] * (dim-1) + [lt])
        for i in range(1,len(v)):
            for j in range(i):
                if maxdiff < abs(k[i] - k[j]): return False
        return True
    return f

def filter_nonzero(dim, lx, lt, v):
    return all([ vi != 0 for vi in v ])

def mk_filter_close_q2(q2, maxdiff, if_sine=False):
    if if_sine: calc_mom = mom2_sine
    else: calc_mom = mom2_cont

    def f_single(dim, lx, lt, v): 
        return abs(calc_mom([lx]*(dim-1)+[lt], v) - q2) <= maxdiff
    def f_list(dim, lx, lt, v):
        return any([abs(calc_mom([lx]*(dim-1)+[lt], v) - x) <= maxdiff 
                    for x in q2])

    if hasattr(q2, '__iter__'): return f_list
    else: return f_single


def list_nonzero(v): return all([ vi != 0 for vi in v])

def mk_filter_mom_std_aniso(subidx):
    """ make filter that checks that subspace momentum is standard, i.e. 
        components are in descending order
    """
    s   = zip(subidx[:-1], subidx[1:])
    return lambda v: all([ v[i1] >= v[i2] for i1, i2 in s ])

def int_vectors_aniso_nsqrange(dim, ls, a, nsq1, nsq2):
    """ anisotropic lattice momentum vectors forming a "shell", 
        nsq1 <= \sum_i (2\pi n_i / (a_i L_i))^2 <= nsq2
    """
    if nsq2 < 0 or dim < 0 or nsq2 < nsq1: raise StopIteration
    elif dim == 0: 
        if nsq1 <= 0 and 0 <= nsq2: yield []
    else:
        k0  = 2. * math.pi / ls[0] / a[0]
        
        if 1 == dim and 0 <= nsq1: nmin = int(math.ceil(math.sqrt(nsq1) / k0))
        else: nmin = 0

        if 0 <= nsq2: nmax = int(math.floor(math.sqrt(nsq2) / k0))
        else: nmax = 0
        if ls[0]/2 < nmax: nmax = ls[0] / 2

        for n in range(nmin, nmax + 1):
            d_nsq   = (n * k0) ** 2
            for v in int_vectors_aniso_nsqrange(dim - 1, ls[1:], a[1:], 
                                                nsq1 - d_nsq, nsq2 - d_nsq):
                yield [ n ] + v

def find_hcsym_groups(ls, a):
    """ find hyperplanes with hypercubic symmetry 
        ls  lattice size
        a   lattice spacing
        return: [ (L, a, [dim1, dim2, ...]), ... ]
    """
    dim = len(ls)
    assert len(a) == dim
    sym_groups = [] # [ (l, a, list_of_indices) ]
    for i in range(dim):
        sg_found = False
        for j in range(len(sym_groups)):
            if sym_groups[j][0:2] == (ls[i], a[i]):
                sym_groups[j][2].append(i)
                sg_found = True
                break
        if not sg_found: sym_groups.append(tuple([ls[i], a[i], [ i ]]))
    return sym_groups

def gen_stdmom_aniso_nsqrange(ls, a, nsq1, nsq2):
    """ anisotropic lattice momentum vectors forming a "shell", 
        nsq1 <= \sum_i (2\pi n_i / (a_i L_i))^2 <= nsq2
        momentum vectors are in standard form
    """
    res = filter(list_nonzero, int_vectors_aniso_nsqrange(dim, ls, a, nsq1, nsq2))
    for sg in find_hcsym_groups(ls, a):
        res = filter(mk_filter_mom_std_aniso(sg[2]), res)
    return res

def gen_npr_mom(ls, a, q, search_mult=200):
    """ generate and select npr momenta in a range around q
        ls              lattice size (int)
        a               [GeV^-1] lattice spacing
        q               approximage momentum [GeV]
        search_mult
    """
    dim = len(ls)
    assert len(a) == dim
    k0  = 2. * math.pi / (np.r_[ls] * np.r_[a])

    # integer volume will be qmax ** dim * vol
    vol = math.pi ** (dim / 2) / sp.special.gamma(dim / 2 + 1) / k0.prod()


    de  = search_mult * 2**dim / (dim * vol * q**dim)
    #print("*** ", q, de, dim * vol * q**dim)
    q2min, q2max = q*q * (1 - de), q*q*(1 + de)
    res = filter(list_nonzero, int_vectors_aniso_nsqrange(dim, ls, a, q2min, q2max))
    for sg in find_hcsym_groups(ls, a):
        res = filter(mk_filter_mom_std_aniso(sg[2]), res)

    np_res = np.asarray(res)

    return res

def gen_npr_mom_list(ls, a, q, search_mult=200, print_max=3):
    """ generate and select a number of npr momenta in [qmin,qmax] range
        order by l4 value, select a number of most-diagonal and least-diagonal
        ls              lattice size (int)
        a               [GeV^-1] lattice spacing
        q               approximage momentum [GeV]
        search_mult
    """
    ls  = np.r_[ls]
    a   = np.r_[a]
    ka0 = 2. * math.pi / ls
    k0  = 2. * math.pi / (ls * a)
    #for q in np.exp(np.r_[math.log(qmin) : math.log(qmax) : n*1j]):
    p = np.asarray(gen_npr_mom(ls, a, q, search_mult=search_mult))
    p2= ((p * k0) ** 2).sum(1)
    p2sin = ((np.sin(p * ka0) / a)**2).sum(1)
    p4= ((p * ka0) ** 4).sum(1)
    i_p4_sort = p4.argsort()
    print("*** ", q)
    for i_min in range(print_max):
        i = i_p4_sort[i_min]
        print("%f\t%e\t%f\t%s\t%s" % (
                    math.sqrt(p2[i]), math.sqrt(p2[i]) / q - 1, 
                    p4[i], str(p[i]), str(p[i]*ka0)))
    for i_max in range(print_max):
        i = i_p4_sort[-i_max-1]
        print("%f\t%e\t%f\t%s\t%s" % (
                    math.sqrt(p2[i]), math.sqrt(p2[i]) / q - 1, 
                    p4[i], str(p[i]), str(p[i]*ka0)))

def make_vec_triangle(a, b) :
    """ (a,b,c) : a+b+c=0 """
    return np.concatenate(
            (   a[...,None,:], 
                b[...,None,:], 
                (-a -b)[...,None,:] ),
            axis=-2)

# set of vector pairs at pi/3 angle
# [i_mom, q0/q1/q2, mu]
def make_vec_triangle_2x_list(xyzt_iter, sign_iter) :
    vec_ab_list = np.concatenate([
        [   [ np.r_[ a*x, b*y,   0,  0], np.r_[ a*x,   0, c*z,   0] ],
            [ np.r_[ a*x, b*y,   0,  0], np.r_[ a*x,   0,   0, c*t] ],
            [ np.r_[ a*x,   0, b*z,  0], np.r_[ a*x,   0,   0, c*t] ],
            [ np.r_[   0, a*y, b*z,  0], np.r_[   0, a*y,   0, c*t] ], ] 
        for (x,y,z,t), (a,b,c,d) in it.product(xyzt_iter, sign_iter) ])
    res = np_uniq(make_vec_triangle(vec_ab_list[:,0], -vec_ab_list[:,1]))
    print("# make_vec_triangle_2x_list: vec_ab_list.shape=%s  res.shape=%s" % (
            str(vec_ab_list.shape), str(res.shape)))
    return res

def make_vec_triangle_4x_list(xyzt_iter, sign_iter) :
    vec_ab_list = np.concatenate([ 
        [   [ np.r_[ a*x, b*y, c*z, d*t], np.r_[ a*x, b*y, c*z,-d*t] ],
            [ np.r_[ a*x, b*y, c*z, d*t], np.r_[ a*x, b*y,-c*z, d*t] ],
            [ np.r_[ a*x, b*y, c*z, d*t], np.r_[ a*x,-b*y, c*z, d*t] ],
            [ np.r_[ a*x, b*y, c*z, d*t], np.r_[-a*x, b*y, c*z, d*t] ], ]
        for (x,y,z,t), (a,b,c,d) in it.product(xyzt_iter, sign_iter) ])
    res = np_uniq(make_vec_triangle(vec_ab_list[:,0], -vec_ab_list[:,1]))
    print("# make_vec_triangle_4x_list: vec_ab_list.shape=%s  res.shape=%s" % (
            str(vec_ab_list.shape), str(res.shape)))
    return res


##############################################################################
# from npr_pt.py
##############################################################################
def inv_wavevec(wv, ls, tw) :
    """ transformation (roughly) preserving momsq """
    wv = np.asarray(wv)
    return np.array(np.round(-wv - 2*tw), wv.dtype)

def cmp_wavevec(x, y) :
    return cmp(tuple(x), tuple(y))

def std_inv_wavevec(wv, ls, tw) :
    """ make wave vector "standard" wrt (Nd)-dim inversions
        std == the first non-zero component is >0
        transformation preserving momsq """
    if cmp_wavevec(wv, (0,)*len(wv)) < 0 :
        return inv_wavevec(wv, ls, tw)
    else :
        return wv
def std_rot3d_wavevec(wv, ls, tw) :
    """ make wave vector "standard" wrt (Nd-1)-dim rotations and (Nd)-dim reflections
        std == all components >=0, first 3 are ordered 
        transformation preserving momsq """
    wv = np.asarray(wv)
    res = np.array(np.maximum(wv, inv_wavevec(wv, ls, tw)), wv.dtype)
    res[:-1].sort()
    return res
    
def make_mom_std(p) : 
    p = np.abs(p) 
    p.sort()
    return p
