from future.utils import iteritems
from past.builtins import basestring
import os, errno, sys
import itertools as it
#from pprint import pformat
import math
import numpy as np
import h5py

def mkpath(path):
    try: os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path): pass
        else: raise

def read_key_list(list_name, title=None):
    res = [ s.rstrip('\n') for s in open(list_name).readlines() ]
    title   = title or ""
    print("'%s' : read list from '%s' = ('%s' ... '%s') [%d]" % (
            title, list_name, res[0], res[-1], len(res)))
    return res

def save_config(config_vars=None, config_file=None, gl_scope=None):
    if None is gl_scope: gl_scope = sys._getframe(1).f_globals
    if None is config_file: 
        if 'config_file' in gl_scope: config_file = config_file_
        else: raise RuntimeError('GLOBAL "config_file_" not defined')
    if None is config_vars: 
        if 'config_vars_' in gl_scope: config_vars = config_vars_
        else: raise RuntimeError('GLOBAL "config_vars_" not defined')
    for v in config_vars:
        if not v in gl_scope:
            raise RuntimeError('GLOBAL "%s" not defined' % v)
    
    def save_var(stream, var_name, var_val=None):
        if None is var_val: 
            if var_name in gl_scope: var_val = gl_scope[var_name]
            else: raise RuntimeError('GLOBAL "%s" not defined' % var_name)
        fo.write(var_name + '=' + pformat(var_val) + '\n')
    
    fo = open(config_file, 'w')
    fo.write('# vim: syn=python\n')
    save_var(fo, 'config_file_', config_file)
    save_var(fo, 'config_vars_', config_vars)
    for v in config_vars: save_var(fo, v)
    fo.close()

def list_del(l, pos_l) :
    """ return list l with pos_l positions deleted, 
        while other elements remain in their relative order """
    m = object()    # marker for positions to delete
    res = list(l)
    for p in pos_l : 
        res[p] = m
    return [ x for x in res if x != m ]


def list_place(a, ij_list) :
    """ return list a with some elements moved to specific positions,
        while the rest remain in their relative order
        a       iterable
        ij_list = [(out_pos, in_pos), ...] 
            out_pos, in_pos may be negative, cannot refer to the same position twice
    """
    a       = list(a)           # a may be an iterator
    n       = len(a)
    out     = [object()] * n    # make uniq object to catch bugs
    pos_in  = set(range(n))
    pos_out = set(range(n))
    for p_out, p_in in ij_list :
        if p_out < 0 : p_out += n
        pos_out.remove(p_out)
        if p_in  < 0 : p_in  += n
        pos_in.remove(p_in)
        out[p_out] = a[p_in]
    for p_out, p_in in zip(sorted(pos_out), sorted(pos_in)) :
        out[p_out] = a[p_in]
    return out

def list_insert(a, ib_list) :
    """ insert new elements into list a at specific positions, 
        while the original list members remain in their relative order
        a           any iterable
        ib_list     [ (out_pos, b), ... ]
            out_pos may be negative, cannot refer to the same position twice
    """
    a       = list(a)           # may be an iterator
    ib_list = list(ib_list)     # may be an iterator
    n       = len(a)
    m       = len(ib_list)
    out     = [object()] * (n + m)
    pos_out = set(range(n + m))
    for p_out, b in ib_list :
        if p_out < 0 : p_out += n + m
        pos_out.remove(p_out)
        out[p_out] = b
    for p_out, x in zip(pos_out, a) :
        out[p_out] = x
    return out

def list_del_dup(l_v) :
    i = 0
    while i < len(l_v) :
        if l_v[i] in l_v[:i] : del l_v[i]
        else : i += 1
    return l_v
def list_cat1(*ll) :
    """ concat lists along axis 1 """
    llen= len(ll[0])
    res = [ [] for i in range(llen) ]
    for l in ll :
        assert(len(l) == llen)
        for i, a in enumerate(l) :
            res[i].extend(a)
    return res
# TODO add dict_cat1

def kvmap(klist, keys, default=None)  :  return np.array([ keys.index() ])
def kvslice(klist, keys, vals): return [ vals[keys.index(k1)] for k1 in k ]
def kvget(k, keys, vals, default=None):
    try : return vals[ keys.index(k) ]
    except ValueError : return default

def make_nested_dict(key_lists, func_leaf=None) :
    """ leaf res[k0][k1]...[kN] will be a result of func_leaf or {} """
    if 0 < len(key_lists) :
        res = {}
        for k in key_lists[0] :
            res[k] = make_nested_dict(key_lists[1:], func_leaf=func_leaf)
        return res
    else :
        if None is func_leaf : return {}
        else : return func_leaf()

# update dict(k->list) from dict(k->val)
def dictlist_update(dlist, d, check_keys=True) :
    if 0 == len(dlist.keys()) : 
        for k in d.keys() : dlist[k] = []
    if check_keys :    
        for k in dlist.keys() :
            if not k in d :    
                raise KeyError(k)
    for k, v in iteritems(d) :
        if check_keys :    
            if not k in dlist :
                raise KeyError(k)
        dlist.setdefault(k, []).append(v)
def dictnew(d, **d_up) :
    res = dict(d)
    for k, v in iteritems(d_up) :
        res[k] = v
    return res
# a version of prev. function for non-string keys
def dictnew_kw(d, d_up) :
    res = dict(d)
    for k, v in iteritems(d_up) :
        res[k] = v
    return res
dict_copy=dictnew
def dict_get(d, *kk) : return [ d[k] for k in kk ]

# "deep" update; make sure there are no ref.loops...
def dictnewd(d, **d_up) :
    res = dict(d)
    for k, v in iteritems(d_up) :
        if not isinstance(v, dict) : res[k] = v
        else : res[k] = dictnewd(d.get(k, {}), **v)
    return res


def purge_keys(d, lk) :
    if isinstance(lk, basestring) : 
        if lk in d : del d[lk]
    elif isinstance(lk, list) or isinstance(lk, tuple) :
        for k in lk : 
            if k in d : del d[k]


def h5_ls(h5g, flags='', prefix_=''):
    def h5_lsobj(h5gk) :
        if   isinstance(h5gk, h5py.Group)   : what = 'Group'
        elif isinstance(h5gk, h5py.Datatype): what = 'Datatype'
        elif isinstance(h5gk, h5py.Dataset) : 
            what = 'Dataset {%s}' % ', '.join([str(d) for d in h5gk.shape])
        else : what = '???'
        print('%s\t%s' % (h5gk.name, what))
        
    recur = 0 < flags.count('r')
    if isinstance(h5g, h5py.Group) : 
        h5_lsobj(h5g)
        klist = h5g.keys()
        klist.sort()
        for k in klist : 
            if recur : h5_ls(h5g[k], flags=flags, 
                             prefix_='%s/%s' % (prefix_, k))
    else : h5_lsobj(h5g)


###############################################################################
# utilities for generating data in config files
###############################################################################
#def make_src_grid(n_src, latsize, x0, dx_c, i_c) :
    #n_src   = np.asarray(n_src)
    #latsize = np.asarray(latsize)
    #x0      = np.asarray(x0)
    #dx_c    = np.asarray(dx_c)
    #
    #dx_g    = latsize / n_src
    #res     = []
    #for ii in np.ndindex(*n_src) :
        #ii_a = np.asarray(ii)
        #res.append( ( x0 + dx_c*i_c + dx_g * ii) % latsize )
    #return np.asarray(res)

def make_src_grid(nsrc, ls, x0, dc, cindex, axis, dx_it=0) :
    nsrc= np.asarray(nsrc)
    x0  = np.asarray(x0)
    dc  = np.asarray(dc)
    ls  = np.asarray(ls)
    x1  = x0 + dc * cindex
    dx_g= ls // nsrc
    return np.array([ (x1 + dx_g * k + dx_it * k[axis]) % ls 
                      for k in np.ndindex(*nsrc) ])
#def  make_srcgrp_grid(n_src, latsize, x0, dx_c, i_c, dx_it)
def  make_srcgrp_grid(nsrc, ls, x0, dc, cindex, axis, ncoh, dx_it=0) :
    ncoh_grp    = nsrc[axis] // ncoh
    assert(ncoh_grp * ncoh == nsrc[axis])
    nsrc        = np.asarray(nsrc)
    x0          = np.asarray(x0)
    dc          = np.asarray(dc)
    ls          = np.asarray(ls)
    x1          = x0 + dc * cindex
    dx_g        = ls // nsrc
    dx_g_t      = np.zeros_like(dx_g)
    dx_g_t[axis]= dx_g[axis]
    dx_it       = np.asarray(dx_it)

    ndim        = len(ls)
    nsrc1       = np.array(nsrc)
    nsrc1[axis] = 1
    csrcgrp0    = [ (x1 + dx_g * k + dx_it * k[axis]) % ls
                    for k in np.ndindex(*nsrc1) ]
    csrcgrp     = [ [ (x + (dx_g_t + dx_it) * (i_coh_grp + ncoh_grp * i_coh)) % ls
                      for i_coh in range(ncoh) ] 
                    for i_coh_grp in range(ncoh_grp) 
                    for x in csrcgrp0 ]

    #ak          = np.ones((ndim,), int)
    #ak[axis]    = ncoh_grp
    #dk          = np.zeros((ndim,), int)
    #res         = []
    #for i_ncoh_grp in range(ncoh_grp) :
        #dk[axis]    = i_ncoh_grp
        #res.append([ (x1 + dx_g * (dk+ak*k) + dx_it * (dk+ak*k)[axis]) % ls
                     #for k in np.ndindex(*nsrc1) ])
    return np.array(csrcgrp)


def range_prod(r_list, filter_func=None) :
    if None is filter_func : filter_func = lambda x : True
    res = np.array([ x for x in it.product(*r_list) if filter_func(x) ] )
    return res
def range_prod_basis(r_list, basis_vecs, filter_func=None) :
    """ note that basis_vecs are indexed as [i_vec, i_comp], like in Qlua
        this order is TRANSPOSED compared to the usual "basis" matrix made of column-vecs """
    return np.dot(range_prod(r_list, filter_func=filter_func), basis_vecs)

def stat_str(a, fmt='%f') :
    """ print stats of an array avg{min:med:max} """
    a   = np.asarray(a)
    a.sort()
    return ('%s(%s/%s/%s)' % ((fmt,)*4)) % (
        np.mean(a), np.min(a), np.median(a), np.max(a))

# TODO change to iterator?
def dict_prod(dconst, **d) :
    k_l, vl_l = [], []
    for k,vl in iteritems(d) :
        k_l.append(k)
        vl_l.append(vl)
    res = []
    for v_l in it.product(*vl_l) :
        d = dict(dconst) # copy of dconst)
        d.update(zip(k_l, v_l))
        res.append(d)
    return res

def dict_prod_auto(**d) :
    dconst  = {}
    dlist   = {}
    for k, v in iteritems(d) :
        if isinstance(v, list) : dlist[k] = v
        else : dconst[k] = v
    return dict_prod(dconst, **dlist)

def dict_copy_pop(d, kl) :
    """ make a copy of dict d with keys in kl removed, returned as elems of tuple 
        return : d1.pop([kl]), tuple(d[kl])
    """
    d1  = dict(d)
    res = []
    for k in kl : res.append(d1.pop(k))
    return d1, tuple(res)

def first_not_none(*l) :
    for k in l :
        if not None is k : return k
    return None

take1st = first_not_none # alias

def dict_copy_keys(dst, src, klist) :
    for k in klist:
        if k in src : dst[k] = src[k]
    
dictnew_pop = dict_copy_pop     # TODO replace everywhere dict_copy_pop --> dictnew_pop

def jsonify(v) :
    if isinstance(v, np.ndarray) : return v.tolist()
    elif isinstance(v, dict) : return dict([ (k, jsonify(a)) for k,a in iteritems(v) ])
    elif isinstance(v, set) : return list(v)
    # TODO add other cases
    else : return v

def strip_prefix(s, p):
    if s.startswith(p) : return s[len(p):]
    else : return None

# !!! XXX DEATH TO UNICODE XXX !!!
def afmt(*p) :
    p = tuple([ (a.encode('ascii') if isinstance(a, str) else a) for a in p ])
    if 0 == len(p) : return None
    elif 1 == len(p) : return p[0]
    else : return p[0] % (p[1:])
