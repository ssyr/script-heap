import numpy
import numpy as np
import math

    

def dot_diag(a,b):
    if a.ndim == 1:
        if b.ndim < 2: raise ValueError('2nd arg is not a matrix')
        return (a * b.swapaxes(b.ndim-1, b.ndim-2)).swapaxes(b.ndim-1,b.ndim-2)
    elif b.ndim == 1:
        if a.ndim < 2: raise ValueError('1st arg is not a matrix')
        return a * b
    else: raise ValueError('none of args is a matrix')

def norm_cov_matrix(a):
    if (a.ndim != 2 or a.shape[1] != a.shape[0]): raise ValueError('arg is not a square matrix')
    n = a.shape[0]
    sigma = numpy.zeros(shape=(n,), dtype=numpy.float64)
    for i in range(n):
        s = a[i,i]
        if s <= 0.: raise ValueError('matrix has non-positive diag item')
        s = math.sqrt(s)
        sigma[i] = s
        for j in range(n):
            a[i,j] /= s
            a[j,i] /= s
    return sigma

def recip_cov_matrix(a, ev_thr=1e-10):
    if (a.ndim != 2 or a.shape[1] != a.shape[0]): raise ValueError('arg is not a square matrix')
    n       = a.shape[0]
    ac      = a.copy()
    sigma   = norm_cov_matrix(ac)
    d, v    = numpy.linalg.eig(ac)
    invd    = numpy.zeros(shape=d.shape, dtype=numpy.float64)
    invsigma = numpy.zeros(shape=d.shape, dtype=numpy.float64)
    for i in range(n):
        assert 0 < sigma[i]
        invsigma[i] = 1. / sigma[i]
        if (d[i] <= 0. or d[i] < ev_thr): raise ValueError(d[i], 'bad eigenvalue')
        invd[i] = 1. / d[i]
    v = dot_diag(v, invsigma)
    return numpy.dot(v, dot_diag(invd, v.transpose()))

def cov_matrix_shrink(ens):
    n       = len(ens)
    assert 1 < n
    avg     = reduce(lambda x,y: x + y, ens, 0) / n
    assert 1 == len(avg.shape)
    dim     = avg.shape[0]
    cov     = numpy.zeros(shape=(dim,dim), dtype=numpy.float64)
    cov2    = numpy.zeros(shape=(dim,dim), dtype=numpy.float64)
    for y in ens:
        yc    = y - avg
        yyt   = numpy.outer(yc,yc)
        cov  += yyt
        cov2 += yyt**2
    cov    /= n - 1
    sigma   = numpy.array([ math.sqrt(cov[i,i]) for i in range(dim)])
    cov    /= sigma.reshape((1,dim)) * sigma.reshape((dim,1))
    sigma2  = sigma * sigma
    cov2   /= sigma2.reshape((1,dim)) * sigma2.reshape((dim,1)) * (n - 1.)**2 / n
    cov2   -= cov**2
    for i in range(dim): cov2[i,i] = 0.
    var_cov_sum = cov2.sum() / (n - 1)
    cov2_sum = (cov**2).sum() - dim
    lm = var_cov_sum / cov2_sum
    assert 0. <= lm
    if 1. < lm: sc = 0.
    else: sc = 1. - lm
    for i in range(dim):
        for j in range(i):
            cov[i,j] *= sc
            cov[j,i] *= sc
    return (lm, sigma / math.sqrt(n), cov)

def cov_matrix_reg_shrink(a, shrval) :
    """ interpolate between full and diag matrix """
    assert(0. <= shrval and shrval <= 1.)
    return (1. - shrval) * a + shrval * np.diag(np.diag(a))

def ncov_matrix_set_evmin(ncov, ncov_evmin) :
    """ ensure that the smallest eigenvalue of the normcov matrix is >=evmin """
    ncov_ev = np.linalg.eigvalsh(0.5*(ncov + ncov.T))
    ncov_dim= ncov.shape[0]
    a   = ncov_ev.min()
    ap  = ncov_evmin
    if ap <= a : return ncov
    else :
        b   = (ap - a) / (1. - a)
        return (1. - b ) * ncov + b * np.eye(ncov_dim)

