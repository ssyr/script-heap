import math
import os

class progbar:
    def __init__(self, total=0, start=0, prefix='', bar_len=50, bar_sym='=', stream=None):
        import sys
        self.cnt    = start
        self.total  = total
        self.prefix = prefix
        self.bar_len= bar_len
        self.bar_sym= bar_sym
        if 0 < total: 
            self.frac   = float(start) / total
            self.bar_str= self.make_bar_str(self.frac)
        else: 
            self.frac   = 0.
            self.bar_str= 'total?'
        if not None is  stream: self.stream = stream
        else: self.stream = sys.stdout
    def make_bar_str(self, frac):
        prog_len    = int(self.bar_len * frac + .5)
        if self.bar_len < prog_len: prog_len = self.bar_len
        return ''.ljust(prog_len, self.bar_sym) + ''.ljust(self.bar_len - prog_len, ' ')
    def make_string(self):
        if 0 < self.total: tot_str = '%d' % self.total
        else: tot_str = '<total?>'
        cnt_tot_str = ('%d/%s' % (self.cnt, tot_str)).rjust( 2 * len(tot_str) + 2)
        res_str = '%s%s %3d%% [%s]' % (self.prefix, cnt_tot_str,
            int(100 * self.frac + .5), self.bar_str)
        if 0 < self.total and self.total < self.cnt: res_str += ' OVER'
        return res_str
    def draw(self):
        self.stream.write(self.make_string() + '\n')
        self.stream.flush()
    def redraw(self):
        self.stream.write('\r' + self.make_string().ljust(79, ' '))
        self.stream.flush()
    def finish(self): self.stream.write('\n')
    def step(self, n=1):
        self.cnt    += n
        if 0 < self.total: 
            frac   = float(self.cnt) / self.total
            if int(self.bar_len * self.frac + .5) < int(self.bar_len * frac + .5):
                self.bar_str = self.make_bar_str(frac)
            self.frac = frac
"""       
def test_progbar(total=0, start=0, prefix='', bar_len=50, bar_sym='=', stream=None, 
            steps=0, sleep=.1):
    import time
    pb=progbar(total=total, start=start, prefix=prefix, bar_len=bar_len, 
                bar_sym=bar_sym, stream=stream)
    if not 0 <= sleep: sleep = .1
    if not 0 < steps: steps = total
    for n in range(steps):
        pb.step()
        pb.redraw()
        time.sleep(sleep)
"""

def timer(str1, VERBOSE=True):
    u1, s1, cu1, cs1, t1    = os.times()
    def f(str2) :
        u2, s2, cu2, cs2, t2    = os.times()
        if VERBOSE :
            print('TIME[%10.3fsec TOT  %10.3fsec USR   %10.3fsec SYS] ("%s", "%s")' % (
                    t2 - t1, 
                    u2 - u1 + cu2 - cu1,
                    s2 - s1 + cs2 - cs1,
                    str1, str2))
    return f
