from __future__ import print_function
import math
import sys
import numpy as np
import itertools as it
from ..misc import estimate #import calc_normabsdev, norm_cov_matrix
from . import numpy_extra as npe
from .. import h5_io

def spprint_ve_0(val, err, errmax=100, precmax=8):
    """ DEPRECATED """
    def exp10(pwr):
        abspwr = abs(pwr)
        res = 1
        a = 10
        while 0 < abspwr:
            if abspwr % 2 == 1: res *= a
            abspwr //= 2
            a *= a
        if (0 <= pwr): return res
        else: return 1. / res
    val_norm, pwr_val = 0,0
    if np.isnan(val).any() or np.isnan(err).any() : return 'nan'
    assert 0 <= err
    if val != 0:
        pwr_val = int(math.floor(math.log10(abs(val))))
        val_norm= val * exp10(-pwr_val)
        if err != 0:
            pwr_err = int(math.ceil(math.log10(float(err) / errmax)))
            err_norm= int(err * exp10(-pwr_err))
            prec_val= pwr_val - pwr_err
            if prec_val < 0:
                err_norm *= exp10(-prec_val)
                prec_val = 0
            elif precmax < prec_val:
                err_norm=int(err_norm * exp10(precmax - prec_val))
                prec_val = precmax
        else:
            err_norm = 0
            prec_val = precmax
    else:
        val_norm = 0
        if err != 0:
            pwr_err = int(math.ceil(math.log10(float(err) / errmax)))
            err_norm= int(err * exp10(-pwr_err))
            pwr_val = pwr_err
            prec_val= 0
        else: return '0(0)'
    return ('%%.%df(%%d)e%%d' % prec_val) % (val_norm, err_norm, pwr_val)

def spprint_ve_v1(val, err, errmax=100, precmax=8, mode='plain'):
    val_norm, pwr_val = 0,0
    err     = np.abs(np.asarray(err, dtype=float))
    if np.isnan(val).any() or np.isnan(err).any() : return 'nan'
    assert (0 <= err).all()
    if val != 0:
        pwr_val = int(math.floor(math.log10(abs(val))))
        val_norm= val * 10**(-pwr_val)
        if (0 != err).any() :
            pwr_err = int(np.ceil(np.log10(err / errmax)).max())
            err_norm= np.round(err * 10.**(-pwr_err))
            prec_val= pwr_val - pwr_err
            if prec_val < 0:
                err_norm *= 10**(-prec_val)
                prec_val = 0
            elif precmax < prec_val:
                err_norm=np.round(err_norm * 10.**(precmax - prec_val))
                prec_val = precmax
        else:
            err_norm = np.zeros(err.shape, dtype=int)
            prec_val = precmax
    else:
        val_norm = 0
        if (0 != err).any() :
            pwr_err = int(np.ceil(np.log10(float(err) / errmax)).max())
            err_norm= np.round(err * 10.**(-pwr_err))
            pwr_val = pwr_err
            prec_val= 0
        else: return '0(0)'
    if not isinstance(err_norm, np.ndarray) :  err_norm_str = '(%d)' % err_norm
    else : err_norm_str = ''.join(['(%d)' % v for v in err_norm ])
    if   'plain' == mode :
        res = ('%%.%df%%se%%d' % prec_val) % (val_norm, err_norm_str, pwr_val)
    elif 'latex' == mode :
        res = ('%%.%df%%s10^{%%d}' % prec_val) % (val_norm, err_norm_str, pwr_val)
    else : raise ValueError(mode)
    return res 

def spprint_ve(val, err, errmax=99, precmax=8, pwrmin=-2, pwrmax=4, mode='plain'):
    """ err may be a list
        [pwrmin ; pwrmax] no-exp10 range
        latex output
        errors have period
    """         
    val_norm, pwr_val = 0,0
    err     = np.abs(np.asarray(err, dtype=float)).reshape(-1)
    if np.isnan(val).any() or np.isnan(err).any() : return 'nan'
    assert (0 <= err).all()
    if val != 0:
        pwr_val = int(math.floor(math.log10(abs(val))))
        if pwrmin <= pwr_val and pwr_val <= pwrmax : pwr_val = 0
        val_norm= val * 10**(-pwr_val)
        if (0 != err).any() : 
            pwr_err     = int(np.ceil(np.log10(err / errmax)).max())
            err_norm    = np.round(err * 10.**(-pwr_err))
            prec_val    = pwr_val - pwr_err
            if prec_val < 0:
                err_norm    *= 10**(-prec_val)
                prec_val    = 0
            elif precmax < prec_val:
                err_norm    = np.round(err_norm * 10.**(precmax - prec_val))
                prec_val    = precmax
        else:
            err_norm    = np.zeros(err.shape, dtype=int)
            prec_val    = precmax
    else:
        val_norm = 0
        if (0 != err).any() :
            pwr_err     = int(np.ceil(np.log10(float(err) / errmax)).max())
            err_norm    = np.round(err * 10.**(-pwr_err))
            pwr_val     = pwr_err
            prec_val    = 0
        else: return '0(0)'
    mant_fmt    = '%%.%df' % prec_val
    mant_s      = mant_fmt % val_norm  
    err_th      = 10 ** prec_val
    err_s       = ''.join([ '(%d)' % e if e < err_th else ('(' + (mant_fmt % (e / err_th)) + ')')  for e in err_norm ])
    if 0 == pwr_val : exp10_s = ''
    else : 
        if   'plain' == mode : exp10_s = 'e%d' % pwr_val
        elif 'latex' == mode : exp10_s = '\cdot10^{%d}' % pwr_val
        else : raise ValueError(mode)
    return "%s%s%s" % (mant_s, err_s, exp10_s)


def pprint_ve_ncov(val, err, ncov, title=None, fo=sys.stdout, prefix=''):
    """ (pretty)print values, errors, normalized covariance matrix
    """
    n_v     = len(val)
    val     = np.asarray(val)
    err     = np.asarray(err)
    if not None is  ncov: ncov    = np.asarray(ncov)
    assert ((title is None or len(title)==n_v) and
            (err.shape==(n_v,)) and
            (None is ncov or ncov.shape==(n_v,n_v)))
    for i, v in enumerate(val):
        e = err[i]
        val_str = spprint_ve(v, e, errmax=316)
        if None is title : title_i = ''
        else : title_i = title[i]
        if None is ncov : ncov_str = ''
        else : ncov_str = '\t'.join([ '%+.3f' % c for c in ncov[i, :i] ])
        fo.write("%s%-15s\t%-23s\t%s\n" % (
                prefix, title_i, val_str, ncov_str))

def pprint_ve_cov(val, cov, title=None, fo=sys.stdout, print_cov=True, prefix=''):
    """ (pretty)print values, errors, normalized covariance matrix
    """
    assert (title is None or len(title)==len(val))
    ncov    = np.asarray(cov).copy()
    err     = lhpd.norm_cov_matrix(ncov)
    if not print_cov: ncov = None
    pprint_ve_ncov(val, err, ncov, title=title, fo=fo, prefix=prefix)

def pprint_ve(val, err, title=None, fo=sys.stdout, print_cov=True, prefix=''):
    """ (pretty)print values, errors, normalized covariance matrix
    """
    assert (title is None or len(title)==len(val))
    pprint_ve_ncov(val, err, None, title=title, fo=fo, prefix=prefix)


def pprint_avg_std_ncov(fo, title, avg, std, ncov, errmax=100, precmax=8):
    """
    DEPRECATED
    """

    if fo is None:
        import sys
        fo = sys.stdout
    assert (title is None or len(title)==len(avg))
    for i in range(len(avg)):
        val_err_str = spprint_ve(avg[i], std[i], errmax, precmax)
        if (not title is None): fo.write('%-15s\t' % title[i])
        fo.write('%-31s' % val_err_str)
        if not ncov is None:
            for j in range(i): 
                fo.write('\t%f' % ncov[i,j])
        fo.write('\t1.')
        fo.write('\n')

def spprint_latex_tab(a) :
    return '\t\\\\\n'.join(['\t&\t'.join(l) for l in a])


def pprint_cov_stat(cov, title=None, short=True, fo=sys.stdout) :
    sigma = np.sqrt(cov.diagonal())
    ncov = cov / sigma / sigma[:, None]
    ncov_eval, ncov_evec = np.linalg.eigh(ncov)
    if not None is title : title = '%s ' % title
    else : title = ''
    ncov_eval_isort = np.argsort(ncov_eval)
    if short :
        vmin, vmax = ncov_eval[ncov_eval_isort[ [0, -1] ]]
        fo.write("%s EVals=[%.3e .. %.3e]  cond=%.3e\n" % (title, vmin, vmax, vmax/vmin))
    else : fo.write("%s EVals=[%s]\n" % (title, ', '.join([ '%.3e' % v for v in ncov_eval[ncov_eval_isort] ])))

def pprint_svd_stat(mat, title=None, short=True, fo=sys.stdout) :
    mat_svd = np.linalg.svd(mat, compute_uv=False)
    if not None is title : title = '%s ' % title
    else : title = ''
    mat_svd_isort = np.argsort(mat_svd)
    
    if short : 
        vmin, vmax = mat_svd[mat_svd_isort[ [0, -1] ]]
        fo.write("%s SVals=[%.3e .. %.3e]  cond=%.3e\n" % (title, vmin, vmax, vmax/vmin))
    else : fo.write("%s SVals=[%s]\n" % (title, ', '.join([ '%.3e' % v for v in mat_svd[mat_svd_isort] ])))

def pprint_cov_stat_short(cov, title=None, fo=sys.stdout) :
    sigma = np.sqrt(cov.diagonal())
    ncov = cov / sigma / sigma[:, None]
    ncov_eval, ncov_evec = np.linalg.eigh(ncov)
    if not None is title : title = '%s ' % title
    else : title = ''
    ncov_eval_isort = np.argsort(ncov_eval)
    fo.write("%s ncov_eval=[%e ... %e]\n" % (title, 
        ncov_eval[ncov_eval_isort][0], ncov_eval[ncov_eval_isort][-1]))

def spprint_list(l, fmt='%s', sep=', ') :
    return sep.join([ fmt % x for x in l ])

def pprint_lists(*ll) :
    for i_k, l in enumerate(zip(*ll)) :
        print("\t".join([str(i_k)] + [str(k) for k in l]))

def np_print_nonzero(t, atol=0) :
    nz = np.array(np.where(atol < np.abs(t))).T
    for i, nz_i in enumerate(nz) :
        print(i, nz_i, t[tuple(nz_i)])

def spprint_eq(v, jstr, atol=0, fmt='%+f') :
    n = len(v)
    assert(len(jstr) == n)
    nzj = np.where(atol < np.abs(v))
    terms = []
    for j in nzj[0] :
        if isinstance(v[j], complex): cstr = "+%s" % str(v[j])
        else : cstr = fmt % v[j]
        terms.append("%s*%s" % (cstr, jstr[j]))
    return ' '.join(terms)

def pprint_eq(v, jstr, **kw) :
    print(spprint_eq(v, jstr, **kw))

def pprint_mateq(t, istr, jstr, **kw) :
    """ pretty-print matrix eqn 
            istr[i] =  +t[i,0]*jstr[0] +t[i,1]*jstr[1] +...
    """
    for i, v in enumerate(t) :
        print("%s = %s" % (istr[i], spprint_eq(v, jstr, **kw)))
        

def pprint_outlier(v, axis=0, tit=None, thresh=5*1.4826) :
    """ compute outlier statistics and print starting with sample having max outliers
        thresh     outlier criterion based on abs.dev/med.abs.dev
                   default is =5sigma for a normalized dist
    """
    sh      = v.shape
    n_data  = sh[axis]
    n_val   = v.size // n_data ; assert(v.size == n_val * n_data)

    # [i_data, i_val]
    v_nad   = estimate.calc_normabsdev(v, axis=axis)
    v_nad   = np.rollaxis(v_nad, axis).reshape(n_data, n_val)
    # [i_data]
    v_nad_avg = v_nad.mean(axis=1) # the avg outlier
    v_nad_max = v_nad.max(axis=1) # the worst outlier

    n_out   = np.where(v_nad < thresh, 0, 1)
    n_out_tot = n_out.sum(1)
    isort_nout= np.argsort(n_out_tot, axis=0)[::-1]
    # find the worst samples
    for i_i, i in enumerate(isort_nout) :
        if None is tit : istr = ("%d" % (i,))
        else : istr = ("%d\t%s" % (i, str(tit[i])))
        print("%s\t%d\t%f\t%f" % (
            istr, n_out_tot[i], v_nad_avg[i], v_nad_max[i]))

def spprint_examine_data(v) :
    n_tot   = v.size
    n_nan   = np.count_nonzero(np.isnan(v))
    n_inf   = np.count_nonzero(np.isinf(v))
    n_fin   = np.count_nonzero(np.isfinite(v))
    n_zero  = np.count_nonzero(0 == v)
    v1      = v.flatten()
    if np.iscomplexobj(v1) : v1 = npe.np_c2r(v1)
    v1.sort()
    dv1     = v1[1:] - v1[:-1]
    n_equ   = np.count_nonzero(0 == dv1)

    return "TOT=%d FIN=%d NAN=%d INF=%d ZERO=%d EQU=%d" % (
            n_tot, n_fin, n_nan, n_inf, n_zero, n_equ)



def pprint_bin_unbias(x_sl, dl_sl, x_ex, dl_ex, bin_by) :
    def bin_group(dl, bin_by) :
        bin_dl = {}
        for i_d, d in enumerate(dl) :
            bin_dl.setdefault(bin_by(d), []).append(i_d)
        keybin = bin_dl.keys()
        keybin.sort()
        keymap = [ bin_dl[k] for k in keybin ]
        return keybin, keymap

    assert(len(x_sl) == len(dl_sl))
    assert(len(x_ex) == len(dl_ex))
    kbin_sl, kmap_sl = bin_group(dl_sl, bin_by)
    kbin_ex, kmap_ex = bin_group(dl_ex, bin_by)
    assert(kbin_sl == kbin_ex)
    n_bins = len(kbin_sl)
    
    res = np.empty((n_bins,) + x_sl.shape[1:], np.result_type(x_sl, x_ex))
    for i_k, k in enumerate(kbin_sl) :
        il_sl = np.asarray(kmap_sl[i_k])
        il_ex = np.asarray(kmap_ex[i_k])
        m1, m2= h5_io.h5_meas_match(dl_sl[il_sl], dl_ex[il_ex])
        #print(len(m1), len(m2))
        assert((dl_sl[il_sl[m1]] == dl_ex[il_ex[m2]]).all())
        xi_sl = np.array(x_sl[il_sl])
        xi_bc = x_ex[il_ex[m2]] - x_sl[il_sl[m1]]
        res[i_k] = xi_sl.mean(0) + xi_bc.mean(0)

        x1_sl = np.array(xi_sl.reshape((len(xi_sl), -1)))
        if (np.iscomplexobj(x1_sl)) : x1_sl = npe.np_c2r(x1_sl)
        x1_sl_std= np.std(x1_sl, axis=0)

        x1_bc = np.array(xi_bc.reshape((len(xi_bc), -1)))
        if (np.iscomplexobj(x1_bc)) : x1_bc = npe.np_c2r(x1_bc)
        x1_bc_av = x1_bc.mean(0)
        bc2std   = np.abs(x1_bc_av) / x1_sl_std

        bc2std_min = bc2std.min()
        bc2std_med = np.median(bc2std)
        bc2std_avg = bc2std.mean()
        bc2std_max = bc2std.max()

        print("%d\t%s\t%.3f\t%.3f\t%.3f\t%.3f" %(
                i_k, str(k),
                bc2std_min, bc2std_med, bc2std_avg, bc2std_max))
        
    return res
