import sys
import time


class log :
    """ formatted multiplexed output with prefix and timestamp
        use-case : printing messages both to stdout and (one or more) files
        line-buffered -- required for prefixing
        verbosity level-dependent output : 
            echo(level, ...)            write line (add '\n' at end)
            lprintf(level, fmt, ...)    write formatted string

        on __del__, will flush buffer, flush all streams, close owned files
    """
    def __init__(self, level=-1, stdout=True, prefix=None, datefmt=None, header=None) :
        if datefmt : datefmt='%y%m%d_%H%M%S'
        self.datefmt    = datefmt
        self.prefix     = prefix
        self.header     = header
        self.level      = level
        self.lstreams   = []
        self.llevel     = []
        self.ldatefmt   = []
        self.lprefix    = []
        self.own_fd     = []            # streams that must be closed in __del__
        self.linbuf     = ''
        if stdout : self.add_stream(sys.stdout, level=level)

    def __del__(self) :
        if '' != self.linbuf : self.write('\n')        # flush self.linbuf
        for ls in self.lstreams : ls.flush()
        for i in self.own_fd : self.lstreams[i].close()

    def add_stream(self, f, mode='a', prefix=None, datefmt=None, level=None) :
        i = len(self.lstreams)
        if isinstance(f, str) :
            self.own_fd.append(i)
            f = open(f, mode)
        assert(f.write)
        self.lstreams.append(f)
        if None is level : level = self.level
        self.llevel.append(level)
        if None is datefmt : datefmt = self.datefmt
        elif datefmt : datefmt='%y%m%d_%H%M%S'
        self.ldatefmt.append(datefmt)
        if None is prefix : prefix = self.prefix
        self.lprefix.append(prefix)
        if not None is self.header : f.write("%s\n" % self.header)

    def get_date_str_(self, i) : 
        datefmt = self.ldatefmt[i]
        if None is datefmt or '' == datefmt : return ''
        else : return '%s' % (time.strftime(datefmt),)
    def get_prefix_str_(self, i) :
        prefix = self.lprefix[i]
        if None is prefix or '' == prefix : return ''
        else: return prefix

    def get_prefix(self, i) : 
        pstr = self.get_prefix_str_(i)
        dstr = self.get_date_str_(i)
        if '' == dstr : 
            if '' == pstr : return ''
            else : return '%s ' % pstr
        else : return '%s[%s] ' % (pstr, dstr)

    def write(self, s, level=0) :
        if not isinstance(s, str) : s = str(s)
        pp = [ self.get_prefix(i) for i in range(len(self.lstreams)) ]
        s_l     = s.split('\n')
        s_l[0]  = "%s%s" % (self.linbuf, s_l[0])
        self.linbuf = s_l[-1]
        for x in s_l[:-1] :
            for i, ls in enumerate(self.lstreams) :
                if self.llevel[i] < 0 or level <= self.llevel[i] :
                    s1 = "%s%s\n" % (pp[i], x)
                    ls.write(s1)
                    ls.flush()
    def writeln(self, s, level=0) : 
        self.write(s, level=level)
        self.write('\n', level=level)

    def printf(self, fmt, *a) :         self.write(fmt % a)
    def echo(self, level, s) :          self.write("%s\n" % str(s), level=level)
    def lprintf(self, level, fmt, *a) : self.echo(level, fmt % a)

