###############################################################################
# generate string primitives for filenames and keypaths
###############################################################################
def csrc_str_f(csrc) : return 'x%dy%dz%dt%d' % tuple(csrc)
def csrc_str_k(csrc) : return 'x%d_y%d_z%d_t%d' % tuple(csrc)

def csrcgrp_str_f(csrcgrp) : 
    return '_'.join([ csrc_str_f(c) for c in csrcgrp ])
csrcgrp_str_k = csrcgrp_str_f

def csrcgrp_str_k_v1(csrcgrp) :     # some old stuff 
    return '_'.join([ csrc_str_k(c) for c in csrcgrp ])

def psnk_str_f(csrc) : return 'PX%dPY%dPZ%d' % tuple(csrc)
def psnk_str_k(csrc) : return 'PX%d_PY%d_PZ%d' % tuple(csrc)

def snk_str_old_f(psnk, tsnk) : return "PX%dPY%dPZ%dT%d" % (tuple(psnk) + (tsnk,))
def snk_str_old_k(psnk, tsnk) : return "PX%d_PY%d_PZ%d_T%d" % (tuple(psnk) + (tsnk,))
def snk_str_f(psnk, tsnk) : return "PX%dPY%dPZ%ddt%d" % (tuple(psnk) + (tsnk,))
def snk_str_k(psnk, tsnk) : return "PX%d_PY%d_PZ%d_dt%d" % (tuple(psnk) + (tsnk,))

def qext_str_f(qext) : return "qx%dqy%dqz%d" % tuple(qext)
def qext_str_k(qext) : return "qx%d_qy%d_qz%d" % tuple(qext)

def lpath_str(lpath):
    """ xyztXYZT """
    return "l%d_%s" % (len(lpath), ''.join(["xyztXYZT"[c] for c in lpath]))
def lpath_simplify(lp_, bb_dim=4) :
    """ get linkpath in the list format [ {[0 : 2D)}, ... ] and 
        simplify it by removing adjacent opposite links
    """
    lp  = list(lp_)
    i   = 1
    while i < len(lp) :
        if ((2 * bb_dim - lp[i - 1] + lp[i]) % (2 * bb_dim) == bb_dim) : 
            del lp[i - 1 : i + 1]
            i   = max(1, i - 1)
        else : i += 1
    return lp

bb_linkpath_simplify = lpath_simplify # alias
