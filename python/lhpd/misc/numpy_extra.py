###############################################################################
# auxuliary functions for proper binning and matching of exact/apprx samples
# instead of performing search & comparison, it may be hard-coded 
# if the number of cfgs and samples per cfg are known and samples are ordered

# XXX note that sample_spec are composite ndarray types ; for comparison,
# any other types (e.g. tuples) should be cast to that ndarray type

###############################################################################
from past.builtins import cmp

import numpy as np
from ..misc import *
from ..pymath import tensor_s_
from ..pymath import H4_tensor

def np_norm_axis(rank, axis) :
    """ make axis positive """
    return (axis if 0 <=axis < 0 else rank + axis)
def np_norm_axes(rank, axes) :
    """ make axis positive """
    return tuple((axis if 0 <=axis else rank + axis) 
                 for axis in axes)

def np_at_shape(a, oth_shape, oth_at_) :
    """ set shape & transpose a such that it can be multiplied 
        with array shape 'oth_shape' and along that array axes 'oth_at' 
        (in order of 'a' axes)
        len(a.shape) <= len(oth_shape); max(oth_at) < len(oth_shape)
        result.shape[i] == {1 or oth_shape[at[i]]}
        NOTE that if len(a.shape) == len(oth_shape), it is equivalent 
        to a.transpose(oth_at.argsort)
        """
    a         = np.asarray(a)
    a_rank    = len(a.shape)
    oth_shape = tuple(oth_shape)
    oth_rank  = len(oth_shape)
    oth_at    = tuple(np_norm_axes(oth_rank, oth_at_))
    if (min(oth_at) < 0 or oth_rank <= max(oth_at)) : 
        raise ValueError("bad axis map %s" % str(oth_at_))
    if (a_rank != len(oth_at)) :
        raise ValueError("tensor rank does not match target shape")
    if (oth_rank < a_rank) :
        raise ValueError("tensor rank exceeds target shape")
    at_fill = oth_at + tuple(i for i in range(oth_rank) if not i in oth_at)
    assert(len(at_fill) == oth_rank)
    return a.reshape(a.shape + (1,) * (oth_rank - a_rank)).transpose(
                np.array(at_fill).argsort())

def np_at_slice(l, *at) :
    """ return slice or sub-element in one or several chosen direction """
    sl  = [slice(None)] * l
    for i, s in at : sl[i] = s
    return tuple(sl)
def np_at(a, *at) : return a[np_at_slice(len(a.shape), *at)]

def np_array_hash(latmom012, tol=1e-4) : 
    """ "hash"-function for equivalent lattice momentum: 
        * sorted abs. values of all components
        * rounded at tol, scaled+converted to int
        * tuple
    """
    lm = np.array(np.abs(latmom012.flatten()) / tol + .5, int)
    lm.sort()
    return tuple(lm[::-1])

def np_group_keys(klist) :
    """ 
        return [k0,k1,...], [[i_k0_0, i_k0_1,...], [i_k1_0,i_k1_1,...], ...]
        (stable in klist)
    """
    d = {}
    k_srt = []
    for i_k, k in enumerate(klist) : 
        if k in d : d[k].append(i_k)
        else :
            d[k] = [i_k]
            k_srt.append(k)    
    return k_srt, [ d[k] for k in k_srt ]

def np_mean_groups(a, axis, axis_grp) :
    nd      = len(a.shape)
    res_sh  = list(a.shape)
    res_sh[axis] = len(axis_grp)
    res     = np.empty(res_sh, a.dtype)
    for i, g in enumerate(axis_grp) :
        res[np_at_slice(nd, (axis, i))] = a[np_at_slice(nd, (axis, g))].mean(axis)
    return res

def np_sum_groups(a, axis, axis_grp) :
    nd      = len(a.shape)
    res_sh  = list(a.shape)
    res_sh[axis] = len(axis_grp)
    res     = np.empty(res_sh, a.dtype)
    for i, g in enumerate(axis_grp) :
        res[np_at_slice(nd, (axis, i))] = a[np_at_slice(nd, (axis, g))].sum(axis)
    return res


# search for numpy
def np_find_all(val, arr) :
    """ return list of (all of) i : arr[i] = val 
        i is a tuple of arrays as returned by np.nonzero
    """
    arr     = np.asarray(arr)
    val     = np.asarray(val, arr.dtype)
    val_dim = len(val.shape)
    if 0 == val_dim : 
        # pad shapes by last x1 dimension
        arr = arr[..., None]
        val = val[..., None]
        val_dim = len(val.shape)
    arr_dim = len(arr.shape)
    ndidx_dim = arr_dim - val_dim
    assert (arr.shape[ndidx_dim:] == val.shape)
    return (arr.reshape(arr.shape[:ndidx_dim] + (-1,)) 
                == val.reshape(-1)).all(axis=-1).nonzero()

def np_count(val, err) :
    """ return the number of times val is contained in err """
    return len(np_find_all(val, err)[0])

def np_find_first(val, arr) :
    """ return ndindex of the first occurrence of val in err 
        raise ValueError if not found
        NOTE : returns a tuple that can be used to index np.array
    """
    r = np_find_all(val, arr)
    if 0 < len(r[0]) : return tuple( i[0] for i in r )
    else : raise ValueError("not in the list", val)

def np_find_all_list(val, arr) :
    """ return {i[k]} : arr[i[k]] = val[k]
        val scalar : equivalent to np_find_first
            1D arr : treat as a list of scalars to search
            >=2D   : 0-th axis is a list of arrays
    """
    val     = np.asarray(val, arr.dtype)
    val_dim = len(val.shape)
    if 0 == val_dim : 
        return np_find_first(val, err)
    elif 1 == val_dim : 
        # scalars -> x1 arrays, [1][i_list]
        val = val[None, ...] 
        arr = arr[..., None]
        val_dim = len(val.shape)
    else : 
        # move the list dim of val to the end: [..., i_list]
        val = np.rollaxis(val, 0, val_dim)  
    val_len = val.shape[-1]

    arr     = np.asarray(arr)
    arr_dim = len(arr.shape)
    ndidx_dim = arr_dim - val_dim + 1
    assert (arr.shape[ndidx_dim:] == val.shape[:-1])

    # (1) arr.shape is padded for the list dim of val
    # (2) arr and val are flattened in the irrelevant dimensions
    # (3) [ndindex, i_list] -> if_match
    r = ( arr.reshape(arr.shape[:ndidx_dim] + (-1, 1)) 
                == val.reshape((-1, val_len)) ).all(axis=-2)
    return [ r[..., i].nonzero() for i in range(val_len) ]

def np_count_list(val, arr) :
    """ return the numbers of occurrences of val[i] in arr for each val[i] """
    return [ len(v[0]) for v in np_find_all_list(val, arr) ]

def np_find_first_list(val, arr) :
    """ for each val[i], return ndindex of the first occurrence of val[i] in err 
        raise ValueError if not found
        NOTE : returns a list of tuples; tuples that can be used to index np.array
        NOTE : it is incorrect to call arr[RETURN]; instead, to achieve the same effect,
               transform list of tuples into tuple of lists (not always possible for d>=2 ndindices, 
               hence not implemented)
    """
    r = np_find_all_list(val, arr)
    res = []
    for i_v, v in enumerate(r) :
        if 0 < len(v[0]) : res.append(tuple(i[0] for i in v))
        else : raise ValueError("not in the list", val[i_v])
    return res

def np_index_first_list(val, arr) :
    """ return list of indices suitable for 1-axis slicing """
    assert(1 == len(arr.shape) or val[0].shape == arr.shape[1:])
    r = np_find_first_list(val, arr)
    return [ r1[0] for r1 in r ]


# search for numpy with tol
def np_find_all_tol(val, arr, rtol=1e-05, atol=1e-08, equal_nan=False) :
    """ return list of (all of) i : arr[i] = val 
        i is a tuple of arrays as returned by np.nonzero
    """
    arr     = np.asarray(arr)
    val     = np.asarray(val, arr.dtype)
    val_dim = len(val.shape)
    if 0 == val_dim : 
        # pad shapes by last x1 dimension
        arr = arr[..., None]
        val = val[..., None]
        val_dim = len(val.shape)
    arr_sh  = arr.shape
    arr_dim = len(arr_sh)
    ndidx_dim = arr_dim - val_dim
    assert (arr.shape[ndidx_dim:] == val.shape)
    return np.where(np.isclose(arr, val, rtol=rtol, atol=atol, equal_nan=equal_nan
                ).reshape(arr_sh[:ndidx_dim] + (-1,)).all(axis=-1))
def np_count_tol(val, err, rtol=1e-05, atol=1e-08, equal_nan=False) :
    """ return the number of times val is contained in err """
    return len(np_find_all_tol(val, err, rtol=rtol, atol=atol, equal_nan=equal_nan)[0])

def np_find_first_tol(val, arr, rtol=1e-05, atol=1e-08, equal_nan=False) :
    """ return ndindex of the first occurrence of val in err 
        raise ValueError if not found
        NOTE : returns a tuple that can be used to index np.array
    """
    r   = np_find_all_tol(val, arr, rtol=rtol, atol=atol, equal_nan=equal_nan)
    if 0 < len(r[0]) : 
        return tuple( i[0] for i in r )
    else : raise ValueError("not in the list", val)



def np_match(dl1, dl2, hashfunc = None, eqfunc=None) :
    """ find intersection and create maps m1,m2 between dl1,dl2 such that
        dl1[m1] == dl2[m2]
        the resulting map m1 is ordered for dl1
        
        primarily intended for data lists; np_find_first_list will do 
        effectively the same (up to transforming to np.array and reshaping)
    """
    if None is hashfunc :   hashfunc = (lambda v : v)
    if None is eqfunc :     eqfunc = (lambda a,b : (a==b))
    # iterate over dl1 ; swap if dl2 < dl1
    if len(dl2) < len(dl1) :
        do_sw   = True
        dl1, dl2 = dl2, dl1
    else : do_sw   = False
    # hash values in dl2
    hash2 = dict([ (hashfunc(v2), i_v2) for i_v2,v2 in enumerate(dl2) ])
    m1, m2 = [], []
    for i_v1,v1 in enumerate(dl1) :
        i_v2    = hash2.get(hashfunc(v1))
        if not None is i_v2 and eqfunc(dl1[i_v1], dl2[i_v2]) :
            m1.append(i_v1)
            m2.append(i_v2)
            
    m1, m2 = np.array(m1), np.array(m2)
    if do_sw : 
        s = m2.argsort()
        return m2[s], m1[s]
    else : return (m1, m2)

""" 
#tests for the above functions
x=np.random.rand(3,4,5,6) ; x[0,1] = x[1,3] ; x[1,1]=x[2,3] ; x[1,2]=x[2,3]; # example for the search
np_find_all(x[0,1], x)
np_count(x[0,1], x)
np_find_all_list([ x[0,0], x[0,1], x[1,1], ], x)
np_count_list([x[1,2],x[0,1],x[2,3]], x)
np_find_first_list([x[1,2],x[0,1],x[2,3]], x)

"""

# FIXME there are probably better NumPy alternatives
def np_subindex(sub_arr, arr) :
    """ return ndarray i : arr[i] = sub_arr """
    l_arr = list(arr)
    return np.r_[ [ l_arr.index(s) for s in sub_arr ] ]


def np_filter_arg(condition, arr) :
    """ return ndarray i : condition(arr[i]) == True """
    return np.r_[ filter(lambda i : condition(arr[i]), range(len(arr))) ]

def np_filter(condition, arr) :
    """ equivalent of filter(list) for an 1D numpy array """
    return np.array(filter(condition, arr), dtype=arr.dtype)

def np_make_diag(a, axis=-1, axis1=-1, axis2=-2, out=None) :
    """ make a diagonal matrix in the last two indices using values along [axis] : 
        e.g. for axis=-1, a[..., i] -> aa[..., i, i]
    """
    a   = np.asarray(a)
    assert (axis1 != axis2)
    len_diag = a.shape[axis]
    out_shape = list_insert(list_del(a.shape, [axis]),
                            [(axis1, len_diag), (axis2, len_diag)])
    if None is out : 
        out = np.zeros(out_shape, a.dtype)
    else :
        out.fill(0)

    for i in range(len_diag) :
        out[tensor_s_(len(out.shape), [i, i], [axis1, axis2])] = a[tensor_s_(len(a.shape), i, axis)]

    return out


def np_ddot(*m) :
    if len(m) <= 0 : return 1
    elif 1 == len(m) : return m[0]
    res = m[0]
    for x in m[1:] : 
        res = np.dot(res, x)
    return res

def np_lex_cmp_tol(a, b, rtol=1e-05, atol=1e-08, equal_nan=False) :
    """ lexicographic comparison of Real arrays a, b 
        rtol, atol, equal_nan are for numpy.isclose """
    a   = np.asarray(a).flat
    b   = np.asarray(b).flat
    idx = np.isclose(a, b, rtol=rtol, atol=atol, equal_nan=equal_nan)
    if idx.all() : return 0
    else :
        idx_ne = np.where(idx == False)[0][0]
        return cmp(a[idx_ne], b[idx_ne])


def np_vec_cmp(a, b) :
    """ Complex vector comparison : norm, cosangle, phase; 
        tail of a.shape == b.shape
        compute "rho"=ridff(|a|, |b|), (\hat{a}*\hat{b}) = "cos" * e^{i * pi/2 * "phi") 
        -1<=cos<=+1 may be pos/neg, -1<phi<=+1"""
    a   = np.asarray(a)
    b   = np.asarray(b)
    sh_a= a.shape
    sh_b= b.shape
    sh_r= sh_a[:-len(sh_b)]          # <result>.shape
    assert (sh_a == sh_r + sh_b)
    sh_x= sh_r + (np.prod(sh_b),)   # for reshape+sum

    n_a = np.sqrt((a * a.conj()).reshape(sh_x).real.sum(-1))
    n_b = np.sqrt((b * b.conj()).real.sum())
    if n_b == 0. : return 0.,0.,0.
    n_a = np.where(0. == n_a, -1., n_a)

    x   = (a * b.conj()).reshape(sh_x).sum(-1) / n_a / n_b
    c   = np.abs(x)
    c   = np.where(x.real < 0, -c, c) 
    x   = np.where(x.real < 0, -x, x)

    phi = np.where(np.abs(c) < 1e-8, 0., np.arctan2(x.imag, x.real))
    
    if () == sh_r :
        c   = np.asscalar(c)
        phi = np.asscalar(phi)

    return n_a / n_b, c, phi

def vec_cmp(a, b) :
    """ Complex vector comparison : norm, cosangle, phase
        compute "rho"=ridff(|a|, |b|), (\hat{a}*\hat{b}) = "cos" * e^{i * pi/2 * "phi") 
        -1<=cos<=+1 may be pos/neg, -1<phi<=+1"""
    a   = np.asarray(a)
    b   = np.asarray(b)
    n_a = math.sqrt((a * a.conj()).real.sum())
    n_b = math.sqrt((b * b.conj()).real.sum())
    if n_a == 0. and n_b == 0. : return 0., 0., 0.
    if n_a == 0. : return 0., 0., 0.
    if n_b == 0. : return -1., 0., 0.   # should be inf, but don't want to mess up averages
    x   = (a*b.conj()).sum() / n_a / n_b
    s   = np.sign(x.real)
    c   = np.abs(x)
    if   c < 1e-8   : phi = 0.
    elif 0 == s     : phi = 1. # set to max (fully imag)
    else :
        x   = x * s
        c   = c * s
        phi = math.atan2(x.imag, x.real) * 2 / math.pi
    return (n_a / n_b), c, phi
cplx_vec_cmp = vec_cmp

def rdiff(a, b):
    a = np.asarray(a)
    b = np.asarray(b)
    if 0 == np.abs(a).sum():
        if 0 == np.abs(b).sum(): return 0
        else: return 1.
    else:
        if 0 == np.abs(a).sum(): return 1.
        else: return float(np.abs(a - b).sum()) / (np.abs(a).sum() + np.abs(b).sum())

#def np_mesh_coord(ls) :
    #""" return [mu, ix,iy,iz,it] = c[mu] """
    #dim = len(ls)
    #return np.array(np.meshgrid(*tuple(np.r_[:lx] for lx in ls), indexing='ij')).reshape(di,-1).T
#
#def np_mesh_delta(ls, csrc=None) :
    #""" return [mu, ix,iy,iz,it] = (c-csrc)[mu], min |c-csrc|"""
    #ls  = np.asarray(ls)
#
    #if None is csrc : csrc = np.zeros_like(ls)
    #else :            csrc  = np.asarray(csrc)
#
    #dx  = np_mesh_coord(ls)
    #for i in range(len(ls)) :
        #l_mu    = ls[i]
        #lh_mu   = l_mu // 2
        #dx[i]   = (dx[i] + (l_mu - csrc[i] + lh_mu)) % l_mu - lh_mu
    #return dx
    

def np_mesh_rsq(ls, csrc=None, axes=None, power=2) :
    ls  = np.asarray(ls)
    if None is axes : axes = range(len(ls))

    if None is csrc : csrc = np.zeros_like(ls)
    else :            csrc  = np.asarray(csrc)

    dx  = [ ((np.r_[:l_mu] - csrc[i] + 3*l_mu//2) % l_mu - l_mu//2)
            for i, l_mu in enumerate(ls) ]
    dx2 = [ c**power for c in dx ]
    # x2,y2,z2,t2=pc[0][:,0,0,0], pc[1][0,:,0,0], pc[2][0,0,:,0], pc[3][0,0,0,:]
    mdx2= np.meshgrid(*tuple(dx2), indexing='ij')

    s   = 0
    for i,a in enumerate(axes) :
        s = s + mdx2[a]
    return s

def np_mask_dist2(ls, csrc, r2cut, axes=None) :
    if None is axes : axes = range(len(ls))
    dr2 = np_mesh_rsq(ls, csrc=csrc, axes=axes)
    return np.where(dr2 <= r2cut, 1, 0)

def np_convn(d, K, axes=None) :
    """ save r2cut qtopo density to h5f:<h5k_top>/r2cut<r2cut> """
    d       = np.asarray(d)
    ls      = np.asarray(d.shape)
    if None is axes : axes = range(len(ls))
    ls_a    = ls[list(axes)]
    d_f     = np.fft.fftn(d, s=ls_a, axes=axes)
    K_f     = np.fft.fftn(K, s=ls_a, axes=axes)
    return np.fft.ifftn(K_f * d_f, s=ls_a, axes=axes)


def np_cast_rshape(a, rshape, pad=None) :
    """ resrict rightmost dims of a: for 1<=i<=min(len(a.shape), len(rshape))
            a.shape[-i] to rshape[-i], 1<=i<=min(len(a.shape),len(rshape))
        if pad=None, check that a[-i] >= rshape[-i]
        otherwise, pad values from pad (can be ndarray)
        FIXME need to support padding with None (produces nans)?
    """
    a_sh    = a.shape
    res_sh  = list(a_sh)
    sl      = [slice(None)] * len(a_sh)
    for i in range(1, 1 + min(len(a_sh), len(rshape))) :
        if (a_sh[-i] < rshape[-i]) and (None is pad) :
            raise ValueError((a.shape, rshape,))
        sl[-i]      = slice(None, min(a_sh[-i], rshape[-i]))
        res_sh[-i]  = rshape[-i]
    res     = np.empty(tuple(res_sh), dtype=a.dtype)
    if not None is pad : res[:]  = pad
    sl      = tuple(sl)
    res[sl] = a[sl]
    return res

# iterators over sorted lists
def np_equal_bound(l, cmp=cmp):
    """ iterator returning (lower, upper, val) : bounds [lower, upper) and value
        from a sorted list """
    ii  = iter(l)
    xn  = next(ii)     # StopIteration if empty
    iu  = 0
    stopiter_mark = object()
    while not xn is stopiter_mark :
        il, xc = iu, xn
        try :
            while 0 == cmp(xc, xn) :
                iu += 1
                xn = next(ii)
        except StopIteration : 
            xn = stopiter_mark
        yield (il, iu, xc)

def np_equal_range(l, cmp=cmp) :
    for il, iu, val in np_equal_bound(l, cmp=cmp) :
        yield il, iu

def pinv_complement(bmatr, sv_min_r=1e-8, sv_min_a=0) :
    """ return pseudo-inverse and complement 
        equation B.p=b <= p = pinv(B) + W'a
        where from SVD B=U.S.W^T, pinv(B) = W.pinv(S).U^T and W^T.W'=0
        bmatr     M,N matrix, M<=N
        sv_min_r  SV rel.cutoff, relative to max(SV)
        sv_min_a  SV abs.cutoff; default is 0 (no cutoff) because 
                  lattice data can vary many orders of mag.

        return pinv(B), W'
        pinv(B)  N,M matrix
        W'       N,(N-rank(B)) matrix, rank(B)<=M
        note: rank(B) is reduced by dropping singular values below sv_min_r
    """
    M, N = bmatr.shape
    bu, bs, bwT = np.linalg.svd(bmatr, full_matrices=True, compute_uv=True)
    # select sv's larger than sv_min_r
    bs_max = bs.max()
    if bs_max <= 0. : 
        return np.zeros((N, M), bmatr.dtype), np.identity(N, bmatr.dtype)
    bs_cut = np.max(bs_max * sv_min_r, sv_min_a)
    nc  = np.count_nonzero(bs_cut <= bs)   # last above cut

    bs_isort = np.argsort(bs)[ : -1-nc : -1]   # last nc, in desc.order
    bs  = bs[bs_isort]

    #print("bs=%s  bs_cut=%s  nc=%d" % (bs, bs_cut, nc))
    
    if nc < len(bs) :
        bs_isort = bs_isort[:nc]
        bs       = bs[:nc]
    icompl = list(set(range(N)) - set(bs_isort))
    icompl.sort()
    assert(len(icompl) == N - nc)
    bpinv   = np.dot(bu[:,bs_isort] / bs, bwT[bs_isort]).T
    wcompl  = bwT[icompl].T
    return bpinv, wcompl
    
def np_uniq(v_list) :
    """ select uniq vectors from array (v_list is supposed to be at least 1d)
        makes sense mostly for int arrays: select lists of lattice coord 
        or momenta without repetition
        NOTE: the original order is preserved
        NOTE: analogous function intvec_uniq in npr/gen_momenta.py
    """
    v_dict = dict([ (tuple(v.flat), i) for i,v in enumerate(v_list) ])
    return np.array([ v_list[i] for i in sorted(v_dict.values()) ])

def np_circshift_coord(f, csrc, bc=None) :
    """ shift res[x] <- f[(csrc+x)%lx] """
    csrc    = np.asarray(csrc)
    f       = np.asarray(f)
    dim     = len(f.shape)
    assert(len(csrc) == dim)
    if None is bc : bc = [ 1 ] * dim
    assert(len(bc) == dim)
    res = np.array(f)
    aux = np.empty_like(f)
    for d in range(dim) :
        lx  = res.shape[d]
        x0  = csrc[d]
        aux[np_at_slice(dim, (d, np.s_[: lx - x0]))] =         res[np_at_slice(dim, (d, np.s_[x0 :]))]
        aux[np_at_slice(dim, (d, np.s_[lx - x0 :]))] = bc[d] * res[np_at_slice(dim, (d, np.s_[: x0]))]
        aux, res = res, aux
    return res

def np_c2r(v, axis=None) : 
    """ convert real to complex, insert new axis before 'axis'
        axis    position before which insert the new re/im axis
                None means at end 
                negative: e.g. axis=-1 will result in re/im axis at -2 
                (same logic as in np.rollaxis)
    """
    assert(np.iscomplexobj(v))
    vsh = list(v.shape)
    if None is axis : 
        vsh.append(1)
        axis= -1
    else : 
        vsh.insert(axis, 1)
        if axis < 0 : axis = len(vsh) + axis - 1
        
    v1  = v.reshape(tuple(vsh))
    return np.r_['%d' % axis, v1.real, v1.imag]


def np_r2c(v, axis=None) : 
    """ convert real to complex, insert new axis at 'axis' """
    assert(not np.iscomplexobj(v))
    if None is axis : axis = -1
    assert(2 == v.shape[axis])
    s_r = np_at_slice(len(v.shape), (axis, 0))
    s_i = np_at_slice(len(v.shape), (axis, 1))
    return v[s_r] + 1j * v[s_i]


def np_linalg_qr_unnorm(t, atol=0) :
    """ unnormalized QR(Gram-Schmidt) decomp: T = Q' . R' 
        with diag(R')=1 and basis vector norms |Q[*,j]|=R[j,j]; 
        relation QR: T = Q.R = Q'.R' : R'=(1/diag[R])*R), Q'=Q*diag(R) """
    q, r    = np.linalg.qr(t)
    r_nrm   = np.diagonal(r)
    r_nrm1  = np.where(atol < np.abs(r_nrm), r_nrm, 1.)
    r_n1    = (1./r_nrm1[:, None]) * r
    q_n1    = q * r_nrm1
    return q_n1, r_n1

def np_linalg_rm_upcol(a, j, atol=0, inv=False) :
    """ transformation to remove column j above diag, assuming [jj] is nonzero:
        return: P_j such that for A'=P_j.A, A'[j,j]=1, A'[:j,j]=0
    """ 
    m, n = a.shape
    assert(m == n)
    ajj = a[j, j]
    
    assert(atol < np.abs(ajj))
    res = np.identity(n, a.dtype)
    if inv : 
        res[:j, j] = a[:j, j]
        res[j, j]  = ajj
    else:
        res[:j, j] = -a[:j, j] / ajj
        res[j, j]  = 1. / ajj
    
    return res

def np_linalg_linspan(q, r, atol=0) :
    """ modify QR(Gram-Schmidt) decomp to select the first lin.indep vectors 
        without modification, and represent the others as their linear combinations
            Q.R = A = Q'.R',
            Q'[*,j] = A[
        for each vector Q'[*,j] linear-independent from vectors Q'[*,i<j], 
            Q'[*,j] = A[*,j]
            R'[i,j]=delta(i,j)   [ i.e., R'[j,j]=1, R'[i!=j,j]=0]
        for each vector Q'[*,j] linear composition of vectors Q'[*,i<j],
            R'[j:,j]=0          [ i.e., R'[i,j]=0 for i >=j ] 
        return : Q', R'
        
        NOTE convenient for classifying linear-dependent vectors and selecting basis:
        first-encountered linear-independent vectors in A=Q.R are preserved in Q',
        and the rest are saved in R as their linear combination """
    n = q.shape[1]
    assert((n, n) == r.shape)
    q1 = np.array(q) # copy
    r1 = np.array(r) # copy
    for j in range(n) : 
        rjj = r[j, j]
        if (np.abs(rjj) <= atol) : # lin.combination of prev.vectors (jvec<j)
            continue 
        else :
            Pj = np_linalg_rm_upcol(r1, j, atol=atol)
            Pj_inv = np_linalg_rm_upcol(r1, j, atol=atol, inv=True)
            r1 = np.dot(Pj, r1)
            q1 = np.dot(q1, Pj_inv)
    return q1, r1

def np_stdvec(vec) : return H4_tensor.H4_stdvec(vec)

def np_mean_weighed(*a) :
    n   = len(a)
    ae2 = [ 1. / a[i].var(axis=0) for i in range(n) ]
    return sum([ a[i] * ae2[i] for i in range(n) ]) / sum(ae2)

def np_outer_proj(v) :
    """ compute v*v^T / v^2 """
    return v[..., None, :] * v[..., :, None] / (v**2).sum(-1)[..., None, None]

