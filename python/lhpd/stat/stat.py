import numpy as np

def cdf_xrange(v) :
    """ make data range for CDF plotting: each data point is doubled """
    v   = np.array(v)
    v.sort(0)
    return np.r_[ [v, v] ].T.flatten()

def cdf_yrange(n, normed=True) :
    """ make count range for CDF plotting """

    x   = np.r_[ 0 : 1. - 1./n : n * 1j]
    x1  = x + 1./n
    if not normed :
        x   *= n
        x1  *= n
    return np.r_[ [x, x1] ].T.flatten()

def kstest_dataset_func(v, cdf_func) :
    v   = np.array(v)
    assert(1 == len(v.shape))
    n   = v.shape[0]
    v.sort()
    cdf_v = np.r_[ 0 : 1. - 1./n : n * 1j]
    cdf_i = cdf_func(v)
    return np.abs(np.r_[ cdf_v - cdf_i, cdf_v + 1./n - cdf_i]).max()


