import numpy as np

def shrink_stein(n, v):
    """ Stein eigenvalue shrinkage estimator
        n       sample size
        v       vector of eigenvalues
    """
    v   = np.asarray(v)
    p   = len(v)
    assert v.shape == (p,)
    vmv_inv = (1. / (np.identity(p) + v[:, None] - v[None, :]) - np.identity(p)).sum(1)


def shrink_haff(n, v, thresh=1e-9):
    """ Haff shrinkage
        n       sample size
        v       vector of eigenvalues
    """
    raise NotImplementedError
    v   = np.asarray(v)
    p   = len(v)
    assert v.shape == (p,)
    
    # s[i] := \sum_{k=0..i} 1/fStein[k], or zero if ill-det
    s   = np.zeros((p,), v.dtype)   
    # i-th domain of degeneracy of v : [ k_{i-1}: k_i ), k_{-1} = 0
    dk  = []    
    for i in range(1, p):
        assert v[i] <= v[i-1]
        if thresh * (v[i-1] + v[i]) < 2*(v[i-1] - v[i]) :    # different e.v.
            dk.append(i)                                    # have new domain [ ... : i )
    dk.append(p)                                            # last domain [ ... : p )
    print(dk)                                               # DEBUG
    
    # calculate domains
    dom     = np.empty((p,), int)
    prev_k  = 0
    for ik, k in enumerate(dk): 
        dom[prev_k: k] = ik
        prev_k = k
    print(dom)

    # sum of inverse differences of v[i]: := \sum_{j=0..i} \sum_{m=i+1...p-1} 1/(l_j - l_m)
    idv     = np.zeros((p,p), v.dtype)
    for i in range(p):
        for j in range(i+1, p):
            if dom[i] != dom[j]: idv[i, j] = 1. / (v[i] - v[j])
    for i in range(1, p):
        idv[i] += idv[i-1]
    idv_i   = np.empty((p,), v.dtype)
    for i in reversed(range(1, p)):
        idv[:,i-1] += idv[:,i]
        idv_i[i-1] = idv[i-1, i]
    idv_i[-1] = 0.

    #if True: #debug
    if 0: 
        sum = 0.
        for i in range(p):
            for j in range(p):
                if thresh * (v[i] + v[j]) < 2 * abs(v[i] - v[j]):
                    sum += 1. / (v[i] - v[j])
            print(i, idv_i[i], sum, idv_i[i] - sum)
    
    
    # sum of inv e.v. : iv[i] = \sum
    iv = 1. / v
    for i in range(1, p):
        iv[i] += iv[i-1]

    vh  = np.zeros((p,), v.dtype)
    for ik, k in enumerate(dk):
        #k = 
        pass
        
import scipy as sp
def chi2_pvalue(ndof, chi2) :
    return 1 - sp.stats.chi2(ndof).cdf(chi2)
