import numpy as np
def gen_ens_mvar(nsamp, xav, xerr, xcov=None) :
    n   = len(xav)
    assert(len(xerr) == n)

    xi  = np.random.normal(size=(nsamp, n))
    if not None is xcov :
        assert(xcov.shape == (n, n))
        # sic! need to mult the [1] axis of xi
        xi  = np.dot(xi, np.linalg.cholesky(xcov).T)       # xcov = chol(xcov).chol(xcov)^T

    return xav + xerr * xi

