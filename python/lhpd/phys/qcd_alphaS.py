# second attempt to get alphaS right, following 0908.1135 Bethke:2009 EPJC
import numpy as np
import scipy as sp
import itertools as it
import math

# following Bethke, the equation is
# Q^2 d\alpha_S / dQ^2 = \beta(\alpha_S) = - \sum_{i=0}^{Nloop-1} \beta_i \alpha_S^{i+2}
def betafunc_series_v0(nf=None, nc=3) :
    """ get coefficients for beta (\\beta_0>0) """
    assert(3 == nc) # TODO implement other options
    assert(0 <= nf)
    return np.r_[
        (33. - 2. * nf) / (12. * math.pi),
        (153. - 19. * nf) / (24 * math.pi**2),
        (77139. - 15099. * nf + 325. * nf**2) / (3456. * math.pi**3),
        (29243. - 6946.3 * nf + 405.089 * nf**2 + 1.49931 * nf**3) / (256 * math.pi**4)
    ]

def betafunc_series(nf=None, nc=3) :
    """ coeffs b0,b1,b2 from PDG2020, b3 from hep-ph/9701390, b4 from 1606.08659
        get coefficients for beta (\\beta_0>0) """
    assert(0 <= nf)
    CA  = nc
    CF  = (nc*nc - 1.)/(2. * nc)
    TR  = .5
    z3  = 1.202056903159594
    b0  = (11. * CA - 4. * nf * TR) / (12. * math.pi)
    b1  = (17. * CA**2 - nf * TR * (10. * CA + 6 * CF)) / (24. * math.pi**2)
    b2  = (2857. - 5033./9. * nf + 325. / 27. * nf**2) / (128. * math.pi**3)
    assert(3 == nc) # TODO implement other options
    b3_nc3 = (  
           149753./6. + 3564.*z3 
         - (1078361./162. + 6508./27*z3) * nf 
         + (50065./162 + 6472./81.*z3) * nf**2
         + 1093./729. * nf**3) / (256. * math.pi**4)
    return np.r_[ b0, b1, b2, b3_nc3 ]

def alphaS_match(alphaS, muR, mH, mass_msbar=True, inv=False) :
    """ match alphaS from nf to nf+1 
        inv=False : alphaS = coupling at Nf ; return coupling at (Nf+1)
        inv=True  : alphaS = coupling at (Nf+1) ; return coupling at Nf
    """
    lnm = math.log(muR/mH)
    c11 = 1. / (6. * math.pi)
    c10 = 0.
    c22 = c11**2
    c21 = 11. / (24. * math.pi**2)
    if mass_msbar : c20 = -11. / (72. * math.pi**2) # also req. mh===mh_msbar_mh=mu ???
    else : c20 = 7. / (24. * math.pi**2)
    a   = alphaS
    #return a * (1 + a * (c10 + c11 * lnm) + a**2 * (c20 + c21 * lnm + c22 * lnm**2))
    c1  = c10 + c11 * lnm
    c2  = c20 + c21 * lnm + c22 * lnm**2
    if inv : return a * (1 + a * (-c1 + a * (2*c1*c1 - c2)))
    else   : return a * (1 + a * ( c1 + a *  c2))

def betafunc_integrate(series, alpha0, mu0, mu1, logstep=1e-4) :
    """ integrate betafunc to find alphaS as a function of mu
        using series for d alphaS / d log(mu^2)
        NOTE: series are expected give [-d/d(log(mu^2)) alphaS] : the minus and the square!)
    """
    lseries = len(series)
    lmu0    = math.log(mu0)
    lmu1    = math.log(mu1)
    a_pows  = 2 + np.r_[:lseries]

    nstep   = 1 + max(1, int(abs(lmu1 - lmu0) / logstep))
    dlmu    = (lmu1 - lmu0) / (nstep - 1)
    exp_dlmu= math.exp(dlmu)


    alpha_r = np.empty((nstep,), np.float)
    mu_r    = np.empty((nstep,), np.float)
    alpha_r[0], mu_r[0] = alpha0, mu0
    for i in range(nstep-1) :
        mu_r[1+i]   = mu_r[i] * exp_dlmu
        # Euler's scheme
        alpha_r[1+i]= alpha_r[i] + dlmu * (-2. * (series * alpha_r[i] ** a_pows).sum())
    return mu_r, alpha_r

def alphaS_running(alphaS0, mu0, mu1, nf, nc) : 
    return betafunc_integrate(betafunc_series(nf=nf, nc=nc), alphaS0, mu0, mu1)[1][-1]


# rgflow: use consistent conventions: alphaS as expn. parameter, t=log\mu as scale var

def series_func(s, n0, nloop=None) :
    """
        n0       lowest power of the series (multiplying s[0]
        nloop    cutoff expansion at nloop terms
    """
    s   = np.array(s)
    if not None is nloop : s = s[:nloop]
    pwr = np.r_[n0 : n0 + len(s)]
    return lambda a : (s * a**pwr).sum()

def rgflow_qcd_betaseries(nf, nc) :
    return -2. * betafunc_series(nf, nc)
def rgflow_qcd_betafunc(nf, nc, nloop=None) :
    return series_func(rgflow_qcd_betaseries(nf, nc), 2, nloop=nloop)

def rgflow_mat(l0, l1, alpha0, betafunc, gammafunc, z0=None, lstep=1e-4) :
    """ leapfrog-integrate over l=log(mu) from l0 to l1 using equations
            dalpha/dl = betafunc(alpha)
            dZ_{ij}/dl = gammafunc(alpha)_{ik} . Z_{kj}
        the anom.dim. is defined so that
            O^R_i(mu) = Z_{ij}(mu) O^{bare}_j,
            (d/dl)O^R_i = dZ_{ik}/dl Z^{-1}_{kj} O^R_j = gammafunc_{ij}(alpha) O^R_j
        NOTE conventions are chosen to be straightforward, which is inconsistent with most papers...
        l0,l1           start and end of integration
        betafunc(alpha)     (vector of) coupling running \beta(alpha) 
        gammafunc(alpha)    (matrix of) anom.dim \gamma(alpha)_{ij}
        NOTE make sure that normalization and sign are correct for QCD:
        for a=alphaS, mu[GeV], nf=5
            betafunc(alpha, nf) = -(33 - 2 nf) / (6*pi)) alpha**2 - ...
            betafunc(alpha, 5) ~= -1.2201879 * alpha^2 -0.48971905 * alpha^3 - ...
    """
    nstep   = max(1, int(abs(l1 - l0) / lstep)) # l[0] = l0, l[nstep] = l1 = l0 + nstep*dl
    dl      = (l1 - l0) / nstep
    alpha1  = alpha0
    alphactr= alpha0 + (0.5 * dl) * betafunc(alpha0)
    if None is z0 : 
        g0      = gammafunc(alpha0)
        if isinstance(g0, np.ndarray) :
            n   = len(g0)
            assert(g0.shape == (n,n))
            z1  = np.identity(n, g0.dtype)
        else : z1 = 1. 
    else : z1 = np.asarray(z0)
    for ns in range(nstep) : # compute step ns+1 from ns
        alpha1  = alpha1 + dl * betafunc(alphactr)
        z1      = z1 + dl * np.dot(gammafunc(alphactr), z1)
        alphactr= alphactr + dl * betafunc(alpha1)
    return alpha1, z1

def rgflow_list(lmu0, lmu1, alphaS0, betafunc, gammafunc_list, z0=None, lstep=1e-4) :
    """ leapfrog-integrate over l=log(mu) from lmu0 to lmu1 using equations
            dalphaS/dlmu = betafunc(alphaS)
            dZ_{ij}/dlmu = gammafunc(alphaS)_{ik} . Z_{kj}
        the anom.dim. is defined so that
            O^R_i(mu) = Z_{ij}(mu) O^{bare}_j,
            (d/dlmu)O^R_i = dZ_{ik}/dlmu Z^{-1}_{kj} O^R_j = gammafunc_{ij}(alphaS) O^R_j
        NOTE conventions are chosen to be straightforward, which is inconsistent with most papers...
        lmu0,lmu1           start and end of integration
        betafunc(alphaS)     (vector of) coupling running \beta(alphaS) 
        gammafunc(alphaS)    (matrix of) anom.dim \gamma(alphaS)_{ij}
        NOTE make sure that normalization and sign are correct for QCD:
        for a=alphaS, mu[GeV], nf=5
            betafunc(alphaS, nf) = -(33 - 2 nf) / (6*pi)) alphaS**2 - ...
            betafunc(alphaS, 5) ~= -1.2201879 * alphaS^2 -0.48971905 * alphaS^3 - ...

        XXX z0 is copied before modification (safe to pass, never modified)
    """
    nstep       = max(1, int(abs(lmu1 - lmu0) / lstep)) # l[0] = lmu0, l[nstep] = lmu1 = lmu0 + nstep*dlmu
    dlmu        = (lmu1 - lmu0) / nstep
    alphaS1     = alphaS0
    alphaSctr   = alphaS0 + (0.5 * dlmu) * betafunc(alphaS0)

    if None is z0 : z0 = [None]*len(gammafunc_list)
    else : z0 = list(z0) # make a copy

    for i in range(len(z0)) :
	z0_i = z0[i]
	if None is z0_i :
	    g0_i    = gammafunc_list[i](alphaS0)
	    if isinstance(g0_i, np.ndarray) :
		n   = len(g0_i)
		assert(g0_i.shape == (n,n))
		z1_i  = np.identity(n, g0_i.dtype)
	    else : z1_i = 1. 
	else : z1_i = np.asarray(z0_i)
	z0[i] = z1_i

    for ns in range(nstep) : # compute step ns+1 from ns
        alphaS1  = alphaS1 + dlmu * betafunc(alphaSctr)
	for i in range(len(z0)) :
	    z0[i] = z0[i] + dlmu * np.dot(gammafunc_list[i](alphaSctr), z0[i])
        alphaSctr= alphaSctr + dlmu * betafunc(alphaS1)
    return alphaS1, z0

def rgflow_list_range(lmu_r, lmu0, alphaS0, betafunc, gammafunc_list, z0=None, lstep=1e-4) :
    """ integrate QCD evolution eqns (NOTE the conventions : all signs are positive, 
                meaning e.g. that b_coeff[0] < 0):
        beta_func(alphaS) = d(alphaS)/d(log(mu))
        gamma_func(alphaS) = d(log Z)/d(log(mu))

        lmu_r   log(mu) stops (don't have to be ordered)
        lmu0    initial point; z-factors=z0 at lmu=lmu0
        alphaS0 initial coupling, a(mu0)=a0
        
        return tuple (a[i_lmu], z[i_lmu, i_c])
    """
    lmu_r       = np.asarray(lmu_r)
    assert(0 < lstep)
    lmu_dict    = {}

    lmu_r_lo    = list(set(filter(lambda x: x <= lmu0, lmu_r)))
    lmu_r_lo.sort(reverse=True)
    lmu_a, alphaS, z1 = lmu0, alphaS0, z0
    for lmu in lmu_r_lo :
        alphaS, z1  = rgflow_list(lmu_a, lmu, alphaS, betafunc, gammafunc_list, z0=z1, lstep=lstep)
        lmu_dict[lmu] = (alphaS, z1)
        lmu_a       = lmu
    
    lmu_r_hi    = list(set(filter(lambda x: lmu0 < x, lmu_r)))
    lmu_r_hi.sort()
    lmu_a, alphaS, z1 = lmu0, alphaS0, z0
    for lmu in lmu_r_hi :
        alphaS, z1  = rgflow_list(lmu_a, lmu, alphaS, betafunc, gammafunc_list, z0=z1, lstep=lstep)
        lmu_dict[lmu] = (alphaS, z1)
        lmu_a       = lmu
    
    return (np.array([ lmu_dict[lmu][0] for lmu in lmu_r ]), 
            np.array([ lmu_dict[lmu][1] for lmu in lmu_r ])) 
