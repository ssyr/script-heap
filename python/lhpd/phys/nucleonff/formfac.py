import dataview as dv
from dataview import style_group_default as stg_list

#plot exp
from past.builtins import execfile
#execfile('formfac_exp_kelly.py')
import .formfac_exp_kelly as ff_kelly ; reload(ff_kelly)
import .formfac_exp_alberico as ff_albe ; reload(ff_albe)
import .formfac_exp_data as ff_data; reload(ff_data)

from formfac_exp_kelly import mu_p, mu_n
def get_curve_ff(q2r, ff, param, q2pow=0) :
    m = None
    if   'albe' == param : m = ff_albe
    elif 'kelly' == param : m = ff_kelly
    else : raise ValueError(param)
        
    if   'F1u'==ff: y = q2r**q2pow * (2*m.F1p(q2r)+m.F1n(q2r))
    elif 'F1d'==ff: y = q2r**q2pow * (2*m.F1n(q2r)+m.F1p(q2r))
    elif 'F1v'==ff: y = q2r**q2pow * (m.F1p(q2r) - m.F1n(q2r))
    elif 'F1s'==ff: y = q2r**q2pow * (m.F1p(q2r) + m.F1n(q2r))

    elif 'F2u'==ff: y = q2r**q2pow * (2*m.F2p(q2r)+m.F2n(q2r))
    elif 'F2d'==ff: y = q2r**q2pow * (2*m.F2n(q2r)+m.F2p(q2r))
    elif 'F2v'==ff: y = q2r**q2pow * (m.F2p(q2r) - m.F2n(q2r))
    elif 'F2s'==ff: y = q2r**q2pow * (m.F2p(q2r) + m.F2n(q2r))

    elif 'GEp'==ff: y = q2r**q2pow * m.GEp(q2r)
    elif 'GEn'==ff: y = q2r**q2pow * m.GEn(q2r)
    elif 'GEv'==ff: y = q2r**q2pow * (m.GEp(q2r) - m.GEn(q2r))
    elif 'GEs'==ff: y = q2r**q2pow * (m.GEp(q2r) + m.GEn(q2r))

    elif 'GMp'==ff: y = q2r**q2pow * m.GMp(q2r)
    elif 'GMn'==ff: y = q2r**q2pow * m.GMn(q2r)
    elif 'GMv'==ff: y = q2r**q2pow * (m.GMp(q2r) - m.GMn(q2r))
    elif 'GMs'==ff: y = q2r**q2pow * (m.GMp(q2r) + m.GMn(q2r))
        
    elif 'F1p'==ff: y = q2r**q2pow * m.F1p(q2r)
    elif 'F1n'==ff: y = q2r**q2pow * m.F1n(q2r)
    elif 'F2p'==ff: y = q2r**q2pow * m.F2p(q2r)
    elif 'F2n'==ff: y = q2r**q2pow * m.F2n(q2r)

    elif 'F2d1p'==ff: y = q2r**q2pow * (m.F2p(q2r)/m.F1p(q2r))
    elif 'F2d1n'==ff: y = q2r**q2pow * (m.F2n(q2r)/m.F1n(q2r))
    elif 'GEdMp'== ff: y = q2r**q2pow * m.GEp(q2r)/m.GMp(q2r)
    elif 'GEdMn'== ff: y = q2r**q2pow * m.GEn(q2r)/m.GMn(q2r)
    else: raise ValueError(ff)
        
    return y

""" functions to convert F1,F2<->GE,GM
    tau = Q^2 / (4M^2)
"""
def calc_GE(tau, f1, f2) :  return f1 - tau * f2
def calc_GM(tau, f1, f2) :  return f1 + f2
def calc_F1(tau, ge, gm) :  return (ge + tau * gm) / (1. + tau)
def calc_F2(tau, ge, gm) :  return (gm - ge) / (1. + tau)

def plot_curve_ff(q2r, ff, param, q2pow=0, yfactor=1., ax=None, stg=stg_list[0], label=None):
    if None is ax : ax = dv.make_std_axes()
    y = get_curve_ff(q2r, ff, param, q2pow=q2pow)
    ax.plot(q2r, yfactor*y, label=label, **stg.line())
    return ax
def calc_muGEdM(q2, F2d1):
    tau = q2 / 4. / ff_albe.mp**2
    return (1. - tau * F2d1) / (1. + F2d1)
def calc_F2d1(q2, GEdM):
    tau = q2 / 4. / ff_albe.mp**2
    return (1. - GEdM) / (tau + GEdM)

def calc_rE2(r12, kappa, mass) : 
    return r12 + (1.5 * kappa) / mass**2
def calc_r12(rE2, kappa, mass) : 
    return rE2 - (1.5 * kappa) / mass**2


# comparing ffs: ratio #1/#2
def plot_ff_cmp_1d2(h5fname1, h5fname2, flav, op, i_ff, tsep,
                     rsplan=('jk',1), 
                     ax=None, label=None, stg=stg_list[0]) :
    if None is ax : ax = dv.make_std_axes()
    ffd = []
    xrd = []
    for h5fn in [ h5fname1, h5fname2 ] :
        h5f = h5py.File(h5fn, 'r')
        h5d = h5f['/proton/%s/%s/ratio_pltx_avg/dt%d' % (op, flav, tsep)]
        ffd.append(h5d[:,0,:,i_ff])
        xrd.append(h5d.attrs['q2_list'])
        h5f.close()
    y_a, y_e = lhpd.calc_avg_err(ffd[0] / ffd[1], rsplan)
    q2r = xrd[0]
    ax.errorbar(q2r*ainv**2, y_a, yerr=y_e, label=label, **stg.edotline())
    ax.axhline(y=0, c='k', ls='-')
    ax.axhline(y=1, c='k', ls=':')
    ax.axhline(y=-1, c='k', ls=':')
    return ax

