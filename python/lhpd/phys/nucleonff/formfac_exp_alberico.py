from .formfac_exp_data import mp, mn, mu_p, mu_n


lambdaD2= 0.71
def GD(Q2) : return 1. / (1. + Q2 / lambdaD2)**2

def GEp_x(x) : return (1 - .19 * x) / (1 + 11.12 * x + 15.16 * x*x + 21.25 * x*x*x)
def GMp_x(x) : return mu_p * (1 + 1.09 * x) / (1 + 12.31 * x + 25.57 * x*x + 30.61 * x*x*x)
def GEn_x(x) : return 1.68 * x / (1. + 3.63 * x) * GD(x * 4. * mp**2)
def GMn_x(x) : return mu_n * (1 + 8.28 * x) / (1 + 21.30 * x + 77 * x*x + 238 * x*x*x)

def GEp(Q2) : return GEp_x(Q2/4./mp**2)
def GMp(Q2) : return GMp_x(Q2/4./mp**2)
def GEn(Q2) : return GEn_x(Q2/4./mp**2)
def GMn(Q2) : return GMn_x(Q2/4./mp**2)

def F1p_x(x) : return (GEp_x(x) + (x) * GMp_x(x)) / (1. + x)
def F2p_x(x) : return (GMp_x(x) - GEp_x(x)) / (1 + x)
def F1n_x(x) : return (GEn_x(x) + (x) * GMn_x(x)) / (1. + x)
def F2n_x(x) : return (GMn_x(x) - GEn_x(x)) / (1 + x)

def F1p(Q2) : return F1p_x(Q2/4./mp**2)
def F2p(Q2) : return F2p_x(Q2/4./mp**2)
def F1n(Q2) : return F1n_x(Q2/4./mp**2)
def F2n(Q2) : return F2n_x(Q2/4./mp**2)
