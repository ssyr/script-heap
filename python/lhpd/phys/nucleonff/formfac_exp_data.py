import numpy as np
from dataview import style_group

muGEdMp = [
    dict(data = np.fromstring("""
0.490   0.040   0.9660  0.0246
0.790   0.020   0.9500  0.0227
1.180   0.070   0.8690  0.0304
1.480   0.110   0.7980  0.0481
1.770   0.120   0.7280  0.0537
1.880   0.130   0.7200  0.0675
2.470   0.170   0.7260  0.0676
2.970   0.200   0.6120  0.0645
3.470   0.200   0.6090  0.0651
""", sep=' ').reshape(-1,4), 
        include = False,
        label   = '',
        citekey = 'Jones:1999rz',
        stg     = style_group('k', '', 'o', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.320   0.000   0.9300  0.0674
0.350   0.000   0.9100  0.0611
0.390   0.000   0.9610  0.0334
0.460   0.000   0.9520  0.0345
0.570   0.000   0.9590  0.0396
0.760   0.000   0.9660  0.0351
0.860   0.000   0.8650  0.0326
0.880   0.000   0.9230  0.0870
1.020   0.000   0.9000  0.0439
1.120   0.000   0.8250  0.0336
1.180   0.000   0.8510  0.0550
1.420   0.000   0.7330  0.0648
1.760   0.000   0.8160  0.1341
""", sep=' ').reshape(-1,4), 
        include = False,
        label   = '',
        citekey = 'Gayou:2001qt',
        stg     = style_group('k', '', 's', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
3.500   0.230   0.5710  0.0723
3.970   0.260   0.4830  0.0526
4.750   0.300   0.3850  0.0541
5.540   0.340   0.2780  0.0917
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Gayou:2001qd',
        stg     = style_group('k', '', 'd', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.490   0.040   0.9790  0.0171
0.790   0.020   0.9510  0.0156
1.180   0.070   0.8830  0.0222
1.480   0.110   0.7980  0.0389
1.770   0.120   0.7890  0.0424
1.880   0.130   0.7770  0.0408
2.130   0.150   0.7470  0.0467
2.470   0.170   0.7030  0.0402
2.970   0.200   0.6150  0.0358
3.470   0.200   0.6060  0.0443
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Punjabi:2005wq',
        stg     = style_group('k', '', 'v', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
5.170   0.123   0.4430  0.0684
6.700   0.190   0.3270  0.1073
8.490   0.167   0.1380  0.1841
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Puckett:2010ac',
        stg     = style_group('k', '', '^', alpha=0.8, alpha_shade=0.3)
    ),
]

# neutron electric form factor
GEn = [
    dict(data = np.fromstring("""
0.300   0.020   0.0552  0.0063
0.590   0.030   0.0477  0.0071
0.790   0.030   0.0468  0.0092
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Glazier:2004ny',
        stg     = style_group('k', '', 'o', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.447   0.000   0.0515  0.0064
1.132   0.000   0.0381  0.0033
1.474   0.000   0.0403  0.0041
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Plaster:2005cx',
        stg     = style_group('k', '', 's', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.495   0.000   0.0463  0.0070
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Zhu:2001md',
        stg     = style_group('k', '', 'x', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.150   0.000   0.0481  0.0084
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Herberg:1999ud',
        stg     = style_group('k', '', '^', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.210   0.000   0.0660  0.0155
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Passchier:1999cj',
        stg     = style_group('k', '', 'v', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.340   0.000   0.0611  0.0093
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Ostrick:1999xa',
        stg     = style_group('k', '', '<', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.385   0.115   0.0334  0.0043
""", sep=' ').reshape(-1,4), 
        include = False,
        label   = '',
        citekey = 'Becker:1999tw',
        stg     = style_group('k', '', '>', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.400   0.000   0.0520  0.0038
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Golak:2000nt',
        stg     = style_group('k', '', '>', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.670   0.000   0.0520  0.0121
""", sep=' ').reshape(-1,4), 
        include = False,
        label   = '',
        citekey = 'Rohe:1999sh',
        stg     = style_group('k', '', '*', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.670   0.000   0.04840  0.0071
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Bermuth:2003qh',
        stg     = style_group('k', '', '+', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
0.500   0.000   0.0526  0.0042
1.000   0.000   0.0454  0.0065
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Warren:2003ma',
        stg     = style_group('k', '', 'h', alpha=0.8, alpha_shade=0.3)
    ),
    dict(data = np.fromstring("""
1.720   0.000   0.0236  0.0031
2.480   0.000   0.0208  0.0031
3.410   0.000   0.0147  0.0024
""", sep=' ').reshape(-1,4), 
        include = True,
        label   = '',
        citekey = 'Riordan:2010id',
        stg     = style_group('k', '', 'd', alpha=0.8, alpha_shade=0.3)
    ),
]

# Q2    F1u     dF1u    F1d     dF1d    F2u     dF2u    F2d     dF2d
F12ud_cates11 = np.fromstring("""
0.300   1.0750  0.0060  0.5050  0.0120  0.7160  0.0060  -0.9950 0.0120
0.450   0.8530  0.0060  0.3770  0.0120  0.5150  0.0060  -0.7770 0.0120
0.500   0.7890  0.0060  0.3320  0.0120  0.4730  0.0060  -0.7080 0.0120
0.500   0.7890  0.0040  0.3400  0.0070  0.4630  0.0040  -0.7130 0.0070
0.590   0.6950  0.0060  0.2830  0.0130  0.3940  0.0060  -0.6170 0.0130
0.670   0.6280  0.0060  0.2490  0.0120  0.3420  0.0060  -0.5520 0.0120
0.790   0.5440  0.0080  0.2060  0.0150  0.2830  0.0080  -0.4670 0.0150
1.000   0.4340  0.0050  0.1540  0.0100  0.2110  0.0050  -0.3570 0.0100
1.130   0.3790  0.0030  0.1240  0.0050  0.1830  0.0030  -0.2980 0.0050
1.450   0.2900  0.0030  0.0930  0.0060  0.1280  0.0030  -0.2130 0.0060
1.720   0.2257  0.0022  0.0529  0.0043  0.1103  0.0022  -0.1429 0.0430
2.480   0.1380  0.0018  0.0278  0.0035  0.0632  0.0018  -0.0707 0.0350
3.410   0.0851  0.0012  0.0131  0.0024  0.0370  0.0012  -0.0337 0.0240
""", sep=' ').reshape(-1,9)



# radius meas
import lhpd
from lhpd.val_err import val_err as ve
mp      = 0.93827
mn      = 0.93957

mu_p    = 2.79285
mu_n    =-1.91304
kappa_p = mu_p - 1
kappa_n = mu_n

rEp_mup     = ve(0.84087, .00039)
rEp_codata  = ve(0.8775, 0.0051)
rEn2        = ve(-0.1161, .0022)

