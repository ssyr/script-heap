mp      = 0.93827
mn      = 0.93957

mu_p    = 2.79285
mu_n    =-1.91304
 
lambdaD2= 0.71
def GD(Q2) : return 1. / (1. + Q2 / lambdaD2)**2
  
  # definitions in terms of x = Q^2 / 4. / Mp^2
def GEp_x(x) : return (1 - 0.24*x) / (1 + 10.98 * x + 12.82 * x*x + 21.97 * x*x*x)
def GMp_x(x) : return mu_p * (1 + 0.12 * x) / (1 + 10.97 * x + 18.86 * x*x + 6.55 * x*x*x)
def GEn_x(x) : return 1.70 * x / (1. + 3.30 * x) * GD(x * 4. * mp**2)
def GMn_x(x) : return mu_n * (1 + 2.33 * x) / (1 + 14.72 * x + 24.20 * x*x + 84.1 * x*x*x)

def GEp(Q2) : return GEp_x(Q2/4./mp**2)
def GMp(Q2) : return GMp_x(Q2/4./mp**2)
def GEn(Q2) : return GEn_x(Q2/4./mp**2)
def GMn(Q2) : return GMn_x(Q2/4./mp**2)

def F1p_x(x) : return (GEp_x(x) + (x) * GMp_x(x)) / (1. + x)
def F2p_x(x) : return (GMp_x(x) - GEp_x(x)) / (1 + x)
def F1n_x(x) : return (GEn_x(x) + (x) * GMn_x(x)) / (1. + x)
def F2n_x(x) : return (GMn_x(x) - GEn_x(x)) / (1 + x)

def F1p(Q2) : return F1p_x(Q2/4./mp**2)
def F2p(Q2) : return F2p_x(Q2/4./mp**2)
def F1n(Q2) : return F1n_x(Q2/4./mp**2)
def F2n(Q2) : return F2n_x(Q2/4./mp**2)
