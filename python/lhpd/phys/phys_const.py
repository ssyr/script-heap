from lhpd.val_err import val_err as val_err
import math

# all in SI units

hbar_SI                 = 1.054571726e-34 # [J*s]
hbar_relerr             = 44e-9
hbar_SI_ve              = val_err(hbar_SI, hbar_SI * hbar_relerr)
hPlanck_SI_ve           = hbar_SI_ve * 2.*math.pi

clight_SI               = 299792458. # [m/s], exact
clight_relerr           = 0
clight_SI_ve            = val_err(clight_SI, clight_SI * clight_relerr)

electronVolt_SI         = 1.602176565e-19 # [J]
electronVolt_relerr     = 22e-9
electronVolt_SI_ve      = val_err(electronVolt_SI, electronVolt_SI * electronVolt_relerr)

hbarc_fmGeV_ve          = hbar_SI_ve * clight_SI_ve / (1e-6 * electronVolt_SI_ve) # [fm*GeV]
hbarc_relerr            = hbarc_fmGeV_ve.relerr()
hbarc_umeV_ve           = hbarc_fmGeV_ve # [um*eV]
hPlanckC_umeV_ve        = hbarc_fmGeV_ve * 2.*math.pi # [um*eV]

kBoltzmann_SI           = 1.3806488e-23 # [J/K]
kBoltzmann_relerr       = 910e-9
kBoltzmann_SI_ve        = val_err(kBoltzmann_SI, kBoltzmann_SI * kBoltzmann_relerr)



def print_energy_units():
    GeV_SI_ve = 1e9 * electronVolt_SI_ve
    # unit_name, val_SI
    arr = [
        ('GeV',     GeV_SI_ve),
        ('J',       1.),
        ('K',       kBoltzmann_SI_ve),
        ('Hz_freq', hPlanck_SI_ve),
        ('Hz_ang',  hbar_SI_ve),
        ('fmInv',   hbar_SI_ve * clight_SI_ve * 1e15)]
    for a in arr:
        s = ''
        for b in arr:
            s += '  =%s%s' % (a[1]/b[1], b[0])
        print(s)

def print_energy_units_2():
    eV_SI_ve = electronVolt_SI_ve
    # unit_name, val_SI
    arr = [
        ('eV',      eV_SI_ve),
        ('K',       kBoltzmann_SI_ve),
        ('Hz_freq', hPlanck_SI_ve) ]
    for a in arr:
        s = ''
        for b in arr:
            s += ' || %s%s' % (a[1]/b[1], b[0])
        print(s)
