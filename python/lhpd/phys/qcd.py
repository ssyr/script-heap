import math
from .qcd_alphaS import *
import numpy as np

def qcd_integrate(mu0, mu1, nsteps, a0, z0, beta_func, gamma_func) :
    """
        c_coeff_list    [i_c, i_order]
    """
    #order_b         = len(b_coeff)
    #c_coeff_list    = np.asarray(c_coeff_list)  # FIXME different orders for different c's ?
    #n_c, order_c    = c_coeff_list.shape
    #def calc_beta(a) :
        #return np.dot(b_coeff, a**np.r[2 : order_b + 2])
    #def calc_gamma_all(a) :
        #""" produce gamma(a)[i_c] """
        #return np.dot(c_coeff_list, a**np.r[1 : order_c + 1])

    assert (0 < nsteps)
    logmu0sq    = 2 * math.log(mu0)
    logmu1sq    = 2 * math.log(mu1)
    dlogmusq    = (logmu1sq - logmu0sq) / nsteps

    # initialize "leapfrog" for `a' 
    a_factor    = 4*math.pi
    a1  = a0
    actr= a0 + beta_func(a0 / a_factor) * a_factor * dlogmusq / 2.
    logz= np.zeros_like(z0, np.float)
    for n in range(nsteps):
        a1   += dlogmusq * beta_func(actr / a_factor) * a_factor
        logz += dlogmusq * gamma_func(actr / a_factor)
        actr += dlogmusq * beta_func(a1 / a_factor) * a_factor
    return a1, z0 * np.exp(logz)
    
""" definition of beta and gamma functions
    * argument aS = alphaS/(4*pi)
    * meaning beta(aS) = d aS / d log(mu^2) = b[0]*aS^2 + b[1]*aS^3 + ...
      (note the sign: b[0] = 11-(2/3)Nf
    * meaning gamma(aS) = d log(Z(mu)) / d log(mu^2) = c[0]*aS+c[1]*aS^2 + ...
        Oren(mu) = Z(mu)*Obare
"""
def make_betafunc_fixorder(nf, order) :
    b0  = 11 - 2./3*nf
    b1  = 102 - 38/3.*nf
    b3  = 0
    if 1==order:    return (lambda a : -a*a*b0)
    elif 2== order: return (lambda a : -a*a*(b0 + a*b1))
    else: raise NotImplementedError
def make_betafunc_series(beta_coeff_list) :
    """ starting from beta0 """
    n = len(beta_coeff_list)
    beta_coeff_list = np.array(beta_coeff_list)
    return lambda a : np.dot(beta_coeff_list, a**np.r_[2 : 2 + n])

def make_gammafunc_series(gamma_coeff_list) :
    n_g     = len(gamma_coeff_list)
    nmax_c  = max([len(gc) for gc in gamma_coeff_list ])
    gcoeff  = np.zeros((n_g, nmax_c), np.float)
    for i_g, gc in enumerate(gamma_coeff_list) :
        n_c = len(gc)
        gcoeff[i_g, :n_c] = gc
    return lambda a: np.dot(gcoeff, a**np.r_[1 : 1 + nmax_c])

def qcd_integrate_range(mu_r, mu0, a0, z0, beta_func, gamma_func, step_logmu=0.001) :
    """ integrate QCD evolution eqns (NOTE the conventions : all signs are positive, 
                meaning e.g. that b_coeff[0] < 0):
        beta_func(a) = da/dlog(mu^2)
        gamma_func(a) = dlogZ/dlog(mu^2)
        a = alpha_s/(4pi)


        mu_r    stops (don't have to be ordered)
        mu0     initial point; z-factors=z0 at mu=mu0
        a0      initial coupling, a(mu0)=a0
        nsteps  number of integration steps between (ordered) stops mu_r
        
        return tuple (a[i_mu], z[i_mu, i_c])
    """
    mu_r    = np.asarray(mu_r)
    assert(0 < step_logmu)
    z0  = np.asarray(z0)

    mu_dict = { mu0 : (a0, z0.copy()) }

    mu_r_lo = list(set(filter(lambda x: x < mu0, mu_r)))
    mu_r_lo.sort(reverse=True)

    a, mu_a = a0, mu0
    z1      = z0.copy()
    for mu in mu_r_lo :
        nsteps  = max(2, int(abs(mu_a - mu) / abs(step_logmu))) # do at least 2 steps
        a, z1   = qcd_integrate(mu_a, mu, nsteps, a, z1, beta_func, gamma_func)
        mu_dict[mu] = (a, z1)
        mu_a = mu
    
    mu_r_hi = list(set(filter(lambda x: mu0 < x, mu_r)))
    mu_r_hi.sort()
    a, mu_a = a0, mu0
    z1      = z0.copy()
    for mu in mu_r_hi :
        nsteps  = max(2, int(abs(mu_a - mu) / abs(step_logmu))) # do at least 2 steps
        a, z1   = qcd_integrate(mu_a, mu, nsteps, a, z1, beta_func, gamma_func)
        mu_dict[mu] = (a, z1)
        mu_a = mu
    
    return (np.array([ mu_dict[mu][0] for mu in mu_r ]), 
            np.array([ mu_dict[mu][1] for mu in mu_r ])) 
