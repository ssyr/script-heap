from __future__ import print_function
import lhpd
from lhpd.fitter.fitter2 import *
import numpy as np

# linear fit
n_x     = 10 
n_data  = 100
xr      = np.r_[0 : 1 : 1j*n_x]
a       = np.random.normal(1., .1, size=(n_data, 1))
b       = np.random.normal(1., .1, size=(n_data, 1))
y_noise = np.random.normal(0., .1, size=(n_data, n_x))
y       = a + b * xr + y_noise
ya,ys   = lhpd.calc_avg_err(y)
lin_mf  = modelfunc_inline(['a', 'b'], lambda x,p: p[0]+p[1]*x, lambda x,p:[1., x])
lin_df  = datafit(lin_mf, xr, ya, ys)
lin_fit = fitter2(lin_df)
res     = lin_fit.fit(p0=[0.,0.])
print(res)
p_ens   = []
for yi in y :
    lin_df.set_all(ya=yi)
    p_ens.append(lin_fit.fit_savestate()[0])

p_ens   = np.asarray(p_ens)
p_aec   = lhpd.calc_avg_err_ncov(p_ens)
lhpd.pprint_ve_ncov(p_aec[0], p_aec[1], p_aec[2], title=lin_mf.pname)
