from lhpd.val_err import val_err

kP  = 1.792847
QP  = 1.
kN  = -1.913043
QN  = 0.
mP  = 0.938272          # GeV
mN  = 0.939565          # GeV
hbarc   = 0.197327


rEp = { # fm
    'PRL84:1673': val_err(0.883,    0.014),     # H Lamb shift
    'Belushkin:2006qa:SC'
                : val_err(0.844,    0.006),
    'Belushkin:2006qa:pQCD'
                : val_err(0.830,    0.006),
    'Pohl'      : val_err(0.84184,  0.00067),   # mu-p Lamb shift
    'CODATA'    : val_err(0.8768,   0.0069),    # world average (date??)
    'PDG2012'   : val_err(0.877,    0.005),
}
rMp = { # fm
    'PLB576:62' : val_err(0.855,    0.035), # world data on elastic electron-proton scattering
    'Belushkin:2006qa:SC'
                : val_err(0.854,    0.005),
    'Belushkin:2006qa:pQCD'
                : val_err(0.850,    0.004),
    'PDG2012'   : val_err(0.777,    0.016)
}
rEn_sq = { # fm^2
    'PRL74:2427': val_err(-.113,    0.005),
    'Belushkin:2006qa:SC'
                : val_err(-.117,    0.009),
    'Belushkin:2006qa:pQCD'
                : val_err(-.119,    0.011),
    'PDG2012'   : val_err(-.1161,   0.0022),
}
rMn = { # fm
    'PLB524:26' : val_err(0.873,    0.011),     # MAMI (2002)
    'Belushkin:2006qa:SC'
                : val_err(0.862,    0.008),
    'Belushkin:2006qa:pQCD'
                : val_err(0.863,    0.006),
    'PDG2012'   : val_err(0.862,    0.008),
}

def r1_sq(rE_sq, kappa, m):
    return rE_sq - 1.5 * kappa / (m / hbarc)**2
def r2_sq(rE_sq, rM_sq, kappa, Q, m):
    """ 
        XXX Q is used only to compute mu=Q+kappa 
    """
    return (float(Q+kappa) * rM_sq - rE_sq) / kappa + 1.5 / (m / hbarc)**2
def r2_sq_umd(r2p, kp, r2n, kn): 
    return (kp * r2p - kn * r2n) / (kp - kn)
