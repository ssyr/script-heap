#
# changes
# 2019/06/13 new opt param to h5file_bin_unbias, h5file_bin : 
#   mfunc=None|mfunc(val[, mlist=<mlist>])  
# combine data with meas.info (eg reweighting) and/or other info; 
# XXX will replace func= and reweigh_func=
# XXX may be extended to take other info in future, so the mfunc prototypes must handle opt. kwargs
# XXX def mfunc1(data, **kwargs)
from __future__ import print_function
from future.utils import iteritems
from past.builtins import basestring
import itertools as it
import numpy as np
import h5py
from ..misc import numpy_extra as npe
from ..misc.misc import jsonify, strip_prefix # remove?
import time
import pickle, json

""" # remove all calls to PyTables
import tables as tb
def h5checkoutCArray(h5, h5kpath, atom, shape):
    # TODO select method based on type(h5)
    if False : # PyTables version
        i   = h5kpath.rfind('/')    # (-1) if not found
        where, name = h5kpath[:i], h5kpath[i+1:]
        if len(where) <= 0 or '/' != where[0]: where = '/' + where
        n   = None
        try:
            n = h5.get_node(where, name)
            if n.shape != shape:
                raise tb.NodeError, "node '%s' already exists with shape %s != %s" % (h5kpath, str(n.shape), str(shape))
            if n.atom != atom:
                raise tb.NodeError, "node '%s' already exists with atom %s != %s" % (h5kpath, str(n.atom), str(atom))
        except tb.NoSuchNodeError:
            n = h5.create_carray(where, name, atom, shape, createparents=True)
        return n
    else : # h5py version
        raise NotImplementedError

def h5checkoutTable(h5, h5kpath, atom, shape):
    # TODO select method based on type(h5)
    if False : # PyTables version 
        i   = h5kpath.rfind('/')    # (-1) if not found
        where, name = h5kpath[:i], h5kpath[i+1:]
        if len(where) <= 0 or '/' != where[0]: where = '/' + where
        n   = None
        try:
            n = h5.getNode(where, name)
            if n.shape != shape:
                raise tb.NodeError, "node '%s' already exists with shape %s != %s" % (h5kpath, str(n.shape), str(shape))
            if n.atom != atom:
                raise tb.NodeError, "node '%s' already exists with atom %s != %s" % (h5kpath, str(n.atom), str(atom))
        except tb.NoSuchNodeError:
            n = h5.createCArray(where, name, atom, shape, createparents=True)
        return n
    else : # h5py version
        raise NotImplementedError
"""

def h5type_attr_fix_(v) :
    """ compatibility type transform """
    if isinstance(v, basestring) : return np.string_(v)
    else : return v

# utilities
def h5_checkout_dataset(h5, h5kpath, atom, shape, exact=False, **kwargs) :
    # h5py version
    return h5.require_dataset(h5kpath, shape, atom, exact=exact, **kwargs)

def h5_purge_keys(h5g, key_list) :
    if isinstance(key_list, basestring) : key_list = [ key_list ]
    for k in key_list :
        #if k in h5g.keys() : del h5g[k]
        try: del h5g[k]
        except: pass

def h5_clean_group(h5g) :
    for k in h5g.keys() :
        del h5g[k]
    for k in h5g.attrs.keys() :
        del h5g.attrs[k]


h5_meas_dtype = [('ckpoint_id', 'S32'), ('source_coord', np.int32, (4,))]
def h5_meas_hash(m) : return hash(m.tostring())
def h5_meas_orderedhash(m) :
    return "% 20s%s" % (m[0], ''.join([ '%03d' % x for x in m[1] ]))
def h5_meas_match(dl_ap, dl_ex, hashfunc=h5_meas_hash) :
    return npe.np_match(dl_ap, dl_ex, hashfunc=h5_meas_hash)
def h5_meas_argsort(ml, hashfunc=h5_meas_orderedhash) : 
    return np.argsort([ hashfunc(m) for m in ml ])
def h5_meas_sort(ml, hashfunc=h5_meas_orderedhash) :
    """ in-place """
    a       = ml[ h5_meas_argsort(ml, hashfunc=hashfunc) ]
    ml[:]   = a     # semantics unclear for ml[:] = ml[order]

def h5_obj_basename(h5o) :
    pn = h5o.parent.name
    return h5o.name[1 + len(pn) : ]

def h5_set_datalist(h5_dset, data_list):
    assert isinstance(h5_dset, h5py.Dataset)

    kpath   = h5_dset.name + '__DATALIST'
    h5f     = h5_dset.file
    try : del h5f[kpath]
    except : pass
    h5_dset_dl = h5f.require_dataset(
            kpath, data_list.shape, data_list.dtype, fletcher32=True)
    h5_dset_dl[...] = data_list
    h5_dset.attrs['data_list'] = h5type_attr_fix_(kpath)

# datalist is typically too large to be an attribute
def h5_get_datalist(h5_dset):
    res = h5_dset.attrs['data_list']
    if (isinstance(res, basestring)):
        # if get a string, use it as a "link"
        res = h5_dset.parent[res][()]
    assert(1 == len(res.shape))
    assert(res.shape[0] == h5_dset.shape[0])
    return res

def h5_is_dset_datalist(h5d) :
    """ if h5d is a datalist to any h5o, return h5o; otherwise return None """
    bn_k = h5_obj_basename(h5d)
    for k in h5d.parent.keys() :
        ak = strip_prefix(bn_k, k)
        if "__DATALIST" == ak and h5_has_datalist(h5d.parent[k]) : 
            return h5d.parent[k]
def h5_is_attr_datalist(h5d, a) :
    """ check if attr satisfies reqs to be a Datalist """
    if 'data_list' != a : return False
    res     = h5d.attrs[a]
    if   isinstance(res, np.ndarray) : 
        assert(res.shape[0] == h5_dset.shape[0])
        return True
    elif isinstance(res, basestring) :
        try : 
            res = h5d.parent[res]
            if not isinstance(res, h5py.Dataset) : return False
        except : return False
        assert(res.shape[0] == h5d.shape[0])
        return True

def h5_has_datalist(h5d) :
    a       = 'data_list'
    if not (a in h5d.attrs.keys()) : return False
    return h5_is_attr_datalist(h5d, a)

# other hugeattrs
def h5_set_hugeattr(h5d, key, a, kname=None, ksuffix=None) :
    """ set hugeattr; delete previous record if exists (must be a Dataset) 
        to store attribute relative to h5d.parent using
        kname   if given and not empty, OR
        basename(h5d)<ksuffix> if given and not empty, OR
        basename(h5d)__HA_<key> otherwise
    """
    n   = h5d.name
    bn  = h5_obj_basename(h5d)
    assert(0 < len(bn))
    if (not None is kname) and ('' != kname) : bn_k = kname
    elif (not None is ksuffix) and ('' != ksuffix) : bn_k = "%s%s" % (bn, ksuffix)
    else : bn_k    = "%s__HA_%s" % (bn, key)

    if key in h5d.attrs.keys() :
        k1 = h5d.attrs[key]
        if not isinstance(k1, basestring) : 
            raise KeyError("not a hugeattr")
        if k1 in h5d.parent.keys() :
            if not isinstance(h5d.parent[k1], h5py.Dataset) :
                raise RuntimeError("not a hugeattr")
            del h5d.parent[k1]

    try : del h5d.parent[bn_k]
    except : pass
    h5d.parent[bn_k] = np.asarray(a)
    h5d.attrs[key] = bn_k

def h5_get_hugeattr(h5d, key) :
    k1  = h5d.attrs[key]
    if not isinstance(k1, basestring) :
        raise KeyError("not a hugeattr")
    if not isinstance(h5d.parent[k1], h5py.Dataset) :
        raise RuntimeError("not a hugeattr")
    return h5d.parent[k1]  # works for both abs. and rel. paths

def h5_is_attr_hugeattr(h5d, key) : 
    k1 = h5d.attrs[key]
    if not isinstance(k1, basestring) : return False
    if not k1.endswith("__HA_%s" % key) : return False
    try :
        if not isinstance(h5d.parent[k1], h5py.Dataset) : return False
    except KeyError : return False
    return True

def h5_is_dset_hugeattr(h5d) : 
    """ if h5d is a hugeattr to h5o object, return (h5o, attrname)
        otherwise return None
    """
    bn_k = h5_obj_basename(h5d)
    for k in h5d.parent.keys() :
        ak = strip_prefix(bn_k, "%s__HA_" % k) #or strip_prefix(bn_k, k)
        if ak and h5_is_attr_hugeattr(h5d.parent[k], ak) : 
            return h5d.parent[k], ak



def h5file_merge_bin(out_h5file, out_h5kpath, in_list, binsize, VERBOSE=False) :
    n   = len(in_list)
    if n <= 0 : return 
    assert(0 < binsize)

    dset_list = [ h5py.File(f, 'r')[k] for f, k in in_list ]
    
    dset0 = dset_list[0]
    dlist = [ h5_get_datalist(dset0) ]
    dset_len = dset0.shape[0]
    dset_select = [0]
    for i_dset in range(1, len(dset_list)) :
        dset = dset_list[i_dset]
        if h5dset_attr_equal(dset0, dset) and h5dset_dshape_equal(dset0, dset) :
            dset_select.append(i_dset)
            dlist.append(h5_get_datalist(dset))
            dset_len += dset.shape[0]
        else : 
            print("skip mismatching {%s}" % str(dset))
            raise ValueError("mismatching dset attr or shape")

    dlist = np.concatenate(dlist)
    assert (len(dlist) == dset_len)
    print("# dlist_len = ", dset_len)
    if (0 != dset_len % binsize) :
        print("WARNING: discard last %d samples due to binning" % (dset_len % binsize))
    n_bins = dset_len / binsize
    
    bin_shape   = (binsize,) + dset0.shape[1:]
    dset_shape  = (n_bins,) + dset0.shape[1:]
    dset_dtype  = dset0.dtype
    out_h5f = h5py.File(out_h5file, mode='a')
    out_h5d = out_h5f.require_dataset(out_h5kpath, shape=dset_shape, 
                        dtype=dset_dtype, fletcher32=True)
    cur_data = np.empty(bin_shape, dset_dtype)
    i_bin = 0
    cur_len = 0
    for i_dset in dset_select :
        if VERBOSE :
            print(i_dset, in_list[i_dset][0], in_list[i_dset][1], time.asctime())
        dset    = dset_list[i_dset]
        i_len   = dset.shape[0]
        dset_v  = dset[()]
        # repeatedly bite off first (binsize-cur_len) from dset_v
        if binsize != i_len :
            raise NotImplemented("needs testing for binsize!=len(dset)")

        while binsize <= cur_len + len(dset_v) :
            cur_data[cur_len : ]= dset_v[ : binsize - cur_len]  # back-fill cur_data
            dset_v              = dset_v[binsize - cur_len : ]  # bite off
            out_h5d[i_bin]      = cur_data.mean(0)              # bin&save
            i_bin              += 1                             # goto next bin
            cur_len             = 0                             # reset cur_data top
            cur_data.fill(np.nan)                               # fill with terror
            i_len              -= binsize - cur_len

        cur_len                 = len(dset_v)                   
        cur_data[ : cur_len]    = dset_v                        # save the rest

    # sanity checks
    assert((dset_len % binsize) == cur_len)
    assert(i_bin == n_bins)

    for k,v in iteritems(dset0.attrs) :
        if not k in ['data_list'] :
            out_h5d.attrs[k] = h5type_attr_fix_(v)
    
    # close open descriptors, or run out of ulimit
    for h5d in dset_list :
        h5d.file.close()

    # finalize
    out_h5f.flush()
    out_h5f.close()

def h5_copy_with_dlist(h5g_dst, dst_path, h5src) :
    h5g_dst.copy(h5src, dst_path)
    if 'data_list' in h5src.attrs.keys() : 
        h5_set_datalist(h5g_dst[dst_path], h5_get_datalist(h5src))
    # FIXME copy hugeattr?

def h5file_join(h5fname_out, h5_f_k_list) : 
    h5f_out = h5py.File(h5fname_out, 'a')
    for k_out, f, k in h5_f_k_list :
        print("# %s[%s] -> %s[]" %(f, k, h5fname_out))
        h5f_in = h5py.File(f, 'r')
        h5_copy_with_dlist(h5f_out, k, h5f_in[k])
        h5f_in.close()
        del h5f_in
        h5f_out.flush()
    h5f_out.close()


def h5file_merge(out_h5file, out_h5kpath, in_list, VERBOSE=False, 
        shape_common=False) :
    """ merge datasets along the "data" dimension (axis 0)
        merge data_list attributes
    """
    n   = len(in_list)
    if n <= 0 : return 

    dset_list = [ h5py.File(f, 'r')[k] for f, k in in_list ]
    
    dset0 = dset_list[0]
    dlist = [ h5_get_datalist(dset0) ]
    dset_len = dset0.shape[0]
    dset_select = [0]
    dshape_list = []
    for i_dset in range(1, len(dset_list)) :
        dset = dset_list[i_dset]
        dshape_list.append(dset.shape)
        if h5dset_attr_equal(dset0, dset) and (
                shape_common or h5dset_dshape_equal(dset0, dset)):
            dset_select.append(i_dset)
            dlist.append(h5_get_datalist(dset))
            dset_len += dset.shape[0]
        else : 
            print("skip mismatching {%s}" % str(dset))
            raise ValueError("mismatching dset attr or shape")
    # TODO if shape_commmon: select common dshape, trim and reorder data if necessary

    dlist = np.concatenate(dlist)
    assert (len(dlist) == dset_len)
    print("# dlist_len = ", dset_len)
    
    dset_shape = (dset_len,) + dset0.shape[1:]
    out_h5f = h5py.File(out_h5file, mode='a')
    out_h5d = out_h5f.require_dataset(out_h5kpath, shape=dset_shape, 
                        dtype=dset0.dtype, fletcher32=True)
    i_data = 0
    for i_dset in dset_select :
        if VERBOSE :
            print(i_dset, in_list[i_dset][0], in_list[i_dset][1], time.asctime())
        dset    = dset_list[i_dset]
        i_len   = dset.shape[0]
        out_h5d[i_data : i_data + i_len] = dset[:]
        i_data += i_len
    assert(i_data == dset_len)

    for k,v in iteritems(dset0.attrs) :
        if k in ['data_list'] : continue
        out_h5d.attrs[k] = h5type_attr_fix_(v)
    h5_set_datalist(out_h5d, dlist)
    
    # close open descriptors, or run out of ulimit
    for h5d in dset_list :
        h5d.file.close()

    # finalize
    out_h5f.flush()
    out_h5f.close()
    
    
def h5dset_dshape_equal(ds1, ds2) :
    """ compare : dataset shape except the "data" dimension """
    if ds1.shape[1:] != ds2.shape[1:] :
        raise TypeError("shape[1:] mismatch: %s != %s" % (ds1.shape[1:], ds2.shape[1:]))
        return False
    return True

def h5dset_attr_equal(ds1, ds2) :
    """ compare : attributes except data_list
    """
    
    def equal_all(a, b) :
        eq = (a == b)
        if isinstance(eq, np.ndarray) : 
            return eq.all()
        elif hasattr(eq, '__iter__') :
            return all(eq)
        else :
            return eq

    klist = set(list(ds1.attrs.keys()) + list(ds2.attrs.keys()))
    for k in klist :
        if k in ['data_list'] : continue
        if not k in ds1.attrs.keys() :
            raise TypeError("absent attribute {%s}.attrs[%s]" % (str(ds1), k))
            return False
        if not k in ds2.attrs.keys() :
            raise TypeError("absent attribute {%s}.attrs[%s]" % (str(ds2), k))
            return False
        if not equal_all(ds1.attrs[k], ds2.attrs[k]) :
            raise TypeError("attribute mismatch {%s}.attrs[%s] != {%s}.attrs[%s]" % (str(ds1), k, str(ds2), k))
            return False
    return True


# TODO move to lib
def h5_copy_attr(h5dst, h5src, klist, 
        check_key=False # raise exception if key is absent in h5src
    ) :
    for k in klist : 
        try :
            h5dst.attrs[k] = h5type_attr_fix_(h5src.attrs[k])
        except KeyError :
            if check_key : raise KeyError(k)
    # FIXME copy hugeattr?


def h5file_bin_unbias(out_h5file, out_h5kpath, in_list, 
            attrs_kw={}, keep_datalist='first', 
            func=None, reweight_func=None,
            mfunc=None,      # either mfunc or func/reweight_func may be specified (!=None)
            binsize=1,
            maxshape=None,
            VERBOSE=False) :
    """ 
            in_list : [(h5f_sloppy, h5k_sloppy, h5f_exact, h5k_exact), ... ]
    """
    n   = len(in_list)
    if n <= 0 : return 
    assert(1 <= binsize)
    if VERBOSE : print("h5file_bin: binsize=%d" % binsize)
    
    # scan files
    dset_select = []        # selected i_dset
    dlist_maps  = []        # pairs of maps
    dlist_bins  = []        # matching datalist [i_bin, i_data]
    # ref. h5file : an example to check consistency
    dset0       = None
    h5f0        = None

    for i_dset, (f1,k1,f2,k2) in enumerate(in_list) :
        h5f1    = h5py.File(f1, 'r')
        dset1   = h5f1[k1]
        h5f2    = h5py.File(f2, 'r')
        dset2   = h5f2[k2]
        if h5dset_attr_equal(dset1, dset2) and (None is dset0 or h5dset_dshape_equal(dset0, dset1)) :
            if None is dset0 or h5dset_attr_equal(dset0, dset1) and h5dset_dshape_equal(dset0, dset1) :
                dset_select.append(i_dset)
                dlist1  = h5_get_datalist(dset1)
                dlist2  = h5_get_datalist(dset2)
                
                m1, m2 = npe.np_match(dlist1, dlist2, hashfunc=h5_meas_hash)
                assert((dlist1[m1] == dlist2[m2]).all())
                assert(0 < len(m1) and len(m2) == len(m1))
                dlist_maps.append( [m1, m2] )
                dlist_bins.append(dlist1[m1])


                # remember as reference to check following data sets
                if None is dset0 : 
                    h5f0    = h5f1
                    dset0   = dset1

                if VERBOSE : 
                    print(i_dset, len(m1))

            else : 
                print("skip mismatching {%s}" % (str(dset1)))
                raise ValueError("mismatching dset attr or shape")
        else :
            print("pair {%s,%s} has attr mismatch" % (str(dset1, dset2)))
            raise ValueError("mismatching dset attr or shape")

        # cannot keep too many files open; will have to reopen
        if not h5f1 is h5f0 : h5f1.close()
        if not h5f2 is h5f0 : h5f2.close()

    #dlist_bins  = np.array(dlist_bins)         # FIXME this will fail with different number of data per file
    n_bins      = len(dlist_bins) // binsize
    n_match_l   = [ len(m[0]) for m in dlist_maps ]
    print("# dlist_len = ", n_bins, " match min/max=(%d,%d)" % (min(n_match_l), max(n_match_l)))
    if (n_bins <= 0) : 
        raise RuntimeError("no matching data sets")


    # prepare output h5 file & dataset
    assert(not None is h5f0)
    assert(not None is dset0)
    out_h5f = h5py.File(out_h5file, mode='a')
    h5_purge_keys(out_h5f, [out_h5kpath])
    out_h5d = None

    dlist_rebins = []
    for k in range(n_bins) :
        dlist_bin   = []
        dset_v1_bin = []
        dset_dv_bin = []
        for i_dset in dset_select[k * binsize : (k+1)*binsize]  :
            if VERBOSE : print(i_dset, in_list[i_dset], time.asctime())

            dlist_bin.extend(dlist_bins[i_dset])

            # reopen files
            f1, k1, f2, k2 = in_list[i_dset]
            h5f1    = h5py.File(f1, 'r')
            dset1   = h5f1[k1]
            h5f2    = h5py.File(f2, 'r')
            dset2   = h5f2[k2]

            # get data & maps
            m1, m2  = dlist_maps[k]
            dset_v1 = dset1[()]
            dset_v2 = dset2[()]
            if not None is mfunc :
                if not ( (None is func) and (None is reweight_func) ) :
                    raise RuntimeError("cannot specify both mfunc and func/reweight_func")
                mlist   = h5_get_datalist(dset1)
                dset_v1 = mfunc(dset_v1, mlist=mlist, attrs=dset1.attrs)
                dset_v2 = mfunc(dset_v2, mlist=mlist[m1], attrs=dset2.attrs)
            else :
                if not None is func :
                    dset_v1 = func(dset_v1)
                    dset_v2 = func(dset_v2)
                if not None is reweight_func :
                    wlist   = reweight_func(h5_get_datalist(dset1))
                    assert(len(wlist) == len(dset_v1))
                    for i, w in enumerate(wlist) :
                        dset_v1[i] *= npe.np_cast_rshape(w, dset_v1.shape[1:])
                    for i, w in enumerate(wlist[m1]) :
                        dset_v2[i] *= npe.np_cast_rshape(w, dset_v2.shape[1:])
            dset_v1_bin.append(dset_v1)
            dset_dv_bin.append(dset_v2[m2] - dset_v1[m1])
            # close in h5files
            h5f1.close()
            h5f2.close()

        dlist_rebins.append(dlist_bin)

        if None is out_h5d :
            assert(0 == k)
            dset_dtype  = dset_v1_bin[0].dtype
            dset_shape  = (n_bins,) + dset_v1_bin[0].shape[1:]
            out_h5d = out_h5f.require_dataset(out_h5kpath, 
                    shape=dset_shape, dtype=dset_dtype, fletcher32=True)
            
        out_h5d[k]  = ( np.concatenate(dset_v1_bin, axis=0).mean(0) 
                      + np.concatenate(dset_dv_bin, axis=0).mean(0))

    # copy attrs
    for k,v in it.chain(iteritems(dset0.attrs), iteritems(attrs_kw)) :
        if not k in ['data_list'] :
            out_h5d.attrs[k] = h5type_attr_fix_(v)
    # close the last (ref.) in-h5file
    h5f0.close() ; h5f0, dset0 = None, None
    
    if not False is keep_datalist and not None is keep_datalist: 
        if 'first' == keep_datalist :
            dlist_bins = [ mb[0] for mb in dlist_rebins ]
        elif callable(keep_datalist) :
            dlist_bins = [ keep_datalist(mb) for mb in dlist_rebins ]
        else : warn('bad keep_datalist option; save datalist verbatim')
        h5_set_datalist(out_h5d, np.array(dlist_bins))

    # finalize
    out_h5f.flush()
    out_h5f.close()

if False :
    # this is a complicated function, use simpler synopsis in h5_write_dict
    def h5file_write_dict(h5g_out, kpath, d, **attr) :
        """ write out a dictionary into h5
            leafs are converted into np.array()
            each leaf will have `attr' added as attributes
        """
        if '' ==  kpath : kpath = '.'
        for k,v in iteritems(d) :
            if '' == kpath : kpath_new = k
            else : kpath_new = '%s/%s' % (kpath, k)
            assert(isinstance(k, basestring))
            if isinstance(v, dict) :
                h5file_write_dict(h5g_out, kpath_new, v, **attr)
            else :
                h5g_out[kpath_new] = np.array(v)
                for a, av in iteritems(attr) :
                    if 'data_list' == a :
                        h5_set_datalist(h5g_out[kpath_new], av)
                    else : h5g_out[kpath_new].attrs[a] = h5type_attr_fix_(av)

def h5_write_dict(h5g, d) :
    h5_purge_keys(h5g, d.keys())
    for k, v in iteritems(d) :
        h5g[k] = v

def h5_combine_shape(dset_shape1, maxshape1) :
    if not None is maxshape1 and 0 < len(maxshape1) :
        assert(len(dset_shape1) == len(maxshape1))
        dset_shape1 = list(dset_shape1)
        dset_slice1 = []
        for i_a, s_a in enumerate(maxshape1) :
            if not None is s_a :
                s1_a = min(s_a, dset_shape1[i_a])
                dset_shape1[i_a] = s1_a
                dset_slice1.append(slice(s1_a))
            else :
                dset_shape1[i_a] = s_a
                dset_slice1.append(slice(None))
        return tuple(dset_shape1), dset_slice1
    else : return dset_shape1, [slice(None)]

def h5file_bin(out_h5file, out_h5kpath, in_list, 
            attrs_kw={}, keep_datalist='first',
            func=None, reweight_func=None,
            mfunc=None,      # either mfunc or func/reweight_func may be specified (!=None)
            binsize=1, # bin also adjacent elements in in_list 
            maxshape=None,
            VERBOSE=False) :
    """ 
            in_list : [(h5f, h5k), ... ]
    """
    n   = len(in_list)
    if n <= 0 : return 
    assert(1 <= binsize)
    if VERBOSE : print("h5file_bin: binsize=%d" % binsize)
    
    # scan files
    dset_select = []        # selected i_dset
    dlist_bins  = []        # matching datalist [i_bin, i_data]
    # ref. h5file : an example to check consistency
    dset0       = None
    h5f0        = None

    for i_dset, (f1,k1) in enumerate(in_list) :
        h5f1  = h5py.File(f1, 'r')
        dset1   = h5f1[k1]
        if None is dset0 or h5dset_attr_equal(dset0, dset1) and h5dset_dshape_equal(dset0, dset1) :
            dset_select.append(i_dset)
            dlist1  = h5_get_datalist(dset1)
            
            dlist_bins.append(dlist1)

            # remember as reference to check following data sets
            if None is dset0 : 
                h5f0    = h5f1
                dset0   = dset1

            if VERBOSE : 
                print(i_dset)

        else : 
            print("skip mismatching {%s}" % str(dset1))
            raise ValueError("mismatching dset attr or shape")

        # cannot keep too many files open; will have to reopen
        if not h5f1 is h5f0 : h5f1.close()

    #dlist_bins  = np.array(dlist_bins)         # FIXME this will fail with different number of data per file

    n_bins      = len(dlist_bins) // binsize
    print("# dlist_len = ", n_bins)
    if (n_bins <= 0) : 
        raise RuntimeError("no matching data sets")
        return

    # prepare output h5 file & dataset
    assert(not None is h5f0)
    assert(not None is dset0)
    out_h5f = h5py.File(out_h5file, mode='a')
    h5_purge_keys(out_h5f, [out_h5kpath])
    out_h5d = None

    dlist_rebins = []
    for k in range(n_bins)  :
        dlist_bin   = []
        dset_bin    = []
        for i_dset in dset_select[k * binsize : (k+1)*binsize] :
            if VERBOSE : print(i_dset, in_list[i_dset], time.asctime())

            dlist_bin.extend(dlist_bins[i_dset])

            # reopen files
            f1, k1  = in_list[i_dset]
            h5f1    = h5py.File(f1, 'r')
            dset1   = h5f1[k1]
            # get data & maps
            dset_v1 = dset1[()]
            if not None is mfunc :
                if not ( (None is func) and (None is reweight_func) ) :
                    raise RuntimeError("cannot specify both mfunc and func/reweight_func")
                mlist   = h5_get_datalist(dset1)
                dset_v1 = mfunc(dset_v1, mlist=mlist)
            else :
                if not None is func :
                    dset_v1 = func(dset_v1)
                if not None is reweight_func :
                    wlist   = reweight_func(h5_get_datalist(dset1))
                    assert(len(wlist) == len(dset_v1))
                    for i, w in enumerate(wlist) :
                        dset_v1[i] *= npe.np_cast_rshape(w, dset_v1.shape[1:])
            dset_bin.append(dset_v1)
            # close in h5files
            h5f1.close()

        dlist_rebins.append(dlist_bin)

        if None is out_h5d :
            assert(0 == k)
            dset_dtype  = dset_bin[0].dtype
            dset_shape1, dset_slice1 = h5_combine_shape(dset_bin[0].shape[1:], maxshape)
            dset_shape  = (n_bins,) + dset_shape1
            #dset_slice  = [slice(None)] + dset_slice1
            
            out_h5d = out_h5f.require_dataset(out_h5kpath, 
                    shape=dset_shape, dtype=dset_dtype, fletcher32=True)

        out_h5d[k]  = np.concatenate(dset_bin, axis=0).mean(0)      # FIXME use maxshape/dset_slice1 here?

    # copy attrs
    for k,v in it.chain(iteritems(dset0.attrs), iteritems(attrs_kw)) :
        if not k in ['data_list'] :
            out_h5d.attrs[k] = h5type_attr_fix_(v)
    # close the last (ref.) in-h5file
    h5f0.close() ; h5f0, dset = None, None
    
    if not False is keep_datalist and not None is keep_datalist: 
        if 'first' == keep_datalist :
            dlist_bins = [ mb[0] for mb in dlist_rebins ]
        elif callable(keep_datalist) :
            dlist_bins = [ keep_datalist(mb) for mb in dlist_rebins ]
        else : warn('bad keep_datalist option; save datalist verbatim')
        h5_set_datalist(out_h5d, np.array(dlist_bins))

    # finalize
    out_h5f.flush()
    out_h5f.close()




def h5_attrdict_equal(a1, a2, except_attr=[]) :
    """ compare (dictionaries of) attributes handling special cases 
        such as numpy arrays 
        XXX except_attr is passed recursively, so it will not be compared 
        in the children either
    """
    if isinstance(a1, dict) :
        k1 = a1.keys() ; k1.sort()
        k2 = a2.keys() ; k2.sort()
        if k1 != k2 : 
            #print(k1, k2)
            return False
        for k in k1 : 
            if k in except_attr : continue
            if not h5_attrdict_equal(a1[k], a2[k], except_attr=except_attr) : 
                return False
        return True
    elif isinstance(a1, np.ndarray) or isinstance(a2, np.ndarray) :
        #if not (a1 == a2).all() : print(a1, a2)
        return (a1 == a2).all()
    else : 
        #if a1 != a2 : print(a1, a2)
        return a1 == a2

def h5file_check_attr_shape_dtype(h5fk_list, check_attr=True, except_attr=[],
        attr0=None, sh0=None, dt0=None  # initial values for comparison
        ) :
    """ verify that attributes in all data sets are identical, 
        (to attr, if specified) as well as shapes of data elements (shape[1:])
        and return attr, shape[1:] """
    dset = []
    for h5f, h5k in h5fk_list :
        h5 = h5py.File(h5f, 'r')
        h5d= h5[h5k]
        attr    = dict(h5d.attrs)
        sh      = h5d.shape[1:]
        dt      = h5d.dtype
        h5.close()
        for k in ['data_list'] + except_attr : attr.pop(k, None)

        if None == attr0 : attr0 = attr
        elif not h5_attrdict_equal(attr, attr0) :
            #print(attr, attr0)
            raise ValueError("attr mismatch in (%s[%s])" % (h5f, h5k))

        if None == sh0 : sh0 = sh
        elif sh != sh0 : 
            #print(sh, sh0)
            raise ValueError("shape mismatch in (%s[%s])" % (h5f, h5k))
        if None == dt0 : dt0 = dt
        elif dt != dt0 : 
            #print(dt, dt0)
            raise ValueError("dtype mismatch in (%s[%s])" % (h5f, h5k))

    return attr0, sh0, dt0

def h5file_concat_datalist(h5fk_list) :
    """ return concatenated data list from files """
    dl = []
    for h5f, h5k in h5fk_list :
        h5 = h5py.File(h5f, 'r')
        dl.append(h5_get_datalist(h5[h5k]))
        h5.close()
    return np.concatenate(dl)

def h5file_concat_dataset(h5fk_list) :
    """ return concatenated numpy array """
    dset = []
    for h5f, h5k in h5fk_list :
        h5 = h5py.File(h5f, 'r')
        dset.append(h5[h5k][()])
        h5.close()
    return np.concatenate(dset)



def h5file_group_bin_unbias(out_h5file, out_h5kpath, in_list_ap, in_list_ex, 
        VERBOSE=False, keep_datalist='first', check_match=True) :
    """ 
            in_list_ap : [ [ ( apprx_bin0_file0, apprx_bin0_key0 ), ... ], ... ]
            in_list_ex : [ [ ( exact_bin0_file0, exact_bin0_key0 ), ... ], ... ]
            = matched lists of h5 dsets to bin and combine for ama bias correction
    """
    n_bins = len(in_list_ap)
    assert(len(in_list_ex) == n_bins)
    if n_bins <= 0 : return 
    
    # scan files
    dlist_maps  = []        # pairs of maps
    dlist_bins  = []        # will be used as datalist for the binned data
    # ref. h5file : an example to check consistency
    at0, sh0, dt0  = None, None, None

    # scan data sets in every bin
    for i_dset in range(n_bins) :
        set_ap  = in_list_ap[i_dset]
        at_ap, sh_ap, dt_ap = h5file_check_attr_shape_dtype(set_ap)
        dl_ap   = h5file_concat_datalist(set_ap)
        set_ex  = in_list_ex[i_dset]
        at_ex, sh_ex, dt_ex = h5file_check_attr_shape_dtype(set_ex)
        dl_ex   = h5file_concat_datalist(set_ex)
        if not (h5_attrdict_equal(at_ap, at_ex) 
                and sh_ap == sh_ex and dt_ap == dt_ex) :
            raise RuntimeError("mismatch in attr / shape / dtype (dset %d)" % i_dset)
        
        if None is at0 : at0, sh0, dt0 = at_ap, sh_ap, dt_ap
        else:
            if not (h5_attrdict_equal(at_ap, at0)
                    and sh_ap == sh0 and dt_ap == dt0) :
                raise RuntimeError("mismatch in attr / shape / dtype (dset %d)" % i_dset)
        
        m1, m2 = npe.np_match(dl_ap, dl_ex, hashfunc=h5_meas_hash)
        assert((dl_ap[m1] == dl_ex[m2]).all())
        dlist_maps.append( [m1, m2] )
        dlist_bins.append(dl_ap[m1])

        if check_match and len(m1) != len(dl_ex) :
            raise RuntimeError("incorrect ap-ex matching (dset %d)" % i_dset)

        if VERBOSE : 
            print(i_dset, len(m1))

    #dlist_bins  = np.array(dlist_bins)         # FIXME this will fail with different number of data per file
    assert(n_bins == len(dlist_bins))
    print("# dlist_len = ", n_bins)
    if (n_bins <= 0) : 
        raise RuntimeError("no matching data sets")
        return

    # prepare output h5 file & dataset
    dset_shape  = (n_bins,) + sh0
    dset_dtype  = dt0
    out_h5f = h5py.File(out_h5file, mode='a')
    h5_purge_keys(out_h5f, [out_h5kpath])
    out_h5d = out_h5f.require_dataset(out_h5kpath, shape=dset_shape, 
                        dtype=dset_dtype, fletcher32=True)
    # copy attrs
    for k,v in iteritems(at0) :
        if not k in ['data_list'] :
            out_h5d.attrs[k] = h5type_attr_fix_(v)

    # bin/unbias data
    for i_dset in range(n_bins) :
        if VERBOSE :
            print(i_dset, time.asctime())
        m1, m2  = dlist_maps[i_dset]
        v_ap    = h5file_concat_dataset(in_list_ap[i_dset])
        v_ex    = h5file_concat_dataset(in_list_ex[i_dset])
        v_unbias= v_ap.mean(0) + (v_ex[m2] - v_ap[m1]).mean(0)
        out_h5d[i_dset] = v_unbias
    
    if not False is keep_datalist and not None is keep_datalist: 
        if 'first' == keep_datalist :
            dlist_bins = [ mb[0] for mb in dlist_bins ]
        elif callable(keep_datalist) :
            dlist_bins = [ keep_datalist(mb) for mb in dlist_bins ]
        else : warn('bad keep_datalist option; save datalist verbatim')
        h5_set_datalist(out_h5d, np.array(dlist_bins))

    # finalize
    out_h5f.flush()
    out_h5f.close()

def h5_get_rsplan(dset) : 
    if 'rsplan' in dset.attrs.keys() : 
        try : return tuple(json.loads(dset.attrs['rsplan']))
        except : pass
        return tuple(pickle.loads(dset.attrs['rsplan']))
    else : return None

def h5_set_rsplan(dset, rsplan) : 
    dset.attrs['rsplan'] = json.dumps(jsonify(rsplan))

def h5_check_rsplan(*dset_list, **d) :
    """ select default rsplan for data sets and check 
        that wherever it is defined, it is consistent """
    rsplan = d.get('rsplan')
    assert (None is rsplan or isinstance(rsplan, tuple))
    for d in dset_list :
        rsplan1 = h5_get_rsplan(d)
        if None is rsplan :
            rsplan = rsplan1
        elif rsplan != rsplan1 : 
            raise ValueError('rsplan mismatch : "%s" != "%s"' % (str(rsplan), str(rsplan1)))
    return rsplan





# FIXME eliminate redundant functions
# h5_merge
# h5file_bin
# h5_merge_bin
# h5file_bin_unbias
# h5file_group_bin_unbias
# TODO instead, write a general binning (+unbias) function :

#   in_list     list of input data sets [i_dset] = (h5f, h5k)
#               must have matching datalists and shape[1:]
#   bin_by      "hash"/key function : meas_spec -> key_bin, 
#               the output bins will be sorted in key_bin ascending order
#   VERBOSE     print_ bin stats (size:count)
#
# problem: cannot open all h5f files; data can be scattered over 
# 1) scan datasets, generage bin_map = {key_bin : [(i_dset, i_data, meas_spec) ... ] }
# 2) sort list of key_bins : this is the bin order 
#    create meas_bin[i_bin] = [ sorted meas_spec_bin.sorted ... ]
# 3) create output zero array with n_data*dshape, dtype - in file or RAM?
# 4) make list of h5d_map [i_dset] = [ (i_data, i_bin) ... ]
# 5) iterate over h5f, h5k, accumulate bins, bin counts (later check against 
def h5file_scan_group_(in_list, bin_by, 
        bin_filter=lambda x:True, except_attr=[]) :
    """ aux function to get info for merging
        bin_by      hash function meas_spec -> key_bin
    """
    n_dset      = len(in_list)
    bin_map     = {}
    dset_dtype  = None
    dset_shape  = None
    dset_attrs  = None

    for i_dset, (h5file, h5kpath) in enumerate(in_list) :
        h5f         = h5py.File(h5file, 'r')
        h5d         = h5f[h5kpath]
        dset_dlist  = h5_get_datalist(h5d)

        # collect dset/data refs into bins
        for i_data, m in enumerate(dset_dlist) :
            if not bin_filter(m) : continue
            bin_map.setdefault(bin_by(m), []).append((i_dset, i_data, m))

        # check shape
        if None is dset_shape : dset_shape = h5d.shape[1:]
        elif dset_shape != h5d.shape[1:] : 
            raise ValueError("shape mismatch: expect %s, get %s in '%s[%s]'" % (dset_shape, h5d.shape[1:], h5file, h5kpath))
        # check dtype
        if None is dset_dtype : dset_dtype = h5d.dtype
        else :
            dset_dtype = np.find_common_type([dset_dtype, h5d.dtype], [])

        # check attrs
        attrs   = dict(h5d.attrs)
        for k in ['data_list'] + except_attr : 
            attrs.pop(k, None)
        if None is dset_attrs : dset_attrs = attrs
        elif not h5_attrdict_equal(dset_attrs, attrs, except_attr=except_attr) :
            raise ValueError("attrs mismatch in '%s[%s]'" % (h5f, h5k))

        h5f.close()

    # bin order, meas_spec bin.rep's
    key_bin     = bin_map.keys()
    key_bin.sort()
    n_bins      = len(key_bin)
    bin_list    = [ bin_map[k] for k in key_bin ]   # ordered values of bin_map
    return dset_shape, dset_dtype, dset_attrs, key_bin, bin_list
    
def h5file_map_dset2bins_(bin_list) :
    """ make map [i_dset] = [(i_data, i_bin) ...] 
        from bin_list[i_bin]=[(i_dset, i_data) ...] 
    """
    # map dset/data -> bin
    h5d_map = {} #[ [] for i in range(n_dset) ]
    for i_bin, bin in enumerate(bin_list) :
        for b in bin : 
            i_dset, i_data = b[:2]
            h5d_map.setdefault(i_dset, []).append((i_data, i_bin))
    #h5d_list = [ h5d_map.get(i_dset, []) for i_dset in range(n_dset) ]
    for l in h5d_map.itervalues() : 
        l.sort()     # sort access order for faster traversal
    return h5d_map

# FIXME split code into separate func?

def h5file_bin_general(out_h5file, out_h5kpath, in_list, bin_by, 
        bin_filter=lambda m : True,
        except_attr=[],
        keep_datalist='first',
        VERBOSE=False) :
    """
        out_h5file, out_h5kpath     where to save binned data
        in_list     [(h5f, h5k) ... ]   TODO h5f may be h5g; don't open/close
        bin_by      hash function meas_spec -> key_bin
        keep_datalist   save a list of representatives from bins ([0] in sorted order)

        TODO spit list of bins into groups of fixed size; 
             accumulate and write by groups sequentiall to avoid ENOMEM
    """
    n_dset      = len(in_list)
    dset_shape, dset_dtype, dset_attrs, key_bin, bin_list = h5file_scan_group_(
                in_list, bin_by, bin_filter=bin_filter, except_attr=except_attr)
    bin_meas    = [ np.array([ a[2] for a in bin ], dtype=h5_meas_dtype) for bin in bin_list ]
    h5d_map     = h5file_map_dset2bins_(bin_list)
    n_bins      = len(bin_list)
        
    if VERBOSE :    # print_ bin stats
        bin_len = [ len(b) for b in bin_list ]
        bin_len.sort()
        bin_len_uniq = list(set(bin_len))
        bin_len_uniq.sort()
        for b in bin_len_uniq :
            print("h5file_bin_general: %d bins of %d data # %s" % (bin_len.count(b), b, time.asctime()))


    # output; FIXME put directly to file to avoid ENOMEM ?
    res     = np.zeros((n_bins,) + dset_shape, dset_dtype)
    res_cnt = np.zeros((n_bins,), np.int)

    for i_dset, (h5file, h5kpath) in enumerate(in_list) :
        if not i_dset in h5d_map : continue
        h5f         = h5py.File(h5file, 'r')
        h5d         = h5f[h5kpath]
        for i_data, i_bin in h5d_map.get(i_dset, []) :
            res[i_bin] += h5d[i_data]
            res_cnt[i_bin] += 1

        h5f.close()

    for i_bin in range(n_bins) : 
        assert(len(bin_list[i_bin]) == res_cnt[i_bin])
        res[i_bin] /= res_cnt[i_bin]

    out_h5f = h5py.File(out_h5file, mode='a')
    h5_purge_keys(out_h5f, [out_h5kpath])
    out_h5d = out_h5f.require_dataset(out_h5kpath, shape=(n_bins,) + dset_shape,
                            dtype=dset_dtype, fletcher32=True)
    out_h5d[:] = res
    for k, v in iteritems(dset_attrs) :
        out_h5d.attrs[k] = h5type_attr_fix_(v)

    #old version
    #if keep_datalist : 
        #for b in bin_meas : 
            #h5_meas_sort(b)     # sort to select representative (FIXME use `max'?)
        #binrep_meas = np.array([ a[0] for a in bin_meas ], dtype=h5_meas_dtype)
        #h5_set_datalist(out_h5d, binrep_meas)
    dlist_bins  = bin_meas      # alias
    if not False is keep_datalist and not None is keep_datalist: 
        if 'first' == keep_datalist :
            dlist_bins = [ mb[0] for mb in dlist_bins ]
        elif callable(keep_datalist) :
            dlist_bins = [ keep_datalist(mb) for mb in dlist_bins ]
        else : warn('bad keep_datalist option; save datalist verbatim')
        h5_set_datalist(out_h5d, np.array(dlist_bins))
    out_h5d.attrs['bincount'] = h5type_attr_fix_(res_cnt)
    
    out_h5f.flush()
    out_h5f.close()



def h5file_bin_unbias_general(out_h5file, out_h5kpath, in_list_ap, in_list_ex, bin_by,
        bin_filter=lambda m : True,
        except_attr=[],
        keep_datalist='first',
        VERBOSE=False) :
    """ 
        TODO spit list of bins into groups of fixed size; 
             accumulate and write by groups sequentiall to avoid ENOMEM
    """
    # scan ap
    n_dset_ap   = len(in_list_ap)
    dset_shape_ap, dset_dtype_ap, dset_attrs_ap, key_bin_ap, bin_list_ap = h5file_scan_group_(
                in_list_ap, bin_by, bin_filter=bin_filter, except_attr=except_attr)
    bin_meas_ap = [ np.array([ a[2] for a in bin ], dtype=h5_meas_dtype) for bin in bin_list_ap ]
    h5d_map_ap  = h5file_map_dset2bins_(bin_list_ap)

    # scan ex 
    n_dset_ex   = len(in_list_ex)
    dset_shape_ex, dset_dtype_ex, dset_attrs_ex, key_bin_ex, bin_list_ex = h5file_scan_group_(
                in_list_ex, bin_by, bin_filter=bin_filter, except_attr=except_attr)
    bin_meas_ex = [ np.array([ a[2] for a in bin ], dtype=h5_meas_dtype) for bin in bin_list_ex ]

    assert(dset_shape_ap == dset_shape_ex) 
    dset_shape = dset_shape_ap
    dset_dtype = np.find_common_type([dset_dtype_ap, dset_dtype_ex], [])
    assert(h5_attrdict_equal(dset_attrs_ap, dset_attrs_ex, except_attr=except_attr))

    # check that sets of bins are the consistent 
    # FIXME here we assume that binning results in the same set of bins
    # for ap and ex data, the order will be the same due to key ordering 
    # if key_bin_ap != key_bin_ex otherwise, desired behaivor must be indicated:
    # * select intersection of key sets OR
    # * put all ap samples, do not do bias-correct if no ex bins OR
    # * ?
    assert(key_bin_ap == key_bin_ex)
    n_bins      = len(bin_list_ap)

    # match ap <-> ex data in every bin
    bin_list_m_ap, bin_list_m_ex = [], []   # matched binning sets for bias correction
    for i_bin in range(n_bins) :
        m_ap, m_ex = h5_meas_match(bin_meas_ap[i_bin], bin_meas_ex[i_bin])
        bin_list_m_ap.append([ bin_list_ap[i_bin][j] for j in m_ap])
        bin_list_m_ex.append([ bin_list_ex[i_bin][j] for j in m_ex])
    h5d_map_m_ap    = h5file_map_dset2bins_(bin_list_m_ap)
    h5d_map_m_ex    = h5file_map_dset2bins_(bin_list_m_ex)
        
    if VERBOSE :    # print_ bin stats
        bin_stat    = [ (len(bin_list_ap[i]), len(bin_list_ex[i]), len(bin_list_m_ex[i])) 
                        for i in range(n_bins) ]
        bin_stat.sort()
        bin_stat_u  = list(set(bin_stat))
        bin_stat_u.sort()
        for b in bin_stat_u :
            print("h5file_bin_unbias_general: %d bins of %d AP data match %d of %d EX data # %s" % (
                    bin_stat.count(b), b[0], b[2], b[1], time.asctime()))

    # bins
    res     = np.zeros((n_bins,) + dset_shape, dset_dtype)
    res_cnt = np.zeros((n_bins,), np.int)
    unb     = np.zeros((n_bins,) + dset_shape, dset_dtype)
    unb_cnt_ap = np.zeros((n_bins,), np.int)
    unb_cnt_ex = np.zeros((n_bins,), np.int)

    # collect AP data
    for i_dset, (h5file, h5kpath) in enumerate(in_list_ap) :
        if not i_dset in h5d_map_ap and not i_dset in h5d_map_m_ap :
            continue
        h5f         = h5py.File(h5file, 'r')
        h5d         = h5f[h5kpath]
        #h5d         = h5f[h5kpath][()]
        for i_data, i_bin in h5d_map_ap.get(i_dset, []) :
            res[i_bin] += h5d[i_data]
            res_cnt[i_bin] += 1
            #print(i_data, i_bin)
        for i_data, i_bin in h5d_map_m_ap.get(i_dset, []) :
            unb[i_bin] -= h5d[i_data]
            unb_cnt_ap[i_bin] += 1
            #print(i_data, i_bin)
        h5f.close()

    # collect EX data
    for i_dset, (h5file, h5kpath) in enumerate(in_list_ex) :
        if not i_dset in h5d_map_m_ex : continue
        h5f         = h5py.File(h5file, 'r')
        h5d         = h5f[h5kpath]
        #h5d         = h5f[h5kpath][()]
        for i_data, i_bin in h5d_map_m_ex.get(i_dset, []) :
            unb[i_bin] += h5d[i_data]
            unb_cnt_ex[i_bin] += 1
            #print(i_data, i_bin)
        h5f.close()

    # combine
    assert((unb_cnt_ex == unb_cnt_ap).all())
    for i_bin in range(n_bins) : 
        assert(len(bin_list_ap[i_bin]) == res_cnt[i_bin])
        assert(len(bin_list_m_ex[i_bin]) == unb_cnt_ex[i_bin])
        if unb_cnt_ex[i_bin] <= 0 : 
            print("WARNING: bin[%d]'%s' has no bias-corr" % (i_bin, str(key_bin_ex[i_bin])))
            res[i_bin] = res[i_bin] / res_cnt[i_bin]
        else :
            res[i_bin] = res[i_bin] / res_cnt[i_bin] + unb[i_bin] / unb_cnt_ex[i_bin]
    
    # save result
    out_h5f = h5py.File(out_h5file, mode='a')
    h5_purge_keys(out_h5f, [out_h5kpath])
    out_h5d = out_h5f.require_dataset(out_h5kpath, shape=(n_bins,) + dset_shape,
                            dtype=dset_dtype, fletcher32=True)
    out_h5d[:] = res
    for k, v in iteritems(dset_attrs_ap) :
        out_h5d.attrs[k] = h5type_attr_fix_(v)
    #if keep_datalist : 
        #for b in bin_meas_ap : 
            #h5_meas_sort(b) # sort to select representative (FIXME use `min'?)
        #binrep_meas_ap  = np.array([ a[0] for a in bin_meas_ap ], dtype=h5_meas_dtype)
        #h5_set_datalist(out_h5d, binrep_meas_ap)
    dlist_bins  = bin_meas      # alias
    if not False is keep_datalist and not None is keep_datalist: 
        if 'first' == keep_datalist :
            dlist_bins = [ mb[0] for mb in dlist_bins ]
        elif callable(keep_datalist) :
            dlist_bins = [ keep_datalist(mb) for mb in dlist_bins ]
        else : warn('bad keep_datalist option; save datalist verbatim')
        h5_set_datalist(out_h5d, np.array(dlist_bins))
    out_h5d.attrs['bincount'] = h5type_attr_fix_(res_cnt)
    out_h5d.attrs['bincount_unbias'] = h5type_attr_fix_(unb_cnt_ap)
    
    out_h5f.flush()
    out_h5f.close()

"""
# test h5_bin_general
f,k='hadspec.sl1.0e-04.proton_AAp.bx0by0bz0.5_GN2.0000x20.h5','/SP/proton_AAp'
in_list=[('hadspec96.0/%s'%f,k), ('hadspec96.96/%s'%f,k)]
h5file_bin_general('tmp.h5', k, in_list, lambda m:'%s%03d' %(m[0],m[1][3]), VERBOSE=True)
"""

