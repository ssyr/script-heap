import numpy as np
import zlib

def read_chroma_bbfile(fname, **kw):
    """ FIXME control endianness of the input file """
    f       = open(fname, "rb")
    bstr    = f.read()
    f.close()
    cksum_data = np.uint32(zlib.crc32(bstr[:-8])) 
    cksum_file = np.fromstring(bstr[-8:-4], '>u4')[0]
    if (cksum_data != cksum_file) :
        raise ValueError("%s : saved cksum %u != %u" % (
                    fname, cksum_file, cksum_data))
    id, ver = np.fromstring(bstr[-4:], '>u2')
    if (3 == ver) :
        return read_chroma_bbfile_data_v3(bstr, **kw)
    elif (4 == ver) : 
        return read_chroma_bbfile_data_v4(bstr, **kw)
    else :
        raise RuntimeError("BB format version=%d is not implemented" % ver)
    raise RuntimeError("should not reach here")
    return None


def read_chroma_bbfile_data_v3(bstr, VERBOSE=False):
    """
    unsigned short int Id;
    unsigned short int Version;
    unsigned short int Flavor;
    unsigned short int Contraction;
    unsigned short int NX;
    unsigned short int NY;
    unsigned short int NZ;
    unsigned short int NT;
    unsigned short int DecayDir;
    signed short int T1;
    signed short int T2;
    unsigned short int MaxNLinks;
    unsigned short int NLinkPatterns;
    unsigned short int NMomPerms;
    signed short int GammaInsertion;
    signed short int PX;
    signed short int PY;
    signed short int PZ;
    ADATUtil::n_uint32_t Checksum1, Checksum2;
    const signed short int   SeqSourceLen = 64;
    char SeqSource[SeqSourceLen];

    BBFile.seekEnd(12 * sizeof( unsigned short int ) + 6 * sizeof( signed short int )
                   + 64 * sizeof( signed char ) + 1 * sizeof( ADATUtil::n_uint32_t ) );

    BBFile.read(Flavor);        // -104
    BBFile.read(Contraction);   // -102
    BBFile.readArray(SeqSource, sizeof( signed char ), SeqSourceLen);   // -100
    BBFile.read(GammaInsertion);// -36
    BBFile.read(NX);            // -34
    BBFile.read(NY);            // -32
    BBFile.read(NZ);            // -30
    BBFile.read(NT);            // -28
    BBFile.read(DecayDir);      // -26
    BBFile.read(T1);            // -24
    BBFile.read(T2);            // -22
    BBFile.read(MaxNLinks);     // -20
    BBFile.read(NLinkPatterns); // -18
    BBFile.read(NMomPerms);     // -16
    BBFile.read(PX);            // -14
    BBFile.read(PY);            // -12
    BBFile.read(PZ);            // -10
    BBFile.read(Checksum1);     // -8
    BBFile.read(Id);            // -4 
    BBFile.read(Version);       // -2

    FIXME single number reads may be implemented faster?
    FIXME control endianness
    FIXME control d/f precision
    """
    flav, cnct  = np.fromstring(bstr[-104:-100], '>u2')
    seqsrc      = str(bstr[-100:-36])
    arr         = np.fromstring(bstr[-36:], '>u2')
    gamma       = np.int16(arr[-18])    # FIXME 'signed int' ; can be negative???
    latsize     = arr[-17:-13]
    t_dir       = arr[-13]
    t1,t2       = arr[-12:-10]          # FIXME 'signed int' ; can be negative???
    lp_maxlen, lp_cnt  = arr[-10:-8]
    n_qext      = arr[-8]
    psnk        = np.fromstring(bstr[-14:-8], np.int16)
    cksum       = np.fromstring(bstr[-8:-4], '>u4')[0]
    id, ver     = arr[-2:] 
    assert(0 == gamma)
    assert(1 == n_qext)
    if VERBOSE :
        print("Id=%d   Ver=%d" % (id, ver))
        print("flav = %d" % flav)
        print("cnct = %d" % cnct)
        print("seqsrc = '%s'" % seqsrc)
        print("gamma= %d" % gamma)
        print("latsize = %s" % latsize)
        print("t_dir = %s" % t_dir)
        print("t1,t2 = (%d,%d)" % (t1, t2))
        print("lp_maxlen = %d   lp_cnt = %d" % (lp_maxlen, lp_cnt))
        print("n_qext = %d" % n_qext)
        print("psnk = %s" % psnk)
        print("cksum = %d" % cksum)


    # define all constants
    n_gamma     = 16
    ndim_space  = 3
    lt          = latsize[t_dir]
    bb_data     = np.zeros((lp_cnt, n_gamma, lt), np.complex128)
    lpath_list  = []
    qext_list_all = None
    bstr_off    = 0
    for i_lp in range(lp_cnt):
        lp_len      = np.frombuffer(bstr, '>u2', count=1, offset=bstr_off)[0]
        bstr_off    += 2
        lpath       = np.frombuffer(bstr, '>u2', count=lp_len, offset=bstr_off)
        bstr_off    += 2 * lp_len
        lpath_list.append(lpath)
        for i_g in range(n_gamma) :
            qext_list = np.empty((n_qext, ndim_space), np.int)
            for i_qext in range(n_qext) :   # FIXME in this version, n_qext means n_mom_perms and should be =1; remove cycle of len=1
                qext_list[i_qext] = np.frombuffer(bstr, '>i2', 
                                           count=ndim_space, offset=bstr_off)
                bstr_off    += 2 * ndim_space
                x = np.frombuffer(bstr, '>c8', 
                                count=lt, offset=bstr_off)
                bb_data[i_lp, i_g] = x
                bstr_off    += 8 * lt
            # consistency check: all records must contain the same qext's
            if None is qext_list_all : 
                qext_list_all = qext_list
            else :
                if not np.allclose(qext_list_all, qext_list) :
                    raise RuntimeError("mismatch in qext_list")

    assert(bstr_off + 104 == len(bstr))
    return bb_data, lpath_list, qext_list_all


def read_chroma_bbfile_data_v4(bstr, VERBOSE=False):
    """ parse BB v4 [Boram Yoon]
        return bb[i_qext, i_lp, i_gamma, t]
    """

    """
void ReadBuildingBlocks_4(const std::string& BuildingBlocksFileName, std::vector<BuildingBlocks_t> &bars) {
    unsigned short int Id;
    unsigned short int Version;
    unsigned short int Flavor;
    unsigned short int Contraction;
    unsigned short int NX;
    unsigned short int NY;
    unsigned short int NZ;
    unsigned short int NT;
    unsigned short int DecayDir;
    signed short int T1;
    signed short int T2;
    unsigned short int MaxNLinks;
    unsigned short int NLinkPatterns;
    signed short int GammaInsertion;
    signed short int PX;
    signed short int PY;
    signed short int PZ;
    unsigned short int NumMoms;
    unsigned short int T;
    ADATUtil::n_uint32_t Checksum1, Checksum2;
    const signed short int   SeqSourceLen = 64;
    char SeqSource[SeqSourceLen];

    BBFile.seekEnd(12 * sizeof( unsigned short int ) + 6 * sizeof( signed short int )
                   + 64 * sizeof( signed char ) + 1 * sizeof( ADATUtil::n_uint32_t ) );
    BBFile.read(Flavor);        // -104
    BBFile.read(Contraction);   // -102
    BBFile.readArray(SeqSource, sizeof( signed char ), SeqSourceLen); // -100
    BBFile.read(GammaInsertion);// -36
    BBFile.read(NX);            // -34
    BBFile.read(NY);            // -32
    BBFile.read(NZ);            // -30
    BBFile.read(NT);            // -28
    BBFile.read(DecayDir);      // -26
    BBFile.read(T1);            // -24
    BBFile.read(T2);            // -22
    BBFile.read(MaxNLinks);     // -20
    BBFile.read(NLinkPatterns); // -18
    BBFile.read(PX);            // -16
    BBFile.read(PY);            // -14
    BBFile.read(PZ);            // -12
    BBFile.read(NumMoms);       // -10
    BBFile.read(Checksum1);     // -8
    BBFile.read(Id);            // -4
    BBFile.read(Version);       // -2

    """
    flav, cnct  = np.fromstring(bstr[-104:-100], '>u2')
    seqsrc      = str(bstr[-100:-36])
    arr         = np.fromstring(bstr[-36:], '>u2')
    gamma       = np.int16(arr[-18])    # FIXME 'signed int' ; can be negative???
    latsize     = arr[-17:-13]
    t_dir       = arr[-13]
    t1,t2       = arr[-12:-10]          # FIXME 'signed int' ; can be negative???
    lp_maxlen, lp_cnt  = arr[-10:-8]
    psnk        = np.fromstring(bstr[-16:-10], '>i2')
    n_qext      = arr[-5]
    cksum       = np.fromstring(bstr[-8:-4], '>u4')[0]
    id, ver     = arr[-2:] 
    n_mom_perms = np.fromstring(bstr[-104 - 2 * n_qext : -104], '>u2')
    assert((1 == n_mom_perms).all())
    assert(0 == gamma)
    if VERBOSE :
        print("Id=%d   Ver=%d" % (id, ver))
        print("flav = %d" % flav)
        print("cnct = %d" % cnct)
        print("seqsrc = '%s'" % seqsrc)
        print("gamma= %d" % gamma)
        print("latsize = %s" % latsize)
        print("t_dir = %s" % t_dir)
        print("t1,t2 = (%d,%d)" % (t1, t2))
        print("lp_maxlen = %d   lp_cnt = %d" % (lp_maxlen, lp_cnt))
        print("n_qext = %d" % n_qext)
        print("psnk = %s" % psnk)
        print("cksum = 0x%08x" % cksum)
        print("n_mom_perms(%d) = %s" % (len(n_mom_perms), n_mom_perms))

    # define all constants
    n_gamma     = 16
    ndim_space  = 3
    lt          = latsize[t_dir]
    bb_data     = np.zeros((n_qext, lp_cnt, n_gamma, lt), np.complex128)
    lpath_list  = []
    qext_list   = None
    bstr_off    = 0
    for i_lp in range(lp_cnt):
        lpath = None
        for i_qext in range(n_qext) :
            lp_len      = np.frombuffer(bstr, '>u2', count=1, offset=bstr_off)[0]
            bstr_off    += 2
            lpath_cur   = np.frombuffer(bstr, '>u2', count=lp_len, offset=bstr_off)
            bstr_off    += 2 * lp_len
            if None is lpath : lpath = lpath_cur
            else : 
                if lpath_cur != lpath :
                    raise RuntimeError("mismatch in lpath")
        lpath_list.append(lpath)

        for i_g in range(n_gamma) :
            qext_list_cur = np.empty((n_qext, ndim_space), np.int)
            for i_qext in range(n_qext) :
                assert(1 == n_mom_perms[i_qext])
                #for i_mom_perm in range(n_mom_perms[i_qext]) : # XXX no cycle
                qext_list_cur[i_qext] = np.frombuffer(bstr, '>i2',
                                count=ndim_space, offset=bstr_off)
                bstr_off    += 2 * ndim_space

                x = np.frombuffer(bstr, '>c8', 
                                count=lt, offset=bstr_off)
                bb_data[i_qext, i_lp, i_g] = x
                bstr_off    += 8 * lt
            # consistency check: all records must contain the same qext's
            if None is qext_list : qext_list = qext_list_cur
            else :
                if not np.allclose(qext_list, qext_list_cur) :
                    raise RuntimeError("mismatch in qext_list")

    assert(bstr_off + 104 + 2 * n_qext == len(bstr))
    return bb_data, lpath_list, qext_list



def runme_test_read_list(files) :
    for i,f in enumerate(files) :
        print("%5d\t%s" %(i, f))
        x,lp,ql = parse_bbfile(f)

