from future.utils import iteritems
from past.builtins import basestring
import numpy as np
import scipy as sp ; import scipy.stats
import h5py
import lhpd
from ..misc.numpy_extra import *
from lhpd.aff_io import aff_key_bb_linkpath_str
from lhpd.pymath.H4_tensor import *
from lhpd.pymath.gamma_matr import Gamma16_dgr, gamma_dgr, gamma_id, gamma_dot, proj_Gamma16
from lhpd.pymath.tensor import tensor_matrix_dot, matrix_tensor_dot
from lhpd.latcorr.ff_coeff import get_gff_number, ff_matrix_f2f
import itertools as it
from lhpd.limits import *
from .. import h5_io

# using De Grand-Rossi basis throughout
Gamma16 = Gamma16_dgr
gamma   = gamma_dgr
gamma5  = Gamma16[15]

# create common polarization matrix names
tpol_map = {}

# spins Sigma_i
tpol_map['Sx']  = -1j * np_ddot(gamma[1], gamma[2])
tpol_map['Sy']  = -1j * np_ddot(gamma[2], gamma[0])
tpol_map['Sz']  = -1j * np_ddot(gamma[0], gamma[1])
tpol_map['Sx4'] = -1j * np_ddot(gamma[1], gamma[2], gamma[3])
tpol_map['Sy4'] = -1j * np_ddot(gamma[2], gamma[0], gamma[3])
tpol_map['Sz4'] = -1j * np_ddot(gamma[0], gamma[1], gamma[3])
for k in ['Sx', 'Sy', 'Sz', 'Sx4', 'Sy4', 'Sz4' ] :
    tpol_map["%splus" % k]  = gamma_id + tpol_map[k]
    tpol_map["%sminus" % k] = gamma_id - tpol_map[k]

# add parity projections
tpol_pos    = 0.5 * (gamma_id + gamma[3])
tpol_neg    = 0.5 * (gamma_id - gamma[3])
for k in list(tpol_map.keys()) : 
    tpol_map["pos%s" % k] = np_ddot(tpol_pos, tpol_map[k])
    tpol_map["neg%s" % k] = np_ddot(tpol_neg, tpol_map[k])
tpol_map['id']          = gamma_id
tpol_map["pos"]         = tpol_pos
tpol_map["neg"]         = tpol_neg

# add gamma_5'ed spinors
for k in list(tpol_map.keys()) : 
    tpol_map["%s5" % k] = np_ddot(tpol_map[k], gamma5)
# Gamma(n)
for g in range(16) : 
    tpol_map["Tg%d" % g] = Gamma16[g]

# NME spinors : unpolarized
tpol_map['I']   = gamma_id
tpol_map['S']   = gamma_id
tpol_map['P']   = gamma5
# NME spinors : polarized
tpol_map['A1']  = np_ddot(gamma[0], gamma5)
tpol_map['A2']  = np_ddot(gamma[1], gamma5)
tpol_map['A3']  = np_ddot(gamma[2], gamma5)
for k in ['I', 'A1', 'A2', 'A3'] :
    tpol_map["pos_%s" % k] = np_ddot(tpol_pos, tpol_map[k])
    tpol_map["neg_%s" % k] = np_ddot(tpol_neg, tpol_map[k])
    

# aliases used in some projects
tpol_map['AApSzplus']   = tpol_map['posSzplus']
"""
# test
from lhpd.latcorr import tpol_map, get_tpol_matr, calc_tpol_fromGamma, Gamma16
for k in tpol_map.keys() : print(k, (calc_tpol_fromGamma(k, lambda g:Gamma16[g]) == get_tpol_matr(k)).all())
"""

def get_tpol_matr(tpol) : return tpol_map[tpol]
def get_tpol_name(tpol, default='unknown') :
    for k,v in iteritems(tpol_map) : 
        if (v == tpol).all() : return k
    return default
def tpol_asmatr(tpol):
    if   isinstance(tpol, int) : 
        return getG(tpol)
    elif isinstance(tpol, basestring) : 
        return get_tpol_matr(tpol)
    else : 
        tpol = np.asarray(tpol)
        assert((4,4) == tpol.shape)
        return tpol

def calc_tpol_fromGamma(tpol, getG) :
    """ codified names for various polarizations """
    tpol = tpol_asmatr(tpol)
    res = None
    for g, v in enumerate(proj_Gamma16(tpol, Gamma16_dgr)) :
        if abs(v) < NONZERO_TOL : continue
        if None is res : res = v * getG(g)
        else : res = res + v * getG(g)

    if None is res : return 0
    else : return res


# all functions returning tuples
def calc_vec_fromGamma(getG)            : return (getG(1), getG(2), getG(4), getG(8))
def calc_axialvec_fromGamma(getG)       : return (getG(14), -getG(13), getG(11), -getG(7))
def calc_scalar_fromGamma(getG)         : return (getG(0),)
def calc_pseudoscalar_fromGamma(getG)   : return (getG(15),)

def calc_sigma2a2_H4d6r1_fromGamma(getG): 
    """ select sigma[mu,nu] tensor from Gamma(n) and return it as 6 components
        of the H4_T2_d6r1 representation; NOTE the sqrt(2) factor """
    fc  = np.sqrt(2.)*1j    # I*(norm. factor for antisymmetric flatindex)
    return (fc*getG( 3), fc*getG( 5), fc*getG( 9),  # xy xz xt
                         fc*getG( 6), fc*getG(10),  #    yz yt
                                      fc*getG(12))  #       zt
def calc_sigmatensor_T2_fromGamma(getG) :
    """ select sigma[mu,nu] tensor from Gamma(n) and return it as double tuple 
        ("rank=2 tensor") 
        each component is an independent ndarray object """
    xy,xz,xt,yz,yt,zt = (1j*getG( 3), 1j*getG( 5), 1j*getG( 9), # xy xz xt
                                      1j*getG( 6), 1j*getG(10), #    yz yt
                                                   1j*getG(12)) #       zt
    return ( (   0,  xy,  xz,  xt ),
             ( -xy,   0,  yz,  yt ),
             ( -xz, -yz,   0,  zt ),
             ( -xt, -yt, -zt,   0 ) )
    
def calc_op_from_bb(op, ir_name, get_bb_G_lp) :
    def get_bb_G_lp0(gN) : return get_bb_G('l0_', gN)
    if   'tensor0' == op :      # TODO for scalar density, also need the usual vacuum avg; that would be q=0
        if 'H4_T0_d1r1' == ir_name : 
            return np.asarray(lhpd.latcorr.calc_scalar_fromGamma(get_bb_G_lp0))
        else : raise ValueError((op, ir_name))
    elif 'pstensor0' == op : 
        if 'H4_T0_d1r1' == ir_name :
            return np.asarray(lhpd.latcorr.calc_pseudoscalar_fromGamma(get_bb_G_lp0))
        else : raise ValueError((op, ir_name))
    elif 'tensor1' == op : 
        if 'H4_T1_d4r1' == ir_name :
            return np.asarray(lhpd.latcorr.calc_vec_fromGamma(get_bb_G_lp0))
        else : raise ValueError((op, ir_name))
    elif 'pstensor1' == op :
        if 'H4_T1_d4r1' == ir_name :
            return np.asarray(lhpd.latcorr.calc_axialvec_fromGamma(get_bb_G_lp0))
        else : raise ValueError((op, ir_name))
    elif 'sigma2a2' == op :
        if 'H4_T2_d6r1' == ir_name :
            return np.asarray(lhpd.latcorr.calc_sigma2a2_H4d6r1_fromGamma(get_bb_G_lp0))
        else : raise ValueError((op, ir_name))

    # TODO implement operators with derivatives
    else : raise NotImplemented((op, ir_name))
    raise RuntimeError('not reachable')



def calc_flav_fromUD(flav, getUD) :
    """ 
        getUD   function flav -> data
    """
    if 'U' == flav or 'D' == flav : return getUD(flav)
    elif 'U-D' == flav  : return (getUD('U') - getUD('D'))
    elif 'U+D' == flav  : return (getUD('U') + getUD('D'))
    elif 'P' == flav    : return (2./3.*getUD('U') - 1./3.*getUD('D'))
    elif 'N' == flav    : return (2./3.*getUD('D') - 1./3.*getUD('U'))
    else : raise ValueError(flav)

def compose_flav(flav, getUDS) :
    """ 
        getUD   function flav -> data
    """
    if 'U' == flav or 'D' == flav or 'S' == flav : return getUDS(flav)
    elif 'U-D' == flav  : return (getUDS('U') - getUDS('D'))
    elif 'U+D' == flav  : return (getUDS('U') + getUDS('D'))
    elif 'P' == flav    : return (2./3.*getUDS('U') - 1./3.*getUDS('D'))
    elif 'N' == flav    : return (2./3.*getUDS('D') - 1./3.*getUDS('U'))
    else : raise ValueError(flav)


def op_default_ir_list(op) :
    """ for debug and initial analysis only 
        set all renorm to 1.
    """
    if op in ['tensor0', 'pstensor0'] : return [('H4_T0_d1r1', 1.)]
    elif op in ['tensor1', 'pstensor1'] : return [('H4_T1_d4r1', 1.)]
    elif op in ['tensor2s2', 'pstensor2s2' ] : 
        return [('H4_T2_d3r1', 1.), ('H4_T2_d6r3', 1.)]
    elif op in ['tensor3s3', 'pstensor3s3' ] :
        return [('H4_T3_d4r2', 1.), ('H4_T3_d8r1_1', 1.)]
    elif op in ['sigma2a2'] : 
        return [('H4_T2_d6r1', 1.)]
    else : raise ValueError(op)

def op_default_ir_list_aff(op) :
    """ for debug and initial analysis only ; 
        correct the lhpc-data normalization to match Gockeler:1996mu,
        apart from that, set all renorm to 1.
    """
    if op in ['tensor0', 'pstensor0'] : return [('H4_T0_d1r1', 1.)]
    elif op in ['tensor1', 'pstensor1'] : return [('H4_T1_d4r1', 1.)]
    elif op in ['tensor2s2', 'pstensor2s2' ] : 
        return [('H4_T2_d3r1', 1.), ('H4_T2_d6r3', math.sqrt(2.))]
    elif op in ['tensor3s3', 'pstensor3s3' ] :
        return [('H4_T3_d4r2', math.sqrt(6.)), ('H4_T3_d8r1_1', 1.)]
    else : raise ValueError(op)

def ir_comp_len(ir) :    return H4_repr_dim[ir]

def ir_list_comp_len(ir_list) : 
    return sum([ H4_repr_dim[ir] for ir, ir_scale in ir_list ])

def ir_list_comp_list(ir_list) :
    """ return list of components (str) """
    return sum([H4_repr_comp[ir[0]] for ir in ir_list ], [])

# momenta definitions suitable for form factor calculations
# XXX momenta are defined as linear in wave number
# 
def lorentz_energy(m, latsize, p3) :
    return math.sqrt(m**2 + lhpd.mom2_cont(latsize[:3], p3[:3]))

def lorentz_mom_transfer_asym(m1, m2, latsize, p3_1, p3_2) :
    p3_1    = np.asarray(p3_1)
    p3_2    = np.asarray(p3_2)
    latsize = np.asarray(latsize)
    return np.r_[ (2 * np.pi / latsize[:3]) * (p3_2 - p3_1),
                  lorentz_energy(m2, latsize, p3_2) - lorentz_energy(m1, latsize, p3_1) ]
def lorentz_mom_transfer(m, latsize, p3_1, p3_2) :  
    return lorentz_mom_transfer_asym(m, m, latsize, p3_1, p3_2)

def lorentz_mom_transfer_asym_sq(m1, m2, latsize, p3_1, p3_2) :
    """ return Q^2 = (qvec**2 - q4**2) """
    q = lorentz_mom_transfer_asym(m1, m2, latsize, p3_1, p3_2)
    return (q[:3]**2).sum() - q[3]**2

def lorentz_mom_transfer_sq(m, latsize, p3_1, p3_2) :
    return lorentz_mom_transfer_asym_sq(m, m, latsize, p3_1, p3_2)

def lorentz_p4_mink(m, latsize, p3) :
    p3 = np.asarray(p3)
    latsize = np.asarray(latsize)
    return np.r_[(2 * np.pi / latsize[:3]) * p3, lorentz_energy(m, latsize, p3)]
lorentz_p4 = lorentz_p4_mink

#def deriv_ft_bcc2pt(q, ls) :
#    """ compute matrix 2-point body-centered div J_mu
#        return cplx[..., mu] 
#    """
#    assert(1 == len(ls.shape))
#    dim = len(ls)
#    assert(q.shape[-1] == dim)
#    exp_mq  = np.exp( (-2j * np.pi / ls) * q)    # cplx[i_mc, i_comp3]
#    # real[..., mu]
#    div_bcc2pt = np.ones_like(exp_mq)
#    for mu in range(dim) :
#        for nu in range(dim) : 
#            if mu == nu : div_bcc2pt[..., mu] *= (exp_mq[..., nu] - 1)
#            else :        div_bcc2pt[..., mu] *= 0.5 * (exp_mq[..., nu] + 1)
#    return div_bcc2pt
#
#def avg_ft_bcc2pt(q, ls) :
#    """ compute matrix 2-point body-centered average
#        q[..., mu]  momentum (int)
#        ls          lattice size
#        return cplx[...] 
#    """
#    assert(1 == len(ls.shape))
#    dim = len(ls)
#    assert(q.shape[-1] == dim)
#    exp_mq  = np.exp( (-2j * np.pi / ls) * q)    # cplx[i_mc, i_comp3]
#    return (0.5 + 0.5 * exp_mq).prod(-1)

def deriv_ft_bcc2pt(q, ls) :
    """ compute matrix 2-point body-centered div J_mu
        return cplx[..., mu] 
    """
    assert(1 == len(ls.shape))
    dim = len(ls)
    assert(q.shape[-1] == dim)
    ph_hq   = (np.pi / ls) * q     # real[i_mc, i_comp3]
    sin_hq  = np.sin(ph_hq)             # real[i_mc, i_comp3]
    cos_hq  = np.cos(ph_hq)             # real[i_mc, i_comp3]
    # real[..., mu]
    div_bcc2pt = np.ones_like(ph_hq, dtype=np.complex128)
    for mu in range(dim) :
        for nu in range(dim) : 
            if mu == nu : div_bcc2pt[..., mu] *=  -2j * sin_hq[..., nu]
            else :        div_bcc2pt[..., mu] *=        cos_hq[..., nu]
    return div_bcc2pt

def avg_ft_bcc2pt(q, ls) :
    """ compute matrix 2-point body-centered average
        q[..., mu]  momentum (int)
        ls          lattice size
        return cplx[...] 
    """
    assert(1 == len(ls.shape))
    dim = len(ls)
    assert(q.shape[-1] == dim)
    ph_hq  = (np.pi / ls) * q      # real[i_mc, i_comp3]
    avg_bcc2pt = np.cos(ph_hq).prod(-1)
    return avg_bcc2pt

def deriv_ft_linear(q, ls) :
    """ compute matrix 2-point body-centered div J_mu
        return cplx[..., mu] 
    """
    assert(1 == len(ls.shape))
    dim = len(ls)
    assert(q.shape[-1] == dim)
    return ((-2j * np.pi / ls) * q).sum(-1)

#def lorentz_energy_sin(m, latsize, p3) :
    #return math.sqrt
#def lorentz_q4_sin(mass_in, mass_out, latsize, p3_in, p3_out) :
    #en_in = 

    
# FIXME the same tpol is assumed for 2pt and 3pt functions
#  if Tr[tpol]=0, the 2pt function is zero in the denom
#  solution: separate 2pt and 3pt traces and divide one by the other later
if False :
    def ff_eqnmat_precond_f2f_v1(op, mass, latsize, tpol_list, mcgrp, ir_list) : 
        """ generate ff eq.matrix for all tpol, mc=(p3src,p3snk), ir=(ir_name, ir_scale)
            indexing is the direct product : flat([i_tpol, i_mc, i_ir, i_comp, i_reim])
            where i_comp is # of component for ir_list[i_ir], i_reim=0,1 for Real/Imag
                tpol_list   list of spin matrices to include (4x4 cplx; FIXME : extend to use tpol names?)
                mcgrp     list of (in,out) 3-momenta combinations
                ir_list     list of irreps to include ; the ir_scale is ignore
            XXX twisted momenta: twisting angles should be added to mc[0], mc[1]
        """

        n_ff    = lhpd.latcorr.get_gff_number(op)
        n_tpol  = len(tpol_list)
        n_mc    = len(mcgrp)
        n_comp  = ir_list_comp_len(ir_list)
        sh_3pt  = (n_tpol, n_mc, n_comp, 2)
        n_c3pt  = n_tpol * n_mc * n_comp * 2
        
        # momenta
        def make_mom_std(p) : 
            p = np.abs(p)
            p.sort()
            return p
        mc_std_list = np.array([ [ make_mom_std(mc[0]), make_mom_std(mc[1]) ]
                                 for mc in mcgrp ])
        q2_list     = np.array([ lorentz_mom_transfer_sq(mass, latsize, mc[0], mc[1]) 
                                 for mc in mcgrp ])
        # sanity check to make sure that all ffs in the system correspond to the same Q2
        assert((np.abs(q2_list - q2_list[0]) < NONZERO_TOL).all())
        
        # separate Re/Im
        eq_mat = np.empty(sh_3pt + (n_ff,), np.float64)
        for i_tpol, tpol in enumerate(tpol_list) :
            for i_mc, mc in enumerate(mcgrp) :
                p3src, p3snk = mc
                p4_in   = lorentz_p4(mass, latsize, p3src)
                p4_out  = lorentz_p4(mass, latsize, p3snk)
                #q4      = lorentz_q4(mass_in, mass_out, latsize, p3src, p3snk)
                mat = lhpd.latcorr.ff_matrix_f2f(mass, p4_in, tpol, mass, p4_out, tpol, op, tpol)
                
                ir_sh = 0
                for (ir_name, ir_scale) in ir_list:
                    ir_dim = H4_repr_dim[ir_name]
                    mat_ir = H4_repr_func[ir_name](mat)
                    #print(ir_name, mat_ir)
                    eq_mat[i_tpol, i_mc, ir_sh : ir_sh + ir_dim, 0] = mat_ir.real
                    eq_mat[i_tpol, i_mc, ir_sh : ir_sh + ir_dim, 1] = mat_ir.imag
                    ir_sh += ir_dim
                assert(ir_sh == n_comp)

        # dyn.create sparse precond matrix : pc[i_row][i_nonzero_elem]
        pc_coeff = []          # [i_eqn][j] -> coeff from eq_mat
        pc_i3pt  = []          # [i_eqn][j] -> idx_3pt from eq_mat
        pc_norm2 = []          # [i_eqn][j] -> norm(eq_mat[i_3pt])

        n_eqn = 0
        for idx_3pt in np.ndindex(n_tpol, n_mc, n_comp, 2) :
            (i_tpol, i_mc, i_comp, i_c) = idx_3pt
            n2_i = (eq_mat[idx_3pt]**2).sum()
            if n2_i <= NONZERO_TOL : continue # drop zero equations

            coeff = None
            for k_eqn in range(n_eqn) :
                assert(0 < len(pc_i3pt[k_eqn]))
                idx_3pt_k   = pc_i3pt[k_eqn][0]
                (k_tpol, k_mc, k_comp, k_c) = idx_3pt_k
                xy  = (np.dot(eq_mat[idx_3pt], eq_mat[idx_3pt_k])  
                            / math.sqrt(n2_i * pc_norm2[k_eqn][0]))
                # FIXME hardcoded here is the condition for merging equations; how to make it flexible?
                if (    k_c == i_c                                          # both Re or Im
                    and (mc_std_list[i_mc] == mc_std_list[k_mc]).all()      # (in,out) momenta are equivalent
                    and lhpd.rdiff(abs(xy), 1.) < NONZERO_TOL               # angle is 0 or Pi
                    and lhpd.rdiff(n2_i, pc_norm2[k_eqn][0]) < NONZERO_TOL  # norm is the same
                        ) : 
                    coeff = xy
                    print("# old pcmat row idx_3pt=", idx_3pt, " ->", idx_3pt_k, " (x%.1f)" % coeff)
                    pc_i3pt[k_eqn].append(idx_3pt)
                    pc_coeff[k_eqn].append(coeff)
                    pc_norm2[k_eqn].append(n2_i)
                    break
                
            if None is coeff :
                print("# new pcmat row idx_3pt=", idx_3pt)
                pc_i3pt.append([idx_3pt])
                pc_coeff.append([1.])
                pc_norm2.append([n2_i])
                n_eqn += 1

        assert(0 < n_eqn)
        assert( len(pc_i3pt) == n_eqn
            and len(pc_coeff) == n_eqn
            and len(pc_norm2) == n_eqn)

        pc_mat = np.zeros(sh_3pt + (n_eqn,), np.float64)
        for k_eqn in range(n_eqn) : 
            for j in range(len(pc_coeff[k_eqn])) : 
                pc_mat[pc_i3pt[k_eqn][j]][k_eqn] = pc_coeff[k_eqn][j]

        #return eq_mat.reshape(n_c3pt, n_ff), pc_mat.reshape(n_c3pt, n_eqn)
        return eq_mat, pc_mat

def mom3_str(p) :
    def sign_int(x, fmt='%d'):
        if 0 < x : return '+%s' % (fmt % x)
        elif 0 == x : return ' %s' % (fmt % x)
        else : return '-%s' %(fmt % -x)
    return ','.join([ sign_int(x, fmt='%1d') for x in p ])

def mom3_texstr(p) :
    def sign_int(x, fmt='%d'):
        if 0 < x : return '+%s' % (fmt % x)
        elif 0 == x : return ' %s' % (fmt % x)
        else : return '-%s' %(fmt % -x)
    return r'\,'.join([ '%d' %x for x in p ])

def matrix_elem_str_f2f_op(p3src, p3snk, tpol, op, ir, comp, reim) :
    # TODO flavor?
    tpol_str = tpol # TODO convert+abbreviate
    assert(isinstance(tpol_str, str))
    # FIXME only specific cases maintained
    op_str, ir_str, comp_str, reim_str = '', '', '', ''
    op_str = ({
        'tensor0' : 'S',  'pstensor0' : 'P',
        'tensor1' : 'V',  'pstensor1' : 'A',
        'sigma2a2' : 'T',
        #'tensor2s2' : 'EM',  'pstensor1' : 'tEM', # FIXME give names to rank-2 ops
        })[op]
    if not None is ir : ir_str = ir
    if not None is comp :
        if isinstance(comp, str) : comp_str = comp
        elif isinstance(comp, int) : comp_str = H4_repr_comp[ir][comp]
        else : raise ValueError(comp)
    if not None is reim : reim_str = (['Re', 'Im'])[reim] + ' '

    return "%sT{%s}<%s|%s_%s%s|%s>" %(reim_str, tpol_str,
            mom3_str(p3snk), op_str, ir_str, comp_str, mom3_str(p3src))
def matrix_elem_texstr_f2f_op(p3src, p3snk, tpol, op, ir, comp, reim) :
    # TODO flavor?
    tpol_str = tpol # TODO convert+abbreviate
    # TODO move to a separate func
    tol_texstr_map = {
        'id' : '' , 'pos' : r'T^+', 'neg' : r'T^-',
        'posSx' : r'T^+ \Sigma_x', 'posSy' : r'T^+ \Sigma_y', 'posSz' : r'T^+ \Sigma_z',
        'posSxplus'     : r'T^+ \Sigma_{x+}',    'posSxminus'    : r'T^+ \Sigma_{x-}',
        'posSyplus'     : r'T^+ \Sigma_{y+}',    'posSyminus'    : r'T^+ \Sigma_{y-}',
        'posSzplus'     : r'T^+ \Sigma_{z+}',    'posSzminus'    : r'T^+ \Sigma_{z-}',
    }
    try : tpol_str = tol_texstr_map[tpol]
    except KeyError : tpol_str = r'\mathrm{T[%s]}' % tpol

    assert(isinstance(tpol_str, str))
    # FIXME only specific cases maintained
    op_str, ir_str, comp_str, reim_str = '', '', '', ''
    op_str = ({
        'tensor0' : 'S',  'pstensor0' : 'P',
        'tensor1' : 'V',  'pstensor1' : 'A',
        'sigma2a2' : 'T',
        #'tensor2s2' : 'EM',  'pstensor1' : 'tEM', # FIXME give names to rank-2 ops
        })[op]
    if not None is ir : ir_str = ir
    if not None is comp :
        if isinstance(comp, str) : comp_str = comp
        elif isinstance(comp, int) : comp_str = H4_repr_comp[ir][comp]
        else : raise ValueError(comp)
    if not None is reim : reim_str = (['Re', 'Im'])[reim] + ' '

    return r'\mathrm{%s} %s \langle %s|%s_%s%s|%s \rangle' %(reim_str, tpol_str,
            mom3_texstr(p3snk), op_str, ir_str, comp_str, mom3_texstr(p3src))


def matrix_elem_str(mc, ins_d) :
    """
        mc      "mom.combo"=in/out states
        ins_d   operator insertion description
    """
    if   'op' == ins_d['kind'] :
        if 'reim' in ins_d : reim_str = (['Re', 'Im'])[ins_d['reim']]

        return matrix_elem_str_f2f_op(mc.get_p3src(), mc.get_p3snk(), mc.get_tpol(), 
                    ins_d['op'], ins_d.get('ir'), ins_d.get('comp'), ins_d.get('reim'))
    # FIXME only specific cases maintained
    else : raise ValueError(ins_d['kind'])


def print_ff_eqnmat_f2f(op, tpol_tag_list, mcgrp, ir_list, eqn_mat, pc_mat) :
    n_tpol  = len(tpol_tag_list)
    n_mc    = len(mcgrp)
    n_comp  = ir_list_comp_len(ir_list)
    n_ff    = lhpd.latcorr.get_gff_number(op)
    sh_3pt  = (n_tpol, n_mc, n_comp, 2)
    n_eqn   = pc_mat.shape[-1]
    assert(eqn_mat.shape == sh_3pt + (n_ff,))
    assert(pc_mat.shape  == sh_3pt + (n_eqn,))

    ir_comp_str = []
    for ir_name, ir_scale in ir_list : 
        ir_comp_str.extend(H4_repr_comp[ir_name])
    def idx3pt_str(idx_3pt):
        i_tpol, i_mc, i_comp, i_c = idx_3pt
        return "%s[{%s}*<%s|%s_%s|%s>]" %(
                ['Re', 'Im'][i_c], tpol_tag_list[i_tpol], 
                mom3_str(mcgrp[i_mc][1]), op, ir_comp_str[i_comp], 
                mom3_str(mcgrp[i_mc][0]))

    # sort by i_eqn : the equations with the same i_eqn are added together
    for i_eqn in range(n_eqn) :
        for idx_3pt_a in np.asarray(np.where(0 != pc_mat[..., i_eqn])).T :
            idx_3pt = tuple(idx_3pt_a)
            print('[%3d] (%+.1f*)  %s = %s*FF' % (
                    i_eqn, pc_mat[idx_3pt][i_eqn], 
                    idx3pt_str(idx_3pt), str(eqn_mat[idx_3pt])))

def pltx_trange(tsep, max_tslice) :
    t1, t2 = 0, tsep + 1
    while max_tslice < (t2 - t1) :
        t1 += 1
        t2 -= 1
    return (t1, t2)


def make_mom_combo_list(m, latsize, psnk_list, q2ext_list) :
    """ generate a list of mom.combinations
    """
    """ test as 
make_mom_combo_list(.5, latsize, [[0,0,0], [-1,0,0]], list(lhpd.int_vectors_maxnorm(3, 10)))
    """
    mom_combo_list = []
    momsq_list     = []
    for psnk in psnk_list :
        for qext in q2ext_list : #lhpd.int_vectors_maxnorm(3, q2ext_max) :
            psnk = np.asarray(psnk)
            qext = np.asarray(qext)
            psrc = psnk - qext
            mom_combo_list.append((psrc, psnk))
            q2   = lorentz_mom_transfer_sq(m, latsize, psrc, psnk)
            momsq_list.append(q2)

    momsq_list = np.asarray(momsq_list)
    
    #return momsq_list
    momsq_map  = {}
    i = 0
    while i < len(momsq_list) :
        if (not np.isnan(momsq_list[i])) :
            q2 = momsq_list[i]
            il_q2 = lhpd.np_filter_arg(lambda x : abs(x - q2) <= 1e-6*(abs(x) + abs(q2)) , 
                                       momsq_list)
            momsq_map[q2] = [ mom_combo_list[j] for j in il_q2 ]
            momsq_list[il_q2] = np.nan
        i += 1
    return momsq_map

def make_ff_eqnmat_precond_f2f(op, mlat, latsize, mcgrp, ir_list) :
    """ generate ff eq.matrix for all tpol, mc=(p3src,p3snk), ir=(ir_name, ir_scale)
        indexing is the direct product : flat([i_tpol, i_mc, i_ir, i_comp, i_reim])
        where i_comp is # of component for ir_list[i_ir], i_reim=0,1 for Real/Imag
            tpol_list   list of spin matrices to include (4x4 cplx; FIXME : extend to use tpol names?)
            mcgrp       list of mc_case objects containing p3src, p3snk, tpol
            ir_list     list of irreps to include ; the ir_scale is ignore
        XXX twisted momenta: twisting angles should be added to mc[0], mc[1]

        NOTE ! preconditioning only over in*out states with equivalent momenta and tpol; 
                otherwise, the 2pt function may be different
    """         
                
    n_ff    = get_gff_number(op) 
    n_mc    = len(mcgrp)
    n_comp  = ir_list_comp_len(ir_list)
    # separate Re/Im
    sh_3pt  = (n_mc, n_comp, 2)
    n_c3pt  = n_mc * n_comp * 2
                         
    # momenta
    def make_mom_std(p) : 
        p = np.abs(p) 
        p.sort()
        return p

    mc_std_list = np.array([ ( make_mom_std(mc.get_p3src()), make_mom_std(mc.get_p3snk()))
                             for mc in mcgrp ])
    q2_list     = np.array([ lorentz_mom_transfer_sq(mlat, latsize, mc.get_p3src(), mc.get_p3snk()) 
                             for mc in mcgrp ])
    # sanity check to make sure that all ffs in the system correspond to the same Q2
    assert((np.abs(q2_list - q2_list[0]) < NONZERO_TOL).all())
    
    eq_mat = np.empty(sh_3pt + (n_ff,), np.float64)
    for i_mc, mc in enumerate(mcgrp) :
        p4_in   = lorentz_p4(mlat, latsize, mc.get_p3src())
        p4_out  = lorentz_p4(mlat, latsize, mc.get_p3snk())
        # FIXME pass separate matrix for 2pt, for cases when Tr[tpol]==0
        mat_mc  = ff_matrix_f2f(
                        mlat, p4_in,  mc.get_tpol_c2_matr(), 
                        mlat, p4_out, mc.get_tpol_c2_matr(),
                        op, mc.get_tpol_matr())
        
        ir_sh = 0
        for (ir_name, ir_scale) in ir_list:
            ir_dim = H4_repr_dim[ir_name]
            mat_mc_ir = H4_repr_func[ir_name](mat_mc)
            #print(ir_name, mat_ir)
            eq_mat[i_mc, ir_sh : ir_sh + ir_dim, 0] = mat_mc_ir.real
            eq_mat[i_mc, ir_sh : ir_sh + ir_dim, 1] = mat_mc_ir.imag
            ir_sh += ir_dim
        assert(ir_sh == n_comp)

    # dyn.create sparse precond matrix : pc[i_row][i_nonzero_elem]
    pc_coeff = []          # [i_eqn][j] -> coeff from eq_mat
    pc_i3pt  = []          # [i_eqn][j] -> idx_3pt from eq_mat
    pc_norm2 = []          # [i_eqn][j] -> norm(eq_mat[i_3pt])

    n_eqn = 0
    for idx_3pt in np.ndindex(n_mc, n_comp, 2) :
        (i_mc, i_comp, i_c) = idx_3pt
        n2_i = (eq_mat[idx_3pt]**2).sum()
        if n2_i <= NONZERO_TOL : continue # drop zero equations

        coeff = None
        for k_eqn in range(n_eqn) :
            assert(0 < len(pc_i3pt[k_eqn]))
            idx_3pt_k   = pc_i3pt[k_eqn][0]
            (k_mc, k_comp, k_c) = idx_3pt_k
            xy  = (np.dot(eq_mat[idx_3pt], eq_mat[idx_3pt_k])
                        / math.sqrt(n2_i * pc_norm2[k_eqn][0]))
            # FIXME hardcoded here is the condition for merging equations; how to make it flexible?
            if (    k_c == i_c                                          # both Re or Im
                and (mc_std_list[i_mc] == mc_std_list[k_mc]).all()      # (in,out) momenta are equivalent
                and lhpd.rdiff(abs(xy), 1.) < NONZERO_TOL               # angle is 0 or Pi
                and lhpd.rdiff(n2_i, pc_norm2[k_eqn][0]) < NONZERO_TOL  # norm is the same
                # FIXME does averaging different polarizations make NO sense? 
                #and mcgrp[i_mc].get_tpol() == mcgrp[k_mc].get_tpol()
                    ) :
                coeff = xy
                #print("# old pcmat row idx_3pt=", idx_3pt, " ->", idx_3pt_k, " (x%.1f)" % coeff)
                pc_i3pt[k_eqn].append(idx_3pt)
                pc_coeff[k_eqn].append(coeff)
                pc_norm2[k_eqn].append(n2_i)
                break

        if None is coeff :
            #print("# new pcmat row idx_3pt=", idx_3pt)
            pc_i3pt.append([idx_3pt])
            pc_coeff.append([1.])
            pc_norm2.append([n2_i])
            n_eqn += 1

    assert(0 < n_eqn)
    assert( len(pc_i3pt) == n_eqn
        and len(pc_coeff) == n_eqn
        and len(pc_norm2) == n_eqn)

    pc_mat = np.zeros(sh_3pt + (n_eqn,), np.float64)
    for k_eqn in range(n_eqn) :
        for j in range(len(pc_coeff[k_eqn])) :
            pc_mat[pc_i3pt[k_eqn][j]][k_eqn] = pc_coeff[k_eqn][j]

    # [i_mc, n_comp, 2, i_ff], [i_mc, i_comp, 2, i_eqn]
    return eq_mat, pc_mat

# for exp fits etc
def c3fit_dexp_2state(de_a, t_a, de_b, t_b) :
    """ compute nexp-state "system matrix" (nucleon overlap Zs are divided out),
            dexp(t_a,t_b, i_p) = c3pt(t_a,t_b,i_p) / (c2pt0_a(t_a)/sqrt(c0a) * c2pt0_b(t_b)/sqrt(c0b))
        * must be summed in axis=-1 with nucleon matrix elements 
            me[i,j] = c3param[i,j] / sqrt(c2pt_a[0]*c2pt_b[0])
          (nucleon overlap Zs are ALSO divided out in me[i,j])
        * normalized to give ground-state matrix element
        return Real [i_data, i_t(t_a,t_b), i_c3_p] """
    assert(de_a.shape == de_b.shape)
    assert(t_a.shape == t_b.shape)
    n_p = 4 # 2 states
    dexp_ttau_r = np.empty(de_b.shape + (t_a.shape[-1], n_p,))
    de_a = de_a[..., None]
    de_b = de_b[..., None]
    dexp_ttau_r[..., 0] = 1.    # sic! - no factor for <0|0> 
    dexp_ttau_r[..., 1] = np.exp(-de_a * t_a)                  # <0|1>
    dexp_ttau_r[..., 2] = np.exp(-de_b * t_b)                  # <1|0>
    dexp_ttau_r[..., 3] = np.exp(-de_a * t_a -de_b * t_b)      # <1|1>
    return dexp_ttau_r

# pdecay form factors
def c2fit_dexp_func(c1d0, de, t) :
    """ [..., t] """
    return  1. + np.asarray(c1d0)[..., None] * np.exp( -np.asarray(de)[..., None] * np.asarray(t))




def calc_ff(gg, mcgrp, ssgrp, op, ir_list, mlat, latsize,
            method_list, 
            **kw) :
    """ this is a long ugly function to compute formfactors;
        temporary implementation, will be organized into subfunctions as will be seen fit
        (although likely to stay forever)

        method_list = [ (name, param1, ...), ...]
                    methods to extract the ground state

        func_get_op     (ama_mode, had, flav, op, ir_list, p3src, p3snk, tsep) -> c3pt[i_data,tau,i_comp]
        func_get_c2pt   (ama_mode, had, p3) -> c2pt[i_data, t]

        return formfactors[data][i_method][i_ff]
        ----------------------
        making it less ugly
        mc  = class {p3src, p3snk, tpol }
        ss  = class { tsep src_tag, snk_tag, sm_tag }
              ?rename as s4 or s5 or s6: "source&sink smearing&separation specification set"
        
    """
    dim     = len(latsize)
    log     = kw.get('log') or lhpd.log()
    lcdb    = kw.get('lcdb')
    rsplan  = kw['rsplan']
    tskip   = kw.get('tskip') or None
    c2fit_nexp= kw.get('c2fit_nexp') or None
    c3fit_nexp= c2fit_nexp
    VERBOSE = kw.get('VERBOSE', 0)
    # TODO rewrite : 
    # # make eqmat, pcmat; apply pcmat
    # # for each ss:
    # #* for all mcgrp : load c3pt (ss, mcgrp, ir_list), resample, apply pcmat
    # #* for all mcgrp : load c2pt for unique ss.key2pt(), [sep. for src, snk?], resample, apply pcmat
    # # for each i_eqn : solve for me (need data for full ssgrp)
    # # solve for ff
    n_mc    = len(mcgrp)
    n_comp  = ir_list_comp_len(ir_list)
    n_c3pt  = n_mc * n_comp * 2
    tsep_list   = sorted(set([ ss.get_tsep() for ss in ssgrp ]))
    tmax    = max(tsep_list) + 1            # FIXME if fit c2pt, may need to plug other value

    mcgrp_psrc = np.array([ mc.get_p3src() for mc in mcgrp ])
    mcgrp_psnk = np.array([ mc.get_p3snk() for mc in mcgrp ])
    mcgrp_tpol = np.array([ mc.get_tpol() for mc in mcgrp ])
    
    list_method_require_c2fit = ['fit_c3']
    disp_h5g = kw.get('disp_h5g', None)
    assert(None is disp_h5g or isinstance(disp_h5g, h5py.Group))
    #data_h5g = kw.get('data_h5g', None)
    #assert(None is data_h5g or isinstance(data_h5g, h5py.Group))
    
    require_c2fit   = False
    for m in method_list :
        if m[0] in list_method_require_c2fit : 
            require_c2fit = True

    for m in method_list :
        if m[0] in list_method_require_c2fit :
            require_c2fit = True

    def load_twopt_srcsnk(ss):
        """ return (Real[i_data, t, i_mc], Real[i_data, t, i_mc]) """
        n_data = res_src = res_snk = None
        for i_mc, mc in enumerate(mcgrp) :
            # [i_data, t]
            x = gg.get_twopt_src(mc, ss)
            y = gg.get_twopt_snk(mc, ss)
            if None is n_data :
                assert(None is res_src)
                assert(None is res_snk)
                n_data = x.shape[0]
                assert(y.shape[0] == n_data)
                res_src = np.empty((n_data, tmax, n_mc), np.float64)
                res_snk = np.empty((n_data, tmax, n_mc), np.float64)
            # [i_data, t, i_mc]
            res_src[:, :, i_mc] = x[:, :tmax].real
            res_snk[:, :, i_mc] = y[:, :tmax].real

        assert(np.isfinite(res_src).all())
        assert(np.isfinite(res_snk).all())
        return res_src, res_snk

    def load_c2fit_srcsnk(ss):
        """ c2 fit parameters for src and snk (c2fitp_src,c2fitp_snk) 
            return Real [i_data, i_p, i_mc] """
        n_data = n_fitp = None
        c2fitp_src = c2fitp_snk = None
        for i_mc, mc in enumerate(mcgrp) :
            # [i_data, i_p]
            p_src_r  = gg.get_twopt_fit_src(mc, ss)
            p_snk_r  = gg.get_twopt_fit_snk(mc, ss)
            if None is n_data :
                assert(None is c2fitp_src and None is c2fitp_snk)
                n_data, n_fitp = p_src_r.shape[0:2]
                assert(p_snk_r.shape[0] == n_data)
                c2fitp_src= np.empty((n_data, n_fitp, n_mc), np.float64)
                c2fitp_snk= np.empty((n_data, n_fitp, n_mc), np.float64)
            # [i_data, t, i_mc]
            c2fitp_src[:, :, i_mc]     = p_src_r
            c2fitp_snk[:, :, i_mc]     = p_snk_r
        assert(np.isfinite(c2fitp_src).all())
        assert(np.isfinite(c2fitp_snk).all())
        return c2fitp_src, c2fitp_snk

    def load_threept(ss) :
        """ return: Real [i_data, tau, i_mc, i_comp, reim] """
        n_data  = res = None
        n_tau   = 1 + ss.get_tsep()
        for i_mc, mc in enumerate(mcgrp) :
            # x[i_data, i_comp, tau] complex
            if lcdb : x = gg.get_threept(mc, ss)
            else :
                # TODO handle iteration over ir_list also in old-style gg
                xl = [ ir_scale * gg.get_threept(mc, ss, op, ir_name) 
                       for ir_name, ir_scale in ir_list ]
                x = np.concatenate(tuple(xl), axis=1)
            if None is res :
                n_data = x.shape[0]
                # res [i_data, tau, i_mc, i_comp, i_c] real
                res = np.empty((n_data, n_tau, n_mc, n_comp, 2), np.float64)

            res[:, :, i_mc, :, 0] = x[..., :n_tau].real.transpose(0,2,1)
            res[:, :, i_mc, :, 1] = x[..., :n_tau].imag.transpose(0,2,1)

        assert(np.isfinite(res).all())
        return res

    def apply_eqn_prec_twopt(c2a, pcmat) :
        """ compute preconditioned c2pt
            return Real [i_data, t, i_eqn] """
        n_eqn   = pcmat.shape[-1]
        res_c2a = np.zeros(c2a.shape[0:2] + (n_eqn,), c2a.dtype)
        for i_eqn, i_mc, i_comp, i_c in np.ndindex(n_eqn, n_mc, n_comp, 2) :
            f   = pcmat[i_mc, i_comp, i_c, i_eqn]
            if 0. == f : continue
            # TODO WARN if variance differs substantially between averaged rows 
            res_c2a[:, :, i_eqn]    += abs(f) * c2a[:, :, i_mc]
        pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
        return res_c2a / pc_norm
    def apply_eqn_prec_fitp(fitp, pcmat) :
        """ compute preconditioned c2pt fit params
            NOTE: both overlaps and energies are "averaged"; however, 2pt is (typically) already 
            averaged over rot/refl, so apply precond does not do anything; note that all |f|==1
            return Real [i_data, i_p, i_eqn] 
        """
        n_eqn   = pcmat.shape[-1]
        res_fitp = np.zeros(fitp.shape[0:2] + (n_eqn,), fitp.dtype)
        for i_eqn, i_mc, i_comp, i_c in np.ndindex(n_eqn, n_mc, n_comp, 2) :
            f   = pcmat[i_mc, i_comp, i_c, i_eqn]
            if 0. == f : continue
            assert(abs(abs(f) - 1.) < 1e-7)     # XXX not meaningful to average energies otherwise
            # TODO WARN if variance differs substantially between averaged rows 
            res_fitp[:, :, i_eqn]    += abs(f) * fitp[:, :, i_mc]
        pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
        return res_fitp / pc_norm

    
    def apply_eqn_prec_threept(c3ba, pcmat) :
        """ return preconditioned c3pt Real[i_data, tau, i_eqn] """
        n_eqn   = pcmat.shape[-1]
        res_c3ba= np.zeros(c3ba.shape[0:2] + (n_eqn,), c3ba.dtype)
        for i_eqn, i_mc, i_comp, i_c in np.ndindex(n_eqn, n_mc, n_comp, 2) :
            f   = pcmat[i_mc, i_comp, i_c, i_eqn]
            if 0. == f : continue
            # TODO WARN if variance differs substantially between averaged rows 
            res_c3ba[:, :, i_eqn]   += f * c3ba[:, :, i_mc, i_comp, i_c]
        pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
        return res_c3ba / pc_norm
    

    # [i_mc, i_comp, 2, i_ff], [i_mc, i_comp, 2, i_eqn]
    (eqmat, pcmat) = make_ff_eqnmat_precond_f2f(
            op, mlat, latsize, mcgrp, ir_list)
    assert(np.isfinite(eqmat).all())
    assert(np.isfinite(pcmat).all())

    n_ff    = eqmat.shape[-1]
    n_eqn   = pcmat.shape[-1]
    # preconditioner is applied to both 3pt and 2pt, so their ratio should be solved against 
    # equations without extra factors; therefore, normalize the result of applying pc to eqmat
    # (divide each row by the multiplicity over which pc matrix summates
    pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
    # [i_eqn, i_ff]
    eqmat_pc= np.dot(pcmat.reshape(n_c3pt, n_eqn).T,
                      eqmat.reshape(n_c3pt, n_ff)) / pc_norm[:,None] 
    if kw.get('print_eqmat_pc', False) :   # print pc'ed eqmat
        c_list      = ['Re', 'Im']
        comp_list   = ir_list_comp_list(ir_list)
        for i_eqn in range(n_eqn) : 
            mat_ls = []
            for i_mc, i_comp, i_c in it.product(range(n_mc), range(n_comp), [0,1]) : 
                pc_i = pcmat[i_mc, i_comp, i_c, i_eqn]
                if 0. != pc_i :
                    mat_ls.append('%+.1f*%s_%s{%s}' % (pc_i, c_list[i_c], 
                            comp_list[i_comp], mcgrp[i_mc]))
            rhs_sep = '\n    '
            log.writeln('%2d\t%s == %s%s' % (i_eqn, eqmat_pc[i_eqn], rhs_sep, rhs_sep.join(mat_ls)))

    # load all data
    c3pt_map_op_r   = {}        # real[ss][i_data, tau, i_mc, i_comp, reim]
    c2pt_src_r   = {}           # real[ss][i_data, t, i_mc]
    c2pt_snk_r   = {}           # real[ss][i_data, t, i_mc]
    c3pt_map_pc_r   = {}        # real[ss][i_data, tau, i_eqn]
    c2pt_src_pc_r   = {}        # real[ss][i_data, t, i_eqn]
    c2pt_snk_pc_r   = {}        # real[ss][i_data, t, i_eqn]
    c2fitp_src_pc_r = c2fitp_snk_pc_r = None    # [ss][i_data, i_p, i_eqn]


    calc_div = (op in ['tensor1', 'pstensor1'])
    if calc_div :
        deriv3_bcc2pt_coeff  = deriv_ft_bcc2pt(mcgrp_psnk - mcgrp_psrc, latsize[:3])  # [i_mc, i_comp(dim-1)]
        c3pt_div3_bcc_map_r = {}        # cplx[ss][i_data, tau, i_mc, reim]
        c3pt_div_bcc_map_r  = {}        # cplx[ss][i_data, tau, i_mc, reim]

    n_data      = None
    for ss in ssgrp :
        c3pt_map_op_r[ss] = lhpd.resample(load_threept(ss), rsplan)
        c3pt_map_pc_r[ss] = apply_eqn_prec_threept(c3pt_map_op_r[ss], pcmat)
        if calc_div :
            # cplx[i_data, tau, i_mc, i_comp]
            c3pt_op_c = c3pt_map_op_r[ss][..., 0] + 1j*c3pt_map_op_r[ss][..., 1]
            # sum_{i_comp(dim-1)} [i_data, tau, i_mc, i_comp] * [i_mc, i_comp]
            c3pt_div3_bcc_map_r[ss] = (c3pt_op_c[..., :3] * deriv3_bcc2pt_coeff).sum(-1)
            c3pt_div_bcc_map_r[ss]  = c3pt_div3_bcc_map_r[ss][:, 1:] - c3pt_div3_bcc_map_r[ss][:, :1]

        if None is n_data : n_data = c3pt_map_pc_r[ss].shape[0]
        assert(c3pt_map_pc_r[ss].shape[0] == n_data)

    ss_c2pt = list(set([ ss.key2pt() for ss in ssgrp ]))
    for ss in ss_c2pt :
        srcsnk = load_twopt_srcsnk(ss)
        c2pt_src_r[ss] = lhpd.resample(srcsnk[0], rsplan)
        c2pt_src_pc_r[ss] = apply_eqn_prec_twopt(c2pt_src_r[ss], pcmat)
        assert(c2pt_src_pc_r[ss].shape[0] == n_data)
        c2pt_snk_r[ss] = lhpd.resample(srcsnk[1], rsplan)
        c2pt_snk_pc_r[ss] = apply_eqn_prec_twopt(c2pt_snk_r[ss], pcmat)
        assert(c2pt_snk_pc_r[ss].shape[0] == n_data)
    
    
    # TODO load c2pt fit params, recreate "c2pt" from them
    if require_c2fit :
        c2fitp_src_r = {}
        c2fitp_snk_r = {}
        c2fitp_src_pc_r = {}
        c2fitp_snk_pc_r = {}
        n_fitp  = None
        c2exp_mf= lhpd.fitter.legacy2modelfunc(lhpd.pyfit.Nexp_ladder_c2pt(c2fit_nexp))
        tr      = np.r_[:tmax]
        for ss in set([ ss.key2pt() ]) :
            srcsnk = load_c2fit_srcsnk(ss)
            # XXX sic! no resampling here because already resampled
            # XXX fits are usually resampled!!! 
            # TODO make sure the rsplan is the same
            # XXX do not apply preconditioner to fitp directly !!! c2fit is not linear in all fitp
            # TODO recover 2pt function here (or in load_c2fit) for each ss
            # TODO apply preconditioner to c2fit
            # XXX how to properly apply precond to e0, de1_0 ???? they typicall have already been averaged over equivs
            c2fitp_src_r[ss] = srcsnk[0]
            c2fitp_snk_r[ss] = srcsnk[1]
            c2fitp_src_pc_r[ss] = apply_eqn_prec_fitp(c2fitp_src_r[ss], pcmat)
            c2fitp_snk_pc_r[ss] = apply_eqn_prec_fitp(c2fitp_snk_r[ss], pcmat)
            if None is n_fitp : 
                n_fitp = c2fitp_src_pc_r[ss].shape[1]
            def mk_nc2fit(fitp) :
                return np.array([ [ c2exp_mf.fv(tr, fitp[i_data, :, i_eqn]) 
                                    for i_eqn in range(n_eqn) ] 
                                  for i_data in range(n_data) ]).transpose(0,2,1) 

            
    # TODO methods with c2fit
    def calc_me_ratio(c2a, c2b, c3ba, tsep) :
        """ ASSUME time axis=1 """
        sqf = np.sqrt(np.abs(  c2b[:, : tsep+1] / c2a[:, : tsep+1]
                               * c2a[:, tsep::-1] / c2b[:, tsep::-1] ))
        return (c3ba / np.sqrt(np.abs(c2b[:, tsep] * c2a[:, tsep]))[:,None] * sqf)
    def calc_me_ratio_all(c2a, c2b, c3ba_map) : 
        return dict([ (ss, calc_me_ratio(c2a[ss.key2pt()], c2b[ss.key2pt()], 
                                         c3ba_map[ss], ss.get_tsep()))
                      for ss in c3ba_map.keys() ]) 
    # me_rs[ss][i_data, tau, i_eqn]
    # TODO solve for me here : ratios, fits, etc ...
    me_rs   = calc_me_ratio_all(c2pt_src_pc_r, c2pt_snk_pc_r, c3pt_map_pc_r)

    # XXX XXX XXX debug print
    if False :
        log.writeln(pc_norm)
        log.writeln(pcmat)
        log.writeln(eqmat_pc)
        for ss in ssgrp :
            log.writeln(ss)
            log.writeln(c3pt_map_pc_r[ss].mean(0))
            log.writeln(me_rs[ss].mean(0))
        for ss in set([ ss.key2pt() ]) :
            log.writeln(ss)
            log.writeln(c2pt_src_pc_r[ss].mean(0))
            log.writeln(c2pt_snk_pc_r[ss].mean(0))

    def chi2_cdf_func(ndof) :
        f_cdf = sp.stats.chi2(ndof).cdf
        return lambda chi2: f_cdf(chi2)
    
    def print_cov_stat(cov, title=None) :
        if 1 <= VERBOSE : 
            lhpd.pprint_cov_stat(cov, title=title, fo=log)

    def calc_ratio_pltx(ss, disp_h5g=None) :
        """ ff on timeslices
            return ff[n_data, 1+tsep, n_ff] """
        #assert(ss in c3pt_map_pc_r)
        tsep    = ss.get_tsep()
        res     = np.empty((me_rs[ss].shape[0], 1+tsep, n_ff), np.float64)
        for tau in range(1+tsep) :
            # solve od: [i_eqn,i_ff] for tau
            cov     = lhpd.calc_cov(me_rs[ss][:,tau], rsplan)
            cov_inv = lhpd.fitter.cov_invert(cov, rmin=1e-4)
            sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
            res[:,tau,:] = matrix_tensor_dot(sol_mat, me_rs[ss][:,tau,:], 1)
        return res

    def calc_ratio_pltx_avg(ss, max_tslice=3, disp_h5g=None) :
        """ ff from ratio pltx center avg
            return ff[n_data, 1, n_ff] """
        assert(ss in c3pt_map_pc_r)
        tsep    = ss.get_tsep()
        t1, t2 = lhpd.latcorr.pltx_trange(tsep, max_tslice)
        
        res     = np.empty((me_rs[ss].shape[0], 1, n_ff), np.float64)
        # solve od: [i_eqn,i_ff] for tau
        cov     = lhpd.calc_cov(me_rs[ss][:, t1:t2].mean(axis=1), 
                                         rsplan)
        cov_inv = lhpd.fitter.cov_invert(cov, rmin=1e-4)
        sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
        me_rs_avg = me_rs[ss][:, t1:t2, :].mean(axis=1)
        res[:, 0, :] = matrix_tensor_dot(sol_mat, me_rs_avg, 1)
        return res

    def calc_ratio_summ(ss_list, tskip=tskip, disp_h5g=None) :
        """ ff from summation-extrapolated matrix elements
            return ff[n_data, 1, n_ff] """
        assert(not None is tskip)
        for ss in ss_list : 
            assert(ss in c3pt_map_pc_r)
        tsep_list   = np.asarray(sorted(set([ ss.get_tsep() for ss in ss_list ])))
        n_tsep      = len(tsep_list)
        assert(len(ss_list) == n_tsep) # TODO implement for general case, e.g. where the same tsep are combined with different smearings
        #raise NotImplementedError
        me_summ     = np.empty((n_data, n_tsep, n_eqn), np.float64)
        for i_ss, ss in enumerate(ss_list) : # FIXME this loop
            tsep    = ss.get_tsep()
            me_summ[:, i_ss]      = me_rs[ss][:, tskip : 1 + tsep - tskip].sum(axis=1)

        # solve od : [i_tsep, {coeff, intercept}]
        me_gs       = np.empty((n_data, n_eqn), np.float64)
        #raise NotImplementedError   # make sure this equation has correct list of tsep for summation
        eqn_summ = np.r_[ [ [ss.get_tsep() for ss in ss_list] ], [[1.] * n_tsep] ].T
        for i_eqn in range(n_eqn) :
            me_summ_cov = lhpd.calc_cov(me_summ[:, :, i_eqn], rsplan)
            cov_inv     = lhpd.fitter.cov_invert(me_summ_cov, rmin=1e-4)        # FIXME regulate cov_inv better
            sol_summ, sol_undef = lhpd.fitter.linsolver_od_matr(eqn_summ, cov_inv)
            me_gs[:, i_eqn]     = matrix_tensor_dot(sol_summ, me_summ[:, :, i_eqn], 1)[:, 0]

        # solve od : [i_eqn, i_ff]
        res         = np.empty((n_data, 1, n_ff), np.float64)
        me_gs_cov   = lhpd.calc_cov(me_gs, rsplan)
        cov_inv     = lhpd.fitter.cov_invert(me_gs_cov, rmin=1e-4)
        sol_ff, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)    # FIXME regulate cov_inv better
        res[:, 0, :]= matrix_tensor_dot(sol_ff, me_gs, 1)
        return res

    def calc_fit_c3(ss_list, tskip=tskip, disp_h5g=None) :
        """ reproducing Boram's fits here """
        ttau    = []  # list of tuples (tsep, tau)
        c3_ttau = []
        for ss in ss_list :
            tsep    = ss.get_tsep()
            ttau.extend([ (ss.get_tsep(), tau) for tau in np.r_[tskip : ss.get_tsep() - tskip + 1]])
            c3_ttau.extend([ c3pt_map_pc_r[ss][:, tskip : ss.get_tsep() - tskip + 1] ])
        # [i_ttau, {tsep,tau}]
        ttau    = np.asarray(ttau)
        n_ttau  = len(ttau)
        # [i_data, i_ttau, i_eqn]
        c3_ttau = np.concatenate(c3_ttau, axis=1)
        #print(ttau, c3_ttau.shape)

        # FIXME name parameters in fits and rewrite

        assert(2 == c2fit_nexp) # TODO any nexp
        fit_c2_np   = 2*c2fit_nexp     # [ C0, E0, C1, dE01, ... ]
        fit_c3_np   = c3fit_nexp * c3fit_nexp     # [ M00, M01, ..., M10, M11, ... ]
        fit_c2src_param = np.empty((n_data, n_eqn, fit_c2_np), np.float64)
        fit_c2snk_param = np.empty((n_data, n_eqn, fit_c2_np), np.float64)
        fit_c3_param    = np.empty((n_data, n_eqn, fit_c3_np), np.float64)

        me_gs           = np.empty((n_data, n_eqn), np.float64)
        c3_param        = np.empty((n_data, n_eqn, fit_c3_np), np.float64)
        c3_chi2         = np.empty((n_data, n_eqn), np.float64)

        c3_ndof         = n_ttau - fit_c3_np
        c3_chi2_cdf     = chi2_cdf_func(c3_ndof)        # FIXME replace with Hotelling dist.
        for i_eqn in range(n_eqn) :
            # [i_data, i_me, i_ttau]
            c3_ttau_cov     = lhpd.calc_cov(c3_ttau[:, :, i_eqn], rsplan)
            print_cov_stat(c3_ttau_cov, title='c3_ttau_cov')
            c3_ttau_cov_inv = lhpd.fitter.cov_invert(c3_ttau_cov, rmin=1e-4)    # FIXME regulate cov_inv better
            me_fit_all      = []
            for i_data in range(n_data) :
                assert(1==len(c2fitp_src_pc_r.keys()))  # TODO >1 smearing pairs
                fitp_src = c2fitp_src_pc_r.values()[0][i_data,:,i_eqn]
                fit_c2src_param[i_data, i_eqn] = fitp_src
                assert(1==len(c2fitp_snk_pc_r.keys()))  # TODO >1 smearing pairs
                fitp_snk = c2fitp_snk_pc_r.values()[0][i_data,:,i_eqn]
                fit_c2snk_param[i_data, i_eqn] = fitp_snk
                # [i_me, i_ttau]
                # TODO replace with c3fit_dexp_2state
                exp_ttau = np.empty((4, n_ttau), np.float64)
                exp_ttau[0] = np.exp(-fitp_src[1] * ttau[:,1] -fitp_snk[1] * (ttau[:,0]-ttau[:,1]))
                #* np.sqrt(fitp_src[0] * fitp_snk[0]) # <0|0>
                exp_ttau[1] = exp_ttau[0] * np.exp(-fitp_src[3] * ttau[:,1])
                #* np.sqrt(fitp_src[2])  # <0|1>
                exp_ttau[2] = exp_ttau[0] * np.exp(-fitp_snk[3] * (ttau[:,0] - ttau[:,1]))  
                #* np.sqrt(fitp_snk[2])  # <1|0>
                exp_ttau[3] = exp_ttau[0] * np.exp(-fitp_src[3] * ttau[:,1] -fitp_snk[3] * (ttau[:,0] - ttau[:,1])) 
                #* np.sqrt(fitp_src[2]*fitp_snk[2]) # <1|1>
                
                sol_me, undef_me    = lhpd.fitter.linsolver_od_matr(exp_ttau.T, c3_ttau_cov_inv) 
                me_fit = np.dot(sol_me, c3_ttau[i_data, :, i_eqn])
                c3_param[i_data, i_eqn] = me_fit

                # divide out <0|0> coeff np.sqrt(fitp_src[0] * fitp_snk[0])
                me_gs[i_data, i_eqn]= me_fit[0] / np.sqrt(fitp_src[0]*fitp_snk[0])

                # [i_ttau]
                c3_diff                 = np.dot(exp_ttau.T, me_fit) -  c3_ttau[i_data, :, i_eqn]
                c3_chi2[i_data, i_eqn]  = np.dot(c3_diff, np.dot(c3_ttau_cov_inv, c3_diff))

            c3_chi2_a, c3_chi2_e = lhpd.calc_avg_err(c3_chi2[:, i_eqn], rsplan)
            pvalue_a    = 1. - c3_chi2_cdf(c3_chi2_a)
            log.writeln("i_eqn=%d  chi2/ndof=%s/%d=%s  pvalue=%f" % (
                        i_eqn, #matrix_elem_str()
                        lhpd.spprint_ve(c3_chi2_a, c3_chi2_e), c3_ndof,
                        lhpd.spprint_ve(c3_chi2_a / c3_ndof, c3_chi2_e / c3_ndof),
                        pvalue_a))

    
        # TODO add prec.ff equation
        # TODO add prec
        # TODO add c3 data?
        # TODO add chi2 for eqn

        # [i_data, i_eqn]
        res             = np.empty((n_data, 1, n_ff), np.float64)
        me_gs_cov       = lhpd.calc_cov(me_gs, rsplan)
        print_cov_stat(me_gs_cov, title='me_gs_cov')
        me_gs_cov_inv   = lhpd.fitter.cov_invert(me_gs_cov, rmin=1e-4)
        sol_ff, undef_ff= lhpd.fitter.linsolver_od_matr(eqmat_pc, me_gs_cov_inv)# FIXME regulate cov_inv better
        ff_fit          = matrix_tensor_dot(sol_ff, me_gs, 1)
        res[:, 0, :]    = ff_fit
        me_diff         = matrix_tensor_dot(eqmat_pc, ff_fit, 1) - me_gs
        me_chi2         = (me_diff * matrix_tensor_dot(me_gs_cov_inv, me_diff, 1)).sum(1)
        if 1 <= VERBOSE : 
            log.writeln("eqmat_pc=\n%s" % eqmat_pc)
            log.writeln("sol_ff=\n%s" % sol_ff)
            log.writeln("me_gs_cov=\n%s" % me_gs_cov)
            log.writeln("me_avg=\n%s" % lhpd.calc_avg(me_gs))

        # [i_data, i_eqn]
        me_ndof         = n_eqn - n_ff
        me_chi2_cdf     = chi2_cdf_func(me_ndof)

        me_chi2_a, me_chi2_e = lhpd.calc_avg_err(me_chi2, rsplan)
        pvalue_a    = 1. - me_chi2_cdf(me_chi2_a)
        log.writeln("me_fit  chi2/ndof=%s/%d=%s  pvalue=%f\n" % (
                    lhpd.spprint_ve(me_chi2_a, me_chi2_e), me_ndof,
                    lhpd.spprint_ve(me_chi2_a / max(1,me_ndof), me_chi2_e / max(1,me_ndof)),
                    pvalue_a))

        if not None is disp_h5g :
            # for plotting c3_data / c2_fit_gs vs c3_ft / c2_fit_gs, so no c2src/snk_data
            # TODO unify index order for 
            #           correlators [ ..., t/ttau, i_mc/i_eqn , ... ] ? 
            #           fit params  [ ..., i_mc/i_eqn , i_p, ... ] ? 
            #      if so, fix plot_fits_c3, plot_divj
            #      affected : c3_ttau, c2{src,snk}_fitp{,_pc}/sskey
            disp_d = {
                # TODO latsize mass q2
                'rsplan'        : rsplan                    , 
                'rsplan_str'    : lhpd.rsplan_str(rsplan)   ,
                'ttau'          : ttau                      ,   # [i_ttau, {tsep, tau}]
                'c3_tskip'      : tskip                     ,   # []
                'c3_nexp'       : c3fit_nexp                ,   # []
                'c3_fitp'       : np.asarray(c3_param)      ,   # [i_data, i_eqn, i_p]
                'c3_ndof'       : c3_ndof                   ,   # []
                'c3_chi2'       : c3_chi2                   ,   # [i_data, i_eqn]
                #'c3_pvalue'     : c3_chi2_cdf(c3_chi2)     ,   # [i_data, i_eqn]
                'c3_ttau'       : c3_ttau                   ,   # [i_data, i_ttau, i_eqn]
                'c2src_fitp'    : c2fitp_src_r.values()[0].transpose((0,2,1)),   # [i_data, i_mc, i_p_c2]
                'c2snk_fitp'    : c2fitp_snk_r.values()[0].transpose((0,2,1)),   # [i_data, i_mc, i_p_c2]
                'c2src_fitp_pc' : fit_c2src_param           ,   # [i_data, i_eqn, i_p_c2]
                'c2snk_fitp_pc' : fit_c2snk_param           ,   # [i_data, i_eqn, i_p_c2]

                'me_ndof'       : me_ndof                   ,   # []
                'me_chi2'       : me_chi2                   ,   # [i_data]

                'c3_op'         : op                        ,   # []
                'c3_psrc'       : mcgrp_psrc                ,   # [i_mc, {x,y,z}]
                'c3_psnk'       : mcgrp_psnk                ,   # [i_mc, {x,y,z}]
                'c3_tpol'       : mcgrp_tpol                ,   # [i_mc]
                'c3_comp_list'  : ir_list_comp_list(ir_list),   # [i_comp]
                'eqmat'         : eqmat                     ,   # [i_mc, i_comp, reim, i_ff] 
                'pcmat'         : pcmat                     ,   # [i_mc, i_comp, reim, i_eqn]
                'eqmat_pc'      : eqmat_pc                  ,   # [i_eqn, i_ff]

            }
            # common data? save to separate [<groupName>__DATA] group?
            for ss in ssgrp:
                sk = ss.strkey()
                disp_d['c3_data/%s' % sk]          = c3pt_map_op_r[ss]  # [i_data, t, i_mc, i_comp, reim]
                disp_d['c3_data_pc/%s' % sk]       = c3pt_map_pc_r[ss]  # [i_data, t, i_eqn]
                #if calc_div :
                #    disp_h5g['c3_div_bcc/%s' % sk] = c3pt_div_bcc_map_r[ss]
            for ss in ss_c2pt :
                sk = ss.strkey()
                disp_d['c2src_data/%s' % sk]       = c2pt_src_r[ss]     # [i_data, t, i_mc ]
                disp_d['c2snk_data/%s' % sk]       = c2pt_snk_r[ss]     # [i_data, t, i_mc ]
                disp_d['c2src_data_pc/%s' % sk]    = c2pt_src_pc_r[ss]  # [i_data, t, i_eqn]
                disp_d['c2snk_data_pc/%s' % sk]    = c2pt_snk_pc_r[ss]  # [i_data, t, i_eqn]

                #disp_d['c2src_fitp/%s' % sk]   = c2fitp_src_r[ss].transpose((0,2,1))      # [i_data, i_mc , i_p]
                #disp_d['c2snk_fitp/%s' % sk    = c2fitp_snk_r[ss].transpose((0,2,1))      # [i_data, i_mc , i_p]
                #disp_d['c2src_fitp_pc/%s' % sk]= c2fitp_src_pc_r[ss].transpose((0,2,1))   # [i_data, i_eqn, i_p]
                #disp_d['c2snk_fitp_pc/%s' % sk]= c2fitp_snk_pc_r[ss].transpose((0,2,1))   # [i_data, i_eqn, i_p]
            h5_io.h5_write_dict(disp_h5g, disp_d)

        return res
        # TODO calc_exp_fit continue here

            
    # TODO calc_gpof


    all_res     = []
    for m in method_list :
        m0  = m[0]
        if   'ratio_pltx' == m0 : 
            for ss in ssgrp :
                all_res.append(calc_ratio_pltx(ss, disp_h5g=disp_h5g))

        elif 'ratio_pltx_avg' == m0 :
            for ss in ssgrp :
                all_res.append(calc_ratio_pltx_avg(ss, disp_h5g=disp_h5g))

        elif 'ratio_summ' == m0 :
            all_res.append(calc_ratio_summ(ssgrp, disp_h5g=disp_h5g))

        elif 'fit_c3' == m0 :
            all_res.append(calc_fit_c3(ssgrp, disp_h5g=disp_h5g))

        else : raise ValueError(m0)

    return all_res


def calc_save_ff(
            h5_file, h5_kpath,  # output
            gg,                 # input 
            mcgrp_list, ssgrp, op, ir_list,
            mlat, latsize, 
            method_list,
            **kw) :
    """ iterate calc_ff over method_list [* ssgrp], mcgrp_list; save to file """ 

    n_q2        = len(mcgrp_list)
    n_ff        = get_gff_number(op)
    save_data   = kw.get('save_data', False)    # whether to save "raw" c3pt, c2pt, ff-eqn data
    save_disp   = kw.get('save_disp', False)    # whether to save "display" information for fits etc
    log         = kw.get('log') or lhpd.log()

    def calc_ff_1method_allq2(k, m, ssgrp) :
        q2_list = []
        dset = None
        n_data, n_method = None, None
        k1      = "%s/%s" % (h5_kpath, k)
        log.writeln("save FF to [%s]" % k1)
        for i_q2, mcgrp in enumerate(mcgrp_list) :
            q2grp   = np.array([ lorentz_mom_transfer_sq(mlat, latsize, 
                                        mc.get_p3src(), mc.get_p3snk())
                                 for mc in mcgrp ])
            q2      = q2grp[0]
            assert(np.allclose(q2, q2grp))
            if 0 < kw.get('verbose', 0) :
                log.writeln("# Q2=%f  " % ( q2, ))
            elif 1 < kw.get('verbose', 0) :
                log.writeln("# Q2=%f  [ %s ]" % ( q2, ','.join([ str(mc) for mc in mcgrp ]) ))
            q2_list.append(q2)

            if save_disp : 
                kdisp = "%s__DISP/q2_%d" % (k1, i_q2)
                log.writeln("save disp to [%s]" % kdisp)
                h5_io.h5_purge_keys(h5_file, [kdisp])
                disp_h5g_i = h5_file.require_group(kdisp)
            else : disp_h5g_i = None

            kw1 = dictnew(kw, disp_h5g=disp_h5g_i)
            ff_ = calc_ff(gg, mcgrp, ssgrp, op, ir_list, mlat, latsize, [m], **kw1)
            assert(1 == len(ff_))
            ff  = ff_[0]

            if None is dset :
                n_data, n_method = ff.shape[0:2]
                h5_io.h5_purge_keys(h5_file, [k1])
                dset = h5_file.require_dataset(k1, (n_data, n_method, n_q2, n_ff), 
                            np.float64, fletcher32=True)
            dset[:, :, i_q2, :] = ff

        dset.attrs['q2_list'] = q2_list
        dset.attrs['dim_spec'] = ['i_data', 'k_method', 'i_q2', 'i_ff'] # k_method stands for tau in ratio_pltx
        
    for m in method_list :
        m0  = m[0]
        if   'ratio_pltx' == m0 or 'ratio_pltx_avg' == m0 :
            tsep_list = []
            for ss in ssgrp :
                tsep    = ss.get_tsep()
                # FIXME make separate records for the same tsep but different smearings
                assert(not tsep in tsep_list)
                tsep_list.append(tsep)
                k = '%s/dt%d' % (m0, tsep,)
                calc_ff_1method_allq2(k, m, [ss])

        elif 'ratio_summ' == m0 or 'fit_c3' == m0 :
            calc_ff_1method_allq2(m0, m, ssgrp)


