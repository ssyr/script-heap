#!/usr/bin/env python
from __future__ import print_function
import sys, os
import lhpd
import numpy as np
import h5py

def print_usage(name) :
    print("""
Usage:
%s  <axis_spec>  <axis_attr>  <mom2_max>  in_file  out_file  kpaths
""" % name)
    return

if len(sys.argv) < 5 :
    print_usage('select')
    sys.exit(1)

#print(sys.argv)
axis_spec   = sys.argv[1]
axis_attr   = sys.argv[2]
skip_attr   = [axis_attr, 'data_list']
mom2max     = int(sys.argv[3])
src_h5name, dst_h5name = sys.argv[4:6]
kpath_list  = sys.argv[6:]

mom_list    = lhpd.normsorted(list(lhpd.int_vectors_maxnorm(3, mom2max))) 
n_mom       = len(mom_list)

src_h5  = h5py.File(src_h5name, 'r')
dst_h5  = h5py.File(dst_h5name, 'a')

for kpath in kpath_list :
    #print(kpath, src_h5[kpath].shape)
    src_dset    = src_h5[kpath]
    i_axis      = list(src_dset.attrs['dim_spec']).index(axis_spec)
    src_mom_list= src_dset.attrs[axis_attr]
    
    print("%s[%s][%s:%d:%s]: %d->%d" % (src_h5name, kpath, 
            axis_spec, i_axis, axis_attr, len(src_mom_list), n_mom))
    
    dst_sh      = list(src_dset.shape)
    dst_sh[i_axis] = n_mom
    dst_sh      = tuple(dst_sh)
    sh_len      = len(dst_sh)
    dst_dtype   = src_dset.dtype
    lhpd.h5_io.h5_purge_keys(dst_h5, [kpath])
    dst_dset    = dst_h5.require_dataset(kpath, dst_sh, dst_dtype, fletcher32=True)

    # copy data
    for i_m, m in enumerate(mom_list) :
        i_m_src = lhpd.np_find_first(m, src_mom_list)[0]
        dst_sl  = lhpd.np_at_slice(sh_len, (i_axis, i_m))
        src_sl  = lhpd.np_at_slice(sh_len, (i_axis, i_m_src))
        #print("%s <- %s" % (dst_sl, src_sl))
        dst_dset[dst_sl] = src_dset[src_sl]

    # copy attrs
    for k in src_dset.attrs.keys() :
        if k in skip_attr : pass
        else :    dst_dset.attrs[k] = src_dset.attrs[k]
    if lhpd.h5_io.h5_has_datalist(src_dset) :
        lhpd.h5_io.h5_set_datalist(dst_dset, lhpd.h5_io.h5_get_datalist(src_dset))
    dst_dset.attrs[axis_attr] = mom_list

    dst_h5.flush()


src_h5.close()
dst_h5.flush()
