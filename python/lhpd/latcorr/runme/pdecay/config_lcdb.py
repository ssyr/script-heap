import aff
import h5py 
import numpy as np
import math
import itertools as it
import lhpd
from lhpd import make_src_grid
from lhpd.misc import strkey

# XXX general idea
# * modular database (many separate files with internal structure)
# * each dataset is addressed by pair (file, kpath)
# * datasets are standard for c2pt, c3pt, c4pt
# * file, kpath) are created by specific functions:
# * * c2pt: (dat_d, c2_d)
# * * c3pt: (dat_d, c3_d, ins_d)
# * * c4pt: (dat_d, c4_d(=?=c3_d), ins1_d, ins2_d)
# * dat_d incorporates type(stage:raw,all,bin,ub), cfg?,csrc?,ama? depending on stage
# * XXX_d stands for "description"(dictionary) for XXX
# * preferably use Attrdict?

# XXX pieces of rationale
# Q: which parts of metadata go to file, which go to kpath?
# * most frequently used together -> kpath, the rest -> file
# * separate files are useful for downloading and working on
# *   partial analysis

# XXX examples
# dat_d = dict(kind='bin'|'all'|'cfg', ama='ub'|'sl'|'ex', cfgkey=, csrc=|csrcgrp=)
# ins_d = dict(
#   kind='bb', flav='U'|'D', lpath='xYz' (if empty: all available?) lpath_list=?
#   kind='op', flav='U'|'D', op='tensor1', ir='adfadf'|ir_list=[], flav=
#   kind='qpdf' probably same as bb
#   kind='tmd'
# c3_d = dict(
#   had='proton', tpol='posSzplus',
#   psnk=
#   tsep=
#   smsrcsnk=, [smtag=]
# c2_d = dict(
#   had='proton', [tpol='posSzplus',]
#   smsrcsnk=, [smtag=,]

# TODO combine all functions into latcorr_db_file, latcorr_db_kpath (or combination?)

def srcsnk_str_f(cN_d) : return '%s' %(cN_d['smsrcsnk'])
def srcsnk_str_k(cN_d) : return '%s' %(cN_d['smsrcsnk'])

def c3snk_str_f(c3_d) : return strkey.snk_str_f(c3_d['psnk'], c3_d['tsep'])
def c3snk_str_k(c3_d) : return strkey.snk_str_k(c3_d['psnk'], c3_d['tsep'])

def ama_str(ama) : return ama

def hadron_pdecay_str(cN_d) : 
    had = cN_d['had']
    if 'proton' == had : return "%s_meson%s" %(had, cN_d['mgamma'])
    else : raise ValueError(had)
hadron_pdecay_str_f = hadron_pdecay_str
hadron_pdecay_str_k = hadron_pdecay_str
hadron_str = hadron_pdecay_str

def dat_str_f(which, dat_d) :       # only for conv, all, bin, ub
    dat_kind = dat_d['kind']
    if   'conv' == dat_kind : 
        return '%s/%s.%s.%s' % (which, which, 
                dat_d['cfgkey'], ama_str(dat_d['ama']))
    elif 'all'  == dat_kind :
        return '%s.all/%s-all.%s' % (which, which, ama_str(dat_d['ama']))
    elif ('ub'   == dat_kind) or ('bin' == dat_kind and 'ub' == dat_d['ama']) : 
        return '%s.bin/%s-unbias' % (which, which)
    elif 'bin'  == dat_kind :
        return '%s.bin/%s-bin.%s' % (which, which, ama_str(dat_d['ama']))
    else : raise ValueError(dat_kind)


#################### c2pt ########################
# XXX c2pt data not generated after May 2019 [JSY email 2020/06/04]
# c2_d = dict(had='proton', smsrcsnk=, smtag=)
# example
#   c2pt/c2pt.3320.sl.x13y25z31t29_x13y25z31t45_x13y25z31t61_x13y25z31t13.GN2x50bxp20_GN2x50bxp20.aff
#       //c2pt/SP/proton_Tg0/PX-1_PY-2_PZ-2
def get_c2pt_file(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    if   'aff'  == dat_kind :
        return 'c2pt/c2pt.%s.%s.%s.aff' % (
                dat_d['cfgkey'], ama_str(dat_d['ama']),
                strkey.csrc_str_f(dat_d['csrcgrp'][0]))
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
        return '%s.%s.%s.h5' % (
                dat_str_f('c2pt', dat_d), 
                c2_d['had'], srcsnk_str_f(c2_d))
    else : raise ValueError(dat_kind)

def get_c2pt_kpath(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    #/c2pt/SS/proton_Tg0/x19_y11_z13_t47/PX1_PY1_PZ-1:
    if   'aff'  == dat_kind :
        return '/c2pt/%s/%s_%s/%s' % (
                c2_d['smsrcsnk'], c2_d['had'], c2_d['tpol'], 
                strkey.csrc_str_k(dat_d['csrcgrp'][0]))
    elif 'conv' == dat_kind : 
        return '/cfg%s/c2pt/%s/%s' % (
                dat_d['cfgkey'], c2_d['smsrcsnk'], c2_d['had'])
    elif ('all' == dat_kind or
          'bin' == dat_kind or
          'ub'  == dat_kind) :
        return '/c2pt/%s/%s' % (c2_d['smsrcsnk'], c2_d['had'])
    else : raise ValueError(dat_kind)

#################### hadspec(kind=c2h) ########################
# XXX hadspec saved instead of c2pt after May 2019 [JSY email 2020/06/04]
# example hadspec/hadspec.01000.sl.x17y13z23t33.aff///c2pt/meson_ss/SS/Tg15/PX2_PY1_PZ-2
# example hadspec/hadspec.01000.sl.x17y13z23t33.aff///c2pt/nucleon/SS/Tg15/PX2_PY0_PZ2
def get_hadspec_file(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    if   'aff'  == dat_kind :
        return 'hadspec/hadspec.%s.%s.%s.aff' % (
                dat_d['cfgkey'], ama_str(dat_d['ama']),
                strkey.csrc_str_f(dat_d['csrcgrp'][0]))
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
        return '%s.%s.%s.h5' % (
                dat_str_f('hadspec', dat_d), 
                c2_d['had'], srcsnk_str_f(c2_d))
    else : raise ValueError(dat_kind)

def get_hadspec_kpath(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    #/c2pt/nucleon/SS/Tg15/PX2_PY0_PZ2
    #/c2pt/SS/proton_Tg0/x19_y11_z13_t47/PX1_PY1_PZ-1:
    if   'aff'  == dat_kind :
        return '/c2pt/%s/%s/%s' % (
                c2_d['had'], c2_d['smsrcsnk'], c2_d['tpol'])
    elif 'conv' == dat_kind : 
        return '/cfg%s/c2pt/%s/%s' % (
                dat_d['cfgkey'], c2_d['smsrcsnk'], c2_d['had'])
    elif ('all' == dat_kind or
          'bin' == dat_kind or
          'ub'  == dat_kind) :
        return '/c2pt/%s/%s' % (c2_d['smsrcsnk'], c2_d['had'])
    else : raise ValueError(dat_kind)

#################### hadspec(kind=c2lec) ########################
def get_c2lec_file(dat_d, c2lec_d) :
    # lec/lec.01000.sl.x17y13z23t33.aff
    dat_kind = dat_d['kind']
    if   'aff'  == dat_kind :
        return 'c2lec/lec.%s.%s.%s.aff' % (
                dat_d['cfgkey'], ama_str(dat_d['ama']),
                strkey.csrc_str_f(dat_d['csrcgrp'][0]))
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
        return '%s.%s.%s.h5' % (
                dat_str_f('c2lec', dat_d), 
                c2lec_d['had'], srcsnk_str_f(c2lec_d))
    else : raise ValueError(dat_kind)

def get_c2lec_kpath(dat_d, c2lec_d) :
    dat_kind = dat_d['kind']
    #/lec_pdecay/SP/lec/tp2_RL/Tg4/PX0_PY0_PZ0:  complex[64]
    if   'aff'  == dat_kind :
        return '/lec_pdecay/%s/lec/%s/%s' % (
                c2lec_d['smsrcsnk'], c2lec_d['op'], c2lec_d['tpol'])
    elif 'conv' == dat_kind : 
        return '/cfg%s/c2lec/%s/%s/%s' % (
                dat_d['cfgkey'], c2lec_d['smsrcsnk'], c2lec_d['had'], c2lec_d['op'])
    elif ('all' == dat_kind or
          'bin' == dat_kind or
          'ub'  == dat_kind) :
        return '/c2lec/%s/%s/%s' % (c2lec_d['smsrcsnk'], c2lec_d['had'], c2lec_d['op'])
    else : raise ValueError(dat_kind)

#################### c3pt ########################
# c3_d  = dict(had='proton', tpol=, psnk=, tsep=, smsrcsnk=, smtag=)
# ins_d = dict(kind=, flav=, op=, ir=)
# example
# data/c3pt_pdecay/c3pt_pdecay.02230.sl.x22y14z10t2_x10y2z22t34.PX0PY0PZ2dt8.Tg15.UD.aff
def get_c3pt_pdecay_file(dat_d, c3_d, ins_d) :
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']
    assert('pdecay' == ins_kind)
    if   'aff'  == dat_kind :
        return 'c3pt_pdecay/c3pt_pdecay.%s.%s.%s.%s.%s.%s.aff' % (
                dat_d['cfgkey'], ama_str(dat_d['ama']), 
                strkey.csrcgrp_str_f(dat_d['csrcgrp']), c3snk_str_f(c3_d),
                c3_d['mgamma'], ins_d['flav'])
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
        return '%s.%s.%s.%s.%s.h5' % (
                dat_str_f('c3pt_pdecay', dat_d),
                c3snk_str_f(c3_d), hadron_pdecay_str_f(c3_d), 
                srcsnk_str_f(c3_d), ins_d['flav'])
    else : raise ValueError((dat_kind, ins_kind))
def get_c3pt_pdecay_kpath(dat_d, c3_d, ins_d) :
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']
    assert('pdecay'   == ins_kind)
    if   'aff'  == dat_kind :
        # [/c3pt_pdecay/SS/proton/Tg15/UD/tp2_RR/PX0_PY0_PZ2_dt8/Tg14/PX0_PY0_PZ-2]:  complex[64]
        return '/c3pt_pdecay/%s/%s/%s/%s/%s/%s/%s' % (
                c3_d['smsrcsnk'], c3_d['had'], c3_d['mgamma'], 
                ins_d['flav'], ins_d['op'], c3snk_str_k(c3_d), 
                c3_d['op_tpol']) # sic! set by conv2hdf_c2pt_aff_chop 
    elif 'conv' == dat_kind : 
        return '/cfg%s/c3pt_pdecay/%s/%s/%s/%s/%s' % (
                dat_d['cfgkey'], c3_d['smsrcsnk'], hadron_pdecay_str_k(c3_d),
                c3snk_str_k(c3_d), ins_d['flav'], ins_d['op'])
    elif ('all' == dat_kind or
          'bin' == dat_kind or
          'ub'  == dat_kind) :
        return '/c3pt_pdecay/%s/%s/%s/%s/%s' % (
                c3_d['smsrcsnk'], hadron_pdecay_str_k(c3_d),
                c3snk_str_k(c3_d), ins_d['flav'], ins_d['op'])
    else : raise ValueError((dat_kind, ins_kind))

#################### c2fit ########################
# c2fit_d = {kind='nexp'|, ..., trange=}
def trange_str(tr) :
    if isinstance(tr, tuple) : 
        assert(2 == len(tr))
        tr_str = "tmin%d_tmax%d" % tr
    elif isinstance(tr, list) or isinstance(tr, np.ndarray) :
        tr_minmax = min(tr), max(tr)
        tr_str = "tmin%d_tmax%d" % tr_minmax
    else : raise TypeError(tr)
    return tr_str

def c2fit_model_str(c2fit_d) :
    c2fit_kind  = c2fit_d['kind']
    if 'nexp' == c2fit_kind :
        nexp    = c2fit_d['nexp']
        tr      = c2fit_d['trange']
        return "nexp%d/%s" % (nexp, trange_str(c2fit_d['trange']))
    else : raise ValueError(c2fit_kind)

def get_c2fit_file(dat_d, c2fit_d, c2_d) :
    return "c2fit.%s_%s.h5" % (c2_d['had'], c2_d['tpol'])
def get_c2fit_kpath(dat_d, c2fit_d, c2_d) :
    psnk_tag = strkey.psnk_str_k(c2_d['psnk'])
    c2fit_model_tag = c2fit_model_str(c2fit_d)
    return "/c2fit/%s_%s/%s/%s/%s" % (c2_d['had'], c2_d['tpol'], c2_d['smsrcsnk'], 
        psnk_tag, c2fit_model_tag)


def get_c3fit_file(dat_d, c3fit_d, c3_d, ins_d) :
    return "pdff.%s.%s.h5" % (dat_d['ama'], hadron_pdecay_str_k(c3_d))

def c3fit_tag_str(d) :
    ss = []
    assert('nexp' == d['kind'])
    ss.append("nexp%d" % d['nexp'])
    ss.append("tsep%s" % '-'.join([ '%d' % t for t in d['tsep_list' ] ]))
    ss.append("tskip%d-%d" % (d['tskip_lo'], d['tskip_hi']))
    if 'ncov_shrink' in d : ss.append('shr%.0e' % d['ncov_shrink'])
    return '_'.join(ss)

def get_c3fit_kpath(dat_d, c3fit_d, c3_d, ins_d) :
    chiavg_tag = ''
    if c3fit_d.c3pt_avg_chi : chiavg_tag = 'chi_avg'
    return '/pdecay_ff/%s/%s/%s/%s' % (
            hadron_pdecay_str_k(c3_d), chiavg_tag, ins_d['op'], c3fit_tag_str(c3fit_d))
    

#------------------------------------------------------------------------------
def get_prop_ft_file(dat_d, lc_d) :
    dkind   = dat_d['kind']
    ama_str = ({ 'ex':'exact', 'sl':'sloppy' })[dat_d['ama']]
    if   'raw' == dkind : 
        return 'prop_ft/prop_FT_0%s_%s_%s.h5' % (
                dat_d['cfgkey'], strkey.csrc_str_k(dat_d['csrc']), ama_str)
    elif 'all' == dkind : 
        return 'prop_ft_selmom.all/prop_ft.%s.h5' % (dat_d['ama'],)
    else : raise ValueError((dkind,))

def get_prop_ft_kpath(dat_d, lc_d) : return '/x/prop_ft'

def get_npr_file(dat_d, lc_d) : return 'npr_contract.h5'
def get_npr_kpath(dat_d, lc_d) : return '/npr/%s' % lc_d['npr_d']['kind']

############# common function #############
def get_lcdb_loc(dat_d, lc_d) :
    """ dat_d   = dict(data_dir=,ama=,cfgkey=,csrcgrp=,...)
        lc_d    = dict(kind='c2',   c2_d=,)
                | dict(kind='c3',   c3_d=, ins_d=)
                | dict(kind='c3vi', c3_d=, ins_d=, vacins_d= } ??? crosscheck with setup @Mira
    """
    ddir    = dat_d['data_dir']
    kind    = lc_d['kind']
    if   'c2'   == kind : 
        c2_d    = lc_d['c2_d']
        return (
            '%s/%s' % (ddir, get_c2pt_file(dat_d, c2_d)), 
            get_c2pt_kpath(dat_d, c2_d))
    elif 'c2fit' == kind :
        c2fit_d = lc_d['c2fit_d']
        c2_d    = lc_d['c2_d']
        return (
            '%s/%s' % (ddir, get_c2fit_file(dat_d, c2fit_d, c2_d)),
            get_c2fit_kpath(dat_d, c2fit_d, c2_d))
    elif 'c3fit' == kind : # XXX of 'ff' ?
        c3fit_d, c3_d, ins_d = lc_d['c3fit_d'], lc_d['c3_d'], lc_d['ins_d']
        if 'pdecay' == ins_d['kind'] :
            return (
                "%s/%s" % (ddir, get_c3fit_file(dat_d, c3fit_d, c3_d, ins_d)),
                get_c3fit_kpath(dat_d, c3fit_d, c3_d, ins_d) )
        else : ValueError, ins_d['kind']
    elif 'c2h'   == kind : 
        c2_d    = lc_d['c2_d']
        return (
            '%s/%s' % (ddir, get_hadspec_file(dat_d, c2_d)), 
            get_hadspec_kpath(dat_d, c2_d))
    elif 'c2lec'   == kind : 
        c2lec_d    = lc_d['c2_d']
        return (
            '%s/%s' % (ddir, get_c2lec_file(dat_d, c2lec_d)), 
            get_c2lec_kpath(dat_d, c2lec_d))
    elif 'c3'   == kind :
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        if 'pdecay' == ins_d['kind'] :
            c3_d1 = c3_d
            # XXX adhoc fix: handle case when req. dat_d.kind=aff and op_tpol is in lc_d.c2_d.tpol 
            #     (set by conv2hdf_c2pt_aff_chop)
            #if (    'aff' == dat_d['kind'] 
                    #and not 'op_tpol' in c3_d 
                    #and 'c2_d' in lc_d and 'tpol' in lc_d['c2_d'] ) : 
                #c3_d1 = dictnew(c3_d, op_tpol=lc_d['c2_d']['tpol'])
            return (
                '%s/%s' % (ddir, get_c3pt_pdecay_file(dat_d, c3_d1, ins_d)), 
                get_c3pt_pdecay_kpath(dat_d, c3_d1, ins_d))
        else: 
            return ('%s/%s' % (ddir, get_c3pt_file(dat_d, c3_d, ins_d)), 
                    get_c3pt_kpath(dat_d, c3_d, ins_d))
    elif 'prop_ft' == kind :
        return ('%s/%s' % (ddir, get_prop_ft_file(dat_d, lc_d)),
                get_prop_ft_kpath(dat_d, lc_d))
    elif 'npr' == kind :
        return ('%s/%s' % (ddir, get_npr_file(dat_d, lc_d)),
                get_npr_kpath(dat_d, lc_d))
    else : raise ValueError(kind)
