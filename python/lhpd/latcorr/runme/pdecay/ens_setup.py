
import math
import numpy as np
import lhpd



class srcsnkpair :
    def __init__(self, tsep, smtag) :
        self.tsep       = tsep
        self.smtag      = smtag

    def __hash__(self) : 
        return (hash(self.tsep) 
                ^ hash(self.smtag))
    def __eq__(self, oth) :
        return (self.tsep == oth.tsep 
                and self.smtag      == oth.smtag)
    def __ne__(self, oth) :
        return not self.__eq__(oth)
    # since srcsnkpair has tsep, using it as key for c2pt map will result in 
    # 2pt functions stored len(tsep_list) times; fix: add special value specifically 
    # for that; when building c2pt map, check if it has already been inserted
    def key2pt(self) : 
        return srcsnkpair(0, self.smtag)
    def strkey(self) :
        return "%s_dt%d" % (self.get_smtag, self.get_tsep())
    def __str__(self) :
        return '%s(T=%d)' % (self.smtag, self.tsep)
    def __repr__(self): return self.__str__() 
    # TODO add str repr
    def get_smtag(self)     : return self.smtag
    def get_tsep(self)      : return self.tsep

class mc_case :
    tpol_map = {
        '3' : lhpd.latcorr.tpol_map['posSzplus'],
        # TODO add others
    }

    def __init__(self, had_src, tpol_src, p3src, had_snk, tpol_snk, p3snk) :
        self.had_src    = had_src
        self.tpol_src   = tpol_src
        self.p3src      = p3src
        self.had_snk    = had_snk
        self.tpol_snk   = tpol_snk
        self.p3snk      = p3snk
    
    def __hash__(self) :
        return (hash(str(self.had_src))
                ^ hash(str(self.tpol_src))
                ^ hash(tuple(self.p3src)) 
                ^ hash(str(self.had_snk))
                ^ hash(str(self.tpol_snk))
                ^ hash(tuple(self.p3snk)))
    def __eq__(self, oth) :
        return (self.tpol_src == oth.tpol_src and self.had_src == oth.had_src and tuple(self.p3src) == tuple(oth.p3src)
            and self.tpol_snk == oth.tpol_snk and self.had_snk == oth.had_snk and tuple(self.p3snk) == tuple(oth.p3snk))
    def __ne__(self, oth) :
        return not self.__eq__(oth)
    def __str__(self) :
        return '<%s_%s%s|%s_%s%s>' % (
                self.had_snk, self.tpol_snk, self.p3snk,
                self.had_src, self.tpol_src, self.p3src)
    def __repr__(self): return self.__str__() 
    def get_had_src(self)   : return self.had_src
    def get_tpol_src(self)  : return self.tpol_src
    def get_p3src(self)     : return self.p3src
    def get_had_snk(self)   : return self.had_snk
    def get_tpol_snk(self)  : return self.tpol_snk
    def get_p3snk(self)     : return self.p3snk



# FIXME data_top is not used
class ens_data_get :
    """ 
        ss  = ( tsep[int], sm_tag[str] )
        mc  = ( p3src[tuple?], p3snk[tuple?] }
    """
    def __init__(self, **kw) :
        self.__dict__['mutable_attr'] = None # for __setattr__
        #self.ama        = kw["ama"]
        #self.flav       = kw["flav"]
        #self.op         = kw["op"]
        self.data_in    = kw["data_in"]
        self.out_dir    = kw.get('out_dir', self.data_in)
        self.mlat       = kw.get('mlat')
        self.latsize    = kw.get('latsize', None)
        self.c2fit_tag  = kw.get('c2fit_tag', None)
        #self.c2fit_dgrp = kw.get("c2fit_dgrp")
        self.VERBOSE    = kw.get('VERBOSE', 0)
        self.log        = kw.get('log')
        self.twopt_avg_rot = kw.get('twopt_avg_rot', False)
        self.dslice     = kw.get('dslice', np.s_[:])
        self.use_tp3_for_tp3c = kw.get('use_tp3_for_tp3c', False) 
        if None is self.log : self.log = lhpd.log()
        # XXX must be set LAST!
        self.mutable_attr = [ 'c2fit_tag' ]

    def __setattr__(self, p, v) :
        if None is self.mutable_attr or p in self.mutable_attr : 
            self.__dict__[p] = v
        else : raise AttributeError(p)

    def get_c3pt(self, had, mgamma, sm, p3src, p3snk, tsep, 
                 op, op_flav, op_tpol):
        p3src = np.asarray(p3src)
        p3snk = np.asarray(p3snk)
        
        h5fname, h5kpath = get_lcdb_loc(
                dict(kind='ub', data_dir=self.data_in),
                dict(kind='c3',
                     c3_d=dict(had=had, mgamma=mgamma, smsrcsnk=sm, psnk=p3snk, tsep=tsep),
                     ins_d=dict(kind='pdecay', op=op, flav=op_flav)))
        h5f = h5py.File(h5fname, 'r')
        h5d = h5f[h5kpath]

        # XXX mom.proj. computed with ft_sign=-1, matching the definition
        # \sum_{y,z} exp(-i*y*psnk -i*z*qext) <Pi(y) Op(z) N(0)>
        qext_list = h5d.attrs['qext_list']
        q3ext = p3src - p3snk # sic! this is outgoing antilepton momentum
        i_qext= lhpd.np_find_first(q3ext, qext_list)[0]
        def get_Tg(i_g) : 
            return h5d[:, i_g, i_qext]
        res = lhpd.latcorr.calc_tpol_fromGamma(op_tpol, get_Tg)
        res = res[self.dslice]
        h5f.close()
        return res

    def get_c3pt_optype_flav_chi_tpol(self, had, mgamma, sm, p3src, p3snk, tsep, op_Y, op_flav, op_chi1, op_chi2, tpol) :
        if self.use_tp3_for_tp3c and 'tp3c' == op_Y : op_Y = 'tp3' # sic! on 24c we need this fix
            
        if   'R' == op_chi2 : g5proj = 0.5 * (gamma_id + gamma5)
        elif 'L' == op_chi2 : g5proj = 0.5 * (gamma_id - gamma5)
        tpol_matr = lhpd.latcorr.tpol_asmatr(tpol)
        tpol_g5proj_matr = np.dot(tpol_matr, g5proj)
        
        return self.get_c3pt(had, mgamma, sm, p3src, p3snk, tsep, 
                        "%s_%s%s" % (op_Y, op_chi1, op_chi2), 
                        op_flav, tpol_g5proj_matr)


    def get_c3pt_chan_tpol(self, had, mgamma, sm, p3src, p3snk, tsep, op_X, op_chi1, op_chi2, tpol) :
        def getUD(op_Y) : 
            return self.get_c3pt_optype_flav_chi_tpol(had, mgamma, sm, p3src, p3snk, tsep, 
                                op_Y, 'UD', op_chi1, op_chi2, tpol)
        def getS(op_Y)  : 
            return self.get_c3pt_optype_flav_chi_tpol(had, mgamma, sm, p3src, p3snk, tsep, 
                                op_Y, 'S',  op_chi1, op_chi2, tpol)
        if   'S1' == op_X : return  getS('tp1')  +getS('tp2')
        elif 'S2' == op_X : return -getS('tp2')  +getS('tp5')
        elif 'S3' == op_X : return  getS('tp3c') +getS('tp4')
        elif 'S4' == op_X : return -getS('tp1')  -getS('tp5')
        elif 'U1' == op_X : return -getUD('tp2') +getUD('tp3c') +getUD('tp4') +getUD('tp5')
        elif 'U0' == op_X : return(-getUD('tp2') +getUD('tp3c') +getUD('tp4') +getUD('tp5')) / 2.**0.5
        elif 'E1' == op_X : return(2*getUD('tp1') +getUD('tp2') +getUD('tp3c') +getUD('tp4') +getUD('tp5')) / 6.**0.5
        else : raise ValueError(op_X)


    def get_c2pt_orig_(self, had, smsrcsnk, p3, tpol, ama='ub', twopt_avg_rot=False):
        h5fname, h5kpath = get_lcdb_loc(
                dict(kind='bin', ama=ama, data_dir=self.data_in),
                dict(kind='c2h', 
                     c2_d=dict(had=had, smsrcsnk=smsrcsnk)))
        h5f = h5py.File(h5fname, 'r')
        h5d = h5f[h5kpath]
        assert([b"Tg%d" % g for g in range(16) ] == list(h5d.attrs['tpol_list']))
        psnk_list = h5d.attrs['psnk_list']
        i_p = lhpd.np_find_first(p3, psnk_list)[0]
        def get_Tg(i_g) :
            if twopt_avg_rot :
                p3_std      = np.sort(np.abs(p3))
                p_std_list  = np.sort(np.abs(psnk_list), axis=1)
                i_p_list    = lhpd.np_find_all(p3_std, p_std_list)[0]
                return np.mean([ h5d[:, i_g, i_p] for i_p in i_p_list], axis=0)
            else :
                i_p     = lhpd.np_find_first(p3, psnk_list)[0]
                return h5d[:, i_g, i_p]
        res = lhpd.latcorr.calc_tpol_fromGamma(tpol, get_Tg)
        res = res[self.dslice]
        h5f.close()
        return res

    # LEC
    def get_c2lec_orig_(self, had, smsrcsnk, p3, tpol, op_X, chi12, ama='ub', twopt_avg_rot=False):
        h5fname, h5kpath = get_lcdb_loc(
                dict(kind='bin', ama=ama, data_dir=self.data_in),
                dict(kind='c2lec', 
                     c2_d=dict(had=had, smsrcsnk=smsrcsnk, op='%s_%s' % (op_X, chi12))))
        h5f = h5py.File(h5fname, 'r')
        h5d = h5f[h5kpath]
        assert([b"Tg%d" % g for g in range(16) ] == list(h5d.attrs['tpol_list']))
        psnk_list = h5d.attrs['psnk_list']
        i_p = lhpd.np_find_first(p3, psnk_list)[0]
        def get_Tg(i_g) :
            if twopt_avg_rot :
                p3_std      = np.sort(np.abs(p3))
                p_std_list  = np.sort(np.abs(psnk_list), axis=1)
                i_p_list    = lhpd.np_find_all(p3_std, p_std_list)[0]
                return np.mean([ h5d[:, i_g, i_p] for i_p in i_p_list], axis=0)
            else :
                i_p     = lhpd.np_find_first(p3, psnk_list)[0]
                return h5d[:, i_g, i_p]

        tpol_P = tpol
        assert(2 == len(chi12))
        if   'R' == chi12[1] : g5proj = 0.5 * (gamma_id + gamma5)
        elif 'L' == chi12[1] : g5proj = 0.5 * (gamma_id - gamma5)
        tpol_P = np.dot(lhpd.latcorr.tpol_asmatr(tpol), g5proj)
        res = lhpd.latcorr.calc_tpol_fromGamma(tpol_P, get_Tg)
        h5f.close()
        if 0 is res : return 0
        else : return res[self.dslice]
        return res
    def get_c2lec_chan_tpol(self, had, smsrcsnk, p3, tpol, op_chi12, ama='ub', twopt_avg_rot=False) :
        return ( self.get_c2lec_orig_(had, smsrcsnk, p3, tpol, 'tp1', op_chi12)
                +self.get_c2lec_orig_(had, smsrcsnk, p3, tpol, 'tp2', op_chi12))


    def get_twopt_src(self, mc, ss) :
        if 3 <= self.VERBOSE : 
            self.log.write("get_twopt_src: mc=%s  ss=%s" % (mc, ss))
        return self.get_c2pt_orig_(mc.had_src, ss.smtag, mc.p3src, 
                mc.tpol_src, twopt_avg_rot=self.twopt_avg_rot)
    if False :
        def get_twopt_snk(self, mc, ss) :
            if 3 <= self.VERBOSE : 
                self.log.write("get_twopt_snk: mc=%s  ss=%s" % (mc, ss))
            smtag = ss.get_smtag_symm()
            return self.get_twopt_symm_mom_tpol_(smtag,
                        self.get_c2symm_p3snk(mc, ss), mc.get_tpol_c2())

        def get_twopt_fit_mom_tpol_(self, p3, tpol) :
            # XXX no dslice for fit params
            p3      = np.asarray(p3)
            snk_str = "px%dpy%dpz%d" % tuple(p3)
            #h5f     = h5py.File(self.c2fit_file, 'r')
            #h5kpath = "/%s_%s/%s" % (self.had, tpol, snk_str)
            #h5d     = h5f[h5kpath]
            h5d     = self.c2fit_dgrp[snk_str]
            assert('c2pt_c0' == h5d.attrs['pname'][0])
            assert('c2pt_e0' == h5d.attrs['pname'][1])
            assert('c2pt_c1' == h5d.attrs['pname'][2])
            assert('c2pt_de1_0' == h5d.attrs['pname'][3])
            res     = h5d['p']
            #h5f.close()
            if 3 <= self.VERBOSE : 
                self.log.write("get_twopt_fit_mom_tpol_: %s[%s]" % (
                        self.c2fit_dgrp.file.filename, self.c2fit_dgrp.name))
                self.log.write("get_twopt_fit_mom_tpol_: p3=%s  %s  pol=%s" % (p3, snk_str, tpol))
            return res

        def get_twopt_fit_src(self, mc, ss) :
            if 3 <= self.VERBOSE : 
                self.log.write("get_twopt_fit_src: mc=%s  ss=%s" % (mc, ss))
            return self.get_twopt_fit_mom_tpol_(self.get_c2symm_p3src(mc, ss), mc.get_tpol_c2()) 
        def get_twopt_fit_snk(self, mc, ss) :
            if 3 <= self.VERBOSE : 
                self.log.write("get_twopt_fit_snk: mc=%s  ss=%s" % (mc, ss))
            return self.get_twopt_fit_mom_tpol_(self.get_c2symm_p3snk(mc, ss), mc.get_tpol_c2()) 

    # TODO move to config_lcdb
    def get_c2fit_file(self, had, tpol, sm) :
        return "%s/c2fit.%s_%s.%s.h5" % (self.out_dir, had, tpol, sm)
    def get_c2fit_kpath(self, psnk, fit_tag) :
        psnk_tag = "px%dpy%dpz%d" % tuple(psnk)
        return "/c2fit/%s/%s" % (fit_tag, psnk_tag)

    def get_pdff_file(self) :
        return "%s/pdff.h5" % (self.out_dir,)

    def get_c2pt_fitp(self, had, tpol, sm, p3) :
        """ returns rotational equivalent """
        h5f = h5py.File(self.get_c2fit_file(had, tpol, sm), 'r')
        if self.twopt_avg_rot : 
            p3 = np.abs(p3)
            p3.sort()
        res_p = h5f["%s/p" % (self.get_c2fit_kpath(p3, self.c2fit_tag),)][()]
        # XXX no dslice for fit params: if dslice is required, must redo fit with proper resampling
        h5f.close()
        return res_p

    def get_c2pt_fitp_v2(self, had, tpol, sm, p3, c2fit_d) :
        """ returns rotational equivalent """
        if self.twopt_avg_rot : 
            p3 = np.abs(p3)
            p3.sort()
        h5fname, h5kpath = get_lcdb_loc(
            dict(kind='bin', ama='ub', data_dir=self.out_dir),
            dict(kind='c2fit', c2fit_d=c2fit_d, c2_d=dict(had=had, tpol=tpol, smsrcsnk=sm, psnk=p3)))
        h5f = h5py.File(h5fname, 'r')
        res_p = h5f["%s/p" % (h5kpath,)][()]
        # XXX no dslice for fit params: if dslice is required, must redo fit with proper resampling
        h5f.close()
        return res_p

    def str_f(self) :
        return '%s.%s.%s.%s' % (
            self.ama, self.had, self.flav, self.op)

