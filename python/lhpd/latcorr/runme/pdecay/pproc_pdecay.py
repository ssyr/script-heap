#import ipdb
from future.utils import iteritems
import numpy as np
import h5py
import lhpd
from lhpd.misc import np_find_first, np_match, purge_keys, dict_copy_pop, dictnew
from lhpd.h5_io import h5type_attr_fix_
import os, sys, time, tempfile, errno
from lhpd.pymath.H4_tensor import *
from lhpd.limits import *

from past.builtins import execfile
execfile('scripts-runme/pproc_common.py')

from lhpd.pymath.gamma_matr import gamma_dgr as gamma
from lhpd.pymath.gamma_matr import gamma_id
from lhpd.pymath.gamma_matr import Gamma16_dgr as Gamma16
gamma_5 = Gamma16[15]
from lhpd.npr import *
from lhpd.npr.npr_common import *


def triquark_right(qa, qb, qc) :
    """ contract 3-quark on the right indices (bar{q}_a bar{q}_b bar_c)
        res[..., ks] = a[..., ic,is]*b[..., jc,js] * qc[kc,ks]  * eps[ic,jc,kc]
        all other indices broadcast
    """
    assert((NCOLOR,NSPIN) == qa.shape[-2:])
    assert((NCOLOR,NSPIN) == qb.shape[-2:])
    
    qa_r2    = np.roll(qa, 2, -2)
    qb_r2    = np.roll(qb, 2, -2)
    qc_r1    = np.roll(qc, 1, -2)
    res = ( ( (qa * qb_r2).sum(-1) - (qa_r2 * qb).sum(-1) )[...,None] * qc_r1 ).sum(-2)
    return res


def pdecay_contract_npr3q_right(qa_, qb_, qc_, op, flav) :
    """ contract right (qbar) indices """
    
    def uds(qa1, qb1, qc1) :
        gamma_id= Gamma16[0]
        cc      = Gamma16[10]
        g5      = Gamma16[15]
        cc_g5   = lhpd.np_ddot(cc, g5)
        gamma_g5    = [ np.dot(g, g5) for g in gamma ]
        cc_gamma    = [ np.dot(cc, g) for g in gamma ]
        cc_gamma_g5 = [ np.dot(cc, g) for g in gamma_g5 ]
        sigma       = [ Gamma16[n] for n in [3, 5, 6, 9, 10, 12] ]
        cc_sigma    = [ np.dot(cc, s) for s in sigma ]
        sigma_g5    = [ np.dot(s, g5) for s in sigma ]

        def contr(spinS, spinT) : 
            return triquark_right(qa1, np.dot(qb1, spinS.T), np.dot(qc1, spinT.T))
            
        if   'SS' == op : return contr(cc,    gamma_id)
        elif 'SP' == op : return contr(cc,    g5)
        elif 'PS' == op : return contr(cc_g5, gamma_id)
        elif 'PP' == op : return contr(cc_g5, g5)
        elif 'VV' == op : 
            return sum([ contr(cc_g, g) for cc_g, g in zip(cc_gamma,    gamma) ])
        elif 'VA' == op : 
            return sum([ contr(cc_g, g) for cc_g, g in zip(cc_gamma,    gamma_g5) ])
        elif 'AV' == op : 
            return sum([ contr(cc_g, g) for cc_g, g in zip(cc_gamma_g5, gamma) ])
        elif 'AA' == op : 
            return sum([ contr(cc_g, g) for cc_g, g in zip(cc_gamma_g5, gamma_g5) ])
        elif 'TT' == op : 
            return sum([ contr(cc_s, s) for cc_s, s in zip(cc_sigma,    sigma) ])
        elif 'TQ' == op : 
            return sum([ contr(cc_s, s) for cc_s, s in zip(cc_sigma,    sigma_g5) ])
        else : raise ValueError(op)
            
    N   = None
    qa  = qa_[..., :,:, N,N, N,N, :,:]
    qb  = qb_[..., N,N, :,:, N,N, :,:]
    qc  = qc_[..., N,N, N,N, :,:, :,:]
    
    if   'uds' == flav :
        return uds(qa, qb, qc)
    elif 'udu' == flav : # also usu, dsd
        return ( uds(qa, qb, qc) - uds(qc, qb, qa) )
    else : raise ValueError(flav)

def pdecay_contract_npr3q_left(qa, qb, qc, op, flav) :
    """ contract left (q) indices """
    qsh = qa.shape
    assert(qb.shape == qsh) 
    assert(qc.shape == qsh)
    n   = len(qsh)
    transp_sh = tuple(np.r_[: n - 4]) + (n-2, n-1, n-4, n-3)
    # create new transposed arrays to ensure locality in contractions
    return pdecay_contract_npr3q_right(
        np.array(qa.transpose(transp_sh)), 
        np.array(qb.transpose(transp_sh)), 
        np.array(qc.transpose(transp_sh)), 
        op, flav)

def calc_npr_pdecay(cfgkey_list, dl_bin_by,
                dat_d, p_d, 
                mom012_list, # [i_mom, q1/q2/q3, mu]
                npr3q_op_flav,
                rsplan,
                data_in=None,
                VERBOSE=True) :
    """
        dl_ex, dl_sl    data_list's (meas_list's) for ex and sl
        dl_bin_by       hash func for binning/ama
    """
    if VERBOSE: print("# WARN: cfgkey_list ignored")
    if None is data_in : data_in = dat_d['data_dir']
    mom012_list = np.asarray(mom012_list)
    n_mom012    = len(mom012_list)
    mom_list    = lhpd.np_uniq(mom012_list.reshape((-1, mom012_list.shape[-1])))
    n_mom       = len(mom_list)

    n_op_flav   = len(npr3q_op_flav)
    npr_dtype   = np.complex

    # loop over ama and create dict
    ama_list    = ['ex', 'sl']
    h5vars      = {}
    for ama in ama_list :
        h5fname, h5kpath  = get_lcdb_loc(
                dictnew(dat_d, data_dir=data_in, ama=ama, kind='all'),
                dictnew(p_d, kind='prop_ft'))
        h5d     = h5py.File(h5fname, 'r')[h5kpath]
        ml      = lhpd.h5_io.h5_get_hugeattr(h5d, 'mom_list')
        dl      = lhpd.h5_io.h5_get_datalist(h5d)
        binmap  = categ_bin(dl, dl_bin_by)
        key_bin = binmap.keys() ; key_bin.sort()
        h5vars[ama] = dict(h5d=h5d, val=h5d[()], ml=ml, dl=dl, binmap=binmap, key_bin=key_bin)

    # find&match bins and ama
    key_bin = h5vars['sl']['key_bin']
    for ama, h5v in iteritems(h5vars) : assert(h5v['key_bin'] == key_bin)
    n_bins      = len(key_bin)
    m_sl_all, m_ex_all = lhpd.np_match(h5vars['sl']['dl'], h5vars['ex']['dl'], hashfunc=lhpd.h5_io.h5_meas_hash)
    if VERBOSE : print("# dl_ex=%d  dl_sl=%d  m_ex/m_sl=%d/%d" % (
                        len(h5vars['ex']['dl']), len(h5vars['sl']['dl']), len(m_ex_all), len(m_sl_all)))

    def get_prop_ft(ama, k, mom_list) :
        h5v     = h5vars[ama]
        h5d, qprop, ml, dl, dl_k = h5v['h5d'], h5v['val'], h5v['ml'], h5v['dl'], h5v['binmap'][k]
        data_i  = [ i[0] for i in lhpd.np_find_first_list(dl_k, dl) ]
        mom_i   = [ i[0] for i in lhpd.np_find_first_list(mom_list, ml) ]
        res = np.empty((len(dl_k), len(mom_list),) + h5d.shape[2:], h5d.dtype)
        for i_dst, i_src in enumerate(data_i) : 
            res[i_dst] = qprop[i_src][mom_i]
        return res

    def calc_qbarq(qprop_out, qprop_in) :
        """ [qbar Gamma16 q] contraction of propagators
            <q(p2) [qbar G q](x) qbar(p1)>[i,j] = Sout(p2,x)[i,k] G[k,l] rev(Sin(p1,x))[l,j]
            qprop_in, qprop_out propagators {p1/p2 <- x}
        """
        qbar_g = np.einsum('...abcd,ide->...iabce', qprop_out, Gamma16)
        qprop_in_rev = qqbar_rev(qprop_in)
        res = np.einsum('...iabcd,...cdef->...iabef', qbar_g, qprop_in_rev)
        return res    

    def calc_ub(d_sl, d_ex, m_sl, m_ex) : 
        return d_sl.mean(0) + (d_ex[m_ex] - d_sl[m_sl]).mean(0)

    # output datasets
    npr3q_h5fname, npr3q_kpath = get_lcdb_loc(dictnew(dat_d), dict(kind='npr', npr_d=dict(kind='pdecay')))
    lhpd.mkpath(os.path.dirname(npr3q_h5fname))
    npr3q_h5f = h5py.File(npr3q_h5fname, 'a')
    lhpd.h5_io.h5_purge_keys(npr3q_h5f, [npr3q_kpath])
    npr3q_h5g = npr3q_h5f.require_group(npr3q_kpath)

    qbargammaq_h5d      = npr3q_h5g.require_dataset('qbargammaq', 
            (n_bins, n_mom012, NSPIN**2, NSPIN**2), npr_dtype, fletcher32=True)
    npr3q_vertex_h5d    = npr3q_h5g.require_dataset('npr3q_vertex', 
            (n_bins, n_mom012, n_op_flav, n_op_flav), npr_dtype, fletcher32=True)
    qprop_inv_proj_h5d  = npr3q_h5g.require_dataset('qprop_inv_proj', 
            (n_bins, n_mom, NSPIN**2), npr_dtype, fletcher32=True)


    # calc&save qprop inv proj data
    # [i_meas, ic,is, ic,is] - for qprop_inv analysis
    qprop_sel_ub= np.empty((n_bins, n_mom) + (NCOLOR,NSPIN,)*2, npr_dtype)
    for i_k, k in enumerate(key_bin) :
        qprop_sel_ex= get_prop_ft('ex', k, mom_list)
        qprop_sel_sl= get_prop_ft('sl', k, mom_list)
        m_sl, m_ex  = lhpd.np_match(h5vars['sl']['binmap'][k], h5vars['ex']['binmap'][k], 
                    hashfunc=lhpd.h5_io.h5_meas_hash)
        if VERBOSE : print("# i_k=%d  k=%s  dl_k_ex=%d  dl_k_sl=%d  m_k_ex/m_k_sl=%d/%d" % (
                i_k, str(k), len(h5vars['ex']['binmap'][k]), len(h5vars['sl']['binmap'][k]),
                len(m_ex), len(m_sl)))
        qprop_sel_ub[i_k] = calc_ub(qprop_sel_sl, qprop_sel_ex, m_sl, m_ex)
    qprop_sel_rs = lhpd.resample(qprop_sel_ub, rsplan)
    # invert and trace resampled props
    qprop_sel_inv_rs = qqbar_inv(qprop_sel_rs)
    qprop_sel_inv_proj_rs = np.einsum('...abac,icb->...i', qprop_sel_inv_rs, Gamma16) / NCOLSPIN
    qprop_inv_proj_h5d[()]= np.asarray(qprop_sel_inv_proj_rs)


    # calc&save qbarq, npr3q data
    for i_mom012, mom012 in enumerate(mom012_list) :
        if VERBOSE : print("# mom012=%s" % (mom012,))
        # [i_meas, q1/q2/q3, ic,is, ic,is]
        qprop_ub    = np.empty((n_bins, 3,) + (NCOLOR,NSPIN,)*2, npr_dtype)
        qprop0in_ub = np.empty((n_bins, 1,) + (NCOLOR,NSPIN,)*2, npr_dtype)
        # [i_meas, gamma, ic,is, ic,is] : <qbar(p2) Op(p1) q(p0)>
        qbarq_ub    = np.empty((n_bins, NSPIN**2,) + (NCOLOR,NSPIN,)*2, npr_dtype)
        # [i_meas, op_flav, ic,is, ic,is, ic,is, is]
        npr3q_ub    = np.empty((n_bins, n_op_flav,) + (NCOLOR,NSPIN,)*3 + (NSPIN,), npr_dtype)
        
        # XXX compute qqbar = <q(p2) [qbar(x) gamma q(x)] qbar(-p0)> 
        #     so that vertex momentum[what convention?] = -p0 -p2 = +p1 
        mom0neg     = -mom012[..., 0:1, :] -two_ft_twist
        for i_k, k in enumerate(key_bin) :
            if VERBOSE : print("# k=%s" % (k,))
            for ama, h5v in iteritems(h5vars) :
                n_k = len(h5v['binmap'][k])
                assert(0 < n_k)
                # read qprop and calc contractions
                qprop       = get_prop_ft(ama, k, mom012)
                qprop0in    = get_prop_ft(ama, k, mom0neg)
                qprop_rev   = qqbar_rev(qprop)
                npr3q       = np.empty((n_k,) + npr3q_ub.shape[1:], npr_dtype)
                for i_op_flav, (op, flav) in enumerate(npr3q_op_flav) :
                    # sic! revert props to have S(x0,p) = <q(x0) qbar(p)> 
                    # and then contract on left (q) indices
                    npr3q[:, i_op_flav] = pdecay_contract_npr3q_left(
                            qprop_rev[:,0], qprop_rev[:,1], qprop_rev[:,2], op, flav)
                h5v['qprop']    = qprop
                h5v['qprop0in'] = qprop0in
                h5v['npr3q']    = npr3q
                h5v['qbarq']    = calc_qbarq(qprop[:, 2], qprop0in[:, 0]) # XXX qprop0in[:, 0] is revved

            # bias-correct, bin
            m_sl, m_ex  = lhpd.np_match(h5vars['sl']['binmap'][k], h5vars['ex']['binmap'][k], 
                        hashfunc=lhpd.h5_io.h5_meas_hash)
            assert(0 < len(m_sl) and 0 < len(m_ex)) 
            qprop_ub[i_k]   = calc_ub(h5vars['sl']['qprop'],    h5vars['ex']['qprop'],    m_sl, m_ex)
            qprop0in_ub[i_k]= calc_ub(h5vars['sl']['qprop0in'], h5vars['ex']['qprop0in'], m_sl, m_ex)
            qbarq_ub[i_k]   = calc_ub(h5vars['sl']['qbarq'],    h5vars['ex']['qbarq'],    m_sl, m_ex)
            npr3q_ub[i_k]   = calc_ub(h5vars['sl']['npr3q'],    h5vars['ex']['npr3q'],    m_sl, m_ex)


        # resample
        qprop_rs    = lhpd.resample(qprop_ub,    rsplan)
        qprop0in_rs = lhpd.resample(qprop0in_ub, rsplan)
        qbarq_rs    = lhpd.resample(qbarq_ub,    rsplan)
        npr3q_rs    = lhpd.resample(npr3q_ub,    rsplan)

        # inverse props for amp
        qprop_inv_rs            = qqbar_inv(qprop_rs)
        qprop_inv_rev_rs        = qqbar_rev(qprop_inv_rs)
        qprop0in_inv_rev_rs     = qqbar_rev(qqbar_inv(qprop0in_rs))

        # amp qbar <q3(p3) [bar{q3}(x) Gamma q1(x)
        qbarq_amp_rs = np.einsum('...ghab,...iabef->...ighef',
                qprop_inv_rs[:,2],
                np.einsum('...iabcd,...cdef->...iabef', 
                        qbarq_rs, 
                        qprop0in_inv_rev_rs[:,0]))
        # project qbar on Gamma16
        qbarq_amp_proj_rs = np.einsum('...ighgf,jfh->...ij', qbarq_amp_rs, Gamma16) / NCOLSPIN

        # amp 3q ops with inv_rev [S(x0,p)]^{-1} = [<q(x0) qbar(p)>]^{-1}
        npr3q_amp_rs = np.einsum('...iabcdefx,...abgh->...ighcdefx', npr3q_rs,     qprop_inv_rev_rs[:,0])
        npr3q_amp_rs = np.einsum('...iabcdefx,...cdgh->...iabghefx', npr3q_amp_rs, qprop_inv_rev_rs[:,1])
        npr3q_amp_rs = np.einsum('...iabcdefx,...efgh->...iabcdghx', npr3q_amp_rs, qprop_inv_rev_rs[:,2])

        # projection on tree-lev tensors
        qprop_id = np.identity(NCOLSPIN, npr_dtype).reshape(NCOLOR,NSPIN, NCOLOR,NSPIN)
        t_op_flav = np.array([ pdecay_contract_npr3q_left(qprop_id, qprop_id, qprop_id, op, flav).reshape(-1) 
                               for op, flav in npr3q_op_flav ])
        tt_metric = np.dot(t_op_flav, t_op_flav.T.conj())
        flav_list = np.array([ flav for op,flav in npr3q_op_flav ])
        tt_metric[ np.where(flav_list[:,None] != flav_list[None,:]) ] = 0
        tt_metric_inv = np.linalg.inv(tt_metric)
        # [i_op_flav_proj, color3spin4]
        tproj_op_flav = np.dot(tt_metric_inv.T, t_op_flav.conj())

        # project 3q on spin/color structures
        # [i_bin, i_mom012, op_flav_contr, op_flav_proj]
        npr3q_amp_proj_rs = np.dot(
                    npr3q_amp_rs.reshape(npr3q_amp_rs.shape[:-7] + (-1,)),
                    tproj_op_flav.T)

        # save this mom012
        qbargammaq_h5d[:, i_mom012]     = np.asarray(qbarq_amp_proj_rs)
        npr3q_vertex_h5d[:, i_mom012]   = np.asarray(npr3q_amp_proj_rs)
    
    for ama, h5v in iteritems(h5vars) : 
        h5v['val'] = None
        h5v['h5d'].file.close()
    h5vars = None

    # save metadata
    npr3q_h5g['mom012_list']    = mom012_list
    npr3q_h5g['mom_list']       = mom_list
    npr3q_h5g['latsize']        = np.asarray(latsize)

    d = qbargammaq_h5d
    d.attrs['dim_spec']         = np.asarray(['i_data', 'i_mom012', 'gamma_contr', 'gamma_proj'])
    d.attrs['latsize']          = np.asarray(latsize)
    d.attrs['gamma_list']       = np.r_[:16]
    lhpd.h5_io.h5_set_rsplan(d, rsplan)

    d = npr3q_vertex_h5d
    d.attrs['dim_spec']         = np.asarray(['i_data', 'i_mom012', 'op_flav_contr', 'op_flav_proj' ])
    d.attrs['latsize']          = np.asarray(latsize)
    d.attrs['op_flav_list']     = np.asarray([ "%s_%s" % op_flav for op_flav in npr3q_op_flav ])
    lhpd.h5_io.h5_set_rsplan(d, rsplan)

    d = qprop_inv_proj_h5d
    d.attrs['dim_spec']         = np.asarray(['i_data', 'i_mom', 'gamma_proj'])
    d.attrs['latsize']          = np.asarray(latsize)
    d.attrs['gamma_list']       = np.r_[:16]
    lhpd.h5_io.h5_set_rsplan(d, rsplan)

    npr3q_h5f.flush()
    npr3q_h5f.close()
