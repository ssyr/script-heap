#!/usr/bin/env python2

import aff
import os, sys
import re
import lhpd
from lhpd.aff_io import aff_copy_recursive

def print_usage(prg) :
    print("""Usage:
%s  --data-in=<data-in-dir>  --data-out=<data-out-dir>  <file1> ...
""" % (prg,))
#%s  <file-in> <file-out>   <key1-in>  <key1-out>  ...

argv    = list(sys.argv)

data_in, data_out = None, None
#key_in, key_out = None, None
while 1 < len(argv) and argv[1].startswith('--') :
    if   argv[1].startswith('--data-in=') : 
        data_in     = re.match('^--data-in=(.+)$',  argv[1]).groups()[0]
    elif argv[1].startswith('--data-out=') : 
        data_out    = re.match('^--data-out=(.+)$', argv[1]).groups()[0]
    #elif argv[1].startswith('--key-in=') : 
        #key_in    = re.match('^--key-in=(.+)$', argv[1]).groups()[0]
    #elif argv[1].startswith('--key-out=') : 
        #key_out    = re.match('^--key-out=(.+)$', argv[1]).groups()[0]
    else : 
        sys.stderr.write("bad arg='%s'\n" % argv[1])
        print_usage(argv[0] or 'me')
        sys.exit(1)
    
    argv.pop(1)

assert(not None is data_in)
assert(not None is data_out)
#assert(not None is key_in)
#assert(not None is key_out)

if len(argv) < 2 :
    print_usage(argv[0]) 
    exit(1)


chi_list = ['RR', 'RL', 'LR', 'LL' ]
n_chi = len(chi_list)
x_op1 = 'tp3'
x_op2 = 'tp3c'
k1  = '/c3pt_pdecay/SS/proton/Tg15'
op12_list = [ ("%s_%s" % (x_op1, chi,), "%s_%s" % (x_op2, chi,)) for chi in chi_list ]

print("%s/FILE[%s?%s] -> %s/FILE[%s] ; op12_list=%s" % (
        data_in, x_op2, x_op1, data_out, x_op2, str(op12_list)))


while 1 < len(argv) :
    f1 = argv.pop(1)
    try :
        aff_r = aff.Reader("%s/%s" % (data_in, f1))
        aff_w = aff.Writer("%s/%s" % (data_out, f1))
        flav_list = aff_r.ls(k1)
        for flav in flav_list :
            k2 = "%s/%s" % (k1, flav)
            op_list = aff_r.ls(k2)
            
            n_op1, n_op2 = 0, 0
            for op1, op2 in op12_list : 
                if op1 in op_list : n_op1 += 1
                if op2 in op_list : n_op2 += 1

            have_op1, have_op2 = False, False
            if 0 < n_op1 : 
                if n_op1 != n_chi : 
                    raise RuntimeError('missing op1')
                have_op1 = True
            if 0 < n_op2 : 
                if n_op2 != n_chi : 
                    raise RuntimeError('missing op2')
                have_op2 = True

            print("%s\t%s\top1=%s\top2=%s" % (f1, flav, have_op1, have_op2))

            if   have_op2 :
                for op1, op2 in op12_list :
                    aff_copy_recursive(aff_r, "%s/%s" % (k2, op2), aff_w, "%s/%s" % (k2, op2))
            elif have_op1 :
                assert(not have_op2)
                for op1, op2 in op12_list :
                    aff_copy_recursive(aff_r, "%s/%s" % (k2, op1), aff_w, "%s/%s" % (k2, op2))
            else : raise RuntimeError('no op1 or op2')

            
            aff_r.close()
            aff_w.close()
                    
    except :
        exc = sys.exc_info()
        print("[%s->%s]/%s fail: %s" % (data_in, data_out, f1, repr(exc[1])))

