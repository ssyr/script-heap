from past.builtins import execfile
import numpy as np, itertools as it, h5py
import lhpd
from lhpd.latcorr import make_linkpath_list
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted, range_prod
from lhpd import dictnew
from lhpd.misc.strkey import *
from lhpd.misc import ldir_3d, space2full

latsize = np.r_[32,32,32,64]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]
q_bc_t  = ferm_bc[t_axis]

# XXX assume no ft twist
two_ft_twist= np.r_[0,0,0,0]
ft_twist    = 0.5 * two_ft_twist

meson_bc_t = q_bc_t**2
baryon_bc_t= q_bc_t**3

cfg_index   = lhpd.read_key_list('cfglist.ALLINDEX.n211', 'index')
#cfg_list    = lhpd.read_key_list('../list.cfg.run1.n100', 'samples')   # XXX read from cmdline instead

ama_ex      = 'ex'
ama_sl      = 'sl'
ama_list = [ama_ex, ama_sl]
def ama_str(ama) : return ama


ncoh_src_t      = 2 # number of coherent src in one group
if False : # csrc_order1
    def make_csrcgrp_list(ama, cfgkey):
        nsrc_ama_map = {
            ama_ex : [1,1,1,2],
            ama_sl : [2,2,2,4] }
        return lhpd.make_srcgrp_grid(
                nsrc_ama_map[ama], latsize, [0,0,0,0], [7,11,13,23],
                1 + cfg_index.index(cfgkey.lstrip('0')), t_axis, ncoh_src_t, 
                dx_it=[16,16,16,0])
else : # csrc_order2
    def make_csrcgrp_list(ama, cfgkey):
        #nsrc_ama_map = {
        #    ama_ex : [1,1,1,2],
        #    ama_sl : [2,2,2,4] }
        cfg_idx = 1 + cfg_index.index(cfgkey.lstrip('0'))
        lt = latsize[t_axis]
        dx_it = [16,16,16,0]
        dx_cfg= [7,11,13,23]
        if 'ex' == ama : 
            nsrc        = [1,1,1,2]
            return lhpd.make_srcgrp_grid(nsrc, latsize, [0,0,0,0], dx_cfg, cfg_idx, t_axis, ncoh_src_t, dx_it=dx_it)
        elif 'sl' == ama :
            nsrc    = [2,2,2,4]
            nsrc_t  = nsrc[t_axis]
            coh_dt  = lt // nsrc_t
            coh_n   = nsrc_t // ncoh_src_t
            nsrc_coh= np.array(nsrc)  ;  nsrc_coh[t_axis] = ncoh_src_t
            return np.concatenate([ lhpd.make_srcgrp_grid(
                    nsrc_coh, latsize, [0, 0, 0, coh_i*coh_dt], dx_cfg, cfg_idx, t_axis, ncoh_src_t, dx_it=dx_it)
                    for coh_i in range(coh_n) ])
        else : raise ValueError(ama)

def make_csrc_list(ama, cfgkey):
    # return flat(make_csrcgrp_list) to ensure consistency and sample order
    # XXX this assumes that the csrcgrp strip function records coherent samples 
    #     next to each other; this is the case for latcorr_conv2hdf
    #     (as wrapper for conv2hdf_c2pt_aff_chop,conv2hdf_qbarq_aff_chop,?)

    csrcgrp_list = np.asarray(make_csrcgrp_list(ama, cfgkey))
    assert(3 == len(csrcgrp_list.shape)) 
    assert(len(latsize) == csrcgrp_list.shape[-1])
    return csrcgrp_list.reshape(-1, len(latsize))

########### c2pt
pm1 = np.r_[-1, 0, 1]
c2pt_psnk_list = normsorted(list(int_vectors_maxnorm(3, 10)))

c2pt_hslab_len  = latsize[t_axis] // ncoh_src_t
c2pt_tpol_list  = [ 'Tg%d' % g for g in range(16) ]
c2pt_spec_list      = (
      [ dict(had='proton', tpol_list=c2pt_tpol_list, 
             psnk_list=c2pt_psnk_list, smsrcsnk=smsrcsnk, saved_bc_t=baryon_bc_t)
        for (smsrcsnk,) in it.product(['SS', 'SP'])] 
    )
hadspec_spec_list       = (
      [ dict(had=had, tpol_list=c2pt_tpol_list, 
             psnk_list=c2pt_psnk_list, smsrcsnk=smsrcsnk, saved_bc_t=baryon_bc_t)
        for had in ['nucleon', 'meson_ss', 'meson_sl', 'meson_ll' ]
        for smsrcsnk in ['SS', 'SP'] ] 
    )

op_tpol_list    = [ 'Tg%d' % g for g in range(16) ]
c3pt_qext_list  = normsorted(list(range_prod([pm1, pm1, pm1])))
c3pt_tsep_list  = [ 8, 9, 10 ]
c3pt_tlen       = c2pt_hslab_len        # sic! use c2pt contraction function

pdecay_c3   = dict(had='proton', op_tpol_list=op_tpol_list, smsrcsnk='SS', saved_bc_t=baryon_bc_t)
pdecay_ins  = dict(kind='pdecay')
pdecay_c3_ins_spec_list  = [ 
        ( dictnew(pdecay_c3, psnk=psnk, tsep=tsep, mgamma=mgamma),
          dictnew(pdecay_ins, op='tp%s_%s' % (tp, chi), flav=flav, qext_list=qext_list) ,)
        for tsep in c3pt_tsep_list
        for psnk in [ [1,1,1], [0,0,2] ]
        for tp in ['1', '2', '3c', '4' ,'5'] for chi in ['LL', 'LR', 'RL', 'RR'] 
        for flav, mgamma, psnk, qext_list in [
            ('UD', 'Tg15', [1,1,1], range_prod([[-2,-1, 0], [-2,-1, 0], [-2,-1, 0]])),
            ('UD', 'Tg15', [0,0,2], range_prod([[-1, 0, 1], [-1, 0, 1], [-3,-2,-1]])),
            ('S',  'Tg15', [0,0,1], range_prod([[-1, 0, 1], [-1, 0, 1], [-2,-1, 0]])),
            ('S',  'Tg15', [0,1,1], range_prod([[-1, 0, 1], [-2,-1, 0], [-2,-1, 0]])) ] 
]

data_dir_aff    = 'XXX'
data_out        = 'XXX'

# npr stuff
# pmin(xyz)=2pi/32*ainv=
#ainv=1.4GeV -> pmin(xyz)=(2*pi/32)*ainv = 0.27; 
# max(|k|)=7, 7*pmin=


# final list of momenta [i_mom, q0/q1/q2, mu]
npr_xr2x, npr_tr2x = np.r_[1:8], np.r_[2:16:2]
npr_xr4x, npr_tr4x = np.r_[1:4], np.r_[2:8:2]
def make_xyzt2x_iter() : return zip(npr_xr2x,npr_xr2x,npr_xr2x,npr_tr2x)
def make_xyzt4x_iter() : return zip(npr_xr4x,npr_xr4x,npr_xr4x,npr_tr4x)
def make_sign_iter(d) : 
    return it.product(*( ([1,-1],) * d ))

npr_mom_diag_list = []
if True :
#if False :
    h5_mom_list = h5py.File('ID32_npr_mom_list.h5', 'r')
    npr3q_mom012_list = lhpd.np_uniq(h5_mom_list['/npr3q_mom012_list'][()])
    h5_mom_list.close()
#elif True :
elif False :
    npr3q_mom012_2x_list = lhpd.make_vec_triangle_2x_list(make_xyzt2x_iter(), make_sign_iter(4))
    npr3q_mom012_4x_list = lhpd.make_vec_triangle_4x_list(make_xyzt4x_iter(), make_sign_iter(4))
    npr3q_mom012_list = np.concatenate((npr3q_mom012_2x_list, npr3q_mom012_4x_list))
    #npr3q_mom012_list = np.concatenate((            # all permut
            #npr3q_mom012_list, npr3q_mom012_list[:,[1,2,0]], npr3q_mom012_list[:,[2,0,1]],
            #npr3q_mom012_list[:,[2,1,0]], npr3q_mom012_list[:,[1,0,2]], npr3q_mom012_list[:,[0,2,1]] ))
    npr3q_mom012_list = lhpd.np_uniq(npr3q_mom012_list)     # ensure uniq
else: # debug/test
    npr3q_mom012_list = np.array([
        [ [ 2, -3,  4,  3], [ 2,  4, -1, -9], [-4, -1, -3,  5] ],
        [ [-2, -4, -3,  3], [-2,  1,  4, -9], [ 4,  3, -1,  5] ],
        [ [ 3, -4,  2,  3], [-4,  1,  2, -9], [ 1,  3, -4,  5] ] ])

npr_mom_diag_list = lhpd.normsorted(
      npr_mom_diag_list + [[0,0,0,0]]
      + sum([ [ (a*x,0,0,0), (0,a*y,0,0), (0,0,a*z,0), (0,0,0,a*t) ] 
              for (x,y,z,t), (a,) in it.product(make_xyzt2x_iter(), make_sign_iter(1)) ], [])
      + sum([ [ (a*x,b*y,0,0), (a*x,0,b*z,0), (a*x,0,0,b*t),
                (0,a*y,b*z,0), (0,a*y,0,b*t), (0,0,a*z,b*t) ] 
              for (x,y,z,t), (a,b) in it.product(make_xyzt2x_iter(), make_sign_iter(2)) ], [])
      + sum([ [ (a*x,b*y,c*z,0), (a*x,b*y,0,c*t), (a*x,0,b*z,c*t), (0,a*y,b*z,c*t) ]
              for (x,y,z,t), (a,b,c) in it.product(make_xyzt2x_iter(), make_sign_iter(3)) ], [])
      + [ (a*x,b*y,c*z,d*t) for (x,y,z,t), (a,b,c,d) in it.product(make_xyzt2x_iter(), make_sign_iter(4)) ]
    )

# list of momenta to strip
npr_mom_list = lhpd.normsorted(lhpd.np_uniq(np.concatenate([
        npr3q_mom012_list.reshape((-1, npr3q_mom012_list.shape[-1])), npr_mom_diag_list ])))

npr3q_op_flav= [ ('SS', 'uds'), ('PP', 'uds'), ('AA', 'uds'), ('SS', 'udu'), ('PP', 'udu')]
npr3q_rsplan = ('jk', 1)

# XXX debug
#npr3q_mom012_list = npr3q_mom012_list[-3:]
print("# npr3q_mom012_list.shape=%s" % (str(npr3q_mom012_list.shape),))
print("# npr_mom_diag_list.shape=%s" % (str(npr_mom_diag_list.shape),))
print("# npr_mom_list.shape=%s" % (str(npr_mom_list.shape),))

execfile('config_lcdb.py')

