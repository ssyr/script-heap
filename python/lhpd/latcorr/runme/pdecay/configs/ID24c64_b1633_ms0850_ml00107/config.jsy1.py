from past.builtins import execfile
import numpy as np
import itertools as it
import lhpd
from lhpd.latcorr import make_linkpath_list
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted, range_prod
from lhpd import dictnew
from lhpd.misc.strkey import *
from lhpd.misc import ldir_3d, space2full

latsize = np.r_[24,24,24,64]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]
q_bc_t  = ferm_bc[t_axis]
meson_bc_t = q_bc_t**2
baryon_bc_t= q_bc_t**3

cfg_index   = lhpd.read_key_list('cfglist.ALLINDEX.n363', 'index')
#cfg_list    = lhpd.read_key_list('../list.cfg.run1.n100', 'samples')   # XXX read from cmdline instead

ama_ex      = 'ex'
ama_sl      = 'sl'
ama_list = [ama_ex, ama_sl]
def ama_str(ama) : return ama

ncoh_src_t      = 2 # number of coherent src in one group
if False : # csrc_order1
    def make_csrcgrp_list(ama, cfgkey):
        nsrc_ama_map = {
            ama_ex : [1,1,1,2],
            ama_sl : [2,2,2,4] }
        return lhpd.make_srcgrp_grid(
                nsrc_ama_map[ama], latsize, [0,0,0,0], [7,11,13,23],
                1 + cfg_index.index(cfgkey.lstrip('0')), t_axis, ncoh_src_t, 
                dx_it=[12,12,12,0])
else : # csrc_order2
    def make_csrcgrp_list(ama, cfgkey):
        #nsrc_ama_map = {
        #    ama_ex : [1,1,1,2],
        #    ama_sl : [2,2,2,4] }
        cfg_idx = 1 + cfg_index.index(cfgkey.lstrip('0'))
        lt = latsize[t_axis]
        dx_it = [12,12,12,0]
        dx_cfg= [7,11,13,23]
        if 'ex' == ama : 
            nsrc        = [1,1,1,2]
            return lhpd.make_srcgrp_grid(nsrc, latsize, [0,0,0,0], dx_cfg, cfg_idx, t_axis, ncoh_src_t, dx_it=dx_it)
        elif 'sl' == ama :
            nsrc    = [2,2,2,4]
            nsrc_t  = nsrc[t_axis]
            coh_dt  = lt // nsrc_t
            coh_n   = nsrc_t // ncoh_src_t
            nsrc_coh= np.array(nsrc)  ;  nsrc_coh[t_axis] = ncoh_src_t
            return np.concatenate([ lhpd.make_srcgrp_grid(
                    nsrc_coh, latsize, [0, 0, 0, coh_i*coh_dt], dx_cfg, cfg_idx, t_axis, ncoh_src_t, dx_it=dx_it)
                    for coh_i in range(coh_n) ])
        else : raise ValueError(ama)

def make_csrc_list(ama, cfgkey):
    # return flat(make_csrcgrp_list) to ensure consistency and sample order
    # XXX this assumes that the csrcgrp strip function records coherent samples 
    #     next to each other; this is the case for latcorr_conv2hdf
    #     (as wrapper for conv2hdf_c2pt_aff_chop,conv2hdf_qbarq_aff_chop,?)

    csrcgrp_list = np.asarray(make_csrcgrp_list(ama, cfgkey))
    assert(3 == len(csrcgrp_list.shape)) 
    assert(len(latsize) == csrcgrp_list.shape[-1])
    return csrcgrp_list.reshape(-1, len(latsize))

########### c2pt
pm1 = np.r_[-1, 0, 1]
c2pt_psnk_list = normsorted(list(int_vectors_maxnorm(3, 10)))

c2pt_hslab_len  = latsize[t_axis] // ncoh_src_t
c2pt_tpol_list  = [ 'Tg%d' % g for g in range(16) ]
c2pt_spec_list      = (
      [ dict(had='proton', tpol_list=c2pt_tpol_list, 
             psnk_list=c2pt_psnk_list, smsrcsnk=smsrcsnk, saved_bc_t=baryon_bc_t)
        for (smsrcsnk,) in it.product(['SS', 'SP'])] 
    )
# problem: wrong meff for pion
# probable cause: mismatch of bc_t and saved_bc_t passed to conv2hdf_c2pt_aff_chop in case of mesons:
#   conv2hdf_c2pt_aff_chop knows meson_bc_t=+1, baryon_bc_t=-1; 
#   pretty sure I passed saved_bc_t=baryon_bc_t for both in first conversion run
#   so mesons got screwed up: sign flipping is more probable as t inc., so meff increases; 
#   this agrees with c2pt_pi,K(Lt-1) < 0 
#   Q: c2pt_nucleon(Lt-1) > 0 -- is it correct? 
# note that Qlua scripts saved mesons with bc_t=+1, baryons with bc_t=-1 -- checked
hadspec_spec_list       = (
      [ dict(had=had, tpol_list=c2pt_tpol_list, 
             psnk_list=c2pt_psnk_list, smsrcsnk=smsrcsnk) #, saved_bc_t=baryon_bc_t)
        for had in ['nucleon' ]
        for smsrcsnk in ['SS', 'SP'] ] 
    + [ dict(had=had, tpol_list=c2pt_tpol_list, 
             psnk_list=c2pt_psnk_list, smsrcsnk=smsrcsnk) #, saved_bc_t=meson_bc_t)
        for had in ['meson_ss', 'meson_sl', 'meson_ll' ]
        for smsrcsnk in ['SS', 'SP'] ] 
    )

op_tpol_list    = [ 'Tg%d' % g for g in range(16) ]
c3pt_qext_list  = normsorted(list(range_prod([pm1, pm1, pm1])))
c3pt_tsep_list  = [ 8, 9, 10 ]
c3pt_tlen       = c2pt_hslab_len        # sic! use c2pt contraction function

pdecay_c3   = dict(had='proton', op_tpol_list=op_tpol_list, smsrcsnk='SS', saved_bc_t=baryon_bc_t)
pdecay_ins  = dict(kind='pdecay')
pdecay_c3_ins_spec_list  = [ 
        ( dictnew(pdecay_c3, psnk=psnk, tsep=tsep, mgamma=mgamma),
          dictnew(pdecay_ins, op='tp%s_%s' % (tp, chi), flav=flav, qext_list=qext_list) ,)
        for tsep in c3pt_tsep_list
        for psnk in [ [1,1,1], [0,0,2] ]
        for tp in ['1', '2', '3', '4' ,'5'] for chi in ['LL', 'LR', 'RL', 'RR'] 
        for flav, mgamma, psnk, qext_list in [
            ('UD', 'Tg15', [1,1,1], range_prod([[-2,-1, 0], [-2,-1, 0], [-2,-1, 0]])),
            ('UD', 'Tg15', [0,0,2], range_prod([[-1, 0, 1], [-1, 0, 1], [-3,-2,-1]])),
            ('S',  'Tg15', [0,0,1], range_prod([[-1, 0, 1], [-1, 0, 1], [-2,-1, 0]])),
            ('S',  'Tg15', [0,1,1], range_prod([[-1, 0, 1], [-2,-1, 0], [-2,-1, 0]])) ] 
]

c2lec_spec_list = [ dict(had='nucleon', tpol_list=c2pt_tpol_list, 
             psnk_list=[[0,0,0]], smsrcsnk='SP', op='tp%s_%s' % (tp, chi)) #, saved_bc_t=baryon_bc_t)
             for tp in ['1', '2'] for chi in ['RL', 'LL'] ] 

data_dir_aff    = 'XXX'
data_out        = 'XXX'
execfile('config_lcdb.py')

