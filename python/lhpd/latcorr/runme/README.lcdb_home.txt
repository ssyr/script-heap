TODO change dat_d={kind='ub', ama=None} -> {kind='bin', ama='ub'}

get_lcdb_loc(dat_d, lc_d)

dat_d= {dat_dir=, kind='aff'|'conv'|'bin'|'ub', 
	['conv'|'bin'==kind ? ama]

# collected from 
# * ff/nedm/configs/config_lcdb.DWF24c64_hokusai.py
# *
lc_d = {
  kind	= 'c2'	      ; c2_d			# 2pt function				    [idata][tpol][psnk][tsnk]
	| 'c2vi'      ; c2_d, vi_d		# 2pt function with V4("vacuum") insertion
	| 'c2fit'     ; c2_d, m_d		# 2pt function: fit parameters

	| 'c3'	      ; c3_d, ins_d		# 3pt function
	| 'c3vi'      ; c3_d, ins_d, vi_d	# 3pt function with V4("vacuum") insertion
	| 'c3ts'      ; c2_d, vi_d		# 3pt function with timeslice insertion
						# different from c3 : tsnk and tins dependence
						
	| 'ff'	      ;	c3_d, ins_d, m_d	# form factors
	| 'ffvi'      ; c3_d, ins_d, vi_d, m_d	# form factors with V4("vacuum") insertion

	| 'al5'	      ; c2_d, vi_d		# alpha5-angle
	| 'dvi'	      ; vi_d			# disconnected V4(vacuum) insertion as function of coord
	| 'qtopo'     ; dkind			# FIXME use dvi instead
}

c2_d = {
  had=, 
  tpol|tpol_list=,
  psnk|psnk_list=,
  smsrcsnk=, 
  smtag=,
  [bgem=],
}

c3_d = {
  had=, 
  tpol=, 
  psnk=, 
  tsep=, 
  smsrcsnk=, 
  smtag=,
  [bgem=],
}

ins_d= {
  kind	= 
	| 'bb'	      ; flav, lmin, lmax	# building blocks (q-bilinear, ?)
	| 'op'	      ; op, ir			# "operator"	  (q-bilinear, ?)
	| 'qpdf'      ; ...			# qPDF		  (q-bilinear, ?)
	| 'tmd'	      ; ...			# TMD
  qext_list=
}
ins_d for matrix_elem_str
{
  ir	= <ir_name>
  comp	= index(int) | comp_name(str)
  reim	= 0,
}

vi_d= {
  kind	= 'volpsc'    ; gf, flav
	| 'volcedm'   ; gf, flav
	| 'qloop'     ; flav, approx
	| 'theta'     ; gf, rcut=, dtcut=
}

quark loop V4
lc_d=dict(
    kind='dvi',	    ; "disconnected vacuum insertion"
    dkind = 'orig'  ; value[x,y,z,t]
	  | 'mom3'  ; value[i_qext, t]; attrs=['qext_list',?]
    vi_d=dict(
        kind  = 'qloop'	; flav, approx
	      | 'qtopo,	; gf, qop
	flav='S'|'UD', 
	approx='defl_hp0n32_ex'
	gf=,
	qop=
    )
)
      

Examples
had     = 'proton'|'pion'
smsrcsnk='SS'|'SP'
smtag	= 'GN2x10kbxp10_GN2x10kbxm10'
op	= 'tensor1'|'pstensor1'

# vim: tw=0
