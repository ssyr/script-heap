from __future__ import print_function
import os
import h5py
import lhpd
import tempfile
import numpy as np
import itertools as it

dbdisco_bin = "/Users/syritsyn/trench/REDSTAR/adat/main/dbutil/dbdisco"


#def conv_disco_sdb2hdf(h5_out, h5key, file_in_sdb, latsize, mom_list, lpath_list, lplen_max) :
def conv_disco_sdb2hdf(h5_out, h5key, vals_file_, latsize, mom_list, lpath_list, lplen_max) :
    """ 
        lpath_list BB-style lpath list [ {0 .. 2*dim-1}, ... ]
        
        in principle, mom_list and lpath_list may be read from the file, 
        but it's easier to pass them and to create hdf5 in a rectilinear form
    """
    mom_list= np.asarray(mom_list)
    n_mom   = len(mom_list)
    n_lpath = len(lpath_list)

    latsize = np.asarray(latsize)
    lt      = latsize[-1]
    dim     = len(latsize)
    assert(4 == dim)   # verify everything for dim!=4

    def lpath2sdb(lp) :
        lm = np.zeros((lplen_max,), int)    # sdb-style linkpath
        for i, l in enumerate(lp) :
            if   l < dim    : lm[i] = -l - 1
            elif dim <= l   : lm[i] =  l - dim + 1
        return lm

    lpath_list_sdb = np.asarray([ lpath2sdb(lp) for lp in lpath_list ])

    keys_fd, keys_file = tempfile.mkstemp(suffix='sdb2hdf') ; os.close(keys_fd)
    vals_fd, vals_file = tempfile.mkstemp(suffix='sdb2hdf') ; os.close(vals_fd)
    xslt_fd, xslt_file = tempfile.mkstemp(suffix='sdb2hdf') ; os.close(xslt_fd)
    
    #status = os.system("%s %s keysxml %s" % (dbdisco_bin, file_in_sdb, keys_file))
    #if 0 != status :
        #raise RuntimeError(status)
    #status = os.system("%s %s keysxml %s %s" %  (dbdisco_bin, file_in_sdb, keys_file, vals_file).read()
    #if 0 != status :
        #raise RuntimeError(status)

    n_gamma = 16
    gamma_list = np.r_[:n_gamma]
    # ordering similar to bb.h5
    res = np.empty((n_gamma, n_lpath, n_mom, lt), np.complex128)
    res.fill(float('nan') + 1j*float('nan'))    # to signal unfilled 
    tau_list    = np.r_[:lt]

    # FIXME very slow to call separate xsltproc for every lpath 
    for i_lp, lp in enumerate(lpath_list_sdb) :
        xslt_f  = open(xslt_file, "w")
        xslt_f.write("""<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- print <Shell|Point>_<Shell|Point>_Wilson_<Baryons|Mesons> records -->
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
<xsl:template match="/DiscoBlocs">
  <xsl:for-each select="elem[Key/disp='%s']">
    <xsl:value-of select="Key/mom"/>  
    <xsl:text> </xsl:text>
    <xsl:value-of select="Key/t_slice"/>  
    <xsl:text> </xsl:text>
    <xsl:for-each select="Val/op/elem">
      <xsl:value-of select="re"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="im"/>
      <xsl:text>    </xsl:text>
    </xsl:for-each>
    <xsl:text>&#xa;</xsl:text>
  </xsl:for-each>
</xsl:template>
<xsl:template match="text()"/>
</xsl:stylesheet>
""" % ' '.join([ '%d' % l for l in lp ]))
        xslt_f.close()

        tstr_f      = os.popen("xsltproc %s %s" %  (xslt_file, vals_file_))
        tstr        = tstr_f.read()
        tstr_f.close()
        tarr        = np.fromstring(tstr, np.float64, sep=' ')
        tarr        = tarr.reshape(-1, dim + 2 * n_gamma)
        n_tarr      = tarr.shape[0]
        assert(n_tarr == lt * n_mom)

        tarr_mom    = np.array(tarr[:, 0 : dim-1], int)
        tarr_t      = np.array(tarr[:, dim-1], int)
        tarr_ql     = tarr[:, dim ::2] + 1j*tarr[:, dim + 1 ::2]

        for i in range(n_tarr) :
            i_mom   = lhpd.np_find_first(tarr_mom[i], mom_list)[0]
            i_t     = lhpd.np_find_first(tarr_t[i], tau_list)[0]
            res[:, i_lp, i_mom, i_t] = tarr_ql[i]
        print(i_lp, lp)
        
    os.remove(keys_file)
    os.remove(vals_file)
    os.remove(xslt_file)

    ql      = h5_out.require_dataset(h5key, res.shape, res.dtype, fletcher32=True)
    ql[:]   = res
    ql.attrs['dim_spec']    = np.array(['i_gamma', 'i_linkpath', 'i_qext', 'i_tau' ], dtype='S32')
    ql.attrs['qext_list']   = mom_list
    ql.attrs['linkpath_list']  = np.array([ lhpd.aff_io.aff_key_bb_linkpath_str(lp) for lp in lpath_list ],
                            dtype='S16')
    ql.attrs['gamma_list']  = gamma_list

def read_latc(h5g, gamma_list, lpath_list, mom_list, lt) :
    n_gamma = len(gamma_list)
    n_lp    = len(lpath_list)
    n_mom   = len(mom_list)
    res     = np.empty((n_gamma, n_lp, n_mom, lt), np.complex128)
    for i_g, g in enumerate(gamma_list) : 
        for i_lp, lp in enumerate(lpath_list) :
            for i_mom, mom in enumerate(mom_list) :
                res[i_g, i_lp, i_mom] = h5g['%s/g%d' % (lp, g)][tuple(mom)]
    return res
"""
latsize=[4,4,4,8]
mom_list=lhpd.latcorr.make_mom_list(2)
lpath_list=lhpd.latcorr.make_linkpath_list(0, 2)
h5f=h5py.File('tmp.sdb2hdf.h5', 'w')
vals_file='disco_newcontractions_SVD.vals.xml'

conv_disco_sdb2hdf(h5f, '/qloop', vals_file, latsize, mom_list, lpath_list, 2)
"""
