import numpy as np
VERBOSE=1


t1max   = 20
Lt      = 96
Ls      = 48


#cfg_list = [
    #640,    720,    800,    880,    960,
    #1040,   1080,   1160,   1200,   1240,
    #1280,   1320,   1400,   1440,   1480,
    #1520,   1560,   1600,   1640,   1680,
    #1720,   1760,   1800,   1840,   1880,
    #1920,   1960,   2000,   2040,   2080,
    #2120,   2160,   2200
#]
#cfg_list = [640, 1280, 1520, 1800, 2160 ]
# XXX OLD MB's set
#cfg_list = [
    #640,  720,  800,  880,  960,
    #1040, 1080, 1160, 1200, 1280,
    #1320, 1400, 1440, 1480, 1520,
    #1560, 1640, 1720, 1760, 1800,
    #1880, 1920, 1960, 2000, 2040,
    #2080, 2160, 2200
#]
# XXX NEW ER's set st30
cfg_list = [
     640,  680,  720,  760,  800,
     840,  880,  920,  960, 1000,
    1040, 1080, 1120, 1160, 1200,
    1240, 1280, 1320, 1360, 1400,
    1440, 1520, 1600, 1680, 1760,
    1840, 1920, 2000, 2080, 2160 
]
# XXX st11
#cfg_list = [
    #680, 760, 840, 920, 1000, 
    #1120, 1240, 1360, 1600, 1680, 1840, 
#]

meas_spec_list = np.array(
        [ ("%d" % cfg, [x,y,z,t]) 
            for cfg in cfg_list
            for x in [0, 16, 32]
            for y in [0, 16, 32]
            for z in [0, 16, 32]
            for t in [0, 32, 64] 
            ],
        dtype=[('ckpoint_id', 'S32'), ('source_coord', np.int32, (4,))])
meas_spec_list_exact = meas_spec_list[::81]
ama_mode_list = [
    {   'approx' : 'exact', 
        'meas_spec_list' : meas_spec_list_exact },
    {   'approx' : 'sloppy',
        'meas_spec_list' : meas_spec_list }
]

c2pt_psnk=[0,0,0]

sink_list = [
    ('proton', 'r1'),
    ('proton', 'r2'),
]
source_list = [
    ('nstar', 'r1'),
    ('nstar', 'r2'),
]
src_snk_list = [ 
    ('nstar', 'r1', 'proton', 'r2'), 
    ('nstar', 'r2', 'proton', 'r1'), 
]

sm_list = ['S', 'P']

#sm_list = ['P'] # only PP meas
#op_list = [
    #"O1LLL", "O1LLR", "O1LRR", "O1RLL", "O1RRL", "O1RRR", 
    #"O2RLL", "O2RLR", "O2RRR", 
    #"O3LLL", "O3LLR", "O3LRR", "O3RLL", "O3RRL", 
#]
#op_list_tmp = [
    #"O1LLL", "O1LLR", "O1LRR", "O1RLL", "O1RRR", 
    #"O2RLL", "O2RLR", "O2RRR", 
    #"O3LLL", "O3LLR", "O3LRR", "O3RLL", "O3RRL", 
#]
op_list = [
    "O1LRR", "O1RLL",
    "O2LLL", "O2LLR", "O2LRL", "O2RLR", "O2RRL", "O2RRR",
    "O3LLL", "O3LLR", "O3LRR", "O3RLL", "O3RRL", "O3RRR" 
]


data_top='DATA-cat/COLLECT.st30'

def get_nnbarcat_dir(meas_spec, ama_mode_str) :
    cfg_key, csrc = meas_spec['ckpoint_id'], meas_spec['source_coord']
    return '%s/config_%s/x%d_y%d_z%d_t%d_%s' % (
                data_top, cfg_key, 
                csrc[0], csrc[1], csrc[2], csrc[3], 
                ama_mode_str)
def get_nnbarcat_file(meas_spec, ama_mode_str):
    cfg_key, csrc = meas_spec['ckpoint_id'], meas_spec['source_coord']
    return '%s/tmp.nnbar-cat' % (
        get_nnbarcat_dir(meas_spec, ama_mode_str),)
def get_nnbarlist_file(meas_spec, ama_mode_str):
    cfg_key, csrc = meas_spec['ckpoint_id'], meas_spec['source_coord']
    return '%s/tmp.nnbar-list-unif' % (
        get_nnbarcat_dir(meas_spec, ama_mode_str),)

def get_orig_dir(meas_spec, ama_mode_str) :
    cfg_key, csrc = meas_spec['ckpoint_id'], meas_spec['source_coord']
    return '%s/config_%s/x%d_y%d_z%d_t%d_%s' % (
                data_top, cfg_key, 
                csrc[0], csrc[1], csrc[2], csrc[3], 
                ama_mode_str)
#def get_orig3pt_file(
        #had_type_src, had_rep_src, sm_src,
        #had_type_snk, had_rep_snk, sm_snk,
        #op, t1,
        #meas_spec, ama_mode_str):
    #raise RuntimeError("should not be used")
    #cfg_key, csrc = meas_spec['ckpoint_id'], meas_spec['source_coord']
    #sm_tag = {'S' : 'SHELL', 'P' : 'POINT'}
#
    #s1 = 'nnbar_%s_%s_%s_%s_%s_smearN_%s_SINK_smearNstar_%s_SINK_px0py0pz0_%s_t%d' % (
            #op, had_type_snk, had_rep_snk, had_type_src, had_rep_src, 
            #sm_tag[sm_snk], sm_tag[sm_src], cfg_key, t1)
    #return '%s/%s' % (get_orig_dir(meas_spec, ama_mode_str), s1)

def get_orig2pt_file(had_type, had_rep, sm_src, sm_snk, meas_spec, ama_mode_str):
    cfg_key, csrc = meas_spec['ckpoint_id'], meas_spec['source_coord']
    sm_tag = {'S' : 'SHELL', 'P' : 'POINT'}

    s1 = '%s_%s_%s_SOURCE_%s_SINK_px0py0pz0_%s' % (
            { 'proton' : 'proton_e11', 'nstar' : 'nstar_e22' }[had_type], had_rep,
            sm_tag[sm_src], sm_tag[sm_snk], cfg_key)
    return '%s/%s' % (get_orig_dir(meas_spec, ama_mode_str), s1)

def get_twopt_bin_h5_file(had_type, had_rep, sm_src, sm_snk, ama_tag) :
    return "nnbar_twopt_%s-%s_%s%s_%s.h5" % (
            had_type, had_rep, sm_src, sm_snk, ama_tag)

def get_c3pt_bin_h5_file(
        had_type_src, had_rep_src, had_type_snk, had_rep_snk,
        sm_src, sm_snk, ama_mode_str) :
    return "nnbar_c3pt_%s-%s_%s-%s_%s%s_%s.h5" % (
            had_type_src, had_rep_src, had_type_snk, had_rep_snk,
            sm_src, sm_snk, ama_mode_str)

