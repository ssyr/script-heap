from __future__ import print_function
from past.builtins import execfile
from future.utils import iteritems
import math
import numpy as np
import h5py
import itertools as it

import os
import sys
import time
import pickle

# this will go to "prod", so leep all module abs.paths
import lhpd
import lhpd.npr
from lhpd.npr.npr_common import *
from lhpd.npr import nnbar
from lhpd.pymath.gamma_matr import Gamma16_dgr as Gamma16

import tsv
import matplotlib as mpl
#mpl.use('Agg')       # don't ask for X
import matplotlib.pyplot as plt
import dataview as dv

#execfile('config.py')

####################################################################################################
# NPR CONTRACTIONS 
####################################################################################################


from lhpd.npr import calc_qbarq_npr

def runme_calc_qbarqvertex_npr(qqbar_i, qqbar_f, rsplan=('jk',1)) :
    i = 0
    assert(qqbar_is_cs(qqbar_i))
    assert(qqbar_is_cs(qqbar_f))
    qbarqv_cnct     = calc_qbarq_npr(ens_cfg_48c96pt, qqbar_i, qqbar_f, Gamma16)
    print('#%d' %i ; i=i+1)
    qqbar_cnct_rs   = lhpd.resample(qqbar_cnct, rsplan)
    print('#%d' %i ; i=i+1)
    qqbar_i_rs      = lhpd.resample(qqbar_i, rsplan)
    print('#%d' %i ; i=i+1)
    qqbar_i_revinv_rs  = qqbar_rev(qqbar_inv(qqbar_i_rs))
    print('#%d' %i ; i=i+1)
    qqbar_f_rs      = lhpd.resample(qqbar_f, rsplan)
    print('#%d' %i ; i=i+1)
    qqbar_f_inv_rs  = qqbar_inv(qqbar_f_rs)
    print('#%d' %i ; i=i+1)
    if False :
        qbarqv_amp_rs    = qqbar_to_cs(np.einsum('...ab,...ibc,...cd->...iad', 
                                qqbar_to_flat(qqbar_f_inv_rs), 
                                qqbar_to_flat(qbarqv_cnct_rs), 
                                qqbar_to_flat(qqbar_i_revinv_rs)))
    elif True :
        qbarqv_leftamp_rs= qqbar_to_cs(np.einsum('...ab,...ibc->...iac',
                                qqbar_to_flat(qqbar_f_inv_rs), 
                                qqbar_to_flat(qbarqv_cnct_rs)))
        qbarqv_amp_rs    = qqbar_to_cs(np.einsum('...iac,...cd->...iad',
                                qqbar_to_flat(qbarqv_leftamp_rs), 
                                qqbar_to_flat(qqbar_i_revinv_rs)))
    else : raise NotImplementedError
        
    print('#%d' %i ; i=i+1)
    qbarqv_ampproj_rs= np.einsum('...iabad,...jdb->...ij', qbarqv_amp_rs, Gamma16) / (NCOLOR * NSPIN)
    print('#%d' %i ; i=i+1)
    
    return qbarqv_ampproj_rs



def calc_save_nnbar_npr(ensc, ama_mode, wvcomb_list, 
                rsplan=('jk', 1),
                meas_slice=slice(None),
                VERBOSE=False) :
    """ wvcomb_list - list of wvcomb to be computed together to save on read/write/etc
                = [ (wv1, wv2, wv3), ...]
    """
    ls          = ensc.latsize
    tw          = ensc.ft_twist
    wvec_list   = list(set(tuple(wv) for wvc in wvcomb_list for wv in wvc))
    wvec_list.sort()
    wvec_map    = dict( (wv, i) for i, wv in enumerate(wvec_list) )
    n_wvec      = len(wvec_list)
    wvec_list_neg   = [ inv_wavevec(wv, ls, tw) for wv in wvec_list ]

    # <- [i_data, i_wvec, ic,is, jc,js], propagagor
    qqbar_px    = ensc.get_frwprop_ft(np.r_[wvec_list, wvec_list_neg], ama_mode, 
                    meas_slice=meas_slice, VERBOSE=VERBOSE)
    # XXX make <q[i, x] qbar[j, p]> to compute op[6*q] at x; 
    # note : can also build op[6*qbar], dont' do qqbar_rev in this case
    qqbar       = qqbar_rev(qqbar_px) 
    n_data      = qqbar.shape[0]
    print(qqbar.shape)
    # <- [i_data, i_wvec, ics, jcs]
    qqbar       = qqbar.reshape(qqbar.shape[:-4] + (NCOLSPIN, NCOLSPIN))
    # XXX this is wrong : cannot resample because contraction computed and then averaged, 
    # XXX may not average propagators first
    #XXX qqbar_rs    = lhpd.resample(qqbar, rsplan)

    qqbar_inv   = np.linalg.inv(qqbar_rs)

    print(qqbar.shape)
    qqbar_pos   = qqbar[:, : n_wvec ]
    qqbar_neg   = qqbar[:, n_wvec : ]

    # antisymmetrize the q(sink)-indices of (quark propagator)^{x2}, 
    # keep qbar(source)-indices intact because the momenta are different
    # qqbar2 <- [i_data, i_wvec, jcs_pos, jcs_neg, ics_asym_flat]
    qqbar2      = lhpd.pymath.tensor_asym_flat_einsum('...i,...j->...ij', 
                        [qqbar_pos, qqbar_neg], NCOLSPIN, [1, 1], a_axes=[-2, -2])

    nnbar_cnct  = nnbar.get_nnbar_cnct_all()
    n_nnbar     = np.prod(nnbar_cnct.shape[:-2])
    # <- [i_op=0..23, i_uu2, i_dd4]
    nnbar_cnct  = nnbar_cnct.reshape((n_nnbar,) + nnbar_cnct.shape[-2:])
    # <- [i_op=0..23, i_uu2, i_dd2_1, i_dd2_2]
    nnbar_cnct  = lhpd.pymath.tensor_asym_flat_ungroup(nnbar_cnct, NCOLSPIN, [2, 2])

    # [i_op][a] -> (b,c,coeff)
    nnbar_sparse = []
    for nn in nnbar_cnct :
        a_sparse = {}
        for a,b,c in zip(*np.where(0 != nn)) :
            y   = a_sparse.get(a, [])
            y.append( (b, c, nn[a, b, c]) )
            a_sparse[a] = y
        nnbar_sparse.append(a_sparse)
        
    for wvc in wvcomb_list : 
        # translate wv indices into qqbar2's
        # sic! order of wvecs in a wvec_comb
        # <- [i_data, jcs_pos, jcs_neg, ics_asym_flat]
        print(time.ctime(), '    ', [ tuple(wv) for wv in wvc ])
        qqbar2_dd0, qqbar2_dd1, qqbar2_uu = [ qqbar2[:, wvec_map[tuple(wv)] ] 
                                              for wv in wvc ]
        for i_nnbar, a_sparse in enumerate(nnbar_sparse) :
            # <- [i_data, jcs_op, jcs_u0p, jcs_u0n, jcs_d0p, jcs_d0n, jcs_d1p, jcs_d1n]
            nnbar       = np.zeros((n_data,) + (NCOLSPIN,)*6, qqbar2.dtype)
            print(i_nnbar, len(a_sparse), sum([len(v) for v in a_sparse.values()]))
            
            for i_uu, bcx_list in iteritems(a_sparse) :
                #print(time.ctime(), i_uu, len(bcx_list))
                # <- [i_data, jcs_d0p, jcs_d0n, jcs_d1p, jcs_d1n]
                prod_bc = np.zeros((n_data,) + (NCOLSPIN,)*4, qqbar2.dtype)
                for i_dd0,i_dd1, m_nnbar in bcx_list :
                    prod_bc += m_nnbar * np.einsum('icd,ief->icdef', 
                                    qqbar2_dd0[:, ..., i_dd0], 
                                    qqbar2_dd1[:, ..., i_dd1])
                    
                nnbar[:] += np.einsum('iab,icdef->iabcdef', 
                                    qqbar2_uu[:, ..., i_uu], prod_bc)

            # TODO resample, amputate, project, save

    print(qqbar2.shape)


def outer_cnct3way_fast0(p, q, cnct, q2q1, q2q2, q2q3, out=None) :
    """ contract 3-way on the 1st indices and and outer-mult on the 2nd indices
        q2q* : [p,q] complex 
        cnct : sparse contraction map {i1 : [(i2,i3,c), ...], ...}
    """
    assert(q2q1.shape == (p, q))
    assert(q2q2.shape == (p, q))
    assert(q2q3.shape == (p, q))
    
    if not None is  out : 
        assert(out.shape == (q, q, q))
        out.fill(0)
    else : 
        out = np.zeros((q, q, q),
                np.find_common_type([], [float, q2q1.dtype, q2q2.dtype, q2q3.dtype]))
            
    for xI, xJKc in iteritems(cnct) :
        prod23  = np.zeros((q, q), out.dtype)
        for xJ, xK, c in xJKc :
            prod23 += c * np.outer(q2q2[xJ], q2q3[xK])
        out += np.outer(q2q1[xI], prod23.reshape(-1)).reshape(q, q, q)

    return out

def outer_cnct3way_fast1(p, q, cnct, q2q1, q2q2, q2q3, out=None) :
    """ contract 3-way on the 1st indices and and outer-mult on the 2nd indices
        q2q* : [p,q] complex 
        cnct : sparse contraction map {i1 : [(i2,i3,c), ...], ...}
    """
    assert(q2q1.shape == (p, q))
    assert(q2q2.shape == (p, q))
    assert(q2q3.shape == (p, q))
    out_dtype = np.find_common_type([], [float, q2q1.dtype, q2q2.dtype, q2q3.dtype])
    if not None is  out : 
        assert(out.shape == (q, q, q))
        out.fill(0)
    else : out = np.zeros((q, q, q), out_dtype)

    # pre-compute 2-way contractions
    xI_list = cnct.keys()
    xI_list.sort()
    xI_map  = dict([ (xI,k) for k,xI in enumerate(xI_list) ])
    n_xI    = len(xI_list)
    prod23  = np.zeros((n_xI, q, q), out.dtype)
    for xI, xJKc in iteritems(cnct) :
        prod23_xI  = np.zeros((q, q), out.dtype)
        for xJ, xK, c in xJKc :
            prod23_xI  += c * np.outer(q2q2[xJ], q2q3[xK])
        prod23[xI_map[xI]] = prod23_xI

    # compute 3-way
    q2q1T = q2q1.T      # transpose for faster access
    for iq1 in range(q) :
        q2q1_iq1 = q2q1T[iq1]
        prod123_iq1 = np.zeros((q, q), out.dtype)
        for xI in xI_list : 
            prod123_iq1 += q2q1_iq1[xI] * prod23[xI_map[xI]]
        out[iq1] = prod123_iq1

    return out

def outer_cnct3way_fast2(p, q, cnct, q2q1, q2q2, q2q3, out=None) :
    """ contract 3-way on the 1st indices and and outer-mult on the 2nd indices
        q2q* : [p,q] complex 
        cnct : sparse contraction map {i1 : [(i2,i3,c), ...], ...}
    """
    assert(q2q1.shape == (p, q))
    assert(q2q2.shape == (p, q))
    assert(q2q3.shape == (p, q))
    out_dtype = np.find_common_type([], [float, q2q1.dtype, q2q2.dtype, q2q3.dtype])
    if None is out : out = np.empty((q, q, q), out_dtype)
    else : assert(out.shape == (q, q, q))

    n_xI    = len(cnct.keys())
    q2q1_sel= np.empty((n_xI, q), out_dtype)
    prod23  = np.empty((n_xI, q, q), out_dtype)
    for k, (xI, xJKc) in enumerate(iteritems(cnct)) :
        prod23_xI  = np.zeros((q, q), out.dtype)
        for xJ, xK, c in xJKc :
            prod23_xI  += c * np.outer(q2q2[xJ], q2q3[xK])
        prod23[k]   = prod23_xI
        q2q1_sel[k] = q2q1[xI]
    
    np.dot(q2q1_sel.T, prod23.reshape(n_xI, q*q), out=out.reshape(q, q*q))
    return out

# brave new version
outer_cnct3way = outer_cnct3way_fast2

def make_cnct3way_sparse(nn) :
    a_sparse = {}
    for a,b,c in zip(*np.where(0 != nn)) :
        a_sparse.setdefault(a, []).append( (b, c, nn[a, b, c]) )
    return a_sparse

from lhpd.misc import timer as mytimer
        

def calc_nnbar_npr_general(qqbar_list, 
        rsplan=('jk',1), nnb_list=None, VERBOSE=True) :
    """ compute contraction with Op(6*q) at x 
        qqbar6 : list of 6 propagators to use for nnbar contractions:
            qqbar[0..5][i_data, i_mom, ic, is, jc, js] = <q(ic,is,p) qbar(jc,js,x)>  
        NOTE: propagators are reversed to be <q(i*,x) qbar(j*,p)>
        NOTE: amputation from the right : G(x,p1...) -> A(x,p1...) <q(i*,x)qbar(j*,p1)>^{-1}
        
        TODO could be easily rewritten for 6 different momenta
    """
    # sanity checks
    assert(len(qqbar_list) == 6)
    assert(all([ qqbar.shape[:2] == qqbar_list[0].shape[:2] 
                 for qqbar in qqbar_list ]))
    nnbar_dtype = np.find_common_type([], [ qqbar.dtype for qqbar in qqbar_list ])
    n_data, n_mom = qqbar_list[0].shape[:2]
    n_cs_asym   = NCOLSPIN * (NCOLSPIN - 1) / 2
    n_cs_sq     = NCOLSPIN * NCOLSPIN
    
    # XXX bootstrap is random, but we need to apply the same resampling to both props and the contraction
    # FIXME introduce 'bsfix' resampling : bootstrap with the resampling map saved in the 1: elements of the tuple
    assert(rsplan[0] != 'bs')
    binsize     = lhpd.get_binstride(rsplan[1]) # workaround for new binsize format
    #assert(int(binsize) == binsize) # check that binsize is castable to int, otherwise trick binning will be required
    #assert(0 < binsize)
    n_bins      = int(n_data / binsize)
    n_data_bins = n_bins * binsize
    if n_data_bins != n_data :
        print("WARNING: ignore last %d samples due to binning" % (
                n_data - n_bins * binsize))
    # make rsplan with binsize=1 for GFs (binning will be done on the fly)
    aux = list(rsplan) ; aux[1] = 1 ; rsplan_nobin = tuple(aux)
    
    # XXX reversal: result [i*,j*] = <q(i*,x) qbar(j*,p)>
    # XXX truncation to whole bins subset
    qqbar_list      = [ qqbar_rev(qqbar[:n_data_bins]) for qqbar in qqbar_list ]

    # resampling and inversion for future amp.
    qqbar_rs_list   = [ lhpd.resample(qqbar, rsplan) for qqbar in qqbar_list ]
    qqbar_inv_rs_list   = [ qqbar_to_flat(qqbar_inv(qqbar_rs))
                            for qqbar_rs in qqbar_rs_list ]

    # antisymmetrize the q(sink)-indices of (quark propagator)^{x2}, 
    # keep qbar(source)-indices intact because the momenta are different
    # qqbar2 <- [i_data, i_wvec, jcs_pos, jcs_neg, ics_asym_flat]
    # XXX NEW for fast-outer: qqbar2 <- [i_data, i_mom, ics_asym_flat, jcs_1pos2neg]
    def mk_qqbar2(qqa, qqb) : 
        qqbar2 = lhpd.pymath.tensor_asym_flat_einsum('...i,...j->...ij',
                            [ qqbar_to_flat(qqa), qqbar_to_flat(qqb) ],
                            NCOLSPIN, [1, 1], a_axes=[-2, -2], out_axis=-3)
        assert(qqbar2.shape == (n_data, n_mom, n_cs_asym, NCOLSPIN, NCOLSPIN))
        return qqbar2.reshape((n_data, n_mom, n_cs_asym, n_cs_sq))

    qqbar2_uu0 = mk_qqbar2(qqbar_list[0], qqbar_list[1])
    qqbar2_dd1 = mk_qqbar2(qqbar_list[2], qqbar_list[3])
    qqbar2_dd2 = mk_qqbar2(qqbar_list[4], qqbar_list[5])



    # contract <Op dbar(-p)dbar(p)dbar(-p)dbar(p)ubar(-p)ubar(p)>
    # = <Op [dbar(-p)dbar(p)]_2 [dbar(-p)dbar(p)]_1 [ubar(-p)ubar(p)_0]>
    # = <CONTRACTION_MATR[I0,I1,I2] * uubar2[I0] ddbar2[I1] ddbar2[I2]
    # where qqbar2[I] = Asym[Ia,Ib->I](<q[Ia]qbar(p)>*<q[Ib]qbar(-p)>)
    nnbar_basis = nnbar.get_nnbar_cnct_basis()
    if None == nnb_list : 
        nnbar_cnct_list = nnbar_basis.copy()
    else : 
        nnbar_cnct_list = np.asarray([ nnbar_basis[i] for i in nnb_list ])

    n_op    = len(nnbar_cnct_list)
    nnbar_g = np.einsum('ikl,jkl->ij', nnbar_basis, nnbar_basis.conj())
    nnbar_g_inv = np.linalg.inv(nnbar_g)    # for projection
    nnbar_cnct  = lhpd.pymath.tensor_asym_flat_ungroup(nnbar_cnct_list, NCOLSPIN, [2,2], a_axis=-1)
    
    # build sparse map for every operator
    nnbar_sparse = [ make_cnct3way_sparse(nn) for nn in nnbar_cnct ]

    # [i_data, i_mom, i_op_cnct, iop_proj]
    nnbar_cnct_proj_rs = np.empty((n_bins, n_mom, n_op, len(nnbar_basis)), np.complex128)
    for i_mom in range(n_mom) :
        for i_nnbar, a_sparse in enumerate(nnbar_sparse) :
            nnbar_timer = mytimer('nnbar_1mom_1samp', VERBOSE=VERBOSE)
            if VERBOSE :
                print(i_mom, i_nnbar, len(a_sparse), sum([len(v) for v in a_sparse.values()]))
            # <- [i_data, jcs_u1p2n, jcs_d3p4n, jcs_d5p6n]
            nnbar_rs    = None
            nnbar_rsacc = lhpd.resample_accumulate(rsplan, (n_cs_sq,)*3, 
                                    nnbar_cnct_proj_rs.dtype)
            
            cur_timer   = mytimer('cnct3way,1mom,1op', VERBOSE=VERBOSE)

            #if True: #False : # einsum version
            if False : # einsum version
                nnbar_rs    = np.zeros((n_bins,) + (n_cs_sq,)*3, nnbar_cnct_proj_rs.dtype)
                nnbar_orig  = np.zeros((n_data,) + (n_cs_sq,)*3, qqbar2.dtype)
                for i_uu0, bcx_list in iteritems(a_sparse) :
                    #print(time.ctime(), i_uu0, len(bcx_list))
                    # <- [i_data, jcs_d3p4n, jcs_d5p6n]
                    prod_bc = np.zeros((n_data,) + (n_cs_sq,)*2, qqbar2.dtype)
                    for i_dd1,i_dd2, matrelem_nnbar in bcx_list :
                        prod_bc += matrelem_nnbar * np.einsum(
                                        'ib,ic->ibc',
                                        qqbar2_dd1[:, i_mom, i_dd1, :], 
                                        qqbar2_dd2[:, i_mom, i_dd2, :])

                    nnbar_orig += np.einsum('ia,ibc->iabc',
                                       qqbar2_uu0[:, i_mom, i_uu0, :], prod_bc)
                # bin contractions 
                # FIXME split contractions into bin-by-bin
                nnbar_rs[:] = nnbar_orig[ : n_bins * binsize].reshape(
                                       ( n_bins , binsize) + (n_cs_sq,)*3).mean(1)
                nnbar_rs    = lhpd.resample(nnbar_rs.reshape((n_bins,) + (NCOLSPIN,)*6), 
                                        rsplan_nobin)


            elif False : # fast-outer version
                nnbar_rs    = np.zeros((n_bins,) + (n_cs_sq,)*3, nnbar_cnct_proj_rs.dtype)
                for i_bin in range(n_bins) :
                    nnbar_rs[i_bin].fill(0)
                    for i in range(binsize) :
                        i_data = i + binsize * i_bin
                        nnbar_rs[i_bin] += outer_cnct3way(
                                        n_cs_asym, n_cs_sq, a_sparse,
                                        qqbar2_uu0[i_data, i_mom],   # u1 u2
                                        qqbar2_dd1[i_data, i_mom],   # d3 d4
                                        qqbar2_dd2[i_data, i_mom],   # d5 d6
                                        )
                    nnbar_rs[i_bin] /= binsize
                nnbar_rs    = lhpd.resample(nnbar_rs.reshape((n_bins,) + (NCOLSPIN,)*6), 
                                        rsplan_nobin)


            elif True : # fast-outer version + resample_accumulate
                for i_data in range(n_data) :
                    nnbar_rsacc.update(outer_cnct3way(
                                    n_cs_asym, n_cs_sq, a_sparse,
                                    qqbar2_uu0[i_data, i_mom],   # u1 u2
                                    qqbar2_dd1[i_data, i_mom],   # d3 d4
                                    qqbar2_dd2[i_data, i_mom],   # d5 d6
                                    ))
                nnbar_rs    = nnbar_rsacc.result()
                nnbar_rs    = nnbar_rs.reshape((-1,) + (NCOLSPIN,)*6)


            cur_timer('done')
            
            cur_timer = mytimer('resample cnct', VERBOSE=VERBOSE)
            # resample, amputate, antisymmetrize, project
            cur_timer('done')

            cur_timer = mytimer('amp1', VERBOSE=VERBOSE)
            nnbar_rs= np.einsum('iabcdef,iax->ixbcdef', nnbar_rs,  qqbar_inv_rs_list[0][:,i_mom])
            cur_timer('done') ; cur_timer = mytimer('amp2', VERBOSE=VERBOSE)
            nnbar_rs= np.einsum('iabcdef,ibx->iaxcdef', nnbar_rs,  qqbar_inv_rs_list[1][:,i_mom])
            cur_timer('done') ; cur_timer = mytimer('amp3', VERBOSE=VERBOSE)
            nnbar_rs= np.einsum('iabcdef,icx->iabxdef', nnbar_rs,  qqbar_inv_rs_list[2][:,i_mom])
            cur_timer('done') ; cur_timer = mytimer('amp4', VERBOSE=VERBOSE)
            nnbar_rs= np.einsum('iabcdef,idx->iabcxef', nnbar_rs,  qqbar_inv_rs_list[3][:,i_mom])
            cur_timer('done') ; cur_timer = mytimer('amp5', VERBOSE=VERBOSE)
            nnbar_rs= np.einsum('iabcdef,iex->iabcdxf', nnbar_rs,  qqbar_inv_rs_list[4][:,i_mom])
            cur_timer('done') ; cur_timer = mytimer('amp6', VERBOSE=VERBOSE)
            nnbar_rs= np.einsum('iabcdef,ifx->iabcdex', nnbar_rs,  qqbar_inv_rs_list[5][:,i_mom])
            cur_timer('done')
            
            cur_timer = mytimer('cnct_amp_proj', VERBOSE=VERBOSE)

            # asym d : [i_data, ics_u1, ics_u2, ics_d3d4d5d6_flat]
            nnbar_rs= lhpd.pymath.tensor_asym_flat_group(nnbar_rs, NCOLSPIN, [1,1,1,1])
            # asym u : [i_data, ics_u1u2_flat, ics_d3d4d5d6_flat]
            nnbar_rs= lhpd.pymath.tensor_asym_flat_group(nnbar_rs, NCOLSPIN, [1,1], 
                                                             a_axes=[-3,-2], out_axis=-2)
            
            # FIXME precompute np.einsum('kud,kj->jud', nnbar_cnct_list.conj(), nnbar_g_inv)
            #       to do two ops below together
            # dot with cnct.vec [i_data, j_op]
            nnbar_proj = np.einsum('iud,jud->ij',
                            nnbar_rs, nnbar_basis.conj())
            # normalize cnct.vec 
            nnbar_cnct_proj_rs[:,i_mom,i_nnbar,:] = np.dot(nnbar_proj, nnbar_g_inv)
            cur_timer('done')
            nnbar_timer('done')

    return nnbar_cnct_proj_rs

"""
def mk_ababab(x) : return [ x[:,0], x[:,1], x[:,0], x[:,1], x[:,0], x[:,1] ]
# run with sloppy
qqbar_sloppy=ens_cfg_48c96pt.get_frwprop_ft_selmom([[3,3,3,6],[-3,-3,-3,-7]], 'sloppy')
nnbar_sloppy=calc_nnbar_npr_general(mk_ababab(qqbar_amabin), rsplan=('jk', 81), nnb_list=None, VERBOSE=True)
# run with AMA bias correction
qqbar_amabin=ens_cfg_48c96pt.get_frwprop_ft_selmom([[3,3,3,6],[-3,-3,-3,-7]], 'amabin')
nnbar_unbias=calc_nnbar_npr_general(mk_ababab(qqbar_amabin), rsplan=('jk',('ama', 81, 1)), nnb_list=None, VERBOSE=True)
"""

def UTEST_qqbar_cnct_random(VERBOSE=False) : 
    qqbar1  = np.random.rand(1,1,3,4,3,4)
    cnct_1   = calc_nnbar_npr_general([qqbar1]*6, rsplan=('av',1), VERBOSE=VERBOSE)
    print(cnct_1.shape, np.allclose(cnct_1, np.identity(14)))
    
    qqbar2  = np.random.rand(2, 1,1,3,4,3,4)
    cnct_2   = calc_nnbar_npr_general([qqbar2[0], qqbar2[1]]*3, rsplan=('av',1), VERBOSE=VERBOSE)
    print(cnct_2.shape, np.allclose(cnct_2, np.identity(14)))

    qqbar6  = np.random.rand(6, 1,1,3,4,3,4)
    cnct_6   = calc_nnbar_npr_general(qqbar6, rsplan=('av',1), VERBOSE=VERBOSE)
    print(cnct_6.shape, np.allclose(cnct_6, np.identity(14)))

def UTEST_qqbar_cnct_random_chiral(VERBOSE=False) : 
    # FIXME gammamatr-dependent
    projR   = .5 * (Gamma16[0] + Gamma16[15])
    qqbarL  = np.einsum('...ab,...ibjc,...cd->...iajd', 
                    projR, np.random.rand(6, 1,1,3,4,3,4), projR)
    cnct_R  = calc_nnbar_npr_general(qqbarL, rsplan=('av',1), VERBOSE=VERBOSE)
    
    projL   = .5 * (Gamma16[0] - Gamma16[15])
    qqbarL  = np.einsum('...ab,...ibjc,...cd->...iajd', 
                    projL, np.random.rand(6, 1,1,3,4,3,4), projL)
    cnct_L  = calc_nnbar_npr_general(qqbarL, rsplan=('av',1), VERBOSE=VERBOSE)

    return cnct_R, cnct_L

def runme_calc_npr_nnbar_nonexc(mom_scale, VERBOSE=True) :
    ml = np.array(
            [[  1,  1,  1,  1 ], 
             [  1,  1, -1, -1 ], 
             [  1, -1,  1, -1 ], 
             [ -1, -1, -1,  1 ], 
             [ -1, -1,  1, -1 ],
             [ -1,  1, -1, -1 ]])
    mom_list = ml * np.r_[1, 1, 1, 2] * int(mom_scale)
    print(mom_list)
    qqbar6 = ens_cfg_48c96pt.get_frwprop_ft_selmom(mom_list, 'sloppy')[None, ...].transpose(2,1,0,3,4,5,6)
    print(qqbar6.shape)
    nnbar_cnct = calc_nnbar_npr_general(qqbar6, rsplan=('jk', 81), VERBOSE=VERBOSE)
    return nnbar_cnct

def runme_calc_npr_nnbar_uposdneg(mom_scale, VERBOSE=True) :
    """ u(p) u(p) d(-p) d(-p) d(-p) d(-p) 
        all u, all d are symmetric, must have no off-diag terms 
    """
    ml = np.array(
            [[  1,  1,  1,  1 ], 
             [  1,  1,  1,  1 ], 
             [ -1, -1, -1, -1 ], 
             [ -1, -1, -1, -1 ], 
             [ -1, -1, -1, -1 ], 
             [ -1, -1, -1, -1 ]])
    mom_list = ml * np.r_[1, 1, 1, 2] * int(mom_scale)
    print(mom_list)
    qqbar6 = ens_cfg_48c96pt.get_frwprop_ft_selmom(mom_list, 'sloppy')[None, ...].transpose(2,1,0,3,4,5,6)
    print(qqbar6.shape)
    nnbar_cnct = calc_nnbar_npr_general(qqbar6, rsplan=('jk', 81), VERBOSE=VERBOSE)
    return nnbar_cnct

def runme_calc_npr_nnbar_onemom(mom, VERBOSE=True) :
    """ u(p) u(p) d(p) d(p) d(p) d(p) 
        all u, all d are symmetric, must have no off-diag terms 
    """
    ml = np.array(mom) * np.ones((6,), int)[:,None]
    mom_list = ml
    print(mom_list)
    qqbar6 = ens_cfg_48c96pt.get_frwprop_ft_selmom(mom_list, 'sloppy')[None, ...].transpose(2,1,0,3,4,5,6)
    print(qqbar6.shape)
    nnbar_cnct = calc_nnbar_npr_general(qqbar6, rsplan=('jk', 81), VERBOSE=VERBOSE)
    return nnbar_cnct

def calc_npr_nnbar_twomom(
        ensc, ama_mode, mom_a, mom_b, 
        nnb_list=None, rsplan=('jk',1), VERBOSE=True) :
    """   ( <u(a) u(a) d(a) d(b) d(b) d(b)>, 
            <u(a) u(b) d(a) d(a) d(b) d(b)>,
            <u(b) u(b) d(a) d(a) d(a) d(b)> )
    """
    qqbar_a = ensc.get_frwprop_ft_selmom([mom_a], ama_mode)
    qqbar_b = ensc.get_frwprop_ft_selmom([mom_b], ama_mode)
    nnbar_cnct_aa = calc_nnbar_npr_general(
                        [qqbar_a, qqbar_a, qqbar_a, qqbar_b, qqbar_b, qqbar_b], 
                        nnb_list=nnb_list, rsplan=rsplan, VERBOSE=VERBOSE)
    nnbar_cnct_ab = calc_nnbar_npr_general(
                        [qqbar_a, qqbar_b, qqbar_a, qqbar_a, qqbar_b, qqbar_b], 
                        nnb_list=nnb_list, rsplan=rsplan, VERBOSE=VERBOSE)
    nnbar_cnct_bb = calc_nnbar_npr_general(
                        [qqbar_b, qqbar_b, qqbar_a, qqbar_a, qqbar_a, qqbar_b], 
                        nnb_list=nnb_list, rsplan=rsplan, VERBOSE=VERBOSE)
    return (nnbar_cnct_aa, nnbar_cnct_ab, nnbar_cnct_bb)

def mom_key(m) : 
    return ('px%d_py%d_pz%d_pt%d' % tuple(m))


def runme_calc_save_npr_nnbar_twomom(ensc, ama_mode, 
            mom_scale_r, h5grp, 
            nnb_list=None, rsplan=('jk',1), VERBOSE=True) :
    momx = np.r_[1,1,1,1] * np.r_[1,1,1,2]
    for sc in mom_scale_r :
        mom_a   = momx * sc
        mom_b   = inv_wavevec(mom_a, ensc.latsize, ensc.ft_twist)
        #mom_b   = -1*mom_a # FIXME incorrect mom selected in tmp file...
        print("BEGIN\t", time.asctime(), mom_a, mom_b )
        nnb_aa, nnb_ab, nnb_bb = calc_npr_nnbar_twomom(
                    ensc, ama_mode, mom_a, mom_b, 
                    nnb_list=nnb_list, rsplan=rsplan, VERBOSE=VERBOSE)
        h5grp_mom   = h5grp.require_group('%s_%s' % (mom_key(mom_a), mom_key(mom_b)))
        h5_purge_keys(h5grp_mom, ['aa', 'ab', 'bb'])
        h5grp_mom['aa'] = nnb_aa
        h5grp_mom['ab'] = nnb_ab
        h5grp_mom['bb'] = nnb_bb
        h5grp.file.flush()
        print("END\t", time.asctime(), mom_a, mom_b, )


"""
runme_calc_save_npr_nnbar_twomom(ens_cfg_48c96pt, 'sloppy', [10], h5py.File('tmp.test-sloppy-mom10.h5', 'a').require_group('x'), rsplan=('jk',81), nnb_list=nnbar.get_nnbar_cnct_basis()[0:2])
"""


def runme_collect_nnbar_twomom(ensc, mom_r, h5g) :
    """ collect 2-momenta nnbar, mom_b = invert (mom_a) """
    ls,ft = ensc.latsize, ensc.ft_twist
    mom_r = np.array(mom_r)
    mom_r.sort(0)
    n_mom = len(mom_r)
    mom_a_list, mom_b_list = [], []
    mom_lin, momsq_lin = [], []
    mom_sin, momsq_sin = [], []
    mom_sym, momsq_sym = [], []

    nnb_aa, nnb_ab, nnb_bb = [], [], []

    for s in mom_r :
        mom_a   = np.r_[1,1,1,1] * np.r_[1,1,1,2] * s
        mom_b   = inv_wavevec(mom_a, ls, ft)
        mom_a_list.append(mom_a)
        mom_b_list.append(mom_b)
        mom_lin.append(calc_mom_lin(mom_a, ls, ft))
        mom_sin.append(calc_mom_sin(mom_a, ls, ft))
        mom_sym.append(calc_mom_sym(mom_a, ls, ft))
        momsq_lin.append(calc_momsq_lin(mom_a, ls, ft))
        momsq_sin.append(calc_momsq_sin(mom_a, ls, ft))
        momsq_sym.append(calc_momsq_sym(mom_a, ls, ft))
        nnb_h5f = h5py.File('prop.st20-redoneBC/part.nnbar-unbias-mom%d.h5' % s, 'r')
        nnb_g   = nnb_h5f['/x/%s_%s' %(mom_key(mom_a), mom_key(mom_b))]
        nnb_aa.append(nnb_g['aa'].value)
        nnb_ab.append(nnb_g['ab'].value)
        nnb_bb.append(nnb_g['bb'].value)
        h5f.close()

    h5_purge_keys(h5g, ['nnbar_aa', 'nnbar_ab', 'nnbar_bb', 
                        'mom_list', 'mom_inv_list',
                        'k_lin', 'k_lin_sq', 
                        'k_sin', 'k_sin_sq', 
                        'k_sym', 'k_sym_sq' ])
    h5g['mom_list'] = np.array(mom_a_list)
    h5g['mom_inv_list'] = np.array(mom_b_list)
    h5g['nnbar_aa'] = np.concatenate(nnb_aa, axis=1)
    h5g['nnbar_ab'] = np.concatenate(nnb_ab, axis=1)
    h5g['nnbar_bb'] = np.concatenate(nnb_bb, axis=1)
    h5g['k_lin']    = np.array(mom_lin)
    h5g['k_lin_sq'] = np.array(momsq_lin)
    h5g['k_sin']    = np.array(mom_sin)
    h5g['k_sin_sq'] = np.array(momsq_sin)
    h5g['k_sym']    = np.array(mom_sym)
    h5g['k_sym_sq'] = np.array(momsq_sym)
    h5g.file.flush()

def calc_pN_lin(powN, mom, ls, ft) :
    klin = calc_mom_lin(mom, ls, ft)
    return (klin ** powN).sum(-1)


def runme_collect_nnbar_twomom_avgjobs(ensc, h5file_kpath_list, h5g) :
    """ collect 2-mom nnbar from jobs """
    ls,ft = ensc.latsize, ensc.ft_twist
        
    nnbar_map = {}
    nnbar_list_aa   = []
    #mom0sq_lin_list = []        # use as "hash" to classify moms
    mom_key_list    = []
    def mom_key(mom) : 
        return np.r_[   calc_pN_lin(2, mom, ls, ft), 
                        calc_pN_lin(4, mom, ls, ft), 
                        calc_pN_lin(6, mom, ls, ft) ]

    for i_mom, (h5file, h5kpath) in enumerate(h5file_kpath_list) :
        h5f_in  = h5py.File(h5file, 'r')
        h5g_in  = h5f_in[h5kpath]
        mom_all6= h5g_in.attrs['mom_list']
        mom0    = mom_all6[0]
        mom0rev = inv_wavevec(mom0, ls, ft)
        #mom0sq_lin  = calc_momsq_lin(mom0, ls, ft)
        
        mk = mom_key(mom0)
        j_mom   = find_first_arr_tol(mk, mom_key_list, 1e-6)
        if None == j_mom : 
            j_mom = len(mom_key_list)
            mom_key_list.append(mk)
        print(i_mom, j_mom, mom0, mk)

        nnbar_m_i = nnbar_map.setdefault(j_mom, {})
        nnbar_m_i.setdefault('mom_all6', []).append(mom_all6)
        
        if (mom_all6[:2] == [mom0, mom0]).all() :       ## aa or bb
            dict_incr(nnbar_m_i, 'cnt_aa_bb')
            dict_incr(nnbar_m_i, 'nnbar_aa_bb', h5g_in.value)
            print(i_mom, 'aa', mom0, mom_all6[1])
        elif (mom_all6[:2] == [mom0, mom0rev]).all() :
            dict_incr(nnbar_m_i, 'cnt_ab')
            dict_incr(nnbar_m_i, 'nnbar_ab', h5g_in.value)
            print(i_mom, 'ab', mom0, mom_all6[1])
        else : raise ValueError("bad 6-mom spec\n%s" % str(mom_all6))
        h5f_in.close()

    n_mom = len(mom_key_list)
    for i in range(n_mom) :
        print(i, mom_key_list[i], nnbar_map[i]['cnt_aa_bb'], nnbar_map[i]['cnt_ab'], len(nnbar_map[i]['mom_all6']))

    nnb_aa_bb = np.concatenate([ nnbar_map[i]['nnbar_aa_bb'] / nnbar_map[i]['cnt_aa_bb']
                                 for i in range(n_mom) ], axis=1)
    nnb_ab    = np.concatenate([ nnbar_map[i]['nnbar_ab'] / nnbar_map[i]['cnt_ab']
                                 for i in range(n_mom) ], axis=1)
    mom0_list = np.array([ nnbar_map[i]['mom_all6'][0][0]
                           for i in range(n_mom) ])


    h5_purge_keys(h5g, ['nnbar',
                        'mom_list', 'mom_inv_list',
                        'k_lin', 'k_lin_sq', 
                        'k_sin', 'k_sin_sq', 
                        'k_sym', 'k_sym_sq' ])
    h5g['nnbar']    = 0.4 * nnb_aa_bb + 0.6 * nnb_ab
    h5g['mom_list'] = mom0_list
    h5g['k_lin']    = calc_mom_lin(mom0_list, ls, ft)
    h5g['k_lin_sq'] = calc_momsq_lin(mom0_list, ls, ft)
    h5g['k_sin']    = calc_mom_sin(mom0_list, ls, ft)
    h5g['k_sin_sq'] = calc_momsq_sin(mom0_list, ls, ft) 
    h5g['k_sym']    = calc_mom_sym(mom0_list, ls, ft)
    h5g['k_sym_sq'] = calc_momsq_sym(mom0_list, ls, ft)
    h5g.file.flush()

def runme_collect_nnbar_twomom_avgH4(ensc, h5file_kpath_list, h5g) :
    """ collect 2-mom nnbar from jobs """
    ls,ft = ensc.latsize, ensc.ft_twist
        
    nnbar_map = {}
    nnbar_list_aa   = []
    invh4_list= []

    for i_mom, (h5file, h5kpath) in enumerate(h5file_kpath_list) :
        h5f_in  = h5py.File(h5file, 'r')
        h5g_in  = h5f_in[h5kpath]
        mom_all6= h5g_in.attrs['mom_list']
        mom0    = mom_all6[0]
        mom0rev = inv_wavevec(mom0, ls, ft)
        
        invh4   = H4_invariants(mom0, ls, ft)
        j_mom   = find_first_arr_tol(invh4, invh4_list, 1e-6)
        if None == j_mom : 
            j_mom = len(invh4_list)
            invh4_list.append(invh4)
        print(i_mom, j_mom, mom0, invh4)

        nnbar_m_i = nnbar_map.setdefault(j_mom, {})
        nnbar_m_i.setdefault('mom_all6', []).append(mom_all6)
        
        if (mom_all6[:2] == [mom0, mom0]).all() :       ## aa or bb
            dict_incr(nnbar_m_i, 'cnt_aa_bb')
            dict_incr(nnbar_m_i, 'nnbar_aa_bb', h5g_in.value)
            print(i_mom, 'aa', mom0, mom_all6[1])
        elif (mom_all6[:2] == [mom0, mom0rev]).all() :
            dict_incr(nnbar_m_i, 'cnt_ab')
            dict_incr(nnbar_m_i, 'nnbar_ab', h5g_in.value)
            print(i_mom, 'ab', mom0, mom_all6[1])
        else : raise ValueError("bad 6-mom spec\n%s" % str(mom_all6))
        h5f_in.close()

    n_mom = len(invh4_list)
    for i in range(n_mom) :
        print(i, invh4_list[i], nnbar_map[i]['cnt_aa_bb'], nnbar_map[i]['cnt_ab'], len(nnbar_map[i]['mom_all6']))

    nnb_aa_bb = np.concatenate([ nnbar_map[i]['nnbar_aa_bb'] / nnbar_map[i]['cnt_aa_bb']
                                 for i in range(n_mom) ], axis=1)
    nnb_ab    = np.concatenate([ nnbar_map[i]['nnbar_ab'] / nnbar_map[i]['cnt_ab']
                                 for i in range(n_mom) ], axis=1)
    mom0_list = np.array([ nnbar_map[i]['mom_all6'][0][0]
                           for i in range(n_mom) ])


    h5_purge_keys(h5g, ['nnbar', 'invh4_list',
                        'mom_list', 'k_lin' ])
    h5g['nnbar']    = 0.4 * nnb_aa_bb + 0.6 * nnb_ab
    h5g['mom_list'] = mom0_list
    h5g['k_lin']    = calc_mom_lin(mom0_list, ls, ft)
    h5g['invh4_list']= invh4_list
    h5g.file.flush()
""" Usage
h5fk_list = [('prop.st20-redoneBC/diag4d_2mom/nnbar_job%04d.h5' %j, 'nnbar_job%04d' %j) for j in range(192) ]
h5g_out = h5py.File('prop.st20-redoneBC/diag4d_2mom/nnbar_collect.h5', 'w').require_group('npr')
runme_collect_nnbar_twomom_avgjobs(ens_cfg_48c96pt, h5fk_list, h5g_out)
runme_calc_save_qbargammaq_excmom(ens_cfg_48c96pt, 'amabin', h5g_out['mom_list'].value, h5g_out)
"""


def calc_npr_qbargammaq_general(prop_in, prop_out, rsplan) :
    """ compute 
        (1/Nsc)*Tr[<Gout>^{-1} . <Gout . Gamma[i] . rev(Gin)> . <rev(Gin)>^{-1} . Gamma[j]]
    """
    prop_inrev          = qqbar_rev(prop_in)
    prop_inrev_rs       = lhpd.resample(prop_inrev, rsplan)
    prop_inrev_inv_rs   = qqbar_inv(prop_inrev_rs)
    
    prop_out_rs         = lhpd.resample(prop_out, rsplan)
    prop_out_inv_rs     = qqbar_inv(prop_out_rs)

    # contraction Qbar.Gamma.Q : separate matrix products for efficiency
    qqbar_cnct      = np.einsum('...abcd,...ide->...iabce', prop_out, Gamma16)
    qqbar_cnct      = np.einsum('...iabcd,...cdef->...iabef', qqbar_cnct, prop_inrev)
    qqbar_cnct_rs   = lhpd.resample(qqbar_cnct, rsplan)

    # amputation: 
    qqbar_vert_rs   = np.einsum('...abcd,...icdef->...iabef', prop_out_inv_rs, qqbar_cnct_rs)
    qqbar_vert_rs   = np.einsum('...icdef,...efch->...idh', qqbar_vert_rs, prop_inrev_inv_rs)
    qqbar_vert_rs   = np.einsum('...idh,...jhd->...ij', qqbar_vert_rs, Gamma16) / NCOLSPIN

    # TODO check volume factors
    return qqbar_vert_rs


def runme_calc_save_qbargammaq_excmom(ensc, ama_mode, 
        mom_list, h5grp, h5key='qbargammaq',
        rsplan=('jk',1), VERBOSE=True) :
    """ compute and save qbargammaq vertices for excmom configuration (mom_out == mom_in)
    """
    qbarq_list = []
    for mom in mom_list :
        if VERBOSE: print(mom)
        qprop   = ensc.get_frwprop_ft_selmom([mom], ama_mode)
        qbarq   = calc_npr_qbargammaq_general(qprop, qprop, rsplan=rsplan)
        qbarq_list.append(qbarq)

    h5_purge_keys(h5grp, [h5key])
    h5grp[h5key] = np.concatenate(qbarq_list, axis=1) # concat along momentum dir
    h5grp.file.flush()


from lhpd.misc import list_del_dup as del_dup

def runme_calc_save_qbargammaq_excmom_avgdir(ensc, ama_mode, 
        mom_list, h5g,
        rsplan=('jk',1), VERBOSE=True) :
    """ compute and save qbargammaq vertices for excmom configuration (mom_out == mom_in)
    """
    ls,ft = ensc.latsize, ensc.ft_twist
    momsq_lin_list = []
    qbarq_map = {}
    for i_mom, mom in enumerate(mom_list) :
        momsq   = calc_momsq_lin(mom, ls, ft)
        if VERBOSE: 
            print(i_mom, mom, momsq)
        j_mom   = find_first(momsq, momsq_lin_list, make_cmp_tol(1e-6))
        if None is j_mom :
            j_mom = len(momsq_lin_list)
            momsq_lin_list.append(momsq)
        qbarq_m_i = qbarq_map.setdefault(j_mom, {})
        qprop   = ensc.get_frwprop_ft_selmom([mom], ama_mode)
        qbarq   = calc_npr_qbargammaq_general(qprop, qprop, rsplan=rsplan)
        #qbarq_list.append(qbarq)
        dict_incr(qbarq_m_i, 'qbarq', qbarq)
        dict_incr(qbarq_m_i, 'qbarq_cnt')
        qbarq_m_i.setdefault('mom_list', []).append(mom)
    
    n_mom = len(momsq_lin_list)
    for j in range(n_mom) :
        print(j, momsq_lin_list[j], qbarq_map[j]['mom_list'])

    momsq_lin_list = np.asarray(momsq_lin_list)
    mom0_list = np.array([ qbarq_map[j]['mom_list'][0] 
                          for j in range(n_mom) ])
    qbarq_list = np.concatenate([ qbarq_map[j]['qbarq'] / qbarq_map[j]['qbarq_cnt'] 
                            for j in range(n_mom) ], axis=1)
    
    h5_purge_keys(h5g, ['qbargammaq', 'mom_list', 'k_lin'])
    h5g['qbargammaq']   = qbarq_list
    h5g['mom_list']     = mom0_list
    h5g['k_lin']        = calc_mom_lin(mom0_list, ls, ft)
    h5g.file.flush()

def runme_calc_save_qbargammaq_excmom_avgH4(ensc, ama_mode, 
        mom_list, h5g,
        rsplan=('jk',1), VERBOSE=True) :
    """ compute and save qbargammaq vertices for excmom configuration (mom_out == mom_in)
    """
    ls,ft = ensc.latsize, ensc.ft_twist
    invh4_list= []
    qbarq_map = {}
    for i_mom, mom in enumerate(mom_list) :
        invh4   = H4_invariants(mom, ls, ft)
        if VERBOSE: 
            print(i_mom, mom, invh4)
        #j_mom   = find_first(invh4, invh4_list, make_cmp_tol(1e-6))
        j_mom   = find_first_arr_tol(invh4, invh4_list, 1e-6)
        if None is j_mom :
            j_mom = len(invh4_list)
            invh4_list.append(invh4)
        qbarq_m_i = qbarq_map.setdefault(j_mom, {})
        qprop   = ensc.get_frwprop_ft_selmom([mom], ama_mode)
        qbarq   = calc_npr_qbargammaq_general(qprop, qprop, rsplan=rsplan)
        #qbarq_list.append(qbarq)
        dict_incr(qbarq_m_i, 'qbarq', qbarq)
        dict_incr(qbarq_m_i, 'qbarq_cnt')
        qbarq_m_i.setdefault('mom_list', []).append(mom)
    
    n_mom = len(invh4_list)
    for j in range(n_mom) :
        print(j, invh4_list[j], qbarq_map[j]['mom_list'])

    mom0_list = np.array([ qbarq_map[j]['mom_list'][0] 
                          for j in range(n_mom) ])
    momsq_lin_list = np.asarray([ invh4_list[k][0] for k in range(n_mom) ])
    qbarq_list = np.concatenate([ qbarq_map[j]['qbarq'] / qbarq_map[j]['qbarq_cnt'] 
                            for j in range(n_mom) ], axis=1)
    
    h5_purge_keys(h5g, ['qbargammaq', 'invh4_list',
                'k_lin', 'mom_list'])
    h5g['qbargammaq']   = qbarq_list
    h5g['mom_list']     = mom0_list
    h5g['invh4_list']   = invh4_list
    h5g['k_lin']        = calc_mom_lin(mom0_list, ls, ft)
    h5g.file.flush()

"""Usage
mom_list_full=h5py.File('prop.st20-redoneBC/diag4d_2mom/momlist_nnbar.h5','r')['mom_list'].value
mom_list_full=np.array(del_dup(map(tuple, mom_list_full.reshape(-1,4))))
runme_calc_save_qbargammaq_excmom_avgdir(ens_cfg_48c96pt, 'amabin', mom_list_full, h5g_out, rsplan=('jk',('ama',81,1)))
"""

from lhpd.phys.qcd import qcd_integrate, make_betafunc_fixorder, make_betafunc_series, make_gammafunc_series, qcd_integrate_range 



# beta function coeff
b_coeff = [ -(11/3.*3 - 4/.3*.5*3) ]
c_coeff = [ 
    # in all lines, assume R<->L also
    [ -12 ],        # [0] R3 L0,    -> [0] RRR3     a3b0
    [  -2 ],        # [1] R1 L0 _1  -> [1] RRR1     a1b0_1   
    [  -6 ],        # [2,3,4] R2 L1 -> [2] RR2L1    a2b1
    [   0 ],        # [5] R0 L1     -> [3] R1LL0    a0b1     
    [  +2 ],        # [6] R1 L0 _2  -> [4] RR1L0    a1b0
]



def print_final_nnbar(ensc, h5g, rsplan, string_ve=lhpd.spprint_ve) :
    momscale   = np.sqrt(h5g['k_lin_sq'].value) * ensc.ainv
    n_mom   = len(momscale)
    
    qbarq   = h5g['qbargammaq'].value.real.diagonal(axis1=-1, axis2=-2)
    qbarq_A = -(qbarq[..., 14] + qbarq[..., 13] + qbarq[..., 11] + qbarq[..., 7]) / 4.
    qbarq_V =  (qbarq[..., 1] + qbarq[..., 2] + qbarq[..., 4] + qbarq[..., 8] ) / 4.
    qbarq_T = -(qbarq[..., 3] + qbarq[..., 5] + qbarq[..., 6] 
              + qbarq[..., 9] + qbarq[..., 10] + qbarq[..., 12] ) /6.
    qbarq_A_a, qbarq_A_e = lhpd.calc_avg_err(qbarq_A, rsplan)
    qbarq_V_a, qbarq_V_e = lhpd.calc_avg_err(qbarq_V, rsplan)
    qbarq_T_a, qbarq_T_e = lhpd.calc_avg_err(qbarq_T, rsplan)

    qbarq_z_V_a, qbarq_z_V_e = lhpd.calc_avg_err(ensc.ZA * qbarq_A / qbarq_V, rsplan)
    qbarq_z_T_a, qbarq_z_T_e = lhpd.calc_avg_err(ensc.ZA * qbarq_A / qbarq_T, rsplan)
    
    # TODO switch between cases with 'nnbar' or 'nnbar_aa/ab/bb'
    if 'nnbar' in h5g.keys() :
        nnbar = h5g['nnbar'].value.real.diagonal(axis1=-1, axis2=-2)
    elif all([k in h5g.keys() for k in ['nnbar_aa', 'nnbar_ab', 'nnbar_bb' ] ]) :
        nnbar   = (  .2 * h5g['nnbar_aa'].value.real
                   + .6 * h5g['nnbar_ab'].value.real
                   + .2 * h5g['nnbar_bb'].value.real
                  ).diagonal(axis1=-1, axis2=-2)
    else :
        raise ValueError('missing nnbar keys')


    nnbar_rl= (nnbar[...,:7] + nnbar[..., 7:]) / 2.
    nnbar_RRR3    = nnbar_rl[..., 0]
    nnbar_RRR1    = nnbar_rl[..., 1]
    nnbar_RR2L1   = nnbar_rl[..., 2:5].mean(-1)
    nnbar_R1LL0   = nnbar_rl[..., 5]
    nnbar_RR1L0   = nnbar_rl[..., 6]
    # XXX note the order
    nnbar_all     = np.r_[ [ nnbar_RRR3,  nnbar_RRR1, 
                             nnbar_RR2L1, nnbar_R1LL0, 
                             nnbar_RR1L0] ]
    nnbar_all   = np.rollaxis(nnbar_all, 0, len(nnbar_all.shape))


    nnbar_all_a, nnbar_all_e = lhpd.calc_avg_err(nnbar_all, rsplan)
    print("# lattice vertex functions")
    print("# i[1]  mu[GeV][2]  LA[3,4]  LV[5,6]  LT[7,8]  LnnbarRRR3[9,10] LnnbarRRR1[11,12] LnnbarRR2L1[13,14] LnnbarR1LL0[15,16] LnnbarRR1L0[17,18]")
    for i in range(n_mom) : 
        a, e = nnbar_all_a[i], nnbar_all_e[i]
        nnb_str = '\t'.join([ string_ve(a[j], e[j]) for j in range(5) ])
        print("%d\t%f\t%s\t%s\t%s\t%s" % (
                i, momscale[i], 
                string_ve(qbarq_A_a[i], qbarq_A_e[i]), 
                string_ve(qbarq_V_a[i], qbarq_V_e[i]), 
                string_ve(qbarq_T_a[i], qbarq_T_e[i]),
                nnb_str))


    nnbar_z_all = (qbarq_A[..., None] * ensc.ZA)**3 / nnbar_all    # here is our Znnbar
    nnbar_z_all_a, nnbar_z_all_e = lhpd.calc_avg_err(nnbar_z_all, rsplan)
    print("")
    print("")
    print("# lattice Z factors")
    print("# i[1]  mu[GeV][2]  ZA[3,4]  ZV[5,6]  ZT[7,8]  ZnnbarRRR3[9,10] ZnnbarRRR1[11,12] ZnnbarRR2L1[13,14] ZnnbarR1LL0[15,16] ZnnbarRR1L0[17,18]")
    for i in range(n_mom) : 
        a, e = nnbar_z_all_a[i], nnbar_z_all_e[i]
        nnb_str = '\t'.join([ string_ve(a[j], e[j]) for j in range(5) ])
        print("%d\t%f\t%s\t%s\t%s\t%s" % (
                i, momscale[i], 
                string_ve(ensc.ZA, ensc.dZA),
                string_ve(qbarq_z_V_a[i], qbarq_z_V_e[i]), 
                string_ve(qbarq_z_T_a[i], qbarq_z_T_e[i]), 
                nnb_str))
    
    alpha, zzT= qcd_integrate_range(momscale, 2., 0.2956, [1], 
                    make_betafunc_series([-9., -64., -643.833, -12090.4]), 
                    make_gammafunc_series([[-1.33333, -34.4444, -976.638]]))
    qbarq_zsi_T_a, qbarq_zsi_T_e = qbarq_z_T_a / zzT[:,0], qbarq_z_T_e / zzT[:,0]

    alpha, zz = qcd_integrate_range(momscale, 2., 0.2956, [1,1,1,1,1], 
                    make_betafunc_series([-9., -64., -643.833, -12090.4]), 
                    make_gammafunc_series([[-12], [-2], [-6.], [0.], [2.]]))
    nnbar_zsi_all = nnbar_z_all / zz
    nnbar_zsi_all_a, nnbar_zsi_all_e = lhpd.calc_avg_err(nnbar_zsi_all, rsplan)
    print("")
    print("")
    print("# scale-independent ZRI")
    print("# i[1]  mu[GeV][2]  ZA[3,4]  ZV[5,6]  ZT[7,8]  ZnnbarRRR3[9,10] ZnnbarRRR1[11,12] ZnnbarRR2L1[13,14] ZnnbarR1LL0[15,16] ZnnbarRR1L0[17,18]")
    for i in range(n_mom) : 
        a, e = nnbar_zsi_all_a[i], nnbar_zsi_all_e[i]
        nnb_str = '\t'.join([ string_ve(a[j], e[j]) for j in range(5) ])
        print("%d\t%f\t%s\t%s\t%s\t%s" % (
                i, momscale[i], 
                string_ve(ensc.ZA, ensc.dZA),
                string_ve(qbarq_z_V_a[i], qbarq_z_V_e[i]), 
                string_ve(qbarq_zsi_T_a[i], qbarq_zsi_T_e[i]), 
                nnb_str))
    return momscale, nnbar_z_all, nnbar_zsi_all


import dataview as dv
from lhpd.fitter import f_wrap, fr_wrap

def plot_final_nnbar(ensc, h5g, rsplan, ax=None, stg_list=dv.style_group_default, fit_range=None) :
    # nnbar[i_data, i_mom, i_z]
    momscale, nnbar_z, nnbar_zsi = print_final_nnbar(ensc, h5g, rsplan)
    n_z = nnbar_z.shape[-1]
    
    if None is fit_range : 
        xr_sl = slice(None)
        fit_range = [ momscale.min(), momscale.max() ]
    else :
        xr_sl = np.where(np.logical_and(fit_range[0] <= momscale,
                                        momscale <= fit_range[1]))[0]
    print("fitting range %s; mom = %s" % (xr_sl, momscale[xr_sl]))
    print(xr_sl)

    xr_fit      = dv.make_range(momscale[xr_sl], 20, margin=.02)
    xr_extrap   = dv.make_range(momscale, 20, min_or=0., margin=0.02)
    xr_trans    = lambda x:x**2 # plot vs x^2
    f_lin   = lambda x,p:(p[0]+p[1] * x**2)

    if None is ax : ax = dv.make_std_axes()
    labels = [  r'$(RRR)_{\mathbf3}$', 
                r'$(RRR)_{\mathbf1}$', 
                r'$(RR)_{\mathbf2} L_{\mathbf1}$', 
                r'$R_{\mathbf1} (LL)_{\mathbf0}$', 
                r'$(RR)_{\mathbf1} L_{\mathbf0}$' ]
    for i_z in range(n_z) :
        p_rs, chi2_rs   = dv.do_fit_rs(f_wrap(f_lin),
                        momscale[xr_sl], nnbar_zsi[:, xr_sl, i_z], rsplan, p0=[0,0])
        p_a, p_e        = lhpd.calc_avg_err(p_rs, rsplan)
        chi2_a, chi2_e  = lhpd.calc_avg_err(chi2_rs, rsplan)
        zsi_avg_a, zsi_avg_e = lhpd.calc_avg_err(nnbar_zsi[:, xr_sl, i_z].mean(-1), rsplan)
        print('# i_z = %d  zsi_avg=%s' % (
                i_z, lhpd.spprint_ve(zsi_avg_a, zsi_avg_e)))
        lhpd.pprint_ve_ncov(p_a, p_e, None)

        stg = stg_list[i_z]
        dv.plot_data_rs(momscale, nnbar_zsi[:, :, i_z], rsplan, 
                        ax, stg, xtransform=lambda x:x**2, label=labels[i_z])
        dv.plot_func_band_rs(f_lin, xr_extrap, p_rs, rsplan, 
                             ax, stg.copy(alpha_shade=.1, edgecolor=(0,0,0,0)), xtransform=xr_trans)
        dv.plot_func_band_rs(f_lin, xr_fit, p_rs, rsplan, 
                             ax, stg.copy(alpha_shade=.5, edgecolor=(.5,.5,.5,.5), linewidth=.1), xtransform=xr_trans)
        ax.errorbar([xr_extrap[1]], [p_a[0]], yerr=[p_e[0]], **stg.copy(marker='*').edot())

    ax.set_xlim([0, None])
    ax.set_ylim([0, None])
    ax.set_xlabel(r'$\mu^2$ [GeV${}^2$]')
    ax.set_ylabel(r'$Z^\mathrm{SI}(\mu) / Z^\mathrm{pert}(2 GeV)$')
    ax.legend()
    return ax
    
"""
# debug
#momlist_debug=np.asarray([[5,5,5,10],[10,10,10,20],[-5,-5,-5,-11],[-10,-10,-10,-21]])
#qqbar_debug=ens_cfg_48c96pt.get_frwprop_ft(momlist_debug, 'sloppy', VERBOSE=True)

h5f=h5py.File('prop.st20-redoneBC/tmp.nnbar-debug.h5', 'a')
#h5f['/x/selmom'] = qqbar_debug
#lhpd.h5_io.h5_set_datalist(h5f['/x/selmom'], ens_cfg_48c96pt.ama_mode_list['sloppy']['meas_spec_list'])
#h5f['/x/selmom'].attrs['mom_list'] = momlist_debug
qqbar_debug=np.asarray(h5f['/x/selmom'])



#h5f['/x/nnbar_cnct'] = nnbar.get_nnbar_cnct_all()

rsplan=('jk', 1)
#rsplan=('jk', 81) # to bin all sloppy together, reduce nnbar-resampling & amp. cost
print(time.asctime()) ; nnbar_cnct_proj=calc_nnbar_npr_posneg1d(qqbar_debug[:30,0:2], qqbar_debug[:30,2:4], rsplan=rsplan) ; print(time.asctime())



print('\n\n'.join([ '\t'.join([ lhpd.spprint_ve(a[i,j], e[i,j]) for j in range(14) ]) for i in range(14) ]))
"""
