from __future__ import print_function
import math
import numpy as np
import h5py
import itertools as it


def runme_group_files(ensc, ama_mode) :
    """ group all data form 1 config and ama_mode into one h5 file """
    msl = ensc.ama_mode_list[ama_mode]['meas_spec_list']
    m = {}
    for c, csrc in msl :
        if c in m :
            m[c].append(csrc)
        else:
            m[c] = [ csrc ]
    for c in sorted(m.keys()) :
        for csrc in m[c] :
            h5src   = h5py.File(ensc.get_frwprop_ft_file(c, csrc, ama_mode), 'r')
            h5src_node = h5src['/x/prop_ft']
            h5dst   = h5py.File(ensc.get_frwprop_ft_file_groupbycfg(
                                c, csrc, ama_mode), 'a')
            h5dst.copy(h5src_node, '/%s/x%d_y%d_z%d_t%d/prop_ft' % ((c,) + tuple(csrc)))
            h5dst.close()
            h5src.close()
            print(c, csrc)
        print(c, 'done')

def runme_average_prop(ensc, VERBOSE=False, cfg_list=None) :
    """ compute 'slonly', 'unbias', 'exonly' by averaging over all samples on 1 config 
        put each data type into one h5 file 
    """
    msl_ex  = ensc.ama_mode_list['exact']['meas_spec_list']
    msl_sl  = ensc.ama_mode_list['sloppy']['meas_spec_list']
    
    if None is cfg_list :
        cfg_set = set(x[0] for x in msl_ex)
        assert(set(x[0] for x in msl_sl) == cfg_set)
        cfg_list= list(cfg_set)
        cfg_list.sort(cmp=lambda x,y : cmp(int(x), int(y)))

    n_cfg       = len(cfg_list)
    n_data_av   = n_cfg

    prop_data_shape = (n_data_av,) + tuple(ensc.latsize_save) + (4,4,3,3)

    h5f_slonly  = h5py.File(ensc.get_frwprop_ft_avg_file('slonly'), 'a')
    h5ds_slonly = h5f_slonly.require_dataset('/x/prop_ft', prop_data_shape, np.complex128, fletcher32=True)
    h5f_unbias  = h5py.File(ensc.get_frwprop_ft_avg_file('unbias'), 'a')
    h5ds_unbias = h5f_unbias.require_dataset('/x/prop_ft', prop_data_shape, np.complex128, fletcher32=True)
    h5f_exonly  = h5py.File(ensc.get_frwprop_ft_avg_file('exonly'), 'a')
    h5ds_exonly = h5f_exonly.require_dataset('/x/prop_ft', prop_data_shape, np.complex128, fletcher32=True)

    if (VERBOSE) :
        pb = lhpd.progbar(n_data_av)

    for i_data, c in enumerate(cfg_list) :
        # average sloppy
        y_av_sl     = np.zeros(prop_data_shape[1:], np.complex128)
        cnt         = 0
        for csrc in [ x[1] for x in msl_sl if x[0] == c ] :
            h5f = h5py.File(ensc.get_frwprop_ft_file_groupbycfg(
                            c, csrc, 'sloppy'), 'r')
            y = h5f['/%s/x%d_y%d_z%d_t%d/prop_ft' % ((c,) + tuple(csrc))].value
            h5f.close()
            y_av_sl = y_av_sl + y
            cnt += 1
            if (VERBOSE) : print('sl', c, csrc, time.ctime())
            #if 2 <= cnt  : break # FIXME debug part

        y_av_sl /= cnt
        
        y_av_ex     = np.zeros(prop_data_shape[1:], np.complex128)
        y_av_bias   = np.zeros(prop_data_shape[1:], np.complex128)
        cnt         = 0
        for csrc in [ x[1] for x in msl_ex if x[0] == c ] :
            h5f = h5py.File(ensc.get_frwprop_ft_file(
                            c, csrc, 'exact'), 'r')
            y   = h5f['/x/prop_ft'].value
            h5f.close()
            y_av_ex = y_av_ex + y
            
            h5f = h5py.File(ensc.get_frwprop_ft_file_groupbycfg(
                            c, csrc, 'sloppy'), 'r')
            y_sl= h5f['/%s/x%d_y%d_z%d_t%d/prop_ft' % ((c,) + tuple(csrc))].value
            h5f.close()
            dy = y - y_sl
            y_av_bias = y_av_bias + dy

            cnt += 1
            if (VERBOSE) : print('ex', c, csrc, time.ctime())
            #if 2 <= cnt  : break # FIXME debug part
        
        y_av_ex     /= cnt
        y_av_bias   /= cnt

        h5ds_slonly[i_data] = y_av_sl
        h5f_slonly.flush()
        h5ds_exonly[i_data] = y_av_ex
        h6f_exonly.flush()
        h5ds_unbias[i_data] = y_av_sl + y_av_bias        
        h5f_unbias.flush()

        if (VERBOSE) :
            pb.step()
            pb.redraw()

    h5f_exonly.close()
    h5f_slonly.close()
    h5f_unbias.close()



def runme_select_momenta(ensc,  ama_mode, mom_list, 
            h5kpath='/x/prop_ft', VERBOSE=False,
            meas_slice=slice(None)) :

    h5f     = h5py.File(ensc.get_frwprop_ft_file_selmom(ama_mode), 'a')
    msl     = ensc.ama_mode_list[ama_mode]['meas_spec_list'][meas_slice]
    h5ds    = h5f.require_dataset(h5kpath, 
                        (len(msl), len(mom_list), NCOLOR, NSPIN, NCOLOR, NSPIN),
                        np.complex128, fletcher32=True)
    data    = ensc.get_frwprop_ft(mom_list, ama_mode, 
                    meas_slice=meas_slice, VERBOSE=VERBOSE)
    h5ds[:] = data
    lhpd.h5_io.h5_set_datalist(h5ds, msl)
    h5kpath_momlist = "%s.MOMLIST" % h5kpath
    h5f[h5kpath_momlist] = mom_list
    h5ds.attrs['mom_list'] = h5kpath_momlist
    h5f.flush()
    h5f.close()
"""
# save select momenta into a separate file:
pmax=11
momlist_alldiag = make_alldiag_vec(pmax) * np.r_[1,1,1,2]
momlist_alldiag[:,3] -= np.where(momlist_alldiag[:,3]<0, 1, 0)
for m in ['sloppy', 'exact'] : runme_select_momenta(ens_cfg_48c96pt, m, momlist_alldiag, VERBOSE=True)
"""
