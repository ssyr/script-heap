# XXX moving to lhpd/latcorr/runme/nnbar/pproc_nnbar.py
from __future__ import print_function
import numpy as np
import h5py
import itertools as it

import tsv
import lhpd





def pack_nnbar_text2hdf(h5f, h5kpath, meas_spec_list, ama_mode_str) :
    
    n_data = len(meas_spec_list)
    h5node = None
    nnbar_list = None
    for i_data, meas_spec in enumerate(meas_spec_list) :
        fname_cat = get_nnbarcat_file(meas_spec, ama_mode_str)
        d   = np.r_[tsv.read_float(open(fname_cat, 'r'))][0, 0, :, 4:]
        if None == h5node:
            n_3pt, n_t = d.shape[-2:]
            h5node = h5f.require_dataset(h5kpath, (n_data, n_3pt, n_t), 
                        np.float64, exact=True, fletcher32=True)
        else:
            assert ((n_data, n_3pt, n_t) == h5node.shape)

        h5node[i_data] = d

        fname_list = get_nnbarlist_file(meas_spec, ama_mode_str)
        nnbar_list_cur = [ s.strip() for s in open(fname_list, 'r').readlines() ]
        if None == nnbar_list :
            nnbar_list = nnbar_list_cur
        else :
            assert(nnbar_list == nnbar_list_cur)

    return nnbar_list

def runme_pack_nnbar_text2hdf(ama_mode) :
    ama_mode_str    = ama_mode['approx']
    meas_spec_list  = ama_mode['meas_spec_list']
    
    h5out   = get_c3pt_pack_h5_file(ama_mode_str)
    h5kpath = '/data_%s' % ama_mode_str
    print("PACK->%s[%s]" % (h5out, h5kpath))
    h5f = h5py.File(h5out, 'a')
    nnbar_list = pack_nnbar_text2hdf(h5f, h5kpath, meas_spec_list, ama_mode_str)
    h5f[h5kpath + '_list'] = np.array(nnbar_list, 'S')
    h5f[h5kpath + '__DATALIST'] = meas_spec_list
    h5f.flush()
    h5f.close()


def get_3pt_h5key(
        had_type_src, had_rep_src, had_sm_src,
        had_type_snk, had_rep_snk, had_sm_snk,
        op):
    return '%s_%s_%s_%s/%s%s/%s' % (
                had_type_src, had_rep_src,
                had_type_snk, had_rep_snk,
                had_sm_src, had_sm_snk, op)

def get_2pt_h5key(had_type, had_rep, sm_src, sm_snk, psnk):
    return '%s_%s/%s%s/PX%d_PY%d_PZ%d' % (
                had_type, had_rep,
                sm_src, sm_snk,
                psnk[0], psnk[1], psnk[2])


def read_float_line(fname) : 
    return map(float, open(fname, 'r').readline().split())

def conv_c3pt_tsv2hdf(
        h5, h5key_prefix,
        had_type_src, had_rep_src, had_sm_src,
        had_type_snk, had_rep_snk, had_sm_snk,
        op, t1_list, t1max, 
        meas_spec_list, ama_mode_str) :
    """ write data[i_meas, t2] from tsv_f to [:,t1,:] of h5f[h5node] 
    """
    # [i_data, -t1, t2]
    if 1 <= VERBOSE: 
        print(
                had_type_src, had_rep_src, had_sm_src, 
                had_type_snk, had_rep_snk, had_sm_snk, op)
    h5key   = '%s/%s' % (
            h5key_prefix.rstrip('/'), 
            get_3pt_h5key(
                    had_type_src, had_rep_src, had_sm_src,
                    had_type_snk, had_rep_snk, had_sm_snk, op))
    h5node  = None

    if False:
        for i_data, meas_spec in enumerate(meas_spec_list) :

            for t1 in t1_list: 
                assert t1 < t1max
                # input: [0, 0, i_data, t2]
                fname = get_orig3pt_file(
                        had_type_src, had_rep_src, had_sm_src,
                        had_type_snk, had_rep_snk, had_sm_snk,
                        op, t1,
                        meas_spec, ama_mode_str)
                if 1 <= VERBOSE: print(fname )

                #d = (    np.r_[tsv.read_float(open(fname, 'r'))][0, 0, 0, 4:]
                #+1j*np.r_[tsv.read_float(open(fname + '_im', 'r'))][0, 0, 0, 4:])
                d = (np.r_[read_float_line(fname)] + 1j*np.r_[read_float_line('%s_im' % fname)])[4:]
                print(d.shape)
                if h5node is None:
                    n_data  = len(meas_spec_list)
                    n_t2    = d.shape[-1]
                    h5node  = h5.require_dataset(h5key, (n_data, t1max, n_t2), 
                                np.complex128, exact=True, fletcher32=True)

                # output: [i_data, t1, t2]
                h5node[i_data, t1]  = d

    elif False :
        import import_mb_nnbar_data as nnbar_in
        cfg_list = [ m[0] for m in meas_spec_list ]
        csrc_list = [ m[1] for m in meas_spec_list ]
        res = nnbar_in.conv_c3pt_tsv2hdf_cy(
                had_type_src, had_rep_src, had_sm_src,
                had_type_snk, had_rep_snk, had_sm_snk,
                op, t1_list, t1max, 
                cfg_list, csrc_list, ama_mode_str)
        h5node  = h5.require_dataset(h5key, res.shape, 
                            np.complex128, exact=True, fletcher32=True)
        h5node[:] = res
        

    else:
        h5in_name   = get_c3pt_pack_h5_file(ama_mode_str)
        h5in        = h5py.File(h5in_name, 'r')
        h5kpath     = '/data_%s' % ama_mode_str
        h5src       = h5in[h5kpath]
        nnbar_list  = h5in[h5kpath + '_list'][:]
        nnbar_map   = dict([ (s[1], s[0]) for s in enumerate(nnbar_list) ])
        meas_spec_list_src = h5in[h5kpath + '__DATALIST'][:]
        assert ((meas_spec_list_src == meas_spec_list).all())
        sm_tag      = {'S' : 'SHELL', 'P' : 'POINT'}

        n_data      = h5src.shape[0]
        n_t2        = h5src.shape[-1]
        h5node      = h5.require_dataset(h5key, (n_data, t1max, n_t2), 
                        np.complex128, exact=True, fletcher32=True)

        for t1 in t1_list: 
            assert t1 < t1max
            nnbar_name = 'nnbar_%s_%s_%s_%s_%s_smearN_%s_SINK_smearNstar_%s_SINK_px0py0pz0_t%d' % (
                    op, had_type_snk, had_rep_snk, had_type_src, had_rep_src, 
                    sm_tag[had_sm_snk], sm_tag[had_sm_src], t1)
            i_re = nnbar_map[nnbar_name]
            i_im = nnbar_map['%s_im' % nnbar_name]
            h5node[:, t1, :] = h5src[:, i_re] + 1j*h5src[:, i_im]


    lhpd.latcorr.h5_set_datalist(h5node, meas_spec_list)

    h5node.attrs['dim_spec']        = np.array(['i_data', 't1', 't2'])
    h5node.attrs['source_hadron']   = had_type_src
    h5node.attrs['sink_hadron']     = had_type_snk
    h5node.attrs['source_rep']      = had_rep_src
    h5node.attrs['sink_rep']        = had_rep_snk
    
    h5node.attrs['ama_mode']        = ama_mode_str
    h5node.attrs['t1_list']         = np.asarray(t1_list)


def conv_c2pt_tsv2hdf(
        h5, h5key_prefix, 
        had_type, had_rep, 
        sm_src, sm_snk, psnk, 
        tmax,
        meas_spec_list, ama_mode_str):
    """ write data[i_meas, t2] from tsv_f to [:,t1,:] of h5f[h5node] 
        to an opened h5 file
    """
    if 1 <= VERBOSE: print(had_type, had_rep, sm_src+sm_snk)

    n_data = len(meas_spec_list)
    
    h5key   = '%s/%s' % (
            h5key_prefix.rstrip('/'), 
            get_2pt_h5key(had_type, had_rep, sm_src, sm_snk, psnk))
    h5node  = None

    # [i_data, t]
    for i_data, meas_spec in enumerate(meas_spec_list):

        fname = get_orig2pt_file(had_type, had_rep, sm_src, 
                                 sm_snk, meas_spec, ama_mode_str)
        #d = (    np.r_[tsv.read_float(open(fname, 'r'))][0, 0, 0, 4:]
        #+1j*np.r_[tsv.read_float(open(fname + '_im', 'r'))][0, 0, 0, 4:])
        #d = (np.r_[read_float_line(fname)] + 1j*np.r_[read_float_line('%s_im' % fname)])[4:]
        d = (    np.fromfile(fname, dtype=float, sep=' ') + 
              1j*np.fromfile('%s_im' % fname, dtype=float, sep=' '))[4:]
        # input: [0, 0, i_data, t]
        n_t = d.shape[-1]
        assert tmax == Lt

        if None == h5node : 
            h5node = h5.require_dataset(h5key, (n_data, n_t),
                        np.complex128, exact=True, fletcher32=True)
        
        # output: [i_data, t]
        h5node[i_data]  = d

    lhpd.latcorr.h5_set_datalist(h5node, meas_spec_list)
    
    h5node.attrs['dim_spec']        = np.array(['i_data', 't'])
    h5node.attrs['source_hadron']   = had_type
    h5node.attrs['sink_hadron']     = had_type
    h5node.attrs['source_rep']      = had_rep
    h5node.attrs['sink_rep']        = had_rep
    h5node.attrs['ama_mode']        = ama_mode_str


def runme_conv_all(
        ama_mode,
        source_list=source_list,
        sink_list=sink_list,
        source_sink_list=src_snk_list,
        op_list=op_list,
        sm_list=sm_list,
        do_c2pt=True,
        do_c3pt=True
        ):

    ama_mode_str    = ama_mode['approx']
    meas_spec_list  = ama_mode['meas_spec_list']

    #if True: # switch
    if do_c2pt : # switch

        for had_type, had_rep in source_list + sink_list :
            for sm_src in ['P'] : #sm_list : hack : no shell source
                for sm_snk in sm_list :
                    h5 = h5py.File(get_twopt_bin_h5_file(
                            had_type, had_rep, sm_src, sm_snk, ama_mode_str), 'a')
                    conv_c2pt_tsv2hdf(
                            h5, '/c2pt',
                            had_type, had_rep, 
                            sm_src, sm_snk, [0,0,0], Lt, 
                            # making data to have uniform length: 1048 samples
                            meas_spec_list, ama_mode_str)
                    h5.close()

    #print("stop here for now..." ; return)

    if do_c3pt :
    #if False: # switch

        for had_type_src, had_rep_src, had_type_snk, had_rep_snk in source_sink_list:
                for sm_src in sm_list:
                    for sm_snk in sm_list:
                        h5 = h5py.File(get_c3pt_bin_h5_file(
                                had_type_src, had_rep_src, had_type_snk, had_rep_snk,
                                sm_src, sm_snk, ama_mode_str), 'a')
                        for op in op_list:
                            conv_c3pt_tsv2hdf(
                                    h5, '/nnbar_c3pt',
                                    had_type_src, had_rep_src, sm_src,
                                    had_type_snk, had_rep_snk, sm_snk,
                                    op, range(t1max), t1max,
                                    meas_spec_list, ama_mode_str)

                                    
                        h5.close()

def runme_bin_twopt(
        c2pt_psnk=c2pt_psnk,
        source_list=source_list,
        sink_list=sink_list,
        source_sink_list=src_snk_list,
        sm_list=sm_list ) :

    for (had_type, had_rep), sm_src, sm_snk in it.product(
                source_list + sink_list, ['P'], sm_list) :
        h5f_sl = get_twopt_bin_h5_file(had_type, had_rep, sm_src, sm_snk, 'sloppy')
        h5f_ex = get_twopt_bin_h5_file(had_type, had_rep, sm_src, sm_snk, 'exact')
        h5k = "/c2pt/%s" % get_2pt_h5key(had_type, had_rep, sm_src, sm_snk, c2pt_psnk)
        bin_by_cfg = lambda m:m['ckpoint_id']   # binning by cfg#
        
        h5f_exbin = get_twopt_bin_h5_file(had_type, had_rep, sm_src, sm_snk, 'ex-bin')
        lhpd.h5_io.h5file_bin_general(h5f_exbin, h5k, [(h5f_ex, h5k)], bin_by_cfg)
        print("bin %s -> %s" % (h5f_ex, h5f_exbin))
        
        h5f_slbin = get_twopt_bin_h5_file(had_type, had_rep, sm_src, sm_snk, 'sl-bin')
        lhpd.h5_io.h5file_bin_general(h5f_slbin, h5k, [(h5f_sl, h5k)], bin_by_cfg)
        print("bin %s -> %s" % (h5f_sl, h5f_slbin))

        h5f_unbias = get_twopt_bin_h5_file(had_type, had_rep, sm_src, sm_snk, 'unbias')
        lhpd.h5_io.h5file_bin_unbias_general(h5f_unbias, h5k, 
                [(h5f_sl, h5k)], [(h5f_ex, h5k)], bin_by_cfg,
                except_attr=['ama_mode'])
        print("bin+unbias (%s,%s) -> %s" % (h5f_ex, h5f_sl, h5f_slbin))
                
def runme_bin_c3pt(
        source_sink_list=src_snk_list,
        sm_list=sm_list,
        op_list=op_list,
        ) :

    for (had_type_src, had_rep_src, had_type_snk, had_rep_snk), sm_src, sm_snk in it.product(
                source_sink_list, sm_list, sm_list) :
        h5f_sl = get_c3pt_bin_h5_file(had_type_src, had_rep_src, had_type_snk, 
                                      had_rep_snk, sm_src, sm_snk, 'sloppy')
        h5f_ex = get_c3pt_bin_h5_file(had_type_src, had_rep_src, had_type_snk, 
                                      had_rep_snk, sm_src, sm_snk, 'exact')
        bin_by_cfg = lambda m:m['ckpoint_id']   # binning by cfg#

        h5f_exbin = get_c3pt_bin_h5_file(had_type_src, had_rep_src, had_type_snk, 
                                             had_rep_snk, sm_src, sm_snk, 'ex-bin')
        h5f_slbin = get_c3pt_bin_h5_file(had_type_src, had_rep_src, had_type_snk, 
                                         had_rep_snk, sm_src, sm_snk, 'sl-bin')
        h5f_unbias= get_c3pt_bin_h5_file(had_type_src, had_rep_src, had_type_snk, 
                                         had_rep_snk, sm_src, sm_snk, 'unbias')
        for op in op_list:
            h5k = "/nnbar_c3pt/%s" % get_3pt_h5key(
                    had_type_src, had_rep_src, sm_src,
                    had_type_snk, had_rep_snk, sm_snk, op)

            lhpd.h5_io.h5file_bin_general(h5f_exbin, h5k, [(h5f_ex, h5k)], bin_by_cfg)
            print("bin %s -> %s [%s]" % (h5f_ex, h5f_exbin, h5k))
            
            lhpd.h5_io.h5file_bin_general(h5f_slbin, h5k, [(h5f_sl, h5k)], bin_by_cfg)
            print("bin %s -> %s [%s]" % (h5f_sl, h5f_slbin, h5k))

            lhpd.h5_io.h5file_bin_unbias_general(h5f_unbias, h5k, 
                    [(h5f_sl, h5k)], [(h5f_ex, h5k)], bin_by_cfg,
                    except_attr=['ama_mode'])
            print("bin+unbias (%s,%s) -> %s [%s]" % (h5f_ex, h5f_sl, h5f_unbias, h5k))
            
