from future.utils import iteritems
import numpy as np
import h5py
import lhpd

NCOLOR, NSPIN = 3, 4
#dirprop_shape = (NCOLOR, NSPIN, NCOLOR, NSPIN)




class ens_cfg_48c96pt_class :
    latsize = np.r_[ 48, 48, 48, 96 ]
    latdim  = len(latsize)
    latvolume = np.prod(latsize)

    t_axis  = 3
    latsize_save = latsize / 2                  # save only 1/2 sites in each direction
    lat_save_max = latsize_save / 2             # ... centered around zero
    #lat_save= ( latsize - lat_save_max,         # lat_src
                #latsize_save,                   # latsize_dst
                #latsize_save - lat_save_max)    # lat_dst
    lat_save_c0_orig = latsize - lat_save_max   # lat_src
                #latsize_save,                   # latsize_dst
    lat_save_c0_save = latsize_save - lat_save_max # lat_dst
    ferm_bc = np.r_[  1,  1,  1, -1 ]
    ft_twist= np.r_[  0,  0,  0, .5 ]   # ferm_bc = exp(i*2*pi*twist) for NPR
    
    gauge_bc    = np.r_[  1,  1,  1,  1 ]
    gauge_twist = np.r_[  0,  0,  0,  0 ]

    lat_aniso   = np.r_[  1,  1,  1,  1 ]   # a[i] = a / aniso[i]

    # read cfg list from files
    cfg_list = [ int(s) for s in open('cfgkey_list.redoneBC.st20', 'r').readlines()] 
    #cfg_list = [ 1040,1120,1200,1280,1360,1440,1600,1680,1760,1840,1920,2000,2080,2160 ] # 14(run5a,4,~4b,~3)
    cfg_list = list(set(cfg_list))
    cfg_list.sort()

    meas_spec_list= np.array([ 
            (str(c), [ 16*ix, 16*iy, 16*iz, 32*it])
              for c in cfg_list
              for it in range(3) for iz in range(3) 
              for iy in range(3) for ix in range(3) ],
            dtype=[('ckpoint_id', 'S32'), ('source_coord', np.int32, (4,))])
    meas_spec_list_sl_exact = np.s_[::81]

    ama_mode_list = {
        'exact' : {
            'meas_spec_list' : meas_spec_list[meas_spec_list_sl_exact]
        },
        'unbias': {
            'meas_spec_list' : meas_spec_list[meas_spec_list_sl_exact]
        },
        'slonly': {
            'meas_spec_list' : meas_spec_list[meas_spec_list_sl_exact]
        },
        'exonly': {
            'meas_spec_list' : meas_spec_list[meas_spec_list_sl_exact]
        },
        'sloppy' : {
            'meas_spec_list' : meas_spec_list,
            'nev'       : 500,
            'ncg'       : None # don't know
        },
        'unit'  : {
            'meas_spec_list' : np.array([ ( 'UNIT', [ 11, 23, 34, 78]) ],
                                dtype=meas_spec_list.dtype) },
        'weak'  : {
            'meas_spec_list' : np.array([ ( 'WEAK', [ 11, 23, 34, 78]) ],
                                dtype=meas_spec_list.dtype) },

    }
    assert((ama_mode_list['exact']['meas_spec_list'] == 
            ama_mode_list['sloppy']['meas_spec_list'][meas_spec_list_sl_exact]).all())
    data_top = '/g/g92/syritsyn/lscratch-data/mdwf_48c96_ml0.00078/gfix-aztec/data'

    def get_frwprop_ft_avg_file(self, ama_mode) :
        assert (ama_mode in ['unbias', 'slonly', 'exonly'])
        return '%s/frw_prop_pp_ft_avg.st20-redoneBC/prop_FT_m0.00078_%s.h5' % (
                self.data_top, ama_mode)
    def get_frwprop_ft_file(self, cfg_key, csrc, ama_mode) :
        """ ama_mode : string """
        assert (ama_mode in ['exact', 'sloppy'])
        return '%s/frw_prop_pp_ft/prop_FT_m0.00078_%s_x%d_y%d_z%d_t%d_%s.h5' % (
                self.data_top, cfg_key, csrc[0], csrc[1], csrc[2], csrc[3], 
                ama_mode)
    def get_gauge_ft_file(self, cfg_key) :
        return '%s/gauge_ft/ckpoint_lat.%s.h5' % (self.data_top, cfg_key)

    def get_frwprop_ft_file_groupbycfg(self, cfg_key, csc, ama_mode) :
        return '%s/frw_prop_pp_ft_groupbycfg/prop_FT_m0.00078_%s_%s.h5' % (
                self.data_top, cfg_key, ama_mode)

    def get_frwprop_ft_file_selmom(self, ama_mode) :
        """ perf.hack for frequently used momenta: save to separate files """
        return '%s/frw_prop_pp_ft_avg.st20-redoneBC/prop_FT_m0.00078_selmom_%s.h5' % (
                self.data_top, ama_mode)

    def get_frwprop_ft_selmom(self, mom_list, ama_mode, h5kpath='/x/prop_ft',
            meas_slice=slice(None), VERBOSE=False) :
        """ perf.hack for frequently used momenta: save to separate files 
            storage format: X[i_data, i_mom, ic,is,jc,js] (!!! already transposed)
                            X.attrs['mom_list'] = mom4d[i_mom, mu]
        """
        if 'amabin' == ama_mode :
            if (slice(None) != meas_slice) :
                raise NotImplementedError
            y_ap    = self.get_frwprop_ft_selmom(mom_list, 'sloppy', h5kpath=h5kpath, 
                                meas_slice=meas_slice, VERBOSE=VERBOSE)
            y_ex    = self.get_frwprop_ft_selmom(mom_list, 'exact', h5kpath=h5kpath, 
                                meas_slice=meas_slice, VERBOSE=VERBOSE)
            assert(np.s_[::81] == self.meas_spec_list_sl_exact)
            return lhpd.make_ama_bins_simple(81, 1, y_ap, y_ap[self.meas_spec_list_sl_exact], y_ex)


        h5f     = h5py.File(self.get_frwprop_ft_file_selmom(ama_mode), 'r')
        h5d     = h5f[h5kpath]
        
        h5d_mom_list = np.asarray(h5d.attrs['mom_list'])
        mom_list = np.asarray(mom_list)
        assert(len(mom_list.shape) == 2)
        sl_mom  = [ ndi[0] for ndi in lhpd.np_find_first_list(mom_list, h5d_mom_list) ]
        
        msl     = self.ama_mode_list[ama_mode]['meas_spec_list'][meas_slice]
        # FIXME only simple slices
        if slice(None) != meas_slice : 
            raise ValueError(meas_slice)
        res     = np.empty((len(msl), len(sl_mom), 
                            NCOLOR, NSPIN, NCOLOR, NSPIN), np.complex128)
        for i_m, mom in enumerate(mom_list) :
            res[:, i_m] = h5d[:, sl_mom[i_m]]
        return res

    
    def mom_index_full2saved(self, mom_list) :
        """ transform wavevec from normal meaning to index in h5 file """
        mom_list = (mom_list + self.latsize - self.lat_save_c0_orig) % self.latsize
        if not (np.all(0 <= mom_list) and np.all(mom_list < self.latsize_save)) :
            raise IndexError
        mom_list = (mom_list + self.lat_save_c0_save) % self.latsize_save
        return mom_list
    
    def get_frwprop_ft(self, mom_list, ama_mode, meas_slice=slice(None), VERBOSE=False) :
        """ mom_n : (list of) propagator moment(a)um to read
            RETURN [i_data, i_mom, ic,is,jc,js]
        """
        # TODO split into separate functions for separate/grouped/averaged, make `get_frwprop_ft' a switch-wrapper
        mom_list = np.asarray(mom_list)
        assert (2 == len(mom_list.shape))
        mom_list = self.mom_index_full2saved(mom_list)

        msl     = self.ama_mode_list[ama_mode]['meas_spec_list'][meas_slice]
        # [i_data, i_mom, ic,is,jc,js]
        res     = np.empty((len(msl), len(mom_list), 
                        NCOLOR, NSPIN, NCOLOR, NSPIN), np.complex128)

        # TODO verify with AVP :
        #       qlua saves prop to h5 as [is, js, ic, jc] == <q(is,ic) qbar(js,jc)> 
        qio_transp = (2,0,3,1)  # QIO[is,js,ic,jc] -> PyNPR[ic,is,jc,js])

        if (ama_mode in [ 'unit', 'weak' ]) : # "crutches" for test cases
            assert (1 == len(msl))
            cfg_key = {'unit' : 'UNIT', 'weak' : 'WEAK'}[ama_mode]
            assert (cfg_key == msl[0]['ckpoint_id'])
            csrc = msl[0]['source_coord']
            h5f_name = self.get_frwprop_ft_file(cfg_key, csrc, 'exact')
            h5f = h5py.File(h5f_name, 'r')
            for i_m, m in enumerate(mom_list) :
                x           = h5f['/x/prop_ft'][tuple(m)]
                res[0, i_m] = x.transpose((2,0,3,1))
            h5f.close()

        elif ama_mode in [ 'exonly', 'slonly', 'unbias' ] :
            # all data in a single file
            h5f = h5py.File(self.get_frwprop_ft_avg_file(ama_mode), 'r')
            for i_m, m in enumerate(mom_list) :
                if len(msl) == len(self.ama_mode_list[ama_mode]['meas_spec_list'][meas_slice]) :
                    x = h5f['/x/prop_ft'][(slice(None),) + tuple(m)]
                    for i_data in range(len(msl)) :
                        res[i_data, i_m] = x[i_data].transpose(qio_transp)
                else : # non-trivial slice FIXME detect if contiguous => do a single read
                    for i_data, (c, csrc) in enumerate(msl) :
                        x = h5f['/x/prop_ft'][(i_data,) + tuple(m)]
                        res[i_data, i_m] = x.transpose(qio_transp)
            h5f.close()

        elif 'sloppy' == ama_mode :
            # optimize by opening each h5file only once
            file_list   = [ self.get_frwprop_ft_file_groupbycfg(c, csrc, ama_mode)
                            for c, csrc in msl ]
            msl_file_map = {}
            for i_data, (c, csrc) in enumerate(msl) :
                f   = self.get_frwprop_ft_file_groupbycfg(c, csrc, ama_mode)
                msl_file_map.setdefault(f, []).append(i_data)

            # read cycle
            if (VERBOSE) : pb  = lhpd.progbar(len(msl), prefix=('%d mom' % len(mom_list)))
            for f, f2i in iteritems(msl_file_map) :
                h5f = h5py.File(f, 'r')
                for i_data in f2i :
                    c, csrc = msl[i_data]
                    for i_m, m in enumerate(mom_list) :
                        res[i_data, i_m] = h5f['/%s/x%d_y%d_z%d_t%d/prop_ft' % (
                                 (c,) + tuple(csrc))][tuple(m)].transpose(qio_transp)
                    if (VERBOSE) :
                        pb.step()
                        pb.redraw()
                h5f.close()
            if VERBOSE : pb.finish()


        elif  'exact'  == ama_mode :
            # simple ungrouped read
            if (VERBOSE) : pb  = lhpd.progbar(len(msl), prefix=('%d mom' % len(mom_list)))
            for i_data, (c, csrc) in enumerate(msl) :
                h5f = h5py.File(self.get_frwprop_ft_file(c, csrc, ama_mode), 'r')
                for i_m, m in enumerate(mom_list) :
                    res[i_data, i_m] = h5f['/x/prop_ft'][tuple(m)].transpose((2,0,3,1))
                h5f.close()

                if (VERBOSE) :
                    pb.step()
                    pb.redraw()
            if VERBOSE : pb.finish()
        else : raise NotImplementedError

        return res
    
    def get_gauge_ft(self, mom_list, meas_slice=slice(None), VERBOSE=False) :
        """ mom_n : (list of) propagator moment(a)um to read
            RETURN [i_data, i_mom, ic,is,jc,js]
        """
        mom_list = np.asarray(mom_list)
        assert (2 == len(mom_list.shape))
        mom_list = self.mom_index_full2saved(mom_list)

        msl     = np.asarray(self.cfg_list)[meas_slice]
        # [i_data, i_mom, ic,is,jc,js]
        res     = np.empty((len(msl), len(mom_list), 
                        self.latdim, NCOLOR, NCOLOR), np.complex128)

        if (VERBOSE) :
            pb  = lhpd.progbar(len(msl), prefix=('%d mom' % len(mom_list)))
        for i_data, cfg_key in enumerate(msl) :
            h5f_name = self.get_gauge_ft_file(cfg_key)
            h5f = h5py.File(h5f_name, 'r')
            for i_m, m in enumerate(mom_list) :
                for mu in range(self.latdim) :
                    res[i_data, i_m, mu] = h5f['/gauge_ft/dir_%d' % mu][tuple(m)]

            h5f.close()
            if (VERBOSE) :
                pb.step()
                pb.redraw()
        
        if VERBOSE : pb.finish()

        return res

    # physical parameters
    ainv        = 1.730 #GeV; pay attention to anisotropy
    ZA          = 0.71191
    dZA         = 0.00005


ens_cfg_48c96pt = ens_cfg_48c96pt_class()
