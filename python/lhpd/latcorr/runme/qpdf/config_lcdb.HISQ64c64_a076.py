import aff
import h5py 
import numpy as np
import math
import itertools as it
import lhpd
from lhpd import make_src_grid
from lhpd.misc import strkey

# for run data.in/c23_1 -> /volatile/pqpdf/luchang/sergey-3pt/data
# * pions : no polarization
# * c2pt, qpdf

# runs with c2pt/*aff.fast
#   data.in/scan_boostsm -> /volatile/pqpdf/luchang/sergey-boost-smear/data
#   data.in/scan_boostsm2 -> /volatile/pqpdf/luchang/sergey-boost-smear-2/data

# runs with hadspec
#   data.in/scan_wup -> /volatile/pqpdf/luchang/sergey-wup/data
#   data.in/scan_wup2 -> /volatile/pqpdf/luchang/sergey-wup2/data       with .aff.fast
#   data.in/scan_wup_cg -> /volatile/pqpdf/luchang/sergey-wup-coulomb/data  EMPTY???

# XXX requires
#   ama_str()

# XXX dat_d = {data_dir=<aff&h5 raw&result data files>

def ama_str(ama) : return ama
def srcsnk_str_f(cN_d) : return '%s.%s' %(cN_d['smsrcsnk'], cN_d['smtag'])
def srcsnk_str_k(cN_d) : return '%s/%s' %(cN_d['smsrcsnk'], cN_d['smtag'])

def c3snk_str_f(c3_d) : return strkey.snk_str_f(c3_d['psnk'], c3_d['tsep'])
def c3snk_str_k(c3_d) : return strkey.snk_str_k(c3_d['psnk'], c3_d['tsep'])
def ins_str_f(ins_d)  : 
    return '%s.%s' % (ins_d['flav'], ins_d['gf_tag'])
def ins_str_k(ins_d)  : 
    return '%s/%s' % (ins_d['flav'], ins_d['gf_tag'])


def hadron_str(cN_d) : 
    had = cN_d['had']
    return "%s_%s" %(had, cN_d['tpol'])
    #elif 'pion'   == had : return "%s" % (had,)
    #else : raise ValueError(had)
hadron_str_f = hadron_str
hadron_str_k = hadron_str

def hadron_str_k_aff(cN_d) :
    had = cN_d['had']

    if   is_baryon(had) : return "%s_%s" %(had, cN_d['tpol'])
    elif is_meson(had)  : 
        meson_list = [
            "a0_1", "rho_x_1", "rho_y_1", "b1_z_1",
            "rho_z_1", "b1_y_1", "b1_x_1", "pion_2",
            "a0_2", "rho_x_2", "rho_y_2", "a1_z_1",
            "rho_z_2", "a1_y_1", "a1_x_1", "pion_1" ]
        ind = meson_list.index(hadron_str_k(cN_d))
        return "meson_g%d" % (ind,)
    else : raise ValueError(had)


def dat_str_f(which, dat_d) :       # only for conv, all, bin, ub
    dat_kind = dat_d['kind']
    if   'conv' == dat_kind : 
        return '%s/%s.%s.%s' % (which, which, 
                dat_d['cfgkey'], ama_str(dat_d['ama']))
    elif 'all'  == dat_kind :
        return '%s.all/%s-all.%s' % (
                which, which, ama_str(dat_d['ama']))
    elif 'bin'  == dat_kind :
        return '%s.bin/%s-bin.%s' % (
                which, which, ama_str(dat_d['ama']))
    elif 'ub'   == dat_kind : 
        return '%s.bin/%s-unbias' % (which, which)
    else : raise ValueError(dat_kind)


#################### c2pt ########################
# c2_d = dict(had='proton', smsrcsnk=, smtag=)
# example
#   c2pt/c2pt.1002.ex.CG52bxp30_CG52bxp30.x17y13z11t1_x17y13z11t33.aff
#       //c2pt/SS/proton_Tg15/PX-1_PY1_PZ1
def get_c2pt_file(dat_d, c2_d) :
    dat_kind    = dat_d['kind']

    if   'aff'  == dat_kind :
        #c2pt/c2pt.1380.ex.CG90bxp00_CG90bxp00.x10y34z46t42.aff
        return 'c2pt/c2pt.%s.%s.%s.%s.aff' % (
                dat_d['cfgkey'], ama_str(dat_d['ama']),
                c2_d['smtag'], strkey.csrcgrp_str_f(dat_d['csrcgrp']))
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
        return '%s.%s.%s.h5' % (
                dat_str_f('c2pt', dat_d), 
                c2_d['had'], srcsnk_str_f(c2_d))
    else : raise ValueError(dat_kind)

def get_c2pt_kpath(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    if   'aff'  == dat_kind :
        #/c2pt/SS/x10y34z46t42/proton_Tg15
        return '/c2pt/%s/%s/%s' % (
                c2_d['smsrcsnk'], strkey.csrcgrp_str_f(dat_d['csrcgrp']), hadron_str_k_aff(c2_d))
    elif 'conv' == dat_kind : 
        return '/cfg%s/c2pt/%s/%s' % (
                dat_d['cfgkey'], c2_d['smsrcsnk'], c2_d['had'])
    elif ('all' == dat_kind or
          'bin' == dat_kind or
          'ub'  == dat_kind) :
        return '/c2pt/%s/%s' % (c2_d['smsrcsnk'], c2_d['had'])
    else : raise ValueError(dat_kind)

#################### c3pt ########################
# c3_d  = dict(had='proton', tpol=, psnk=, tsep=, smsrcsnk=, smtag=)
# ins_d = dict(kind=, flav=, op=, ir=)
# example
#   qpdf/qpdf.1002.ex.x17y13z11t1_x17y13z11t33.PX5PY0PZ0dt8.CG52bxp30_CG52bxp30.aff
#       //qpdf/SS/meson/x17y13z11t1_x17y13z11t33/PX5_PY0_PZ0_dt8/l8_XXXXXXXX/g15/qx-1_qy0_qz1
def get_c3pt_file(dat_d, c3_d, ins_d) :
    # XXX bb is very generic: also tmd or qpdf, just change 'bb' appropriately
    ins_kind    = ins_d['kind']
    dat_kind    = dat_d['kind']

    if   'qpdf'   == ins_kind :
        # bb is very generic: also tmd or qpdf, just change 'bb' appropriately
        if   'aff'  == dat_kind :
            #qpdf/qpdf_ts6/qpdf.1380.ex.proton_posSxminus.D.x10y34z46t42.PX0PY0PZ0dt6.CG90bxp00_CG90bxp00_hyp.aff
            return 'qpdf/qpdf.%s.%s.%s.%s.%s.%s.%s_%s.aff' % (
                    dat_d['cfgkey'], ama_str(dat_d['ama']), hadron_str_f(c3_d), ins_d['flav'], 
                    strkey.csrcgrp_str_f(dat_d['csrcgrp']), c3snk_str_f(c3_d), 
                    c3_d['smtag'], ins_d['gf_tag'])
        elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            return '%s.%s.%s.%s.%s.h5' % (
                    dat_str_f('qpdf', dat_d),
                    c3snk_str_f(c3_d), hadron_str_f(c3_d), 
                    srcsnk_str_f(c3_d), ins_str_f(ins_d))
        else : raise ValueError((dat_kind, ins_kind))

    #elif 'op'   == ins_kind :
        #if dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            #return '%s.%s.%s.%s.%s.%s.h5' % (
                    #dat_str_f('op', dat_d),
                    #c3snk_str_f(c3_d), hadron_str_f(c3_d), srcsnk_str_f(c3_d), 
                    #ins_d['flav'], ins_d['op'])
        #else : raise ValueError((dat_kind, ins_kind))
    else : raise ValueError(ins_kind)

def get_c3pt_kpath(dat_d, c3_d, ins_d) :
    # XXX bb is very generic: also tmd or qpdf, just change 'bb' appropriately
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']
    if   'qpdf'   == ins_kind :
        if   'aff'  == dat_kind :
            #/qpdf/SS/proton_posSxminus/D/x10y34z46t42/PX0_PY0_PZ0_dt6/l32_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/g15/qx-2_qy-2_qz0
            return '/qpdf/%s/%s/%s/%s/%s' % (
                    c3_d['smsrcsnk'], 
                    hadron_str_k(c3_d), ins_d['flav'], 
                    strkey.csrcgrp_str_k(dat_d['csrcgrp']), 
                    c3snk_str_k(c3_d))
        elif 'conv' == dat_kind : 
            return '/cfg%s/qpdf/%s/%s/%s/%s' % (
                    dat_d['cfgkey'], 
                    c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_str_k(ins_d))
        elif ('all' == dat_kind or
              'bin' == dat_kind or
              'ub'  == dat_kind) :
            return '/qpdf/%s/%s/%s/%s' % (
                    c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_str_k(ins_d))
        else : raise ValueError((dat_kind, ins_kind))

    #elif 'op'   == ins_kind :       # TODO fix flavor
        #if   'conv' == dat_kind : 
            #return '/cfg%s/op/%s/%s/%s/%s/%s/%s' % (
                    #dat_d['cfgkey'], c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    #c3snk_str_k(c3_d), ins_d['flav'], ins_d['op'], ins_d['ir'])
        #elif ('all' == dat_kind or
              #'bin' == dat_kind or
              #'ub'  == dat_kind) :
            #return '/op/%s/%s/%s/%s/%s/%s' % (
                    #c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    #c3snk_str_k(c3_d), ins_d['flav'], ins_d['op'], ins_d['ir'])
        #else : raise ValueError((dat_kind, ins_kind))

############# common function #############
def get_lcdb_loc(dat_d, lc_d) :
    what    = lc_d['kind']
    ddir    = dat_d['data_dir']
    if   'c2'   == what : 
        c2_d    = lc_d['c2_d']
        return ('%s/%s' % (ddir, get_c2pt_file(dat_d, c2_d)), 
                get_c2pt_kpath(dat_d, c2_d))
    elif 'c3'   == what :
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        return ('%s/%s' % (ddir, get_c3pt_file(dat_d, c3_d, ins_d)), 
                get_c3pt_kpath(dat_d, c3_d, ins_d))
    else : raise ValueError(ins_kind)
