import numpy as np
import lhpd
import itertools as it
import h5py
import aff
import os, sys, time, tempfile
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted
from lhpd.misc.strkey import *

latsize = np.r_[48,48,48,96]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]
run_tag_list = [ 'm0340', 'm0390' ]


meson_list = [ 'pion_1' ]
baryon_list= [ 'proton_1' ]
c2pt_psnk_list  = normsorted(list(int_vectors_maxnorm(3, 10)))

#c3pt_tpol_list = [ 'AApSzplus' ]
#c3pt_tsep_list = [ 6, 7, 8, 9, 10]
#c3pt_flav_list = ['U', 'D']
#c3pt_op_list   = [ 'tensor1', 'pstensor1' ]


###############################################################################
# AMA & source coord selection
###############################################################################

cfg_index   = lhpd.read_key_list('list.cfg.all', 'index')
cfg_list    = lhpd.read_key_list('list.cfg.run1', 'samples')


meas_x0 = np.r_[0,0,0,0]
meas_x1 = np.r_[8,8,0,0]
meas_dxc= np.r_[7,11,13,23]

ama_ex = 'ex'
ama_sl = 'sl'
ama_list = [ama_ex, ama_sl]

nsrc_ama_map = {
    ama_ex : [1,1,1,1], 
    ama_sl : [2,2,1,2] }
def make_csrcgrp_list(ama, cfgkey) :
    return lhpd.make_src_grid(nsrc_ama_map[ama], latsize, meas_x0, meas_dxc, 
            1 + cfg_index.index(cfgkey))



###############################################################################
# file locations 
###############################################################################
# XXX psnk is psnkrel everywhere; otherwise, files will be different with ksnk_tag / ksrc_tag

def ama_str(ama) : return ama

data_top='data.in'

def get_hadspec_aff_file(cfgkey, ama, csrc, run_tag) :
    return '%s/hadspec/hadspec.%s.%s.%s.%s.aff' % (
            data_top, cfgkey, ama_str(ama), run_tag, csrc_str_f(csrc))
def get_hadspec_aff_kpath(cfgkey, csrc, tpol) :
    return '/cfg%s/hadspec/SS/proton_%s/%s' % (
            cfgkey, tpol, csrc_str_k(csrc))
def make_hsfilekpath_func(cfgkey, ama, bsm_tag) :
    def hsfilekpath_func(csrc, tpol) :
        return (get_hadspec_aff_file(cfgkey, ama, csrc, bsm_tag),
                get_hadspec_aff_kpath(cfgkey, csrc, tpol))
    return hsfilekpath_func


#def get_bb_aff_file(cfgkey, ama, csrc, psnk, tsep, tpol, flav, bsm_tag) :
    #return "%s/bb/bb.%s.%s.%s.%sdt%d.%s.%s.%s.aff" % (
            #data_top, cfgkey, ama_str(ama), csrc_str_f(csrc), 
            #psnk_str_f(psnk), tsep, tpol, flav, bsm_tag)
#def get_bb_aff_kpath(cfgkey, csrc, psnk, tsnk, tpol, flav) :
    #return "/cfg%s/bb/SS/proton_%s/%s/%s/%s_T%d" % (
            #cfgkey, tpol, flav, csrc_str_k(csrc), psnk_str_k(psnk), tsnk)
#def make_bbfilekpath_func(cfgkey, ama, psnk, tsep, tpol, flav, bsm_tag) :
    #""" return aff file and kpath generator function to be used with `conv_bb2hdf'
    #"""
    #def bbfilekpath_func(csrc, tsnk) :
        #return (
            #get_bb_aff_file(cfgkey, ama, csrc, psnk, tsep, tpol, flav, bsm_tag),
            #get_bb_aff_kpath(cfgkey, csrc, psnk, tsnk, tpol, flav))
    #return bbfilekpath_func


data_out    = '.'

def get_hspec_h5_file(cfgkey, ama, had, bsm_tag) :
    return '%s/hspec/hspec.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, had, bsm_tag)
def get_hspec_bin_h5_file(ama, had, bsm_tag) :
    return '%s/hspec.bin/hspec-bin.%s.%s.%s.h5' % (
            data_out, ama, had, bsm_tag)
def get_hspec_unbias_h5_file(had, bsm_tag) :
    return '%s/hspec.bin/hspec-unbias.%s.%s.h5' % (
            data_out, had, bsm_tag)


#def get_bb_h5_file(cfgkey, ama, psnk, tsep, tpol_tag, flav, bsm_tag) :
    #return '%s/bb/bb.%s.%s.%sdt%d.%s.%s.%s.h5' % (
            #data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
            #tpol_tag, flav, bsm_tag)
#def get_bb_bin_h5_file(ama, psnk, tsep, tpol_tag, flav, bsm_tag) :
    #return '%s/bb.bin/bb-bin.%s.%sdt%d.%s.%s.%s.h5' % (
            #data_out, ama, psnk_str_f(psnk), tsep, 
            #tpol_tag, flav, bsm_tag)
#def get_bb_unbias_h5_file(psnk, tsep, tpol_tag, flav, bsm_tag) :
    #return '%s/bb.bin/bb-unbias.%s.%sdt%d.%s.%s.%s.h5' % (
            #data_out, psnk_str_f(psnk), tsep, 
            #tpol_tag, flav, bsm_tag)
#
#
#def get_op_h5_file(cfgkey, ama, psnk, tsep, tpol_tag, flav, op, bsm_tag) :
    #return '%s/op/op.%s.%s.%sdt%d.%s.%s.%s.%s.h5' % (
            #data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
            #tpol_tag, flav, op, bsm_tag)
#def get_op_bin_h5_file(ama, psnk, tsep, tpol_tag, flav, op, bsm_tag) :
    #return '%s/op.bin/op.%s.%sdt%d.%s.%s.%s.%s.h5' % (
            #data_out, ama, psnk_str_f(psnk), tsep, 
                #tpol_tag, flav, op, bsm_tag)
#def get_op_unbias_h5_file(psnk, tsep, tpol_tag, flav, op, bsm_tag) :
    #return '%s/op.bin/op-unbias.%sdt%d.%s.%s.%s.%s.h5' % (
            #data_out, psnk_str_f(psnk), tsep, 
                #tpol_tag, flav, op, bsm_tag)

# DISCO stuff
#def get_bbqloop_h5_file(cfgkey) :
    #return "%s/bb-qloop/bb-qloop.%s.h5" % (
            #data_out, cfgkey)
#
#def get_opdisc_h5_file(cfgkey, ama, psnk, op, bsm_tag) : 
    #return "%s/op-disc/op.%s.%s.%s.%s.h5" % (
            #data_out, cfgkey, ama, psnk_str_f(psnk), op)
