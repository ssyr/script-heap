from past.builtins import execfile
import numpy as np
import lhpd
import itertools as it
import h5py
import aff
import os, sys, time, tempfile
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted
from lhpd.misc import strkey
from lhpd.misc import ldir_3d, space2full
from lhpd.misc import dictnew, dict_prod

latsize = np.r_[64,64,64,64]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]

###############################################################################
# AMA & source coord selection
###############################################################################

cfg_index   = lhpd.read_key_list('cfglist.Hotqcd_hisqsea_a060_48c64.ALL', 'index')

meas_x0     = np.r_[0,0,0,0]
meas_dxc    = np.r_[7,11,13,23]
meas_dx_it  = np.r_[0,0,0,0]
ncoh_src_t  = 1 # number of coherent src in one group

def ama_str(ama) : return ama
ama_ex      = 'ex'
ama_sl      = 'sl'
ama_list    = [ama_ex, ama_sl]

nsrc_ama_map= {
    ama_ex : [1,1,1,1], 
    ama_sl : [2,2,2,4] }

def make_csrcgrp_list(ama, cfgkey):
    return lhpd.make_srcgrp_grid(
            nsrc_ama_map[ama], latsize, meas_x0, meas_dxc,
            1 + cfg_index.index(cfgkey), t_axis, ncoh_src_t, 
            dx_it=meas_dx_it)


###############################################################################
# correlator params
###############################################################################

pm1     = np.r_[-1:1+1] 
pm2     = np.r_[-2:1+2]
pm6     = np.r_[-6:1+6]
pm10    = np.r_[-10:1+10]

kbxp10  = space2full(ldir_3d['x'], t_axis)
kbxm10  = space2full(ldir_3d['X'], t_axis)
kbym10  = space2full(ldir_3d['Z'], t_axis)
kbzm10  = space2full(ldir_3d['Y'], t_axis)

smear_opt_b00       = dict(tag='CG90bxp00', boost=0*kbxp10)
srcsnk_smtag_list   = [ 
    # forward (symm) pairs
    (s['tag'], s['tag']) for s in [smear_opt_b00]
]

symm_smtag_list     = [ '%s_%s' % (s[0], s[0]) for s in srcsnk_smtag_list ]
skew_smtag_list     = [ '%s_%s' % (s[0], s[1]) for s in srcsnk_smtag_list ]

########### c2pt
c2pt_symm_psnk_list = normsorted(lhpd.range_prod([pm2, pm2, pm2]))
#c2pt_symm_psnk_list = normsorted(lhpd.range_prod([pm10, pm2, pm2]))

c2pt_smsrcsnk_list = ['SS', 'SP']
c2pt_smtag_list     = symm_smtag_list

c2pt_hslab_len      = latsize[t_axis] // ncoh_src_t

c2pt_spec_list      = [
    dict(had='proton', tpol_list=[ 'Tg%d' % g for g in range(16) ], 
         psnk_list=c2pt_symm_psnk_list, 
         smsrcsnk=smsrcsnk, smtag=smtag,
         saved_bc_t=-1)     # go to different records
    for smsrcsnk, smtag in it.product(c2pt_smsrcsnk_list, c2pt_smtag_list)
]


########### c3pt
c3pt_tlen           = 32
c3pt_tsep_list      = [ 8, 10, 12 ]
c3pt_psnk_smtag_list = [ 
    ([0,0,0], skew_smtag_list[0] ),
    #([3,0,0], skew_smtag_list[0] ),
    #([4,0,0], skew_smtag_list[1] ),
    #([5,0,0], skew_smtag_list[1] ),
    ]
c3pt_tpol_list      = [ 'posSzplus', 'posSxplus', 'posSxminus' ]
c3pt_spec_list      = [ 
    # pion_1
    dict(had='proton', tpol=tpol, psnk=psnk, tsep=tsep, smsrcsnk='SS', smtag=smtag, saved_bc_t=1)
    for ((psnk, smtag), tsep, tpol) in it.product(
            c3pt_psnk_smtag_list, c3pt_tsep_list, c3pt_tpol_list) 
]

c3pt_qext_list      = [ [0,0,0] ]
#c3pt_qext_list      = normsorted(lhpd.range_prod([pm2, pm2, pm2]))

qpdf_lmax       = 1
qpdf_spec_list  = [
    dict(kind='qpdf', ldir='x', lmin=-qpdf_lmax, lmax=qpdf_lmax, qext_list=c3pt_qext_list, flav=flav, gf_tag='hyp')
    for flav in ['U', 'D']
]

###############################################################################
# file locations (lcdb)
###############################################################################

data_dir_aff= 'XXX'
data_dir    = 'XXX'
execfile('config_lcdb.py')
