from past.builtins import execfile
import numpy as np
import lhpd
import itertools as it
import h5py
import aff
import os, sys, time, tempfile
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted
from lhpd.misc import strkey
from lhpd.misc import ldir_3d, space2full
from lhpd.misc import dictnew, dict_prod

latsize = np.r_[48,48,48,64]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]

###############################################################################
# AMA & source coord selection
###############################################################################

cfg_index   = lhpd.read_key_list('list.cfg.all', 'index')

meas_x0     = np.r_[0,0,0,0]
meas_dxc    = np.r_[7,11,13,23]
meas_dx_it  = latsize // 2 ; meas_dx_it[t_axis] = 0
ncoh_src_t  = 4 # number of coherent src in one group

def ama_str(ama) : return ama
ama_ex      = 'ex'
ama_sl      = 'sl'
ama_list    = [ama_ex, ama_sl]

nsrc_ama_map= {
    ama_ex : [1,1,1,4], 
    ama_sl : [2,2,2,4] }

#def make_csrc_list(ama, cfgkey) :
    #return lhpd.make_src_grid(
            #nsrc_ama_map[ama], latsize, meas_x0, meas_dxc, 
            #1 + cfg_index.index(cfgkey)).reshape(-1,1,len(latsize),
            #t_axis, dx_it=meas_dx_it)

def make_csrcgrp_list(ama, cfgkey):
    #ngrid_t     = csrc_ngrid[ama][t_axis]
    #assert(0 == ngrid_t % ncoh_src_t)
    #ncoh_grp    = ngrid_t // ncoh_src_t
    ## assuming sources are grouped so that csrc[t_axis] changes fastest
    #csrc_list   = make_csrc_list(ama, cfg_key)
    #ndim        =len(latsize)
    #csrcgrp_list= csrc_list.reshape(-1, ncoh_src_t, ncoh_grp, ndim) # split into groups with equal spatial coords
    #csrcgrp_list= csrcgrp_list.transpose(0,2,1,3)                   # rearrange to have max. t-separation in each grp
    #csrcgrp_list= csrcgrp_list.reshape(-1, ncoh_src_t, ndim)        # flatten (0,1) axes       
    #assert((csrcgrp_list[:, :, :-1] == csrcgrp_list[:, 0:1, :-1]).all())
    #return csrcgrp_list
    return lhpd.make_srcgrp_grid(
            nsrc_ama_map[ama], latsize, meas_x0, meas_dxc,
            1 + cfg_index.index(cfgkey), t_axis, ncoh_src_t, 
            dx_it=meas_dx_it)


###############################################################################
# correlator params
###############################################################################

pm1     = np.r_[-1:1+1] 
pm2     = np.r_[-2:1+2]
pm6     = np.r_[-6:1+6]
pm10    = np.r_[-10:1+10]

kbxp10  = space2full(ldir_3d['x'], t_axis)
kbxm10  = space2full(ldir_3d['X'], t_axis)
kbym10  = space2full(ldir_3d['Z'], t_axis)
kbzm10  = space2full(ldir_3d['Y'], t_axis)

smear_opt_b20       = dict(tag='CG52bxp20', boost=2*kbxp10)
smear_opt_b30       = dict(tag='CG52bxp30', boost=3*kbxp10)
srcsnk_smtag_list   = [ 
    # forward (symm) pairs
    (s['tag'], s['tag']) for s in [smear_opt_b20, smear_opt_b30]
]

symm_smtag_list     = [ '%s_%s' % (s[0], s[0]) for s in srcsnk_smtag_list ]
skew_smtag_list     = [ '%s_%s' % (s[0], s[1]) for s in srcsnk_smtag_list ]

########### c2pt
#c2pt_had_tpol_list  = [
    #dict(had='pion',    tpol=[ '1', '2' ]),
    #dict(had='proton',  tpol=[ 'Tg%d' % g for g in range(16) ]),
#]
#c2pt_psnk_list      = normsorted(list(int_vectors_maxnorm(3, 10)))
c2pt_symm_psnk_list = normsorted(lhpd.range_prod([pm10, pm1, pm1]))

c2pt_smsrcsnk_list = ['SS', 'SP']
c2pt_smtag_list     = symm_smtag_list

c2pt_hslab_len      = latsize[t_axis] // ncoh_src_t
c2pt_phrew_tmax     = latsize[t_axis]       # for phase reweighting

c2pt_spec_list      = [
    dictnew(ht, psnk_list=c2pt_symm_psnk_list,  # go to the same record
            smsrcsnk=smsrcsnk, smtag=smtag)     # go to different records
    for ht in [
        dict(had='pion',    tpol_list=['1', '2']),
        dict(had='proton',  tpol_list=[ 'Tg%d' % g for g in range(16) ]) ]
    for smsrcsnk, smtag in it.product(c2pt_smsrcsnk_list, c2pt_smtag_list)
]


########### c3pt
c3pt_tlen           = 16
c3pt_tsep_list      = [ 8, 10, 12 ]
c3pt_psnk_smtag_list_pion = [ 
    ([2,0,0], skew_smtag_list[0] ),
    ([3,0,0], skew_smtag_list[0] ),
    ([4,0,0], skew_smtag_list[1] ),
    ([5,0,0], skew_smtag_list[1] ),
    ]
c3pt_spec_list      = [ 
    # pion_1
    dictnew(dict(had='pion', tpol='1', smsrcsnk='SS'),
            psnk=psnk, smtag=smtag, tsep=tsep)
    for ((psnk, smtag), tsep) in it.product(
            c3pt_psnk_smtag_list_pion, c3pt_tsep_list) 
]

c3pt_qext_list      = normsorted(list(int_vectors_maxnorm(3, 3)))

qpdf_spec_list  = [
    dict(type='qpdf', ldir='x', lmin=-8, lmax=8, qext_list=c3pt_qext_list),
]

###############################################################################
# file locations (lcdb)
###############################################################################

data_dir_aff= 'data.in/c23_1'
data_dir    = 'data.out/c23_1.coh4X'
execfile('config_lcdb.py')
