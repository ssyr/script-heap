from __future__ import print_function
from past.builtins import execfile
#import ipdb
from future.utils import iteritems
import numpy as np
import h5py
import lhpd
from lhpd.misc import np_find_first, np_match, purge_keys, dict_copy_pop
from lhpd.h5_io import h5type_attr_fix_
import os, sys, time, tempfile, errno
from lhpd.pymath.H4_tensor import *
#import matplotlib as mpl
#import matplotlib.pyplot as plt
#import dataview as dv
from lhpd.limits import *

execfile('scripts-runme/pproc_common.py')

###############################################################################
# HADSPEC
###############################################################################

if False :
    def conv_hspec2hdf(
                h5g, h5key,             # output
                latsize, 
                hsfilekpath_func,       # input
                cfgkey, csrcgrp_list, psnk_list, hslab_len, 
                had, had_bc_t,
                attrs_kw, 
                t_dir=3, hs_dtype=np.complex128, h5_overwrite=True) :
        """
            csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
            
            save data to
            h5g[h5key][n_data, len(psnk_list), hslab_len]
        """
        """ TEST 
        """
        latsize     = np.asarray(latsize)
        ndim        = len(latsize)
        assert(4 == ndim)
        assert(3 == t_dir)  # smart, eh?
        lt          = latsize[t_dir]
        csrcgrp_list= np.asarray(csrcgrp_list)
        n_grp, grp_size = csrcgrp_list.shape[0:2]
        n_data      = n_grp * grp_size
        assert(4 == csrcgrp_list.shape[-1])
        n_psnk      = len(psnk_list)
        
        csrc_list   = []
        hs_res      = np.empty((n_data, n_psnk, hslab_len), hs_dtype)

        i_data = 0
        for i_grp, csrcgrp in enumerate(csrcgrp_list) :
            n_src       = len(csrcgrp)
            # check coord
            for i_src in range(n_src) :
                for mu in range(ndim):
                    assert(mu == t_dir or csrcgrp[i_src][mu] == csrcgrp[0][mu])
            print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
            csrc0       = csrcgrp[0]
            tsrc0       = csrc0[t_dir]
            
            hsfile, hskpath = hsfilekpath_func(csrc0, had)
            aff_r   = aff.Reader(hsfile)
            hs      = lhpd.latcorr.aff_read_hadspec_list(aff_r, hskpath, psnk_list)
            aff_r.close()

            for i_c, c in enumerate(csrcgrp) :
                t_sh    = (lt + c[t_dir] - tsrc0) % lt
                # correcting BC[t] applied in [Qlua]save_2pt_list
                if tsrc0 <= c[t_dir] : bc_factor = 1.
                else : bc_factor = 1. / had_bc_t
                hs_res[i_data + i_c] = bc_factor * hs[..., t_sh : t_sh + hslab_len]

            csrc_list.extend(list(csrcgrp))
            i_data += len(csrcgrp)

        assert(n_data == i_data)

        meas_spec_list = np.array([ (cfgkey, c) for c in csrc_list], 
                            dtype=lhpd.h5_io.h5_meas_dtype)
        assert(len(meas_spec_list) == n_data)

        if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5key])
        hs_shape = (n_data, n_psnk, hslab_len)
        h = h5g.require_dataset(h5key, hs_shape, hs_dtype, fletcher32=True)
        h[:] = hs_res

        # "axes"
        h.attrs['dim_spec']      = np.array(['i_data', 'i_psnk', 'i_t' ],
                                               dtype='S')
        # axes "ticks"
        lhpd.h5_io.h5_set_datalist(h, meas_spec_list)
        h.attrs['psnk_list']    = np.array(psnk_list, dtype=np.int32)
        h.attrs['t_list']       = np.r_[0 : hslab_len]

        for k, v in iteritems(attrs_kw) :
            h.attrs[k] = h5type_attr_fix_(v)

        h.file.flush()
        

    def conv_c2pt2hdf(
                data_out, data_in,      # output, input 
                dat_d,                  # cfgkey, ama
                c2_d,                   # had, smsrcsnk, smtag, tpol_list, psnk_list
                csrcgrp_list,
                latsize, hslab_tlen, had_bc_t, attrs_kw,
                t_dir=3, hs_dtype=np.complex128, h5_overwrite=True) :
        """ looping over "coherent" 2pt : compatible with non-coherent
            csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
            
            save data to
            h5g[h5key][n_data, len(psnk_list), hslab_tlen]

            XXX should also work for hspec_volcedm

            TODO check that hslabs do not overlap
        """
        latsize     = np.asarray(latsize)
        ndim        = len(latsize)
        assert(4 == ndim)
        assert(3 == t_dir)  # smart, eh?
        def get_spatial(x) : return np.asarray(x)[..., :t_dir]
        latsize_x   = get_spatial(latsize)
        lt          = latsize[t_dir]
        csrcgrp_list= np.asarray(csrcgrp_list)
        n_grp, grp_size = csrcgrp_list.shape[0:2]
        n_data      = n_grp * grp_size
        assert(4 == csrcgrp_list.shape[-1])

        # [i_psnk, mu]
        c2_d1, (psnk_list, tpol_list)   = dict_copy_pop(c2_d, ['psnk_list', 'tpol_list'])
        psnk_list   = np.asarray(psnk_list)
        n_psnk      = len(psnk_list)
        n_tpol      = len(tpol_list)
        
        csrc_list   = []
        hs_res      = np.empty((n_data, n_tpol, n_psnk, hslab_tlen), hs_dtype)

        i_data = 0
        for i_grp, csrcgrp in enumerate(csrcgrp_list) :
            n_src       = len(csrcgrp)
            print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
            csrc0       = csrcgrp[0]
            tsrc0       = csrc0[t_dir]
            
            for i_tpol, tpol in enumerate(tpol_list) :
                
                hsfile, hskpath = get_lcdb_loc('c2', 
                        dictnew(dat_d, data_dir=data_in, type='aff', csrcgrp=csrcgrp), 
                        c2_d=dictnew(c2_d1, tpol=tpol))
                aff_r   = aff.Reader(hsfile)
                hs      = lhpd.latcorr.aff_read_hadspec_list(aff_r, hskpath, psnk_list)
                aff_r.close()

                for i_c, c in enumerate(csrcgrp) :
                    # [mu]
                    x10dl   = np.asarray(get_spatial(c - csrc0), np.float64) / latsize_x
                    # [i_qext]
                    csrc_ph = np.exp(2j*math.pi * (x10dl * psnk_list).sum(-1))
                    # phase correction: c2pt are psnk-projected wrt csrc0 with exp[-i*psnk*(x-csrc0)], 
                    #  so correction = exp[i*psnk*(csrc_i-csrc0)]
                    t_sh    = (lt + c[t_dir] - tsrc0) % lt
                    # correcting BC[t] applied in [Qlua]save_2pt_list
                    if tsrc0 <= c[t_dir] : bc_factor = 1.
                    else : bc_factor = 1. / had_bc_t
                    hs_res[i_data + i_c, i_tpol] = bc_factor * hs[..., t_sh : t_sh + hslab_tlen] * csrc_ph[:, None]

            csrc_list.extend(list(csrcgrp))
            i_data += len(csrcgrp)

        assert(n_data == i_data)

        meas_spec_list = np.array([ (dat_d['cfgkey'], c) for c in csrc_list], 
                            dtype=lhpd.h5_io.h5_meas_dtype)
        assert(len(meas_spec_list) == n_data)

        h5file, h5key  = get_lcdb_loc('c2', 
                dictnew(dat_d, data_dir=data_out, type='conv'), c2_d=c2_d1)
        lhpd.mkpath(os.path.dirname(h5file))
        h5f         = h5py.File(h5file, 'a')

        if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5f, [h5key])
        hs_shape = (n_data, n_tpol, n_psnk, hslab_tlen)
        h = h5f.require_dataset(h5key, hs_shape, hs_dtype, fletcher32=True)
        h[:] = hs_res

        # "axes"
        h.attrs['dim_spec']      = np.array(['i_data', 'i_tpol', 'i_psnk', 'i_t' ],
                                               dtype='S')
        # axes "ticks"
        lhpd.h5_io.h5_set_datalist(h, meas_spec_list)
        h.attrs['psnk_list']    = np.array(psnk_list, dtype=np.int32)
        h.attrs['tpol_list']    = np.array(tpol_list, dtype='S')
        h.attrs['t_list']       = np.r_[0 : hslab_tlen]

        for k, v in iteritems(attrs_kw) :
            h.attrs[k] = h5type_attr_fix_(v)

        h5f.flush()
        h5f.close()


    def conv2hdf_c3pt_qbarq(
                data_out, data_in,      # output, input
                dat_d,                  # cfgkey, ama
                c3_d,                   # had, smsrcsnk, smtag, tpol, psnk, tsep
                ins_d,                  # type, flav, qext_list, 
                csrcgrp_list,
                latsize, c3pt_tlen, had_bc_t, attrs_kw,
                t_dir=3, bb_dtype=np.complex128, h5_overwrite=True) :
        """
            csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
            
            save data to
            h5g[h5key][n_data, n_gamma, len(lpath_list), len(qext_list), tsep+1]
        """
        """ TEST 
        """
        print("WARNING : APPLYING BC_T FOR WRAP-AROUND BB")
        latsize     = np.asarray(latsize)
        ndim        = len(latsize)
        lt          = latsize[t_dir]
        assert(4 == ndim)
        assert(3 == t_dir)  # smart, eh?
        def get_spatial(x) : return np.asarray(x)[..., :t_dir]
        latsize_x   = get_spatial(latsize)
        csrcgrp_list= np.asarray(csrcgrp_list)
        n_grp, grp_size = csrcgrp_list.shape[0:2]
        n_data      = n_grp * grp_size
        assert(4 == csrcgrp_list.shape[-1])

        c3_d1       = dict(c3_d)
        psnk        = np.asarray(c3_d['psnk'])  # for phase correction if xyzsrc!=xyzsrc0
        tsep        = c3_d['tsep']
        tcut        = min(1+tsep, c3pt_tlen)    # number of available timeslices

        ins_d1, (qext_list,) = dict_copy_pop(ins_d, ['qext_list'])
        qext_list   = np.asarray(qext_list)
        n_qext      = len(qext_list)
        n_gamma     = 16

        # TODO generate lpath_list depending on ins_d['type']
        ins_type = ins_d['type']
        if   'bb'   == ins_type :
            lpath_list = lhpd.latcorr.make_linkpath_list_bb(
                    ins_d['lmin'], ins_d['lmax'])
        elif 'qpdf' == ins_type :
            lpath_list = lhpd.latcorr.make_linkpath_list_qpdf(
                    ins_d['ldir'], ins_d['lmin'], ins_d['lmax'])
        elif 'tmd'  == ins_type :
            lpath_list = lhpd.latcorr.make_linkpath_list_tmd(
                    ) # TODO implement
        else : raise ValueError(ins_type)
        n_lpath     = len(lpath_list)

        # some entries may be undef. if tcut < 1+tsep
        bb_res      = np.zeros((n_data, n_gamma, n_lpath, n_qext, 1+tsep), bb_dtype)

        n_csrc      = len(csrcgrp)
        csrc_list   = []
        i_data = 0
        for i_grp, csrcgrp in enumerate(csrcgrp_list) :
            n_src       = len(csrcgrp)
            print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
            csrc0       = csrcgrp[0]
            tsrc0       = csrc0[t_dir]
            
            bbfile, bbkpath = get_lcdb_loc('c3',
                    dictnew(dat_d, data_dir=data_in, type='aff', csrcgrp=csrcgrp),
                    c3_d=c3_d1, ins_d=ins_d1)
            aff_r   = aff.Reader(bbfile)

            bb      = lhpd.latcorr.aff_read_lpathkey_list(aff_r, bbkpath, 
                            range(n_gamma), lpath_list, qext_list)
            assert(bb.shape[-1] == n_csrc * c3pt_tlen)
            aff_r.close()

            for i_c, c in enumerate(csrcgrp) :
                # for initial source at x0, lattice data are
                #   bb(psnk, qext; x0) = \sum_{y,z} exp( -i*psnk.(y-x0) +i*qext.(z-x0))
                # with different x1 != x0, 
                #   bb(psnk, qext; x1) = \sum_{y,z} exp( -i*psnk.(y-x1) +i*qext.(z-x1))
                #           = bb(psnk, qext; x0) * exp( i*(psnk-qext).(x1-x0) )
                x10dl   = np.asarray(get_spatial(c - csrc0), np.float64) / latsize_x
                # [i_qext]
                csrc_ph = np.exp(2j*math.pi * (x10dl * (-qext_list)).sum(-1))       
                # sic! bkwsrc's are created with individual phases (sink mom proj wrt csrc_i)
                #  but insertion-momentum projected wrt csrc0, ~exp(i*q.(x-src0)), 
                #  so correction = exp(i*(-q)*(csrc_i - csrc0))
                #  did not do any difference for psnk=(0,0,0) used so far [2018/01/09]

                # XXX [Qlua] qcd.save_bb DOES NOT apply BC[t]
                # XXX calc_c23pt_cpbar_volcedm.qlua DOES NOT apply BC[t] to seqsrc
                # XXX therefore, NEED TO APPLY BC_FACTOR HERE
                if c[t_dir] + tsep < lt: bc_factor = 1
                else : bc_factor = had_bc_t
                #t_sh    = (lt + c[t_dir] - tsrc0) % lt     # XXX if "unchopped"
                t_sh    = i_c * c3pt_tlen
                # phase if csrc is shifted wrt csrc0

                bb_res[i_data,...,:tcut] = bc_factor * bb[..., t_sh : t_sh + tcut] * csrc_ph[:, None]
                csrc_list.append(c)
                i_data += 1

        assert(n_data == i_data)

        meas_spec_list = np.array([ (dat_d['cfgkey'], c) for c in csrc_list], 
                            dtype=lhpd.h5_io.h5_meas_dtype)
        assert(len(meas_spec_list) == n_data)

        h5file, h5key  = get_lcdb_loc('c3',
                dictnew(dat_d, data_dir=data_out, type='conv'), 
                c3_d=c3_d1, ins_d=ins_d1)
        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')

        if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5f, [h5key])
        bb_shape = (n_data, n_gamma, len(lpath_list), len(qext_list), tsep + 1)
        b = h5f.require_dataset(h5key, bb_shape, bb_dtype, fletcher32=True)
        b[:] = bb_res

        # "axes"
        b.attrs['dim_spec']      = np.array(['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau' ],
                                               dtype='S')
        # axes "ticks"
        lhpd.h5_io.h5_set_datalist(b, meas_spec_list)
        b.attrs['gamma_list']    = np.r_[0 : 16]
        b.attrs['linkpath_list'] = np.array(lpath_list, dtype='S')
        b.attrs['psnk']          = psnk
        b.attrs['qext_list']     = np.array(qext_list, dtype=np.int32)
        b.attrs['tau_list']      = np.r_[0 : tsep+1]
        # other parameters
        b.attrs['source_sink_dt']= int(tsep)

        for k, v in iteritems(attrs_kw) :
            b.attrs[k] = h5type_attr_fix_(v)

        h5f.flush()
        h5f.close()
