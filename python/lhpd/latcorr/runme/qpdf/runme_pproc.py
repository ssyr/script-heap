#!/usr/bin/env python 
from __future__ import print_function
from past.builtins import execfile
from future.utils import iteritems
import os, sys, re
import traceback
from lhpd import dictnew, dict_prod, dict_prod_auto
from attrdict import AttrDict as adict

execfile('pproc_qpdf.py')
def print_usage(prg) :
    print("""Usage:
%s  [--sj=i{=0..TOT-1}/TOT]  [--mpi=PMI] [--bin=<binsize>]
        [--data-in=<data_in>]  [--data-out=<data_out>] 
        <what>   <config>[,<config2>,...]  [<job-list>] ++ [<job_id0>] ...
What :
    conv_{c2pt,qpdf}
    binx_{c2pt,qpdf}
    merge_{c2pt,bb}
""" % (prg,))


print_err_excraise = {
    AssertionError          : 1,
    SyntaxError             : 1,
    NameError               : 1,
    NotImplementedError     : 1,
    IndentationError        : 1,
    SystemError             : 1,
    TypeError               : 1,
    UnboundLocalError       : 1,
    ValueError              : 1,
    ZeroDivisionError       : 1,
    OSError                 : 1,
    RuntimeError            : 1,
    KeyError                : 1,
    IndexError              : 1,
    AttributeError          : 1,
    KeyboardInterrupt       : 1,
    MemoryError             : 1,
    # tmp debug
    IOError                 : 1,
}

def print_err(what, i_sj, *clist) :
    #def strip_longopt(c) :
        #if isinstance(c, dict) :
            #c = dict(c)
            #purge_keys(c, [''])
        #else : return c
        
    #sys.stderr.write("%s[%d] %s {%s}\n" % (what, i_sj, ', '.join([ str(x) for x in clist] ))
    exc = sys.exc_info()
    sys.stderr.write("ERROR %s[%d]: %s\n" % (what, i_sj, repr(exc[1])))
    if 0 < print_err_excraise[exc[0]] : 
        traceback.print_tb(exc[2])
        raise

# TODO put to lib if not yet?
def strip_prefix(s, p):
    if s.startswith(p) : return s[len(p):]
    else : return None


if '__main__' == __name__ :

    argv = list(sys.argv)
    if len(argv) < 1 :
        print_usage('me')
        sys.exit(1)

    sj_this, sj_tot = 0, 1
    binsize     = 1

    opts = adict()
    while 1 < len(argv) and argv[1].startswith('--') :
        if argv[1].startswith('--sj=') :
            sj = re.match('^--sj=(\d+)/(\d+)$', argv[1])
            if not sj : 
                raise ValueError(argv[1])
            sj_this, sj_tot = [ int(x) for x in sj.groups() ]
            if (sj_this < 0 or sj_tot <= sj_this) :
                raise ValueError(("bad sj = %d/%d" % (sj_this, sj_tot)))
            print("sj = ", sj_this, sj_tot)

        elif argv[1].startswith('--data-in=') :
            opts.data_in = re.match('^--data-in=(.+)$', argv[1]).groups()[0]
        elif argv[1].startswith('--data=') :
            opts.data_in = re.match('^--data=(.+)$', argv[1]).groups()[0]
            opts.data_out= opts.data_in
        elif argv[1].startswith('--data-out=') :
            opts.data_out = re.match('^--data-out=(.+)$', argv[1]).groups()[0]

        elif argv[1].startswith('--bin=') :
            opts.binsize = int(re.match('^--bin=(.+)$', argv[1]).groups()[0])
            if opts.binsize <= 0 :
                raise ValueError(("bad binsize = %d" % opts.binsize))

        elif argv[1].startswith('--mpi=') :
            mpitype = re.match('^--mpi=(.+)$', argv[1]).groups()[0]
            if 'PMI' == mpitype :
                sj_this = int(os.environ['PMI_RANK'])
                sj_tot  = int(os.environ['PMI_SIZE'])
            else : raise ValueError(("bad mpi=%s" % mpitype))

            if (sj_this < 0 or sj_tot <= sj_this) :
                raise ValueError(("bad sj[PMI] = %d/%d" % (sj_this, sj_tot)))
            print("sj = ", sj_this, sj_tot)

        else :
            sys.stderr.write("bad arg='%s'\n" % argv[1])
            print_usage(argv[0] or 'me')
            sys.exit(1)
        argv.pop(1)

    if len(argv) < 3 :
        print_usage(argv[0] or 'me')
        sys.exit(1)

    try : isep = argv.index('++')
    except : isep = len(argv)
    #print("len(argv)=%d  isep=%d  argv=%s" % (len(argv), isep, str(argv)))

    if isep < 3 :
        print_usage(argv[0] or 'me')
        sys.exit(1)

    cfglist_cmd_ = argv[isep+1:]
    cfglist_file_ = argv[3:isep]
    #print("joblist=file(%s) + %s" % (str(cfglist_file_), str(cfglist_cmd_)))

    cfgkey_list = list(cfglist_cmd_)
    for f in cfglist_file_ :
        cfgkey_list.extend(lhpd.read_key_list(f, 'samples'))

    if len(cfgkey_list) <= 0 :
        sys.stderr.write('empty list; exit\n')
        sys.exit(1)

    print("Total samples: ", cfgkey_list_str(cfgkey_list))

    # TODO check for repeated keys

    conf_py   = argv[2]
    print("FIXME data_in, data_out overwritten by config")
    for cf in conf_py.split(',') :
        if 0 == len(cf) : continue
        try :
            print("load config '%s'" %(cf,))
            execfile(cf)
            print("#disco_ins_vi_spec_list =", #disco_ins_vi_spec_list)
        except : 
            print_err('load config', -1, (cf,))

    print("SET opt %s" % str(opts))
    for k, v in iteritems(opts) : globals()[k] = v
    print("data_in  = '%s'" % data_in)
    print("data_out = '%s'" % data_out)


def runme_conv_c2pt(
        cfgkey, c2_d,
        ama_list = ama_list, 
        attrs_kw={}) :

    """ convert CP-even 2pt functions 
    """
    had     = c2_d['had']
    bc_t    = get_hadron_bc(had, ferm_bc[t_axis])
    saved_bc_t= c2_d['saved_bc_t']
    print("runme_conv_c2pt: bc_t=%s, saved_bc_t=%s" % (str(bc_t), str(saved_bc_t)))
    for ama in ama_list :
        print("# %s %s %s %s %s " % (c, ama_str(ama), 
                had, c2_d['tpol_list'], srcsnk_str_f(c2_d)))
        latcorr_conv2hdf(
                dict(data_dir=data_out, cfgkey=cfgkey, ama=ama),        # dat_d
                dict(kind='c2', c2_d=c2_d),
                make_csrcgrp_list(ama, cfgkey),                         # csrcgrp_list
                latsize, t_axis, c2pt_hslab_len, bc_t, 
                dictnew(attrs_kw, source_hadron=had, sink_hadron=had),  # attrs_kw
                data_in=data_in, #data_dir_aff, 
                saved_bc_t=saved_bc_t,
                scale=c2_d.get('conv_scale'),
                VERBOSE=True) # attrs_kw

def runme_conv_c3pt_qbarq(
                cfgkey, c3_d, ins_d,
                ama_list = ama_list,
                attrs_kw={}) :
    had     = c3_d['had']
    bc_t    = get_hadron_bc(had, ferm_bc[t_axis])
    saved_bc_t= c3_d['saved_bc_t']
    print("runme_conv_c3pt_qbarq: bc_t=%s, saved_bc_t=%s" % (str(bc_t), str(saved_bc_t)))
    for ama in ama_list :
        print("# %s %s %s %s %s " % (c, ama_str(ama), 
                hadron_str(c3_d), srcsnk_str_f(c3_d), c3snk_str_f(c3_d)))
        latcorr_conv2hdf(
                dict(data_dir=data_out, cfgkey=cfgkey, ama=ama),
                dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
                make_csrcgrp_list(ama, cfgkey),
                latsize, t_axis, c3pt_tlen, bc_t,
                dictnew(attrs_kw, source_hadron=had, sink_hadron=had, 
                        tpol=c3_d['tpol'], time_neg=False, sink_mom=c3_d['psnk']),
                data_in=data_in, #data_dir_aff,
                saved_bc_t=saved_bc_t,
                scale=c3_d.get('conv_scale'),
                VERBOSE=True)



##############################################################################
##############################################################################
if '__main__' == __name__ :
    what   = argv[1]
    jobid_list      = cfgkey_list

    def  subjob_iter(j_iter) :
        j_list  = list(j_iter)
        nj      = len(j_list)
        jj0     = ( sj_this    * nj) // sj_tot
        jj1     = ((sj_this+1) * nj) // sj_tot
        for j in j_list[jj0:jj1] : yield j

    if   'conv_c2pt' == what :
        for i_sj, (c, c2_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, c2pt_spec_list))) :
            runme_conv_c2pt(c, c2_d, ama_list=ama_list)
            #try : runme_conv_c2pt(c, c2_d, ama_list=ama_list)
            #except : print_err(what, i_sj, (c, c2_d))

    elif 'binx_c2pt' == what :
        for i_sj, c2_d in enumerate(subjob_iter(c2pt_spec_list)) :
            try : 
                latcorr_bin_unbias(cfgkey_list, ama_list, 
                    dict(data_dir=data_out),
                    dict(kind='c2', c2_d=c2_d),
                    data_in=data_in,
                    binsize=binsize,
                    do_ama=True,
                    VERBOSE=True)
            except : print_err(what, i_sj, (c2_d,))

    elif 'merge_c2pt' == what :
        for i_sj, (c2_d,) in enumerate(subjob_iter(
                it.product(c2pt_spec_list))) :
            for ama in ama_list :
                try :
                    latcorr_merge(cfgkey_list, ama_list, 
                        dict(data_dir=data_out, ama=ama),
                        dict(kind='c2', c2_d=c2_d),
                            VERBOSE=True)
                except : print("EXCEPT", sys.exc_info()[0], ama, c2_d)

    elif 'conv_qpdf' == what :
        for i_sj, (c, c3_d, ins_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, c3pt_spec_list, qpdf_spec_list))) :
            runme_conv_c3pt_qbarq(c, c3_d, ins_d, ama_list=ama_list)
            #try : runme_conv_c3pt_qbarq(c, c3_d, ins_d, ama_list=ama_list)
            #except : print("EXCEPT", sys.exc_info()[0], c, c3_d, ins_d)
    
    elif 'binx_qpdf' == what :
        for i_sj, (c3_d, ins_d) in enumerate(subjob_iter(it.product(
                c3pt_spec_list, qpdf_spec_list))) :
            try :
                latcorr_bin_unbias(cfgkey_list, ama_list, 
                    dict(data_dir=data_out),
                    dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
                    data_in=data_in,
                    binsize=binsize,
                    do_ama=True,
                    VERBOSE=True)
            except : print_err(what, i_sj, (c3_d, ins_d))

    # FIXME merge is incorrect in merge_c2pt or merge_op
    elif 'merge_qpdf' == what :
        for i_sj, (c3_d, ins_d) in enumerate(subjob_iter(it.product(
                c3pt_spec_list, qpdf_spec_list))) :
            for ama in ama_list :
                try :
                    latcorr_merge(cfgkey_list,
                        dict(data_dir=data_out, ama=ama),
                        dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
                            VERBOSE=True)
                except : print("EXCEPT", sys.exc_info()[0], ama, c3_d, ins_d)

    else :
        raise ValueError(what)
