from __future__ import print_function

import math
import numpy as np
import lhpd


class srcsnkpair :
    def __init__(self, tsep, smtag_src, smboost_src, smtag_snk, smboost_snk) :
        self.tsep           = tsep
        self.smtag_src      = smtag_src
        self.smboost_src    = np.asarray(smboost_src)
        self.smtag_snk      = smtag_snk
        self.smboost_snk    = np.asarray(smboost_snk)

    def __hash__(self) : 
        return (hash(self.tsep) 
                ^ hash(self.smtag_src) 
                ^ hash(self.smtag_snk))
    def __eq__(self, oth) :
        return (self.tsep == oth.tsep 
                and self.smtag_src      == oth.smtag_src
                and (self.smboost_src   == oth.smboost_src).all()
                and self.smtag_snk      == oth.smtag_snk
                and (self.smboost_snk   == oth.smboost_snk).all())
    def __ne__(self, oth) :
        return not self.__eq__(oth)
    # since srcsnkpair has tsep, using it as key for c2pt map will result in 
    # 2pt functions stored len(tsep_list) times; fix: add special value specifically 
    # for that; when building c2pt map, check if it has already been inserted
    def key2pt(self) : 
        return srcsnkpair(0, self.smtag_src, self.smboost_src, self.smtag_snk, self.smboost_snk)
    def strkey(self) :
        return "%s_%s_dt%d" % (self.get_smtag_src(), self.get_smtag_snk(), self.get_tsep())
    def __str__(self) :
        return '<%s|%s>(T=%d)' % (self.smtag_snk, self.smtag_src, self.tsep)
    def __repr__(self): return self.__str__() 
    # TODO add str repr
    def get_smtag_src(self)     : return self.smtag_src
    def get_smboost_src(self)   : return self.smboost_src
    def get_smtag_snk(self)     : return self.smtag_snk
    def get_smtag(self)         : return '%s_%s' % (self.smtag_src, self.smtag_snk)
    def get_smtag_symm(self)    : return '%s_%s' % (self.smtag_src, self.smtag_src)
    def get_smboost_snk(self)   : return self.smboost_snk
    def get_tsep(self)      : return self.tsep

class mc_case :

    def __init__(self, p3src, p3snk, tpol, tpol_c2=None) :
        self.p3src      = p3src
        self.p3snk      = p3snk
        self.tpol       = tpol
        self.tpol_c2    = tpol_c2 or tpol
    def __hash__(self) :
        return (hash(str(self.tpol))
                ^ hash(str(self.tpol_c2))
                ^ hash(tuple(self.p3src)) 
                ^ hash(tuple(self.p3snk)))
    def __eq__(self, oth) :
        return (self.tpol == oth.tpol and self.tpol_c2 == oth.tpol_c2
                and tuple(self.p3src) == tuple(oth.p3src)
                and tuple(self.p3snk) == tuple(oth.p3snk))
    def __ne__(self, oth) :
        return not self.__eq__(oth)
    def __str__(self) :
        return '%s<%s|%s>' % (self.tpol, self.p3snk, self.p3src)
    def __repr__(self): return self.__str__() 
    def get_p3src(self)     : return self.p3src
    def get_p3snk(self)     : return self.p3snk
    def get_tpol(self)      : return self.tpol
    def get_tpol_matr(self) : return lhpd.latcorr.get_tpol_matr(self.tpol)
    def get_tpol_c2(self)   : return self.tpol_c2
    def get_tpol_c2_matr(self): return lhpd.latcorr.get_tpol_matr(self.tpol_c2)


# FIXME data_top is not used
class ens_data_get :
    """ 
        ss  = ( tsep[int], ksrc_tag[str], ksnk_tag[str], smtag[str] )
        mc  = ( p3src[tuple?], p3snk[tuple?] }
    """
#    kbxp10  = np.r_[  1,  0,  0,  0]
#    kbxm10  = np.r_[ -1,  0,  0,  0]
#    kbyp10  = np.r_[  0,  1,  0,  0]
#    kbym10  = np.r_[  0, -1,  0,  0]
#    kbzp10  = np.r_[  0,  0,  1,  0]
#    kbzm10  = np.r_[  0,  0, -1,  0]
#    kbtp10  = np.r_[  0,  0,  0,  1]
#    kbtm10  = np.r_[  0,  0,  0, -1]
#
#    kbqp10  = np.r_[  1,  1,  0,  0]
#    kbqm10  = np.r_[ -1, -1,  0,  0]
#    kbrp10  = np.r_[ -1,  1,  0,  0]
#    kbrm10  = np.r_[  1, -1,  0,  0]
#
#    kmom_map= {
#        'bqp10' : np.array(kbqp10[:3]),
#        'bqm10' : np.array(kbqm10[:3]),
#        'brm10' : np.array(kbrm10[:3]),
#        'bxp10' : np.array(kbxp10[:3]),
#        'bxm10' : np.array(kbxm10[:3]),
#        'bym10' : np.array(kbym10[:3]),
#        'bzm10' : np.array(kbzm10[:3]),
#    }
    def __init__(self, **kw) :
        self.ama        = kw["ama"]
        self.had        = kw["had"]
        self.flav       = kw["flav"]
        self.gf_tag     = kw["gf_tag"]
        self.data_top   = kw["data_top"]
        self.mlat       = kw.get('mlat')
        self.latsize    = kw.get('latsize', None)
        self.c3pt_contr = kw.get('c3pt_contr')
        self.c3pt_improv= kw.get('c3pt_improv')
        #self.c3pt_improv_coeff = kw.get('c3pt_improv_coeff') or {}
        #self.c2fit_file = kw.get("c2fit_file")
        #self.c2fit_dgrp = kw.get("c2fit_dgrp")
        self.VERBOSE    = kw.get('VERBOSE', 0)
        self.log        = kw.get('log')
        if None is self.log : self.log = lhpd.log()

    def get_threept_conn_flav_(self, smtag, tsep, p3src, p3snk, tpol, gamma, lp, flav, gf_tag) :

        p3src   = np.asarray(p3src)
        p3snk   = np.asarray(p3snk)
        qext    = p3snk - p3src
        
        def get_conn_flav(flav) :
            dkind = 'bin'
            if self.ama == 'ub' : dkind = 'ub'
            dat_d = dict(data_dir=self.data_top, kind=dkind)
            c3_d  = dict(had=self.had, tpol=tpol, 
                         psnk=p3snk, tsep=tsep, smsrcsnk='SS', smtag=smtag)
            ins_d = dict(kind='qpdf', flav=flav, gf_tag=gf_tag)

            h5fname, h5kpath = get_lcdb_loc(dat_d, dict(kind='c3', c3_d=c3_d, ins_d=ins_d))
            #print("%s[%s]" % (h5fname, h5kpath))
            h5f     = h5py.File(h5fname, 'r')
            h5d     = h5f[h5kpath]
            i_qext  = lhpd.np_find_first(qext, h5d.attrs['qext_list'])[0]
            i_lp    = lhpd.np_find_first(lp, h5d.attrs['linkpath_list'])[0]
            i_gamma = lhpd.np_find_first(gamma, h5d.attrs['gamma_list'])[0] # TODO make arbitrary gamma
            res     = h5d[:, i_gamma, i_lp, i_qext]

            if 3 <= self.VERBOSE :
                self.log.write("get_threept_conn_flav_: %s[%s]" % (h5fname, h5kpath))
                self.log.write("get_threept_conn_flav_: p3src=%s  p3snk=%s  qext_list[%d]=%s" % ( 
                            p3src, p3snk, i_qext, h5d.attrs['qext_list'][i_qext]))

            h5f.close()
            return res
            
        return lhpd.latcorr.calc_flav_fromUD(flav, get_conn_flav)

    if False :
        #def get_threept_disc_flav_(self, smtag, tsep, p3src, p3snk, tpol, op, ir_name, flav) :
        def get_threept_disc_flav_(self, smtag, tsep, p3src, p3snk, tpol, gamma, lp, flav, gf_tag) : 
        # FIXME op,irname->gamma,lp
            p3src   = np.asarray(p3src)
            p3snk   = np.asarray(p3snk)
            qext    = p3snk - p3src

            def get_disc_flav(flav) :
                dkind   = 'bin'
                if self.ama == 'ub' : dkind = 'ub'
                dat_d   = dict(data_dir=self.data_top, kind=dkind)
                if   'U' == flav or 'D' == flav :
                    flav1   = 'discUD'
                elif 'S' == flav :
                    flav1   = 'discS'
                c3_d    = dict(had=self.had, tpol=tpol, 
                             psnk=p3snk, tsep=tsep, smsrcsnk='SS', smtag=smtag)
                ins_d = dict(kind='op', op=op, flav=flav1, ir=ir_name)

                h5fname, h5kpath = get_lcdb_loc(dat_d, dict(kind='c3', c3_d=c3_d, ins_d=ins_d))
                #print("%s[%s]" % (h5fname, h5kpath))
                h5f = h5py.File(h5fname, 'r')
                h5d = h5f[h5kpath]
                i_qext  = lhpd.np_find_first(qext, h5d.attrs['qext_list'])[0]
                res = h5d[:, :, i_qext]

                if 3 <= self.VERBOSE :
                    self.log.write("get_threept_disc_flav_: %s[%s]" % (h5fname, h5kpath))
                    self.log.write("get_threept_disc_flav_: p3src=%s  p3snk=%s  qext_list[%d]=%s" % ( 
                                p3src, p3snk, i_qext, h5d.attrs['qext_list'][i_qext]))

                h5f.close()
                return res
                
            return lhpd.latcorr.compose_flav(flav, get_disc_flav)

        #def get_threept_1imp_flav_(self, smtag, tsep, p3src, p3snk, tpol, op, ir_name, flav, c3pt_contr) :
        def get_threept_1imp_flav_(self, smtag, tsep, p3src, p3snk, tpol, gamma, lp, flav, gf_tag, c3pt_contr) : 
            # FIXME op,irname->gamma,lp
            p4src   = lhpd.latcorr.lorentz_p4(self.mlat, self.latsize, p3src) * [1,1,1,1j]
            p4snk   = lhpd.latcorr.lorentz_p4(self.mlat, self.latsize, p3snk) * [1,1,1,1j]
            q4ext   = p4snk - p4src
            #print(repr(q4ext))
            res     = None
            if 'tensor1' == op :
                sig_comp= self.get_threept_flav_(flav, smtag, tsep,
                            'sigma2a2', 'H4_T2_d6r1', p3src, p3snk, tpol, c3pt_contr)

                xy,xz,xt,yz,yt,zt = tuple([ sig_comp[:,i] for i in range(6) ])
                zz = np.zeros_like(xy)
                n_data  = xy.shape[0]
                sig_munu= [ [ zz, xy, xz, xt ], 
                            [-xy, zz, yz, yt ],
                            [-xz,-yz, zz, zt ],
                            [-xt,-yt,-zt, zz ] ]
                res     = np.zeros((n_data,4) + xy.shape[1:], np.complex128)
                for mu in range(4) :
                    #for nu in range(4) :
                    #    res[:,mu] += q4ext[nu]*sig_munu[mu][nu]
                    for nu in range(3) :
                        res[:,mu] += q4ext[nu]*sig_munu[mu][nu]
                    def time_deriv(a) : return (a[..., 2:] - a[..., :-2])
                    res[:,mu,...,1:-1] += 1j * time_deriv(sig_munu[mu][3])

                return self.c3pt_improv_coeff[op] * res
            else : raise NotImplementedError(op)


    #def get_threept_flav_(self, smtag, tsep, p3src, p3snk, tpol, op, ir_name, flav, c3pt_contr) :
    def get_threept_flav_(self, smtag, tsep, p3src, p3snk, tpol, gamma, lp, flav, gf_tag, c3pt_contr) :
        if 'conn' == c3pt_contr : 
            return self.get_threept_conn_flav_(smtag, tsep, p3src, p3snk, tpol, gamma, lp, flav, gf_tag)
        elif 'disc' == c3pt_contr :
            return self.get_threept_disc_flav_(smtag, tsep, p3src, p3snk, tpol, gamma, lp, flav, gf_tag)
        elif 'full' == c3pt_contr :
            raise ValueError("verify math: flavor-dependent coefficients?")
            return (  self.get_threept_conn_flav_(smtag, tsep, p3src, p3snk, tpol, gamma, lp, flav, gf_tag)
                    + self.get_threept_conn_flav_(smtag, tsep, p3src, p3snk, tpol, gamma, lp, flav, gf_tag) )
        else : raise ValueError(c3pt_contr)



    def get_threept(self, mc, ss, lp, gamma) :
        if 3 <= self.VERBOSE : 
            self.log.write("get_threept: mc=%s  ss=%s" % (mc, ss))
        if   '0imp' == self.c3pt_improv : 
            return self.get_threept_flav_(
                        ss.get_smtag(), ss.tsep, mc.p3src, mc.p3snk, mc.tpol, 
                        lp, gamma, self.flav, self.gf_tag, self.c3pt_contr)
        elif '1imp' == self.c3pt_improv :
            return self.get_threept_1imp_flav_(
                        ss.get_smtag(), ss.tsep, mc.p3src, mc.p3snk, mc.tpol, 
                        lp, gamma, self.flav, self.gf_tag, self.c3pt_contr)
        elif '01imp' == self.c3pt_improv :
            # TODO need to initialize self.c3pt_improv_coeff[op] map
            return (  self.get_threept_flav_(
                        ss.get_smtag(), ss.tsep, mc.p3src, mc.p3snk, mc.tpol, 
                        lp, gamma, self.flav, self.gf_tag, self.c3pt_contr)
                    + self.get_threept_1imp_flav_(
                        ss.get_smtag(), ss.tsep, mc.p3src, mc.p3snk, mc.tpol, 
                        lp, gamma, self.flav, self.gf_tag, self.c3pt_contr) )
        else : raise ValueError(self.c3pt_improv)



    def filter_c2symm_momlist(self, mom_list, psq, k, pk, atol=1e-5) :
        """ 
            mom_list[i_mom, k] = list of 3-component momenta in units of 2pi/L
            k = boost momentum
            pk = (p.k)
        """
        mom_list    = np.asarray(mom_list)
        assert(len(mom_list.shape) == 2 and mom_list.shape[1] == 3)
        k           = np.asarray(k)
        assert(k.shape == (3,))
        momsq_list  = (mom_list**2).sum(-1)
        kmom_list   = (mom_list * k).sum(-1)
        return np.where((np.abs(momsq_list - psq) < atol) * (np.abs(kmom_list - pk) < atol))[0]

    # get momenta appropriate for getting symmetrically boost-smeared c2pt
    def get_c2symm_p3src(self, mc, ss) : 
        """ trivial """
        return mc.get_p3src()

    def get_c2symm_p3snk(self, mc, ss) :
        """ OLD COMMENTS:
            return p3snk that should be used to get c2, 
            since with boosted smearing, not all directions of p3snk are available in c2 
            NEW COMMENTS:
            need to find psnkP(rime) such that |psnkP|==|psnk| && psnkP.bsrc_q == psnk.bsnk_q
            XXX what about c2pt spin projection (c2_d.tpol)?

        """
        bsrc_q  = ss.get_smboost_src()  #self.kmom_map[ss.ksrc_tag]        # src boosting mom
        bsrc_n  = math.sqrt((bsrc_q**2).sum())      # ... and its norm
        bsnk_q  = ss.get_smboost_snk()  #self.kmom_map[ss.ksnk_tag]        # snk boosting mom
        bsnk_n  = math.sqrt((bsnk_q**2).sum())      # ... and its norm
        #ipdb.set_trace()
        p       = np.asarray(mc.p3snk)
        # FIXME currently make vector (bsnk.psnk)*bsrc / (|bsnk| * |bsrc|)
        # will fail if psnk is not aligned with bsnk? 
        # figure out symmetry transf. between bsnk and bsrc, apply to psnk
        if 0 < bsrc_n and 0 < bsrc_n : 
            p3snkP  = (p * bsnk_q).sum() * bsrc_q / bsrc_n / bsnk_n
            p3snkP  = np.array(p3snkP + .5, int)     # round to closest: (int)p_i==floor(p_i)
            assert ((p3snkP**2).sum() == (p**2).sum())
            assert ((p3snkP * bsrc_q).sum() == (p * bsnk_q).sum())
        else : 
            p3snkP = p
        # only on-axis for now
        #print("# snkrel=(%s-%s)=%s -> srcrel=(%s-%s)=%s" % (
            #mc.p3snk, 3*bsnk_q, mc.p3snk - 3*bsnk_q,
            #p3snk, 3*bsrc_q, p3snk - 3*bsrc_q))
        return p3snkP

    # TODO average over all momenta p1 such that |p1|==|p3| and k.p1 == k.p3
    def get_twopt_symm_mom_tpol_(self, smtag, p3, tpol) :
        dkind = 'bin'
        if self.ama == 'ub' : dkind = 'ub'
        dat_d = dict(data_dir=self.data_top, kind=dkind)
        c2_d  = dict(had=self.had, smsrcsnk='SS', smtag=smtag)

        h5fname, h5kpath = get_lcdb_loc(dat_d, dict(kind='c2', c2_d=c2_d))
        #print("%s[%s]" % (h5fname, h5kpath))
        h5f = h5py.File(h5fname, 'r')
        h5d = h5f[h5kpath]

        p3snk = np.asarray(p3)
        i_p = lhpd.np_find_first(p3snk, h5d.attrs['psnk_list'])[0]

        g_list = []
        def get_Tg(g) : 
            g_list.append(g)
            return h5d[:, g, i_p]

        res = lhpd.latcorr.calc_tpol_fromGamma(tpol, get_Tg)

        if 3 <= self.VERBOSE :
            self.log.write("get_twopt_symm_mom_tpol_: %s[%s]" % (h5fname, h5kpath))
            self.log.write("get_twopt_symm_mom_tpol_: psnk=%s  psnk_list[%d]=%s" % (
                        p3, i_p, h5d.attrs['psnk_list'][i_p]))
            self.log.write("get_twopt_symm_mom_tpol_: read_tpol=%s" % (g_list))

        h5f.close()
        return res

    def get_twopt_src(self, mc, ss) :
        if 3 <= self.VERBOSE : 
            self.log.write("get_twopt_src: mc=%s  ss=%s" % (mc, ss))
        smtag = ss.get_smtag_symm()
        return self.get_twopt_symm_mom_tpol_(smtag,
                    self.get_c2symm_p3src(mc, ss), mc.get_tpol_c2())
    def get_twopt_snk(self, mc, ss) :
        if 3 <= self.VERBOSE : 
            self.log.write("get_twopt_snk: mc=%s  ss=%s" % (mc, ss))
        smtag = ss.get_smtag_symm()
        return self.get_twopt_symm_mom_tpol_(smtag,
                    self.get_c2symm_p3snk(mc, ss), mc.get_tpol_c2())

    def get_twopt_fit_mom_tpol_(self, p3, tpol) :
        p3      = np.asarray(p3)
        snk_str = "px%dpy%dpz%d" % tuple(p3)
        #h5f     = h5py.File(self.c2fit_file, 'r')
        #h5kpath = "/%s_%s/%s" % (self.had, tpol, snk_str)
        #h5d     = h5f[h5kpath]
        h5d     = self.c2fit_dgrp[snk_str]
        assert('c2pt_c0' == h5d.attrs['pname'][0])
        assert('c2pt_e0' == h5d.attrs['pname'][1])
        assert('c2pt_c1' == h5d.attrs['pname'][2])
        assert('c2pt_de1_0' == h5d.attrs['pname'][3])
        res     = h5d['p']
        #h5f.close()
        if 3 <= self.VERBOSE : 
            self.log.write("get_twopt_fit_mom_tpol_: %s[%s]" % (
                    self.c2fit_dgrp.file.filename, self.c2fit_dgrp.name))
            self.log.write("get_twopt_fit_mom_tpol_: p3=%s  %s  pol=%s" % (p3, snk_str, tpol))
        return res

    def get_twopt_fit_src(self, mc, ss) :
        if 3 <= self.VERBOSE : 
            self.log.write("get_twopt_fit_src: mc=%s  ss=%s" % (mc, ss))
        return self.get_twopt_fit_mom_tpol_(self.get_c2symm_p3src(mc, ss), mc.get_tpol_c2()) 
    def get_twopt_fit_snk(self, mc, ss) :
        if 3 <= self.VERBOSE : 
            self.log.write("get_twopt_fit_snk: mc=%s  ss=%s" % (mc, ss))
        return self.get_twopt_fit_mom_tpol_(self.get_c2symm_p3snk(mc, ss), mc.get_tpol_c2()) 

    def str_f(self) :
        return '%s.%s.%s.%s' % (
            self.ama, self.had, self.flav, self.op)
 
