# XXX common functions for processing c2,c3,c4
# XXX better understanding of layers (aff,h5,lcdb) and their interaction is required

from future.utils import iteritems
#from lhpd.misc import np_find_first, np_match, 
import numpy as np
import h5py
from lhpd.misc import purge_keys, dict_copy_pop, dictnew, dictnewd
from lhpd.h5_io import h5type_attr_fix_
from lhpd.aff_io import *


c2_dim_spec     = np.array(['i_data', 'i_tpol', 'i_psnk', 'i_t' ],
                            dtype='S')
c3_bb_dim_spec  = np.array(['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau' ],
                            dtype='S')
c3_op_dim_spec  = np.array(['i_data', 'i_comp', 'i_qext', 'i_tau' ], 
                            dtype='S32')

# temporary format used in c2*qloop->c3 contractions
c3x_op_dim_spec = np.array(['i_data', 'i_tpol', 'i_psnk', 'i_tsep', 'i_comp', 'i_qext', 'i_tau' ], 
                            dtype='S32')

# contractions c2*scalar(e.g., theta)
# XXX not used currently: add to runme/ff/nedm/runme_pproc.py:runme_binx_c3ts_theta
c3ts_op_dim_spec = np.array(['i_data', 'i_tpol', 'i_psnk', 'i_tsep', 'i_tau' ], 
                            dtype='S32')

def is_baryon(had) :
    if   (   had.startswith('proton') 
          or had.startswith('nucleon') ) : 
        return True     # TODO other baryons
    elif (   had.startswith('pion')   
          or had.startswith('meson') ) : 
        return False    # TODO other mesons
    else : raise ValueError(had)
def is_meson(had) :
    if   (   had.startswith('proton') 
          or had.startswith('nucleon') ) : 
        return False    # TODO other baryons
    elif (   had.startswith('pion')   
          or had.startswith('meson') ) : 
        return True     # TODO other mesons
    else : raise ValueError(had)
def get_hadron_bc(had, bc_q) :
    if   is_baryon(had)     : return bc_q**3
    elif is_meson(had)      : return bc_q**2
    else : raise ValueError(had)

def cfgkey_list_str(cfgkey_list) :
    return "%d ('%s' ... '%s')" % (len(cfgkey_list), cfgkey_list[0], cfgkey_list[-1])

def vacins_str_desc(vi_d) : 
    vi_kind = vi_d['kind']
    vi_str = None
    if   'theta' == vi_kind : 
        rcut_s, dtcut_s = '', ''
        if vi_d.get('rcut') : rcut_s  = '.rcut%d'  % vi_d['rcut']
        if vi_d.get('dtcut'): dtcut_s = '.dtcut%d' % vi_d['dtcut']
        vi_str = '%s%s%s' % (vi_d['gf'], rcut_s, dtcut_s)
    elif 'volcedm'  == vi_kind : 
        vi_str = '%s.%s' % (vi_d['gf'], vi_d['flav'])
    elif 'volpsc' == vi_kind : 
        vi_str = '%s' % (vi_d['flav'],)
    else : raise ValueError(vi_d)
    return '%s.%s' % (vi_kind, vi_str)

def ins_str_desc(ins_d) :
    kind    = ins_d['kind']
    if 'op' == kind :
        return '%s.%s' % (ins_d['op'], ins_d['flav'])
    return 'ins'

def latcorr_str(lc_d): 
    kind    = lc_d['kind']
    if   kind in ['c2', 'c2h', 'c2lec'] :
        c2_d    = lc_d['c2_d']
        tpol_str= c2_d.get('tpol') or str(c2_d.get('tpol_list'))
        return "%s %s %s" % (c2_d['had'], tpol_str, srcsnk_str_f(c2_d))
    if   'c2vi'   == kind :
        c2_d    = lc_d['c2_d']
        tpol_str= c2_d.get('tpol') or str(c2_d.get('tpol_list'))
        vi_d    = lc_d['vi_d']
        #return "%s %s %s %s" % (c2_d['had'], tpol_str, srcsnk_str_f(c2_d), vacins_str_f(vi_d))
        return "%s %s %s %s" % (c2_d['had'], tpol_str, srcsnk_str_f(c2_d), vacins_str_desc(vi_d))
    elif 'c3'   == kind : 
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        return "%s %s %s" % (hadron_str(c3_d), srcsnk_str_f(c3_d), c3snk_str_f(c3_d))
    elif 'c3x'   == kind : 
        c3x_d   = lc_d['c3x_d']
        ins_d   = lc_d['ins_d']
        return "%s %s %s" % (c3x_d['had'], srcsnk_str_f(c3x_d), ins_str_desc(ins_d))
    elif 'c3vi' == kind :
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        vi_d    = lc_d['vi_d']
        return "%s %s %s %s" % (hadron_str(c3_d), srcsnk_str_f(c3_d), c3snk_str_f(c3_d), vacins_str_desc(vi_d))
    elif 'c3ts' == kind : 
        c2_d    = lc_d['c2_d']
        tpol_str= c2_d.get('tpol') or str(c2_d.get('tpol_list'))
        vi_d    = lc_d['vi_d']
        return "%s %s %s %s" % (c2_d['had'], tpol_str, srcsnk_str_f(c2_d), vacins_str_desc(vi_d))
    else: raise ValueError(kind)

def datd_str(dat_d) :
    kind_str    = dat_d.get('kind', '')
    cfgkey_str  = dat_d.get('cfgkey', '')
    ama_str     = dat_d.get('ama', '')
    csrc_str = ""
    if 'csrc' in dat_d : csrc_str = csrc_str_f(dat_d['csrc'])
    elif 'csrcgrp' in dat_d : csrc_str = csrcgrp_str_f(dat_d['csrcgrp'])
    return ','.join([ s for s in [kind_str, cfgkey_str, ama_str, csrc_str] if s != ''])

def conv2hdf_c2pt_aff_chop(h5f, h5key, fk_in_func,
            cfgkey, csrcgrp_list, psnk_list, tpol_list,
            latsize, t_dir, corr_tlen, bc_t, attrs_kw,
            tmax=None,
            saved_bc_t=None,        # if different from bc_t
            scale=None,             # convert
            dtype=np.complex128, 
            VERBOSE=False) :
    """ looping over "coherent" 2pt : compatible with non-coherent
        csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
        
        save data to
        h5g[h5key][n_data, len(psnk_list), corr_tlen]

        XXX should also work for hspec_volcedm

        TODO check that hslabs do not overlap
    """
    if None is saved_bc_t : saved_bc_t = bc_t
    if None is scale : scale = 1
    latsize     = np.asarray(latsize)
    ndim        = len(latsize)
    assert(4 == ndim)
    assert(3 == t_dir)  # smart, eh?
    def get_spatial(x) : return np.asarray(x)[..., :t_dir]
    latsize_x   = get_spatial(latsize)
    lt          = latsize[t_dir]
    csrcgrp_list= np.asarray(csrcgrp_list)
    n_grp, grp_size = csrcgrp_list.shape[0:2]
    n_data      = n_grp * grp_size
    assert(4 == csrcgrp_list.shape[-1])

    # [i_psnk, mu]
    psnk_list   = np.asarray(psnk_list)
    n_psnk      = len(psnk_list)
    n_tpol      = len(tpol_list)
    
    csrc_list   = []
    if None is tmax or corr_tlen < tmax : tmax = corr_tlen
    hs_res      = np.empty((n_data, n_tpol, n_psnk, tmax), dtype)
    hs_res[:]   = np.nan        # fill to catch uninitialized entries

    i_data = 0
    for i_grp, csrcgrp in enumerate(csrcgrp_list) :
        n_src       = len(csrcgrp)
        if VERBOSE : 
            print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
        csrc0       = csrcgrp[0]
        tsrc0       = csrc0[t_dir]
        
        for i_tpol, tpol in enumerate(tpol_list) :
            
            hsfile, hskpath = fk_in_func(csrcgrp, tpol)
            aff_r   = aff_open_reader_protected(hsfile)
            hs      = lhpd.latcorr.aff_read_hadspec_list(aff_r, hskpath, psnk_list)
            assert(hs.shape[-1] == corr_tlen * grp_size) # sanity check
            aff_r.close()

            for i_c, c in enumerate(csrcgrp) :
                # [mu]
                x10dl   = np.asarray(get_spatial(c - csrc0), np.float64) / latsize_x
                # [i_qext]
                csrc_ph = np.exp(2j*math.pi * (x10dl * psnk_list).sum(-1))
                # phase correction: c2pt are psnk-projected wrt csrc0 with exp[-i*psnk*(x-csrc0)], 
                #  so correction = exp[i*psnk*(csrc_i-csrc0)]
                assert((lt + c[t_dir] - tsrc0) % lt == i_c * corr_tlen) # sanity check: assuming no chopping for c2pt
                t_sh    = i_c * corr_tlen
                

                fix_saved_bc_t  = 1.
                # XXX the factor (1/saved_bc_t) is applied AT MOST once:
                # XXX this happens only if ncoh>1 and i_c>0
                if c[t_dir] < tsrc0 : fix_saved_bc_t = 1. / saved_bc_t  # if bc_t was applied to sample with wraparound tsrc,tsnk
                hs_res[i_data + i_c, i_tpol] = fix_saved_bc_t * hs[..., t_sh : t_sh + tmax] * csrc_ph[:, None]

                # apply correct BC to wraparound part of the correlator
                t_wrap          = (lt - c[t_dir]) % lt
                # XXX this happens only if c[t_dir]>=tsrc0, otherwise it would overlap with [tsrc0:tsrc0+corr_len)
                if t_wrap < corr_tlen : 
                    bc_factor   = bc_t / saved_bc_t
                    hs_res[i_data + i_c, i_tpol, ..., t_wrap:] *= bc_factor
                    

        csrc_list.extend(list(csrcgrp))
        i_data += len(csrcgrp)

    assert(n_data == i_data)                # sanity check
    assert(len(csrc_list) == n_data)       # sanity check
    meas_spec_list = np.array([ (cfgkey, c) for c in csrc_list], 
                        dtype=lhpd.h5_io.h5_meas_dtype)

    hs_shape = (n_data, n_tpol, n_psnk, tmax)
    h = h5f.require_dataset(h5key, hs_shape, dtype, fletcher32=True)
    h[:] = scale * hs_res

    # "axes"
    h.attrs['dim_spec']      = c2_dim_spec
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(h, meas_spec_list)
    h.attrs['psnk_list']    = np.array(psnk_list, dtype=np.int32)
    h.attrs['tpol_list']    = np.array(tpol_list, dtype='S')
    h.attrs['t_list']       = np.r_[0 : tmax]
    h.attrs['latsize']      = np.array(latsize)
    h.attrs['t_dir']        = t_dir

    for k, v in iteritems(attrs_kw) :
        h.attrs[k] = h5type_attr_fix_(v)

    h5f.flush()


def conv2hdf_qbarq_aff_chop(h5f, h5key, fk_in_func,
            cfgkey, csrcgrp_list, psnk, tsep, 
            gamma_list, lpath_list, qext_list,
            latsize, t_dir, corr_tlen, bc_t, attrs_kw,
            saved_bc_t=None,         # if different from bc_t
            scale=None,
            dtype=np.complex128,
            VERBOSE=False) :
    """ convert list of qbarq AFF->HDF5 (c3,c4,etc  with  bb,qpdf,tmd,etc)
        csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
        
        save data to
        h5g[h5key][n_data, n_gamma, len(lpath_list), len(qext_list), tsep+1]
    """
    """ TEST 
    """
    print("WARNING : APPLYING BC_T FOR WRAP-AROUND BB")
    if None is saved_bc_t : saved_bc_t = 1
    if None is scale : scale = 1
    else : assert(1 == saved_bc_t) # sic! not implemented for other values; perhaps all correlators should be saved with bc_t=1 and BC taken into account in pproc

    latsize     = np.asarray(latsize)
    ndim        = len(latsize)
    lt          = latsize[t_dir]
    assert(4 == ndim)
    assert(3 == t_dir)  # smart, eh?
    def get_spatial(x) : return np.asarray(x)[..., :t_dir]
    latsize_x   = get_spatial(latsize)

    csrcgrp_list= np.asarray(csrcgrp_list)
    n_grp, grp_size = csrcgrp_list.shape[0:2]
    n_data      = n_grp * grp_size
    assert(4 == csrcgrp_list.shape[-1])

    if None is corr_tlen : corr_tlen = 1+tsep
    psnk        = np.asarray(psnk)  # for phase correction if xyzsrc!=xyzsrc0
    tcut        = min(1+tsep, corr_tlen)    # number of available timeslices
    qext_list   = np.asarray(qext_list)
    n_gamma     = len(gamma_list)
    n_lpath     = len(lpath_list)
    n_qext      = len(qext_list)

    # some entries may be undef. if tcut < 1+tsep
    bb_res      = np.empty((n_data, n_gamma, n_lpath, n_qext, 1+tsep), dtype)
    bb_res[:]   = np.nan        # fill to catch uninitialized entries
    bb_res[..., tcut:] = 0.     # ... except where data is unavailable

    csrc_list   = []
    i_data = 0
    for i_grp, csrcgrp in enumerate(csrcgrp_list) :
        n_src       = len(csrcgrp)
        if VERBOSE : 
            print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
        csrc0       = csrcgrp[0]
        tsrc0       = csrc0[t_dir]
        
        bbfile, bbkpath = fk_in_func(csrcgrp)
        aff_r   = aff_open_reader_protected(bbfile)
        bb      = lhpd.latcorr.aff_read_lpathkey_list(aff_r, bbkpath, 
                        gamma_list, lpath_list, qext_list)
        assert(bb.shape[-1] == corr_tlen * grp_size)        # sanity check
        aff_r.close()

        for i_c, c in enumerate(csrcgrp) :
            # for initial source at x0, lattice data are
            #   bb(psnk, qext; x0) = \sum_{y,z} exp( -i*psnk.(y-x0) +i*qext.(z-x0))
            # with different x1 != x0, 
            #   bb(psnk, qext; x1) = \sum_{y,z} exp( -i*psnk.(y-x1) +i*qext.(z-x1))
            #           = bb(psnk, qext; x0) * exp( i*(psnk-qext).(x1-x0) )
            x10dl   = np.asarray(get_spatial(c - csrc0), np.float64) / latsize_x
            # [i_qext]
            csrc_ph = np.exp(2j*math.pi * (x10dl * (-qext_list)).sum(-1))       
            # sic! bkwsrc's are created with individual phases (sink mom proj wrt csrc_i)
            #  but insertion-momentum projected wrt csrc0, ~exp(i*q.(x-src0)), 
            #  so correction = exp(i*(-q)*(csrc_i - csrc0))
            #  did not do any difference for psnk=(0,0,0) used so far [2018/01/09]

            # XXX [Qlua] qcd.save_bb DOES NOT apply BC[t]
            # XXX [Qlua] qcd.contract_ for qbarq is also called with bc_t=1
            # XXX calc_c23pt_cpbar_volcedm.qlua DOES NOT apply BC[t] to seqsrc
            # XXX therefore, NEED TO APPLY BC_FACTOR HERE
            if c[t_dir] + tsep < lt: bc_factor = 1
            else : bc_factor = bc_t
            #t_sh    = (lt + c[t_dir] - tsrc0) % lt     # XXX if "unchopped"
            t_sh    = i_c * corr_tlen
            # phase if csrc is shifted wrt csrc0

            bb_res[i_data + i_c,...,:tcut] = bc_factor * bb[..., t_sh : t_sh + tcut] * csrc_ph[:, None]

        csrc_list.extend(list(csrcgrp))
        i_data += len(csrcgrp)

    assert(n_data == i_data)
    assert(len(csrc_list) == n_data)
    meas_spec_list = np.array([ (cfgkey, c) for c in csrc_list],
                        dtype=lhpd.h5_io.h5_meas_dtype)

    bb_shape = (n_data, n_gamma, len(lpath_list), len(qext_list), 1+tsep)
    b = h5f.require_dataset(h5key, bb_shape, dtype, fletcher32=True)
    b[:] = scale * bb_res

    # "axes"
    b.attrs['dim_spec']      = c3_bb_dim_spec
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(b, meas_spec_list)
    b.attrs['gamma_list']    = np.r_[0 : 16]
    b.attrs['linkpath_list'] = np.array(lpath_list, dtype='S')
    b.attrs['psnk']          = psnk
    b.attrs['qext_list']     = np.array(qext_list, dtype=np.int32)
    b.attrs['tau_list']      = np.r_[0 : tsep+1]
    # other parameters
    b.attrs['source_sink_dt']= int(tsep)
    b.attrs['latsize']       = np.array(latsize)
    b.attrs['t_dir']         = t_dir

    for k, v in iteritems(attrs_kw) :
        b.attrs[k] = h5type_attr_fix_(v)

    h5f.flush()


def latcorr_lpath_list(ins_d) :
    """ generate lpath_list depending on ins_d['kind'] """
    ins_kind    = ins_d['kind']
    if   'bb'   == ins_kind :
        return lhpd.latcorr.make_linkpath_list_bb(ins_d['lmin'], ins_d['lmax'])
    elif 'qpdf' == ins_kind :
        return lhpd.latcorr.make_linkpath_list_qpdf(
                ins_d['ldir'], ins_d['lmin'], ins_d['lmax'])
    elif 'tmd'  == ins_kind :
        return lhpd.latcorr.make_linkpath_list_tmd(
                ) # TODO implement
    else : raise ValueError(ins_kind)


def latcorr_conv2hdf(
            dat_d,                  # cfgkey, ama, ...
            lc_d,                   # all latcorr metainfo
            csrcgrp_list,
            latsize, t_dir, corr_tlen, bc_t, attrs_kw,
            data_in=None, 
            h5_overwrite=True,
            dtype=np.complex128, 
            saved_bc_t=None,         # if different from bc_t
            scale=None,
            VERBOSE=False) :
    # in
    if None is data_in : data_in = dat_d['data_dir']
    if None is saved_bc_t : saved_bc_t = bc_t
    # out
    h5file, h5key = get_lcdb_loc(dictnew(dat_d, kind='conv'), lc_d)
    lhpd.mkpath(os.path.dirname(h5file))
    h5f     = h5py.File(h5file, 'a')
    if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5f, [h5key])

    kind    = lc_d['kind']
    if   kind in [ 'c2', 'c2vi', 'c2h', 'c2lec' ]:
        c2_d        = lc_d['c2_d']
        tpol_list   = c2_d['tpol_list']
        psnk_list   = c2_d['psnk_list']
        tmax        = c2_d.get('tmax')

        def fk_in_func(csrcgrp, tpol) :
            return get_lcdb_loc(
                    dictnew(dat_d, data_dir=data_in, kind='aff', csrcgrp=csrcgrp), 
                    dictnewd(lc_d, c2_d=dict(tpol=tpol)))

        conv2hdf_c2pt_aff_chop(h5f, h5key, fk_in_func,
            dat_d['cfgkey'], csrcgrp_list, 
            psnk_list, tpol_list, 
            latsize, t_dir, corr_tlen, bc_t, attrs_kw, 
            saved_bc_t=saved_bc_t,
            tmax=tmax,
            scale=scale,
            dtype=dtype, VERBOSE=VERBOSE)

    elif 'c3' == kind and 'pdecay' == lc_d['ins_d']['kind'] :
        c3_d        = lc_d['c3_d']
        ins_d       = lc_d['ins_d']
        op_tpol_list= c3_d['op_tpol_list'] 
        qext_list   = np.asarray(ins_d['qext_list'])
        tmax        = 1 + c3_d['tsep']

        def fk_in_func(csrcgrp, tpol) :
            return get_lcdb_loc(
                    dictnew(dat_d, data_dir=data_in, kind='aff', csrcgrp=csrcgrp), 
                    dictnewd(lc_d, c3_d=dict(op_tpol=tpol)))

        conv2hdf_c2pt_aff_chop(h5f, h5key, fk_in_func,
            dat_d['cfgkey'], csrcgrp_list, 
            qext_list, op_tpol_list, 
            latsize, t_dir, corr_tlen, bc_t, attrs_kw, 
            saved_bc_t=saved_bc_t,
            tmax=tmax,
            scale=scale,
            dtype=dtype, VERBOSE=VERBOSE)
    
    elif kind in [ 'c3', 'c3vi' ] :
        c3_d        = lc_d['c3_d']
        ins_d       = lc_d['ins_d']
        gamma_list  = range(16)
        lpath_list  = latcorr_lpath_list(ins_d)
        qext_list   = np.asarray(ins_d['qext_list'])

        def fk_in_func(csrcgrp) :
            return get_lcdb_loc(
                    dictnew(dat_d, data_dir=data_in, kind='aff', csrcgrp=csrcgrp), 
                    lc_d)

        conv2hdf_qbarq_aff_chop(h5f, h5key, fk_in_func, 
            dat_d['cfgkey'], csrcgrp_list, 
            c3_d['psnk'], c3_d['tsep'], gamma_list, lpath_list, qext_list,
            latsize, t_dir, corr_tlen, bc_t, attrs_kw, 
            saved_bc_t=saved_bc_t,
            scale=scale,
            dtype=dtype, VERBOSE=VERBOSE)


    # TODO handle cases other than c2
    else : raise ValueError(kind)

    h5f.flush()
    h5f.close()


def h5select_c2(h5f, h5key, h5d_in, 
            tpol_list=None, 
            psnk_list=None, 
            tmax=None) :    
    tpol_list_in    = h5d_in.attrs['tpol_list']
    if not None is tpol_list :
        tpol_slice  = lhpd.np_index_first_list(tpol_list, tpol_list_in)
        assert(len(tpol_slice) == len(tpol_list))
    else : tpol_slice = slice(None)

    psnk_list_in    = h5d_in.attrs['psnk_list']
    if not None is psnk_list :
        psnk_slice  = lhpd.np_index_first_list(psnk_list, psnk_list_in)
        assert(len(psnk_slice) == len(psnk_list))
    else : psnk_slice = slice(None)
    
    tmax_in = h5d_in.shape[-1]
    if not None is tmax and tmax < tmax_in :
        t_slice = slice(tmax)
    else :
        tmax = tmax_in
        t_slice = slice(None)

    h5d_in_slice = ( slice(None), tpol_slice, psnk_slice, t_slice)

    h5d = h5f.create_dataset(h5key, data=h5d_in[()][h5d_in_slice])
    h5d.attrs['psnk_list']  = np.array(psnk_list, dtype=np.int32)
    h5d.attrs['tpol_list']  = np.array(tpol_list, dtype='S')
    h5d.attrs['t_list']     = np.r_[0 : tmax]
    
    # copy remaining metainfo
    lhpd.h5_io.h5_set_datalist(h5d, lhpd.h5_io.h5_get_datalist(h5d_in))
    lhpd.h5_io.h5_copy_attr(h5d, h5d_in, ['dim_spec', 'latsize', 't_dir'])

    h5d_attrs_keys = h5d.attrs.keys()
    for k in h5d_in.attrs.keys() : 
        if not k in h5d_attrs_keys : 
            h5d.attrs[k] = h5type_attr_fix_(h5d_in.attrs[k])
        
    h5f.flush()

def h5select_c3x_c3(h5f, h5key, h5d_in, 
            tpol, psnk, tsep,
            h5_overwrite=True,
            VERBOSE=False) :
    assert((h5d_in.attrs['dim_spec'] == c3x_op_dim_spec).all())

    tpol            = np.string_(tpol)
    tpol_list_in    = h5d_in.attrs['tpol_list']
    i_tpol          = lhpd.np_find_first(tpol, tpol_list_in)[0]

    psnk            = np.asarray(psnk)
    psnk_list_in    = h5d_in.attrs['psnk_list']
    i_psnk          = lhpd.np_find_first(psnk, psnk_list_in)[0]

    tsep_list_in    = h5d_in.attrs['tsep_list']
    i_tsep          = lhpd.np_find_first(tsep, tsep_list_in)[0]

    n_data  = h5d_in.shape[0]
    n_qext  = h5d_in.shape[5]
    n_comp  = h5d_in.shape[4]
    sh_out  = (n_data, n_comp, n_qext, 1 + tsep)
    if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5f, [h5key])
    h5d     = h5f.require_dataset(h5key, shape=sh_out, dtype=h5d_in.dtype, fletcher32=True)
    a       = h5d_in[:, i_tpol, i_psnk, i_tsep]
    h5d[:]  = a[..., : 1 + tsep]

    lhpd.h5_io.h5_copy_attr(h5d, h5d_in, [
            'source_hadron', 'sink_hadron',
            'comp_list', 'qext_list', 'tau_list' ],
            check_key=True)
    h5d.attrs['dim_spec']   = c3_op_dim_spec
    h5d.attrs['tpol']       = tpol
    h5d.attrs['sink_mom']   = psnk
    h5d.attrs['source_sink_dt'] = tsep
    lhpd.h5_io.h5_set_datalist(h5d, lhpd.h5_io.h5_get_datalist(h5d_in))
    h5f.flush()


def latcorr_select(
            dat_d, lc_d,     # out
            data_in=None, 
            h5_overwrite=True,
            VERBOSE=False) :
    
    assert(not None is data_in)
    assert(data_in != dat_d['data_dir'])
    h5file_in, h5key_in = get_lcdb_loc(dictnew(dat_d, data_dir=data_in), lc_d)
    
    h5file, h5key = get_lcdb_loc(dat_d, lc_d)
    lhpd.mkpath(os.path.dirname(h5file))
    h5f     = h5py.File(h5file, 'a')
    if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5f, [h5key])

    kind    = lc_d['kind']
    if   kind in [ 'c2', 'c2vi' ] :
        c2_d        = lc_d['c2_d']
        tpol_list   = c2_d.get('tpol_list')
        psnk_list   = c2_d.get('psnk_list')
        tmax        = c2_d.get('tmax')
        h5select_c2pt(h5f, h5key, h5file_in[h5key_in], 
                tpol_list=tpol_list, psnk_list=psnk_list, tmax=tmax)


    # TODO handle cases other than c2
    elif kind in [ 'c3', 'c3vi' ] : raise NotImplementedError(kind)
    else : raise ValueError(kind)

    h5f.flush()
    h5f.close()

def latcorr_bin_unbias(
            cfgkey_list, ama_list, dat_d, lc_d, 
            attrs_kw={},
            data_in=None, 
            lc_d_in=None,
            func=None,
            reweight_func=None,
            mfunc=None,
            keep_datalist=lambda mb: mb[0],
            do_ama=True,
            binsize=1,
            maxshape=None,      # FIXME this parameter will reduce shape but not axis description argiments!
            VERBOSE=False) :
    """ xd_kw = { c2_d= } for what='c2'
              = { c3_d=, ins_d= } for what='c3'
              = { c4_d=, ins_d=, vacins_d= } for what='c4'
    """
    # TODO if tpol_list/momlist/etc in source data are different, select/calc accordingly
    if None is data_in : data_in = dat_d['data_dir']
    if None is lc_d_in : lc_d_in = lc_d
    if VERBOSE :
        print("# cfgkey_list=%s  dat_d={%s}  lc={%s}" % (
                cfgkey_list_str(cfgkey_list), datd_str(dat_d), latcorr_str(lc_d)))
    assert(1 <= binsize)

    # bin individually
    h5_list_ama = {}
    for ama in ama_list : 
        h5_list  = [ get_lcdb_loc(dictnew(dat_d, data_dir=data_in, kind='conv', 
                            ama=ama, cfgkey=c), lc_d_in)
                     for c in cfgkey_list ]
        h5_list_ama[ama] = h5_list
        if do_ama : 
            h5_file, h5_kpath  = get_lcdb_loc(dictnew(dat_d, kind='bin', ama=ama), lc_d)
            if VERBOSE : print("# >>> %s[%s]" % (h5_file, h5_kpath))
            lhpd.mkpath(os.path.dirname(h5_file))
            lhpd.h5_io.h5file_bin(h5_file, h5_kpath, h5_list, 
                            attrs_kw=attrs_kw, keep_datalist=keep_datalist, 
                            func=func, reweight_func=reweight_func, mfunc=mfunc,
                            binsize=binsize,
                            maxshape=maxshape,
                            VERBOSE=VERBOSE)
                
    # unbias
    # TODO more general AMA schemes? several layers of stoch.improvement?
    # FIXME do not rely on global ama_ex, ama_sl
    if (ama_ex in ama_list) and (ama_sl in ama_list) :  
        h5_list  = [ c1 + c2 for c1,c2 in zip(
                h5_list_ama[ama_sl], h5_list_ama[ama_ex]) ]
        h5_file, h5_kpath  = get_lcdb_loc(dictnew(dat_d, kind='ub'), lc_d)
        if VERBOSE : print("# >>> %s[%s]" % (h5_file, h5_kpath))
        lhpd.mkpath(os.path.dirname(h5_file))
        lhpd.h5_io.h5file_bin_unbias(h5_file, h5_kpath, h5_list, 
                        attrs_kw=attrs_kw, keep_datalist=keep_datalist, 
                        func=func, reweight_func=reweight_func, mfunc=mfunc,
                        binsize=binsize,
                        maxshape=maxshape,
                        VERBOSE=VERBOSE)


def latcorr_merge(
            cfgkey_list, dat_d, lc_d,
            data_in=None, 
            VERBOSE=False) :
    # TODO if tpol_list/momlist/etc in source data are different, select/calc accordingly
    if None is data_in : data_in = dat_d['data_dir']
    if VERBOSE : 
        print("# cfgkey_list=%s  dat_d={%s}  lc_d={%s}" % (
                cfgkey_list_str(cfgkey_list), datd_str(dat_d), latcorr_str(lc_d)))
    h5_list  = [ get_lcdb_loc(dictnew(dat_d, data_dir=data_in, kind='conv', cfgkey=c), lc_d)
                    for c in cfgkey_list ]
    h5_file, h5_kpath  = get_lcdb_loc(dictnew(dat_d, kind='all'), lc_d)
    if VERBOSE : print("# >>> %s[%s]" % (h5_file, h5_kpath))
    lhpd.mkpath(os.path.dirname(h5_file))
    lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)


# compute op's and store in an HDF5 file
# TODO if tpol_list/momlist/etc in source data are different, select/calc accordingly
def calc_qbarq_op_conn(
            h5f, h5key, h5_bb,
            qext_list, op, ir, ir_scale,
            VERBOSE=False) :
    """
        h5f     open HDF5 h5py file
        h5key    kpath relative to root
    """
    qext_list   = np.asarray(qext_list)
    p3snk       = h5_bb.attrs['sink_mom']
    tsep        = h5_bb.attrs['source_sink_dt']
    latsize     = h5_bb.attrs['latsize']
    t_dir       = h5_bb.attrs['t_dir']

    n_data  = h5_bb.shape[0]
    n_tau   = h5_bb.shape[-1]
    n_qext  = len(qext_list)
    
    bb_dim_spec     = h5_bb.attrs['dim_spec']
    bb_qext_list    = np.asarray(h5_bb.attrs['qext_list'])
    bb_lpath_list   = h5_bb.attrs['linkpath_list']
    bb_gamma_list   = np.asarray(h5_bb.attrs['gamma_list'])
    assert((bb_qext_list == qext_list).all())       # FIXME select by slice
    assert((np.array(['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau'], 'S')
            == bb_dim_spec[[0,1,2,3,4]]).all())
    def func_get_bb(gamma, lpath_str) :
        i_l = np_find_first(lpath_str, bb_lpath_list)[0]
        i_g = np_find_first(gamma, bb_gamma_list)[0]
        #i_q = np_find_first(q3ext, bb_qext_list)[0]
        return ir_scale * h5_bb[:, i_g, i_l]

    n_comp = H4_repr_dim[ir]
    op_shape = (n_data, n_comp, n_qext, n_tau)
    # [i_data, i_comp, i_qext, i_tau]
    h5_op = h5f.require_dataset(h5key, op_shape, np.complex128, 
                    fletcher32=True)
    
    # TODO select qext-slice (with some general func) and do all qext at once
    #for i_qext, q3ext in enumerate(h5_bb.attrs['qext_list']) :
        #p3src = p3snk - q3ext

    # TODO cleanup: use universal functions (esp.>=1 link ops.) to keep it DRY
    # TODO add tensor0, pstensor0, sigma2a2
    if 'tensor0' == op : 
        assert('H4_T0_d1r1' == ir)
        h5_op[:, 0] =  func_get_bb( 0, 'l0_')
    elif 'pstensor0' == op : 
        assert('H4_T0_d1r1' == ir)
        h5_op[:, 0] =  func_get_bb(15, 'l0_')
    elif 'tensor1' == op : 
        assert('H4_T1_d4r1' == ir)
        h5_op[:, 0] =  func_get_bb( 1, 'l0_')
        h5_op[:, 1] =  func_get_bb( 2, 'l0_')
        h5_op[:, 2] =  func_get_bb( 4, 'l0_')
        h5_op[:, 3] =  func_get_bb( 8, 'l0_')
    elif 'pstensor1' == op :
        assert('H4_T1_d4r1' == ir)
        h5_op[:, 0] =  func_get_bb(14, 'l0_')
        h5_op[:, 1] = -func_get_bb(13, 'l0_')
        h5_op[:, 2] =  func_get_bb(11, 'l0_')
        h5_op[:, 3] = -func_get_bb( 7, 'l0_')
    elif 'sigma2a2' == op :
        assert('H4_T2_d6r1' == ir)     # antisymmetric
        h5_op[:, 0] =  1j*func_get_bb( 3, 'l0_')
        h5_op[:, 1] =  1j*func_get_bb( 5, 'l0_')
        h5_op[:, 2] =  1j*func_get_bb( 9, 'l0_')
        h5_op[:, 3] =  1j*func_get_bb( 6, 'l0_')
        h5_op[:, 4] =  1j*func_get_bb(10, 'l0_')
        h5_op[:, 5] =  1j*func_get_bb(12, 'l0_')
    else : raise NotImplemented((op, ir))

    lhpd.h5_io.h5_copy_attr(h5_op, h5_bb, [
            'sink_mom', 'tau_list', 'source_sink_dt', 
            'source_hadron', 'sink_hadron', 'tpol'],
            check_key=True)
    h5_op.attrs['qext_list'] = qext_list
    lhpd.h5_io.h5_set_datalist(h5_op, lhpd.h5_io.h5_get_datalist(h5_bb))
    h5_op.attrs['dim_spec'] = c3_op_dim_spec
    h5_op.attrs['comp_list'] = h5type_attr_fix_(H4_repr_comp[ir])
    h5f.flush()



def latcorr_calc_qbarq_op(
            dat_d, lc_d, 
            data_in=None, 
            h5_overwrite=True, 
            VERBOSE=False) :
    kind = lc_d['kind']
    assert(kind in ['c3', 'c3vi']) # TODO support c3vi?
    ins_d   = lc_d['ins_d']
    op      = ins_d['op']
    
    # in
    if None is data_in : data_in = dat_d['data_dir']
    bb_file, bb_key = get_lcdb_loc(dictnew(dat_d, data_dir=data_in),
                                   dictnewd(lc_d, ins_d=dict(kind='bb')))
    bb_h5f  = h5py.File(bb_file, 'r')
    
    ir_list = ins_d.get('ir_list', lhpd.latcorr.op_default_ir_list(op))   # XXX no renormalization
    for ir, ir_scale in ir_list :
        op_file, op_key = get_lcdb_loc(dat_d, dictnewd(lc_d, ins_d=dict(ir=ir)))
        lhpd.mkpath(os.path.dirname(op_file))
        op_h5f  = h5py.File(op_file, 'a')

        if h5_overwrite : lhpd.h5_io.h5_purge_keys(op_h5f, [op_key])
        calc_qbarq_op_conn(op_h5f, op_key, bb_h5f[bb_key], ins_d['qext_list'], 
                op, ir, ir_scale, VERBOSE=VERBOSE)

        op_h5f.close()

    bb_h5f.close()


def h5_save_sum_rcut(h5f, h5k_top, d, rcut_list, axes=None) :
    """ save rcut qtopo density to h5f:<h5k_top>/rcut<rcut> """
    d       = np.asarray(d)
    ls      = np.asarray(d.shape) 
    if None is axes : axes = range(len(ls))
    ls_a    = ls[list(axes)]
    d_f     = np.fft.fftn(d, s=ls_a, axes=axes)
    r2      = lhpd.np_mesh_rsq(ls, csrc=None, axes=axes)
    for i_rcut, rcut in enumerate(rcut_list) :
        if None is rcut : continue # skip full sum
        xmask   = np.where(r2 <= rcut*rcut, 1, 0)
        xmask_f = np.fft.fftn(xmask, s=ls_a, axes=axes)
        dcut    = np.fft.ifftn(xmask_f * d_f, s=ls_a, axes=axes)
        #print("rcut=%d  |dcut.real|=%e  |dcut.imag|=%e" % (
        #        rcut, np.abs(dcut.real).sum(), np.abs(dcut.imag).sum()))

        k       = "%s/rcut%d" % (h5k_top, rcut)
        try : del h5f[k]
        except : pass
        h5f[k]  = np.array(dcut.real, dtype=np.float32)
        h5f.flush()


def collect_prop_ft(cfgkey_list, ama_list,
                dat_d, p_d, 
                mom_list, latsize,
                data_in=None,
                VERBOSE=True) :

    from lhpd.npr import NCOLOR as NC, NSPIN as NS
    if None is data_in : data_in = dat_d['data_dir']

    for ama in ama_list :
        prop_fname_out, prop_kpath_out = get_lcdb_loc(dictnew(dat_d, ama=ama, kind='all'), p_d)
        lhpd.mkpath(os.path.dirname(prop_fname_out))
        h5f_out = h5py.File(prop_fname_out, 'a')
        lhpd.h5_io.h5_purge_keys(h5f_out, [prop_kpath_out])

        csrc_list_c = [ make_csrcgrp_list(ama, c).reshape(-1, len(latsize)) for c in cfgkey_list ]
        n_data  = sum([ len(cl) for cl in csrc_list_c ])
        n_mom   = len(mom_list)
        h5d_out = h5f_out.require_dataset(
            prop_kpath_out, (n_data, n_mom, NC,NS,NC,NS), np.complex128, fletcher32=True)
        
        if VERBOSE: print("# ama=%s" % ama)
        meas_list = []
        i_data = 0
        for i_c, cfgkey in enumerate(cfgkey_list) :
            if VERBOSE: print("# cfgkey=%s" % cfgkey)
            csrc_list    = csrc_list_c[i_c]
            meas_list.append(np.array([ (cfgkey, csrc) for csrc in csrc_list ], 
                             dtype=lhpd.h5_io.h5_meas_dtype))
            for i_csrc, csrc in enumerate(csrc_list) :
                if VERBOSE: print("# csrc=%s" % str(csrc))
                prop_fname_in, prop_kpath_in = get_lcdb_loc(dictnew(
                        dat_d, data_dir=data_in, ama=ama, cfgkey=cfgkey, csrc=csrc, kind='raw'), p_d)
                h5f_in = h5py.File(prop_fname_in, 'r')
                h5d_in = h5f_in[prop_kpath_in]
                prop_saved_ls = h5d_in.shape[:4]
                assert((prop_saved_ls <= latsize).all())
                mom_list_norm = (mom_list + prop_saved_ls) % prop_saved_ls
                assert((0 <= mom_list_norm).all())
                h5d_in_v = h5d_in[()]
                h5d_out[i_data] = np.array([ h5d_in_v[tuple(m)].transpose((2,0,3,1)) for m in mom_list_norm ])
                i_data += 1
                h5d_in, h5d_in_v = None, None
                h5f_in.close()

        meas_list   = np.concatenate(meas_list)

        h5d_out.attrs['dim_spec']   = ['i_data', 'i_mom', 'ic', 'is', 'jc', 'js']
        lhpd.h5_io.h5_set_datalist(h5d_out, meas_list)
        h5d_out.attrs['latsize']    = np.array(latsize)
        lhpd.h5_io.h5_set_hugeattr(h5d_out, 'mom_list', np.array(mom_list))

        h5f_out.flush()
        h5f_out.close()

