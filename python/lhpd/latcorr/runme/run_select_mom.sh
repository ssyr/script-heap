#!/bin/bash -x


for run in $* ; do
  ( cd data_out/$run ;
    echo PWD $(pwd)
  
    mkdir -p c2pt.bin.selmom c2pt_volcedm.bin.selmom
    for sm in SS SP  ; do
      ipython --pdb ../../h5select.py i_psnk psnk_list 1 \
          c2pt.bin/c2pt-unbias.proton.${sm}.h5 \
          c2pt.bin.selmom/c2pt-unbias.proton.${sm}.h5 \
          /c2pt/${sm}/proton

      for flav in U D ; do
        ipython --pdb ../../h5select.py i_psnk psnk_list 1 \
            c2pt_volcedm.bin/c2pt_volcedm-unbias.proton.${sm}.${flav}.orig.h5 \
            c2pt_volcedm.bin.selmom/c2pt_volcedm-unbias.proton.${sm}.${flav}.orig.h5 \
            /c2pt_volcedm/${sm}/proton/${flav}/orig
      done
    done
  )
done
