from __future__ import print_function
from future.utils import iteritems
from past.builtins import basestring
import numpy as np
import h5py
import lhpd
from lhpd.pymath.H4_tensor import *
import dataview as dv

def conv_qloop_sdbh5_h5(cfg_key, n_hada,
        latsize, t_axis, qext_list, lpath_list,
        attr_kw={}) :
    """ convert to regular bbqloop.h5 """
    n_gamma = 16
    n_qext  = len(qext_list)
    n_lpath = len(lpath_list)
    lt      = latsize[t_axis]
    data_in = np.empty((n_qext, n_lpath, n_gamma, lt), np.complex128)

    sdbh5_h5file= get_disc_sdbh5_file(cfg_key, n_hada)
    sdbh5_h5f   = h5py.File(sdbh5_h5file, 'r')
    
    for i_qext, qext in enumerate(qext_list) :
        for i_lp, lp in enumerate(lpath_list) :
            k = "%s-%s" % (qext_str_sdbh5(qext), lpath_str_sdbh5(lp))
            data_in[i_qext, i_lp] = sdbh5_h5f["/%s-re" % k].value + 1j*sdbh5_h5f["/%s-im" % k].value

    sdbh5_h5f.close()

    bbql_h5file = get_bbqloop_h5_file(cfg_key, n_hada) 
    lhpd.mkpath(os.path.dirname(bbql_h5file))
    bbql_h5f    = h5py.File(bbql_h5file, 'a')

    for i_lp, lp in enumerate(lpath_list) :
        for g in range(n_gamma) :
            k   = "/%s/g%d" % (lpath_str(lp), g)
            h5d = bbql_h5f.require_dataset(k, (n_qext, lt), np.complex128, exact=True, fletcher32=True)
            h5d[:] = data_in[:, i_lp, g, :]
            h5d.attrs['dim_spec']   = ['i_qext', 'i_t']
            h5d.attrs['qext_list']  = qext_list
            h5d.attrs['t_list']     = np.r_[:lt]
            for k, v in iteritems(attr_kw) :
                h5d.attrs[k] = v

    bbql_h5f.flush()
    bbql_h5f.close()

# need to bin&unbias right away because have many more index values
def calc_save_unbias_opdisc(
                h5file_sl, h5key_sl,             # output
                h5file_ub, h5key_ub,
                latsize,
                c2pt_h5d_sl, c2pt_h5d_ex, 
                bbqloop_h5g,
                psnk, qext_list,      # 3d, rel. to c2pt boosts
                srctype_list, snktype_list, tpol_list, 
                tsep_max, tau_max,          # set both to hslab_len-1
                op, ir_name, 
                t_dir=3,
                h5_overwrite=True,
                c2ptavg_sl_h5key=None,      # if given, save c2ptavg
                c2ptavg_ub_h5key=None,      # if given, save c2ptavg
                opqloop_sl_h5key=None,      # if given, save op-qloop 
                opqloop_ub_h5key=None,      # if given, save op-qloop 
                attrs_kw={}) :
    """
        in : c2pt, bb-qloop
        out: op, binned over c2pt samples
    """
    assert(3 == t_dir)
    assert(4 == len(latsize))
    lt          = latsize[t_dir]
    latsize_x   = latsize[:t_dir]
    n_src       = c2pt_h5d_sl.shape[0]
    n_srctype   = len(srctype_list)
    n_snktype   = len(snktype_list)
    n_tpol      = len(tpol_list)
    tlen_c2     = c2pt_h5d_sl.shape[-1]
    assert((np.r_[:tlen_c2] == c2pt_h5d_sl.attrs['t_list']).all())    # now can work only with contiguous tsnk
    assert(tsep_max < tlen_c2)
    n_tsep      = tsep_max + 1
    n_tau       = tau_max + 1
    n_comp      = H4_repr_dim[ir_name]
    n_qext      = len(qext_list)

    def get_bb(gamma, lp_str, q_list, tsrc, t_len) :
        """ t_len = how many timeslices to read, counting from tsrc from  
            return [i_q, t]
        """
        n_q     = len(q_list)
        res     = np.empty((n_q, t_len), dtype=np.complex128)
        if tsrc + t_len <= lt :
            bbql_h5d = bbqloop_h5g["%s/g%d" % (lp_str, gamma)]
            for i_q, qext in enumerate(q_list) :
                i_qext  = lhpd.np_find_first(qext, bbql_h5d.attrs['qext_list'])[0]
                res[i_q]= bbql_h5d[i_qext, tsrc : tsrc + t_len]
        else :
            t1  = lt - tsrc
            res[:, : t1 ]   = get_bb(gamma, lp_str, q_list, tsrc, t1)
            res[:, t1 : ]   = get_bb(gamma, lp_str, q_list, 0, t_len - t1)
        return res

    def get_op_qloop(op, q_list, tsrc, t_len) :
        """ return [i_q, i_comp, t] """
        n_q     = len(q_list)
        # TODO cleanup: use universal functions (esp.>=1 link ops.) to keep it DRY
        get_bb_G= lambda g : get_bb(g, 'l0_', q_list, tsrc, t_len)
        if   'tensor0' == op :      # TODO for scalar density, also need the usual vacuum avg; that would be q=0
            assert('H4_T0_d1r1' == ir_name)
            res = np.asarray(lhpd.latcorr.calc_scalar_fromGamma(get_bb_G))
            #res -= res.mean(0).mean(-1)[None, :, None] # XXX subtraction of cfg average is wrong
        elif 'pstensor0' == op : 
            assert('H4_T0_d1r1' == ir_name)
            res = np.asarray(lhpd.latcorr.calc_pseudoscalar_fromGamma(get_bb_G))
        elif 'tensor1' == op : 
            assert('H4_T1_d4r1' == ir_name)
            res     = np.asarray(lhpd.latcorr.calc_vec_fromGamma(get_bb_G))
        elif 'pstensor1' == op :
            assert('H4_T1_d4r1' == ir_name)
            res     = np.asarray(lhpd.latcorr.calc_axialvec_fromGamma(get_bb_G))
        elif 'sigma2a2' == op :
            assert('H4_T2_d6r1' == ir_name)     # antisymmetric
            res     = np.asarray(lhpd.latcorr.calc_sigma2a2_H4d6r1_fromGamma(get_bb_G))
        else : raise NotImplemented((op, ir_name))
        return (-res) # sic!  the Grassmann sign is applied here

    def get_c2pt(h5d, psnk) :
        """ return [i_src, i_srctype, i_snktype, i_tpol, t] """
        assert(not h5d.attrs['time_rev'])
        n_data      = h5d.shape[0]
        n_t         = h5d.shape[-1]
        assert(n_tsep <= n_t)
        res         = np.empty((n_data, n_srctype, n_snktype, n_tpol, n_tsep), np.complex128)

        i_psnk  = lhpd.np_find_first(psnk, h5d.attrs['psnk_list'])[0]
        srctype_index   = list(h5d.attrs['srctype_list'])
        snktype_index   = list(h5d.attrs['snktype_list'])
        had_tpol_index  = list(h5d.attrs['had_list'])
        for (i1,srctype), (i2,snktype), (i3,tpol) in it.product(
                    enumerate(srctype_list), 
                    enumerate(snktype_list), 
                    enumerate(tpol_list)) :
            i_src   = srctype_index.index(srctype)
            i_snk   = snktype_index.index(snktype)
            i_tpol  = had_tpol_index.index("proton_%s" % tpol)
            res[:, i1, i2, i3] = h5d[:, i_src, i_snk, i_tpol, i_psnk, :n_tsep]

        # [i_src, i_srctype, i_snktype, i_tpol, t]
        return res


    # get c2pt DATALISTs, csrc lists
    assert(lhpd.h5_io.h5dset_attr_equal(c2pt_h5d_sl, c2pt_h5d_ex))
    c2pt_dl_sl  = lhpd.h5_io.h5_get_datalist(c2pt_h5d_sl)
    assert(0 < len(c2pt_dl_sl))
    c2pt_dl_ex  = lhpd.h5_io.h5_get_datalist(c2pt_h5d_ex)
    assert(0 < len(c2pt_dl_ex))
    csrc_list_sl= np.array([ m['source_coord'] for m in c2pt_dl_sl ])
    assert(len(csrc_list_sl) == n_src)

    # check that the config is the same, build map of equal tsrc_samp: [tsrc_samp]=[ csrc, ... ]
    cfgkey      = c2pt_dl_sl[0]['ckpoint_id']
    isamp_tmap   = {}
    for i_src, m in enumerate(c2pt_dl_sl) :
        assert(cfgkey == m['ckpoint_id'])
        tsrc_samp        = csrc_list_sl[i_src][t_dir]
        isamp_tmap.setdefault(tsrc_samp, []).append(i_src)
    for i_src, m in enumerate(c2pt_dl_ex) :
        assert(cfgkey == m['ckpoint_id'])

    # read c2pt, unbias
    # [i_src, i_srctype, i_snktype, i_tpol, tsep]
    psnk     = np.asarray(psnk)
    c2pt_sl     = get_c2pt(c2pt_h5d_sl, psnk)
    c2pt_ex     = get_c2pt(c2pt_h5d_ex, psnk)
    m_sl, m_ex  = lhpd.np_match(c2pt_dl_sl, c2pt_dl_ex, hashfunc=lhpd.h5_io.h5_meas_hash)
    assert(0 < len(m_sl))   # must have bias correction
    c2pt_ub     = c2pt_sl + (c2pt_ex[m_ex] - c2pt_sl[m_sl]).mean(0)
    # [i_q, mu]
    qext_list= np.asarray(qext_list)
    
    # sum_{tsrc_samp} [ op(tsrc_samp) * sum_{xsrc} [ c2pt{xsrc, tsrc_samp} * exp(-I*xsrc*qext) ] ]
    opdisc_sh   = (n_srctype, n_snktype, n_tpol, n_tsep, n_comp, n_qext, n_tau)
    opdisc_sl   = np.zeros(opdisc_sh, dtype=np.complex128)
    opdisc_ub   = np.zeros(opdisc_sh, dtype=np.complex128)
    # avg factors
    c2ptavg_sh  = (n_srctype, n_snktype, n_tpol, n_tsep)
    opqloop_sh  = (n_comp, n_qext, n_tau)
    opqloop_factor  = np.zeros(opqloop_sh, dtype=np.complex128)

    #c2pt_factor_sl  = np.zeros((n_srctype, n_snktype, n_tpol, n_tsep), dtype=np.complex128)
    #c2pt_factor_ub  = np.zeros((n_srctype, n_snktype, n_tpol, n_tsep), dtype=np.complex128)

    samp_cnt    = 0
    for tsrc_samp, isamp_list in iteritems(isamp_tmap) :
        assert((csrc_list_sl[isamp_list][:, t_dir] == tsrc_samp).all())
        print("# tsrc_samp=%d, n_src_tslice=%d" % (tsrc_samp, len(isamp_list)))
        # use actual qext, neg.components wrapped
        # [i_comp, i_qext, tau]
        op_ql   = get_op_qloop(op, qext_list, tsrc_samp, n_tau) # FIXME may be better to read full lt right away?
        opqloop_factor += op_ql
        # [i_src_tslice, mu]
        x2l_list    = np.asarray(csrc_list_sl[isamp_list, :t_dir], np.float64) / latsize_x
        # [i_src_tslice, i_qext]
        expi_qx     = np.exp(-2j*math.pi * (x2l_list[:,None] * qext_list[None,:]).sum(-1))
        # [i_srctype, i_snktype, i_tpol, tsep, i_qext]
        c2pt_avg_sl = (c2pt_sl[isamp_list, :, :, :, :, None] * expi_qx[ :, None, None, None, None, : ]).mean(0)
        c2pt_avg_ub = (c2pt_ub[isamp_list, :, :, :, :, None] * expi_qx[ :, None, None, None, None, : ]).mean(0)
        
        # [i_srctype, i_snktype, i_tpol, tsep, i_comp, i_qext, tau]
        opdisc_sl += c2pt_avg_sl[:, :, :, :, None, :, None] * op_ql[None, None, None, None, :, :, :]
        opdisc_ub += c2pt_avg_ub[:, :, :, :, None, :, None] * op_ql[None, None, None, None, :, :, :]
        samp_cnt  += 1

    opdisc_sl   /= samp_cnt
    opdisc_ub   /= samp_cnt
    opqloop_factor /= samp_cnt

    def save_opdisc(h5g, h5kpath, opdisc, h5d_c2) :
        """ 
            h5d_c2      copy necessary attrs from here
        """
        if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5kpath])
        h5d_out = h5g.require_dataset(h5kpath, opdisc_sh, np.complex128,
                            fletcher32=True)
        h5d_out[:] = opdisc
        lhpd.h5_io.h5_copy_attr(h5d_out, h5d_c2, 
                [], # 
                check_key=True) 
        h5d_out.attrs['dim_spec']   = ['i_srctype', 'i_snktype', 'i_tpol', 'i_tsep', 'i_comp', 'i_qext', 'i_tau']
        h5d_out.attrs['sink_mom']   = psnk
        h5d_out.attrs['qext_list']  = qext_list
        h5d_out.attrs['tsep_list']  = np.r_[ : n_tsep]
        h5d_out.attrs['tau_list']   = np.r_[ : n_tau]
        h5d_out.attrs['comp_list']  = H4_repr_comp[ir_name]
        h5d_out.attrs['srctype_list']   = srctype_list
        h5d_out.attrs['snktype_list']   = snktype_list
        h5d_out.attrs['tpol_list']  = tpol_list
        
        for k, v in iteritems(attrs_kw) :
            h.attrs[k] = v
        h5d_out.file.flush()
        
    def save_c2ptavg(h5g, h5kpath, c2ptavg, h5d_c2) :
        if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5kpath])
        h5d_out = h5g.require_dataset(h5kpath, c2ptavg_sh, np.complex128,
                            fletcher32=True)
        h5d_out[:]  = c2ptavg
        lhpd.h5_io.h5_copy_attr(h5d_out, h5d_c2, 
                [], # 
                check_key=True) 
        h5d_out.attrs['dim_spec']   = ['i_srctype', 'i_snktype', 'i_tpol', 'i_tsep']
        h5d_out.attrs['sink_mom']   = psnk
        h5d_out.attrs['tsep_list']  = np.r_[ : n_tsep]
        h5d_out.attrs['srctype_list']   = srctype_list
        h5d_out.attrs['snktype_list']   = snktype_list
        h5d_out.attrs['tpol_list']  = tpol_list
        
        for k, v in iteritems(attrs_kw) :
            h.attrs[k] = v
        h5d_out.file.flush()

    def save_opqloop(h5g, h5kpath, opqloop) :
        if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5kpath])
        h5d_out = h5g.require_dataset(h5kpath, opqloop_sh, np.complex128,
                            fletcher32=True)
        h5d_out[:]  = opqloop
        h5d_out.attrs['dim_spec']   = ['i_comp', 'i_qext', 'i_tau']
        h5d_out.attrs['qext_list']  = qext_list
        h5d_out.attrs['tau_list']   = np.r_[ : n_tau]
        h5d_out.attrs['comp_list']  = H4_repr_comp[ir_name]
        for k, v in iteritems(attrs_kw) :
            h.attrs[k] = v
        h5d_out.file.flush()

    # save opdisc, c2ptavg, opqloop sl
    save_opdisc(h5file_sl, h5key_sl, opdisc_sl, c2pt_h5d_sl)
    if not None is c2ptavg_sl_h5key :
        save_c2ptavg(h5file_sl, c2ptavg_sl_h5key, c2pt_sl.mean(0), c2pt_h5d_sl)
    if not None is opqloop_sl_h5key :
        save_opqloop(h5file_sl, opqloop_sl_h5key, opqloop_factor) 

    # save opdisc, c2ptavg, opqloop ub
    save_opdisc(h5file_ub, h5key_ub, opdisc_ub, c2pt_h5d_sl)
    if not None is c2ptavg_ub_h5key :
        save_c2ptavg(h5file_ub, c2ptavg_ub_h5key, c2pt_ub.mean(0), c2pt_h5d_sl) # sic! no c2pt_h5d_ub
    if not None is opqloop_ub_h5key :
        save_opqloop(h5file_ub, opqloop_ub_h5key, opqloop_factor) 

def runme_calc_save_ff(
        h5_file, 
        mcgrp_list, ssgrp,
        flav_list, op_list,
        method_list,
        **kw ) :

    mlat        = kw['mlat']
    latsize     = kw['latsize']
    had         = kw['had']

    if isinstance(h5_file, basestring) :
        h5f = h5py.File(h5_file, 'a')
    elif isinstance(h5_file, h5py.File) or isinstance(h5_file, h5py.Group) : 
        h5f = h5_file
    else : raise ValueError(h5_file)

    for flav in flav_list :
        for op in op_list :
            ir_list = lhpd.latcorr.op_default_ir_list(op)
            gg      = ens_data_get(flav=flav, op=op, **kw)
            h5k = '/%s/%s/%s' % (had, op, flav)
            lhpd.h5_io.h5_purge_keys(h5f, [h5k])
            kw1 = dict(kw)
            lhpd.purge_keys(kw1, ['mlat', 'latsize'])
            lhpd.latcorr.calc_save_ff(h5f, h5k, gg, mcgrp_list, ssgrp, 
                         op, ir_list, mlat, latsize, method_list, **kw1)

    if isinstance(h5_file, basestring) :         
        h5f.close()
    elif isinstance(h5_file, h5py.File) or isinstance(h5_file, h5py.Group) : 
        h5f.flush()
    else : raise ValueError(h5_file)

