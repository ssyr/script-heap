# use cfgreflkey throughout; some data file names requrie cfgkey, without reflXXXX
def cfgreflkey_to_cfgkey(cfgreflkey) :
    return cfgreflkey[:-8] 

def get_disc2pt_h5_file(cfgkey, ama) :
    return "%s/disc2pt/disc2pt.%s.%s.h5" % (
            data_cache, cfgreflkey_to_cfgkey(cfgkey), ama)
get_c2pt_h5_file = get_disc2pt_h5_file

def get_c2pt_all_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
    return '%s/c2pt.all/c2pt-all.%s.%s.%s.h5' % (
            data_out, ama, had, sm_srcsnk)
def get_c2pt_bin_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
    return '%s/c2pt.bin/c2pt-bin.%s.%s.%s.h5' % (
            data_out, ama, had, sm_srcsnk)
def get_c2pt_unbias_h5_file(had, sm_srcsnk, data_out=data_out) :
    return '%s/c2pt.bin/c2pt-unbias.%s.%s.h5' % (
            data_out, had, sm_srcsnk)


def get_disc_sdbh5_file(cfgkey, n_hada) : 
    return "%s/%d/hada_0-%d_SVD_disco_cfg_cl21_32_64_b6p3_m0p2390_m0p2050_cfg_%s.h5" % (
            data_sdbh5, n_hada, n_hada - 1, cfgkey)

def get_bbqloop_h5_file(cfgkey, n_hada) :
    return "%s/bb-qloop-%d/bb-qloop.%s.h5" % (
            data_out, n_hada, cfgkey)

def get_opdisc_bin_h5_file(cfgkey, n_hada, ama, had, sm_srcsnk, psnk, op, data_out=data_out) :
    return "%s/op-disc-%d/op.%s.%s.%s.%s.%s.%s.h5" % (
            data_out, n_hada, cfgkey, ama, had, sm_srcsnk, psnk_str_f(psnk), op)

def get_opdisc_all_h5_file(n_hada, ama, had, sm_srcsnk, psnk, op, data_out=data_out) :
    return "%s/op-disc-%d.bin/op.%s.%s.%s.%s.%s.h5" % (
            data_out, n_hada, ama, had, sm_srcsnk, psnk_str_f(psnk), op)

