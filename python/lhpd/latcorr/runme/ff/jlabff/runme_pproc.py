from __future__ import print_function
from past.builtins import execfile
import itertools as it
import os, sys
                    
if '__main__' == __name__ :
    def print_usage(prg) :
        print("""Usage:
%s  <what>  <config>  [<job_id0>, ...]
What :
    conv_qloop_sdbh5_h5
    calc_opdisc
    bin_unbias_c2pt
    merge_opdisc
""" % (prg,))

    if len(sys.argv) < 3 : 
        print_usage(sys.argv[0] or 'me')
        sys.exit(1)
    conf_py   = sys.argv[2]
    execfile(conf_py)
    execfile('pproc_jlabff.py')

def runme_bin_unbias_c2pt(cfgkey_list, had, sm_srcsnk,
                ama_list=ama_list,
                tpol_list=c2pt_tpol_list,
                attrs_kw={},
                VERBOSE=False) :

    attrs_kw1   = dict(attrs_kw)
    attrs_kw1.update(tpol_list=tpol_list)

    h5_kpath_in = "/disc2pt/time_pos"
    h5_kpath_out= "/c2pt/%s/%s" % (sm_srcsnk, had)
    # check of tpol
    had_tpol_list = [ "%s_%s" % (had, tpol) for tpol in tpol_list ]
    def check_had_tpol(ll) :
        for f,k in ll :
            h5f = h5py.File(f, 'r')
            assert(list(h5f[k].attrs['had_list']) == had_tpol_list)
            h5f.close()

    # bin ex
    if ama_ex in ama_list :
        h5_list_ex  = [ (get_c2pt_h5_file(c, ama_ex),
                        '/cfg%s/%s' % (cfgreflkey_to_cfgkey(c), h5_kpath_in))
                        for c in cfgkey_list ]
        check_had_tpol(h5_list_ex)
        h5_file_ex  = get_c2pt_bin_h5_file(ama_ex, had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_ex))
        lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath_out, h5_list_ex, 
                attrs_kw=attrs_kw1, VERBOSE=VERBOSE)

    # bin sl
    if ama_sl in ama_list :
        h5_list_sl  = [ (get_c2pt_h5_file(c, ama_sl),
                        '/cfg%s/%s' % (cfgreflkey_to_cfgkey(c), h5_kpath_in))
                        for c in cfgkey_list ]
        check_had_tpol(h5_list_sl)
        h5_file_sl  = get_c2pt_bin_h5_file(ama_sl, had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_sl))
        lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath_out, h5_list_sl, 
                attrs_kw=attrs_kw1, VERBOSE=VERBOSE)

    # unbias
    if ama_ex in ama_list and ama_sl in ama_list :
        h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
        h5_file_ub  = get_c2pt_unbias_h5_file(had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_ub))
        lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath_out, h5_list_ub, 
                attrs_kw=attrs_kw1, VERBOSE=VERBOSE)

##############################################################################
def runme_calc_save_unbias_opdisc(cfgkey, bbql_nhada, 
                had, sm_srcsnk,
                tsep_max=opdisc_tsep_max,
                psnk_list=c3pt_psnk_list,
                qext_list=c3pt_qext_list,
                srctype_list=c3pt_srctype_list,
                snktype_list=c3pt_snktype_list,
                tpol_list=c3pt_tpol_list,
                op_list=c3pt_op_list,
                attrs_kw={}) :
    
    for (op, psnk) in it.product(
            op_list, psnk_list) :
        ir_list = lhpd.latcorr.op_default_ir_list(op)

        # input c2pt
        c2pt_sl_h5file  = get_disc2pt_h5_file(cfgkey, 'sl')
        c2pt_sl_h5f     = h5py.File(c2pt_sl_h5file, 'r')
        c2pt_ex_h5file  = get_disc2pt_h5_file(cfgkey, 'ex')
        c2pt_ex_h5f     = h5py.File(c2pt_ex_h5file, 'r')
        c2pt_kpath      = "/cfg%s/disc2pt/time_pos" % (cfgreflkey_to_cfgkey(cfgkey),)
        # input bb-qloop
        bbql_h5file = get_bbqloop_h5_file(cfgkey, bbql_nhada)
        bbql_h5f    = h5py.File(bbql_h5file, 'r')
        bbql_kpath  = '/'
        # output
        opdisc_sl_h5file= get_opdisc_bin_h5_file(cfgkey, bbql_nhada, 'sl', had, sm_srcsnk, psnk, op)
        lhpd.mkpath(os.path.dirname(opdisc_sl_h5file))
        opdisc_sl_h5f   = h5py.File(opdisc_sl_h5file, 'a')

        opdisc_ub_h5file= get_opdisc_bin_h5_file(cfgkey, bbql_nhada, 'ub', had, sm_srcsnk, psnk, op)
        lhpd.mkpath(os.path.dirname(opdisc_ub_h5file))
        opdisc_ub_h5f   = h5py.File(opdisc_ub_h5file, 'a')
        print("# <<< %s[%s]" % (c2pt_ex_h5file, c2pt_kpath))
        print("# <<< %s[%s]" % (c2pt_sl_h5file, c2pt_kpath))
        print("# <<< %s[%s]" % (bbql_h5file, bbql_kpath))

        for ir_name, ir_scale in ir_list :
            opdisc_h5kpath  = "/cfg%s/opdisc/%s/%s/%s/%s/%s" % (
                    cfgkey, sm_srcsnk, had, op, ir_name, psnk_str_k(psnk))
            c2ptavg_h5kpath  = "/cfg%s/c2ptavg/%s/%s/%s" % (
                    cfgkey, sm_srcsnk, had, psnk_str_k(psnk))
            opqloop_h5kpath  = "/cfg%s/opqloop/%s/%s" % (
                    cfgkey, op, ir_name)
            print("# >>> %s[%s]" % (opdisc_sl_h5file, opdisc_h5kpath))
            print("# >>> %s[%s]" % (opdisc_ub_h5file, opdisc_h5kpath))

            calc_save_unbias_opdisc(
                        opdisc_sl_h5f, opdisc_h5kpath,
                        opdisc_ub_h5f, opdisc_h5kpath,
                        latsize,
                        c2pt_sl_h5f[c2pt_kpath], c2pt_ex_h5f[c2pt_kpath],
                        bbql_h5f[bbql_kpath],
                        psnk, qext_list,
                        srctype_list, snktype_list, tpol_list, 
                        tsep_max, tsep_max,
                        op, ir_name, 
                        c2ptavg_sl_h5key=c2ptavg_h5kpath, opqloop_sl_h5key=opqloop_h5kpath,
                        c2ptavg_ub_h5key=c2ptavg_h5kpath, opqloop_ub_h5key=opqloop_h5kpath,
                        attrs_kw=attrs_kw)
        
        opdisc_sl_h5f.flush()
        opdisc_sl_h5f.close()
        
        opdisc_ub_h5f.flush()
        opdisc_ub_h5f.close()



def runme_merge_opdisc(cfgkey_list, n_hada, ama, had, sm_srcsnk,
                psnk_list=c3pt_psnk_list,
                op_list=c3pt_op_list,
                merge_c2ptavg=False,
                merge_opqloop=False,
                attrs_kw={}) :
        
    n_data  = len(cfgkey_list)
    for (op, psnk) in it.product(
            op_list, psnk_list) :
        ir_list = lhpd.latcorr.op_default_ir_list(op)

        out_h5file = get_opdisc_all_h5_file(n_hada, ama, had, sm_srcsnk, psnk, op)
        lhpd.mkpath(os.path.dirname(out_h5file))
        print(out_h5file)
        out_h5f = h5py.File(out_h5file, 'a')
        out_h5d = None
        out_opqloop_h5d = None
        out_c2ptavg_h5d = None
        for ir_name, ir_scale in ir_list :
            h5key   = "opdisc/%s/%s/%s/%s/%s" % (
                        sm_srcsnk, had, op, ir_name, psnk_str_k(psnk))
            c2ptavg_h5key  = "c2ptavg/%s/%s/%s" % (
                    sm_srcsnk, had, psnk_str_k(psnk))
            opqloop_h5key  = "opqloop/%s/%s" % (op, ir_name)
            for i_c, cfgkey in enumerate(cfgkey_list) :
                print("# %s" % cfgkey)
                opdisc_h5file= get_opdisc_bin_h5_file(cfgkey, n_hada, ama, had, sm_srcsnk, psnk, op)
                opdisc_h5f  = h5py.File(opdisc_h5file, 'r')
                # opdisc
                opdisc_h5d  = opdisc_h5f["/cfg%s/%s" % (cfgkey, h5key)]
                if None is out_h5d :
                    out_h5d = out_h5f.require_dataset(h5key, (n_data,) + opdisc_h5d.shape,
                                opdisc_h5d.dtype, fletcher32=True)
                    lhpd.h5_io.h5_copy_attr(out_h5d, opdisc_h5d,
                            [ 'tpol_list', 'sink_mom', 'qext_list', 'tsep_list', 'tau_list', 'comp_list',
                              'srctype_list', 'snktype_list', 'tpol_list' ],
                            check_key=True)
                    out_h5d.attrs['dim_spec'] = ['i_data', 'i_srctype', 'i_snktype', 'i_tpol', 'i_tsep', 
                                                 'i_comp', 'i_qext', 'i_tau']
                out_h5d[i_c] = opdisc_h5d

                # opqloop
                if merge_opqloop :
                    opqloop_h5d  = opdisc_h5f["/cfg%s/%s" % (cfgkey, opqloop_h5key)]
                    if None is out_opqloop_h5d :
                        out_opqloop_h5d = out_h5f.require_dataset(opqloop_h5key, 
                                (n_data,) + opqloop_h5d.shape, opqloop_h5d.dtype, fletcher32=True)
                        lhpd.h5_io.h5_copy_attr(out_opqloop_h5d, opqloop_h5d,
                                [ 'qext_list', 'tau_list', 'comp_list' ], 
                                check_key=True)
                        out_opqloop_h5d.attrs['dim_spec'] = ['i_data', 'i_comp', 'i_qext', 'i_tau']
                    out_opqloop_h5d[i_c] = opqloop_h5d
                
                # c2ptavg
                if merge_c2ptavg :
                    c2ptavg_h5d  = opdisc_h5f["/cfg%s/%s" % (cfgkey, c2ptavg_h5key)]
                    if None is out_c2ptavg_h5d :
                        out_c2ptavg_h5d = out_h5f.require_dataset(c2ptavg_h5key, 
                                    (n_data,) + c2ptavg_h5d.shape, c2ptavg_h5d.dtype, fletcher32=True)
                        lhpd.h5_io.h5_copy_attr(out_c2ptavg_h5d, c2ptavg_h5d,
                                [ 'tpol_list', 'sink_mom', 'tsep_list', 'srctype_list', 'snktype_list' ],
                                check_key=True)
                        out_c2ptavg_h5d.attrs['dim_spec'] = ['i_data', 'i_srctype', 'i_snktype', 'i_tpol', 'i_tsep' ]
                    out_c2ptavg_h5d[i_c] = c2ptavg_h5d



        out_h5f.flush()
        out_h5f.close()


if '__main__' == __name__ : 
    what   = sys.argv[1]
    jobid_list      = sys.argv[3:]

    if 'conv_qloop_sdbh5_h5' == what :
        for c in jobid_list :
            for n_hada in qloop_nhada_list :
                print(c, n_hada)
                conv_qloop_sdbh5_h5(c, n_hada, latsize, t_axis, qloop_qext_list, qloop_lpath_list)

    elif 'calc_opdisc' == what :
        if len(jobid_list) <= 0 :
            print("no jobid list")
        for c in jobid_list : 
            for bbql_nhada in qloop_nhada_list :
                print(c, bbql_nhada)
                runme_calc_save_unbias_opdisc(c, bbql_nhada, 'proton', 'SS')

    elif 'bin_unbias_c2pt' == what :
        print("# nconf=%d " % (len(cfg_list)))
        runme_bin_unbias_c2pt(cfg_list, 'proton', 'SS', ama_list=ama_list)

    elif 'merge_opdisc' == what :
        wl  = list(it.product(c3pt_op_list, c3pt_psnk_list, qloop_nhada_list))
        print("# len(wl) =", len(wl))
        for i, w in enumerate(wl) : print(i, w)

        for j_str in jobid_list :
            j   = int(j_str)
            assert(0 <= j and j < len(wl))
            print(j, ':', wl[j])
            op, psnk, n_hada = wl[j]

            for ama in ['ub'] : # only the necessary
                runme_merge_opdisc(cfg_list, n_hada, ama, 'proton', 'SS',
                        op_list=[op], psnk_list=[psnk])

    else : raise ValueError(what)
    
