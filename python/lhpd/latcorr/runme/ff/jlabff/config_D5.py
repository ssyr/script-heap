from past.builtins import execfile
import os, sys
import numpy as np
import h5py
import lhpd
from lhpd.misc.strkey import *


latsize = np.r_[32,32,32,64]
t_axis  = len(latsize) - 1

ama_sl  = 'sl'
ama_ex  = 'ex'
ama_list = [
    ama_ex,
    ama_sl ]

qloop_tpol_list = [ 'pos_I', 'pos_A1', 'pos_A2', 'pos_A3']
opdisc_tpol_list= qloop_tpol_list
srctype_list    = ['src9']
snktype_list    = ['sink_9.0']

qloop_nhada_list= [ 32, 64, 128, 256 ]
qloop_lpath_min, qloop_lpath_max=0,1
qloop_lpath_list = lhpd.latcorr.make_linkpath_list(qloop_lpath_min, qloop_lpath_max, dim=4)
qloop_qext_list = lhpd.normsorted(list(lhpd.int_vectors_maxnorm(3, 10)))
c3pt_qext_list  = qloop_qext_list
c3pt_psnk_list  = [[0,0,0]]
c3pt_tpol_list  = qloop_tpol_list
c3pt_srctype_list = srctype_list
c3pt_snktype_list = snktype_list
#c3pt_op_list    = ['tensor0', 'pstensor0', 'tensor1', 'pstensor1']
#c3pt_op_list    = ['tensor1']
c3pt_op_list    = ['tensor0', 'pstensor0', 'tensor1', 'pstensor1', 'sigma2a2'] # add others
opdisc_tsep_max = 18

def lpath_str_sdbh5(lp, dim=4):
    #p3-10-d4-re
    def mu2sdbh5(m) :
        if m < 0 : raise ValueError
        elif m < dim : return -1 - m
        elif m < 2*dim : return 1 + m - dim
        else : raise ValueError
    if len(lp) <= 0 : return "d0"
    else : return "d%s" % (''.join(['%d' % mu2sdbh5(m) for m in lp ]))
def qext_str_sdbh5(qext) :
    return "p%s" % (''.join(["%d" % qi for qi in qext]))


data_sdbh5  = 'data_sdbh5'
data_out    = 'data_out'
data_cache  = 'data_cache'
execfile('config_filenames.py')


cfg_list= lhpd.read_key_list('list.cfg', 'samples')

