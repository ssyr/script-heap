# JLab form factors project
# this script provides examples on how to read data from stripped HDF5 files
# also, it reads matrix el. of spatial vec.current for smallest non-zero momenta
#   and computes SNRs for  them

from past.builtins import execfile
import matplotlib as mpl ; mpl.use('agg') ; import matplotlib.pyplot as plt
import lhpd
import dataview as dv

# load data set parameters
execfile('config_D5.py') ; execfile('ens_setup.py')

# some constants to save keystrokes
mlat=0.465              # \pm0.005, from fit t=4--15
op='tensor1'            # code name for vector current 
ir_name='H4_T1_d4r1'    # H(4) irrep for the operator (only have one)
flav='U+D'              # or 'D', does not matter for disc.ffs
rsplan=('jk',1)         # resampling plan : jackknife, no binning
n_hada=256

# dict of parameters
gg_param_kw=dict(mlat=mlat, latsize=latsize, rsplan=rsplan, ama='ub', had='proton', data_top=data_out, c3pt_contr='disc', twopt_avg_rot=True, n_hada=n_hada, tskip=3)

gg=ens_data_get(op=op, flav=flav, **gg_param_kw)

# list of source/sink smearings and separations which are used to extract 
# matrix elements from 2- and 3-point functions
# TODO perhaps SP correlators?
tsep_list=[8,10,12]     
ssgrp=[ srcsnkpair(dt, 'SS') for dt in tsep_list ]

# list of 3-momenta for vertex  momentum projection
qext_list=np.array(list(lhpd.int_vectors_maxnorm(3, 10)))

# dictionary matching Q^2(momentum transfer squared) to a list of pairs of 
# in/out momenta :{ <Q2(float)> : [ (psrc, psnk), ... ], ... }
p0p1_dict=lhpd.latcorr.make_mom_combo_list(.7, latsize, [[0,0,0]], qext_list)
# list of standard in-momenta
p0_list = [ p0p1_dict[k][0][0] for k in sorted(p0p1_dict.keys()) ]

# generate list of "kinemtics" (psrc,psnk,tpol) for extracting form factors
# no need to be smart here - the solver will select non-zero equations automatically
tpol_list=['pos_I', 'pos_A1', 'pos_A2', 'pos_A3']
tpol_c2 = 'pos_I'   # need to set the polarization for the 2pt in the ratio denominator
# list of kinematics for the 2pt fitter; only psrc is used
mc_list_c2fit = [ mc_case(p, [0,0,0], tpol_c2, tpol_c2) for p in p0_list ]

mcgrp_list = [ [ mc_case(p0, p1, tpol, tpol_c2) for p0,p1 in p0p1_dict[k] for tpol in tpol_list ] for k in sorted(p0p1_dict.keys()) ]

# testing read for 2pt
mc_std_list = [ mcgrp[0] for mcgrp in mcgrp_list ]  # same, from older version
x=gg.get_twopt_src(mc_std_list[0], ssgrp[0]); print(x.shape)

# testing read for 3pt
x=gg.get_threept(mc_std_list[0], ssgrp[0], op, ir_name) ; print(x.shape)

"""
# runme_fit_c2pt
execfile('fit_c2.py')
h5f_c2fit = h5py.File('c2fit.unbias.proton.SS.h5', 'a')

t1=18
tr_list = [np.r_[t0:t1] for t0 in [4,5,6,7]]

c2fit_kw=dict(rsplan=('jk',1), c2fit_nexp=2, method='nlm')
h5g_c2fit=h5f_c2fit.require_group('/c2fit/SS/proton')

for tr in tr_list :
    runme_fit_save_c2pt(h5g_c2fit, gg, tr, mc_list_c2fit, ssgrp[0], **c2fit_kw)
h5f_c2fit.flush()

ax=runme_plot_fit_c2pt(h5g_c2fit, tr_list, mom_list, xlim=[0,20], xl=r'$t$', ylim=[0,1], yl=r'$m_{eff}$', ax=dv.class_logger(dv.make_std_axes()), **c2fit_kw)

name='meff.unbias.proton.SS'
dv.save_figdraw(name + '.plt.py', x=ax)
ax.figure.savefig(name + '.pdf')
"""


