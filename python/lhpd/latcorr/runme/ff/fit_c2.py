from __future__ import print_function
import math, numpy as np, itertools as it
import tabulate
import h5py
import pickle, json

import lhpd
from lhpd import dictnew, jsonify
import matplotlib as mpl
import matplotlib.pyplot as plt
import dataview as dv
from dataview import latexize

##############################################################################
# c2pt fits
##############################################################################
def make_energy_prior_simple(c2exp_fv, *prior_param, **kw) :
    pfx = kw.get('pname_prefix', '')
    pr_list = []
    for p in c2exp_fv.pname :
        if p.startswith('%se' % pfx) or p.startswith('%sde' % pfx) :
            pr_list.append((p,) + tuple(prior_param))
    return lhpd.fitter.prior_simple(*pr_list)

def make_cfactor_prior_simple(c2exp_fv, *prior_param) :
    pr_list = []
    for p in c2exp_fv.pname :
        if p.startswith('c2pt_c') :
            pr_list.append((p,) + tuple(prior_param))
    return lhpd.fitter.prior_simple(*pr_list)

def make_logdetvdm_prior(c2exp_fv, nexp) : 
    """ -2\sum{i \ne j} TODO """ 
    # TODO
    return 

def fit_save_c2pt(
        h5g_out,    # output
        gg,         # input
        tr, mc, ss,
        **kw) :
    rsplan      = kw['rsplan']
    c2fit_nexp  = kw['c2fit_nexp']
    method      = kw['method']
    opt_kw      = kw.get('opt_kw') or {}
    disp_ens    = kw.get('disp_ens') or 0
    VERBOSE     = kw.get('VERBOSE') or 0
    ncov_evmin  = kw.get('ncov_evmin', 0)
    log         = kw.get('log')
    if None is log : log = lhpd.log(level=VERBOSE)

    c2_ens      = gg.get_twopt_src(mc, ss).real[:, tr]
    c2_scale    = lhpd.calc_avg(c2_ens[:,0])
    assert(0. < c2_scale)
    log.echo(1, ("*** ", mc, ss, (tr.min(), tr.max())) )
    log.echo(1, "c2_scale=%e" % c2_scale)

    # resampled rescaled ensemble
    c2r_ens_r   = lhpd.resample(c2_ens / c2_scale, rsplan)
    c2r_a, c2r_e, c2r_ncov = lhpd.calc_avg_err_ncov(c2r_ens_r, rsplan)
    c2r_ncov_ev = np.linalg.eigvalsh(c2r_ncov)
    c2r_ncov_ev.sort()
    c2r_ncov_dim = c2r_ncov.shape[0]
    log.echo(1, "c2r_ncov[%d,%d] evals = %e .. %e" % (c2r_ncov.shape + (
            c2r_ncov_ev.min(), c2r_ncov_ev.max())))
    c2r_ncov = lhpd.ncov_matrix_set_evmin(c2r_ncov, ncov_evmin)
    c2r_ncov_ev = np.linalg.eigvalsh(c2r_ncov)
    log.echo(1, "c2r_ncov[%d,%d] evals2= %e .. %e" % (c2r_ncov.shape + (
            c2r_ncov_ev.min(), c2r_ncov_ev.max())))

    c2exp_mf    = lhpd.fitter.legacy2modelfunc(lhpd.pyfit.Nexp_ladder_c2pt(c2fit_nexp))
    # XXX make sure that the order in `p_scale', `p0' corresponds to c2exp_mf
    p_scale     = np.array([ c2_scale, 1. ] * c2fit_nexp) 
    p0  = np.asarray([1., 1.] * c2fit_nexp)   # all parameters are of order 1 because of rescaling
    c2exp_fv    = lhpd.fitter.datafit(c2exp_mf, tr, c2r_a, c2r_e, c2r_ncov)

    # XXX make sure that the order in `reparam' corresponds to c2exp_mf
    c2exp_fit   = lhpd.fitter.fitter2(
                        lhpd.fitter.datafit_join([], c2exp_fv, 
                                make_energy_prior_simple(c2exp_fv, 'lognormal', .5, 2.)),
                        reparam=lhpd.fitter.reparam(
                                *([('range', 0., None), ('range', .0, None) ] * c2fit_nexp) ))

    res = c2exp_fit.fit(p0, method=method, opt_kw=opt_kw)
    popt, pcov, chi2opt = res
    chi2_2 = c2exp_fit.chi2(popt)
    #print("params of fit to avg(c2pt) ", res)
    log.echo(1, "  chi2/dof = %f / %d = %f" % (chi2opt, c2exp_fit.ndof(), chi2opt / c2exp_fit.ndof()))
    
    n_data      = len(c2r_ens_r)
    p_r     = np.empty((n_data, c2exp_mf.plen()), np.float64)
    chi2_r  = np.empty((n_data,), np.float64)
    for i_data, c2r in enumerate(c2r_ens_r) :
        opt_kw1         = dict(opt_kw)
        opt_kw1['disp'] = disp_ens
        c2exp_fv.set_all(xv=tr, ya=c2r, ys=c2r_e, ycov=c2r_ncov)
        p_i, pcov_i, chi2_i = c2exp_fit.fit(p0=p0,
                    method=method, opt_kw=opt_kw1)
        p_r[i_data]     = p_i * p_scale
        chi2_r[i_data]  = chi2_i

    p_a, p_e, p_cov = lhpd.calc_avg_err_ncov(p_r, rsplan)
    lhpd.pprint_ve_ncov(p_a, p_e, p_cov, title=c2exp_mf.pname)
    if not None is h5g_out :
        lhpd.h5_io.h5_clean_group(h5g_out)
        # XXX all saved data is resampled with attrs['rsplan']
        h5g_out['xr']           = tr
        h5g_out['fit_data']     = c2r_ens_r * c2_scale
        h5g_out['fit_data_a']   = c2r_a     * c2_scale
        h5g_out['fit_data_e']   = c2r_e     * c2_scale
        h5g_out['popt']         = popt
        h5g_out['p']            = p_r
        h5g_out['p_a']          = p_a
        h5g_out['p_e']          = p_e
        h5g_out['p_cov']        = p_cov
        h5g_out['chi2opt']      = chi2opt

        #h5g_out.attrs['rsplan'] = lhpd.rsplan_str(rsplan)
        lhpd.h5_io.h5_set_rsplan(h5g_out, rsplan)
        h5g_out.attrs['pname']  = c2exp_fv.pname
        h5g_out.attrs['ndof']   = c2exp_fv.ndof()
    return p_a, p_e, p_cov

def runme_fit_save_c2pt(
        h5g_out_top,    # output
        gg,         # input
        tr, mc_list, ss,
        **kw) :
    vrb = kw.get('VERBOSE', False)
    for mc in mc_list :
        #h5k = "tr%d--%d" % (tr.min(), tr.max())
        psnk= mc.get_p3src()
        h5k = "tr%d--%d/px%dpy%dpz%d" % ((tr.min(), tr.max()) + tuple(psnk))
        h5g = h5g_out_top.require_group(h5k)
        res=fit_save_c2pt(h5g, gg, tr, mc, ss, **kw) 


#########################################
def mk_prior_get_c2fit_param(pname, dat_d, lc_d) :
    """ get prior params from c2fit """
    h5fname, h5kpath = get_lcdb_loc(dat_d, lc_d)
    h5f         = h5py.File(h5fname, 'r')
    h5g         = h5f[h5kpath]
    pname_all   = list(h5g.attrs['pname'])
    idx         = np.array([ pname_all.index(p) for p in pname ])
    popt        = h5g['popt'][()][idx]
    p_e         = h5g['p_e'][()][idx]
    h5f.close()
    return popt, p_e

def mk_prior_c2fit(c2exp_fv, **kw) :
    """ make prior for c2fit 
    """
    log         = kw['log']
    prior       = kw.get('prior')
    if   None is prior : return None
    elif 'regular' == prior :
        log.echo(2, '*** prior=%s' % str(prior))
        return make_energy_prior_simple(c2exp_fv, 'lognormal', .5, 2.)
    elif isinstance(prior, dict) :
        log.echo(2, '*** prior=%s' % str(prior))
        if   'direct' == prior['kind'] :
            param = prior['param']
            if isinstance(param, tuple) : param = [ param ]
            return lhpd.fitter.prior_simple(*param)
        elif 'c2fit' == prior['kind'] :
            c2fit_d     = prior['c2fit_d']
            pname       = prior['pname']
            if isinstance(pname, str) : pname = pname.split(',')
            scale       = np.array(prior['scale'])
            prior_dat_d = prior.get('dat_d') or kw['dat_d'] 
            prior_c2_d  = prior.get('c2_d') or kw['c2_d']
            prior_lc_d  = dict(kind='c2fit', c2fit_d=c2fit_d, c2_d=prior_c2_d)
            popt, p_e   = mk_prior_get_c2fit_param(pname, prior_dat_d, prior_lc_d)
            assert(len(popt) == len(pname))
            assert(len(p_e) == len(pname))
            p_e *= scale
            assert(len(p_e) == len(pname))
            prior_opt   = [ (n, 'normal', v, e) for n,v,e in zip(pname, popt, p_e) ]
            log.echo(1, "*** prior_c2fit = %s" % str(prior_opt))
            return lhpd.fitter.prior_simple(*prior_opt) 
        else : raise NotImplementedError
    # TODO add other priors
    elif isinstance(prior, list) : 
        return [ mk_prior_c2fit(c2exp_fv, **dictnew(kw, prior=p)) for p in prior ]
    else : raise TypeError(prior)



def mk_c2fit_tr(trange) :
    """ naming struggle: will use 'tr' for an array of t-values, 
        'trange' for range specification in c2fit_d etc
    """
    if isinstance(trange, tuple) :
        assert(2 == len(trange))
        return np.r_[ trange[0] : trange[1] + 1 ]
    elif isinstance(trange, list) or isinstance(trange, np.ndarray) :
        return np.array(trange)
    else : raise TypeError(trange)

def mk_c2fit_log(**kw) :
    log         = kw.get('log')
    VERBOSE     = kw.get('VERBOSE', 1)
    if   None is log : 
        return lhpd.log(level=VERBOSE)
    elif True is log :
        out_h5fname, out_h5kpath = get_lcdb_loc(kw['dat_d'], 
                dict(kind='c2fit', c2fit_d=kw['c2fit_d'], c2_d=kw['c2_d']))
        log_fname =  "%s.LOG" % out_h5fname
    elif isinstance(log, str) : 
        log_fname = log
    else : return log

    log_    = lhpd.log(level=VERBOSE)
    log_.add_stream(log_fname)
    return log_

    
def fit_save_c2pt_v2(
        **kw) :
    dat_d       = kw['dat_d']
    in_dat_d    = kw.get('in_dat_d', dat_d)
    c2fit_d     = kw['c2fit_d']
    c2_d        = kw['c2_d']
    gg_opt      = kw['gg_opt']

    rsplan      = kw['rsplan']
    method      = kw['method']
    ncov_evmin  = kw.get('ncov_evmin', 0)
    VERBOSE     = kw.get('VERBOSE', 1)
    log         = mk_c2fit_log(**kw) ; kw['log'] = log
    opt_kw      = kw.get('opt_kw') or {}
    disp_ens    = kw.get('disp_ens') or 0
    do_chi2_r   = kw.get('do_chi2_r', False)

    # output
    out_h5fname, out_h5kpath = get_lcdb_loc(dat_d, dict(kind='c2fit', c2fit_d=c2fit_d, c2_d=c2_d))
    out_h5f = h5py.File(out_h5fname, 'a')
    out_h5g = out_h5f.require_group(out_h5kpath)

    # input
    gg = ens_data_get(**dictnew(gg_opt, data_in=in_dat_d['data_dir']))
    # FIXME project-specific mc_case, srcsnkpair
    mc1         = mc_case(c2_d['had'], c2_d['tpol'], c2_d['psnk'], 
                      c2_d['had'], c2_d['tpol'], c2_d['psnk'])
    ss1         = srcsnkpair(-1, c2_d['smsrcsnk'])

    # data load, resample
    tr          = mk_c2fit_tr(c2fit_d['trange'])
    c2_ens      = gg.get_twopt_src(mc1, ss1).real[:, tr]
    c2_scale    = lhpd.calc_avg(c2_ens[:,0])
    assert(0. < c2_scale)
    log.echo(1, "--------------------------------")
    log.echo(1, (mc1, ss1, (tr.min(), tr.max()) ))
    log.echo(2, "*** tr=%s  c2_scale=%e" % (str(tr), c2_scale))
    
    if do_chi2_r : 
        c2r_ens_r, c2r_ens_cov   = lhpd.resample_cov(c2_ens, rsplan)
    else : c2r_ens_r = lhpd.resample(c2_ens, rsplan)
    n_data      = len(c2r_ens_r)
    c2r_a, c2r_e, c2r_ncov = lhpd.calc_avg_err_ncov(c2r_ens_r, rsplan)
    c2r_ncov = lhpd.ncov_matrix_set_evmin(c2r_ncov, ncov_evmin)
    c2r_ncov_ev = np.linalg.eigvalsh(c2r_ncov)
    c2r_ncov_ev.sort()
    c2r_ncov_dim = c2r_ncov.shape[0]
    log.echo(2, "*** c2r_ncov[%d,%d] evals = %e .. %e" % (c2r_ncov.shape + (
            c2r_ncov_ev.min(), c2r_ncov_ev.max())))

    # select model
    c2fit_kind  = c2fit_d['kind']
    if 'nexp'   == c2fit_kind :
        c2fit_nexp  = c2fit_d['nexp']

        p0  = np.asarray([c2_scale, 1.] * c2fit_nexp)   # all parameters are of order 1 because of rescaling

        c2exp_mf    = lhpd.fitter.legacy2modelfunc(lhpd.pyfit.Nexp_ladder_c2pt(c2fit_nexp))
        c2exp_fv    = lhpd.fitter.datafit(c2exp_mf, tr, c2r_a, c2r_e, c2r_ncov)

        prior_fv    = mk_prior_c2fit(c2exp_fv, **kw)
        log.echo(1, "  prior_fv = %s" % str(prior_fv))
        if None is prior_fv : c2exp_fv_all = c2exp_fv
        elif isinstance(prior_fv, list) : c2exp_fv_all = lhpd.fitter.datafit_join([], c2exp_fv, *prior_fv)
        else : c2exp_fv_all = lhpd.fitter.datafit_join([], c2exp_fv, prior_fv)

        reparam=lhpd.fitter.reparam(
                    *([('range', 0., None), ('range', .0, None) ] * c2fit_nexp) )
        c2exp_fit   = lhpd.fitter.fitter2(c2exp_fv_all, reparam=reparam)
        c2fit_ndof_tot = c2exp_fit.ndof()
        c2fit_ndof  = c2exp_fv.ndof()

    else : raise ValueError(c2fit_kind)  # TODO add other models


    res = c2exp_fit.fit(p0, method=method, opt_kw=opt_kw)
    popt, pcov, chi2opt_tot = res

    if isinstance(c2exp_fv_all, lhpd.fitter.datafit_join) : 
        chi2_m = c2exp_fv_all.chi2_m(popt)
        log.echo(1, "  chi2: tot=%.2f  model=%.2f  priors=[%s]" % (
                sum(chi2_m), chi2_m[0], ', '.join(['%.2f' % c for c in chi2_m[1:]])))
    else : log.echo(1, "  chi2=%f  ndof=$d" % (c2exp_fv_all.chi2(popt), c2exp_fv_all.ndof()))

    chi2opt     = c2exp_fv.chi2(popt)       # no priors
    log.echo(1, "  popt=%s" % str(popt))
    log.echo(1, "  chi2=%.2f(ndof=%d)  ndata=%d" % (
            chi2opt, c2fit_ndof, n_data))

    pval_chi2   = sp.stats.chi2.sf(chi2opt, c2fit_ndof)
    pval_ht2     = sp.stats.f.sf(chi2opt * float(n_data - c2fit_ndof) / float(c2fit_ndof * (n_data - 1)), 
                                c2fit_ndof, n_data - c2fit_ndof)
    p_r         = np.empty((n_data, c2exp_mf.plen()), np.float64)
    # for empirical ensemble CDF
    c2r_opt     = c2exp_mf.fv(tr, popt)     
    chi2_r      = np.empty((n_data,), np.float64)
    for i_data, c2r_i in enumerate(c2r_ens_r) :
        opt_kw1         = dict(opt_kw)
        opt_kw1['disp'] = disp_ens
        c2exp_fv.set_all(xv=tr, ya=c2r_i, ys=c2r_e, ycov=c2r_ncov)
        p_i, pcov_i, chi2_i = c2exp_fit.fit(p0=popt,
                    method=method, opt_kw=opt_kw1)
        p_r[i_data]     = p_i

        # compute empirical pvalue distrib
        if do_chi2_r :
            c2r_cov_i       = np.array(c2r_ens_cov[i_data])
            c2r_e_i         = lhpd.norm_cov_matrix(c2r_cov_i)
            c2exp_fv.set_all(xv=tr, ya=c2r_i - c2r_a + c2r_opt, ys=c2r_e_i, ycov=c2r_cov_i)
            p_i1, pcov_i1, chi2_i1 = c2exp_fit.fit(p0=popt, method=method, opt_kw=opt_kw1)
            chi2_r[i_data]  = c2exp_fv.chi2(p_i1)

    if do_chi2_r : 
        pval_emp = 1. - lhpd.ordered_cdf(chi2opt, chi2_r)
        log.echo(1, "  pval_chi2=%.4f pval_ht2=%.4f pval_emp=%.4f" % (pval_chi2, pval_ht2, pval_emp))
    else : log.echo(1, "  pval_chi2=%.4f pval_ht2=%.4f" % (pval_chi2, pval_ht2))

    p_a, p_e, p_cov = lhpd.calc_avg_err_ncov(p_r, rsplan)
    if 1 <= VERBOSE :
        lhpd.pprint_ve_ncov(p_a, p_e, p_cov, title=c2exp_mf.pname, fo=log)
    fit_func_opt = c2exp_mf.fv(tr, popt)


    lhpd.h5_io.h5_clean_group(out_h5g)
    # XXX all saved data is resampled with attrs['rsplan']
    out_h5g['xr']           = tr
    out_h5g['fit_data']     = c2r_ens_r
    out_h5g['fit_data_a']   = c2r_a
    out_h5g['fit_data_e']   = c2r_e
    out_h5g['popt']         = popt
    out_h5g['p']            = p_r
    out_h5g['p_a']          = p_a
    out_h5g['p_e']          = p_e
    out_h5g['p_cov']        = p_cov
    out_h5g['chi2opt']      = chi2opt
    out_h5g['pvalue_chi2']  = pval_chi2
    out_h5g['pvalue_ht2']   = pval_ht2
    out_h5g['fit_func_opt'] = fit_func_opt
    if do_chi2_r : 
        out_h5g['chi2_r'] = chi2_r            # empirical chi2
        out_h5g['pvalue_emp'] = pval_emp      # empirical pvalue

    lhpd.h5_io.h5_set_rsplan(out_h5g, rsplan)
    #out_h5g.attrs['rsplan'] = lhpd.rsplan_str(rsplan)
    out_h5g.attrs['pname']  = c2exp_fv.pname
    out_h5g.attrs['ndof']   = c2fit_ndof
    out_h5g.attrs['n_data'] = n_data
    out_h5g.attrs['prior']  = json.dumps(jsonify(kw.get('prior')))

    out_h5f.flush()
    out_h5f.close()

    return p_a, p_e, p_cov



def fit_save_c2pt_exc_refine(
        h5g_out,    # output
        gg,         # input
        tmax, mc, ss,
        **kw) :
    rsplan  = kw['rsplan']
    c2fit_nexp= kw['c2fit_nexp']
    method  = kw['method']
    opt_kw  = kw.get('opt_kw') or {}
    disp_ens= kw.get('disp_ens') or 0
    pvthres = kw['pvthres']
    dt_min= 3  # TODO param?

    # reduce tmin starting from tmax-3
    tmin    = tmax - dt_min
    for nexp in range(1, 1 + c2fit_nexp) :
        val_r, pv_r = fit_c2pt_step()


################################################################################
# fit with lognormal priors
def fit_save_c2pt_prior_lnorm(h5g_out,
            gg, tr, mc, ss,
            cp_l, ep_l, e0, 
            **kw):
    # params
    rsplan      = kw['rsplan']
    model       = kw['model']
    method      = kw['method']
    pname_prefix= kw.get('pname_prefix') or ''
    opt_kw      = kw.get('opt_kw') or {}
    disp_ens    = kw.get('disp_ens') or 0
    ncov_evmin  = kw.get('ncov_evmin') or None
    p0          = kw.get('p0') or None
    # data
    tmax        = tr.max()
    c2_ens      = gg.get_twopt(mc, ss).real[:, tr]
    c2_scale    = lhpd.calc_avg(c2_ens[:,0])
    c2r_ens_r   = lhpd.resample(c2_ens / c2_scale, rsplan)
    n_data      = len(c2r_ens_r)
    c2r_a, c2r_e, c2r_ncov = lhpd.calc_avg_err_ncov(c2r_ens_r, rsplan)
    pnpfx       = pname_prefix

    if not None is ncov_evmin :
        c2r_ncov = lhpd.ncov_matrix_set_evmin(c2r_ncov, ncov_evmin)
    c2r_ncov_ev = np.linalg.eigvalsh(c2r_ncov)
    c2r_ncov_ev.sort()
    c2r_ncov_dim = c2r_ncov.shape[0]
    if 1 <= VERBOSE :
        print( "c2r_ncov[%d,%d] evals = %e .. %e" % (c2r_ncov.shape + (
            c2r_ncov_ev.min(), c2r_ncov_ev.max())))
    
    def print_opt_stat(d) :
        print("niter=%d  fcalls=%d  gcalls=%d  fcall_exc=%d trial_exc=%d" % (
            d['niter'], d['fcalls'], d['gcalls'], d['fcall_exc'], d['trial_exc']))

    c2fit_cpriors = []
    # fit classes
    if   'nexp' == model[0]:
        nexp        = model[1]
        c2exp_mf    = lhpd.fitter.legacy2modelfunc(lhpd.pyfit.Nexp_ladder_c2pt(
                nexp, pname_prefix=pnpfx))
        c2fit_cpriors = [
            c2fit_ladder_prior_lnorm(nexp, cp_l, ep_l, e0=e0, pname_prefix=pnpfx) ]
        p0x     = np.asarray([1., e0] * nexp)
        p_scale = np.array([ c2_scale, 1. ] * nexp)
    elif 'nexp_osc' == model[0] : 
        nexp        = model[1]
        c2exp_mf    = lhpd.fitter.legacy2modelfunc(lhpd.pyfit.Nexp_osc_ladder_c2pt(
                nexp, pname_prefix=pnpfx))
        c2fit_cpriors = [
            c2fit_ladder_prior_lnorm(nexp, cp_l, ep_l, e0=e0, pname_prefix=pnpfx),
            lhpd.fitter.prior_lnorm_rel0(['%se0' % pnpfx, '%sdeosc_0' % pnpfx], 1.) ]
        p0x     = np.asarray([1., e0] * nexp + [0., e0 ] )
        p_scale = np.array([ c2_scale, 1. ] * (1 + nexp))
    else :
        raise ValueError(model)

    if None is p0 : p0 = p0x

    c2exp_fv    = lhpd.fitter.datafit(c2exp_mf, tr, c2r_a, c2r_e, 
                          c2r_ncov)
    c2exp_fit   = lhpd.fitter.fitter2(lhpd.fitter.datafit_join([], c2exp_fv, *c2fit_cpriors))
    popt, pcov, chi2 = c2exp_fit.fit(p0, method=method, opt_kw=opt_kw)
    #print_opt_stat(c2exp_fit.opt_stat)

    p_r     = np.empty((n_data, c2exp_mf.plen()), np.float64)
    chi2_r  = np.empty((n_data,), np.float64)
    chi2f_r = np.empty((n_data,), np.float64)
    # scan ens
    for i_data, c2r in enumerate(c2r_ens_r) :
        opt_kw1         = dict(opt_kw)
        opt_kw1['disp'] = disp_ens
        c2exp_fv.set_all(xv=tr, ya=c2r, ys=c2r_e, 
                         ycov=c2r_ncov)
        p_i, pcov_i, chi2_i = c2exp_fit.fit(p0=popt,
                method=method, opt_kw=opt_kw1)
        #print_opt_stat(c2exp_fit.opt_stat)
        p_r[i_data]     = p_i * p_scale
        chi2_r[i_data]  = c2exp_fv.chi2(p_i)
        chi2f_r[i_data] = chi2_i # XXX chi2f includes the prior !!!
        #sys.stdout.write('.')
        #if 0 == (i_data+1) % 80 : sys.stdout.write('\n')
    #sys.stdout.write('\n')
    c2exp_fit.print_stat()
    p_a, p_e, p_cov = lhpd.calc_avg_err_ncov(p_r, rsplan)

    if not None is h5g_out :
        lhpd.h5_io.h5_clean_group(h5g_out)
        # XXX all saved data is resampled with attrs['rsplan']
        h5g_out['xr']           = tr
        h5g_out['fit_data']     = c2r_ens_r * c2_scale
        h5g_out['fit_data_a']   = c2r_a     * c2_scale
        h5g_out['fit_data_e']   = c2r_e     * c2_scale
        h5g_out['popt']         = popt
        h5g_out['p']            = p_r
        h5g_out['p_a']          = p_a
        h5g_out['p_e']          = p_e
        h5g_out['p_cov']        = p_cov
        h5g_out['chi2']         = chi2_r
        h5g_out['chi2f']        = chi2f_r

        h5g_out.attrs['rsplan'] = lhpd.rsplan_str(rsplan)
        h5g_out.attrs['ndof']   = c2exp_fit.ndof()
        h5g_out.attrs['pname']  = c2exp_fv.pname
        h5g_out.attrs['model']  = model
    return p_a, p_e, p_cov, c2exp_fit.fit_stat


def runme_fit_save_c2pt_prior_lnorm(
            h5g_out_top, gg, tr_list, mc_list, ss,
            cp_l, ep_l, e0,
            **kw) :
    rsplan      = kw['rsplan']
    vrb = kw.get('VERBOSE', False)
    fit_stat = []
    for mc in mc_list :
        psnk= mc.get_p3src()

        ndof_tr     = []    # [i_tr]
        p_tr_ae     = []    # [i_tr, {a|e}, i_p]
        chi2_tr_ae  = []    # [i_tr, {a|e}]
        chi2f_tr_ae = []    # [i_tr, {a|e}]    chi2 with prior contrib
        pname       = None
        # scan tr
        for tr in tr_list :
            print("runme_fit_save_c2pt_prior_lnorm: p=%s tr=%d-%d" % (psnk, tr.min(), tr.max()))
            h5k = "tr%d--%d/px%dpy%dpz%d" % ((tr.min(), tr.max()) + tuple(psnk))
            h5g = h5g_out_top.require_group(h5k)
            res = fit_save_c2pt_prior_lnorm(h5g, gg, tr, mc, ss, cp_l, ep_l, e0, **kw)
            h5g_out_top.file.flush()
            fit_stat.append(res[3])
            ndof_tr.append(h5g.attrs['ndof'])
            p_tr_ae.append(np.array([h5g['p_a'], h5g['p_e']]))
            chi2_tr_ae.append(lhpd.calc_avg_err(h5g['chi2'], rsplan))
            chi2f_tr_ae.append(lhpd.calc_avg_err(h5g['chi2f'], rsplan))
            if None is pname : pname = h5g.attrs['pname']

        # print summary
        if vrb :
            tr_tab      = [ '%2d-%2d' %(tr.min(), tr.max()) for tr in tr_list ]
            ndof_tab    = [ [n] for n in ndof_tr ]
            chi2_ve_tab = [ [ lhpd.spprint_ve(*ae) ] for ae in chi2_tr_ae ]
            chi2f_ve_tab= [ [ lhpd.spprint_ve(*ae) ] for ae in chi2f_tr_ae ]
            p_ve_tab    = [ [ lhpd.spprint_ve(*ae) for ae in p_ae.T ] 
                            for p_ae in p_tr_ae ]
            print('p=', psnk, lhpd.rsplan_str(rsplan))
            print(tabulate.tabulate(
                    lhpd.list_cat1(ndof_tab, chi2_ve_tab, chi2f_ve_tab, p_ve_tab), 
                    ['tr', 'ndof', 'chi2', 'chi2f'] + list(pname),
                    showindex=tr_tab, disable_numparse=True))
    return fit_stat

def plot_meff_c2pt(gg, tr, mc, ss, **kw) :
    ax      = kw.get('ax') or dv.make_std_axes()
    rsplan  = kw['rsplan']
    stg     = kw.get('stg') or dv.style_group_default[0]
    xsh     = kw.get('xsh') or kw.get('xshift') or 0.
    label   = kw.get('label') or None#("fit[%d:%d]" % tr.min(), tr.max())
    dslice  = kw.get('dslice', np.s_[:])
    c2      = gg.get_twopt_src(mc, ss).real[dslice]
    c2_r    = lhpd.resample(c2, rsplan)
    meff_r  = np.log(c2_r[:, tr] / c2_r[:, tr+1])
    meff_ae = lhpd.calc_avg_err(meff_r, rsplan)
        
    ax.errorbar(xsh + tr, meff_ae[0], yerr=meff_ae[1], label=label, **stg.edotline())

    xlim=ax.get_xlim()
    ax.plot(xlim, [0,0], 'k:')
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)

    return ax


    
# TODO separate plotting data and fit
def plot_meff_fit_c2pt(h5g, 
        **kw) :
    ax      = kw.get('ax') or dv.make_std_axes()
    rsplan  = kw['rsplan']
    model   = kw['model']
    stg     = kw.get('stg') or dv.style_group_default[0]
    xsh     = kw.get('xsh') or kw.get('xshift') or 0.
    xmargin_band= kw.get('xmargin_band') or 0.
    plot_data   = kw.get('plot_data', True)
    plot_fit    = kw.get('plot_fit', True)
    label_data  = kw.get('label_data') or None#("fit[%d:%d]" % tr.min(), tr.max())
    label_fit   = kw.get('label_fit') or None#("fit[%d:%d]" % tr.min(), tr.max())
    yfactor     = kw.get('yfactor') or 1.
    
    if 'nexp' == model[0] :
        c2fit_nexp = model[1]
        c2exp_mf = lhpd.fitter.legacy2modelfunc(lhpd.pyfit.Nexp_ladder_c2pt(c2fit_nexp))
    elif 'nexp_osc' == model[0] :
        c2fit_nexp = model[1]
        c2exp_mf = lhpd.fitter.legacy2modelfunc(lhpd.pyfit.Nexp_osc_ladder_c2pt(c2fit_nexp))
    else :
        raise ValueError(model)

    tr      = h5g['xr'][()]
    c2_r    = h5g['fit_data'][()]
    assert(len(tr) == c2_r.shape[1])
    meff_r  = np.where(tr[:-1]+1 == tr[1:], np.log(c2_r[:, :-1] / c2_r[:, 1:]), 0.)
    meff_ae = lhpd.calc_avg_err(meff_r, rsplan)

    tr_band = np.r_[tr.min() - xmargin_band : tr.max() + xmargin_band : 100j]
    p_r     = h5g['p'][()]
    c2m_r   = np.array([ c2exp_mf.fv(tr_band, p) for p in p_r ])
    c2mp1_r = np.array([ c2exp_mf.fv(1. + tr_band, p) for p in p_r ])
    meff_m_ae   = lhpd.calc_avg_err(np.log(c2m_r / c2mp1_r), rsplan)
    meff_m_lo   = meff_m_ae[0] - meff_m_ae[1]
    meff_m_hi   = meff_m_ae[0] + meff_m_ae[1]
    #print(plot_data, label_data, plot_fit, label_fit)

    if plot_fit : 
        ax.fill_between(xsh + tr_band, yfactor * meff_m_lo, yfactor * meff_m_hi, 
                    label=label_fit, **stg.band())
    if plot_data :
        ax.errorbar(xsh + tr[:-1], yfactor * meff_ae[0], yerr=yfactor * meff_ae[1], 
                    label=label_data, **stg.edotline())
    xlim=ax.get_xlim()
    ax.plot(xlim, [0,0], 'k:')
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)

    return ax

def runme_plot_meff(gg, tr, mc_list, ss,
        **kw) :
    ax          = kw.get('ax') or dv.make_std_axes()
    stg_iter    = iter(kw.get('stg_list') or kw.get('stg_iter') 
                       or dv.style_group_default)
    kw1         = dict(kw)
    lhpd.purge_keys(kw1, ['ax', 'xlim', 'xl', 'ylim', 'yl'])
    n_psnk      = len(mc_list)
    xsh         = .5/max(1, n_psnk-1)
    for i_psnk, mc in enumerate(mc_list) :
        stg = next(stg_iter)
        plot_meff_c2pt(gg, tr, mc, ss, 
                       ax=ax, label=str(mc.get_p3src()), stg=stg, 
                       xsh=i_psnk * xsh, **kw1)
        
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)
    #ax.legend()

    return ax

def runme_plot_fit_c2pt(
        h5g_top,    # output
        tr_list, psnk_list,
        **kw) :
    ax          = kw.get('ax') or dv.make_std_axes()
    plot_data   = kw.get('plot_data') or True
    stg_iter    = iter(kw.get('stg_list') or kw.get('stg_iter') 
                       or dv.style_group_default)
    kw1         = dict(kw)
    lhpd.purge_keys(kw1, ['ax', 'plot_data', 'xlim', 'xl', 'ylim', 'yl', 'xsh'])
    n_psnk      = len(psnk_list)
    n_tr        = len(tr_list)
    xsh         = kw.get('xsh', .5/max(1, n_psnk*n_tr-1))
    for i_psnk, psnk in enumerate(psnk_list) :
        stg = next(stg_iter)
        for i_tr, tr in enumerate(tr_list) :
            h5k     = "tr%d--%d/px%dpy%dpz%d" % ((tr.min(), tr.max()) + tuple(psnk))
            if 0 == i_tr : 
                label   = "%s" % (str(psnk),)
            else : label= None
            plot_meff_fit_c2pt(h5g_top[h5k], plot_data=(0==i_tr), 
                    ax=ax, 
                    label_data=label, label_fit=None, 
                    stg=stg,#dv.style_group_default[i_tr], 
                    xsh=(i_psnk * n_tr + i_tr) * xsh,
                    **kw1)
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)
    ax.legend()

    return ax

def runme_plot_fit_c2pt_lcdb(psnk_list, tr_list, 
        **kw) :
    dat_d       = kw['dat_d']
    c2fit_d     = kw['c2fit_d']
    c2_d        = kw['c2_d']
    ax          = kw.get('ax') or dv.make_std_axes()
    plot_data   = kw.get('plot_data') or True
    stg_iter    = iter(kw.get('stg_list') or kw.get('stg_iter') 
                       or dv.style_group_default)
    kw1         = dict(kw)
    lhpd.purge_keys(kw1, ['ax', 'plot_data', 'xlim', 'xl', 'ylim', 'yl', 'xsh'])
    n_psnk      = len(psnk_list)
    n_tr        = len(tr_list)
    xsh         = kw.get('xsh', .5/max(1, n_psnk*n_tr-1))
    for i_psnk, psnk in enumerate(psnk_list) :
        stg = next(stg_iter)
        for i_tr, tr in enumerate(tr_list) :
            h5fname, h5kpath = get_lcdb_loc(dat_d, 
                    dict(kind='c2fit', c2fit_d=dictnew(c2fit_d, trange=tr), c2_d=dictnew(c2_d, psnk=psnk)))
            h5f = h5py.File(h5fname, 'r')
            if 0 == i_tr : 
                label   = "%s" % (str(psnk),)
            else : label= None
            plot_meff_fit_c2pt(h5f[h5kpath], plot_data=(0==i_tr), 
                    ax=ax, 
                    label_data=label, label_fit=None, 
                    stg=stg,#dv.style_group_default[i_tr], 
                    xsh=(i_psnk * n_tr + i_tr) * xsh,
                    **kw1)
            h5f.close()
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)
    ax.legend()

    return ax


# dispersion relation
def plot_fit_disprel(h5g_top, tr, psnk_list, **kw) :
    """ plot E(p) 
        E=(c2_fitp[1]*yfactor)**ypow
        p=(|plat|*xfactor)**xpow
    """
    ax          = kw.get('ax') or dv.make_std_axes()
    ainv        = kw.get('ainv') or 1.
    xfactor     = kw.get('xfactor') or 1.
    xpow        = kw.get('xpow') or 1.
    yfactor     = kw.get('yfactor') or 1.
    ypow        = kw.get('ypow') or 1.
    rsplan      = kw['rsplan']
    latsize     = kw['latsize']
    stg         = kw.get('stg') or dv.style_group_default[0]
    xsh         = kw.get('xsh') or kw.get('xshift') or 0.
    xmargin_band= kw.get('xmargin_band') or 0.
    label       = kw.get('label') or None

    psnk2_l     = lhpd.mom2_cont(latsize[:3], psnk_list[:,:3])
    x_r         = (xfactor**2 * psnk2_l) ** (xpow / 2.)
    
    elat_r  = []
    for i_psnk, psnk in enumerate(psnk_list) :
        h5k     = "tr%d--%d/px%dpy%dpz%d" % ((tr.min(), tr.max()) + tuple(psnk))
        h5g     = h5g_top[h5k]
        p_r     = h5g['p'][()]
        assert('c2pt_e0' == h5g.attrs['pname'][1])
        elat_r.append(p_r[:,1])
    elat_r      = np.array(elat_r).transpose(1,0)
    elat_ae     = lhpd.calc_avg_err(elat_r, rsplan)
    y_a         = (yfactor * elat_ae[0]) ** ypow
    y_e         = yfactor**ypow * (ypow * elat_ae[1]) * elat_ae[0]**(ypow - 1)
    ax.errorbar(xsh + x_r, y_a, yerr=y_e, label=label, **stg.edotline())

    # relativistic disp.rel through datapoint[0]
    p2min,p2max = psnk2_l.min(), psnk2_l.max()
    p2_rb       = np.r_[p2min : p2max : 100j]
    x_rb        = (xfactor**2 * p2_rb) ** (xpow / 2.)
    imin        = np.argmin(psnk2_l)
    e_rel_r     = (yfactor**2 * (p2_rb - psnk2_l[imin] + elat_ae[0][imin]**2)) ** (ypow / 2.)

    ax.plot(xsh + x_rb, e_rel_r, **stg.cp(ls='--').line())

    xlim=ax.get_xlim()
    ax.plot(xlim, [0,0], 'k:')
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)

    return ax

def runme_plot_fit_disprel(
        h5g_top,    # output
        tr_list, psnk_list,
        **kw) :
    psnk_list   = np.array(psnk_list)
    n_psnk      = len(psnk_list)
    n_tr        = len(tr_list)
    latsize = kw['latsize']
    ax          = kw.get('ax') or dv.make_std_axes()
    plot_data   = kw.get('plot_data') or True
    stg_iter    = iter(kw.get('stg_list') or kw.get('stg_iter') 
                       or dv.style_group_default)
    kw1         = dict(kw)
    lhpd.purge_keys(kw1, ['ax', 'plot_data', 'xlim', 'xl', 'ylim', 'yl'])

    xsh         = .3/max(1, n_tr - 1)
    psnk2_l     = lhpd.mom2_cont(latsize[:3], psnk_list[:,:3])
    p2min, p2max = psnk2_l.min(), psnk2_l.max()
    xsh         = (p2max - p2min) / max(1, len(psnk2_l) - 1) * .3 / n_tr

    for i_tr, tr in enumerate(tr_list) :
        stg = next(stg_iter)
        label   = "t=[%d:%d]" % (tr.min(), tr.max())
        plot_fit_disprel(h5g_top, tr, psnk_list,
                    ax=ax, 
                    label=label, 
                    stg=stg,
                    xsh=i_tr * xsh,
                    **kw1)
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)
    ax.legend()

    return ax

def plot_fit_disprel_lcdb(psnk_list, **kw) :
    """ plot E(p) 
        E=(c2_fitp[1]*yfactor)**ypow
        p=(|plat|*xfactor)**xpow
    """
    psnk_list   = np.asarray(psnk_list)
    dat_d       = kw['dat_d']
    c2fit_d     = kw['c2fit_d']
    c2_d        = kw['c2_d']
    ax          = kw.get('ax') or dv.make_std_axes()
    ainv        = kw.get('ainv') or 1.
    xfactor     = kw.get('xfactor') or 1.
    xpow        = kw.get('xpow') or 1.
    yfactor     = kw.get('yfactor') or 1.
    ypow        = kw.get('ypow') or 1.
    rsplan      = kw['rsplan']      
    latsize     = np.asarray(kw['latsize'])
    stg         = kw.get('stg') or dv.style_group_default[0]
    xsh         = kw.get('xsh') or kw.get('xshift') or 0.
    xmargin_band= kw.get('xmargin_band') or 0.
    label       = kw.get('label') or None

    psnk2_l     = lhpd.mom2_cont(latsize[:3], psnk_list[:,:3])
    x_r         = (xfactor**2 * psnk2_l) ** (xpow / 2.)
    
    elat_r  = []
    for i_psnk, psnk in enumerate(psnk_list) :
        h5fname, h5kpath = get_lcdb_loc(dat_d, 
                dict(kind='c2fit', c2fit_d=c2fit_d, c2_d=dictnew(c2_d, psnk=psnk)))
        h5f = h5py.File(h5fname, 'r')
        h5g = h5f[h5kpath]
        if not (None is rsplan) : lhpd.h5_io.h5_check_rsplan(h5g, rsplan=rsplan)
        p_r     = h5g['p'][()]

        assert(h5g.attrs['pname'][1].endswith('e0'))
        elat_r.append(p_r[:,1])
        h5f.close()

    elat_r      = np.array(elat_r).transpose(1,0)
    elat_ae     = lhpd.calc_avg_err(elat_r, rsplan)  # FIXME use instead saved rsplan from each fit
    y_a         = (yfactor * elat_ae[0]) ** ypow
    y_e         = yfactor**ypow * (ypow * elat_ae[1]) * elat_ae[0]**(ypow - 1)
    ax.errorbar(xsh + x_r, y_a, yerr=y_e, label=label, **stg.edotline())

    # relativistic disp.rel through datapoint[0]
    p2min,p2max = psnk2_l.min(), psnk2_l.max()
    p2_rb       = np.r_[p2min : p2max : 100j]
    x_rb        = (xfactor**2 * p2_rb) ** (xpow / 2.)
    imin        = np.argmin(psnk2_l)
    e_rel_r     = (yfactor**2 * (p2_rb - psnk2_l[imin] + elat_ae[0][imin]**2)) ** (ypow / 2.)

    ax.plot(xsh + x_rb, e_rel_r, **stg.cp(ls='--').line())

    ax.axhline(y=0, c='k', ls=':')
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)

    return ax

def runme_plot_fit_disprel_lcdb(psnk_list, tr_list,
        **kw) :
    psnk_list   = np.asarray(psnk_list)
    ax          = kw.get('ax') or dv.make_std_axes()
    c2fit_d     = kw['c2fit_d']
    n_tr        = len(tr_list)
    latsize     = np.asarray(kw['latsize'])
    stg_iter    = iter(kw.get('stg_list') or kw.get('stg_iter') 
                       or dv.style_group_default)
    kw1         = dict(kw)
    lhpd.purge_keys(kw1, ['ax', 'label', 'xlim', 'xl', 'ylim', 'yl'])

    psnk2_l     = lhpd.mom2_cont(latsize[:3], psnk_list[:,:3])
    p2min, p2max = psnk2_l.min(), psnk2_l.max()
    xsh         = (p2max - p2min) / max(1, len(psnk2_l) - 1) * .3 / n_tr

    for i_tr, tr in enumerate(tr_list) :
        stg     = next(stg_iter)
        tr1     = mk_c2fit_tr(tr)
        label   = "t=[%d:%d]" % (tr1.min(), tr1.max())
        plot_fit_disprel_lcdb(psnk_list,
                    **dictnew(kw1, c2fit_d = dictnew(c2fit_d, trange=tr),
                              ax=ax, label=label, stg = stg, xsh = i_tr * xsh))
    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)
    ax.legend()

    return ax

def plot_cmp_c2fit_lcdb(pname, xval_dat_lc_list, **kw) :
    ax          = kw.get('ax') or dv.make_std_axes()
    stg         = kw.get('stg') or dv.style_group_default[0]
    label       = kw.get('label') or None
    xsh         = kw.get('xsh') or kw.get('xshift') or 0.
    rsplan      = kw.get('rsplan')
    yfactor     = kw.get('yfactor') or 1.

    attrname = lhpd.strip_prefix(pname, 'attr.') or None

    xr, p_a, p_e = [], [], []
    for i_fit, (xval, dat_d, lc_d) in enumerate(xval_dat_lc_list) :
        xr.append(xval)
        h5fname, h5kpath = get_lcdb_loc(dat_d, lc_d)
        h5f     = h5py.File(h5fname, 'r')
        h5g     = h5f[h5kpath]
        rsplan_i= lhpd.h5_io.h5_check_rsplan(h5g, rsplan=rsplan)
        if None is attrname :
            i_p     = list(h5g.attrs['pname']).index(pname)
            p_r     = h5g['p'][()][:, i_p]
            p_ae    = lhpd.calc_avg_err(p_r, rsplan_i)
            p_a.append(p_ae[0])
            p_e.append(p_ae[1])
            h5f.close()
        else : # attributes
            if   'pvalue_ht2'  == attrname : p_a.append(h5g['pvalue_ht2'][()])
            elif 'pvalue_chi2' == attrname : p_a.append(h5g['pvalue_chi2'][()])
            elif 'pvalue_emp' == attrname : p_a.append(h5g['pvalue_emp'][()])
            else : raise ValueError(attrname)

    xr      = np.array(xr)
    p_a     = np.array(p_a)
    if None is attrname : 
        p_e     = np.array(p_e)
        ax.errorbar(xsh + xr, yfactor * p_a, yerr = yfactor * p_e, label=label, **stg.edotline())
    else : ax.plot(xsh + xr, yfactor * p_a, label=label, **stg.dotline())

    lhpd.purge_keys(kw, ['ax'])
    dv.set_std_axes(ax, **kw)
    return ax

def plot_cmp_c2fitp_multi(pname_list, xval_dat_lc_list, ax_list, **kw) :
    do_pvalue   = kw.get('do_pval') or kw.get('do_pvalue') or kw.get('pvalue', True)
    yfactor     = kw.get('yfactor') or 1.
    
    lhpd.purge_keys(kw, ['yfactor']) # pvalue must be plotted wo yfactor

    i_ax = 0
    if do_pvalue :
        plot_cmp_c2fit_lcdb('attr.pvalue_emp', xval_dat_lc_list, ax=ax_list[i_ax], **kw)
        i_ax += 1
    for pname in pname_list :
        plot_cmp_c2fit_lcdb(pname, xval_dat_lc_list, ax=ax_list[i_ax], yfactor=yfactor, **kw)
        i_ax += 1

#########################################
# utility functions for fitting  c2pt
def pname_c2fit_c_(nexp, pname_prefix='') :
    """ create parameter list in specific order """
    assert 0 < nexp
    pname = ['%sc0' % pname_prefix]
    for i in range(1, nexp):
        pname.append('%sc%d' % (pname_prefix, i))
    return pname
def pname_c2fit_e_(nexp, pname_prefix='') :
    """ create parameter list in specific order """
    assert 0 < nexp
    pname = ['%se0' % pname_prefix]
    for i in range(1, nexp):
        pname.append('%sde%d_%d' % (pname_prefix, i, i-1))
    return pname
def pname_c2fit_ce_(nexp, pname_prefix='') :
    """ create parameter list in specific order """
    assert 0 < nexp
    pname = ['%sc0' % pname_prefix, 
             '%se0' % pname_prefix]
    for i in range(1, nexp):
        pname.append('%sc%d' % (pname_prefix, i))
        pname.append('%sde%d_%d' % (pname_prefix, i, i-1))
    return pname

from lhpd.fitter.fitter2 import prior_lnorm_rel0_abs0, prior_lnorm_rel0
def c2fit_ladder_prior_lnorm(nexp, 
        clambda, elambda, 
        c0=None, e0=None,
        pname_ce=None, pname_c = None, pname_e = None, pname_prefix='') :
    """ generate lognormal priors for 
                [ {c0}?, c1/c0, c2/c0, ...] and [ {e0}?, de10/e0, de21/e0, ...]
    """
    if None is pname_c : pname_c = pname_c2fit_c_(nexp, pname_prefix=pname_prefix)
    if None is c0 : cprior = prior_lnorm_rel0(pname_c, clambda)
    else : cprior = lhpd.fitter.prior_lnorm_rel0_abs0(pname_c, c0, clambda)
    if None is pname_e : pname_e = pname_c2fit_e_(nexp, pname_prefix=pname_prefix)
    if None is e0 : eprior = prior_lnorm_rel0(pname_e, elambda)
    else : eprior = lhpd.fitter.prior_lnorm_rel0_abs0(pname_e, e0, elambda)
    if None is pname_ce: pname_ce= pname_c2fit_ce_(nexp, pname_prefix=pname_prefix)
    return lhpd.fitter.datafit_join(pname_ce, cprior, eprior)


"""
class c2fit_ladder_lnprior(datafit) :
    def __init__(self, nexp, 
                 cprior, cprior_lambda, 
                 eprior, eprior_lambda,
                 pname_prefix='c2fit_') :
        self.nexp   = nexp
        #self.cprior = cprior
        #self.eprior = eprior
        #self.cprior_lambda = cprior_lambda
        #self.eprior_lambda = eprior_lambda
        # "interface" - specific order of parameters
        self.pname = pname_c2fit_ce_(nexp, pname_prefix)
        self.cep   = np.r_[cprior, eprior]
        self.cep_l = np.r_[cprior_lambda, eprior_lambda]
    
    def plen(self) : return 2*self.nexp
    def vlen(self) : return self.plen()    # len(vector) = len(param)
    
    def fv(self, p): 
        assert(self.plen() == len(p))
        pce = np.array(p).reshape(-1,2) # [i, {c|e}]
        if (pce <= 0).any(): 
            raise OverflowError
        pce[1:] /= pce[0]
        pce[0]  /= self.cep
        return (np.log(pce) / self.cep_l).flatten()
    def dfv(self, p): 
        assert(self.plen() == len(p))
        pce = np.array(p).reshape(-1,2) # [i, {c|e}]
        if (pce <= 0).any(): 
            raise OverflowError
        rpce= 1. / (pce * self.cep_l)
        dpce = np.zeros((self.vlen(), self.plen()), pce.dtype)
        
        dpce[::2,0]  = - rpce[0,0] 
        dpce[1::2,1] = - rpce[0,1]
        # sic! this overwrites [0,0] and [1,1]
        dpce[np.diag_indices(self.plen())] = rpce.flat
        return dpce
"""         
