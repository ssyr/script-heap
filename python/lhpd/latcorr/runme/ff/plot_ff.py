from __future__ import print_function
from past.builtins import basestring
import lhpd
import numpy as np
import dataview as dv

def runme_calc_save_ff(
        h5_file, 
        mcgrp_list, ssgrp,
        flav_list, op_list,
        method_list,
        **kw ) :

    mlat        = kw['mlat']
    latsize     = kw['latsize']
    had         = kw['had']
    VERBOSE     = kw.get('VERBOSE', 0)

    if isinstance(h5_file, basestring) :
        h5f = h5py.File(h5_file, 'a')
    elif isinstance(h5_file, h5py.File) or isinstance(h5_file, h5py.Group) :
        h5f = h5_file
    else : raise ValueError(h5_file)

    for flav in flav_list :
        for op in op_list :
            print("# ", op, flav)
            ir_list = lhpd.latcorr.op_default_ir_list(op)
            gg      = ens_data_get(flav=flav, op=op, **kw)
            h5k = '/%s/%s/%s' % (had, op, flav)
            #lhpd.h5_io.h5_purge_keys(h5f, [h5k])
            kw1 = dict(kw)
            purge_keys(kw1, ['mlat', 'latsize'])
            lhpd.latcorr.calc_save_ff(h5f, h5k, gg, mcgrp_list, ssgrp, 
                         op, ir_list, mlat, latsize, method_list, **kw1)

    if isinstance(h5_file, basestring) :
        h5f.close()
    elif isinstance(h5_file, h5py.File) or isinstance(h5_file, h5py.Group) :
        h5f.flush()
    else : raise ValueError(h5_file)

def runme_calc_save_ff_lcdb(
        mcgrp_list, ssgrp,
        method_list,
        **kw ) :

    dat_d       = kw['dat_d']
    data_in     = kw.get('data_in', dat_d['data_dir'])
    c2_d        = kw['c2_d']
    c3_d        = kw['c3_d']
    ins_d       = kw['ins_d']

    print("runme_calc_save_ff_lcdb:", ins_str_desc(ins_d))
    assert('op' == ins_d['kind'])
    op      = ins_d['op']
    ir_list = ins_d.get('ir_list', lhpd.latcorr.op_default_ir_list(op))
    ins1_d  = dictnew(ins_d, ir_list=ir_list)

    h5file, h5kpath = get_lcdb_loc(dat_d, dict(kind='ff', c3_d=c3_d, ins_d=ins1_d))
    h5f     = h5py.File(h5file, 'a')
    lhpd.h5_io.h5_purge_keys(h5f, [h5kpath])
    kw1, (mlat, latsize) = dictnew_pop(kw, ['mlat', 'latsize'])
    gg      = ens_data_get(**dictnew(kw1, ins_d=ins1_d))

    lhpd.latcorr.calc_save_ff(h5f, h5kpath, gg, mcgrp_list, ssgrp, 
                 op, ir_list, mlat, latsize, method_list, **kw1)
#
    h5f.flush()
    h5f.close()


# XXX copy/paste from analysis_ff.py
def plot_pltx_cmp(dgrp_ff, i_ff, i_q2, tsep_list, rsplan=('jk',1),
            ylabel=None, yr=None, xlabel=r'$\tau$', xr=None, x_sh=None,
            ax=None, stg_list=dv.style_group_default, stg_summ=dv.style_group_default[-1]) :
    """ quick & dirty plotting of plateaus pltx averages and methods """
    if None is ax : ax = dv.make_std_axes()

    max_tslice  = 3    # FIXME must be a parameter to ratio_pltx_avg, ratio_summ

    n_tsep = len(tsep_list)
    tmax = max(tsep_list)
    # plot ratio_pltx, ratio_pltx_avg
    if None is x_sh : x_sh = .4 / max(1, n_tsep - 1)

    # bands
    for i_tsep, tsep in enumerate(tsep_list) :
        y2  = dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)][:, 0, i_q2, i_ff]
        y2_a, y2_e = lhpd.calc_avg_err(y2, rsplan)
        tr2 = np.r_[lhpd.latcorr.pltx_trange(tsep, max_tslice)]
        tr2[-1] -= 1
        ax.fill_between(tr2 + i_tsep * x_sh, [y2_a - y2_e] * 2, [y2_a + y2_e] *2,
                        **stg_list[i_tsep].band())
        ax.plot(tr2 + i_tsep * x_sh, [y2_a]*2, **stg_list[i_tsep].line())

    # plateaus
    for i_tsep, tsep in enumerate(tsep_list) :
        tr  = np.r_[0 : 1+tsep]
        y   = dgrp_ff['ratio_pltx/dt%d' % (tsep)][:, :, i_q2, i_ff]
        y_a, y_e = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(tr + i_tsep * x_sh, y_a, yerr=y_e, label=(r'$T=%da$' % tsep),
                    **stg_list[i_tsep].edotline())

    # summation res
    ys = dgrp_ff['ratio_summ'][:, 0, i_q2, i_ff]
    ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
    ax.errorbar([-1.], ys_a, yerr=ys_e, label='summ', **stg_summ.edot())

    if None is xr : xr = [-1.5, 1+tsep]
    ax.set_xlim(xr)
    ax.set_xlabel(r'$\tau$')
    if not None is  yr : ax.set_ylim(yr)
    if None is ylabel: ylabel = dgrp_ff.name
    ax.set_ylabel(ylabel)

    ax.legend()

    return ax

def ff_get_h5attr(a, dgrp_ff, method_list, tsep_list) :
    ml = []
    for m in method_list :
        if 'ratio_pltx_avg' == m :
            ml.extend([ 'ratio_pltx_avg/dt%d' % tsep for tsep in tsep_list ])
        else : ml.append(m)
    # find q2r
    q2r = None
    for m in ml :
        try :
            q2r = dgrp_ff[m].attrs[a]
            print("%s : from ['%s']" % (a, m))
            break       # found q2r
        except KeyError : pass 
    
    return q2r


# XXX copy/paste from analysis_ff.py
def plot_q2dep_cmp(dgrp_ff, i_ff, tsep_list, method_list,
        q2_slice=slice(None),
        rsplan=('jk',1), ylabel=None, yr=None, xlabel=r'$Q^2$[lat]', xr=None, x_sh=None,
        ax=None, stg_list=dv.style_group_default, 
        stg_summ=dv.style_group_default[-1],
        stg_fit_c3=dv.style_group_default[-2],
        **kw) :
    """ plot q2 dependence of a ff, compare methods
        q2 (x-axis is in lattice units, x=Q2 *a**2
    """
    xlabel  = kw.get('xl') or kw.get('xlabel') or None
    ainv    = kw.get('ainv') or None
    yfactor = kw.get('yfactor') or 1.
    if None is ainv : q2mul  = 1.
    else : q2mul = ainv**2
    q2pow   = kw.get('q2pow') or 0.

    if None is ax : ax = dv.make_std_axes()
    ls = latsize[0]
    assert (np.asarray(latsize[:3]) == ls).all()

    # find q2r
    q2r = ff_get_h5attr('q2_list', dgrp_ff, method_list, tsep_list)
    assert(not None is q2r)
    q2r = q2r[q2_slice]

    n_tsep = len(tsep_list)
    d_xr = kw.get('x_sh') or (q2r.max() - q2r.min()) / len(q2r) *.5
    if None is x_sh : x_sh = d_xr *.4 / max(1, n_tsep+1) # +summation+fit_c3


    def plot_ffi(dgrp_x, x_sh, label_x, stg_x) :
        assert(np.allclose(q2r, dgrp_x.attrs['q2_list'][q2_slice]))
        ys  = dgrp_x[:, 0, :, i_ff]
        ys  = ys[:, q2_slice] * (q2r * q2mul)**q2pow * yfactor
        ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
        ax.errorbar(q2mul*(q2r + x_sh), ys_a, yerr=ys_e, label=label_x,
                    **stg_x.edotline())

    # plateau avg
    if 'ratio_pltx_avg' in method_list : 
        for i_tsep, tsep in enumerate(tsep_list) :
            plot_ffi(dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)], i_tsep * x_sh, r'$T=%da$' % tsep, stg_list[i_tsep])

    cnt_xsh = n_tsep
    # summation
    if 'ratio_summ' in method_list : 
        plot_ffi(dgrp_ff['ratio_summ'], cnt_xsh * x_sh, 'summ', stg_summ)
        cnt_xsh += 1
    if 'fit_c3' in method_list : 
        plot_ffi(dgrp_ff['fit_c3'], cnt_xsh * x_sh, 'fit-c3', stg_fit_c3)
        cnt_xsh += 1

    ax.axhline(y=0, c='k', ls=':')

    #if None is xr : xr = [ q2mul*(q2r.min() - .5 * d_xr), q2mul*(q2r.max() + 1. * d_xr)]
    #ax.set_xlim(xr)
    #if None is xlabel : 
        #if None is ainv : ax.set_xlabel(r'$Q^2$ [lat]')
        #else : ax.set_xlabel(r'$Q^2$ [GeV${}^2$]')
    #else : ax.set_xlabel(xlabel)
    #if not None is  yr : ax.set_ylim(yr)
    #if None is ylabel: ylabel = dgrp_ff.name

    dv.set_std_axes(ax, **kw)
    ax.set_ylabel(ylabel)

    return ax

def get_ff_data(dgrp_ff, obsv, 
        **kw) :

    q2_slice= kw.get('xslice') or kw.get('q2_slice') or np.s_[:]
    ainv    = kw.get('ainv') or None
    mlat    = None
    if obsv in ['GE', 'GM'] : mlat    = kw['mlat']
    yfactor = kw.get('yfactor') or 1.
    yfunc   = kw.get('yfunc') 
    if None is ainv : q2mul  = 1.
    else : q2mul = ainv**2
    q2pow   = kw.get('q2pow') or 0.

    method  = kw.get('method')
    q2r = ff_get_h5attr('q2_list', dgrp_ff, [method], tsep_list)
    assert(not None is q2r)
    q2r = q2r[q2_slice]

    if   'F1' == obsv : ys  = dset0[:, 0, :, 0]
    elif 'F2' == obsv : ys  = dset0[:, 0, :, 1]
    elif 'GE' == obsv : 
        ys  = dset0[:, 0, :, 0] - q2r / (4. * mlat**2) * dset0[:, 0, :, 1]
    elif 'GM' == obsv : 
        ys  = dset0[:, 0, :, 0] + dset0[:, 0, :, 1]
    # axial
    elif 'GA' == obsv : ys  = dset0[:, 0, :, 0]
    elif 'GP' == obsv : ys  = dset0[:, 0, :, 1]
    # cpviol vector
    elif 'FA' == obsv : ys  = dset0[:, 0, :, 0] 
    elif 'F3' == obsv : ys  = dset0[:, 0, :, 1] 
    # error
    else : raise ValueError(obsv)
    #else:
        #i_gff = lhpd.latcorr.get_gff_ind(obsv)
        #ys  = dset0[:, 0, :, i_gff]

    xs  = q2mul * q2r
    ys  = ys[:, q2_slice] * (q2r * q2mul)**q2pow * yfactor
    if not None is yfunc : 
        ys = yfunc(xs, ys)

    return xs, ys

if False :
    # this was used to plot O(a^1)/O(a^0) ratios
    # comparing ffs: ratio #1/#2
    def plot_ff_cmp_1d2v2(
            h5fname1, h5kpath1, 
            h5fname2, h5kpath2, 
            i_ff, tsep, method_list,
            rsplan=('jk',1), 
            ax=None, label=None, stg=stg_list[0], **kw) :

        print("FIXME ignoring method_list, using ONLY ratio_pltx_avg")

        if None is ax : ax = dv.make_std_axes()
        yfactor     = kw.get('yfactor', 1.)
        ffd = []
        xrd = []
        for h5fn, h5kp in [ (h5fname1, h5kpath1), (h5fname2, h5kpath2) ] :
            h5f = h5py.File(h5fn, 'r')
            h5d = h5f['%s/ratio_pltx_avg/dt%d' % (op, flav, tsep)]
            ffd.append(h5d[:,0,:,i_ff])
            xrd.append(h5d.attrs['q2_list'])
            h5f.close()

        # check mom
        xrd = np.array(xrd)
        assert(np.allclose(xrd[0], xrd[1:]))
        q2r = xrd[0]

        y_a, y_e = lhpd.calc_avg_err(yfactor * ffd[0] / ffd[1], rsplan)
        ax.errorbar(q2r*ainv**2, y_a, yerr=y_e, label=label, **stg.edotline())

        ax.axhline(y=0, c='k', ls='-')
        ax.axhline(y=1, c='k', ls=':')
        ax.axhline(y=-1, c='k', ls=':')
        return ax


# this was used to plot disc/conn ratios
def plot_cmp_ff_ratio(dgrp1, dgrp2, i_ff, tsep_list, **kw) :
    """ plot ff ratio dgrp1 / dgrp2
    """
    rsplan  = kw['rsplan']
    xlabel  = kw.get('xl') or kw.get('xlabel') or None
    ylabel  = kw.get('yl') or kw.get('ylabel') or None
    xr      = kw.get('xr')
    yr      = kw.get('yr')
    ainv    = kw.get('ainv') or None
    yfactor = kw.get('yfactor', 1.)
    q2_slice= kw.get('xslice') or np.s_[:]
    stg_list= kw.get('stg_list') or dv.style_group_default
    ax      = kw.get('ax') or dv.make_std_axes()
    x_sh    = kw.get('x_sh')
    method_list = kw.get('method_list', ['fit_c3', 'ratio_summ', 'ratio_pltx_avg'])
    dslice1 = kw.get('dslice1', slice(None))
    dslice2 = kw.get('dslice2', slice(None))
    bin1 = kw.get('bin1', 1)
    bin2 = kw.get('bin2', 1)

    print("q2_slice =", q2_slice)
    if None is ainv : q2mul  = 1.
    else : q2mul = ainv**2

    q2r = ff_get_h5attr('q2_list', dgrp1, method_list, tsep_list)
    assert(not None is q2r)
    q2r = q2r[q2_slice]
    
    q2r2= ff_get_h5attr('q2_list', dgrp2, method_list, tsep_list)
    assert(not None is q2r2)
    assert(np.allclose(q2r, q2r2))

    n_tsep = len(tsep_list)
    d_xr = (q2r.max() - q2r.min()) / len(q2r) *.1
    if None is x_sh : x_sh = d_xr *.3 / max(1, n_tsep+1) # +summation+fit_c3
    def plot_ratio(ff_num_r, ff_den_r, cnt_xsh, label, stg) :
        #print("ff_num_r.shape=%s  ff_den_r.shape=%s" % (ff_num_r.shape, ff_den_r.shape))
        if 1 != bin1 : ff_num_r = lhpd.rebin(ff_num_r, bin1, rsplan)
        if 1 != bin2 : ff_den_r = lhpd.rebin(ff_den_r, bin2, rsplan)
        assert(len(ff_num_r) == len(ff_den_r))
        a,e     = lhpd.calc_avg_err(yfactor * ff_num_r / ff_den_r, rsplan)
        ax.errorbar(q2mul*(q2r + d_xr * cnt_xsh), a, yerr=e, label=label, **stg.edotline())

    i_plot = 0
    for m in method_list :
        if 'ratio_pltx_avg' == m :
            for i_tsep, tsep in enumerate(tsep_list) :
                m1 = 'ratio_pltx_avg/dt%d' % tsep
                dgrp_ff_1i  = dgrp1[m1][dslice1, 0, :, i_ff]
                dgrp_ff_2i  = dgrp2[m1][dslice2, 0, :, i_ff]
                plot_ratio(dgrp_ff_1i, dgrp_ff_2i, i_plot, 
                    r'$T=%da$' % tsep, stg_list[i_plot])
                i_plot +=1 
        elif 'ratio_summ' == m or 'fit_c3' == m :
            plot_ratio(dgrp1['fit_c3'][dslice1, 0, :, i_ff], 
                       dgrp2['fit_c3'][dslice2, 0, :, i_ff], 
                    i_plot, dv.latexize(m), stg_list[i_plot])
            i_plot +=1 
        else : raise ValueError(m)

    if None is xr : xr = [ q2mul*(q2r.min() - .5 * d_xr), q2mul*(q2r.max() + 1. * d_xr)]
    ax.set_xlim(xr)
    ax.plot(xr, [0,0], "k:")
    if not None is yr : ax.set_ylim(yr)

    if None is xlabel :
        if None is ainv : ax.set_xlabel(r'$Q^2$ [lat]')
        else : ax.set_xlabel(r'$Q^2$ [GeV${}^2$]')
    else : ax.set_xlabel(xlabel)
    #if None is ylabel : ax.set_ylabel(r'ratio')
    if not None is ylabel : ax.set_ylabel(ylabel)
    lhpd.purge_keys(kw, ['ax', 'xl', 'xlabel', 'yl', 'ylabel'])
    dv.set_std_axes(ax, **kw)

    return ax


def plot_q2dep_cmp_ff(dgrp_ff, obsv, tsep_list, method_list,
        rsplan=('jk',1), 
        #ylabel=None, yr=None, xlabel=r'$Q^2$[lat]', xr=None, x_sh=None,
        ax=None, stg_list=dv.style_group_default, 
        yfunc=None,
        #stg_summ=dv.style_group_default[-1],
        #stg_fit_c3=dv.style_group_default[-2],
        label=None,
        **kw) :
    """ plot q2 dependence of a ff, compare methods
        q2 (x-axis is in lattice units, x=Q2 *a**2
    """
    #print("plot_q2dep_cmp_ff: kw.keys=%s" % kw.keys())
    q2_slice= lhpd.take1st(kw.get('xslice'), kw.get('q2_slice'), np.s_[:])
    xlabel  = lhpd.take1st(kw.get('xl'), kw.get('xlabel'))
    label_prefix = kw.get('label_prefix', '')
    ainv    = kw.get('ainv')
    mlat    = kw.get('mlat')
    yfactor = lhpd.take1st(kw.get('yfactor'), 1.)
    if None is ainv : q2mul  = 1.
    else : q2mul = ainv**2
    q2pow   = kw.get('q2pow') or 0.

    if None is ax : ax = dv.make_std_axes()
    ls = latsize[0]
    assert (np.asarray(latsize[:3]) == ls).all()

    
    if 'ratio_summ' in method_list : dset0 = dgrp_ff['ratio_summ']
    elif 'ratio_pltx_avg' in method_list : 
        dset0 = dgrp_ff['ratio_pltx_avg/dt%d' % (tsep_list[0])]
    else : raise NotImplementedError("cannot get q2r")
    q2r = dset0.attrs['q2_list'][q2_slice]

    n_tsep = len(tsep_list)
    d_xr = (q2r.max() - q2r.min()) / len(q2r) *.5
    x_sh = lhpd.take1st(kw.get('x_sh', d_xr *.4 / max(1, n_tsep+1)))
    x_sh0 = kw.get('x_sh0', 0.)

    def plot_ffi(dgrp_x, x_sh, label_x, stg_x) :
        if   'F1' == obsv : ys  = dgrp_x[:, 0, :, 0]
        elif 'F2' == obsv : ys  = dgrp_x[:, 0, :, 1]
        elif 'GE' == obsv : ys  = dgrp_x[:, 0, :, 0] - q2r / (4. * mlat**2) * dgrp_x[:, 0, :, 1]
        elif 'GM' == obsv : ys  = dgrp_x[:, 0, :, 0] + dgrp_x[:, 0, :, 1]
        # axial
        elif 'GA' == obsv : ys  = dgrp_x[:, 0, :, 0]
        elif 'GP' == obsv : ys  = dgrp_x[:, 0, :, 1]
        # cpviol vector
        elif 'FA' == obsv : ys  = dgrp_x[:, 0, :, 0] 
        elif 'F3' == obsv : ys  = dgrp_x[:, 0, :, 1] 
        # error
        else : raise ValueError(obsv)
        #else:
            #i_gff = lhpd.latcorr.get_gff_ind(obsv)
            #ys  = dgrp_x[:, 0, :, i_gff]

        xr  = q2mul * q2r
        ys  = ys[:, q2_slice] * (q2r * q2mul)**q2pow * yfactor
        if not None is yfunc : 
            ys = yfunc(xr, ys)
        ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
        ax.errorbar(q2mul*(q2r + x_sh), ys_a, yerr=ys_e, label='%s%s' % (label_prefix, label_x),
                    **stg_x.edotline())

    dl_def = { 'ratio_pltx_avg' : None, 'fit_c3' : 'fit-c3', 'summ' : 'summ' }
    dl = {}
    if not None is label: 
        dl = { 'fit_c3' : None, 'ratio_summ' : None, 'ratio_pltx_avg' : None }
        if 'fit_c3' in method_list : dl['fit_c3'] = label
        elif 'ratio_summ' in method_list : dl['ratio_summ'] = label
        elif 'ratio_pltx_avg' in method_list : dl['ratio_pltx_avg'] = label

    i_plot = 0
    for m in method_list :
        # plateau avg
        if 'ratio_pltx_avg' == m : 
            for i_tsep, tsep in enumerate(tsep_list) :
                m1 = 'ratio_pltx_avg/dt%d' % (tsep)
                plot_ffi(dgrp_ff[m1], i_plot * x_sh + x_sh0, 
                        # QUICKFIX #dl.get('ratio_pltx_avg', 
                        r'$T=%da$' % tsep, 
                        stg_list[i_plot])
                if not None is label : # ??? label only for one tsep?
                    dl['ratio_pltx_avg'] = None
                i_plot += 1

        elif 'ratio_summ' == m or 'fit_c3' == m :
            plot_ffi(dgrp_ff[m], i_plot * x_sh + x_sh0,
                    dl.get(m, dl_def[m]), stg_list[i_plot])
            i_plot += 1

    ax.axhline(y=0, c='k', ls=':')

    #if None is xr : xr = [ q2mul*(q2r.min() - .5 * d_xr), q2mul*(q2r.max() + 1. * d_xr)]
    #ax.set_xlim(xr)
    if True is xlabel : 
        if None is ainv : ax.set_xlabel(r'$Q^2$ [lat]')
        else : ax.set_xlabel(r'$Q^2$ [GeV${}^2$]')
    elif not None is xlabel : ax.set_xlabel(xlabel)
    #if not None is  yr : ax.set_ylim(yr)
    #if None is ylabel: ylabel = "%s:%s" % (dgrp_ff.name, obsv)
    #ax.set_ylabel(ylabel)
    #ax.legend()
    kw1 = dict(kw)
    lhpd.purge_keys(kw1, ['xlabel', 'xl'])
    # set xlim, ylim, ylabel, title
    dv.set_std_axes(ax, **kw1)

    return ax


def plot_q2dep_cmp_F2dF1(dgrp_ff, tsep_list, method_list,
        rsplan=('jk',1), ylabel=None, yr=None, xlabel=r'$Q^2$[lat]', xr=None, x_sh=None,
        ax=None, stg_list=dv.style_group_default, 
        stg_summ=dv.style_group_default[-1],
        stg_fit_c3=dv.style_group_default[-2],
        **kw) :
    """ plot q2 dependence of a ff, compare methods
        q2 (x-axis is in lattice units, x=Q2 *a**2
    """
    xlabel  = kw.get('xl') or kw.get('xlabel') or None
    ainv    = kw.get('ainv') or None
    q2_slice= kw.get('xslice') 
    if None is q2_slice : q2_slice = kw.get('q2_slice') 
    if None is q2_slice : q2_slice = np.s_[:]

    yfactor = kw.get('yfactor') or 1.
    print("q2_slice =", q2_slice)
    if None is ainv : q2mul  = 1.
    else : q2mul = ainv**2
    q2pow   = kw.get('q2pow') or 0.

    if None is ax : ax = dv.make_std_axes()
    ls = latsize[0]
    assert (np.asarray(latsize[:3]) == ls).all()

    # find q2r
    q2r = ff_get_h5attr('q2_list', dgrp_ff, method_list, tsep_list)
    assert(not None is q2r)
    q2r = q2r[q2_slice]

    n_tsep = len(tsep_list)
    d_xr = (q2r.max() - q2r.min()) / len(q2r) *.5
    if None is x_sh : x_sh = d_xr *.4 / max(1, n_tsep) # +summation

    def plot_F2dF1(dgrp_x, x_sh, label_x, stg_x) :
        f1  = dgrp_x[:, 0, :, 0]
        f2  = dgrp_x[:, 0, :, 1]
        ys   = f2 / f1  * (q2r * q2mul)**q2pow * yfactor
        ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
        ax.errorbar(q2mul*(q2r + x_sh), ys_a, yerr=ys_e, label=label_x,
                    **stg_x.edotline())

    # plateau avg
    if 'ratio_pltx_avg' in method_list : 
        for i_tsep, tsep in enumerate(tsep_list) :
            plot_F2dF1(dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)], i_tsep * x_sh, r'$T=%da$' % tsep, stg_list[i_tsep])

    cnt_xsh = n_tsep
    if 'ratio_summ' in method_list : 
        plot_F2dF1(dgrp_ff['ratio_summ'], cnt_xsh * x_sh, 'summ', stg_summ)
        cnt_xsh +=1
    
    if 'fit_c3' in method_list : 
        plot_F2dF1(dgrp_ff['fit_c3'], cnt_xsh * x_sh, 'fit-c3', stg_fit_c3)
        cnt_xsh +=1

    ax.axhline(y=0, c='k', ls=':')

    #if None is xr : xr = [ q2mul*(q2r.min() - .5 * d_xr), q2mul*(q2r.max() + 1. * d_xr)]
    #ax.set_xlim(xr)
    #if not None is yr : ax.set_ylim(yr)

    #if None is xlabel : 
        #if None is ainv : ax.set_xlabel(r'$Q^2$ [lat]')
        #else : ax.set_xlabel(r'$Q^2$ [GeV${}^2$]')
    #else : ax.set_xlabel(xlabel)

    #if None is ylabel : ax.set_ylabel(r'$G_E/G_M$')
    #else : ax.set_ylabel(ylabel)
    #ax.legend()

    dv.set_std_axes(ax, **kw)

    return ax

def plot_q2dep_cmp_GE2GM(dgrp_ff, tsep_list, method_list,
        rsplan=('jk',1), ylabel=None, yr=None, xlabel=r'$Q^2$[lat]', xr=None, x_sh=None,
        ax=None, stg_list=dv.style_group_default, 
        stg_summ=dv.style_group_default[-1], 
        stg_fit_c3=dv.style_group_default[-2],
        **kw) :
    """ plot q2 dependence of a ff, compare methods
        q2 (x-axis is in lattice units, x=Q2 *a**2
    """
    xlabel  = kw.get('xl') or kw.get('xlabel') or None
    ylabel  = kw.get('yl') or kw.get('ylabel') or None
    ainv    = kw.get('ainv') or None
    mlat    = kw.get('mlat') or None
    q2_slice= kw.get('xslice') 
    if None is q2_slice : q2_slice = kw.get('q2_slice') 
    if None is q2_slice : q2_slice = np.s_[:]
    yfactor = kw.get('yfactor') or 1.
    print("q2_slice =", q2_slice)
    if None is ainv : q2mul  = 1.
    else : q2mul = ainv**2

    if None is ax : ax = dv.make_std_axes()
    ls = latsize[0]
    assert (np.asarray(latsize[:3]) == ls).all()

    # find q2r
    q2r = ff_get_h5attr('q2_list', dgrp_ff, method_list, tsep_list)
    assert(not None is q2r)
    q2r = q2r[q2_slice]

    n_tsep = len(tsep_list)
    d_xr = (q2r.max() - q2r.min()) / len(q2r) *.5
    if None is x_sh : x_sh = d_xr *.3 / max(1, n_tsep+1) # +summation+fit_c3

    def calc_GE(q2r, f1r, f2r) :
        return f1r - f2r * (q2r / 4. / mlat**2)
    def calc_GM(q2r, f1r, f2r) :
        return f1r + f2r

    def plot_GE2GM(dgrp_x, x_sh, label_x, stg_x) :
        f1  = dgrp_x[:, 0, :, 0]
        f2  = dgrp_x[:, 0, :, 1]
        ge  = calc_GE(q2r, f1[:, q2_slice], f2[:, q2_slice])
        gm  = calc_GM(q2r, f1[:, q2_slice], f2[:, q2_slice])
        ys   = ge / gm * yfactor
        ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
        ax.errorbar(q2mul*(q2r + x_sh), ys_a, yerr=ys_e, label=label_x,
                    **stg_x.edotline())

    # plateau avg
    if 'ratio_pltx_avg' in method_list : 
        for i_tsep, tsep in enumerate(tsep_list) :
            plot_GE2GM(dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)], i_tsep * x_sh, (r'$T=%da$' % tsep), stg_list[i_tsep])

    # summation
    cnt_xsh = n_tsep
    if 'ratio_summ' in method_list : 
        plot_GE2GM(dgrp_ff['ratio_summ'], cnt_xsh * x_sh, 'summ', stg_summ)
        cnt_xsh +=1
    # fit_c3
    if 'fit_c3' in method_list :
        plot_GE2GM(dgrp_ff['fit_c3'], cnt_xsh * x_sh, 'fit-c3', stg_fit_c3)
        cnt_xsh +=1

    ax.axhline(y=0, c='k', ls=':')

    #if None is xr : xr = [ q2mul*(q2r.min() - .5 * d_xr), q2mul*(q2r.max() + 1. * d_xr)]
    #ax.set_xlim(xr)
    #if not None is xr : ax.set_xlim(xr)

    #if not None is yr : ax.set_ylim(yr)

    #if None is xlabel : 
        #if None is ainv : ax.set_xlabel(r'$Q^2$ [lat]')
        #else : ax.set_xlabel(r'$Q^2$ [GeV${}^2$]')
    #else : ax.set_xlabel(xlabel)
    #if not None is xlabel : ax.set_xlabel(xlabel)
    
    #if None is ylabel : ax.set_ylabel(r'$G_E/G_M$')
    #else : ax.set_ylabel(ylabel)
    #ax.legend()

    dv.set_std_axes(ax, **kw)

    return ax

def runme_plot_all_ff(h5_fname, flav_list, obsv_list, tsep_list, method_list,
            **kw) :
    """ plot plateaus for gV, gA, ... """
    #print("runme_plot_all_ff: kw.keys=%s" % kw.keys())
    out_dir = kw.get('out_dir', '.') 
    mlat    = kw['mlat']
    
    h5f     = h5py.File(h5_fname, 'r')
    try : os.mkdir(out_dir)
    except OSError as ee : 
        if os.errno.EEXIST == ee[0] : pass
        else : raise OSError(ee)

    for flav, obsv in it.product(flav_list, obsv_list) :
        print(flav, obsv)
        dgrp_ff = h5f["/proton/tensor1/%s" %(flav,)]

        # find q2r, nonzero slice
        q2r = ff_get_h5attr('q2_list', dgrp_ff, method_list, tsep_list)
        assert(not None is q2r)
        q2_nz_slice= np.where(lhpd.NONZERO_TOL < np.abs(q2r))[0]
        #print(q2_nz_slice)
        if obsv in [ 
                    'F2', 'GP', 'B20', 'C20', 'B20p', 
                    'Q4F1', 'Q4F2', 'Q2F2dF1' ]:
            q2_slice = q2_nz_slice
        else : q2_slice = np.s_[:]

        obsv_latex_map ={ 'F1' : 'F_1', 'F2' : 'F_2', 
                          'GE' : 'G_E', 'GM' : 'G_M',
                          'GA' : 'G_A', 'GP' : 'G_P',
                          'A20': 'A_{20}', 'B20': 'B_{20}', 'C20': 'C_{20}',
                          'A20p': r'\tilde{A}_{20}', 'B20p': r'\tilde{B}_{20}' }
        obsv_op_map = { 'F1':'tensor1', 'F2':'tensor1', 
                        'GE':'tensor1', 'GM':'tensor1', 
                        'GA':'pstensor1', 'GP':'pstensor1',
                        'A20':'tensor2s2', 'B20':'tensor2s2', 'C20':'tensor2s2',
                        'A20p':'pstensor2s2', 'B20p':'pstensor2s2'}
        if obsv in obsv_latex_map.keys() :
            # F1(Q2)
            name='%s/ff-q2dep.%s.%s' % (out_dir, flav, obsv)
            print('# %s' % (name,))
            ax  = plot_q2dep_cmp_ff(
                        h5f["/proton/%s/%s" % (obsv_op_map[obsv], flav,)], 
                        obsv, tsep_list, method_list,
                        ylabel=r"$%s^{%s}$" % (obsv_latex_map[obsv], flav),
                        q2_slice=q2_slice,
                        yfactor=ZV,
                        ax=dv.class_logger(dv.make_std_axes()), 
                        **kw)
        
        elif 'Q4F1' == obsv :
            # Q4F1(Q2)
            name='%s/ff-q2dep.%s.Q4F1' % (out_dir, flav)
            print('# %s' % (name,))
            ax  = plot_q2dep_cmp(
                        h5f["/proton/tensor1/%s" % (flav,)], 0, tsep_list, method_list,
                        q2pow=2,    # multiply by q2^2
                        yfactor=ZV,
                        q2_slice=q2_slice,
                        ylabel=r"$Q^4 F_1^{%s}$" % (flav,),
                        ax=dv.class_logger(dv.make_std_axes()), 
                        **kw)

        elif 'Q4F2' == obsv :
            # Q4F2(Q2)
            name='%s/ff-q2dep.%s.Q4F2' % (out_dir, flav)
            print('# %s' % (name,))
            ax  = plot_q2dep_cmp(
                        dgrp_ff, 1, tsep_list, method_list,
                        q2pow=2,    # multiply by q2^2
                        yfactor=ZV,
                        ylabel=r'$Q^4 F_2^{%s}$' %(flav,), 
                        ax=dv.class_logger(dv.make_std_axes()), 
                        q2_slice=q2_slice,
                        **kw)

        elif 'Q2F2dF1' == obsv :
            name="%s/ff-q2dep.%s.Q2F2dF1" % (out_dir, flav)
            print('# %s' % (name,))
            ax  = plot_q2dep_cmp_F2dF1(
                        dgrp_ff, tsep_list, method_list,
                        q2_slice=q2_slice,
                        ylabel=r'$Q^2 F_2^{%s} / F_1^{%s}$' %(flav, flav),
                        q2pow=1, #multiply by
                        ax=dv.class_logger(dv.make_std_axes()),
                        **kw)

        elif 'GE2GM' == obsv :
            # GE/GM(Q2)
            name="%s/ff-q2dep.%s.GE2GM" % (out_dir, flav)
            print('# %s' % (name,))
            ax  = plot_q2dep_cmp_GE2GM(
                        dgrp_ff, tsep_list, method_list,
                        q2_slice=q2_slice,
                        ylabel=r'$G_E^{%s} / G_M^{%s}}$' %(flav, flav),
                        ax=dv.class_logger(dv.make_std_axes()),
                        **kw)
        
        else : raise ValueError(obsv )
        ax.legend(loc='best', numpoints=1)
        ax.figure.savefig(name+'.pdf')
        dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
        dv.close_fig(ax.figure)


# XXX copy/paste from analysis_ff.py
def plot_pltx_cmp(dgrp_ff, i_ff, i_q2, tsep_list, rsplan=('jk',1),
            ylabel=None, yr=None, xlabel=r'$\tau$', xr=None, xsh=None,
            ax=None, stg_list=dv.style_group_default, stg_summ=dv.style_group_default[-1], 
            method_list=['ratio_pltx_avg', 'ratio_pltx']) :
    """ quick & dirty plotting of plateaus pltx averages and methods """
    if None is ax : ax = dv.make_std_axes()

    max_tslice  = 3    # FIXME must be a parameter to ratio_pltx_avg, ratio_summ

    n_tsep = len(tsep_list)
    tmax = max(tsep_list)
    # plot ratio_pltx, ratio_pltx_avg
    if None is xsh : xsh = .4 / max(1, n_tsep - 1)

    # bands
    if 'ratio_pltx_avg' in method_list :
        for i_tsep, tsep in enumerate(tsep_list) :
            y2  = dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)][:, 0, i_q2, i_ff]
            y2_a, y2_e = lhpd.calc_avg_err(y2, rsplan)
            tr2 = np.r_[lhpd.latcorr.pltx_trange(tsep, max_tslice)]
            tr2[-1] -= 1
            ax.fill_between(tr2 + i_tsep * xsh, [y2_a - y2_e] * 2, [y2_a + y2_e] *2,
                            **stg_list[i_tsep].band())
            ax.plot(tr2 + i_tsep * xsh, [y2_a]*2, **stg_list[i_tsep].line())

    # plateaus
    if 'ratio_pltx' in method_list :
        for i_tsep, tsep in enumerate(tsep_list) :
            tr  = np.r_[0 : 1+tsep]
            y   = dgrp_ff['ratio_pltx/dt%d' % (tsep)][:, :, i_q2, i_ff]
            y_a, y_e = lhpd.calc_avg_err(y, rsplan)
            ax.errorbar(tr + i_tsep * xsh, y_a, yerr=y_e, label=(r'$T=%da$' % tsep),
                        **stg_list[i_tsep].edotline())

    # summation res
    if 'ratio_summ' in method_list :
        ys = dgrp_ff['ratio_summ'][:, 0, i_q2, i_ff]
        ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
        ax.errorbar([-1.], ys_a, yerr=ys_e, label='summ', **stg_summ.edot())

    if None is xr : xr = [-1.5, 1+tsep]
    ax.set_xlim(xr)
    ax.set_xlabel(r'$\tau$')
    if not None is  yr : ax.set_ylim(yr)
    if None is ylabel: ylabel = dgrp_ff.name
    ax.set_ylabel(ylabel)

    ax.legend()

    return ax

# XXX copy/paste from analysis_ff.py
def plot_pltx_cmp_qlist(dgrp_ff, i_ff, i_q2_list, tsep, rsplan=('jk',1),
            ylabel=None, yr=None, xlabel=r'$\tau$', xr=None, xsh=None,
            ax=None, stg_list=dv.style_group_default, stg_summ=dv.style_group_default[-1], 
            method_list=['ratio_pltx_avg', 'ratio_pltx'],
            labels=None, **kw) :
    """ quick & dirty plotting of plateaus pltx averages and methods """
    if None is ax : ax = dv.make_std_axes()

    max_tslice  = 3    # FIXME must be a parameter to ratio_pltx_avg, ratio_summ

    n_q2 = len(i_q2_list)
    tmax = 1+tsep
    yfactor = kw.get('yfactor') or 1.
    # plot ratio_pltx, ratio_pltx_avg
    if None is xsh : xsh = .4 / max(1, n_q2 - 1)

    # bands
    if 'ratio_pltx_avg' in method_list :
        for ii_q2, i_q2 in enumerate(i_q2_list) :
            y2  = yfactor * dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)][:, 0, i_q2, i_ff]
            y2_a, y2_e = lhpd.calc_avg_err(y2, rsplan)
            tr2 = np.r_[lhpd.latcorr.pltx_trange(tsep, max_tslice)]
            tr2[-1] -= 1
            ax.fill_between(tr2 + ii_q2 * xsh, [y2_a - y2_e] * 2, [y2_a + y2_e] *2,
                            **stg_list[ii_q2].band())
            ax.plot(tr2 + ii_q2 * xsh, [y2_a]*2, **stg_list[ii_q2].line())

    # plateaus
    if 'ratio_pltx' in method_list :
        for ii_q2, i_q2 in enumerate(i_q2_list) :
            tr  = np.r_[0 : 1+tsep]
            y   = yfactor * dgrp_ff['ratio_pltx/dt%d' % (tsep)][:, :, i_q2, i_ff]
            y_a, y_e = lhpd.calc_avg_err(y, rsplan)
            if None is labels : label_i = None
            else : label_i = labels[ii_q2]
            ax.errorbar(tr + ii_q2 * xsh, y_a, yerr=y_e, 
                        label=label_i, #label=(r'$T=%da$' % tsep),
                        **stg_list[ii_q2].edotline())

    # summation res
    if 'ratio_summ' in method_list :
        for i_q2, q2 in enumerate(tsep_list) :
            ys = yfactor * dgrp_ff['ratio_summ'][:, 0, i_q2, i_ff]
            ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
            ax.errorbar([-1.], ys_a, yerr=ys_e, #label='summ', 
                        **stg_summ.edot())

    if None is xr : xr = [-1.5, 1+tsep]
    ax.set_xlim(xr)
    ax.set_xlabel(r'$\tau$')
    if not None is  yr : ax.set_ylim(yr)
    if None is ylabel: ylabel = dgrp_ff.name
    ax.set_ylabel(ylabel)

    #ax.legend()

    return ax

if False :
    # XXX copy/paste from analysis_ff.py
    def plot_q2dep_cmp_ff(dgrp_ff, obsv, tsep_list, method_list,
            q2_slice=slice(None),
            rsplan=('jk',1), ylim=None, xlim=None, 
            xsh=None, xsh0=0.,
            ax=None, stg_list=dv.style_group_default, 
            stg_summ=dv.style_group_default[-1],
            stg_fit_c3=dv.style_group_default[-2],
            plot_callback=None,
            yfunc=None,
            **kw) :
        """ plot q2 dependence of a ff, compare methods
            q2 (x-axis is in lattice units, x=Q2 *a**2
        """
        xslice  = kw.get('xslice') or np.s_[:]
        ainv    = kw.get('ainv') or None
        mlat    = kw.get('mlat') or None
        yfactor = kw.get('yfactor') or 1.
        label_prefix=kw.get('label_prefix') or ''
        if None is ainv : q2mul  = 1.
        else : q2mul = ainv**2
        q2pow   = kw.get('q2pow') or 0.

        if None is ax : ax = dv.make_std_axes()
        ls = latsize[0]
        assert (np.asarray(latsize[:3]) == ls).all()

        if 'ratio_summ' in method_list :
            q2r = dgrp_ff['ratio_summ'].attrs['q2_list'][q2_slice]
        elif 'ratio_pltx_avg' in method_list :
            q2r = dgrp_ff['ratio_pltx_avg/dt%d' % tsep_list[0]].attrs['q2_list'][q2_slice]
        else : raise RuntimeError('cannot get q2 list')

        n_tsep  = len(tsep_list)
        d_xr    = (q2r.max() - q2r.min()) / len(q2r) *.5
        if None is xsh : xsh = d_xr *.4 / max(1, n_tsep+1) # +summation+fit_c3
        

        def plot_ffi(dgrp_x, xsh, label_x, stg_x) :
            # vector
            if   'F1' == obsv : ys  = dgrp_x[:, 0, :, 0]
            elif 'F2' == obsv : ys  = dgrp_x[:, 0, :, 1]
            elif 'GE' == obsv : ys  = dgrp_x[:, 0, :, 0] - q2r / (4. * mlat**2) * dgrp_x[:, 0, :, 1]
            elif 'GM' == obsv : ys  = dgrp_x[:, 0, :, 0] + dgrp_x[:, 0, :, 1]
            # axial
            elif 'GA' == obsv : ys  = dgrp_x[:, 0, :, 0]
            elif 'GP' == obsv : ys  = dgrp_x[:, 0, :, 1]
            # cpviol vector
            elif 'FA' == obsv : ys  = dgrp_x[:, 0, :, 0] 
            elif 'F3' == obsv : ys  = dgrp_x[:, 0, :, 1] 
            # error
            else : raise ValueError(obsv)

            xr  = q2mul*q2r[xslice]
            ys  = ys[:, q2_slice] * (q2r * q2mul)**q2pow * yfactor
            if not None is yfunc : 
                ys = yfunc(xr, ys)
            ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
            xr_plot = xr + q2mul * xsh
            ax.errorbar(xr_plot, ys_a[xslice], yerr=ys_e[xslice], label=label_x,
                        **stg_x.edotline())
            if not None is plot_callback :
                plot_callback(ax, xr, ys_a, ys_e, stg=stg_x, xsh=q2mul * xsh)

        # plateau avg
        if 'ratio_pltx_avg' in method_list : 
            for i_tsep, tsep in enumerate(tsep_list) :
                plot_ffi(dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)], i_tsep * xsh + xsh0, 
                    r'%s$(T=%d)$' % (label_prefix, tsep), stg_list[i_tsep])

        cnt_xsh = n_tsep
        # summation
        if 'ratio_summ' in method_list : 
            plot_ffi(dgrp_ff['ratio_summ'], cnt_xsh * xsh + xsh0, 'summ', stg_summ)
            cnt_xsh += 1
        if 'fit_c3' in method_list : 
            plot_ffi(dgrp_ff['fit_c3'], cnt_xsh * xsh + xsh0, 'fit-c3', stg_fit_c3)
            cnt_xsh += 1

        if None is xlim : xlim = [ q2mul*(q2r.min() - .5 * d_xr), q2mul*(q2r.max() + 1. * d_xr)]
        ax.set_xlim(xlim)

        ax.axhline(y=0, ls=":", c='k')
        
        if 'xl' in kw or 'xlabel' in kw :
            xlabel  = kw.get('xl') or kw.get('xlabel') or ''
        else :
            if None is ainv : xlabel = r'$Q^2$ [lat]'
            else : xlabel = r'$Q^2$ [GeV${}^2$]'
        ax.set_xlabel(xlabel)
        
        if not None is  ylim : ax.set_ylim(ylim)

        if 'yl' in kw or 'ylabel' in kw :
            ylabel  = kw.get('yl') or kw.get('ylabel') or ''
        else :
            ylabel = "%s:%s" % (dgrp_ff.name, obsv)
        ax.set_ylabel(ylabel)
        print("xlabel='%s'  ylabel='%s'" % (xlabel, ylabel))

        #ax.legend()

        return ax


# XXX TODO lib plans MANAGING KW PARAMETERS FOR PLOTIING FUNCTIONS ETC
# functionality: update (a copy of) dictionary
# * remove keys
# * set keys
# * replace keys if (present|None|some_condition)
# * handle key synonyms (ie replace with new common name, etc

# for paper, F3 plateaus
def plotpltx_ff(dgrp_ff, i_ff, i_q2, ss,
            **kw) :
    """ plot one item """
    #mlat    = kw['mlat']
    #latsize = kw['latsize']
    tskip   = kw.get('tskip') or 0
    rsplan  = kw['rsplan']
    max_tslice = kw.get('max_tslice') or 3
    #ipdb.set_trace()
    ax      = kw.get("ax")      or dv.make_std_axes()
    xsh     = kw.get("xsh") or kw.get("xshift") or 0   
    yfactor = kw.get('yfactor') or 1.
    label   = kw.get("label")   or str(ss)
    stg     = kw.get("stg")     or dv.style_group_None()
    method_list = kw.get('method_list') or ['ratio_pltx']
    
    tsep    = ss.get_tsep()

    # bands
    if 'ratio_pltx_avg' in method_list :
        y2  = yfactor * dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)][:, 0, i_q2, i_ff]
        y2_a, y2_e = lhpd.calc_avg_err(y2, rsplan)
        tr2 = np.array(lhpd.latcorr.pltx_trange(tsep, max_tslice), np.float)
        tr2[-1] -= .8
        tr2[ 0] -= .2
        dv.plot_ve_band(tr2 + xsh - tsep/2., y2_a, y2_e, ax, stg.cp(edgecolor='none'))
        #ax.fill_between(tr2 + xsh, [y2_a - y2_e] * 2, [y2_a + y2_e] *2,
        #                **stg.band())
        #ax.plot(tr2 + xsh, [y2_a]*2, **stg.line())

    # plateaus
    if 'ratio_pltx' in method_list :
        tr  = np.r_[tskip : 1+tsep-tskip]
        y   = yfactor * dgrp_ff['ratio_pltx/dt%d' % (tsep)][:, :, i_q2, i_ff]
        y   = y[:, tr]
        y_a, y_e = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(tr + xsh - tsep/2., y_a, yerr=y_e, label=(r'$T=%da$' % tsep),
                    **stg.edotline())

    # summation res
    #if 'ratio_summ' in method_list :
    #    ys = dgrp_ff['ratio_summ'][:, 0, i_q2, i_ff]
    #    ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
    #    ax.errorbar([-1.], ys_a, yerr=ys_e, label='summ', **stg_summ.edot())

    return ax


def plotpltx_ff_list_Iss(dgrp_ff, i_ff, i_q2, ssgrp,
        **kw):
    ax      = kw.get("ax")      or dv.make_std_axes()
    stg_list= kw.get("stg_list")or it.cycle(dv.style_group_default)
    n_plot  = len(ssgrp)
    #tmax = max(tsep_list)
    xsh     = .4 / max(1., n_plot - 1)
    xsh     = kw.get("xsh") or kw.get("xshift") or xsh

    stg_list_iter = iter(stg_list)
    purge_keys(kw, ["xsh", "label", "ax"])
    for i_ss, ss in enumerate(ssgrp) :
        #print(ss)
        plotpltx_ff(dgrp_ff, i_ff, i_q2, ss,
                    label="T=%d" % ss.get_tsep(), xsh=i_ss*xsh,
                    ax=ax, stg=next(stg_list_iter), **kw)

    xlim = ax.get_xlim()
    #ax.axhline(y=0, c='k', ls=':')

    purge_keys(kw, ["ax"])
    dv.set_std_axes(ax, **kw)
    #ax.legend()
    return ax

def plotpltx_ff_chart_Iss_Mq2(dgrp_ff, i_ff, i_q2_list, ssgrp,
        **kw) :
    """ iterate over mcgrp """
    #ipdb.set_trace()
    #mlat    = kw["mlat"]
    #latsize = kw["latsize"]
    method_list = kw.get('method_list') or ['ratio_pltx']
    n_ax    = len(i_q2_list)
    xl      = kw.get("xlabel") or kw.get("xl") or r'$\tau$'
    #yl      = kw.get("ylabel") or kw.get("yl") or yl_default.get(yval_type, None)
    adj_xlim= kw.get("adj_xlim") or False
    adj_ylim= kw.get("adj_ylim") or False

    ax_list = kw.get('ax_list') or None
    fig     = None
    if None is ax_list :
        panelx  = kw.get("panelx") or 6.0
        panely  = kw.get("panely") or 4.5
        ax_ncol = kw.get("ax_ncol") or 2
        ax_nrow = (n_ax + ax_ncol - 1) // ax_ncol
        figsize = kw.get("figsize")or [panelx * ax_ncol, panely*ax_nrow]
        fig     = plt.figure(figsize=figsize)
        closex  = kw.get("closex") or False
        closey  = kw.get("closey") or False
        ax_list = dv.make_yxsubplot(ax_ncol, ax_nrow, fig=fig,
                closex=closex, xlabel=xl,
                closey=closey, ylabel=yl)

    if 'ainv' in kw : 
        ainv    = kw['ainv']
        q2mult  = ainv**2
        q2unit  = '\mathrm{GeV}^2'
    else :
        ainv    = None
        q2mult  = 1
        q2unit  = 'lat'

    purge_keys(kw, ["figsize", "ax_ncol"])
    ax_list_iter = iter(ax_list)
    for i, i_q2 in enumerate(i_q2_list) :
        ax  = next(ax_list_iter)
        plotpltx_ff_list_Iss(dgrp_ff, i_ff, i_q2, ssgrp,
                ax=ax, **kw)
        if 'ratio_summ' in method_list :
            q2 = dgrp_ff['ratio_summ'].attrs['q2_list'][i_q2]
        elif 'ratio_pltx_avg' in method_list :
            q2 = dgrp_ff['ratio_pltx_avg/dt%d' % tsep_list[0]].attrs['q2_list'][i_q2]
        else : raise RuntimeError('cannot get q2 list')

        ax.text(.5, 0.1, r'$Q^2=%.2f\,%s$' % (q2 * q2mult, q2unit),
                size='x-small', ha='center', va='bottom', transform=ax.transAxes)

    if adj_xlim : dv.adjust_multiplot_xlim(ax_list[:n_ax])
    if adj_ylim : dv.adjust_multiplot_ylim(ax_list[:n_ax])
    return fig


# TODO move to lib
def h5_parse_rsplan(x) :
    # TODO support BS resampling map
    rsplan_id = str(x[0])
    return (rsplan_id,) + tuple([ int(i) for i in x[1:] ])

def numeqn_fits_c3(h5d) :
    disp_h5g= h5d.file["%s__DISP" % h5d.name]
    q2_list = h5d.attrs['q2_list']
    n_q2    = len(q2_list)
    n_eqn_tot = 0
    for i_q2, q2 in enumerate(q2_list) :
        disp_h5g_q2  = disp_h5g["q2_%d" % i_q2]
        n_data, n_eqn, n_p = disp_h5g_q2['c3_fitp'].shape
        n_eqn_tot += n_eqn
        
    return n_eqn_tot

def num_mc_c3(h5d) :
    disp_h5g= h5d.file["%s__DISP" % h5d.name]
    q2_list = h5d.attrs['q2_list']
    n_q2    = len(q2_list)
    n_mc_tot = 0
    for i_q2, q2 in enumerate(q2_list) :
        disp_h5g_q2  = disp_h5g["q2_%d" % i_q2]
        n_mc = disp_h5g_q2['eqmat'].shape[0]
        n_mc_tot += n_mc
        
    return n_mc_tot

def c3fit_exp0(e0_a, t_a, e0_b, t_b) :
    """ compute combination of ground-state 2pt fits, to divide from lattice c3pt 
            exp0(t_a,t_b) = c2pt0_a(t_a) / c0a * c2pt0_b(t_b) / c0b
        * for plotting me ~ ( c3pt_lat(t_a, t_b) / sqrt(c0a*c0b) ) / exp0(t_a,t_b) 
        return Real [i_data, i_t(t_a,t_b)] """
    e0_a = e0_a[..., None]
    e0_b = e0_b[..., None]
    return np.exp(-e0_a * t_a -e0_b * t_b)

def plot_fits_c3(h5d, ax_iter, 
                 **kw) :
    stg_list1= kw.get('stg_list', stg_list)
    band_dx = kw.get('band_dx', .2)
    disp_h5g= h5d.file["%s__DISP" % h5d.name]
    q2_list = h5d.attrs['q2_list']
    n_q2    = len(q2_list)
    
    tsep_all= []
    tau_all = []
    ax_list = []
    for i_q2, q2 in enumerate(q2_list) :
        disp_h5g_q2  = disp_h5g["q2_%d" % i_q2]
        c3_ttau    = disp_h5g_q2['c3_ttau'][()]          # [i_data, i_ttau, i_eqn]
        c3_p_r     = disp_h5g_q2['c3_fitp'][()]          # [i_data, i_eqn, i_p]
        c2src_p_r  = disp_h5g_q2['c2src_fitp_pc'][()]    # [i_data, i_eqn, i_p_c2]
        c2snk_p_r  = disp_h5g_q2['c2snk_fitp_pc'][()]    # [i_data, i_eqn, i_p_c2]
        ff_r       = h5d[:, 0, i_q2]                     # [i_data, i_ff]
        eqmat_pc   = disp_h5g_q2['eqmat_pc'][()]         # [i_eqn, i_ff]
        pcmat      = disp_h5g_q2['pcmat'][()]            # [i_mc, i_comp, reim, i_eqn]
        c3_chi2    = disp_h5g_q2['c3_chi2'][()]          # [i_data, i_eqn]
        c3_ndof    = disp_h5g_q2['c3_ndof'][()]          # []
        me_chi2    = disp_h5g_q2['me_chi2'][()]          # [i_data]
        me_ndof    = disp_h5g_q2['me_ndof'][()]          # []
        c3_psrc    = disp_h5g_q2['c3_psrc'][()]          # [i_mc, {x,y,z}]
        c3_psnk    = disp_h5g_q2['c3_psnk'][()]          # [i_mc, {x,y,z}]
        c3_tpol    = disp_h5g_q2['c3_tpol'][()]          # [i_mc]
        c3_op      = disp_h5g_q2['c3_op'][()]            # []
        c3_comp_list=disp_h5g_q2['c3_comp_list'][()]     # [i_comp]
        c3_pvalue_a= 1. - sp.stats.chi2(c3_ndof).cdf(lhpd.calc_avg(c3_chi2))  # [i_eqn]
        me_pvalue_a= 1. - sp.stats.chi2(c3_ndof).cdf(lhpd.calc_avg(me_chi2))  # []

        n_data, n_eqn, n_p = c3_p_r.shape
        c0ab_r     = np.sqrt(c2src_p_r[:, :, 0] * c2snk_p_r[:, :, 0]) # [i_data, i_eqn]
        me_p_r     = c3_p_r / c0ab_r[..., None]                       # [i_data, i_eqn, i_p]
        me_ttau    = c3_ttau / c0ab_r[..., None, :]                   # [i_data, i_ttau, i_eqn]

        rsplan     = h5_parse_rsplan(disp_h5g_q2['rsplan'][()])
        me0ff_ae   = lhpd.calc_avg_err((ff_r[:, None] * eqmat_pc).sum(-1), rsplan) # [i_eqn]
        me0_ae     = lhpd.calc_avg_err(me_p_r[:, :, 0], rsplan)                    # [i_eqn]
        
        # separate by tsep
        ttau    = disp_h5g_q2['ttau'][()]
        tsep_all.extend(ttau[:, 0])
        tau_all.extend(ttau[:, 1])
        n_ttau  = len(ttau)
        tsep_list = np.array(list(set(ttau[:, 0]))) ; 
        assert(0 < len(tsep_list))
        tsep_list.sort()
        # [i_tsep, i_tau]
        tau_r   = []
        i_ttau  = []
        for i_tsep, tsep in enumerate(tsep_list) :
            i_ttau_1 = np.where(tsep == ttau[:, 0])[0]
            assert(0 < len(i_ttau_1))
            i_tau_s = ttau[i_ttau_1, 1].argsort()
            i_ttau_1 = i_ttau_1[i_tau_s]
            i_ttau.append(i_ttau_1)
            tau_r.append(ttau[i_ttau_1, 1])
               
        n_tsep  = len(tsep_list)
        x_sh    = 0.3 / max(n_tsep - 1, 1)
        
        
        for i_eqn in range(n_eqn) :
            ax  = next(ax_iter)
            ax_list.append(ax)
            # [i_data, i_ttau, i_c3_p]

            # ground-state ME reconstructed from ff
            y_a, y_e = me0ff_ae[0][i_eqn], me0ff_ae[1][i_eqn]
            stg_me0ff = stg_list1[0].cp(lc='k', ls=':')
            ax.axhspan(y_a - y_e, y_a + y_e, **stg_me0ff.cp(linewidth=0.5, alpha_fc=.2).band())
            ax.axhline(y_a, **stg_me0ff.line())

            # ground-state ME from fits 
            y_a, y_e  = me0_ae[0][i_eqn], me0_ae[1][i_eqn]
            stg_me0 = stg_list1[0].cp(lc='k', ls='-')
            ax.axhspan(y_a - y_e, y_a + y_e, **stg_me0.cp(linewidth=0, alpha_fc=.4).band())
            ax.axhline(y_a, **stg_me0.line())

            # ME data points and fit ranges (normalized by ground-state c2pt)
            e0_a, e0_b = c2src_p_r[:, i_eqn, 1], c2snk_p_r[:, i_eqn, 1]
            de_a, de_b = c2src_p_r[:, i_eqn, 3], c2snk_p_r[:, i_eqn, 3]            
            for i_tsep, tsep in enumerate(tsep_list) :
                i_ttau_1 = i_ttau[i_tsep]
                tau_r_1 = tau_r[i_tsep]
                tau_min, tau_max = tau_r_1.min(), tau_r_1.max()
                tau_rb  = np.r_[tau_min, tau_max]                       # range-"box"

                tau_rc  = np.r_[tau_min-band_dx : tau_max+band_dx 
                                : 10j*(tau_max - tau_min+2*band_dx)]    # range-"continuous"
                dexp_tau_rc = lhpd.latcorr.c3fit_dexp_2state(de_a, tau_rc, de_b, tsep - tau_rc)     # [i_data, i_tau, i_p]
                # [i_data, i_t]
                c3fit_r  = (dexp_tau_rc * me_p_r[:, i_eqn, None, :]).sum(-1)
                c3fit_ae = lhpd.calc_avg_err(c3fit_r, rsplan)
                
                ax.fill_between(x_sh*i_tsep + tau_rc, c3fit_ae[0] - c3fit_ae[1], c3fit_ae[0] + c3fit_ae[1],
                        **stg_list1[i_tsep].band())
                
                # lattice c3 / sqrt(c2a0*c2b0)
                me_r    = me_ttau[:, i_ttau_1, i_eqn] / c3fit_exp0(e0_a, tau_r_1, e0_b, tsep - tau_r_1)
                me_ae   = lhpd.calc_avg_err(me_r, rsplan)
                ax.errorbar(x_sh*i_tsep + tau_r_1, me_ae[0], yerr=me_ae[1], 
                        **stg_list1[i_tsep].edotline())
            ax.text(0, 1, r'$(aQ)^2=%.4f$' % (q2,),
                    ha='left', va='top', transform=ax.transAxes)
            ax.text(1, 1, r'$p=%.4f$' % (c3_pvalue_a[i_eqn],),
                    ha='right', va='top', transform=ax.transAxes)
            i_nz_pcmat = np.where(0 != pcmat[:,:,:,i_eqn])
            i_mc, i_comp, reim = [ i[0] for i in i_nz_pcmat ]
            me_name_tex = lhpd.latcorr.matrix_elem_texstr_f2f_op(
                        c3_psrc[i_mc], c3_psnk[i_mc], c3_tpol[i_mc], 
                        c3_op, None, c3_comp_list[i_comp], reim)
            #print("i_nz_pcmat=%s  %s" % (str((i_mc, i_comp, reim)), me_name_tex))
            # TODO add matr.element name at
            ax.text(0.5, 0, r'$%s$' % me_name_tex,
                    ha='center', va='bottom', transform=ax.transAxes)

    tsep_all.sort()
    tsep_min, tsep_max = tsep_all[0], tsep_all[-1]
    tau_all.sort()
    tau_min, tau_max = tau_all[0], tau_all[-1]
    for ax in ax_list :
        ax.set_xlim([tau_min-1, tau_max+1])
        #ax.axhline(y=0, ls=':', c='k')


def plot_divj_c3(h5d, ssgrp, latsize, ax_iter, **kw) :
    """ 
    
        NOTE assuming that t_axis== len(latsize)-1 
    """
    stg_list1   = kw.get('stg_list', stg_list)
    tskip       = kw.get('tskip', 1)
    assert(0 < tskip)   # cannot compute t-deriv otherwise
    div3        = kw.get('div3', False)     # plot div3 ?

    # linear|sin3pt|bcc2pt
    deriv       = kw.get('deriv', 'linear')     

    # t2pt  : 2pt t-deriv, plot at midpoints [tskip+.5 : tsep-tskip-.5](inc)
    # t3pt  : 3pt t-deriv, plot at [tskip : tsep-tskip](inc)
    # t3pt_fit : as 't3pt', but t-deriv applied to c3pt/c3pt0 (ground state divided out)
    derivt      = kw.get('derivt', 't2pt')

    disp_h5g    = h5d.file["%s__DISP" % h5d.name]
    q2_list     = h5d.attrs['q2_list']
    n_q2        = len(q2_list)
    dim         = len(latsize)
    assert(4 == dim)    
    
    tsep_all    = []
    tau_all     = []
    ax_list     = []
    for i_q2, q2 in enumerate(q2_list) :
        disp_h5g_q2 = disp_h5g["q2_%d" % i_q2]
        rsplan      = h5_parse_rsplan(disp_h5g_q2['rsplan'][()])
        # iterate over mc, not eqn
        n_mc = disp_h5g_q2['eqmat'].shape[0]
        
        c2src_p_r   = disp_h5g_q2['c2src_fitp'][()] # [i_data, i_mc, i_p]
        c2snk_p_r   = disp_h5g_q2['c2snk_fitp'][()] # [i_data, i_mc, i_p]
        # [i_data, i_mc]
        e0_a, e0_b = c2src_p_r[:, :, 1], c2snk_p_r[:, :, 1]
        c0ab_r     = np.sqrt(c2src_p_r[:, :, 0] * c2snk_p_r[:, :, 0])

        ttau        = disp_h5g_q2['ttau'][()]
        n_ttau      = len(ttau)
        tsep_all.extend(ttau[:, 0])
        tau_all.extend(ttau[:, 1])
        tsep_list = np.array(list(set(ttau[:, 0])))
        assert(0 < len(tsep_list))
        tsep_list.sort()
        # [i_tsep, i_tau]
        tau_r   = [] 
        i_ttau  = []
        for i_tsep, tsep in enumerate(tsep_list) :
            i_ttau_1 = np.where(tsep == ttau[:, 0])[0]
            assert(0 < len(i_ttau_1))
            i_tau_s = ttau[i_ttau_1, 1].argsort()
            i_ttau_1 = i_ttau_1[i_tau_s]
            i_ttau.append(i_ttau_1)
            tau_r.append(ttau[i_ttau_1, 1])
        
        # [i_mc, i_comp]
        p3src       = disp_h5g_q2['c3_psrc'][()]
        p3snk       = disp_h5g_q2['c3_psnk'][()]
        qext        = p3snk - p3src
        # compute 4d deriv coeffs : need to average the t component over hypercube too
        if   'linear' == deriv :
            div_coeff   = lhpd.latcorr.deriv_ft_linear(qext, latsize[:-1])      # [i_mc, i_comp]
            avg_coeff   = np.ones((n_mc,), np.float64)                          # [i_mc]
        elif 'sin3pt' == deriv :
            div_coeff   = -1j * np.sin((2 * np.pi / latsize[:-1]) * qext)       # [i_mc, i_comp]
            avg_coeff   = np.ones((n_mc,), np.float64)                          # [i_mc]
        elif 'bcc2pt' == deriv :
            div_coeff   = lhpd.latcorr.deriv_ft_bcc2pt(qext, latsize[:-1])      # [i_mc, i_comp]
            avg_coeff   = lhpd.latcorr.avg_ft_bcc2pt(qext, latsize[:-1])        # [i_mc]
        else : raise ValueError(deriv)

        for i_mc in range(n_mc) :
            ax = next(ax_iter)
            ax_list.append(ax)
            
            i_plot = 0
            x_sh = .3 / (max(1, len(ssgrp) - 1))
            for ss in ssgrp :
                tsep = ss.get_tsep()
                tr_full = np.r_[ : 1 + tsep]

                # c2 denominator [i_data, i_t]
                #c2_den_r = c3fit_exp0(e0_a[:, i_mc], tr, e0_b[:, i_mc], tsep - tr)
                c2f_den_r = c3fit_exp0(e0_a[:, i_mc], tr_full, e0_b[:, i_mc], tsep - tr_full) # sic! need full t range for t3pt_fit

                # [i_data, t, i_comp, reim]
                #c3_r = disp_h5g_q2["c3_data/%s" % ss.strkey()][:, tr, i_mc, :, :] 
                c3f_r = disp_h5g_q2["c3_data/%s" % ss.strkey()][:, tr_full, i_mc, :, :]     # sic! need full t range for t-deriv
                assert(c3f_r.shape[-2:] == (dim, 2))
                # [i_data, t, i_comp]
                c3cf_r       = c3f_r[..., 0] + 1j * c3f_r[..., 1]

                # space div3 [i_data, t]
                c3cf_div3_r = (div_coeff[i_mc] * c3cf_r[..., :-1]).sum(-1)
                if   derivt in ['t2pt', 't2pt_fit'] :
                    tr      = np.r_[ tskip : tsep - tskip ]     # data indices
                    tr_plt  = tr + 0.5                          # x-axis values
                    c2_den_r    = 0.5 * (c2f_den_r[:, tr] + c2f_den_r[:, tr + 1])
                    c3c_div3_r  = 0.5 * (c3cf_div3_r[..., tr] + c3cf_div3_r[..., tr + 1])
                elif derivt in ['t3pt', 't3pt_fit'] :
                    tr      = np.r_[ tskip : 1 + tsep - tskip ] # data indices
                    tr_plt  = tr                                # x-axis values
                    c2_den_r    = c2f_den_r[..., tr]
                    c3c_div3_r  = c3cf_div3_r[..., tr]
                else : raise ValueError(derivt)

                # time component
                c3cft_r     = c3cf_r[..., -1]
                if   't2pt' == derivt :
                    c3c_divt_r  = avg_coeff[i_mc] * (c3cft_r[..., tr + 1]  - c3cft_r[..., tr])
                elif 't2pt_fit' == derivt :
                    raise NotImplementedError(derivt)
                elif 't3pt' == derivt :
                    c3c_divt_r  = avg_coeff[i_mc] * 0.5 * (c3cft_r[..., tr + 1]  - c3cft_r[..., tr - 1])
                elif 't3pt_fit' == derivt :
                    c3d2t_r = c3cft_r / c2f_den_r
                    c3c_divt_r  = ( (e0_b[:, i_mc] - e0_a[:, i_mc])[..., None] * c3cft_r[..., tr]
                                    + c2f_den_r[..., tr] * 0.5 * (c3d2t_r[..., tr + 1] - c3d2t_r[..., tr - 1]) )
                    c3c_divt_r  = avg_coeff[i_mc] * c3c_divt_r 
                else : raise ValueError(derivt)
                    
                # bcc div J [i_data, t-1/2]
                c3c_div_r   = c3c_divt_r + c3c_div3_r
                

                mec_div_r = (c3c_div_r / c0ab_r[:, i_mc, None] / c2_den_r)
                yr_ae = lhpd.calc_avg_err(mec_div_r.real, rsplan)
                yi_ae = lhpd.calc_avg_err(mec_div_r.imag, rsplan)
                ax.errorbar(tr_plt + x_sh * i_plot, # midpoint
                            yr_ae[0], yerr=yr_ae[1], 
                            **stg_list[i_plot].edotline())
                ax.errorbar(tr_plt + x_sh * i_plot, # midpoint
                            yi_ae[0], yerr=yi_ae[1], 
                            **stg_list[i_plot].cp(mfc='none', ls='--').edotline())

                if div3 :
                    mec_div3_r = (c3c_div3_r / c0ab_r[:, i_mc, None] / c2_den_r)
                    zr_ae = lhpd.calc_avg_err(mec_div3_r.real, rsplan)
                    zi_ae = lhpd.calc_avg_err(mec_div3_r.imag, rsplan)
                    ax.errorbar(tr_plt + x_sh * i_plot, # midpoint
                                zr_ae[0], yerr=zr_ae[1], 
                                **stg_list[i_plot].cp(ls=':'). edotline())
                    ax.errorbar(tr_plt + x_sh * i_plot, # midpoint
                                zi_ae[0], yerr=zi_ae[1], 
                                **stg_list[i_plot].cp(mfc='none', ls='-.').edotline())

                i_plot += 1

        ax.axhline(y=0, ls=':', c='k')

