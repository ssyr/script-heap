from __future__ import print_function
from past.builtins import execfile
execfile("pproc_nde.py")


"""
# example: only diagonal
mc_dict = make_mom_combo_list(.5, latsize, [[0,0,0]], list(lhpd.int_vectors_maxnorm(3, 10)))
q2_list = sorted(mc_dict.keys())
mc_list = [ mc_dict[q2] for q2 in q2_list ]
flav='U-D'
op='tensor1'
method_list=[('ratio_pltx',), ('ratio_pltx_avg',), ('ratio_summ',)]
# calc
x=calc_ff('src3', 'sink_3.0', had, Tpol_proton3, flav, op, op_default_ir_list(op), mhad, mc_list[1], [12], method_list, apponly=True, rsplan=('jk',96))
# calc&save
calc_save_ff(h5py.File('tmp.h5', 'a'), '/tmp', 'src3', 'sink_3.0', had, Tpol_proton3, flav, op, op_default_ir_list(op), mhad, mc_list[0:3], tsep_list, method_list, apponly=True)
"""

wl = list(it.product(
                zip(srctype_list, snktype_list), 
                ['U-D', 'U+D'], 
                op_list
                ))

if not 'job_key' in globals() :
    print("'job_key' NOT FOUND")
    print("# n_jobs = %d" % len(wl))
    raise RuntimeError

had = 'proton_3'
(s0,s1), flav, op = wl[job_key]

fname   = "%s/ff/ff.%s.%s.%s.%s.%s.h5" % (out_data_prefix, s0, s1, had, flav, op)

print("# %s, %s, %s -> '%s'" % (str((s0,s1)), flav, op, fname), time.asctime())
runme_calc_save_ff(fname, Tpol_proton3, mhad, 
            srctype_list=[s0], snktype_list=[s1], had_list=[had],
            op_list=[op], flav_list=[flav], rsplan=rsplan, apponly=True)
print(time.asctime())
