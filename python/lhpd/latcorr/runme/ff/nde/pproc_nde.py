from __future__ import print_function
from past.builtins import execfile
from future.utils import iteritems
import numpy as np
import h5py
import lhpd
from lhpd.misc import np_find_first, np_match
import os, sys, time, tempfile, errno
import dataview as dv
from dataview import latexize

from lhpd.pymath.H4_tensor import *


execfile("analysis.py")

# XXX
# needs config to define the following
# * out_data_prefix
# * chromabb_ver 
# * t_step_2pt


###############################################################################
# BB section
###############################################################################

import lhpd

def conv_bb2hdf(h5g, h5key, latsize, bbfile_func, 
            cfgkey, srcgrp_list, tsep, qext_list, lpath_list, 
            attrs_kv,
            t_dir=3, bb_dtype=np.complex64, h5_overwrite=True, 
            chromabb_ver=chromabb_ver) :
    """
        srcgrp_list [i_LP, i_grp, i_csrc, mu] - 2-level grouping 8(LP dir)*4(file)*3(src/file)
        
        save data to
        h5g[h5key][n_data, n_gamma, len(lpath_list), len(qext_list), tsep+1]
    """
    """ TEST 
pdb on
cfgkey_this='1480'
h5out=h5py.File('tmp.h5', 'a')
bbfile_func=mkfunc_bbfile(ama_apprx, 'src3', 'sink_3.0', 'proton_3', 'U', [0,0,0], 12)
srcgrp_list=get_srcgrp_list(cfgkey_this)
qext_list=lhpd.latcorr.make_mom_list(10)
lpath_list=[[]]+[[i] for i in range(8)]
conv_bb2hdf(h5out, '/tmp/U/px0py0pz0/src3/snk3', [32,32,32,64], bbfile_func, cfgkey_this, srcgrp_list, 12, qext_list, lpath_list, attrs_kv)
    """
    latsize         = np.asarray(latsize)
    lt              = latsize[t_dir]
    srcgrp_list     = np.asarray(srcgrp_list)
    nA, nB          = srcgrp_list.shape[0:2]
    assert(4 == srcgrp_list.shape[-1])

    qext_list       = np.array(qext_list)
    qext_tuple_list = [ tuple(q) for q in qext_list ]
    n_qext          = len(qext_list)
    n_gamma         = 16
    n_lpath         = len(lpath_list)
    # to reorder linkpaths exactly as in lpath_list
    lpath_tuple_list= [ tuple(l) for l in lpath_list ]

    csrc_list = []
    bb_srcgrp_list = []
    for iA in range(nA) :
        for iB in range(nB) :
            srcgrp = srcgrp_list[iA][iB]
            print(iA, iB, '{', ', '.join( [ str(c) for c in srcgrp ]), '} # ', time.asctime())
            n_src   = len(srcgrp)
            t_list  = [ c[t_dir] for c in srcgrp ]
            # discard the sample if error occurs
            try :
                bb_i = np.empty((n_src, n_gamma, n_lpath, n_qext, 1+tsep), bb_dtype)
                # here comes the ugliness
                if 3 == chromabb_ver :
                    for i_qext, qext in enumerate(qext_list) :
                        bbfile = bbfile_func(cfgkey, iA, iB, srcgrp, qext)
                        #print(bbfile)
                        # x[i_lp, i_gamma, t] :
                        x, x_lp_list, x_qext = lhpd.latcorr.read_chroma_bbfile(bbfile)
                        assert(3 == len(x.shape))
                        assert(np.allclose(x_qext, qext)) # FIXME handle multi-qext
                        x_lp_map = dict([ (tuple(l), i) for i,l in enumerate(x_lp_list) ])
                        i_lp = np.asarray([ x_lp_map[l] for l in lpath_tuple_list ])
                        # x[i_gamma, i_lp_reqd, t] :
                        x = x[i_lp].transpose(1,0,2)
                        for i_src, t in enumerate(t_list) :
                            if t + tsep < lt : y = x[..., t : t + tsep + 1]
                            else : y  = np.concatenate((x[..., t:], x[..., : t + tsep + 1 - lt]), axis=-1)
                            bb_i[i_src, :, :, i_qext] = y
                elif 4 == chromabb_ver :
                    bbfile  = bbfile_func(cfgkey, iA, iB, srcgrp, None)
                    # return [i_qext, i_lp, i_gamma, t]
                    x, x_lp_list, x_qext_list = lhpd.latcorr.read_chroma_bbfile(bbfile)
                    assert(4 == len(x.shape))
                    # reorder qext
                    x_qext_tuple_list = [ tuple(q) for q in x_qext_list ]
                    m1, m2 = np_match(qext_tuple_list, x_qext_tuple_list)
                    assert(len(m1) == len(qext_list))
                    assert((np.r_[:len(qext_list)] == m1).all())
                    x   = x[m2]
                    # reorder lpath
                    x_lp_map = dict([ (tuple(l), i) for i,l in enumerate(x_lp_list) ])
                    i_lp = np.asarray([ x_lp_map[l] for l in lpath_tuple_list ])
                    x   = x[:, i_lp]
                    # x[i_gamma, i_lp_reqd, i_qext, t] :
                    x = x.transpose(2,1,0,3)
                    for i_src, t in enumerate(t_list) :
                        if t + tsep < lt : y = x[..., t : t + tsep + 1]
                        else : y  = np.concatenate((x[..., t:], x[..., : t + tsep + 1 - lt]), axis=-1)
                        bb_i[i_src] = y
                else : raise ValueError("unknown chromabb_ver=%d" % chromabb_ver)

                bb_srcgrp_list.append(bb_i)
                csrc_list.extend(srcgrp)
            #except : # catch-all
            except RuntimeError:
                # TODO parse I/O exceptions
                print(" *** [%s %d %d] exception '%s'" % (cfgkey, iA, iB, str(sys.exc_info()[0])))

    meas_spec_list = np.array(
                [ (cfgkey, c) for c in csrc_list],
                dtype=[('ckpoint_id', 'S32'), ('source_coord', np.int32, (4,))])
    n_data = len(meas_spec_list)

    if h5_overwrite :
        try : del h5g[h5key]
        except : pass
    bb_shape = (n_data, n_gamma, len(lpath_list), len(qext_list), tsep + 1)
    b = h5g.require_dataset(h5key, bb_shape, bb_dtype, fletcher32=True)
    b[:] = np.concatenate(bb_srcgrp_list, axis=0)

    # "axes"
    b.attrs['dim_spec']      = np.array(['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau' ],
                                           dtype='S32')
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(b, meas_spec_list)
    b.attrs['gamma_list']    = np.r_[0 : 16]
    b.attrs['linkpath_list'] = np.array(
            [ lhpd.aff_io.aff_key_bb_linkpath_str(lp) for lp in lpath_list ],
            dtype='S16')
    b.attrs['qext_list']     = np.array(qext_list, dtype=np.int32)
    b.attrs['tau_list']      = np.r_[0 : tsep+1]
    # other parameters
    b.attrs['source_sink_dt']= int(tsep)

    #b.attrs['source_hadron'] = str(had)
    #b.attrs['sink_hadron']   = str(had)
    #b.attrs['sink_mom']      = np.array(psnk, dtype=np.int32)
    #b.attrs['time_neg']      = int(time_neg)
    for k, v in iteritems(attrs_kv) : 
        b.attrs[k] = v

    b.file.flush()

def get_srcgrp_list(cfgkey, ama) :
    shape   = ama['srcgrp_shape']
    nA      = shape[0]
    return np.asarray([ np.fromfile(
                            '%s/srcgrp_list/%s/csrc_list.%s.%d' % (
                                    out_data_prefix, 
                                    {'exact':'ex', 'sloppy':'sl'}[ama['approx']], 
                                    cfgkey, iA),
                            np.uint, sep=' ').reshape(shape[1:]) 
                        for iA in range(nA) ])

def runme_conv_bb_sample(cfgkey, ama,
                 srctype_list=srctype_list, 
                 snktype_list=snktype_list, 
                 had_list=had_list,
                 flav_list=flav_list, 
                 psnk_list=psnk_list, 
                 tsep_list=tsep_list, 
                 qext2_max=qext2_max,
                 lpath_min=lpath_min, lpath_max=lpath_max, 
                 attrs_kv={}) :
    srcgrp_list = get_srcgrp_list(cfgkey, ama)
    qext_list   = lhpd.latcorr.make_mom_list(qext2_max)
    lpath_list  = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)
    nA, nB      = srcgrp_list.shape[0:2]
    for srctype, snktype, had, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list, flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, flav, psnk, tsep, time.asctime())
        # output bb h5
        h5file  = get_bb_file_h5(cfgkey, ama, srctype, snktype, had, flav, psnk, tsep)
        lhpd.mkpath(os.path.dirname(h5file))
        print("# > %s" % h5file)
        h5f = h5py.File(h5file, 'a')
        h5kpath = "/cfg%s/%s/%s/%s_dt%d" % (cfgkey, had, flav, psnk_str(psnk), tsep)
        print("# >>> %s " % h5kpath)
        bbfile_func = mkfunc_bbfile(ama, srctype, snktype, had, flav, psnk, tsep)
        if False : # test bbfile names
            for iA in range(nA) :
                for iB in range(nB) :
                    for qext in qext_list :
                        bbfile = bbfile_func(cfgkey, iA, iB, srcgrp_list[iA, iB], qext)
                        try : os.stat(bbfile)
                        except : print("os.stat error on ", bbfile)
        attrs_kv_this = dict(attrs_kv)
        attrs_kv_this.update(
                    source_hadron=had, sink_hadron=had, 
                    sink_mom=psnk, time_neg=False)

        attrs_kv_this['sink_mom'] = psnk
        conv_bb2hdf(h5f, h5kpath, latsize, bbfile_func, cfgkey, 
                    srcgrp_list, tsep, qext_list, lpath_list, attrs_kv_this)

        h5f.flush()
        h5f.close()



def runme_bin_bb(ama,
            cfgkey_list=cfgkey_list,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt,
            flav_list=flav_list,
            psnk_list=psnk_list,
            tsep_list=tsep_list,
            VERBOSE=False) :
    for srctype, snktype, had, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list,  
                flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, flav, psnk, tsep, time.asctime())
        h5_fname= get_bb_bin_file_h5(ama, srctype, snktype, had, flav, psnk, tsep)
        lhpd.mkpath(os.path.dirname(h5_fname))

        h5_kpath= '%s/%s/%s_dt%d' % (
                        had, flav, psnk_str(psnk), tsep)
        h5_list = [ ( get_bb_file_h5(cfgkey, ama, srctype, snktype, 
                                     had, flav, psnk, tsep),
                      '/cfg%s/%s' % (cfgkey, h5_kpath) )
                    for cfgkey in cfgkey_list ]

        lhpd.h5_io.h5file_bin(h5_fname, h5_kpath, h5_list, VERBOSE=VERBOSE)

def runme_bin_unbias_bb(
            cfgkey_list=cfgkey_list,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt,
            flav_list=flav_list,
            psnk_list=psnk_list,
            tsep_list=tsep_list,
            VERBOSE=False) :
    for srctype, snktype, had, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list, 
                flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, flav, psnk, tsep, time.asctime())
        h5_fname= get_bb_unbias_file_h5(srctype, snktype, had, flav, psnk, tsep)
        lhpd.mkpath(os.path.dirname(h5_fname))

        h5_kpath= '/%s/%s/%s_dt%d' % (
                        had, flav, psnk_str(psnk), tsep)
        h5_list_ap = [ [ ( get_bb_file_h5(cfgkey, ama_apprx, srctype, snktype, 
                                          had, flav, psnk, tsep),
                           '/cfg%s/%s' % (cfgkey, h5_kpath) ) ] for cfgkey in cfgkey_list ]
        h5_list_ex = [ [ ( get_bb_file_h5(cfgkey, ama_exact, srctype, snktype,
                                          had, flav, psnk, tsep),
                           '/cfg%s/%s' % (cfgkey, h5_kpath) ) ] for cfgkey in cfgkey_list ]

        lhpd.h5_io.h5file_group_bin_unbias(h5_fname, h5_kpath,
                    h5_list_ap, h5_list_ex, VERBOSE=VERBOSE)

###############################################################################
# CALC_OP section
###############################################################################

def runme_calc_op_sample(cfgkey, ama,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt, 
            op_list=op_list,
            flav_list=flav_list, 
            psnk_list=psnk_list_c3pt,
            tsep_list=tsep_list
            ) :
    """ cycle over correlators """
    for srctype, snktype, had, op, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list, op_list, 
                flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, op, flav, psnk, tsep, time.asctime())
        ir_list = op_default_ir_list(op)

        # input bb
        fname_in    = get_bb_file_h5(cfgkey, ama, srctype, snktype, had, flav, psnk, tsep)
        kpath_in    = '/cfg%s/%s/%s/%s_dt%d' % (cfgkey, had, flav, psnk_str(psnk), tsep)
        h5f_in      = h5py.File(fname_in, 'r')
        h5_bb       = h5f_in[kpath_in]
        
        # output op
        h5_fname= get_op_file_h5(cfgkey, ama, srctype, snktype, had, op, flav, psnk, tsep)
        lhpd.mkpath(os.path.dirname(h5_fname))
        h5_file = h5py.File(h5_fname, 'a')

        for ir_name, ir_scale in ir_list :
            print('# ', had, srctype, snktype, flav, psnk, tsep, op, ir_name, time.asctime())
            h5_kpath= '/%s/%s/%s/%s_dt%d/%s' % (
                        had, op, flav, 
                        psnk_str(psnk), tsep, ir_name)
            calc_save_op(h5_file, h5_kpath, 
                         had, flav, op, ir_name, 
                         psnk, tsep, 
                         latsize, t_axis, h5_bb)

        h5_file.close()



def runme_merge_op(ama,
            cfgkey_list=cfgkey_list,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt,
            op_list=op_list, 
            flav_list=flav_list,
            psnk_list=psnk_list,
            tsep_list=tsep_list,
            binsize=None
        ) :
    do_bin = not None is binsize and 0 < binsize
    for srctype, snktype, had, op, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list, op_list, 
                flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, op, flav, psnk, tsep, time.asctime())
        if do_bin : h5_fname= get_op_bin_file_h5(ama, srcstr, snkstr, had, op, flav, psnk, tsep)
        else :      h5_fname= get_op_all_file_h5(ama, srcstr, snkstr, had, op, flav, psnk, tsep)
        lhpd.mkpath(os.path.dirname(h5_fname))
        h5_file = h5py.File(h5_fname, 'a')
        for ir_name, ir_scale in op_default_ir_list(op) :
            h5_kpath= '/%s/%s/%s/%s_dt%d/%s' % (
                            had, op, flav, 
                            psnk_str(psnk), tsep,
                            ir_name)
            h5_list = [ ( get_op_file_h5(cfgkey, ama, srctype, snktype, 
                                         had, op, flav, psnk, tsep),
                          h5_kpath)
                        for cfgkey in cfgkey_list ]

            if do_bin : lhpd.h5_io.h5file_merge_bin(h5_fname, h5_kpath, h5_list, binsize, VERBOSE=True)
            else      : lhpd.h5_io.h5file_merge(h5_fname, h5_kpath, h5_list, VERBOSE=True)
            h5_file.flush() # just in case

        # finalize
        h5_file.close()

def runme_bin_unbias_op(
            cfgkey_list=cfgkey_list,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt,
            op_list=op_list, 
            flav_list=flav_list,
            psnk_list=psnk_list,
            tsep_list=tsep_list,
            VERBOSE=False) :
    for srctype, snktype, had, op, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list, op_list, 
                flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, op, flav, psnk, tsep, time.asctime())
        h5_fname= get_op_unbias_file_h5(srctype, snktype, had, op, flav, psnk, tsep)
        lhpd.mkpath(os.path.dirname(h5_fname))
        for ir_name, ir_scale in op_default_ir_list(op) :
            h5_kpath= '/%s/%s/%s/%s_dt%d/%s' % (
                            had, op, flav, 
                            psnk_str(psnk), tsep,
                            ir_name)
            h5_list = [ ( get_op_file_h5(cfgkey, ama_apprx, srctype, snktype, 
                                         had, op, flav, psnk, tsep),
                          h5_kpath,
                          get_op_file_h5(cfgkey, ama_exact, srctype, snktype,
                                         had, op, flav, psnk, tsep),
                          h5_kpath)
                        for cfgkey in cfgkey_list ]

            lhpd.h5_io.h5file_bin_unbias(h5_fname, h5_kpath, h5_list, VERBOSE=VERBOSE)

def runme_bin_op(ama,
            cfgkey_list=cfgkey_list,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt,
            op_list=op_list, 
            flav_list=flav_list,
            psnk_list=psnk_list,
            tsep_list=tsep_list,
            VERBOSE=False) :
    for srctype, snktype, had, op, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list, op_list, 
                flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, op, flav, psnk, tsep, time.asctime())
        h5_fname= get_op_bin_file_h5(ama, srctype, snktype, had, op, flav, psnk, tsep)
        lhpd.mkpath(os.path.dirname(h5_fname))
        for ir_name, ir_scale in op_default_ir_list(op) :
            h5_kpath= '/%s/%s/%s/%s_dt%d/%s' % (
                            had, op, flav, 
                            psnk_str(psnk), tsep,
                            ir_name)
            h5_list = [ ( get_op_file_h5(cfgkey, ama, srctype, snktype, 
                                         had, op, flav, psnk, tsep),
                          h5_kpath)
                        for cfgkey in cfgkey_list ]

            lhpd.h5_io.h5file_bin(h5_fname, h5_kpath, h5_list, VERBOSE=VERBOSE)



# XXX unused (will raise an exception)
def runme_bin_op_OLD(cfgkey_list, binsize,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt,
            op_list=op_list, 
            flav_list=flav_list,
            psnk_list=psnk_list,
            tsep_list=tsep_list
        ) :
    raise RuntimeError("use runme_merge_op(binsize=your_binsize)")
    for srctype, snktype, had, op, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list, op_list, 
                flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, op, flav, psnk, tsep, time.asctime())
        h5_fname= '%s/op/%s/%s/op-all.%s.%s.%s.%s_dt%d.h5' % (
                    out_data_prefix, srctype, snktype, 
                    had, op, flav, psnk_str(psnk), tsep)
        h5_file = h5py.File(h5_fname, 'a')
        for ir_name, ir_scale in op_default_ir_list(op) :
            h5_kpath= '/%s/%s/%s/%s_dt%d/%s' % (
                            had, op, flav, 
                            psnk_str(psnk), tsep,
                            ir_name)
            h5_list = [ ( get_op_file_h5(cfgkey, ama, srctype, snktype, 
                                         had, op, flav, psnk, tsep),
                          h5_kpath)
                        for cfgkey in cfgkey_list ]

            lhpd.h5_io.h5file_merge_bin(h5_fname, h5_kpath, h5_list, binsize, VERBOSE=True)
            h5_file.flush() # just in case

        # finalize
        h5_file.close()


###############################################################################
# CALC_FF section
###############################################################################


###############################################################################
# HADSPEC section
###############################################################################

def conv_hadspec2hdf(h5g, h5key, cfgkey, ama, 
            src_sm, snk_sm, srcgrp_list, attrs_kv,
            h5_overwrite=True, hs_dtype=np.complex128,
            srctype_list=srctype_list, 
            snktype_list=snktype_list) :
    """ 
        XXX if snk_sm=='P', len(snktype_list) must be =1
    """
    nA, nB, nC  = srcgrp_list.shape[0:3]
    n_srctype   = len(srctype_list)
    n_snktype   = len(snktype_list)
    if 'P' == snk_sm : assert(1 == n_snktype)

    lt          = latsize[t_dir]
    chromahadspec_barlist = [
        "proton_1", "lambda_1", "delta_1",  "proton_2", 
        "lambda_2", "delta_2",  "proton_3", "lambda_3",
        "delta_3",  "proton_4", "proton_5", "proton_6",
        "lambda_4", "xi_1",     "lambda_5",     "xi_2",
        "proton_negpar_3" ]
    chromahadspec_meslist = [
        "a0_1", "rho_x_1", "rho_y_1", "b1_z_1",
        "rho_z_1", "b1_y_1", "b1_x_1", "pion_2",
        "a0_2", "rho_x_2", "rho_y_2", "a1_z_1",
        "rho_z_2", "a1_y_1", "a1_x_1", "pion_1" ]
    chromahadspec_hadlist = chromahadspec_barlist + chromahadspec_meslist
    sm_tag = {'P':'Point', 'S':'Shell'}
    n_barlist   = len(chromahadspec_barlist)
    n_meslist   = len(chromahadspec_meslist)
    n_had       = len(chromahadspec_hadlist)
    psnk_list   = None
    n_psnk      = None
    csrc_list   = []
    res_list    = []
    """
      <Shell_Point_Wilson_Mesons>
      <Shell_Point_Wilson_Baryons>
      <Shell_Shell_Wilson_Mesons>
      <Shell_Shell_Wilson_Baryons>
    """
    xslt_fd, xslt_file = tempfile.mkstemp(suffix=cfgkey) ; os.close(xslt_fd)
    for iA in range(nA) :
        for iB in range(nB) :
            srcgrp = srcgrp_list[iA, iB]
            for i in range(len(srcgrp) - 1) :
                assert((lt + srcgrp[i+1][t_axis] - srcgrp[i][t_axis]) % lt == t_step_2pt)
            try :
                res_iAB = None
                
                for i_srctype, srctype in enumerate(srctype_list) :
                    
                    hs_file = get_chromahadspec_file(cfgkey, ama, srctype, "", srcgrp)
                    #print(srctype, iA, iB, time.asctime())
                    xslt_f = open(xslt_file, 'w')
                    xslt_f.write("""<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- print <Shell|Point>_<Shell|Point>_Wilson_<Baryons|Mesons> records -->
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
<xsl:template match="/hadspec/Wilson_hadron_measurements">
  <xsl:for-each select="elem[SourceSinkType/sink_type_1='%s_SINK']">
    <!-- Baryons -->
    <xsl:for-each select="%s_%s_Wilson_Baryons/elem">
      <xsl:for-each select="momenta/elem">
        <xsl:value-of select="../../baryon_num"/>  
        <xsl:text> </xsl:text>
        <xsl:value-of select="../../../../t0"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="sink_mom_num"/>  
        <xsl:text>    </xsl:text>
        <xsl:value-of select="sink_mom"/>
        <xsl:text>    </xsl:text>
        <xsl:for-each select="barprop/elem">
          <xsl:value-of select="re"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="im"/>
          <xsl:text>    </xsl:text>
        </xsl:for-each>
        <xsl:text>&#xa;</xsl:text>
      </xsl:for-each>
    <xsl:text>&#xa;</xsl:text>
    </xsl:for-each>
    <!-- Mesons -->
    <xsl:for-each select="%s_%s_Wilson_Mesons/elem">
      <xsl:for-each select="momenta/elem">
        <xsl:value-of select="../../gamma_value"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="../../../../t0"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="sink_mom_num"/>
        <xsl:text>    </xsl:text>
        <xsl:value-of select="sink_mom"/>
        <xsl:text>    </xsl:text>
        <xsl:for-each select="mesprop/elem">
          <xsl:value-of select="re"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="im"/>
          <xsl:text>    </xsl:text>
        </xsl:for-each>
        <xsl:text>&#xa;</xsl:text>
      </xsl:for-each>
    <xsl:text>&#xa;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xa;</xsl:text>  <!-- Separator new line -->
  </xsl:for-each>
</xsl:template>
<xsl:template match="text()"/>
</xsl:stylesheet>
""" % (sm_tag[snk_sm].upper(), sm_tag[src_sm], sm_tag[snk_sm], sm_tag[src_sm], sm_tag[snk_sm]))
                    xslt_f.close()
                    tstr = os.popen('xsltproc %s %s' % (xslt_file, hs_file)).read()
                    tarr = np.fromstring(tstr, np.float64, sep=' ')
                    l_tarr = 6 + 2*lt
                    print(src_sm, snk_sm, iA, iB, i_srctype, tarr.shape)
                    tarr = tarr.reshape(n_snktype, n_had, -1, l_tarr)
                    # [i_snktype, i_had, i_mom, t (-t0?)]
                    tarr_hspec  = tarr[:,:,:,6::2] + 1j*tarr[:,:,:,7::2]
                    # control indices
                    tarr_i_bar  = np.array(tarr[:, 0 : n_barlist, :, 0], int)
                    tarr_i_mes  = np.array(tarr[:, n_barlist : n_barlist + n_meslist , :, 0], int)
                    tarr_t0     = np.array(tarr[:,:,:,1], int)
                    t0          = tarr_t0[0,0,0]
                    assert(t0 == srcgrp[0][t_axis])
                    tarr_i_mom  = np.array(tarr[:,:,:,2], int)
                    tarr_mom    = np.array(tarr[:,:,:,3:6], int)
                    # verify control indices
                    assert((tarr_i_bar == np.r_[:n_barlist][None,:,None]).all())
                    assert((tarr_i_mes == np.r_[:n_meslist][None,:,None]).all())
                    if None is psnk_list :
                        psnk_list = tarr_mom[0,0]
                        n_psnk  = len(psnk_list)
                    assert((tarr_i_mom == np.r_[:n_psnk][None,None,:]).all())
                    assert((tarr_mom == psnk_list).all())
                    assert((tarr_t0 == t0).all())
                    assert(srcgrp[0, t_dir] == t0)

                    # split time into nC data points
                    #assert(3 == nC) # here it is also important
                    t1 = 0  # data is shifted
                    if None is res_iAB :
                        res_iAB = np.zeros((nC, n_srctype, n_snktype, n_had, n_psnk, t_step_2pt), 
                                    np.complex128)
                    for iC in range(nC) :
                        res_iAB[iC, i_srctype] = tarr_hspec[..., t1 : t1 + t_step_2pt]
                        t1 += t_step_2pt
                    
                    os.remove(xslt_file)

                res_list.append(res_iAB)
                csrc_list.extend(srcgrp)
            except RuntimeError : 
                print("[%s %d %d] exception '%s'" % (cfgkey, iA, iB, str(sys.exc_info()[0])))

    
    meas_spec_list = np.array(
                [ (cfgkey, c) for c in csrc_list],
                dtype=[('ckpoint_id', 'S32'), ('source_coord', np.int32, (4,))])
    n_data = len(meas_spec_list)
    

    if h5_overwrite :
        try : del h5g[h5key]
        except : pass
    hs_shape = (n_data, n_srctype, n_snktype, n_had, n_psnk, t_step_2pt)
    b = h5g.require_dataset(h5key, hs_shape, hs_dtype, fletcher32=True)
    b[:] = np.concatenate(res_list, axis=0)

    # "axes"
    b.attrs['dim_spec']      = np.array(['i_data', 'i_srctype', 'i_snktype', 'i_had', 'i_mom', 'i_t' ],
                                           dtype='S32')
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(b, meas_spec_list)
    b.attrs['srctype_list'] = np.array(srctype_list)
    b.attrs['snktype_list'] = np.array(snktype_list)
    b.attrs['had_list']     = np.array(chromahadspec_hadlist, dtype='S32')
    b.attrs['psnk_list']    = np.array(psnk_list, dtype=np.int32)
    b.attrs['t_list']       = np.r_[0 : t_step_2pt]
    # other parameters
    for k, v in iteritems(attrs_kv) : 
        b.attrs[k] = v

    b.file.flush()

def runme_conv_hadspec_sample(cfgkey, ama) :
    srcgrp_list = get_srcgrp_list(cfgkey, ama)

    h5_file = get_hadspec_cfg_file_h5(cfgkey, ama)
    lhpd.mkpath(os.path.dirname(h5_file))
    h5f     = h5py.File(h5_file, 'a')
    attrs_kv = {}

    conv_hadspec2hdf(h5f, "/cfg%s/hadspec/SS" % cfgkey, 
            cfgkey, ama, 'S', 'S', srcgrp_list, attrs_kv)
    conv_hadspec2hdf(h5f, "/cfg%s/hadspec/SP" % cfgkey, 
            cfgkey, ama, 'S', 'P', srcgrp_list, attrs_kv,
            snktype_list=['P'])
    h5f.close()



def conv_disc2pt2hdf(h5g, h5key, cfgkey, ama, 
            src_sm, snk_sm, srcgrp_list, time_rev, attrs_kv,
            h5_overwrite=True, hs_dtype=np.complex128,
            srctype_list=srctype_list, 
            snktype_list=snktype_list) :
    """ 
        XXX if snk_sm=='P', len(snktype_list) must be =1
    """
    assert(1 < t_step_2pt)
    nA, nB, nC  = srcgrp_list.shape[0:3]
    n_srctype   = len(srctype_list)
    n_snktype   = len(snktype_list)
    if 'P' == snk_sm : assert(1 == n_snktype)

    lt          = latsize[t_dir]
    chromahadspec_list = [ 
        "proton_pos_I", "proton_pos_A1", "proton_pos_A2", "proton_pos_A3",
        "proton_neg_I", "proton_neg_A1", "proton_neg_A2", "proton_neg_A3" ]
    sm_tag = {'P':'Point', 'S':'Shell'}
    n_had       = len(chromahadspec_list)
    psnk_list   = None
    n_psnk      = None
    csrc_list   = []
    res_list    = []
    xslt_fd, xslt_file = tempfile.mkstemp(suffix=cfgkey) ; os.close(xslt_fd)
    for iA in range(nA) :
        for iB in range(nB) :
            srcgrp = srcgrp_list[iA, iB]
            for i in range(len(srcgrp) - 1) :
                assert((lt + srcgrp[i+1][t_axis] - srcgrp[i][t_axis]) % lt == t_step_2pt)
            try :
                res_iAB = None
                
                for i_srctype, srctype in enumerate(srctype_list) :
                    
                    hs_file = get_chroma2pt_file(cfgkey, ama, srctype, "", time_rev, srcgrp)

                    #print(srctype, iA, iB, time.asctime())
                    xslt_f = open(xslt_file, 'w')
                    xslt_f.write("""<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
<xsl:template match="/nucl_2pt_proj/Wilson_nucl_2pt_proj_measurements">
  <xsl:for-each select="elem[SourceSinkType/sink_type_1='%s_SINK']">
    <xsl:for-each select="%s_%s_Wilson_Baryons/elem">
      <xsl:for-each select="momenta/elem">
        <xsl:value-of select="../../baryon_num"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="../../../../t0"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="sink_mom_num"/>
        <xsl:text>    </xsl:text>
        <xsl:value-of select="sink_mom"/>
        <xsl:text>    </xsl:text>
        <xsl:for-each select="barprop/elem">
          <xsl:value-of select="re"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="im"/>
          <xsl:text>    </xsl:text>
        </xsl:for-each>
        <xsl:text>&#xa;</xsl:text>
      </xsl:for-each>
    <xsl:text>&#xa;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xa;</xsl:text>
  </xsl:for-each>
</xsl:template>
<xsl:template match="text()"/>
</xsl:stylesheet>
""" % (sm_tag[snk_sm].upper(), sm_tag[src_sm], sm_tag[snk_sm]))
                    xslt_f.close()
                    xslt_p = os.popen('xsltproc %s %s' % (xslt_file, hs_file))
                    tstr = xslt_p.read()
                    if not None is xslt_p.close() : raise RuntimeError((xslt_file, hs_file))
                    tarr = np.fromstring(tstr, np.float64, sep=' ')
                    l_tarr = 6 + 2*lt
                    print(src_sm, snk_sm, iA, iB, i_srctype, tarr.shape)
                    tarr = tarr.reshape(n_snktype, n_had, -1, l_tarr)
                    # [i_snktype, i_had, i_mom, t (-t0?)]
                    tarr_hspec  = tarr[:,:,:,6::2] + 1j*tarr[:,:,:,7::2]
                    # control indices
                    tarr_i_bar  = np.array(tarr[:, :, :, 0], int)
                    tarr_t0     = np.array(tarr[:,:,:,1], int)
                    t0          = tarr_t0[0,0,0]
                    assert(t0 == srcgrp[0][t_axis])
                    tarr_i_mom  = np.array(tarr[:,:,:,2], int)
                    tarr_mom    = np.array(tarr[:,:,:,3:6], int)
                    # verify control indices
                    assert((tarr_i_bar == np.r_[:n_had][None,:,None]).all())
                    if None is psnk_list :
                        psnk_list = tarr_mom[0,0]
                        n_psnk  = len(psnk_list)
                    assert((tarr_i_mom == np.r_[:n_psnk][None,None,:]).all())
                    assert((tarr_mom == psnk_list).all())
                    assert((tarr_t0 == t0).all())
                    assert(srcgrp[0, t_dir] == t0)

                    # split time into nC data points
                    #assert(3 == nC) # here it is also important
                    t1 = 0  # data is shifted
                    if None is res_iAB :
                        res_iAB = np.zeros((nC, n_srctype, n_snktype, n_had, n_psnk, t_step_2pt), 
                                    np.complex128)
                    for iC in range(nC) :
                        x = tarr_hspec[..., t1 : t1 + t_step_2pt]
                        if time_rev : 
                            y   = np.empty_like(x)
                            y[..., 0]   = x[..., 0]
                            y[..., 1:]  = x[..., :0:-1]
                            res_iAB[iC, i_srctype] = y
                        else : res_iAB[iC, i_srctype] = x
                        #if time_rev : res_iAB[iC, i_srctype] = x[::-1]
                        #else : res_iAB[iC, i_srctype] = x
                        t1 += t_step_2pt
                    
                    os.remove(xslt_file)

                res_list.append(res_iAB)
                csrc_list.extend(srcgrp)
            except RuntimeError : 
                print("[%s %d %d] exception '%s'" % (cfgkey, iA, iB, str(sys.exc_info()[0])))

    
    meas_spec_list = np.array(
                [ (cfgkey, c) for c in csrc_list],
                dtype=[('ckpoint_id', 'S32'), ('source_coord', np.int32, (4,))])
    n_data = len(meas_spec_list)
    

    if h5_overwrite :
        try : del h5g[h5key]
        except : pass
    hs_shape = (n_data, n_srctype, n_snktype, n_had, n_psnk, t_step_2pt)
    b = h5g.require_dataset(h5key, hs_shape, hs_dtype, fletcher32=True)
    b[:] = np.concatenate(res_list, axis=0)

    # "axes"
    b.attrs['dim_spec']      = np.array(['i_data', 'i_srctype', 'i_snktype', 'i_had', 'i_mom', 'i_t' ],
                                           dtype='S32')
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(b, meas_spec_list)
    b.attrs['srctype_list'] = np.array(srctype_list)
    b.attrs['snktype_list'] = np.array(snktype_list)
    b.attrs['had_list']     = np.array(chromahadspec_list, dtype='S32')
    b.attrs['psnk_list']    = np.array(psnk_list, dtype=np.int32)
    b.attrs['t_list']       = np.r_[0 : t_step_2pt]
    b.attrs['time_rev']     = time_rev
    # other parameters
    for k, v in iteritems(attrs_kv) : 
        b.attrs[k] = v

    b.file.flush()

def runme_conv_disc2pt_sample(cfgkey, ama) :
    srcgrp_list = get_srcgrp_list(cfgkey, ama)

    h5_file = get_disc2pt_cfg_file_h5(cfgkey, ama)
    lhpd.mkpath(os.path.dirname(h5_file))
    h5f     = h5py.File(h5_file, 'a')
    attrs_kv = {}

    conv_disc2pt2hdf(h5f, "/cfg%s/disc2pt/time_pos" % cfgkey, 
            cfgkey, ama, 'S', 'S', srcgrp_list, False, attrs_kv)
    conv_disc2pt2hdf(h5f, "/cfg%s/disc2pt/time_neg" % cfgkey, 
            cfgkey, ama, 'S', 'S', srcgrp_list, True, attrs_kv)

    h5f.close()



def rename_hadspec_momlist_psnklist(h5fname, h5k) :
    h5f = h5py.File(h5fname, 'a')
    h5dset = h5f[h5k]
    h5dset.attrs['psnk_list'] = h5dset.attrs['mom_list']
    del h5dset.attrs['mom_list']
    h5f.flush()
    h5f.close()
    del h5f


def runme_merge_hadspec(ama,
            cfgkey_list=cfgkey_list) :    
    for had in had_list :
        print('# ', had)
        h5_fname = '%s/hspec/hspec-all.h5' % out_data_prefix

        h5_kpath= '/%s' %(had,)
        h5_list = [ ( get_hadspec_cfg_file_h5(cfgkey, ama), 
                      '/cfg%s/baryon' %(cfgkey) ) 
                    for cfgkey in cfgkey_list ]

        lhpd.h5_io.h5file_merge(h5_fname, h5_kpath, h5_list, VERBOSE=True)

def split_hadspec(ama, srctype, snktype, sm_tag, had, split_dir,
            cfgkey_list=cfgkey_list,
            VERBOSE=False) :
    """ 
        return h5_list = [ (h5f, h5k), ...] for merging
    """
    try : os.mkdir(split_dir)
    except OSError : pass

    h5_list = []
    for cfgkey in cfgkey_list :
        h5_fname= get_hadspec_cfg_file_h5(cfgkey, ama)
        h5_fname_sel    = '%s/hspec/split/hspec-split.%s.%s.%s.%s.%s.h5' % (
                    out_data_prefix, cfgkey, ama_desc(ama), srctype, snktype, had)
        lhpd.mkpath(os.path.dirname(h5_fname_sel))
        h5_kpath= '/cfg%s/hadspec/%s' %(cfgkey, sm_tag)
        if VERBOSE :
            print('# cp %s[%s] -> %s' % (h5_fname, h5_kpath, h5_fname_sel), time.asctime())

        h5_list.append((h5_fname_sel, h5_kpath))
        h5f_in  = h5py.File(h5_fname, 'r')
        h5d_in  = h5f_in[h5_kpath]
        i_src   = np_find_first(srctype, h5d_in.attrs['srctype_list'])[0]
        i_snk   = np_find_first(snktype, h5d_in.attrs['snktype_list'])[0]
        i_had   = np_find_first(had, h5d_in.attrs['had_list'])[0]

        h5f_sel = h5py.File(h5_fname_sel, 'a')
        lhpd.h5_io.h5_purge_keys(h5f_sel, [h5_kpath, '%s__DATALIST' % h5_kpath])

        h5f_sel[h5_kpath] = h5d_in[:, i_src, i_snk, i_had]
        h5d_sel = h5f_sel[h5_kpath]
        dim_spec_in = h5d_in.attrs['dim_spec']
        for k, v in iteritems(h5d_in.attrs) :
            if not k in [ 'data_list', 'dim_spec', 
                      'had_list', 'srctype_list', 'snktype_list'
                    ] : 
                h5d_sel.attrs[k] = h5d_in.attrs[k]
        h5d_sel.attrs['dim_spec'] = np.r_[ dim_spec_in[0:1], dim_spec_in[4:] ]
        lhpd.h5_io.h5_set_datalist(h5d_sel, lhpd.h5_io.h5_get_datalist(h5d_in))

        h5f_in.close()
        h5f_sel.flush()
        h5f_sel.close()
        del h5f_sel
        del h5f_in

    return h5_list

def merge_select_hadspec(ama, srctype, snktype, sm_tag, had, cfgkey_list=cfgkey_list,
            VERBOSE=False, remove_sel=True, binsize=None) :
    """ 
        first, copy selected data to a separate h5file
        second, merge 
        if binsize is not None and >0, 
    """
    """
# Example
for s0,s1,had in it.product(srctype_list, snktype_list,had_list) : runme_merge_select_hadspec(s0, s1, had)
    """

    
    print('# ', had)
    split_dir   = '%s/hspec/split' % (out_data_prefix,)
    try : os.mkdir(split_dir)
    except OSError : pass

    h5_list = split_hadspec(ama, srctype, snktype, sm_tag, had, split_dir, 
                        cfgkey_list=cfgkey_list, VERBOSE=False)
    
    h5_kpath_out= '/%s/%s' %(had, sm_tag)
    if not None is binsize and 0 < binsize :
        h5_fname_out= get_hadspec_bin_file_h5(ama, srctype, snktype, had)
        lhpd.mkpath(os.path.dirname(h5_fname_out))
        lhpd.h5_io.h5file_merge_bin(h5_fname_out, h5_kpath_out, h5_list, binsize, VERBOSE=VERBOSE)
    else :
        h5_fname_out= get_hadspec_all_file_h5(ama, srctype, snktype, had)
        lhpd.mkpath(os.path.dirname(h5_fname_out))
        lhpd.h5_io.h5file_merge(h5_fname_out, h5_kpath_out, h5_list, VERBOSE=VERBOSE)

    if remove_sel :
        for h5f, h5k in h5_list :
            if VERBOSE :
                print('# rm ', h5f, time.asctime())

            os.remove(h5f)
        #try : os.rmdir(split_dir)
        #except OSError : pass

def runme_merge_select_hadspec(ama, sm_tag,
            srctype_list=srctype_list, 
            snktype_list=snktype_list, 
            had_list=had_list,
            **kwargs) :
    for srctype, snktype, had in it.product(
                srctype_list, snktype_list, had_list) :
        merge_select_hadspec(ama, srctype, snktype, sm_tag, had, **kwargs)

def bin_unbias_select_hadspec(ama_apprx, ama_exact, srctype, snktype, sm_tag, had, 
            cfgkey_list=cfgkey_list, VERBOSE=False, remove_sel=True) :
    """ 
        first, copy selected data to a separate h5file
        second, merge 
        if binsize is not None and >0, 
    """
    
    print('# ', had)
    split_dir   = '%s/hspec/split' % (out_data_prefix,)
    try : os.mkdir(split_dir)
    except OSError : pass

    h5_kpath_out= '/%s' %(had,)
    # bin approx
    h5_list_apprx = split_hadspec(ama_apprx, srctype, snktype, sm_tag, had, split_dir, 
                        cfgkey_list=cfgkey_list, VERBOSE=False)
    h5_fname_out_apprx = get_hadspec_bin_file_h5(ama_apprx, srctype, snktype, sm_tag, had)
    lhpd.mkpath(os.path.dirname(h5_fname_out_apprx))
    lhpd.h5_io.h5file_bin(h5_fname_out_apprx, h5_kpath_out, h5_list_apprx, VERBOSE=VERBOSE)
    # bin exact
    h5_list_exact = split_hadspec(ama_exact, srctype, snktype, sm_tag, had, split_dir, 
                        cfgkey_list=cfgkey_list, VERBOSE=False)
    h5_fname_out_exact = get_hadspec_bin_file_h5(ama_exact, srctype, snktype, sm_tag, had)
    lhpd.mkpath(os.path.dirname(h5_fname_out_exact))
    lhpd.h5_io.h5file_bin(h5_fname_out_exact, h5_kpath_out, h5_list_exact, VERBOSE=VERBOSE)
    # bin unbias
    n_bins = len(h5_list_apprx)
    assert(len(h5_list_exact) == n_bins)
    h5_list_unbias = [ (h5_list_apprx[i] + h5_list_exact[i]) for i in range(n_bins) ]
    h5_fname_unbias = get_hadspec_unbias_file_h5(srctype, snktype, sm_tag, had)
    lhpd.mkpath(os.path.dirname(h5_fname_unbias))
    lhpd.h5_io.h5file_bin_unbias(h5_fname_unbias, h5_kpath_out, h5_list_unbias, VERBOSE=VERBOSE)

    if remove_sel :
        for h5f1, h5k1, h5f2, h5k2 in h5_list_unbias :
            if VERBOSE :
                print('# rm ', h5f1, h5f2, time.asctime())
            os.remove(h5f1)
            os.remove(h5f2)

def runme_bin_unbias_select_hadspec(ama_apprx, ama_exact, sm_tag,
            srctype_list=srctype_list, 
            snktype_list=snktype_list, 
            had_list=had_list,
            **kwargs) :
    for srctype, snktype, had in it.product(
                srctype_list, snktype_list, had_list) :
        bin_unbias_select_hadspec(ama_apprx, ama_exact, srctype, snktype, sm_tag, had, **kwargs)



def bin_select_hadspec(ama, srctype, snktype, sm_tag, had, 
            cfgkey_list=cfgkey_list, VERBOSE=False, remove_sel=True) :
    """ 
        first, copy selected data to a separate h5file
        second, bin
    """
    
    print('# ', had)
    split_dir   = '%s/hspec/split' % (out_data_prefix,)
    try : os.mkdir(split_dir)
    except OSError : pass

    h5_kpath_out= '/%s' %(had,)
    # bin approx
    h5_list_apprx = split_hadspec(ama_apprx, srctype, snktype, sm_tag, had, split_dir, 
                        cfgkey_list=cfgkey_list, VERBOSE=False)
    h5_fname_out_apprx = get_hadspec_bin_file_h5(ama_apprx, srctype, snktype, sm_tag, had)
    lhpd.mkpath(os.path.dirname(h5_fname_out_apprx))
    lhpd.h5_io.h5file_bin(h5_fname_out_apprx, h5_kpath_out, h5_list_apprx, VERBOSE=VERBOSE)

def runme_bin_select_hadspec(ama, sm_tag,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt,
            **kwargs) :
    for srctype, snktype, had in it.product(
                srctype_list, snktype_list, had_list
                ) :
        bin_select_hadspec(ama, srctype, snktype, sm_tag, had, **kwargs)
        

# XXX unused (will raise an exception)
def runme_bin_op_OLD2(binsize,
            srctype_list=srctype_list, 
            snktype_list=snktype_list,
            had_list=had_list_c3pt, 
            op_list=op_list,
            flav_list=flav_list, 
            psnk_list=psnk_list_c3pt,
            tsep_list=tsep_list
            ) :
    raise RuntimeError("use runme_merge_op(binsize=your_binsize)")
    for srctype, snktype, had, op, flav, psnk, tsep in it.product(
                srctype_list, snktype_list, 
                had_list, op_list, 
                flav_list, psnk_list, tsep_list) :
        print(srctype, snktype, had, op, flav, psnk, tsep, time.asctime())
        ir_list = op_default_ir_list(op)

        # input op
        h5_fname_in     = '%s/op/%s/%s/op-all.%s.%s.%s.%s_dt%d.h5' % (
                            out_data_prefix, srctype, snktype, 
                            had, op, flav, psnk_str(psnk), tsep)
        h5f_in          = h5py.File(h5_fname_in, 'r')

        # output op
        h5_fname_out    = '%s/op/%s/%s/op-bin.%s.%s.%s.%s_dt%d.h5' % (
                            out_data_prefix, srctype, snktype,  
                            had, op, flav, psnk_str(psnk), tsep)
        h5f_out          = h5py.File(h5_fname_out, 'a')

        for ir_name, ir_scale in op_default_ir_list(op) :
            h5_kpath    = '/%s/%s/%s/%s_dt%d/%s' % (
                            had, op, flav, psnk_str(psnk), tsep, ir_name)
            dset_in     = h5f_in[h5_kpath]
            h5f_out[h5_kpath] = lhpd.resample(dset_in.value, ('av', binsize))
            dset_out    = h5f_out[h5_kpath]

            for k,v in iteritems(dset_in.attrs) :
                if 'data_list' == k : continue
                dset_out.attrs[k] = v
            # TODO set datalist?

        h5f_out.flush()
        h5f_out.close()
        h5f_in.close()




##############################################################################
# FORM FACTORS
##############################################################################
# FIXME make this non-global; move to mc?
calc_ff_tpol_map = {
    '3' : lhpd.latcorr.tpol_map['posSzplus'],
    # TODO add others
}
def make_ff_eqnmat_precond_f2f(op, mlat, latsize, mcgrp, ir_list) :
    """ generate ff eq.matrix for all tpol, mc=(p3src,p3snk), ir=(ir_name, ir_scale)
        indexing is the direct product : flat([i_tpol, i_mc, i_ir, i_comp, i_reim])
        where i_comp is # of component for ir_list[i_ir], i_reim=0,1 for Real/Imag
            tpol_list   list of spin matrices to include (4x4 cplx; FIXME : extend to use tpol names?)
            mcgrp       list of mc_case objects containing p3src, p3snk, tpol
            ir_list     list of irreps to include ; the ir_scale is ignore
        XXX twisted momenta: twisting angles should be added to mc[0], mc[1]
    """         
                
    n_ff    = lhpd.latcorr.get_gff_number(op) 
    n_mc    = len(mcgrp)
    n_comp  = ir_list_comp_len(ir_list)
    # separate Re/Im
    sh_3pt  = (n_mc, n_comp, 2)
    n_c3pt  = n_mc * n_comp * 2
                         
    # momenta
    def make_mom_std(p) : 
        p = np.abs(p) 
        p.sort()
        return p
    #p3src_list  = [ mc.p3src for mc in mcgrp ]
    #p3snk_list  = [ mc.p3snk for mc in mcgrp ]
    mc_std_list = np.array([ ( make_mom_std(mc.p3src), make_mom_std(mc.p3snk))
                             for mc in mcgrp ])
    q2_list     = np.array([ lorentz_mom_transfer_sq(mlat, latsize, mc.p3src, mc.p3snk) 
                             for mc in mcgrp ])
    # sanity check to make sure that all ffs in the system correspond to the same Q2
    assert((np.abs(q2_list - q2_list[0]) < NONZERO_TOL).all())
    
    eq_mat = np.empty(sh_3pt + (n_ff,), np.float64)
    for i_mc, mc in enumerate(mcgrp) :
        p4_in   = lorentz_p4(mlat, latsize, mc.p3src)
        p4_out  = lorentz_p4(mlat, latsize, mc.p3snk)
        mat_mc  = lhpd.latcorr.ff_matrix_f2f(
                        mlat, p4_in,  mc.get_tpol_matr(), 
                        mlat, p4_out, mc.get_tpol_matr(), 
                        op, mc.get_tpol_matr())
        
        ir_sh = 0
        for (ir_name, ir_scale) in ir_list:
            ir_dim = H4_repr_dim[ir_name]
            mat_mc_ir = H4_repr_func[ir_name](mat_mc)
            #print(ir_name, mat_ir)
            eq_mat[i_mc, ir_sh : ir_sh + ir_dim, 0] = mat_mc_ir.real
            eq_mat[i_mc, ir_sh : ir_sh + ir_dim, 1] = mat_mc_ir.imag
            ir_sh += ir_dim
        assert(ir_sh == n_comp)

    # dyn.create sparse precond matrix : pc[i_row][i_nonzero_elem]
    pc_coeff = []          # [i_eqn][j] -> coeff from eq_mat
    pc_i3pt  = []          # [i_eqn][j] -> idx_3pt from eq_mat
    pc_norm2 = []          # [i_eqn][j] -> norm(eq_mat[i_3pt])

    n_eqn = 0
    for idx_3pt in np.ndindex(n_mc, n_comp, 2) :
        (i_mc, i_comp, i_c) = idx_3pt
        n2_i = (eq_mat[idx_3pt]**2).sum()
        if n2_i <= NONZERO_TOL : continue # drop zero equations

        coeff = None
        for k_eqn in range(n_eqn) :
            assert(0 < len(pc_i3pt[k_eqn]))
            idx_3pt_k   = pc_i3pt[k_eqn][0]
            (k_mc, k_comp, k_c) = idx_3pt_k
            xy  = (np.dot(eq_mat[idx_3pt], eq_mat[idx_3pt_k])
                        / math.sqrt(n2_i * pc_norm2[k_eqn][0]))
            # FIXME hardcoded here is the condition for merging equations; how to make it flexible?
            if (    k_c == i_c                                          # both Re or Im
                and (mc_std_list[i_mc] == mc_std_list[k_mc]).all()      # (in,out) momenta are equivalent
                and lhpd.rdiff(abs(xy), 1.) < NONZERO_TOL               # angle is 0 or Pi
                and lhpd.rdiff(n2_i, pc_norm2[k_eqn][0]) < NONZERO_TOL  # norm is the same
                    ) :
                coeff = xy
                print("# old pcmat row idx_3pt=", idx_3pt, " ->", idx_3pt_k, " (x%.1f)" % coeff)
                pc_i3pt[k_eqn].append(idx_3pt)
                pc_coeff[k_eqn].append(coeff)
                pc_norm2[k_eqn].append(n2_i)
                break

        if None is coeff :
            print("# new pcmat row idx_3pt=", idx_3pt)
            pc_i3pt.append([idx_3pt])
            pc_coeff.append([1.])
            pc_norm2.append([n2_i])
            n_eqn += 1

    assert(0 < n_eqn)
    assert( len(pc_i3pt) == n_eqn
        and len(pc_coeff) == n_eqn
        and len(pc_norm2) == n_eqn)

    pc_mat = np.zeros(sh_3pt + (n_eqn,), np.float64)
    for k_eqn in range(n_eqn) :
        for j in range(len(pc_coeff[k_eqn])) :
            pc_mat[pc_i3pt[k_eqn][j]][k_eqn] = pc_coeff[k_eqn][j]

    #return eq_mat.reshape(n_c3pt, n_ff), pc_mat.reshape(n_c3pt, n_eqn)
    return eq_mat, pc_mat

###############################################################################
"""
mlat1=0.47
qext2_max1=10
p1p2_dict = make_mom_combo_list(mlat1, latsize, psnk_list, list(lhpd.int_vectors_maxnorm(3, qext2_max1)))
p1p2_list = [ p1p2_dict[q2] for q2 in sorted(p1p2_dict.keys()) ]
mcgrp_list = [ [ mc_case(pp[0], pp[1], '3') for pp in ppgrp ] for ppgrp in p1p2_list ]
ssgrp1 = [ srcsnkpair(tsep, 'src5', 'sink_5.0', 'SS') for tsep in np.r_[10:20:2]]
op1='tensor1'
ir_list1=lhpd.latcorr.op_default_ir_list(op1)
gg1=ens_data_get(data_top='D5.sm5_dt10--18', ama='ub', had='proton', flav='U-D', op=op1)
method_list1=[('ratio_pltx',), ('ratio_pltx_avg',)]
rsplan1 = ('jk', 1)
calc_ff(gg1, mcgrp_list[0], ssgrp1, op1, ir_list1, mlat1, latsize, method_list1, rsplan=rsplan1)
"""
# TODO update calc_ff, calc_save_ff, runme_calc_save_ff functions here to use mcgrp_list, ss_list
def calc_ff(gg, mcgrp, ssgrp, op, ir_list, mlat, latsize,
            method_list, 
            **kw) :
    """ this is a long ugly function to compute formfactors;
        temporary implementation, will be organized into subfunctions as will be seen fit
        (although likely to stay forever)

        method_list = [ (name, param1, ...), ...]
                    methods to extract the ground state

        func_get_op     (ama_mode, had, flav, op, ir_list, p3src, p3snk, tsep) -> c3pt[i_data,tau,i_comp]
        func_get_c2pt   (ama_mode, had, p3) -> c2pt[i_data, t]

        return formfactors[data][i_method][i_ff]
        ----------------------
        making it less ugly
        mc  = class {p3src, p3snk, tpol }
        ss  = class { tsep src_tag, snk_tag, sm_tag }
              ?rename as s4 or s5 or s6: "source&sink smearing&separation specification set"
        
    """
    rsplan  = kw['rsplan']
    tskip   = kw.get('tskip') or None
    # TODO rewrite : 
    # # make eqmat, pcmat; apply pcmat
    # # for each ss:
    # #* for all mcgrp : load c3pt (ss, mcgrp, ir_list), resample, apply pcmat
    # #* for all mcgrp : load c2pt for unique ss.key2pt(), [sep. for src, snk?], resample, apply pcmat
    # # for each i_eqn : solve for me (need data for full ssgrp)
    # # solve for ff
    n_mc    = len(mcgrp)
    n_comp  = ir_list_comp_len(ir_list)
    n_c3pt  = n_mc * n_comp * 2
    tmax    = max(tsep_list) + 1            # FIXME if fit c2pt, may need to plug other value


    def load_c3pt(ss) :
        n_data = res = None
        for i_mc, mc in enumerate(mcgrp) :
            ir_sh = 0
            for ir_name, ir_scale in ir_list :
                ir_len  = H4_repr_dim[ir_name]
                # [i_data, tau, i_comp]
                x = gg.get_threept(mc, ss, op, ir_name)
                if None is res :
                    n_data = x.shape[0]
                    # [i_data, tau, i_mc, i_comp, i_c]
                    res = np.empty((n_data, 1+ss.tsep, n_mc, n_comp, 2), np.float64)
                res[:, :, i_mc, ir_sh : ir_sh + ir_len, 0] = ir_scale * x.real.transpose(0,2,1)
                res[:, :, i_mc, ir_sh : ir_sh + ir_len, 1] = ir_scale * x.imag.transpose(0,2,1)
                ir_sh += ir_len
            assert(ir_sh == n_comp)

        return res
                
    c3pt_map_r = {}
    for ss in ssgrp :
        c3pt_map_r[ss] = lhpd.resample(load_c3pt(ss))
        
    def load_c2pt_src_snk(ss):
        n_data = res_src = res_snk = None
        for i_mc, mc in enumerate(mcgrp) :
            # [i_data, t]
            x = gg.get_twopt_src(mc, ss)
            y = gg.get_twopt_snk(mc, ss)
            if None is n_data :
                assert(None is res_src)
                assert(None is res_snk)
                n_data = x.shape[0]
                assert(y.shape[0] == n_data)
                res_src = np.empty((n_data, tmax, n_mc), np.float64)
                res_snk = np.empty((n_data, tmax, n_mc), np.float64)
            # [i_data, t, i_mc]
            res_src[:, :, i_mc] = x[:, :tmax].real
            res_snk[:, :, i_mc] = y[:, :tmax].real
        return res_src, res_snk
    def load_c2fit_src_snk(ss):
        n_data = n_fitp = res_src = res_snk = None
        for i_mc, mc in enumerate(mcgrp) :
            # [i_data, t]
            x = gg.get_twopt_fit_src(mc, ss)
            y = gg.get_twopt_fit_snk(mc, ss)
            if None is n_data :
                assert(None is res_src)
                assert(None is res_snk)
                n_data, n_fitp = x.shape[0:2]
                assert(y.shape[0] == n_data)
                res_src = np.empty((n_data, n_fitp, n_mc), np.float64)
                res_snk = np.empty((n_data, n_fitp, n_mc), np.float64)
            # [i_data, t, i_mc]
            res_src[:, :, i_mc] = x[:, :tmax].real
            res_snk[:, :, i_mc] = y[:, :tmax].real
        return res_src, res_snk
    
    c2pt_src_r  = {}
    c2pt_snk_r  = {}
    c2fit_src_r = {}
    c2fit_snk_r = {}
    for ss in set([ ss.key2pt() ]) :
        # TODO apply precond here
        srcsnk = load_c2pt_src_snk(ss)
        c2pt_src_r[ss] = lhpd.resample(srcsnk[0], rsplan)
        c2pt_snk_r[ss] = lhpd.resample(srcsnk[1], rsplan)
    
    c2fit_src_r = {}
    c2fit_snk_r = {}
    for ss in set([ ss.key2pt() ]) :
        srcsnk = load_c2fit_src_snk(ss)
        # XXX sic! no resampling here because already resampled
        # XXX fits are usually resampled!!! 
        # TODO make sure the rsplan is the same
        c2fit_src_r[ss] = srcsnk[0]
        c2fit_snk_r[ss] = srcsnk[1]
    
    n_fitp  = c2fit_src_r.values()[0].shape[1]
    # TODO check that n_data is uniform in c3pt_map_r,c2pt_src_r ,c2fit_snk_r,c2fit_src_r,c2fit_snk_r
    # TODO check that n_fitp is uniform in c2fit_src_r,c2fit_snk_r
    
    # TODO load c2pt fits, recreate 2pt from them

    def apply_eqn_prec_twopt(c2a, pcmat) :
        """ return preconditioned versions of c2, c3 """
        n_eqn   = pcmat.shape[-1]
        res_c2a = dict([ (k, np.zeros(c2a[k].shape[0:2] + (n_eqn,), c2a[k].dtype))
                         for k in c2a.keys() ])
        for k,v in iteritems(c2a) : 
            for i_eqn, i_mc, i_comp, i_c in np.ndindex(n_eqn, n_mc, n_comp, 2) :
                f   = pcmat[i_mc, i_comp, i_c, i_eqn]
                if 0. == f : continue
                res_c2a[k][:, :, i_eqn] += abs(f) * v[:, :, i_mc]
        return res_c2a

    def apply_eqn_prec_threept(c3ba, pcmat) :
        """ return preconditioned versions of c2, c3 """
        n_eqn   = pcmat.shape[-1]
        res_c3ba= dict([ (k, np.zeros(c3ba[k].shape[0:2] + (n_eqn,), c3ba[k].dtype))
                         for k in c3ba.keys() ])
        for k,v in iteritems(c3ba): 
            for i_eqn, i_mc, i_comp, i_c in np.ndindex(n_eqn, n_mc, n_comp, 2) :
                f   = pcmat[i_mc, i_comp, i_c, i_eqn]
                if 0. == f : continue
                res_c3ba[k][:, :, i_eqn]+= f * v[:, :, i_mc, i_comp, i_c]
        return res_c3ba


    # [i_mc, n_comp, 2, i_ff], [i_mc, i_comp, 2, i_eqn]
    (eqmat, pcmat) = make_ff_eqnmat_precond_f2f(
            op, mlat, latsize, mcgrp, ir_list)
    n_ff    = eqmat.shape[-1]
    n_eqn   = pcmat.shape[-1]
    
    ### compute preconditioned c3pt, c2pt for combined and app-only
    c2pt_src_rs_pc  = apply_eqn_prec_twopt(c2pt_src_r, pcmat)
    c2pt_snk_rs_pc  = apply_eqn_prec_twopt(c2pt_snk_r, pcmat)
    c3pt_map_rs_pc  = apply_eqn_prec_threept(c3pt_map_r, pcmat)
    
    c2fit_src_rs_pc = apply_eqn_prec_twopt(c2fit_src_r, pcmat)
    c2fit_snk_rs_pc = apply_eqn_prec_twopt(c2fit_snk_r, pcmat)

    
    # preconditioner is applied to both 3pt and 2pt, so their ratio should be solved against 
    # equations without extra factors; therefore, normalize the result of applying pc to eqmat
    # (divide each row by the multiplicity over which pc matrix summates
    pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
    eqmat_pc= np.dot(pcmat.reshape(n_c3pt, n_eqn).T,
                      eqmat.reshape(n_c3pt, n_ff)) / pc_norm[:,None]

    
    def calc_me_ratio(c2a, c2b, c3ba, tsep) :
        """ ASSUME time axis=1 """
        sqf = np.sqrt(np.abs(  c2b[:, : tsep+1] / c2a[:, : tsep+1]
                               * c2a[:, tsep::-1] / c2b[:, tsep::-1] ))
        return (c3ba / np.sqrt(np.abs(c2b[:, tsep] * c2a[:, tsep]))[:,None] * sqf)
    def calc_me_ratio_all(c2a, c2b, c3ba_map) : 
        return dict([ (ss, calc_me_ratio(c2a[ss.key2pt()], c2b[ss.key2pt()], c3ba_map[ss], ss.tsep))
                      for ss in c3ba_map.keys() ]) 
    # me[tsep][i_data, tau, i_eqn]
    # TODO solve for me here : ratios, fits, etc ...
    me_rs   = calc_me_ratio_all(c2pt_src_rs_pc, c2pt_snk_rs_pc, c3pt_map_rs_pc)
    
    n_data = c3pt_map_r.values()[0].shape[0]
    def calc_ratio_pltx(ss) :
        """ ff on timeslices
            return ff[n_data, 1+tsep, n_ff] """
        assert(ss in c3pt_map_r)
        tsep= ss.tsep
        res = np.empty((me_rs[ss].shape[0], 1+tsep, n_ff), np.float64)
        for tau in range(1+tsep) :
            # solve od: [i_eqn,i_ff] for tau
            cov     = lhpd.calc_cov(me_rs[ss][:,tau], rsplan)
            cov_inv = lhpd.fitter.cov_invert(cov, rmin=1e-4)
            sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
            res[:,tau,:] = matrix_tensor_dot(sol_mat, me_rs[ss][:,tau,:], 1)
        return res

    def calc_ratio_pltx_avg(ss, max_tslice=3) :
        """ ff from ratio pltx center avg
            return ff[n_data, 1, n_ff] """
        assert(ss in c3pt_map_r)
        tsep = ss.tsep
        t1, t2 = lhpd.latcorr.pltx_trange(tsep, max_tslice)
        
        res     = np.empty((me_rs[ss].shape[0], 1, n_ff), np.float64)
        # solve od: [i_eqn,i_ff] for tau
        cov     = lhpd.calc_cov(me_rs[ss][:, t1:t2].mean(axis=1), 
                                         rsplan)
        cov_inv = lhpd.fitter.cov_invert(cov, rmin=1e-4)
        sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
        me_rs_avg = me_rs[ss][:, t1:t2, :].mean(axis=1)
        res[:, 0, :] = matrix_tensor_dot(sol_mat, me_rs_avg, 1)
        return res

    def calc_ratio_summ(ss_list, tskip=tskip) :
        """ ff from summation-extrapolated matrix elements
            return ff[n_data, 1, n_ff] """
        for ss in ss_list : 
            assert(ss in c3pt_map_r)
        tsep_list   = np.asarray([ ss.tsep for ss in ss_list ])
        n_tsep      = len(tsep_list)
        
        me_summ     = np.empty((n_data, n_tsep, n_eqn), np.float64)
        for i_ss, ss in enumerate(ss_list) :
            tsep    = ss.tsep
            me_summ[:, i_ss]      = me_rs[ss][:, tskip : 1 + tsep - tskip].sum(axis=1)
        
        # solve od : [i_tsep, {coeff, intercept}]
        me_gs       = np.empty((n_data, n_eqn), np.float64)
        eqn_summ = np.r_[ [tsep_list], [[1.] * n_tsep] ].T
        for i_eqn in range(n_eqn) :
            me_summ_cov = lhpd.calc_cov(me_summ[:, :, i_eqn], rsplan)
            cov_inv     = lhpd.fitter.cov_invert(me_summ_cov, rmin=1e-4)
            sol_summ, sol_undef = lhpd.fitter.linsolver_od_matr(eqn_summ, cov_inv)
            me_gs[:, i_eqn]     = matrix_tensor_dot(sol_summ, me_summ[:, :, i_eqn], 1)[:, 0]

        # solve od : [i_eqn, i_ff]
        res         = np.empty((n_data, 1, n_ff), np.float64)
        me_gs_cov   = lhpd.calc_cov(me_gs, rsplan)
        cov_inv     = lhpd.fitter.cov_invert(me_gs_cov, rmin=1e-4)
        sol_ff, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
        res[:, 0, :]= matrix_tensor_dot(sol_ff, me_gs, 1)
        return res

    def calc_me_fit_simple(ss_list, tskip=tskip) :
        """ reproducing Boram's fits here """
        tseptau_list= []  # list of tuples (tsep, tau)
        c3_tseptau  = []
        for ss in ss_list :
            tseptau_list.extend([ (ss.tsep, tau) for tau in np.r_[tskip : ss.tsep - tskip + 1]])
            c3_tseptau.extend([ c3_map_rs_pc[ss][:, tskip : ss.tsep - tskip + 1] ])
        tseptau     = np.asarray(tseptau_list)
        exp_tseptau = np.asarray()
        c3_tseptau = np.concatenate(c3_tseptau, axis=1)     # concat along t-axis
        # TODO calc_exp_fit continue here

            
    # TODO calc_gpof


    # put plateaus:tsep->x, plateau averages:tsep->x, summation
    all_res = []
    for m in method_list :
        if   'ratio_pltx' == m[0] : 
            for ss in ssgrp :
                all_res.append(calc_ratio_pltx(ss))
        elif 'ratio_pltx_avg' == m[0] :
            for ss in ssgrp :
                all_res.append(calc_ratio_pltx_avg(ss))
        elif 'ratio_summ' == m[0] :
            all_res.append(calc_ratio_summ(ssgrp))
        else : raise ValueError(m[0])

    return all_res


def calc_save_ff(h5_file, h5_kpath,
            mcgrp_list, ssgrp, flav, op, ir_list, mlat, latsize, 
            method_list,
            rsplan = ('jk', 1), apponly=False, print_pltx=False) :
    n_q2 = len(mc_list_list)
    n_data = None
    n_ff = lhpd.latcorr.get_gff_number(op)
    all_ff = []
    q2_list= []
    for i_q2, mc_list in enumerate(mc_list_list) :
        mc0 = mc_list[0]
        q2 = lorentz_mom_transfer_sq(mlat, latsize, mc0[0], mc0[1])
        print(mc0, "Q2=", q2)
        q2_list.append(q2)
        all_ff.append(calc_ff(srcstr, snkstr, had, gamma_pol, flav, op, ir_list, mlat, mc_list,
                              tsep_list, method_list,
                              rsplan = rsplan, apponly=apponly, print_pltx=print_pltx))

    q2_list = np.asarray(q2_list)
    n_data = all_ff[0][0].shape[0]
    assert (all_ff[0][0].shape[-1] == n_ff)

    def save_ff(dset, i) :
        dset.attrs['q2_list'] = q2_list
        for i_q2, q2 in enumerate(q2_list) :
            # [i_data, i_t, i_q2, i_ff]
            dset[:, :, i_q2, :] = all_ff[i_q2][i]

    cnt = 0
    for m in method_list :
        if   'ratio_pltx' == m[0] :
            for tsep in tsep_list :
                ff_shape = (n_data, 1 + tsep, n_q2, n_ff)
                k = '%s/ratio_pltx/dt%d' % (h5_kpath, tsep)
                h5_ff = h5_file.require_dataset(k, ff_shape, np.float64, 
                                                fletcher32=True)
                save_ff(h5_ff, cnt)
                cnt += 1
        elif 'ratio_pltx_avg' == m[0] :
            for tsep in tsep_list :
                ff_shape = (n_data, 1, n_q2, n_ff)
                k = '%s/ratio_pltx_avg/dt%d' % (h5_kpath, tsep)
                h5_ff = h5_file.require_dataset(k, ff_shape, np.float64, 
                                                fletcher32=True)
                save_ff(h5_ff, cnt)
                cnt += 1
        elif 'ratio_summ' == m[0] :
            ff_shape = (n_data, 1, n_q2, n_ff)
            k = '%s/ratio_summ' % (h5_kpath)
            h5_ff = h5_file.require_dataset(k, ff_shape, np.float64, 
                                            fletcher32=True)
            save_ff(h5_ff, cnt)
            cnt += 1


if False:       # comment out project-specific runmeXXX;
                # when checking into git, go back to the original version (fnal-dwf-2013-15)
    def runme_calc_save_ff(h5_fname, 
                gamma_pol, mlat, 
                psnk_list=psnk_list_c3pt,
                qext2_max=qext2_max,
                srctype_list=srctype_list,
                snktype_list=snktype_list,
                had_list=had_list,
                op_list=['tensor1', 'pstensor1', 'tensor2s2'], 
                flav_list=['U-D', 'U+D'], 
                tsep_list=tsep_list, 
                rsplan = ('jk', 1), apponly=False) :
        """ compute form factors w/o renormalization """
        method_list = [('ratio_pltx',), ('ratio_pltx_avg',), ('ratio_summ',)]
        h5f = h5py.File(h5_fname, 'a')
        mc_dict = make_mom_combo_list(mlat, latsize, psnk_list, 
                                      list(lhpd.int_vectors_maxnorm(3, qext2_max)))
        q2_list = sorted(mc_dict.keys())
        mc_list_list = [ mc_dict[q2] for q2 in q2_list ]

        for srctype, snktype, had, op, flav in it.product(
                srctype_list, snktype_list, had_list, op_list, flav_list) :
            print("flav=%s, op=%s" % (flav, op))
            ir_list = op_default_ir_list(op)
            h5k = '/%s/%s/%s' % (had, op, flav)
            calc_save_ff(h5f, h5k, srctype, snktype, had, gamma_pol, flav, op, ir_list, 
                         mlat, mc_list_list,
                         tsep_list, method_list,
                         rsplan=rsplan, apponly=apponly)

        h5f.flush()
        h5f.close()
        del h5f
def runme_calc_save_ff(h5_fname, 
            gamma_pol, mlat, 
            psnk_list=psnk_list_c3pt,
            qext2_min=0,
            qext2_max=qext2_max,
            srctype_list=srctype_list,
            snktype_list=snktype_list,
            had_list=had_list,
            op_list=op_list,
            flav_list=['U-D', 'U+D'], 
            tsep_list=tsep_list, 
            rsplan = ('jk', 1), apponly=False) :
    """ compute form factors w/o renormalization """
    method_list = [('ratio_pltx',), ('ratio_pltx_avg',), ('ratio_summ',)]
    lhpd.mkpath(os.path.dirname(h5_fname))
    h5f = h5py.File(h5_fname, 'a')
    mc_dict = make_mom_combo_list(mlat, latsize, psnk_list, 
                                  list(lhpd.int_vectors_maxnorm(3, qext2_max, norm2_min=qext2_min)))
    q2_list = sorted(mc_dict.keys())
    mc_list_list = [ mc_dict[q2] for q2 in q2_list ]

    for srctype, snktype, had, op, flav in it.product(
            srctype_list, snktype_list, had_list, op_list, flav_list) :
        print("flav=%s, op=%s" % (flav, op))
        ir_list = op_default_ir_list(op)
        h5k = '/%s/%s/%s' % (had, op, flav)
        calc_save_ff(h5f, h5k, srctype, snktype, had, gamma_pol, flav, op, ir_list, 
                     mlat, mc_list_list,
                     tsep_list, method_list,
                     rsplan=rsplan, apponly=apponly, print_pltx=True)

    h5f.flush()
    h5f.close()
    del h5f

def runme_plot_all_q2dep_nde(had, 
        op_list=op_list,
        flav_list=['U-D', 'U+D']) :

    for op in op_list : 
        for flav in flav_list :
            for i_gff in range(lhpd.latcorr.get_gff_number(op)) :
                list_what=[ ( h5py.File('%s/ff/ff.%s.%s.%s.%s.%s.h5'%(out_data_prefix,s0,s1,had,flav,op), 'r')[
                                        "/%s/%s/%s" %(had,op,flav)], 
                              r"$\langle %s|%s\rangle$" %(latexize(s1),latexize(s0)), 
                              i_gff, tsep_list, slice(None)) 
                            for (s0,s1) in zip(srctype_list,snktype_list)]
                ff_name = lhpd.latcorr.get_gff_name(op, i_gff)
                name    = '%s/ff.figs/%s_%s_%s_%s_q2dep' % (out_data_prefix, ff_name, had, op, flav)
                print("# %s %s %s %s -> %s" % (had, op, flav, ff_name, name) )
                ax      = plot_q2dep_cmp_files(list_what, method_list=['ratio_pltx_avg'], 
                                ylabel=(r'$\mathrm{%s}^\mathrm{%s}$'%(latexize(ff_name), latexize(flav))))
                ax.figure.savefig(name+'.pdf')
                dv.save_figdraw(name+'.plt.py')
                dv.close_fig(ax.figure)

def runme_plot_cmpruns_q2dep_nde(had, 
        out_data_prefix_list, srcsnktype_list, 
        op_list, flav_list, tsep_list, 
        out_dir='.') :      # do not use any defaults set in individual configs

    if (len(out_data_prefix_list) <= 0) : 
        print("empty input list")
        return
    for op, flav, tsep, (s0,s1) in it.product(op_list, flav_list, tsep_list, 
                                              srcsnktype_list) : 
        for i_gff in range(lhpd.latcorr.get_gff_number(op)) :
            list_what=[ ( h5py.File('%s/ff/ff.%s.%s.%s.%s.%s.h5'%(
                                        out_data_p, s0, s1, had, flav, op), 'r')[
                                    "/%s/%s/%s" %(had, op, flav)], 
                          r"%s $\langle %s|%s\rangle$" %(
                                latexize(out_data_p), 
                                latexize(s1), latexize(s0)), 
                          i_gff, [tsep], slice(None)) 
                        for out_data_p in out_data_prefix_list ]
            ff_name = lhpd.latcorr.get_gff_name(op, i_gff)
            name    = '%s/%s_%s_%s_%s_%s-%s_dt%d_q2dep' % (
                        out_dir, ff_name, had, op, flav, 
                        s0, s1, tsep)
            print( "# %s %s %s %s %s-%s dt=%d -> %s" % (
                        had, op, flav, ff_name, 
                        s0, s1, tsep, 
                        name))
            ax      = plot_q2dep_cmp_files(list_what, method_list=['ratio_pltx_avg'], 
                            ylabel=(r'$\mathrm{%s}^\mathrm{%s}$'%(latexize(ff_name), latexize(flav))))
            ax.figure.savefig(name+'.pdf')
            dv.save_figdraw(name+'.plt.py')
            dv.close_fig(ax.figure)

def runme_plot_all_pltx_nde(had, 
            op_list=op_list,
            flav_list=['U-D', 'U+D'],
            nxy=(2,5), size=(16,20)) :

        nx, ny = nxy
        for op in op_list : 
            for flav in flav_list :
                for i_gff in range(lhpd.latcorr.get_gff_number(op)) :
                    ff_name = lhpd.latcorr.get_gff_name(op, i_gff)
                    ylabel  = r'$\mathrm{%s}^\mathrm{%s}$'%(latexize(ff_name), latexize(flav))
                    name    = '%s/ff.figs/%s_%s_%s_%s_pltx' % (out_data_prefix, ff_name, had, op, flav)

                    fig     = plt.figure(figsize=size)
                    ax_list = dv.make_xysubplot(nx, ny, fig=fig, 
                            flat=True, closex=True, closey=True,
                            xlabel=r'$\tau$', ylabel=ylabel)
                    print("# %s %s %s %s -> %s" % (had, op, flav, ff_name, name) )

                    dgrp_ff_list = [ h5py.File('%s/ff/ff.%s.%s.%s.%s.%s.h5'%(out_data_prefix, s0, s1, had, flav, op), 
                                               'r')['/%s/%s/%s' %(had, op, flav)] 
                                     for (s0, s1) in zip(srctype_list, snktype_list)]
                    label_prefix_list = [ r"$\langle %s|%s\rangle$" %(latexize(s1),latexize(s0))
                                          for (s0, s1) in zip(srctype_list, snktype_list)]
                    n_ax    = nx * ny
                    q2r     = dgrp_ff_list[0]['ratio_summ'].attrs['q2_list']
                    n_q2    = len(q2r)
                    if n_ax < n_q2 :
                        print("WARNING: plot %d out of %d q2 points" % (n_ax, n_q2))
                    for i_q2 in range(n_ax) :
                        q2  = q2r[i_q2]
                        list_what = [ ( dgrp_ff_list[i], label_prefix_list[i], i_gff, i_q2, tsep_list) 
                                      for i in range(len(dgrp_ff_list)) ]
                        ax  = ax_list[i_q2]
                        ax.text(.5, .1, r'$Q^2=%.3f$' % q2, transform=ax.transAxes,
                                verticalalignment='bottom')
                        plot_pltx_cmp_files(list_what, ax=ax, method_list=['ratio_pltx_avg'], 
                                    #xlabel="", ylabel="")
                                    xlabel=None, ylabel=None)
                        ax.legend()
                    dv.adjust_multiplot_xlim(ax_list)
                    for i_y in range(ny) :
                        dv.adjust_multiplot_ylim(ax_list[ i_y * nx : (i_y + 1) * nx])
                    fig.savefig(name + '.pdf')
                    #dv.save_figdraw(name + '.plt.py')
                    dv.close_fig(ax.figure)


def runme_plot_cmpruns_pltx_nde(had, 
            out_data_prefix_list, srcsnktype_list,
            op_list, flav_list, tsep_list,
            out_dir='.', nxy=(2,5), size=(16,20)) :

        nx, ny = nxy
        for op, flav, tsep, (s0,s1) in it.product(op_list, flav_list, tsep_list, 
                                                  srcsnktype_list) : 
            for i_gff in range(lhpd.latcorr.get_gff_number(op)) :
                ff_name = lhpd.latcorr.get_gff_name(op, i_gff)
                ylabel  = r'$\mathrm{%s}^\mathrm{%s}$'%(latexize(ff_name), latexize(flav))
                name    = '%s/%s_%s_%s_%s_%s-%s_dt%d_pltx' % (
                                out_dir, ff_name, had, op, flav,
                                s0, s1, tsep)
                fig     = plt.figure(figsize=size)
                ax_list = dv.make_xysubplot(nx, ny, fig=fig, 
                        flat=True, closex=True, closey=True,
                        xlabel=r'$\tau$', ylabel=ylabel)
                print("# %s %s %s %s %s-%s dt=%d -> %s" % (
                            had, op, flav, ff_name, 
                            s0, s1, tsep,
                            name))

                dgrp_ff_list = [ h5py.File('%s/ff/ff.%s.%s.%s.%s.%s.h5'%(out_data_p, s0, s1, had, flav, op), 
                                           'r')['/%s/%s/%s' %(had, op, flav)] 
                                 for out_data_p in out_data_prefix_list]
                label_prefix_list = [ r"%s $\langle %s|%s\rangle$" %(
                                            latexize(out_data_p),
                                            latexize(s1), latexize(s0))
                                      for out_data_p in out_data_prefix_list]
                n_ax    = nx * ny
                q2r     = dgrp_ff_list[0]['ratio_summ'].attrs['q2_list']
                n_q2    = len(q2r)
                if n_ax < n_q2 :
                    print("WARNING: plot %d out of %d q2 points" % (n_ax, n_q2))
                for i_q2 in range(n_ax) :
                    q2  = q2r[i_q2]
                    list_what = [ ( dgrp_ff_list[i], label_prefix_list[i], i_gff, i_q2, [ tsep ]) 
                                  for i in range(len(dgrp_ff_list)) ]
                    ax  = ax_list[i_q2]
                    ax.text(.5, .1, r'$Q^2=%.3f$' % q2, transform=ax.transAxes,
                            verticalalignment='bottom')
                    plot_pltx_cmp_files(list_what, ax=ax, method_list=['ratio_pltx_avg'], 
                                #xlabel="", ylabel="")
                                xlabel=None, ylabel=None)
                    ax.legend()
                dv.adjust_multiplot_xlim(ax_list)
                for i_y in range(ny) :
                    dv.adjust_multiplot_ylim(ax_list[ i_y * nx : (i_y + 1) * nx])
                fig.savefig(name + '.pdf')
                #dv.save_figdraw(name + '.plt.py')
                dv.close_fig(ax.figure)


