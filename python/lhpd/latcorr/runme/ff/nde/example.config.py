import numpy as np



in_data_prefix  = "data_in/D5_b6p3_mlm0p2390.src5-7-9_t12"
out_data_prefix = "data_out/sm5-7-9_dt12.sloppy"


cfgkey_list = open('cfgkey_list', 'r').read().split()
srctype_list= [ "src5", "src7", "src9" ]
n_srctype   = len(srctype_list)
snktype_list= [ "sink_5.0", "sink_7.0", "sink_9.0" ]
n_snktype   = len(snktype_list)
t_step_2pt  = 21
t_end_2pt   = t_step_2pt
latsize     = np.array([32, 32, 32, 64])
t_axis      = 3
t_dir       = t_axis    # obsolete alias

flav_list   = ['U', 'D']
psnk_list   = np.array([[0,0,0]])
psnk_list_c3pt  = psnk_list
lpath_min, lpath_max = 0, 1
tsep        = 12
tsep_list   = [ tsep ]
src_snk_dt_list = tsep_list
qext2_max   = 10

op_list         = [
        'tensor1', 'pstensor1',
        'tensor2s2', 'pstensor2s2',
        #'tensor3s3', 'pstensor3s3',
        ]
had_list        = [ 'proton_3' ]
had_list_hadspec= [ 'proton_3' ]
had_list_c3pt   = [ 'proton_3' ]

# AMA modes 
ama_exact = {   'approx' : 'exact' }
ama_apprx   = {   
        'approx' : 'sloppy' }

ama_mode_list = [ ama_apprx ]


def csrc_str(csrc) : return ("t%dx%dy%dz%d" % (csrc[3], csrc[0], csrc[1], csrc[2]))
def qext_str(qext) : return ("qz%+d_qy%+d_qx%+d" % (qext[2], qext[1], qext[0]))
def psnk_str(psnk) : return ("px%dpy%dpz%d" % tuple(psnk))

def mkfunc_bbfile(src_str, snk_str, had, flav, psnk, tsep) :
    assert(flav in ["U", "D"])
    str1    = ("%s/D5_b6p3_mlm0p2390.%s/output/bbs_t12/%s" % (
                in_data_prefix, src_str, snk_str))
    def bbfile_func(cfgkey, iA, iB, src_grp, qext) :
        return ("%s/cnf.%s/meas_LP_%02d/NUCL_%s_MIXED_NONREL.%s.%s_%s_%s_LP_%s.bb" % (
                str1, cfgkey, 1+iA, flav, psnk_str(psnk), 
                csrc_str(src_grp[0]), csrc_str(src_grp[1]), csrc_str(src_grp[2]), 
                qext_str(qext)))
    return bbfile_func

def get_chromahadspec_file(cfgkey, srctype, snktype, srcgrp) :
    """
D5_b6p3_mlm0p2390.src3/output/hadspec/cnf.1000/hadspec_cl21_32_64_b6p3_m0p2390_m0p2050.1000_t20x0y10z26_t41x8y10z26_t62x4y10z26_LP.dat.xml
    """
    return "%s/D5_b6p3_mlm0p2390.%s/output/hadspec/cnf.%s/hadspec_cl21_32_64_b6p3_m0p2390_m0p2050.%s_%s_%s_%s_LP.dat.xml" % (
                in_data_prefix, srctype, cfgkey, cfgkey, 
                csrc_str(srcgrp[0]), csrc_str(srcgrp[1]), csrc_str(srcgrp[2]))

def ama_mode_desc(ama_mode) :
    if 'exact' == ama_mode['approx'] : 
        return 'exact'
    elif 'sloppy' == ama_mode['approx'] :
        return ('sloppy')
    else:
        raise ValueError(ama_mode['approx'])



data_top = out_data_prefix
def get_bb_dset(cfgkey, srcstr, snkstr, ama_mode, had, flav, psnk, tsep) :
    """ get BB dataset 
        return : HDF5 dataset handler
        the actual data is not (necessarily) loaded to memory until
        a (result).value or (result)[slice] is taken
    """
    fname = '%s/bb/%s/%s/bb.%s.%s.%s.%s_dt%d.h5' % (
                data_top, srcstr, snkstr, cfgkey, 
                had, flav, psnk_str(psnk), tsep)
    kpath = '/cfg%s/%s/%s/%s_dt%d' % (cfgkey, had, flav, psnk_str(psnk), tsep)
    h5f   = h5py.File(fname, 'r')
    return h5f[kpath]



##############################################################################
# calc_ff 
##############################################################################
from lhpd.pymath.gamma_matr import gamma_dgr, gamma_id, gamma_dot
gamma = gamma_dgr
Tpol_proton3 = gamma_dot((gamma_id + gamma[3])/2., 
                         gamma_id - 1j * gamma_dot(gamma[0], gamma[1]))
had     = 'proton_3'
mhad    = .4714 # (28) from David's analysis
if False : # switch to use binned data
    rsplan  = ('jk', 96)
    def get_c2pt_dset(ama_mode, srcstr, snkstr, had) :
        """ 
        """
        if 'exact' == ama_mode['approx'] : 
            return None
        elif 'sloppy' == ama_mode['approx'] :
            fname = '%s/hspec/hspec-all.%s.%s.%s.h5' % (data_top, srcstr, snkstr, had)
            kpath = '/%s' % (had,)
            h5f   = h5py.File(fname, 'r')
            return h5f[kpath]
        else : raise NotImplementedError

    def psnk_str(psnk) : return ("px%dpy%dpz%d" % tuple(psnk))
    def get_op_dset(ama_mode, srcstr, snkstr, had, flav, op, psnk, tsep, ir_name):
        if 'exact' == ama_mode['approx'] : 
            return None
        elif 'sloppy' == ama_mode['approx'] :
            fname = '%s/op/%s/%s/op-all.%s.%s.%s.%s_dt%d.h5' % (
                    data_top, srcstr, snkstr, 
                    had, op, flav, psnk_str(psnk), tsep)
            kpath = '/%s/%s/%s/%s_dt%d/%s' % (
                    had, op, flav, 
                    psnk_str(psnk), tsep, ir_name)
            h5f   = h5py.File(fname, 'r')
            return h5f[kpath]
        else : raise NotImplementedError
elif True:
    rsplan  = ('jk', 1) # already binned
    def get_c2pt_dset(ama_mode, srcstr, snkstr, had) :
        """ 
        """
        if 'exact' == ama_mode['approx'] : 
            return None
        elif 'sloppy' == ama_mode['approx'] :
            fname = '%s/hspec/hspec-bin.%s.%s.%s.h5' % (data_top, srcstr, snkstr, had)
            kpath = '/%s' % (had,)
            h5f   = h5py.File(fname, 'r')
            return h5f[kpath]
        else : raise NotImplementedError

    def psnk_str(psnk) : return ("px%dpy%dpz%d" % tuple(psnk))
    def get_op_dset(ama_mode, srcstr, snkstr, had, flav, op, psnk, tsep, ir_name):
        if 'exact' == ama_mode['approx'] : 
            return None
        elif 'sloppy' == ama_mode['approx'] :
            fname = '%s/op/%s/%s/op-bin.%s.%s.%s.%s_dt%d.h5' % (
                    data_top, srcstr, snkstr, 
                    had, op, flav, psnk_str(psnk), tsep)
            kpath = '/%s/%s/%s/%s_dt%d/%s' % (
                    had, op, flav, 
                    psnk_str(psnk), tsep, ir_name)
            h5f   = h5py.File(fname, 'r')
            return h5f[kpath]
        else : raise NotImplementedError
else: raise NotImplementedError
