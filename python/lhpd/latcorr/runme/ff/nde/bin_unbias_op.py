from __future__ import print_function
from past.builtins import execfile
execfile('pproc_nde.py')

wl  = list(it.product(srctype_list, snktype_list, had_list, op_list, flav_list, tsep_list))
print("# n_jobs = %d" % len(wl))

if not 'job_key' in globals() :
    print("'job_key' NOT FOUND")
    raise RuntimeError

assert (not None is job_key)
w   = wl[job_key]
print(w)

for ama in ama_list : runme_bin_op(ama,
    srctype_list=[w[0]], snktype_list=[w[1]], 
    had_list=[w[2]], op_list=[w[3]], flav_list=[w[4]], tsep_list=[w[5]])

runme_bin_unbias_op(
    srctype_list=[w[0]], snktype_list=[w[1]], 
    had_list=[w[2]], op_list=[w[3]], flav_list=[w[4]], tsep_list=[w[5]])
