from __future__ import print_function
from past.builtins import execfile
execfile('pproc_nde.py')

if not 'job_key' in globals() :
    print("'job_key' NOT FOUND")
    print("# n_jobs = %d" % len(cfgkey_list))
    raise RuntimeError

assert (not None is job_key)
runme_calc_op_sample(cfgkey_list[job_key])
""" 
#test calc_save_op
h5f = h5py.File('data/sm3-5-7_dt12.sloppy/bb/src3/sink_3.0/bb.2740.U.px0py0pz0.h5', 'r')
h5bb= h5f['/cfg2740/U/px0py0pz0']
h5out=h5py.File('tmp.ff.h5', 'w')
calc_save_op(h5out, '/tmp-ff', 'proton_3', 'U', 'tensor1', 'H4_T1_d4r1', [0,0,0], 12, latsize, 3, h5bb)
"""
