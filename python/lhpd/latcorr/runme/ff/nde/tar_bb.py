from __future__ import print_function
from past.builtins import execfile
execfile('pproc_nde.py')

if not 'job_key' in globals() :
    print("'job_key' NOT FOUND")
    raise RuntimeError

assert (not None is job_key)
cfgkey = cfgkey_list[job_key]


try:    
    #os.mkdir('hspec.tar')
    os.mkdir(r'%s/bb.tar' %(out_data_prefix,))
    os.mkdir(r'%s/op.tar' %(out_data_prefix,))
except OSError : pass
    
bb_tar  = "bb.tar/bb.%s.tar" % (cfgkey,)
status = os.system(r"cd %s   &&   bash -c 'find bb/*/*  |grep bb\.%s\.  |tar -T - -cvf %s'" % (
    out_data_prefix, cfgkey, bb_tar))
if 0 != status : raise RuntimeError("os.system exited with error")

#op_tar  = "op.tar/op.%s.tar" % (cfgkey,)
#status = os.system(r"cd %s   &&   bash -c 'find op/*/*  |grep op\.%s\.  |tar -T - -cvf %s'" % (
    #out_data_prefix, cfgkey, op_tar))
#if 0 != status : raise RuntimeError("os.system exited with error")
