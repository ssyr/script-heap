from __future__ import print_function
from past.builtins import execfile
execfile('pproc_nde.py')

wl  = list(it.product(srctype_list, had_list_hadspec))
print("# n_jobs = %d" % len(wl))

if not 'job_key' in globals() :
    print("'job_key' NOT FOUND")
    print("len(wl) = %d" % len(wl))
    raise RuntimeError

assert (not None is job_key)
w   = wl[job_key]
print(w)

for ama in ama_list : 
    runme_bin_select_hadspec(ama,
            'SS',
            srctype_list=[w[0]], 
            snktype_list=snktype_list, 
            had_list=[w[1]])
    runme_bin_select_hadspec(ama,
            'SP',
            srctype_list=[w[0]], 
            snktype_list=['P'],
            had_list=[w[1]])
