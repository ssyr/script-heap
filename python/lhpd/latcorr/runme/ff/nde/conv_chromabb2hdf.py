from __future__ import print_function
from past.builtins import execfile
execfile("pproc_nde.py")

if not 'job_key' in globals() :
    print("'job_key' NOT FOUND")
    print("# n_jobs = %d" % len(cfgkey_list))
    raise RuntimeError

assert (not None is job_key)
runme_conv_bb_sample(cfgkey_list[job_key])
