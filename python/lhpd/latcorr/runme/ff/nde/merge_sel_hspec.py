from __future__ import print_function
from past.builtins import execfile
execfile('pproc_nde.py')

wl  = list(it.product(srctype_list, snktype_list,had_list))

if not 'job_key' in globals() :
    print("'job_key' NOT FOUND")
    print("len(wl) = %d" % len(wl))
    raise RuntimeError

assert (not None is job_key)
w   = wl[job_key]
print(w)
runme_merge_select_hadspec(w[0], w[1], w[2])
