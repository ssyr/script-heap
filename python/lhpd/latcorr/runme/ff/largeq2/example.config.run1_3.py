from __future__ import print_function
from past.builtins import execfile
import numpy as np
import itertools as it
import lhpd
from lhpd.latcorr import make_linkpath_list
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted
from lhpd.misc.strkey import *
from lhpd.misc import ldir_3d, space2full

latsize = np.r_[32,32,32,64]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]
q_bc_t  = ferm_bc[t_axis]
meson_bc_t = q_bc_t**2
baryon_bc_t= q_bc_t**3

cfg_index   = lhpd.read_key_list('../list.cfg.all', 'index')
#cfg_list    = lhpd.read_key_list('../list.cfg.run1.n100', 'samples')   # XXX read from cmdline instead

ama_ex      = 'ex'
ama_sl      = 'sl'
ama_list = [ama_ex, ama_sl]
def ama_str(ama) : return ama

ncoh_src_t      = 4 # number of coherent src in one group
def make_csrcgrp_list(ama, cfgkey):
    nsrc_ama_map = {
        ama_ex : [1,1,1,4],
        ama_sl : [4,2,2,4] }
    #csrc_meas_dxc    = [7,11,13,23]
    #csrc_meas_x0     = [0,0,0,0]
    return lhpd.make_srcgrp_grid(
            nsrc_ama_map[ama], latsize, [0,0,0,0], [7,11,13,23],
            1 + cfg_index.index(cfgkey), t_axis, ncoh_src_t, 
            dx_it=[0,0,0,0])

pm1     = np.r_[-1:1+1] 
pm2     = np.r_[-2:1+2]
pm6     = np.r_[-6:1+6]
pm10    = np.r_[-10:1+10]

kbxp10  = space2full(ldir_3d['x'], t_axis)
kbxm10  = space2full(ldir_3d['X'], t_axis)
kbym10  = space2full(ldir_3d['Z'], t_axis)
kbzm10  = space2full(ldir_3d['Y'], t_axis)

#smear_shape1 = dict(type='wuppertal', t_axis=t_axis,
  #wup_U=gauge_u_ape, wup_N=50, wup_alpha=2.0 )
smear_opt_b20       = dict(tag='GN2x50bxp20', boost= 2*kbxp10)
smear_opt_b20neg    = dict(tag='GN2x50bxm20', boost=-2*kbxp10)
srcsnk_smtag_list  = [ (smear_opt_b20['tag'], smear_opt_b20neg['tag']) ]
symm_smtag_list    = [ '%s_%s' % (s[0], s[0]) for s in srcsnk_smtag_list ]
skew_smtag_list    = [ '%s_%s' % (s[0], s[1]) for s in srcsnk_smtag_list ]

########### c2pt
c2pt_symm_psnk_list = normsorted(lhpd.range_prod([pm10, pm2, pm2]))
c2pt_skew_psnk_list = c2pt_symm_psnk_list

c2pt_hslab_len  = latsize[t_axis] // ncoh_src_t
c2pt_tpol_list  = [ 'Tg%d' % g for g in range(16) ]

print("*** FIX FOR c2pt/SS/proton bc_t=meson_bc_t")
c2pt_spec_list      = ([]   # syntax stub to allow unary+ on lists
    #+ [ dict(had='proton', tpol_list=c2pt_tpol_list, 
             #psnk_list=c2pt_symm_psnk_list, smsrcsnk=smsrcsnk, smtag=smtag, saved_bc_t=baryon_bc_t)
        #for smsrcsnk, smtag in it.product(['SP'], symm_smtag_list) ]
    + [ dict(had='proton', tpol_list=c2pt_tpol_list, 
             psnk_list=c2pt_symm_psnk_list, smsrcsnk=smsrcsnk, smtag=smtag, saved_bc_t=baryon_bc_t)
        for smsrcsnk, smtag in it.product(['SS'], symm_smtag_list)] 
    #+ [ dict(had='proton', tpol_list=c2pt_tpol_list,
             #psnk_list=c2pt_skew_psnk_list, smsrcsnk=smsrcsnk, smtag=smtag, saved_bc_t=baryon_bc_t)
        #for smsrcsnk, smtag in it.product(['SS'], skew_smtag_list) ]
    )

############## c3pt
c3pt_tlen       = 12
c3pt_tpol_list  = ['posSzplus']
c3pt_psnk_list  = np.asarray([[-4,0,0]])
c3pt_qext_list  = normsorted(lhpd.range_prod([pm10, pm2, pm2]))
c3pt_tsep_list  = [ 6,7,8,9,10,11,12 ]

flav_list       = ['U', 'D']
flav_cur_list   = flav_list
c3pt_op_list    = [
    'pstensor0', 'tensor0',
    'tensor1', 'pstensor1', 
    'sigma2a2', ]
#lpath_list = make_linkpath_list(c3pt_lpath_min, c3pt_lpath_max, dim=4)
#lpath_str_list = [ lpath_str(lp) for lp in lpath_list ]

c3pt_spec_list      = [ 
    dict(had='proton', tpol=tpol, psnk=psnk, tsep=tsep,
         smsrcsnk='SS', smtag=smtag, saved_bc_t=1)        # sic! bc is not used in c3pt saving
    for (tpol, psnk, smtag, tsep) in it.product(
            c3pt_tpol_list, c3pt_psnk_list, 
            skew_smtag_list, c3pt_tsep_list) ]

bb_spec_list     = [
    dict(kind='bb', lmin=0, lmax=1, flav=flav, qext_list=c3pt_qext_list)
    for (flav,) in it.product(
            flav_cur_list) ]
op_spec_list     = [
    dict(kind='op', op=op, flav=flav, qext_list=c3pt_qext_list)
    for (flav, op) in it.product(
            flav_cur_list, c3pt_op_list) ]


data_dir_aff    = 'data.in/run1_3'
data_out        = 'data.out/run1_3'
execfile('config_lcdb.py')

