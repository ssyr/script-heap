import aff
import h5py 
import numpy as np
import math
import itertools as it
import lhpd
from lhpd import make_src_grid
from lhpd.misc import strkey

# XXX general idea
# * modular database (many separate files with internal structure)
# * each dataset is addressed by pair (file, kpath)
# * datasets are standard for c2pt, c3pt, c4pt
# * file, kpath) are created by specific functions:
# * * c2pt: (dat_d, c2_d)
# * * c3pt: (dat_d, c3_d, ins_d)
# * * c4pt: (dat_d, c4_d(=?=c3_d), ins1_d, ins2_d)
# * dat_d incorporates type(stage:raw,all,bin,ub), cfg?,csrc?,ama? depending on stage
# * XXX_d stands for "description"(dictionary) for XXX
# * preferably use Attrdict?

# XXX pieces of rationale
# Q: which parts of metadata go to file, which go to kpath?
# * most frequently used together -> kpath, the rest -> file
# * separate files are useful for downloading and working on
# *   partial analysis

# XXX examples
# dat_d = dict(kind='bin'|'all'|'cfg', ama='ub'|'sl'|'ex', cfgkey=, csrc=|csrcgrp=)
# ins_d = dict(
#   kind='bb', flav='U'|'D', lpath='xYz' (if empty: all available?) lpath_list=?
#   kind='op', flav='U'|'D', op='tensor1', ir='adfadf'|ir_list=[], flav=
#   kind='qpdf' probably same as bb
#   kind='tmd'
# c3_d = dict(
#   had='proton', tpol='posSzplus',
#   psnk=
#   tsep=
#   smsrcsnk=, [smtag=]
# c2_d = dict(
#   had='proton', [tpol='posSzplus',]
#   smsrcsnk=, [smtag=,]

# TODO combine all functions into latcorr_db_file, latcorr_db_kpath (or combination?)

def srcsnk_str_f(cN_d) : return '%s.%s' %(cN_d['smsrcsnk'], cN_d['smtag'])
def srcsnk_str_k(cN_d) : return '%s/%s' %(cN_d['smsrcsnk'], cN_d['smtag'])

def c3snk_str_f(c3_d) : return strkey.snk_str_f(c3_d['psnk'], c3_d['tsep'])
def c3snk_str_k(c3_d) : return strkey.snk_str_k(c3_d['psnk'], c3_d['tsep'])
def ama_str(ama) : return ama

def hadron_str(cN_d) : 
    had = cN_d['had']
    if 'proton' == had : return "%s_%s" %(had, cN_d['tpol'])
    else : raise ValueError(had)
hadron_str_f = hadron_str
hadron_str_k = hadron_str

def dat_str_f(which, dat_d) :       # only for conv, all, bin, ub
    dat_kind = dat_d['kind']
    if   'conv' == dat_kind : 
        return '%s/%s.%s.%s' % (which, which, 
                dat_d['cfgkey'], ama_str(dat_d['ama']))
    elif 'all'  == dat_kind :
        return '%s.all/%s-all.%s' % (which, which, ama_str(dat_d['ama']))
    elif ('ub'   == dat_kind) or ('bin' == dat_kind and 'ub' == dat_d['ama']) : 
        return '%s.bin/%s-unbias' % (which, which)
    elif 'bin'  == dat_kind :
        return '%s.bin/%s-bin.%s' % (which, which, ama_str(dat_d['ama']))
    else : raise ValueError(dat_kind)


#################### c2pt ########################
# c2_d = dict(had='proton', smsrcsnk=, smtag=)
# example
#   c2pt/c2pt.3320.sl.x13y25z31t29_x13y25z31t45_x13y25z31t61_x13y25z31t13.GN2x50bxp20_GN2x50bxp20.aff
#       //c2pt/SP/proton_Tg0/PX-1_PY-2_PZ-2
def get_c2pt_file(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    if   'aff'  == dat_kind :
        return 'c2pt/c2pt.%s.%s.%s.%s.aff' % (
                dat_d['cfgkey'], ama_str(dat_d['ama']),
                strkey.csrcgrp_str_f(dat_d['csrcgrp']), c2_d['smtag'])
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
        return '%s.%s.%s.h5' % (
                dat_str_f('c2pt', dat_d), 
                c2_d['had'], srcsnk_str_f(c2_d))
    else : raise ValueError(dat_kind)

def get_c2pt_kpath(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    if   'aff'  == dat_kind :
        return '/c2pt/%s/%s_%s' % (
                c2_d['smsrcsnk'], c2_d['had'], c2_d['tpol'])
    elif 'conv' == dat_kind : 
        return '/cfg%s/c2pt/%s/%s' % (
                dat_d['cfgkey'], c2_d['smsrcsnk'], c2_d['had'])
    elif ('all' == dat_kind or
          'bin' == dat_kind or
          'ub'  == dat_kind) :
        return '/c2pt/%s/%s' % (c2_d['smsrcsnk'], c2_d['had'])
    else : raise ValueError(dat_kind)

#################### c3pt ########################
# c3_d  = dict(had='proton', tpol=, psnk=, tsep=, smsrcsnk=, smtag=)
# ins_d = dict(kind=, flav=, op=, ir=)
# example
def get_c3pt_file(dat_d, c3_d, ins_d) :
    # XXX bb is very generic: also tmd or qpdf, just change 'bb' appropriately
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']

    if   'bb'   == ins_kind :
        # bb is very generic: also tmd or qpdf, just change 'bb' appropriately
        if   'aff'  == dat_kind :
            return 'bb/bb.%s.%s.%s.%s.%s.%s.%s.aff' % (
                    dat_d['cfgkey'], ama_str(dat_d['ama']), 
                    strkey.csrcgrp_str_f(dat_d['csrcgrp']), c3snk_str_f(c3_d),
                    c3_d['tpol'], ins_d['flav'], c3_d['smtag'])
        elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            return '%s.%s.%s.%s.%s.h5' % (
                    dat_str_f('bb', dat_d),
                    c3snk_str_f(c3_d), hadron_str_f(c3_d), 
                    srcsnk_str_f(c3_d), ins_d['flav'])
        else : raise ValueError((dat_kind, ins_kind))

    elif 'op'   == ins_kind :
        if dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            return '%s.%s.%s.%s.%s.%s.h5' % (
                    dat_str_f('op', dat_d),
                    c3snk_str_f(c3_d), hadron_str_f(c3_d), 
                    srcsnk_str_f(c3_d), ins_d['flav'], ins_d['op'])
        else : raise ValueError((dat_kind, ins_kind))
    else : raise ValueError(ins_kind)

def get_c3pt_kpath(dat_d, c3_d, ins_d) :
    # XXX bb is very generic: also tmd or qpdf, just change 'bb' appropriately
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']
    if   'bb'   == ins_kind :
        if   'aff'  == dat_kind :
            return '/bb/%s/%s/%s/%s/%s' % (
                    c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    strkey.csrcgrp_str_k(dat_d['csrcgrp']), 
                    c3snk_str_k(c3_d), ins_d['flav'])
        elif 'conv' == dat_kind : 
            return '/cfg%s/bb/%s/%s/%s/%s' % (
                    dat_d['cfgkey'], c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_d['flav'])
        elif ('all' == dat_kind or
              'bin' == dat_kind or
              'ub'  == dat_kind) :
            return '/bb/%s/%s/%s/%s' % (
                    c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_d['flav'])
        else : raise ValueError((dat_kind, ins_kind))

    elif 'op'   == ins_kind :
        if   'conv' == dat_kind : 
            return '/cfg%s/op/%s/%s/%s/%s/%s/%s' % (
                    dat_d['cfgkey'], c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_d['flav'], ins_d['op'], ins_d['ir'])
        elif ('all' == dat_kind or
              'bin' == dat_kind or
              'ub'  == dat_kind) :
            return '/op/%s/%s/%s/%s/%s/%s' % (
                    c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_d['flav'], ins_d['op'], ins_d['ir'])
        else : raise ValueError((dat_kind, ins_kind))
    else : raise ValueError((dat_kind, ins_kind))




def get_c3x_file(dat_d, c3x_d, ins_d) :
    # XXX bb is very generic: also tmd or qpdf, just change 'bb' appropriately
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']

    if   'bb'   == ins_kind :
        # bb is very generic: also tmd or qpdf, just change 'bb' appropriately
        if   dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            return '%s.%s.%s.%s.h5' % (
                    dat_str_f('c3x_bb', dat_d),
                    c3x_d['had'], 
                    srcsnk_str_f(c3x_d), ins_d['flav'])
        else : raise ValueError((dat_kind, ins_kind))

    elif 'op'   == ins_kind :
        if dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            return '%s.%s.%s.%s.%s.h5' % (
                    dat_str_f('c3x_op', dat_d), c3x_d['had'], 
                    srcsnk_str_f(c3x_d), ins_d['flav'], ins_d['op'])
        else : raise ValueError((dat_kind, ins_kind))
    else : raise ValueError(ins_kind)

def get_c3x_kpath(dat_d, c3x_d, ins_d) :
    # XXX bb is very generic: also tmd or qpdf, just change 'bb' appropriately
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']
    if   'bb'   == ins_kind :
        if 'conv' == dat_kind : 
            return '/cfg%s/c3x_bb/%s/%s/%s' % (
                    dat_d['cfgkey'], c3x_d['smsrcsnk'], c3x_d['had'], ins_d['flav'])
        elif ('all' == dat_kind or
              'bin' == dat_kind or
              'ub'  == dat_kind) :
            return '/c3x_bb/%s/%s/%s' % (
                    c3x_d['smsrcsnk'], c3x_d['had'], ins_d['flav'])
        else : raise ValueError((dat_kind, ins_kind))

    elif 'op'   == ins_kind :
        if   'conv' == dat_kind : 
            return '/cfg%s/c3x_op/%s/%s/%s/%s/%s' % (
                    dat_d['cfgkey'], c3x_d['smsrcsnk'], c3x_d['had'],
                    ins_d['flav'], ins_d['op'], ins_d['ir'])
        elif ('all' == dat_kind or
              'bin' == dat_kind or
              'ub'  == dat_kind) :
            return '/c3x_op/%s/%s/%s/%s/%s' % (
                    c3x_d['smsrcsnk'], c3x_d['had'],
                    ins_d['flav'], ins_d['op'], ins_d['ir'])
        else : raise ValueError((dat_kind, ins_kind))
    else : raise ValueError((dat_kind, ins_kind))



def get_dvi_file(dat_d, vi_d, dkind) :
    dat_kind    = dat_d['kind']
    vi_kind     = vi_d['kind']
    if 'qloop' == vi_kind :
        assert('conv' == dat_kind)
        if   'orig' == dkind :
            return 'qloop/qloop.%s.%s.%s.h5' % (
                dat_d['cfgkey'], vi_d['flav'], vi_d['approx'])
        elif 'mom3' == dkind :
            return 'qloop_mom3/qloop_mom3.%s.%s.%s.h5' % (
                dat_d['cfgkey'], vi_d['flav'], vi_d['approx'])
        else : raise ValueError((dat_kind, vi_kind, dkind))
    else : raise ValueError((dat_kind, vi_kind, dkind))

def get_dvi_kpath(dat_d, vi_d, dkind) :
    dat_kind    = dat_d['kind']
    vi_kind     = vi_d['kind']
    if 'qloop' == vi_kind : 
        assert('conv' == dat_kind)
        if   'orig' == dkind : 
            return ''
        elif 'mom3' == dkind :
            return 'qloop_mom3'
        else : raise ValueError((dat_kind, vi_kind, dkind))

    else : raise ValueError((dat_kind, vi_kind))

############# common function #############
def get_lcdb_loc(dat_d, lc_d) :
    """ dat_d   = dict(data_dir=,ama=,cfgkey=,csrcgrp=,...)
        lc_d    = dict(kind='c2',   c2_d=,)
                | dict(kind='c3',   c3_d=, ins_d=)
                | dict(kind='c3vi', c3_d=, ins_d=, vacins_d= } ??? crosscheck with setup @Mira
    """
    ddir    = dat_d['data_dir']
    lc_kind = lc_d['kind']
    if   'c2'   == lc_kind : 
        c2_d    = lc_d['c2_d']
        return ("%s/%s" % (ddir, get_c2pt_file(dat_d, c2_d)), 
                get_c2pt_kpath(dat_d, c2_d))

    elif 'c3'   == lc_kind :
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        return ("%s/%s" % (ddir, get_c3pt_file(dat_d, c3_d, ins_d)), 
                get_c3pt_kpath(dat_d, c3_d, ins_d))

    elif 'c3x'  == lc_kind :
        c3x_d   = lc_d['c3x_d']
        ins_d   = lc_d['ins_d']
        return ("%s/%s" % (ddir, get_c3x_file(dat_d, c3x_d, ins_d)), 
                get_c3x_kpath(dat_d, c3x_d, ins_d))

    elif 'dvi' == lc_kind :
        vi_d    = lc_d['vi_d']
        dkind   = lc_d['dkind']
        return ("%s/%s" % (ddir, get_dvi_file(dat_d, vi_d, dkind)),
                get_dvi_kpath(dat_d, vi_d, dkind))
        
    else : raise ValueError(kind)
