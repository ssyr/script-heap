#import ipdb
from __future__ import print_function
from past.builtins import execfile
from past.builtins import basestring
import numpy as np
import h5py
import lhpd
from lhpd.misc import np_find_first, np_match, purge_keys, dict_copy_pop, dictnew
from lhpd.h5_io import h5type_attr_fix_
import os, sys, time, tempfile, errno
from lhpd.pymath.H4_tensor import *
from lhpd.limits import *

execfile('scripts-runme/pproc_common.py')

# XXX copied from runme/ff/pproc_jlabff.py
#def runme_calc_save_ff(
#        h5_file,
#        mcgrp_list, ssgrp,
#        flav_list, op_list,
#        method_list,
#        **kw ) :
#
#    mlat        = kw['mlat']
#    latsize     = kw['latsize']
#    had         = kw['had']
#
#    if isinstance(h5_file, basestring) :
#        h5f = h5py.File(h5_file, 'a')
#    elif isinstance(h5_file, h5py.File) or isinstance(h5_file, h5py.Group) :
#        h5f = h5_file
#    else : raise ValueError(h5_file)
#
#    for flav in flav_list :
#        for op in op_list :
#            print("runme_calc_save_ff : ", op, flav)
#            ir_list = lhpd.latcorr.op_default_ir_list(op)
#            gg      = ens_data_get(flav=flav, op=op, **kw)
#            h5k = '/%s/%s/%s' % (had, op, flav)
#            lhpd.h5_io.h5_purge_keys(h5f, [h5k])
#            kw1 = dict(kw)
#            lhpd.purge_keys(kw1, ['mlat', 'latsize'])
#            lhpd.latcorr.calc_save_ff(h5f, h5k, gg, mcgrp_list, ssgrp,
#                         op, ir_list, mlat, latsize, method_list, **kw1)
#
#    if isinstance(h5_file, basestring) :
#        h5f.close()
#    elif isinstance(h5_file, h5py.File) or isinstance(h5_file, h5py.Group) :
#        h5f.flush()
#    else : raise ValueError(h5_file)
#
#def h5_save_sum_rcut(h5f, h5k_top, d, rcut_list, axes=None) :
#    """ save rcut qtopo density to h5f:<h5k_top>/rcut<rcut> """
#    d       = np.asarray(d)
#    ls      = np.asarray(d.shape) 
#    if None is axes : axes = range(len(ls))
#    ls_a    = ls[list(axes)]
#    d_f     = np.fft.fftn(d, s=ls_a, axes=axes)
#    r2      = lhpd.np_mesh_rsq(ls, csrc=None, axes=axes)
#    for i_rcut, rcut in enumerate(rcut_list) :
#        xmask   = np.where(r2 <= rcut*rcut, 1, 0)
#        xmask_f = np.fft.fftn(xmask, s=ls_a, axes=axes)
#        dcut    = np.fft.ifftn(xmask_f * d_f, s=ls_a, axes=axes)
#        #print("rcut=%d  |dcut.real|=%e  |dcut.imag|=%e" % (
#        #        rcut, np.abs(dcut.real).sum(), np.abs(dcut.imag).sum()))
#
#        k       = "%s/rcut%d" % (h5k_top, rcut)
#        try : del h5f[k]
#        except : pass
#        h5f[k]  = np.array(dcut.real, dtype=np.float32)
#        h5f.flush()
