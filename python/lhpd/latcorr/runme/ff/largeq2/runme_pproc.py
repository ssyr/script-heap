#!/usr/bin/env python3
from future.utils import iteritems
import os, sys, re
import traceback
import lhpd
from lhpd import dictnew, dict_prod, dict_prod_auto
from attrdict import AttrDict as adict
from past.builtins import execfile

execfile('pproc_largeq2.py')

def print_usage(prg) :
    print("""Usage:
%s  [--sj=i{=0..TOT-1}/TOT]  [--mpi=PMI] [--bin=<binsize>]
        [--data-in=<data_in>]  [--data-out=<data_out>] 
        <what>   <config>[,<config2>,...]  [<job-list>] ++ [<job_id0>] ...
What :
    conv_{c2pt,bb}
    calc_op
    binx_{c2pt,op}

    calc_qloop_mom3
    binx_c3pt_qloop
    binx_c3x_qloop
    select_c3x_c3

    merge_{c2pt,bb}

""" % (prg,))


print_err_excraise = {
    AssertionError          : 1,
    SyntaxError             : 1,
    NameError               : 1,
    NotImplementedError     : 1,
    IndentationError        : 1,
    SystemError             : 1,
    TypeError               : 1,
    UnboundLocalError       : 1,
    ValueError              : 1,
    ZeroDivisionError       : 1,
    OSError                 : 1,
    RuntimeError            : 1,
    KeyError                : 1,
    IndexError              : 1,
    AttributeError          : 1,
    KeyboardInterrupt       : 1,
    MemoryError             : 1,
    # tmp debug
    IOError                 : 1,
}
do_ama = False
def print_err(what, i_sj, *clist) :
    #def strip_longopt(c) :
        #if isinstance(c, dict) :
            #c = dict(c)
            #purge_keys(c, [''])
        #else : return c
        
    #sys.stderr.write("%s[%d] %s {%s}\n" % (what, i_sj, ', '.join([ str(x) for x in clist] ))
    exc = sys.exc_info()
    sys.stderr.write("ERROR %s[%d]: %s\n" % (what, i_sj, repr(exc[1])))
    if 0 < print_err_excraise[exc[0]] : 
        traceback.print_tb(exc[2])
        raise

# TODO put to lib if not yet?
def strip_prefix(s, p):
    if s.startswith(p) : return s[len(p):]
    else : return None


if '__main__' == __name__ :

    argv = list(sys.argv)
    if len(argv) < 1 :
        print_usage('me')
        sys.exit(1)

    sj_this, sj_tot = 0, 1
    binsize     = 1

    opts = adict()
    while 1 < len(argv) and argv[1].startswith('--') :
        if argv[1].startswith('--sj=') :
            sj = re.match('^--sj=(\d+)/(\d+)$', argv[1])
            if not sj : 
                raise ValueError(argv[1])
            sj_this, sj_tot = [ int(x) for x in sj.groups() ]
            if (sj_this < 0 or sj_tot <= sj_this) :
                raise ValueError(("bad sj = %d/%d" % (sj_this, sj_tot)))
            print("sj = ", sj_this, sj_tot)

        elif argv[1].startswith('--data-in=') :
            opts.data_in = re.match('^--data-in=(.+)$', argv[1]).groups()[0]
        elif argv[1].startswith('--data=') :
            opts.data_in = re.match('^--data=(.+)$', argv[1]).groups()[0]
            opts.data_out= opts.data_in
        elif argv[1].startswith('--data-out=') :
            opts.data_out = re.match('^--data-out=(.+)$', argv[1]).groups()[0]

        elif argv[1].startswith('--bin=') :
            opts.binsize = int(re.match('^--bin=(.+)$', argv[1]).groups()[0])
            if opts.binsize <= 0 :
                raise ValueError(("bad binsize = %d" % opts.binsize))

        elif argv[1].startswith('--mpi=') :
            mpitype = re.match('^--mpi=(.+)$', argv[1]).groups()[0]
            if 'PMI' == mpitype :
                sj_this = int(os.environ['PMI_RANK'])
                sj_tot  = int(os.environ['PMI_SIZE'])
            else : raise ValueError(("bad mpi=%s" % mpitype))

            if (sj_this < 0 or sj_tot <= sj_this) :
                raise ValueError(("bad sj[PMI] = %d/%d" % (sj_this, sj_tot)))
            print("sj = ", sj_this, sj_tot)

        else :
            sys.stderr.write("bad arg='%s'\n" % argv[1])
            print_usage(argv[0] or 'me')
            sys.exit(1)
        argv.pop(1)

    if len(argv) < 3 :
        print_usage(argv[0] or 'me')
        sys.exit(1)

    try : isep = argv.index('++')
    except : isep = len(argv)
    #print("len(argv)=%d  isep=%d  argv=%s" % (len(argv), isep, str(argv)))

    if isep < 3 :
        print_usage(argv[0] or 'me')
        sys.exit(1)

    cfglist_cmd_ = argv[isep+1:]
    cfglist_file_ = argv[3:isep]
    #print("joblist=file(%s) + %s" % (str(cfglist_file_), str(cfglist_cmd_)))

    cfgkey_list = list(cfglist_cmd_)
    for f in cfglist_file_ :
        cfgkey_list.extend(lhpd.read_key_list(f, 'samples'))

    if len(cfgkey_list) <= 0 :
        sys.stderr.write('empty list; exit\n')
        sys.exit(1)

    print("Total samples: ", cfgkey_list_str(cfgkey_list))

    # TODO check for repeated keys

    conf_py   = argv[2]
    print("FIXME data_in, data_out overwritten by config")
    for cf in conf_py.split(',') :
        if 0 == len(cf) : continue
        try :
            print("load config '%s'" %(cf,))
            execfile(cf)
            #print("#disco_ins_vi_spec_list =", disco_ins_vi_spec_list)
        except : 
            print_err('load config', -1, (cf,))

    print("SET opt %s" % str(opts))
    for k, v in iteritems(opts) : globals()[k] = v
    print("data_in  = '%s'" % data_in)
    print("data_out = '%s'" % data_out)


def runme_conv_c2pt(
        cfgkey, c2_d,
        ama_list = ama_list, 
        attrs_kw={}) :

    """ convert CP-even 2pt functions 
    """
    had     = c2_d['had']
    bc_t    = get_hadron_bc(had, ferm_bc[t_axis])
    saved_bc_t= c2_d['saved_bc_t']
    print("runme_conv_c2pt: bc_t=%s, saved_bc_t=%s" % (str(bc_t), str(saved_bc_t)))
    for ama in ama_list :
        print("# %s %s %s %s %s " % (c, ama_str(ama), had, c2_d['tpol_list'], srcsnk_str_f(c2_d)))
        latcorr_conv2hdf(
                dict(data_dir=data_out, cfgkey=cfgkey, ama=ama),        # dat_d
                dict(kind='c2', c2_d=c2_d),
                make_csrcgrp_list(ama, cfgkey),                         # csrcgrp_list
                latsize, t_axis, c2pt_hslab_len, bc_t, 
                dictnew(attrs_kw, source_hadron=had, sink_hadron=had),  # attrs_kw
                data_in=data_in, #data_dir_aff, 
                saved_bc_t=saved_bc_t,
                scale=c2_d.get('conv_scale'),
                VERBOSE=True) # attrs_kw

def runme_conv_c3pt_qbarq(
                cfgkey, c3_d, ins_d,
                ama_list = ama_list,
                attrs_kw={}) :
    had     = c3_d['had']
    bc_t    = get_hadron_bc(had, ferm_bc[t_axis])
    saved_bc_t= c3_d['saved_bc_t']
    print("runme_conv_c3pt_qbarq: bc_t=%s, saved_bc_t=%s" % (str(bc_t), str(saved_bc_t)))
    for ama in ama_list :
        print("# %s %s %s %s %s " % (c, ama_str(ama), hadron_str(c3_d), srcsnk_str_f(c3_d), c3snk_str_f(c3_d)))
        latcorr_conv2hdf(
                dict(data_dir=data_out, cfgkey=cfgkey, ama=ama),
                dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
                make_csrcgrp_list(ama, cfgkey),
                latsize, t_axis, c3pt_tlen, bc_t,
                dictnew(attrs_kw, source_hadron=had, sink_hadron=had, 
                        tpol=c3_d['tpol'], time_neg=False, sink_mom=c3_d['psnk']),
                data_in=data_in, #data_dir_aff,
                saved_bc_t=saved_bc_t,
                scale=c3_d.get('conv_scale'),
                VERBOSE=True)


###############################################################################
# calc_op, bin_unbias_op
###############################################################################
def runme_calc_save_op(
                cfgkey, c3_d, ins_d,
                ama_list = ama_list,
                attrs_kw={}) :
    
    for ama in ama_list :
        print("# %s %s %s %s %s " % (c, ama_str(ama), hadron_str(c3_d), srcsnk_str_f(c3_d), c3snk_str_f(c3_d)))
        latcorr_calc_qbarq_op(
                dict(kind='conv', data_dir=data_out, cfgkey=cfgkey, ama=ama),
                dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
                VERBOSE=True)

# TODO move to library pproc_common.py
def runme_qloop_orig_to_mom3(
        c, vi_d,                # general for any dvi
        lpath_list,             # specific to qloop-BB
        qext_list,
        VERBOSE=False) :
    funcname_   = 'runme_qloop_orig_to_mom3'
    vi_kind = vi_d['kind']
    assert('qloop' == vi_kind)
    dat_d   = dict(cfgkey=c, kind='conv')
    lc_d    = dict(kind='dvi', vi_d=vi_d)

    # FIXME general t_axis
    assert(len(latsize) - 1 == t_axis)
    latsize_x3 = latsize[:-1]
    lt  = latsize[t_axis]

    qext_list_i = (qext_list + latsize_x3) % latsize_x3

    h5file_in,  h5kpath_in = get_lcdb_loc(dictnew(dat_d, data_dir=data_in),  dictnew(lc_d, dkind='orig'))
    h5f_in  = h5py.File(h5file_in, mode='r')
    h5file_out, h5kpath_out= get_lcdb_loc(dictnew(dat_d, data_dir=data_out), dictnew(lc_d, dkind='mom3'))
    lhpd.mkpath(os.path.dirname(h5file_out))
    h5f_out = h5py.File(h5file_out, mode='a')
    if VERBOSE: print("%s: %s[%s]  ->  %s[%s]" % (funcname_, 
            h5file_in, h5kpath_in, h5file_out, h5kpath_out))

    # now specific to qloop
    gamma_list = np.r_[0 : 16]
    n_gamma = len(gamma_list)
    n_lpath = len(lpath_list)
    n_qext  = len(qext_list)
    lhpd.h5_io.h5_purge_keys(h5f_out, [h5kpath_out])
    h = h5f_out.require_dataset(h5kpath_out, fletcher32=True, dtype=np.complex128,
            shape=(n_gamma, n_lpath, n_qext, lt))
    for g, (i_lp, lp) in it.product(
            gamma_list, enumerate(lpath_list) ):
        if VERBOSE: print("%s: g=%d lp='%s'" % (funcname_, g, lp))
        k = "%s/g%d" % (lp, g)
        h5d_src = h5f_in["%s/%s" % (h5kpath_in, k)]
        assert((h5d_src.shape == latsize).all())
        # sic! ifft === sum_x exp(i*q.x)*a_x over x,y,z (no t)
        a_q = np.fft.ifftn(h5d_src.value, axes=np.r_[:t_axis]) * np.prod(latsize_x3)
        h[g, i_lp] = np.array([ a_q[tuple(q)] for q in qext_list_i ])

    h.attrs['dim_spec']     = np.array(['i_gamma', 'i_linkpath', 'i_qext', 't'], dtype='S')
    h.attrs['linkpath_list']= np.array(lpath_list, dtype='S')
    h.attrs['gamma_list']   = gamma_list
    h.attrs['qext_list']    = np.array(qext_list, dtype=np.int32)
    h.attrs['t_dir']        = t_axis
    h5f_in.close()
    h5f_out.flush()
    h5f_out.flush()




# ins_d = { kind='op', flav='discUD', 
#           approx='defl_hp0n512_sl' }
#
#  vi_d     input vi 
#           e.g. = {kind='qloop', flav=IGNORED, approx='defl_hp0n512_sl' }
# ins_d     output ins
#           e.g. = {kind='op', flav='S', op='tensor1', ir='H4_T1_d4r1', qext_list=..., }
def runme_binx_c3pt_qloop(cfgkey_list, ama_list, c3_d, ins_d, vi_d, VERBOSE=False) :
    funcname_   = 'runme_binx_c3pt_qloop'
    assert('qloop' == vi_d['kind'])
    assert('op' == ins_d['kind'])
    assert(ins_d['flav'] == ("disc%s" % vi_d['flav']))
    ins_op  = ins_d['op']
    ins_ir  = ins_d['ir']

    # FIXME general t_axis
    assert(len(latsize) - 1 == t_axis)
    latsize_x3 = latsize[:-1]
    lt  = latsize[t_axis]

    qext_list   = ins_d['qext_list']
    assert(2 == len(qext_list.shape))
    assert(qext_list.shape[1] == len(latsize) - 1)
    n_qext      = len(qext_list)
    n_comp      = lhpd.latcorr.ir_comp_len(ins_ir)

    qloop_dat_d = dict(kind='conv', data_dir=data_in)
    qloop_lc_d  = dict(kind='dvi', dkind='mom3', vi_d=vi_d)

    c3_tpol     = c3_d['tpol']
    c3_psnk     = np.asarray(c3_d['psnk'])
    c3_tsep     = c3_d['tsep']

    c2_d        = dict(c3_d)
    lhpd.purge_keys(c2_d, ['tpol', 'psnk', 'tsep', 'qext_list'])
    c2_dat_d    = dict(kind='conv', data_dir=data_in)

    tpol_list_G16 = np.array([ 'Tg%d' % g for g in np.r_[:16] ], dtype='S')


    def get_qloop_op(c):
        # return [i_comp, i_qext, t]
        h5file, h5kpath = get_lcdb_loc(dictnew(qloop_dat_d, cfgkey=c), qloop_lc_d)

        h5f     = h5py.File(h5file, 'r')
        h5d     = h5f[h5kpath]
        qloop_gamma_list = h5d.attrs['gamma_list']
        qloop_lpath_list = h5d.attrs['linkpath_list']
        qloop_qext_list  = h5d.attrs['qext_list']
        qext_slice = lhpd.np_index_first_list(qext_list, h5d.attrs['qext_list'])

        def get_qloop(g, lp) :  # analog of pproc_common:func_get_bb
            # return [i_qext, t]
            i_g     = lhpd.np_find_first(g,  qloop_gamma_list)[0]
            i_lp    = lhpd.np_find_first(lp, qloop_lpath_list)[0]
            return h5d[i_g, i_lp][qext_slice]    # sic! slice in memory

        # TODO rewrite using fancy projectors
        if      'tensor0'   == ins_op :    
            res = np.array([ get_qloop( 0, 'l0_') ])
        elif    'pstensor0' == ins_op :
            res = np.array([ get_qloop(15, 'l0_') ])
        elif    'tensor1'   == ins_op :
            res = np.array([ 
                     get_qloop( 1, 'l0_'), 
                     get_qloop( 2, 'l0_'), 
                     get_qloop( 4, 'l0_'), 
                     get_qloop( 8, 'l0_') ])
        elif    'pstensor1' == ins_op :
            res = np.array([ 
                     get_qloop(14, 'l0_'), 
                    -get_qloop(13, 'l0_'), 
                     get_qloop(11, 'l0_'), 
                    -get_qloop( 7, 'l0_') ])
        elif    'sigma2a2' == ins_op :
            res = 1j * np.array([
                     get_qloop( 3, 'l0_'),
                     get_qloop( 5, 'l0_'),
                     get_qloop( 9, 'l0_'),
                     get_qloop( 6, 'l0_'),
                     get_qloop(10, 'l0_'),
                     get_qloop(12, 'l0_') ])
        else : raise ValueError(ins_op)
        
        h5f.close()
        return res


    def mfunc_qloop_tslice(d, **kw) :
        funcname_   = 'mfunc_qloop_tslice'
        mspec_list  = kw['mlist']
        n_data      = len(mspec_list)
        c2_attrs    = kw['attrs']
        assert((tpol_list_G16 == c2_attrs['tpol_list']).all())  # need full tpol matrix to compute specific
        i_psnk  = lhpd.np_find_first(c3_psnk, c2_attrs['psnk_list'])

        # extract and tpol-project c2
        c2_G16  = d[:, :, i_psnk, c3_tsep]
        c2      = lhpd.latcorr.calc_tpol_fromGamma(c3_tpol, lambda g: c2_G16[:, g])

        # group by ckpoint_id 
        m_c = {}
        for i_m, m in enumerate(mspec_list) : 
            m_c.setdefault(m['ckpoint_id'], []).append((i_m, m))

        res     = np.empty((n_data, n_comp, n_qext, c3_tsep + 1), d.dtype)
        res[:]  = np.nan
        for c, mlist in iteritems(m_c) :
            disc_q_t = get_qloop_op(c)

            for i_m, m in mlist :
                csrc    = m['source_coord']
                tsrc    = csrc[t_axis]
                csrc_x3 = csrc[:-1]
                # [i_qext]
                phase_list = np.exp(-1j * (qext_list * csrc_x3 * 2 * np.pi / latsize_x3).sum(-1))
                # [i_comp, i_qext, t_ins]
                qloop_ins  = lhpd.np_circshift_coord(disc_q_t, [0, 0, tsrc])[..., : c3_tsep + 1]

                # XXX minus sign for anticommut. qbar-q
                res[i_m] = ((-1.) * c2[i_m] * qloop_ins * phase_list[..., None])   

        return res

    attrs_kw = dict(
        dim_spec    = np.array(['i_data', 'i_comp', 'i_qext', 'i_tau' ], dtype='S32'),
        qext_list   = qext_list,
        tpol        = c3_tpol,
        sink_mom    = c3_psnk,
        comp_list   = lhpd.latcorr.H4_repr_comp[ins_ir],
        tau_list    = np.r_[ : c3_tsep + 1],
        source_sink_dt = c3_tsep,
        )

    latcorr_bin_unbias(cfgkey_list, ama_list,
        dict(data_dir=data_out),
        dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
        lc_d_in=dict(kind='c2', c2_d=c2_d),
        data_in=data_in,
        mfunc=mfunc_qloop_tslice,
        binsize=binsize,
        do_ama=do_ama,
        attrs_kw=attrs_kw,
        VERBOSE=VERBOSE)


# extended c3pt
# ins_d = { kind='op', flav='discUD', 
#           approx='defl_hp0n512_sl' }
#
#  vi_d     input vi 
#           e.g. = {kind='qloop', flav=IGNORED, approx='defl_hp0n512_sl' }
# ins_d     output ins
#           e.g. = {kind='op', flav='S', op='tensor1', ir='H4_T1_d4r1', qext_list=..., }
def runme_binx_c3x_qloop(cfgkey_list, ama_list, c3x_d, ins_d, vi_d, VERBOSE=False) :
    funcname_   = 'runme_binx_c3pt_qloop'
    assert('qloop' == vi_d['kind'])
    assert('op' == ins_d['kind'])
    assert(ins_d['flav'] == ("disc%s" % vi_d['flav']))
    ins_op      = ins_d['op']
    ins_ir      = ins_d['ir']

    # FIXME general t_axis
    assert(len(latsize) - 1 == t_axis)
    latsize_x3 = latsize[:-1]
    lt  = latsize[t_axis]

    tpol_list   = np.asarray(c3x_d['tpol_list'], dtype='S')
    psnk_list   = np.asarray(c3x_d['psnk_list'])
    tsep_list   = np.asarray(c3x_d['tsep_list'])
    c3_tlen     = 1 + tsep_list.max()

    qext_list   = ins_d['qext_list']
    assert(2 == len(qext_list.shape))
    assert(qext_list.shape[1] == len(latsize) - 1)
    n_tpol      = len(tpol_list)
    n_psnk      = len(psnk_list)
    n_tsep      = len(tsep_list)
    n_qext      = len(qext_list)
    n_comp      = lhpd.latcorr.ir_comp_len(ins_ir)

    qloop_dat_d = dict(kind='conv', data_dir=data_in)
    qloop_lc_d  = dict(kind='dvi', dkind='mom3', vi_d=vi_d)

    c2_d        = dict(c3x_d)
    lhpd.purge_keys(c2_d, ['tpol_list', 'psnk_list', 'tsep_list', 'qext_list'])
    c2_dat_d    = dict(kind='conv', data_dir=data_in)

    tpol_list_G16 = np.array([ 'Tg%d' % g for g in np.r_[:16] ], dtype='S')

    # FIXME repeated function from runme_binx_c3pt_qloop with some modifications
    def get_qloop_op(c):
        # return [i_comp, i_qext, t]
        h5file, h5kpath = get_lcdb_loc(dictnew(qloop_dat_d, cfgkey=c), qloop_lc_d)

        h5f     = h5py.File(h5file, 'r')
        h5d     = h5f[h5kpath]
        qloop_gamma_list = h5d.attrs['gamma_list']
        qloop_lpath_list = h5d.attrs['linkpath_list']
        qloop_qext_list  = h5d.attrs['qext_list']
        i_qext_list = lhpd.np_index_first_list(qext_list, h5d.attrs['qext_list'])

        def get_qloop(g, lp) :  # analog of pproc_common:func_get_bb
            # return [i_qext, t]
            i_g     = lhpd.np_find_first(g,  qloop_gamma_list)[0]
            i_lp    = lhpd.np_find_first(lp, qloop_lpath_list)[0]
            d       = h5d[i_g, i_lp]
            return d[i_qext_list]    # sic! slice in memory

        # TODO rewrite using fancy projectors
        if      'tensor0'   == ins_op :    
            res = np.array([ get_qloop( 0, 'l0_') ])
        elif    'pstensor0' == ins_op :
            res = np.array([ get_qloop(15, 'l0_') ])
        elif    'tensor1'   == ins_op :
            res = np.array([ 
                     get_qloop( 1, 'l0_'), 
                     get_qloop( 2, 'l0_'), 
                     get_qloop( 4, 'l0_'), 
                     get_qloop( 8, 'l0_') ])
        elif    'pstensor1' == ins_op :
            res = np.array([ 
                     get_qloop(14, 'l0_'), 
                    -get_qloop(13, 'l0_'), 
                     get_qloop(11, 'l0_'), 
                    -get_qloop( 7, 'l0_') ])
        elif    'sigma2a2' == ins_op :
            res = 1j * np.array([
                     get_qloop( 3, 'l0_'),
                     get_qloop( 5, 'l0_'),
                     get_qloop( 9, 'l0_'),
                     get_qloop( 6, 'l0_'),
                     get_qloop(10, 'l0_'),
                     get_qloop(12, 'l0_') ])
        else : raise ValueError(ins_op)
        
        h5f.close()
        return res


    def mfunc_qloop_tslice(d, **kw) :
        funcname_   = 'mfunc_qloop_tslice'
        mspec_list  = kw['mlist']
        n_data      = len(mspec_list)
        c2_attrs    = kw['attrs']
        assert((tpol_list_G16 == c2_attrs['tpol_list']).all())  # need full tpol matrix to compute specific
        i_psnk_list = np.array([ i[0] for i in 
                lhpd.np_find_first_list(psnk_list, c2_attrs['psnk_list']) ])

        # extract and tpol-project : 
        # from d[i_data, i_tpol0, i_psnk0, t] 
        # to c2_G16,c2[i_data, i_tpol1, i_psnk1, i_tsep]
        c2_G16  = d[:, :, i_psnk_list][:, :, :, tsep_list]
        assert(c2_G16.shape == (n_data, len(tpol_list_G16), n_psnk, n_tsep))
        c2      = np.empty((n_data, n_tpol, n_psnk, n_tsep), dtype=c2_G16.dtype)
        c2[:]   = np.nan
        for i_tpol, tpol in enumerate(tpol_list) :
            c2[:, i_tpol] = lhpd.latcorr.calc_tpol_fromGamma(tpol, lambda g: c2_G16[:, g])


        # group by ckpoint_id 
        m_c = {}
        for i_m, m in enumerate(mspec_list) : 
            m_c.setdefault(m['ckpoint_id'], []).append((i_m, m))

        res     = np.empty((n_data, n_tpol, n_psnk, n_tsep, n_comp, n_qext, c3_tlen), d.dtype)
        res[:]  = np.nan
        for c, mlist in iteritems(m_c) :
            disc_q_t = get_qloop_op(c)

            for i_m, m in mlist :
                csrc    = m['source_coord']
                tsrc    = csrc[t_axis]
                csrc_x3 = csrc[:-1]
                # [i_qext]
                phase_list = np.exp(-1j * (qext_list * csrc_x3 * 2 * np.pi / latsize_x3).sum(-1))
                # [i_comp, i_qext, t_ins]
                qloop_ins  = (lhpd.np_circshift_coord(disc_q_t, [0, 0, tsrc])[..., : c3_tlen] 
                              * phase_list[..., None])
                # XXX sic! minus sign for anticommut. qbar-q
                # [i_data][i_tpol1, i_psnk1, i_tsep1, i_comp, i_qext, t_ins]
                res[i_m] = ((-1.) * c2[i_m][..., None, None, None] * qloop_ins)    

        return res

    attrs_kw = dict(
        dim_spec    = c3x_op_dim_spec,
        tpol_list   = tpol_list,
        psnk_list   = psnk_list,
        tsep_list   = tsep_list,
        comp_list   = lhpd.latcorr.H4_repr_comp[ins_ir],
        qext_list   = qext_list,
        tau_list    = np.r_[ : c3_tlen]
        )

    latcorr_bin_unbias(cfgkey_list, ama_list,
        dict(data_dir=data_out),
        dict(kind='c3x', c3x_d=c3x_d, ins_d=ins_d),
        lc_d_in=dict(kind='c2', c2_d=c2_d),
        data_in=data_in,
        mfunc=mfunc_qloop_tslice,
        binsize=binsize,
        do_ama=do_ama,
        attrs_kw=attrs_kw,
        VERBOSE=VERBOSE)

def runme_select_c3x_c3(cfgkey_list, ama_list, c3_d, ins_d,
        do_ama=False,
        VERBOSE=False):

    dat_d   = dict(data_dir=data_in, kind='ub')
    tpol    = c3_d['tpol']
    psnk    = c3_d['psnk']
    tsep    = c3_d['tsep']
    c3x_d   = dict(c3_d)
    lhpd.purge_keys(c3x_d, ['tpol', 'psnk', 'tsep', 'qext_list'])
    
    if do_ama :
        raise ValueError('only ub data supported')
    # ub part
    h5file_in,  h5kpath_in  = get_lcdb_loc(dict(data_dir=data_in, kind='ub'), 
                    dict(kind='c3x', c3x_d=c3x_d, ins_d=ins_d))
    h5file_out, h5kpath_out = get_lcdb_loc(dict(data_dir=data_out, kind='ub'), 
                    dict(kind='c3', c3_d=c3_d, ins_d=ins_d))
    h5f_in  = h5py.File(h5file_in, 'r')
    lhpd.mkpath(os.path.dirname(h5file_out))
    h5f_out = h5py.File(h5file_out, 'a')
    h5select_c3x_c3(h5f_out, h5kpath_out, h5f_in[h5kpath_in],
            tpol, psnk, tsep, VERBOSE=VERBOSE)

    h5f_out.flush()
    h5f_out.close()
    h5f_in.close()

##############################################################################
##############################################################################
if '__main__' == __name__ :
    what   = argv[1]
    jobid_list      = cfgkey_list

    def  subjob_iter(j_iter) :
        j_list  = list(j_iter)
        nj      = len(j_list)
        jj0     = ( sj_this    * nj) // sj_tot
        jj1     = ((sj_this+1) * nj) // sj_tot
        for j in j_list[jj0:jj1] : yield j

    if   'conv_c2pt' == what :
        for i_sj, (c, c2_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, c2pt_spec_list))) :
            try : runme_conv_c2pt(c, c2_d, ama_list=ama_list)
            except : print_err(what, i_sj, (c, c2_d))

    elif 'binx_c2pt' == what :
        for i_sj, c2_d in enumerate(subjob_iter(c2pt_spec_list)) :
            try : 
                latcorr_bin_unbias(cfgkey_list, ama_list, 
                    dict(data_dir=data_out),
                    dict(kind='c2', c2_d=c2_d),
                    data_in=data_in,
                    binsize=binsize,
                    do_ama=do_ama,
                    VERBOSE=True)
            except : print_err(what, i_sj, (c2_d,))

    # FIXME test merge
    elif 'merge_c2pt' == what :
        for i_sj, (c2_d,) in enumerate(subjob_iter(
                it.product(c2pt_spec_list))) :
            for ama in ama_list :
                try :
                    latcorr_merge(cfgkey_list, ama_list, 
                        dict(data_dir=data_out, ama=ama),
                        dict(kind='c2', c2_d=c2_d),
                            VERBOSE=True)
                except : print_err(what, i_sj, ama, c2_d)

    elif 'conv_bb' == what : # TODO generalize for c3pt=bb,tmd,...
        for i_sj, (c, c3_d, ins_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, c3pt_spec_list, bb_spec_list))) :
            try : runme_conv_c3pt_qbarq(c, c3_d, ins_d, ama_list=ama_list)
            except : print_err(what, i_sj, c, c3_d, ins_d)
    
    elif 'calc_op' == what :
        for i_sj, (c, c3_d, ins_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, c3pt_spec_list, op_spec_list))) :
            try : runme_calc_save_op(c, c3_d, ins_d, ama_list=ama_list)
            except : print_err(what, i_sj, c, c3_d, ins_d)

    elif 'binx_op' == what :
        for i_sj, (c3_d, ins_d) in enumerate(subjob_iter(it.product(
                c3pt_spec_list, op_spec_list))) :
            for ir in lhpd.latcorr.op_default_ir_list(ins_d['op']) :
                try :
                    latcorr_bin_unbias(cfgkey_list, ama_list, 
                        dict(data_dir=data_out),
                        dict(kind='c3', c3_d=c3_d, ins_d=dictnew(ins_d, ir=ir[0])),
                        data_in=data_in,
                        binsize=binsize,
                        do_ama=do_ama,
                        VERBOSE=True)
                except : print_err(what, i_sj, (c3_d, ins_d))

    # FIXME merge is incorrect in merge_c2pt or merge_op
    elif 'merge_op' == what :
        for i_sj, (c3_d, ins_d) in enumerate(subjob_iter(it.product(
                c3pt_spec_list, op_spec_list))) :
            for ir in lhpd.latcorr.op_default_ir_list(ins_d['op']) :
                for ama in ama_list :
                    try :
                        latcorr_merge(cfgkey_list,
                            dict(data_dir=data_out, ama=ama),
                            dict(kind='c3', c3_d=c3_d, ins_d=dictnew(ins_d, ir=ir[0])),
                                VERBOSE=True)
                    except : print_err(what, i_sj, (ama, c3_d, ins_d, ir))

    elif 'calc_qloop_mom3' == what :
        for i_sj, (c, vi_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, disco_vi_spec_list))) :
            try :
                runme_qloop_orig_to_mom3(c, vi_d,
                        disco_lpath_list, disco_qext_list,
                        VERBOSE=True)
            except : print_err(what, i_sj, (c, vi_d))

    elif 'binx_c3pt_qloop' == what :
        for i_sj, (c3_d, (ins_d, vi_d)) in enumerate(subjob_iter(it.product(
                disco_c3pt_spec_list, disco_ins_vi_spec_list))) :
            try :
                runme_binx_c3pt_qloop(cfgkey_list, ama_list, c3_d, ins_d, vi_d,
                        VERBOSE=True)
            except : print_err(what, i_sj, (c3_d, vi_d))

    elif 'binx_c3x_qloop' == what :
        for i_sj, (c3x_d, (ins_d, vi_d)) in enumerate(subjob_iter(it.product(
                disco_c3x_spec_list, disco_ins_vi_spec_list))) :
            try :
                runme_binx_c3x_qloop(cfgkey_list, ama_list, c3x_d, ins_d, vi_d,
                        VERBOSE=True)
            except : print_err(what, i_sj, (c3x_d, vi_d))

    elif 'select_c3x_c3' == what :
        for i_sj, (c3_d, (ins_d, vi_d)) in enumerate(subjob_iter(it.product(
                disco_c3pt_spec_list, disco_ins_vi_spec_list))) :
            try :
                runme_select_c3x_c3(cfgkey_list, ama_list, c3_d, ins_d, 
                        VERBOSE=True)
            except : print_err(what, i_sj, (c3_d, ins_d))

    else : raise ValueError(what)
