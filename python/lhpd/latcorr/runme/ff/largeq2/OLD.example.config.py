from past.builtins import execfile
import numpy as np
import lhpd
from lhpd.latcorr import make_linkpath_list
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted
from lhpd.misc.strkey import *
from lhpd.misc import ldir_3d, space2full

latsize = np.r_[32,32,32,64]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]


cfg_index   = lhpd.read_key_list('../list.cfg.all', 'index')
#cfg_list    = lhpd.read_key_list('../list.cfg.run1.n100', 'samples')   # XXX read from cmdline instead

ama_ex      = 'ex'
ama_sl      = 'sl'
ama_list = [ama_ex, ama_sl]
def ama_str(ama) : return ama

csrc_ngrid = {
    ama_ex : [1,1,1,4],
    ama_sl : [2,2,2,4] }
csrc_meas_dxc    = [7,11,13,23]
csrc_meas_x0     = [0,0,0,0]

def make_csrc_list(ama, cfg_key):
    return lhpd.make_src_grid(csrc_ngrid[ama], latsize, 
                csrc_meas_x0, csrc_meas_dxc, 
                1 + cfg_index.index(cfg_key))

ncoh_src_t      = 4 # number of coherent src in one group
c2pt_hslab_len  = latsize[t_axis] // ncoh_src_t
def make_csrcgrp_list(ama, cfg_key):
    ngrid_t     = csrc_ngrid[ama][t_axis]
    assert(0 == ngrid_t % ncoh_src_t)
    ncoh_grp    = ngrid_t // ncoh_src_t
    # assuming sources are grouped so that csrc[t_axis] changes fastest
    csrc_list   = make_csrc_list(ama, cfg_key)
    ndim        =len(latsize)
    csrcgrp_list= csrc_list.reshape(-1, ncoh_src_t, ncoh_grp, ndim) # split into groups with equal spatial coords
    csrcgrp_list= csrcgrp_list.transpose(0,2,1,3)                   # rearrange to have max. t-separation in each grp
    csrcgrp_list= csrcgrp_list.reshape(-1, ncoh_src_t, ndim)        # flatten (0,1) axes       
    assert((csrcgrp_list[:, :, :-1] == csrcgrp_list[:, 0:1, :-1]).all())
    return csrcgrp_list


#meas_list_sl = np.array([(c, csrc) for c in cfg_list for csrc in make_csrc_list('sl', c)], dtype=h5_meas_dtype)
#meas_list_ex = np.array([(c, csrc) for c in cfg_list for csrc in make_csrc_list('ex', c)], dtype=h5_meas_dtype)
#meas_list = { 
    #ama_ex : meas_list_ex,
    #ama_sl : meas_list_sl
#}

pm1     = np.r_[-1:1+1] 
pm2     = np.r_[-2:1+2]
pm6     = np.r_[-6:1+6]
pm10    = np.r_[-10:1+10]

kbxp10  = space2full(ldir_3d['x'], t_axis)
kbxm10  = space2full(ldir_3d['X'], t_axis)
kbym10  = space2full(ldir_3d['Z'], t_axis)
kbzm10  = space2full(ldir_3d['Y'], t_axis)

#smear_shape1 = dict(type='wuppertal', t_axis=t_axis,
  #wup_U=gauge_u_ape, wup_N=50, wup_alpha=2.0 )
smear_opt_b20       = dict(tag='GN2x50bxp20', boost=2*kbxp10)
smear_opt_b20neg    = dict(tag='GN2x50bxm20', boost=2*kbxm10)
srcsnk_sm_tag_list  = [ (smear_opt_b20['tag'], smear_opt_b20neg['tag']) ]
symm_sm_tag_list    = [ '%s_%s' % (s[0], s[0]) for s in srcsnk_sm_tag_list ]
skew_sm_tag_list    = [ '%s_%s' % (s[0], s[1]) for s in srcsnk_sm_tag_list ]

########### c2pt

c2pt_had_list   = ['proton']

# TODO add skew
c2pt_symm_psnk_list = normsorted(lhpd.range_prod([pm10, pm2, pm2]))
c2pt_sm_srcsnk_list = ['SS', 'SP']
c2pt_sm_tag_list    = symm_sm_tag_list
c2pt_tpol_list  = [ 'Tg%d' % g for g in range(16) ]

############## c3pt

c3pt_tpol_list      = ['posSzplus']
c3pt_psnk_list      = np.asarray([[4,0,0]])
c3pt_sm_tag_list    = skew_sm_tag_list

c3pt_qext_list      = normsorted(lhpd.range_prod([pm6, pm1, pm1]))

c3pt_tsep_list = [ 8 ]
c3pt_tsnk_full = False

flav_list       = ['U', 'D']
flav_cedm_list  = flav_list
flav_cur_list   = flav_list
gf_cedm_list    = ['orig']
 
c3pt_lpath_min, c3pt_lpath_max=0,1
lpath_list = make_linkpath_list(c3pt_lpath_min, c3pt_lpath_max, dim=4)
lpath_str_list = [ lpath_str(lp) for lp in lpath_list ]

c3pt_op_list = ['tensor1']
#c3pt_op_list = ['pstensor1']
#c3pt_op_list = ['tensor1', 'pstensor1', 'sigma2a2']


data_top='data.in/run1'
data_out='data.out/run1'
execfile('config_lcdb.py')

