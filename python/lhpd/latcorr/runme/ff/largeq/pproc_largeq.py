from __future__ import print_function
from past.builtins import execfile
#import ipdb
from future.utils import iteritems
import numpy as np
import h5py
import lhpd
from lhpd.misc import np_find_first, np_match, purge_keys, np_at
import os, sys, time, tempfile, errno
from lhpd.pymath.H4_tensor import *
import matplotlib as mpl
import matplotlib.pyplot as plt
import dataview as dv
from lhpd.limits import *
from lhpd.latcorr import op_default_ir_list, ir_list_comp_len



###############################################################################
# HADSPEC
###############################################################################

def conv_hspec2hdf(
            h5g, h5key,             # output
            latsize, 
            hsfilekpath_func,       # input
            cfgkey, csrcgrp_list, psnk_list, tpol_list, hslab_len,
            had_bc_t,
            attrs_kw, 
            t_dir=3, hs_dtype=np.complex128, h5_overwrite=True) :
    """
        csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
        
        save data to
        h5g[h5key][n_data, len(psnk_list), hslab_len]
    """
    """ TEST 
    """
    latsize     = np.asarray(latsize)
    ndim        = len(latsize)
    assert(4 == ndim)
    assert(3 == t_dir)  # smart, eh?
    lt          = latsize[t_dir]
    csrcgrp_list= np.asarray(csrcgrp_list)
    n_grp, grp_size = csrcgrp_list.shape[0:2]
    n_data      = n_grp * grp_size
    assert(4 == csrcgrp_list.shape[-1])
    n_psnk      = len(psnk_list)
    n_tpol      = len(tpol_list)
    
    csrc_list   = []
    hs_res      = np.empty((n_data, n_tpol, n_psnk, hslab_len), hs_dtype)

    i_data = 0
    for i_grp, csrcgrp in enumerate(csrcgrp_list) :
        n_src       = len(csrcgrp)
        # check coord
        for i_src in range(n_src) :
            for mu in range(ndim):
                assert(mu == t_dir or csrcgrp[i_src][mu] == csrcgrp[0][mu])
        print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
        csrc0       = csrcgrp[0]
        tsrc0       = csrc0[t_dir]
        
        for i_tpol, tpol in enumerate(tpol_list) :
            hsfile, hskpath = hsfilekpath_func(csrc0, tpol)
            aff_r   = aff.Reader(hsfile)
            hs      = lhpd.latcorr.aff_read_hadspec_list(aff_r, hskpath, psnk_list)
            aff_r.close()

            for i_c, c in enumerate(csrcgrp) :
                t_sh    = (lt + c[t_dir] - tsrc0) % lt
                # correcting BC[t] applied in [Qlua]save_2pt_list
                if tsrc0 <= c[t_dir] : bc_factor = 1.
                else : bc_factor = 1. / had_bc_t
                hs_res[i_data + i_c, i_tpol] = bc_factor * hs[..., t_sh : t_sh + hslab_len]

        csrc_list.extend(list(csrcgrp))
        i_data += len(csrcgrp)

    assert(n_data == i_data)

    meas_spec_list = np.array([ (cfgkey, c) for c in csrc_list], 
                        dtype=lhpd.h5_io.h5_meas_dtype)
    assert(len(meas_spec_list) == n_data)

    if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5key])
    hs_shape = (n_data, n_tpol, n_psnk, hslab_len)
    h = h5g.require_dataset(h5key, hs_shape, hs_dtype, fletcher32=True)
    h[:] = hs_res

    # "axes"
    h.attrs['dim_spec']      = np.array(['i_data', 'i_tpol', 'i_psnk', 'i_t' ],
                                           dtype='S32')
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(h, meas_spec_list)
    h.attrs['psnk_list']    = np.array(psnk_list, dtype=np.int32)
    h.attrs['tpol_list']    = np.array(tpol_list, dtype='S16')
    h.attrs['t_list']       = np.r_[0 : hslab_len]

    for k, v in iteritems(attrs_kw) :
        h.attrs[k] = v

    h.file.flush()


###############################################################################
# BB
###############################################################################

def conv_bb2hdf(
            h5g, h5key,             # output
            latsize, 
            bbfilekpath_func,       # input
            cfgkey, csrcgrp_list, tsep, qext_list, lpath_list, 
            had_bc_t,
            attrs_kw, 
            t_dir=3, bb_dtype=np.complex128, h5_overwrite=True) :
    """
        csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
        
        save data to
        h5g[h5key][n_data, n_gamma, len(lpath_list), len(qext_list), tsep+1]
    """
    """ TEST 
    """
    latsize     = np.asarray(latsize)
    ndim        = len(latsize)
    assert(4 == ndim)
    assert(3 == t_dir)  # smart, eh?
    lt          = latsize[t_dir]
    csrcgrp_list= np.asarray(csrcgrp_list)
    n_grp, grp_size = csrcgrp_list.shape[0:2]
    n_data      = n_grp * grp_size
    assert(4 == csrcgrp_list.shape[-1])

    qext_list   = np.array(qext_list)
    n_qext      = len(qext_list)
    n_gamma     = 16
    n_lpath     = len(lpath_list)

    csrc_list   = []
    bb_res      = np.empty((n_data, n_gamma, n_lpath, n_qext, 1+tsep), bb_dtype)

    i_data = 0
    for i_grp, csrcgrp in enumerate(csrcgrp_list) :
        n_src       = len(csrcgrp)
        # check coord
        for i_src in range(n_src) :
            for mu in range(ndim):
                assert(mu == t_dir or csrcgrp[i_src][mu] == csrcgrp[0][mu])
        print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
        csrc0       = csrcgrp[0]
        tsrc0       = csrc0[t_dir]
        tsnk_full   = (lt + tsrc0 - 1) % lt

        # discard the sample if error occurs

        bbfile, bbkpath = bbfilekpath_func(csrc0, tsnk_full)
        aff_r   = aff.Reader(bbfile)
        bb      = lhpd.latcorr.aff_read_bb_list(aff_r, bbkpath, 
                        range(n_gamma), lpath_list, qext_list)
        aff_r.close()

        for i_c, c in enumerate(csrcgrp) :
            # XXX [Qlua] qcd.save_bb DOES NOT apply BC[t]
            # XXX calc-boostsm-c23pt.qlua DOES apply BC[t] to seqsrc
            # XXX therefore, NO NEED TO APPLY BC_FACTOR HERE
            #if c[t_dir] + tsep < lt: bc_factor = 1
            #else : bc_factor = had_bc_t
            bc_factor = 1.
            t_sh    = (lt + c[t_dir] - tsrc0) % lt
            bb_res[i_data] = bc_factor * bb[..., t_sh : t_sh + tsep + 1]
            csrc_list.append(c)
            i_data += 1

    assert(n_data == i_data)

    meas_spec_list = np.array([ (cfgkey, c) for c in csrc_list], 
                        dtype=lhpd.h5_io.h5_meas_dtype)
    assert(len(meas_spec_list) == n_data)

    if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5key])
    bb_shape = (n_data, n_gamma, len(lpath_list), len(qext_list), tsep + 1)
    b = h5g.require_dataset(h5key, bb_shape, bb_dtype, fletcher32=True)
    b[:] = bb_res

    # "axes"
    b.attrs['dim_spec']      = np.array(['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau' ],
                                           dtype='S32')
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(b, meas_spec_list)
    b.attrs['gamma_list']    = np.r_[0 : 16]
    b.attrs['linkpath_list'] = np.array(
            [ lhpd.strkey.lpath_str(lp) for lp in lpath_list ],
            dtype='S16')
    b.attrs['qext_list']     = np.array(qext_list, dtype=np.int32)
    b.attrs['tau_list']      = np.r_[0 : tsep+1]
    # other parameters
    b.attrs['source_sink_dt']= int(tsep)

    for k, v in iteritems(attrs_kw) :
        b.attrs[k] = v

    b.file.flush()



###############################################################################
# OP
###############################################################################
def calc_qbarDq_v2(func_get_bb_v2, gamma_list, n_deriv, 
                q3ext, latsize, tdir, 
                dir_deriv='symm', bb_dim=4, keep_nan=False) :
    """ compute 
        func_get_bb_v2(gamma, lpath)
                    return BB[i_data, t] for gamma, lpath
        return deriv[i_data, t, i_gamma, mu_1, ... mu_N]
                where (i_data, t) axes are "copied" from func_get_bb

        q3ext       3D momentum in lat.units
        keep_nan    mark undefined elements (ususally on border in t) with 'nan'
                    otherwise, these elements are replaced with zeros (default)

        NOTE if generalizing for other shapes returned by func_get_bb,
             make sure that the time axis is the last one

        TODO ? generalize for other than "cutoff" BC in time dir
    """
    q3ext = np.asarray(q3ext)

    # qbar is at the origin ; add shifted value to make the derivative symmetric
    def bb_shift_from(y, mu, d, t_axis=-1) :
        """ for d=+1, return y(z+\hat\mu) 
            TODO mark absent values by nan and set them to zero at the end
        """
        if mu == tdir : # coordinate direction : shift along t_axis
            lt  = y.shape[t_axis]
            res = np.zeros_like(y)
            if 1 == d : 
                np_at(res, (t_axis, slice(0, lt-1)))[:] = np_at(y, (t_axis, slice(1, lt)))
                np_at(res, (t_axis, lt-1))[:] = np.nan
            elif -1 == d :
                
                np_at(res, (t_axis, slice(1, lt)))[:] = np_at(y, (t_axis, slice(0, lt-1)))
                np_at(res, (t_axis, 0))[:] = np.nan
            else : raise ValueError(d)
            return res
        else :  # Fourier-transformed direction : mult. by phase
            return x * np.exp(-2.j * np.pi / latsize[mu] * q3ext[mu] * d)
        
    n_gamma = len(gamma_list)
    x_shape = None
    t_axis  = None
    res     = None
    bb      = None
    for lpath in np.ndindex(*([2 * bb_dim] * n_deriv)) :
        lpath_str = lhpd.strkey.lpath_str(lhpd.strkey.lpath_simplify(
                        lpath, bb_dim=bb_dim))
        muN_slice = tuple([ mu_dir % bb_dim for mu_dir in reversed(lpath) ])
        for i_g, gamma in enumerate(gamma_list) :
            x = func_get_bb_v2(gamma, lpath_str)
            if None is res : 
                x_shape = x.shape
                res = np.zeros(x_shape + (n_gamma,) + (bb_dim,) * n_deriv, 
                               dtype=x.dtype)

            sgn = 1
            for i, mu_dir in enumerate(lpath) :
                # qbar is at the origin
                # d is displacement of q wrt qbar in coordinate mu
                mu, d = mu_dir % bb_dim, 2 * (mu_dir / bb_dim) - 1
                assert (0 <= mu and mu < bb_dim)
                assert (-1 == d or 1 == d)
                sgn *= d
                if   'symm' == dir_deriv :
                    x = .5 * (x + bb_shift_from(x, mu, -d)) 
                elif 'right' == dir_deriv :
                    pass
                elif 'left' == dir_deriv :
                    x = -bb_shift_from(x, mu, -d)
                else : raise ValueError(dir_deriv)
                

            res[(Ellipsis, i_g) + muN_slice ] += sgn * x
    
    if not keep_nan : res = np.nan_to_num(res)

    return res / 2**n_deriv

def calc_op_qbarq_v2(func_get_bb_v2, op, ir_list, q3ext,
                  latsize, tdir, bb_dim=4, keep_nan=False) :
    """
        ir_list  [(irrep_name, renorm_factor), ... ]
        return [i_data, t, i_comp]
        keep_nan    mark undefined elements (ususally on border in t) with 'nan'
                    otherwise, these elements are replaced with zeros (default)
    """
    res = None
    # no derivative ops
    if   'tensor0' == op :
        assert (1==len(ir_list) and 'H4_T0_d1r1' == ir_list[0][0])
        res = (calc_qbarDq_v2(func_get_bb_v2, [ 0 ], 0, q3ext,
                            latsize, tdir, bb_dim=bb_dim, keep_nan=True)[..., 0]
                * ir_list[0][1])
    elif 'pstensor0' == op :
        assert (1==len(ir_list) and 'H4_T0_d1r1' == ir_list[0][0])
        res = (calc_qbarDq_v2(func_get_bb_v2, [ 15 ], 0, q3ext,
                            latsize, tdir, bb_dim=bb_dim, keep_nan=True)[..., 0]
                * ir_list[0][1])
    elif 'tensor1' == op :
        assert (1==len(ir_list) and 'H4_T1_d4r1' == ir_list[0][0])
        res = (calc_qbarDq_v2(func_get_bb_v2, [ 1, 2, 4, 8 ], 0, q3ext, 
                            latsize, tdir, bb_dim=bb_dim, keep_nan=True)
                * ir_list[0][1])
    elif 'pstensor1' == op :
        assert (1==len(ir_list) and 'H4_T1_d4r1' == ir_list[0][0])
        res = (calc_qbarDq_v2(func_get_bb_v2, [ 14, 13, 11, 7 ], 0, q3ext, 
                            latsize, tdir, bb_dim=bb_dim, keep_nan=True)
                * [ 1, -1, 1, -1 ] * ir_list[0][1])
        
    # TODO add tensor charge
    else :
        
        # 1 & 2 deriv operators
        if   op in ['tensor2s2', 'pstensor2s2' ] : n_deriv = 1
        elif op in ['tensor3s3', 'pstensor3s3' ] : n_deriv = 2
        else : raise ValueError(op)

        if   op in ['tensor2s2', 'tensor3s3' ] :    gamma_list = [1, 2, 4, 8]
        elif op in ['pstensor2s2', 'pstensor3s3' ] :gamma_list = [14, 13, 11, 7 ]
        else : raise ValueError(op)
        # TODO add d1/d2

        y = calc_qbarDq_v2(func_get_bb_v2, gamma_list, n_deriv, q3ext,
                        latsize, tdir, bb_dim=bb_dim, keep_nan=True)
        if op in ['pstensor2s2', 'pstensor3s3' ] :
            np_at(y, (-n_deriv-1, 1))[...] *= -1
            np_at(y, (-n_deriv-1, 3))[...] *= -1

        n_comp  = ir_list_comp_len(ir_list)
        y_dim   = len(y.shape)
        y_dim_d = y_dim - n_deriv - 1 # non-Lorentz indices (i_data, t, etc)
        res     = np.empty(y.shape[:y_dim_d] + (n_comp,), y.dtype)

        # transpose order 
        tr1     = np.r_[y_dim_d : y_dim, : y_dim_d]
        tr2     = np.r_[1 : y_dim_d+1, 0]
        
        sh_comp = 0
        for i, (ir_name, ir_scale) in enumerate(ir_list) :
            ir_dim = H4_repr_dim[ir_name]
            res[..., sh_comp : sh_comp + ir_dim] = (H4_repr_func[ir_name](
                            y.transpose(tr1)).transpose(tr2) * ir_scale)
            sh_comp += ir_dim
        assert(sh_comp == n_comp)

        # set border in t to zeros (although not all of the components are undef)
        for dt in range(n_deriv) :
            np_at(res, (1, dt))[...] = 0.
            np_at(res, (1, -dt-1))[...] = 0.


    if keep_nan : return res
    else : return np.nan_to_num(res)



# compute op's and store in an HDF5 file
def calc_save_op(h5_file, h5_kpath, 
                latsize, 
                h5_bb,
                p3snk, tsep, op, ir_name ) :
    """
        h5_file     open HDF5 h5py file
        h5_kpath    kpath relative to root
    """
    p3snk = np.asarray(p3snk)
    assert (p3snk == h5_bb.attrs['sink_mom']).all()
    assert (tsep == h5_bb.attrs['source_sink_dt'])

    n_data  = h5_bb.shape[0]
    n_qext  = h5_bb.shape[-2]
    n_tau   = h5_bb.shape[-1]
    
    n_comp = H4_repr_dim[ir_name]
    op_shape = (n_data, n_comp, n_qext, n_tau)
    # [i_data, i_comp, i_qext, i_tau]
    h5_op = h5_file.require_dataset(h5_kpath, op_shape, np.complex128, 
                    fletcher32=True)
    
    for i_qext, q3ext in enumerate(h5_bb.attrs['qext_list']) :
        p3src = p3snk - q3ext
        def func_get_bb_v2(gamma, lpath_str) :
            i_l = np_find_first(lpath_str, h5_bb.attrs['linkpath_list'])[0]
            i_g = np_find_first(gamma, h5_bb.attrs['gamma_list'])[0]     
            i_q = np_find_first(q3ext, h5_bb.attrs['qext_list'])[0]
            return h5_bb[:, i_g, i_l, i_q]

        # TODO cleanup: use universal functions (esp.>=1 link ops.) to keep it DRY
        # TODO add tensor0, pstensor0, sigma2a2
        if 'tensor0' == op : 
            assert('H4_T0_d1r1' == ir_name)
            h5_op[:, 0, i_qext, :] =  func_get_bb_v2( 0, 'l0_')
        elif 'pstensor0' == op : 
            assert('H4_T0_d1r1' == ir_name)
            h5_op[:, 0, i_qext, :] =  func_get_bb_v2(15, 'l0_')
        elif 'tensor1' == op : 
            assert('H4_T1_d4r1' == ir_name)
            h5_op[:, 0, i_qext, :] =  func_get_bb_v2( 1, 'l0_')
            h5_op[:, 1, i_qext, :] =  func_get_bb_v2( 2, 'l0_')
            h5_op[:, 2, i_qext, :] =  func_get_bb_v2( 4, 'l0_')
            h5_op[:, 3, i_qext, :] =  func_get_bb_v2( 8, 'l0_')
        elif 'pstensor1' == op :
            assert('H4_T1_d4r1' == ir_name)
            h5_op[:, 0, i_qext, :] =  func_get_bb_v2(14, 'l0_')
            h5_op[:, 1, i_qext, :] = -func_get_bb_v2(13, 'l0_')
            h5_op[:, 2, i_qext, :] =  func_get_bb_v2(11, 'l0_')
            h5_op[:, 3, i_qext, :] = -func_get_bb_v2( 7, 'l0_')
        elif 'sigma2a2' == op :
            assert('H4_T2_d6r1' == ir_name)     # antisymmetric
            h5_op[:, 0, i_qext, :] =  1j*func_get_bb_v2( 3, 'l0_')
            h5_op[:, 1, i_qext, :] =  1j*func_get_bb_v2( 5, 'l0_')
            h5_op[:, 2, i_qext, :] =  1j*func_get_bb_v2( 9, 'l0_')
            h5_op[:, 3, i_qext, :] =  1j*func_get_bb_v2( 6, 'l0_')
            h5_op[:, 4, i_qext, :] =  1j*func_get_bb_v2(10, 'l0_')
            h5_op[:, 5, i_qext, :] =  1j*func_get_bb_v2(12, 'l0_')
        elif op in [ 'tensor2s2', 'pstensor2s2',
                     'tensor3s3', 'pstensor3s3' ] :
            h5_op[:,:,i_qext,:] = calc_op_qbarq_v2(func_get_bb_v2, op, [(ir_name, 1.)],
                        q3ext, latsize, t_axis).transpose((0,2,1))
        else : raise NotImplemented((op, ir_name))

    lhpd.h5_io.h5_copy_attr(h5_op, h5_bb, [
            'sink_mom', 'tau_list', 'qext_list', 'source_sink_dt', 
            'source_hadron', 'sink_hadron', 'src_boost_mom', 'snk_boost_mom'],
            check_key=True)
    lhpd.h5_io.h5_set_datalist(h5_op, lhpd.h5_io.h5_get_datalist(h5_bb))
    h5_op.attrs['dim_spec'] = np.array(['i_data', 'i_comp', 'i_qext', 'i_tau' ],
                                       dtype='S32')
    h5_op.attrs['comp_list'] = H4_repr_comp[ir_name]


###############################################################################
# OP-DISC 
###############################################################################

"""
# 
h5f=h5py.File('tmp.opdisc.h5', 'a')
c2_h5k='/cfg1000/hadspec/SS/proton'
c2_ex_h5f=h5py.File('hspec/hspec.1000.ex.proton.GN2x50_bxp10_bxm10.h5', 'r')
c2_sl_h5f=h5py.File('hspec/hspec.1000.sl.proton.GN2x50_bxp10_bxm10.h5', 'r')
bbql_h5f=h5py.File('../data.qloop/bb-qloop/bb-qloop.1000.h5', 'r')
calc_save_bin_unbias_opdisc(h5f, '/cfg1000/sl', h5f, '/cfg1000/ub', latsize, c2_sl_h5f[c2_h5k], c2_ex_h5f[c2_h5k], bbql_h5f, [0,0,0], c3pt_qextrel_list, 16, 16, 'tensor1', 'H4_T1_d4r1')
"""
def calc_save_unbias_opdisc(
                h5file_sl, h5key_sl,             # output
                h5file_ub, h5key_ub,
                latsize,
                c2pt_h5d_sl, c2pt_h5d_ex, # need to bin&unbias right away because have many more index values
                bbqloop_h5g,
                psnkrel, qextrel_list,      # 3d, rel. to c2pt boosts
                tsep_max, tau_max,          # set both to hslab_len-1
                op, ir_name, 
                t_dir=3,
                h5_overwrite=True,
                attrs_kw={}) :
    """
        in : c2pt, bb-qloop
        out: op, binned over c2pt samples
    """
    assert(3 == t_dir)
    assert(4 == len(latsize))
    lt          = latsize[t_dir]
    latsize_x   = latsize[:t_dir]
    n_src       = c2pt_h5d_sl.shape[0]
    n_tpol      = c2pt_h5d_sl.shape[1]
    tlen_c2     = c2pt_h5d_sl.shape[-1]
    assert((np.r_[:tlen_c2] == c2pt_h5d_sl.attrs['t_list']).all())    # now can work only with contiguous tsnk
    n_tsep      = tsep_max + 1
    n_tau       = tau_max + 1
    n_comp      = H4_repr_dim[ir_name]
    n_qext      = len(qextrel_list)

    def get_bb(gamma, lp_str, q_list, tsrc, t_len) :
        """ t_len = how many timeslices to read, counting from tsrc from  
            return [i_q, t]
        """
        n_q     = len(q_list)
        res     = np.empty((n_q, t_len), dtype=np.complex128)
        if tsrc + t_len <= lt :
            bbql_h5d = bbqloop_h5g["%s/g%d" % (lp_str, gamma)]
            for i_q, q in enumerate(q_list) :
                res[i_q] = bbql_h5d[tuple(q)][tsrc : tsrc + t_len]
        else :
            t1  = lt - tsrc
            res[:, : t1 ]   = get_bb(gamma, lp_str, q_list, tsrc, t1)
            res[:, t1 : ]   = get_bb(gamma, lp_str, q_list, 0, t_len - t1)
        return res

    def get_op_qloop(op, q_list, tsrc, t_len) :
        """ return [i_q, i_comp, t] """
        q_list  = np.asarray(q_list) % latsize_x
        n_q     = len(q_list)
        # TODO cleanup: use universal functions (esp.>=1 link ops.) to keep it DRY
        if   'tensor0' == op :      # TODO for scalar density, also need the usual vacuum avg; that would be q=0
            assert('H4_T0_d1r1' == ir_name)
            res = get_bb( 0, 'l0_', q_list, tsrc, t_len).reshape(1, n_q, t_len)
        elif 'pstensor0' == op : 
            assert('H4_T0_d1r1' == ir_name)
            res = get_bb(15, 'l0_', q_list, tsrc, t_len).reshape(1, n_q, t_len)
        elif 'tensor1' == op : 
            assert('H4_T1_d4r1' == ir_name)
            res     = np.empty((4, n_q, t_len), dtype=np.complex128)
            res[0]  =  get_bb( 1, 'l0_', q_list, tsrc, t_len)
            res[1]  =  get_bb( 2, 'l0_', q_list, tsrc, t_len)
            res[2]  =  get_bb( 4, 'l0_', q_list, tsrc, t_len)
            res[3]  =  get_bb( 8, 'l0_', q_list, tsrc, t_len)
        elif 'pstensor1' == op :
            assert('H4_T1_d4r1' == ir_name)
            res     = np.empty((4, n_q, t_len), dtype=np.complex128)
            res[0]  =  get_bb(14, 'l0_', q_list, tsrc, t_len)
            res[1]  = -get_bb(13, 'l0_', q_list, tsrc, t_len)
            res[2]  =  get_bb(11, 'l0_', q_list, tsrc, t_len)
            res[3]  = -get_bb( 7, 'l0_', q_list, tsrc, t_len)
        elif 'sigma2a2' == op :
            assert('H4_T2_d6r1' == ir_name)     # antisymmetric
            res     =  np.empty((6, n_q, t_len), dtype=np.complex128)
            res[0]  =  1j*get_bb( 3, 'l0_', q_list, tsrc, t_len)   # xy
            res[1]  =  1j*get_bb( 5, 'l0_', q_list, tsrc, t_len)   # xz
            res[2]  =  1j*get_bb( 9, 'l0_', q_list, tsrc, t_len)   # xt
            res[3]  =  1j*get_bb( 6, 'l0_', q_list, tsrc, t_len)   # yz
            res[4]  =  1j*get_bb(10, 'l0_', q_list, tsrc, t_len)   # yt
            res[5]  =  1j*get_bb(12, 'l0_', q_list, tsrc, t_len)   # zt
        else : raise NotImplemented((op, ir_name))
        return (-res) # sic!  the Grassmann sign is applied here

    def get_c2pt(h5d, psnkrel) :
        """ return [i_src, i_tpol, t] """
        i_psnk  = lhpd.np_find_first(psnkrel, h5d.attrs['psnk_list'])[0]
        # [i_src, i_tpol, t]
        return h5d[:, :, i_psnk, :n_tsep]   


    # get c2pt DATALISTs, csrc lists
    assert(lhpd.h5_io.h5dset_attr_equal(c2pt_h5d_sl, c2pt_h5d_ex))
    c2pt_dl_sl  = lhpd.h5_io.h5_get_datalist(c2pt_h5d_sl)
    assert(0 < len(c2pt_dl_sl))
    c2pt_dl_ex  = lhpd.h5_io.h5_get_datalist(c2pt_h5d_ex)
    assert(0 < len(c2pt_dl_ex))
    csrc_list_sl= np.array([ m['source_coord'] for m in c2pt_dl_sl ])
    assert(len(csrc_list_sl) == n_src)

    # check that the config is the same, build map of equal tsrc: [tsrc]=[ csrc, ... ]
    cfgkey      = c2pt_dl_sl[0]['ckpoint_id']
    isrc_tmap   = {}
    for i_src, m in enumerate(c2pt_dl_sl) :
        assert(cfgkey == m['ckpoint_id'])
        tsrc        = csrc_list_sl[i_src][t_dir]
        isrc_tmap.setdefault(tsrc, []).append(i_src)
    for i_src, m in enumerate(c2pt_dl_ex) :
        assert(cfgkey == m['ckpoint_id'])

    # read c2pt, unbias
    # [i_src, i_tpol, tsep]
    psnkrel     = np.asarray(psnkrel)
    c2pt_sl     = get_c2pt(c2pt_h5d_sl, psnkrel)
    c2pt_ex     = get_c2pt(c2pt_h5d_ex, psnkrel)
    m_sl, m_ex  = lhpd.np_match(c2pt_dl_sl, c2pt_dl_ex, hashfunc=lhpd.h5_io.h5_meas_hash)
    c2pt_ub     = c2pt_sl + (c2pt_ex[m_ex] - c2pt_sl[m_sl]).mean(0)

    # [i_q, mu]
    qextrel_list= np.asarray(qextrel_list)
    qext_list   = qextrel_list + c2pt_h5d_sl.attrs['snk_boost_mom'] - c2pt_h5d_sl.attrs['src_boost_mom']
    
    # sum_{tsrc} [ op(tsrc) * sum_{xsrc} [ c2pt{xsrc, tsrc} * exp(-I*xsrc*qext) ] ]
    opdisc_sh   = (n_tpol, n_tsep, n_comp, n_qext, n_tau)
    opdisc_sl   = np.zeros(opdisc_sh, dtype=np.complex128)
    opdisc_ub   = np.zeros(opdisc_sh, dtype=np.complex128)
    for tsrc, isrc_list in iteritems(isrc_tmap) :
        assert((csrc_list_sl[isrc_list][:, t_dir] == tsrc).all())
        print("# tsrc=%d, n_src_tslice=%d" % (tsrc, len(isrc_list)))
        # use actual qext, neg.components wrapped
        # [i_comp, i_qext, tau]
        op_ql   = get_op_qloop(op, qext_list, tsrc, n_tau) # FIXME may be better to read full lt right away?

        # [i_src_tslice, mu]
        x2l_list    = np.asarray(csrc_list_sl[isrc_list, :t_dir], np.float64) / latsize_x
        # [i_src_tslice, i_qext]
        expi_qx     = np.exp(-2j*math.pi * (x2l_list[:,None] * qext_list[None,:]).sum(-1))
        # [i_tpol, tsep, i_qext]
        c2pt_avg_sl = (c2pt_sl[isrc_list, :, :, None] * expi_qx[ :, None, None, : ]).sum(0)
        c2pt_avg_ub = (c2pt_ub[isrc_list, :, :, None] * expi_qx[ :, None, None, : ]).sum(0)
        
        # [i_tpol, tsep, i_comp, i_qext, tau]
        opdisc_sl += c2pt_avg_sl[:, :, None, :, None] * op_ql[None, None, :, :, :]
        opdisc_ub += c2pt_avg_ub[:, :, None, :, None] * op_ql[None, None, :, :, :]

    def save_opdisc(h5g, h5kpath, opdisc, h5d_c2) :
        """ 
            h5d_c2      copy necessary attrs from here
        """
        if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5kpath])
        h5d_out = h5g.require_dataset(h5kpath, opdisc_sh, np.complex128,
                            fletcher32=True)
        h5d_out[:] = opdisc
        lhpd.h5_io.h5_copy_attr(h5d_out, h5d_c2, 
                [ 'tpol_list', 'source_hadron', 'sink_hadron', 'src_boost_mom', 'snk_boost_mom' ], 
                check_key=True) 
        h5d_out.attrs['dim_spec']   = ['i_tpol', 'i_tsep', 'i_comp', 'i_qext', 'i_tau']
        h5d_out.attrs['sink_mom']   = psnkrel
        h5d_out.attrs['qext_list']  = qextrel_list
        h5d_out.attrs['tsep_list']  = np.r_[ : n_tsep]
        h5d_out.attrs['tau_list']   = np.r_[ : n_tau]
        h5d_out.attrs['comp_list']  = H4_repr_comp[ir_name]

        for k, v in iteritems(attrs_kw) :
            h.attrs[k] = v

        h5d_out.file.flush()

    # save opdisc_sl, opdisc_ub
    save_opdisc(h5file_sl, h5key_sl, opdisc_sl, c2pt_h5d_sl)
    save_opdisc(h5file_ub, h5key_ub, opdisc_ub, c2pt_h5d_sl)



# TODO MOVE TO LIB (h5_io)
class h5f_cache :
    def __init__(self, maxfiles, mode) :
        self.maxfiles   = maxfiles
        self.mode       = mode
        self.h5list     = []
        self.h5map      = {}
    def open(self, h5file) :
        if h5file in self.h5map : 
            # maintain lru list (first is least recently used)
            assert(1 == self.h5list.count(h5file))
            self.h5list.remove(h5file)
            self.h5list.append(h5file)
            return h5map[h5file]
        else : 
            assert(0 == self.h5list.count(h5file))
            while self.maxfiles <= len(self.h5list) :
                f = self.h5list.pop(0)
                self.h5map[f].close()
            self.h5map[h5file] = h5py.File(h5file, mode)
            self.h5list.append(h5file)
            return self.h5map[h5file]
    def close(self, h5file) :
        self.h5map[h5file].close()
        self.h5list.remove(h5file)
    def closeall(self):
        for f,v in iteritems(self.h5map) :
            v.close()
            self.h5list.remove(f)
        assert(0 == len(self.h5list))


###############################################################################
# plot stuff
###############################################################################
def plot_eeff(gg, tr, p3snk, tpol_tag, 
        rsplan=('jk',1), **kw) :
    ax      = kw.get("ax")      or dv.make_std_axes()
    xsh     = kw.get("xsh") or kw.get("xshift") or 0   # example of multiple names for the same param
    label   = kw.get("label")   or "%s %s" % (tpol_tag, str(p3snk))
    stg     = kw.get("stg")     or dv.style_group_None()

    c2      = gg.get_twopt_symm(p3snk, tpol_tag).real
    c2_r    = lhpd.resample(c2, rsplan)
    meff_r  = np.log(c2_r[:,tr] / c2_r[:,1+tr])
    meff_ae = lhpd.calc_avg_err(meff_r, rsplan)
    ax.errorbar(xsh + tr, meff_ae[0], yerr=meff_ae[1], 
                label=label, **stg.edotline())
    return ax

def plot_eeff_list(gg, tr, p3snk_list, tpol_tag_list, **kw) :
    ax      = kw.get("ax")      or dv.make_std_axes()
    stg_list= kw.get("stg_list")or it.cycle(dv.style_group_default)
    n_plot  = len(p3snk_list) * len(tpol_tag_list)
    xsh     = .6 / max(1, n_plot - 1)
    xsh     = kw.get("xsh") or kw.get("xshift") or xsh

    purge_keys(kw, ["xsh", "label", "ax"])
    for p3snk, tpol_tag in it.product(p3snk_list, tpol_tag_list) :
        plot_eeff(gg, tr, p3snk, tpol_tag, ax=ax, xsh=xsh, stg=next(stg_list), **kw)
    ax.legend()
    purge_keys(kw, ["ax"])
    dv.set_std_axes(ax, **kw)
    return ax

def plot_delta_eeff(gg, tr, p3snk, tpol_tag, p3snk0, tpol_tag0,
        rsplan=('jk',1), **kw) :
    ax      = kw.get("ax")      or dv.make_std_axes()
    xsh     = kw.get("xsh") or kw.get("xshift") or 0   # example of multiple names for the same param
    label   = kw.get("label")   or "%s %s" % (tpol_tag, str(p3snk))
    stg     = kw.get("stg")     or dv.style_group_None()

    c2      = gg.get_twopt_symm(p3snk, tpol_tag).real
    c2_r    = lhpd.resample(c2, rsplan)
    c2_0    = gg.get_twopt_symm(p3snk0, tpol_tag0).real
    c2_0_r  = lhpd.resample(c2_0, rsplan)
    meff_r  = np.log(  c2_r[:,tr] / c2_r[:,1+tr] 
                    / (c2_0_r[:,tr] / c2_0_r[:,1+tr]) )
    meff_ae = lhpd.calc_avg_err(meff_r, rsplan)
    ax.errorbar(xsh + tr, meff_ae[0], yerr=meff_ae[1], 
                label=label, **stg.edotline())
    return ax
def plot_delta_eeff_list(gg, tr, p3snk_list, tpol_tag_list, 
        p3snk0, tpol_tag0,
        **kw) :
    ax      = kw.get("ax")      or dv.make_std_axes()
    stg_list= kw.get("stg_list")or it.cycle(dv.style_group_default)
    n_plot  = len(p3snk_list) * len(tpol_tag_list)
    xsh     = .6 / max(1, n_plot - 1)
    xsh     = kw.get("xsh") or kw.get("xshift") or xsh


    purge_keys(kw, ["xsh", "label", "ax"])
    for p3snk, tpol_tag in it.product(p3snk_list, tpol_tag_list) :
        plot_delta_eeff(gg, tr, p3snk, tpol_tag, p3snk0, tpol_tag0,
                    ax=ax, xsh=xsh, stg=next(stg_list), **kw)

    ax.legend()
    purge_keys(kw, ["ax"])
    dv.set_std_axes(ax, **kw)
    return ax



###############################################################################
# general plotting functions for c3pt and c3pt/c2pt plateaus
# remove the (T,tau)-dependence of the data
#   mc = (p3src, p3snk) OR
#   mc = (p3src, p3snk, p3src_c2, p3src_c2) in case c2 with some p3 are N/A
def plotpltx_c3(gg, yval_type, 
            tsep, op, ir_name, i_comp, i_reim, mc, tpol_tag, tpol_c2, 
            **kw) :
    """ plot one item """
    mlat    = kw['mlat']
    latsize = kw['latsize']
    rsplan  = kw['rsplan']
    #ipdb.set_trace()
    ax      = kw.get("ax")      or dv.make_std_axes()
    xsh     = kw.get("xsh") or kw.get("xshift") or 0   # example of multiple names for the same param
    label   = kw.get("label")   or "%s %s" % (tpol_tag, str(p3snk))
    stg     = kw.get("stg")     or dv.style_group_None()

    if   (2 == len(mc)) : p3src, p3snk, p3src_c2, p3snk_c2 = tuple(mc) + tuple(mc)
    elif (4 == len(mc)) : p3src, p3snk, p3src_c2, p3snk_c2 = tuple(mc)
    else : raise ValueError(mc)
    esrc    = lhpd.latcorr.lorentz_energy(mlat, latsize, p3src)
    esnk    = lhpd.latcorr.lorentz_energy(mlat, latsize, p3snk)

    c3_cplx = gg.get_threept(tsep, op, ir_name, p3src, p3snk, tpol_tag)
    if   0 == i_reim : c3 = c3_cplx[:,i_comp].real
    elif 1 == i_reim : c3 = c3_cplx[:,i_comp].imag
    else : raise ValueError
    c3_r    = lhpd.resample(c3, rsplan)
    
    tr      = np.r_[: tsep + 1]     # TODO this too can be adjusted
    # TODO make flexible the t-dep of denominator model ?
    if   "c3pt" == yval_type :
        t_op_exp= np.exp(-esrc * tr -esnk*(tsep - tr))
        y_r     = c3_r / t_op_exp
    elif "c3d2i" == yval_type :
        t_op_exp= np.exp(-(esnk - esrc) * (tsep - tr))
        c2_src_r= lhpd.resample(gg.get_twopt_symm(p3src_c2, tpol_c2).real, rsplan)
        y_r     = c3_r / c2_src_r[:, tsep : tsep+1] / t_op_exp
    elif "c3d2f" == yval_type :
        t_op_exp= np.exp( (esnk - esrc) * tr)
        c2_snk_r= lhpd.resample(gg.get_twopt_symm(p3snk_c2, tpol_c2).real, rsplan)
        y_r     = c3_r / c2_snk_r[:, tsep : tsep+1] / t_op_exp
    elif "c3d2symm" == yval_type :
        c2_src_r= lhpd.resample(gg.get_twopt_symm(p3src_c2, tpol_c2).real, rsplan)
        c2_snk_r= lhpd.resample(gg.get_twopt_symm(p3snk_c2, tpol_c2).real, rsplan)
        t_op_exp= np.exp(-(esnk - esrc) * (tsep / 2. - tr))
        y_r  = c3_r / np.sqrt(c2_src_r[:, tsep : tsep+1] * c2_snk_r[:, tsep : tsep+1]) / t_op_exp
    elif "c3d2full" == yval_type :
        t_op_exp= 1.
        c2_src_r= lhpd.resample(gg.get_twopt_symm(p3src_c2, tpol_c2).real, rsplan)
        c2_snk_r= lhpd.resample(gg.get_twopt_symm(p3snk_c2, tpol_c2).real, rsplan)
        c2_f2i_r= c2_snk_r / c2_src_r
        y_r  = c3_r / c2_snk_r[:, tsep : tsep+1] * np.sqrt(
                      c2_f2i_r[:, tsep : tsep+1] 
                    * c2_f2i_r[:, tr] / c2_f2i_r[:, tsep - tr]) / t_op_exp
    else : raise ValueError(yval_type)
    
    y_ae = lhpd.calc_avg_err(y_r, rsplan)

    ax.errorbar(xsh + tr, y_ae[0], yerr=y_ae[1], 
                label=label, **stg.edotline())
    return ax

#psnk_c2=[3,0,0]
#ax=plotpltx_c3_list_Itsep(gg, np.r_[8:13], 'tensor1', 'H4_T1_d4r1', 1, 0, [3,0,0],psnk_c2, 'AApSzplus', psnk, 'AApSzplus', .47, latsize)
def plotpltx_c3_list_Itsep(gg, yval_type, 
        tsep_list, op, ir_name, i_comp, i_reim, mc, tpol_tag, tpol_c2,
        **kw) :
    """ iterate over tsep """
    #ipdb.set_trace()
    ax      = kw.get("ax")      or dv.make_std_axes()
    stg_list= kw.get("stg_list")or it.cycle(dv.style_group_default)
    n_plot  = len(tsep_list)
    xsh     = .6 / max(1., n_plot - 1)
    xsh     = kw.get("xsh") or kw.get("xshift") or xsh

    purge_keys(kw, ["xsh", "label", "ax"])
    for i_tsep, tsep in enumerate(tsep_list) :
        #print(tsep)
        plotpltx_c3(gg, yval_type, 
                    tsep, op, ir_name, i_comp, i_reim, mc, tpol_tag, tpol_c2,
                    label="T=%d" % tsep, xsh=i_tsep*xsh, 
                    ax=ax, stg=next(stg_list), **kw)
    
    xlim = ax.get_xlim()
    ax.plot(xlim, [0,0], 'k:')

    purge_keys(kw, ["ax"])
    dv.set_std_axes(ax, **kw)
    ax.legend()
    return ax

#mc_list=[([k,0,0],[-3,0,0], [k,0,0],[3,0,0]) for k in np.r_[-3:5]]
#gg=calc_ff_case_get('unbias', 'proton', 'U+D', 'GN2x50', 'bxp10', 'bxm10')
#fig=plotpltx_c3_chart_Itsep_Mmc(gg, "c3d2full", np.r_[8:11], 'tensor1', 'H4_T1_d4r1', 3, 0, mc_list, 'AApSzplus', 'AApSzplus', mlat=.47, latsize=latsize)
def plotpltx_c3_chart_Itsep_Mmc(gg, yval_type,
        tsep_list, op, ir_name, i_comp, i_reim, mc_list, tpol_tag, tpol_c2,
        **kw) : 
    """ iterate over mc_list """
    #ipdb.set_trace()
    yl_default = {
        "c3pt"      : r'$M_0^{-1}(T,\tau) C_3(T,\tau)$',
        "c3d2i"     : r'$M_0^{-1}(T,\tau)\frac{C_3(T,\tau)}{C_2^i(T)}$',
        "c3d2f"     : r'$M_0^{-1}(T,\tau)\frac{C_3(T,\tau)}{C_2^f(T)}$',
        "c3d2symm"  : r'$M_0^{-1}(T,\tau)\frac{C_3(T,\tau)}{\sqrt{C_2^i(T) C_2^f(T)}}$',
        "c3d2full"  : r'$\frac{C_3(T,\tau)}{C_2^f(T)}\sqrt{\frac{C_2^f(\tau) C_2^i(T-\tau)}{C_2^i(\tau)C_2^f(T-\tau)}}$',
    }
    mlat    = kw["mlat"]
    latsize = kw["latsize"]
    n_ax    = len(mc_list)
    ax_ncol = kw.get("ax_ncol") or 2
    ax_nrow = (n_ax + ax_ncol - 1) // ax_ncol
    panelx  = kw.get("panelx") or 6.0
    panely  = kw.get("panely") or 4.5
    figsize = kw.get("figsize")or [panelx * ax_ncol, panely*ax_nrow]
    fig     = plt.figure(figsize=figsize)
    xl      = kw.get("xlabel") or kw.get("xl") or r'$\tau$'
    yl      = kw.get("ylabel") or kw.get("yl") or yl_default.get(yval_type, None)
    closex  = kw.get("closex") or False
    closey  = kw.get("closey") or False
    adj_xlim= kw.get("adj_xlim") or False
    adj_ylim= kw.get("adj_ylim") or False
    ax_list = dv.make_yxsubplot(ax_ncol, ax_nrow, fig=fig, 
            closex=closex, xlabel=xl, 
            closey=closey, ylabel=yl)
    purge_keys(kw, ["figsize", "ax_ncol"])
    for i_ax, mc in enumerate(mc_list) :
        ax=ax_list[i_ax]
        plotpltx_c3_list_Itsep(gg, yval_type, 
                tsep_list, op, ir_name, i_comp, i_reim, mc, tpol_tag, tpol_c2,
                ax=ax, **kw)
        # TODO make std function for mc_str ; XXX q2 depends on mlat, latsize
        p3src, p3snk = mc[:2]
        q2 = lhpd.latcorr.lorentz_mom_transfer_sq(mlat, latsize, p3src, p3snk)
        ax.text(.5, 0.1, r'$\langle%s|%s\rangle$ ($Q^2=%.2f$)' % (str(p3snk), str(p3src), q2), 
                size='x-small', ha='center', va='bottom', transform=ax.transAxes)

    if adj_xlim : dv.adjust_multiplot_xlim(ax_list[:n_ax])
    if adj_ylim : dv.adjust_multiplot_ylim(ax_list[:n_ax])
    return fig

#flav_list=['U', 'D', 'U-D', 'U+D', 'P', 'N']
#mc_list=[([k,0,0],[0,-3,0], [k,0,0],[3,0,0]) for k in np.r_[-3:5]]
#runme_plotpltx_c3_chart_Itsep_Mmc('ub','proton', 'GN2x50','bxp10','bxm10', "c3d2f", flav_list, np.r_[8:11], ['tensor1'], mc_list, 'AApSzplus', 'AApSzplus', mlat=0.47, latsize=latsize, rsplan=('jk',1), out_dir='figs.c3d2f.dt8--10')
def runme_plotpltx_c3_chart_Itsep_Mmc(ama, had, sm,bsrc,bsnk, 
        yval_type,
        flav_list, tsep_list, op_list, mc_list, tpol_tag, tpol_c2,
        **kw) :
    """ iterate over flav, comp, reim 
        (ir_name, ir_comp) list is deduced from op
    """
    out_dir = kw.get("out_dir") or '.'
    rsplan  = kw.get("rsplan")  or ('jk',1)

    purge_keys(kw, ["out_dir", "rsplan"])
    for op, flav in it.product(op_list, flav_list) :
        gg  = calc_ff_case_get(ama, had, flav, sm, bsrc, bsnk)
        for ir_name, ir_scale in lhpd.latcorr.op_default_ir_list(op) :
            for i_comp, i_reim in it.product(
                    range(lhpd.pymath.H4_tensor.H4_repr_dim[ir_name]), [0,1]) :
                fig_name = "%s.%s.%d%s_%s" % (gg.case_str(), 
                            op, i_comp, lhpd.pymath.H4_tensor.H4_repr_comp[ir_name][i_comp],
                            (['r','i'])[i_reim])
                print("%s -> %s/%s" % (yval_type, out_dir, fig_name))
                fig = plotpltx_c3_chart_Itsep_Mmc(gg, yval_type, 
                        tsep_list, op, ir_name, i_comp, i_reim, mc_list, tpol_tag, tpol_c2, 
                        rsplan=rsplan, **kw)
                fig.savefig('%s/%s.pdf' % (out_dir, fig_name))
                dv.close_fig(fig)

###############################################################################
# calc ff 
###############################################################################
calc_ff_tpol_map = {
    'AAp'           : lhpd.latcorr.tpol_map['pos'],
    'AApSz'         : lhpd.latcorr.tpol_map['posSz'],
    'AApSzplus'     : lhpd.latcorr.tpol_map['posSzplus'],
    }
def calc_ff_v1(gg, op, mlat, latsize, 
            tpol_list, mcgrp, tpol_c2_list, psnk_c2_list, ir_list, tsep_list, 
            method_list, 
            rsplan = ('jk', 1)) :
    """ this is a long ugly function to compute formfactors;
        temporary implementation, will be organized into subfunctions as will be seen fit
        (although likely to stay forever)

        method_list = [ (name, param1, ...), ...]
                    methods to extract the ground state

        func_get_op     (ama_mode, had, flav, op, ir_list, p3src, p3snk, tsep) -> c3pt[i_data,tau,i_comp]
        func_get_c2pt   (ama_mode, had, p3) -> c2pt[i_data, t]

        return formfactors[data][i_method][i_ff]
    """
    c3pt_map = {}
    #psnk_list = [ list(x) for x in set([ tuple(mc[1]) for mc in mcgrp ]) ]
    
    n_tpol  = len(tpol_list)
    assert(len(tpol_c2_list) == n_tpol)
    n_mc    = len(mcgrp)
    assert(len(psnk_c2_list) == n_mc)
    n_comp  = ir_list_comp_len(ir_list)
    n_c3pt  = n_tpol * n_mc * n_comp * 2
    tmax    = max(tsep_list) + 1            # FIXME if fit c2pt, may need to plug other value

    def load_c2pt_src_snk():
        n_data = res_src = res_snk = None
        for (i_tpol, tpol), i_mc in it.product(
                enumerate(tpol_c2_list), range(n_mc)) :
            # [i_data, t]
            p3src = mcgrp[i_mc][0]
            p3snk = psnk_c2_list[i_mc]
            x = gg.get_twopt_symm(p3src, tpol)
            y = gg.get_twopt_symm(p3snk, tpol)
            if None is res_src or None is res_snk :
                n_data = x.shape[0]
                assert(y.shape[0] == n_data)
                res_src = np.empty((n_data, tmax, n_tpol, n_mc), np.float64)
                res_snk = np.empty((n_data, tmax, n_tpol, n_mc), np.float64)
            res_src[:, :, i_tpol, i_mc] = x[:, :tmax].real
            res_snk[:, :, i_tpol, i_mc] = y[:, :tmax].real
        return res_src, res_snk
    
    def load_c3pt(tsep) :
        n_data = res = None
        for (i_tpol, tpol), (i_mc, (p3src, p3snk)) in it.product(
                enumerate(tpol_list), enumerate(mcgrp)) :
            ir_sh = 0
            for ir_name, ir_scale in ir_list :
                ir_len  = H4_repr_dim[ir_name]
                # [i_data, tau, i_comp]
                x = gg.get_threept(tsep, op, ir_name, p3src, p3snk, tpol)
                if None is res :
                    n_data = x.shape[0]
                    # [i_data, tau, i_mc, i_comp, i_c]
                    res = np.empty((n_data, 1+tsep, n_tpol, n_mc, n_comp, 2), np.float64)
                res[:, :, i_tpol, i_mc, ir_sh : ir_sh + ir_len, 0] = ir_scale * x.real.transpose(0,2,1)
                res[:, :, i_tpol, i_mc, ir_sh : ir_sh + ir_len, 1] = ir_scale * x.imag.transpose(0,2,1)
                ir_sh += ir_len
            assert(ir_sh == n_comp)

        return res


    def apply_eqn_prec(c2a, c2b, c3ba, pcmat) :
        """ return preconditioned versions of c2, c3 """
        n_eqn   = pcmat.shape[-1]
        res_c2a = np.zeros(c2a.shape[0:2] + (n_eqn,), c2a.dtype)
        res_c2b = np.zeros(c2b.shape[0:2] + (n_eqn,), c2b.dtype)
        res_c3ba= dict([ (tsep, np.zeros(c3ba[tsep].shape[0:2] + (n_eqn,), c3ba[tsep].dtype)) 
                         for tsep in tsep_list ])
        for i_tpol, i_mc, i_comp, i_c in np.ndindex(n_tpol, n_mc, n_comp, 2) :
            for i_eqn in range(n_eqn) :
                f   = pcmat[i_tpol, i_mc, i_comp, i_c, i_eqn]
                if 0. == f : continue
                res_c2a[:, :, i_eqn] += abs(f) * c2a[:, :, i_tpol, i_mc]
                res_c2b[:, :, i_eqn] += abs(f) * c2b[:, :, i_tpol, i_mc]
                for tsep in tsep_list :
                    res_c3ba[tsep][:, :, i_eqn]+= f * c3ba[tsep][:, :, i_tpol, i_mc, i_comp, i_c]
        return res_c2a, res_c2b, res_c3ba


    ### compute the eqmat and pcmat
    # [i_tpol, i_mc, n_comp, 2, i_ff], [i_mc, i_comp, 2, i_eqn]
    eqmat, pcmat    = ff_eqnmat_precond_f2f(op, mlat, latsize, 
                        [ calc_ff_tpol_map[tpol] for tpol in tpol_list ], 
                        mcgrp, ir_list)
    n_ff    = eqmat.shape[-1]
    n_eqn   = pcmat.shape[-1]
    
    ### collect all relevant correlator data
    # [i_data, t, i_mc]
    c2pt_src, c2pt_snk = load_c2pt_src_snk()
    # {tsep -> c3pt[i_data, tau, i_mc, i_comp, i_c]
    c3pt_map    = dict([ (tsep, load_c3pt(tsep))
                         for tsep in tsep_list ])
    ### resample
    c2pt_src_rs = lhpd.resample(c2pt_src, rsplan)
    c2pt_snk_rs = lhpd.resample(c2pt_snk, rsplan)
    c3pt_map_rs = dict([ (tsep, lhpd.resample(c3pt_map[tsep], rsplan))
                         for tsep in tsep_list ])

    
    ### compute preconditioned c3pt, c2pt for combined and app-only
    c2pt_src_rs_pc, c2pt_snk_rs_pc, c3pt_map_rs_pc = apply_eqn_prec(
                c2pt_src_rs, c2pt_snk_rs, c3pt_map_rs, pcmat)

    def calc_me_ratio(c2a, c2b, c3ba, tsep) :
        """ ASSUME time axis=1 """
        sqf = np.sqrt(np.abs(  c2b[:, : tsep+1] / c2a[:, : tsep+1]
                               * c2a[:, tsep::-1] / c2b[:, tsep::-1] ))
        return (c3ba / np.sqrt(np.abs(c2b[:, tsep] * c2a[:, tsep]))[:,None] * sqf)
    def calc_me_ratio_all(c2a, c2b, c3ba_map) : 
        return dict([ (tsep, calc_me_ratio(c2a, c2b, c3ba_map[tsep], tsep))
                      for tsep in c3ba_map.keys() ]) 

    # preconditioner is applied to both 3pt and 2pt, so their ratio should be solved against 
    # equations without extra factors; therefore, normalize the result of applying pc to eqmat
    # (divide each row by the multiplicity over which pc matrix summates
    pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
    eqmat_pc= np.dot(pcmat.reshape(n_c3pt, n_eqn).T, 
                      eqmat.reshape(n_c3pt, n_ff)) / pc_norm[:,None]

    #print_ff_eqnmat_f3f(op, ir_list, mcgrp, eqmat, pcmat)
    # me[tsep][i_data, tau, i_eqn]
    me_rs   = calc_me_ratio_all(c2pt_src_rs_pc, c2pt_snk_rs_pc, c3pt_map_rs_pc)
    n_data  = me_rs[tsep_list[0]].shape[0]

    # XXX XXX XXX debug print
    if False :
        print(pc_norm)
        print(pcmat)
        print(eqmat_pc)
        for tsep in sorted(c3pt_map_rs_pc.keys()) :
            print(tsep)
            print(c3pt_map_rs_pc[tsep].mean(0) / pc_norm)
            print(me_rs[tsep].mean(0))
        print(c2pt_src_rs_pc.mean(0) / pc_norm)
        print(c2pt_snk_rs_pc.mean(0) / pc_norm)

    def calc_ratio_pltx(tsep) :
        """ ff on timeslices
            return ff[n_data, 1+tsep, n_ff] """
        res = np.empty((me_rs[tsep].shape[0], 1+tsep, n_ff), np.float64)
        for tau in range(1+tsep) :
            # solve od: [i_eqn,i_ff] for tau
            cov     = lhpd.calc_cov(me_rs[tsep][:,tau], rsplan)
            cov_inv = lhpd.fitter.cov_invert(cov, rmin=1e-4)
            sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
            res[:,tau,:] = matrix_tensor_dot(sol_mat, me_rs[tsep][:,tau,:], 1)
        return res

    def calc_ratio_pltx_avg(tsep, max_tslice=3) :
        """ ff from ratio pltx center avg
            return ff[n_data, 1, n_ff] """
        assert(tsep in c3pt_map_rs)
        t1, t2 = 0, tsep + 1
        while max_tslice < (t2 - t1) :
            t1 += 1
            t2 -= 1
        # FIXME this is 'regression test'; replace code above with this function
        assert(lhpd.latcorr.pltx_trange(tsep, max_tslice) == (t1, t2))
        
        res     = np.empty((me_rs[tsep].shape[0], 1, n_ff), np.float64)
        # solve od: [i_eqn,i_ff] for tau
        cov     = lhpd.calc_cov(me_rs[tsep][:, t1:t2].mean(axis=1), rsplan)
        cov_inv = lhpd.fitter.cov_invert(cov, rmin=1e-4)
        sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
        me_rs_avg = me_rs[tsep][:, t1:t2, :].mean(axis=1)
        res[:, 0, :] = matrix_tensor_dot(sol_mat, me_rs_avg, 1)
        return res

    def calc_ratio_summ(tsep_list, tskip=2) :
        """ ff from summation-extrapolated matrix elements
            return ff[n_data, 1, n_ff] """
        tsep_list   = np.asarray(tsep_list)
        n_tsep      = len(tsep_list)
        
        me_summ     = np.empty((n_data, n_tsep, n_eqn), np.float64)
        for i_tsep, tsep in enumerate(tsep_list) :
            me_summ[:, i_tsep]      = me_rs[tsep][:, tskip : 1 + tsep - tskip].sum(axis=1)
        
        # solve od : [i_tsep, {coeff, intercept}]
        me_gs       = np.empty((n_data, n_eqn), np.float64)
        eqn_summ = np.r_[ [tsep_list], [[1.] * n_tsep] ].T
        for i_eqn in range(n_eqn) :
            me_summ_cov = lhpd.calc_cov(me_summ[:, :, i_eqn], rsplan)
            cov_inv     = lhpd.fitter.cov_invert(me_summ_cov, rmin=1e-4)
            sol_summ, sol_undef = lhpd.fitter.linsolver_od_matr(eqn_summ, cov_inv)
            me_gs[:, i_eqn]     = matrix_tensor_dot(sol_summ, me_summ[:, :, i_eqn], 1)[:, 0]

        # solve od : [i_eqn, i_ff]
        res         = np.empty((n_data, 1, n_ff), np.float64)
        me_gs_cov   = lhpd.calc_cov(me_gs, rsplan)
        cov_inv     = lhpd.fitter.cov_invert(me_gs_cov, rmin=1e-4)
        sol_ff, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
        res[:, 0, :]= matrix_tensor_dot(sol_ff, me_gs, 1)
        return res
            
    # TODO calc_gpof
    # TODO calc_exp_fit


    # put plateaus:tsep->x, plateau averages:tsep->x, summation
    all_res = []
    for m in method_list :
        if   'ratio_pltx' == m[0] : 
            for tsep in tsep_list :
                all_res.append(calc_ratio_pltx(tsep))
                print(all_res[-1].mean(0))
        elif 'ratio_pltx_avg' == m[0] :
            for tsep in tsep_list :
                all_res.append(calc_ratio_pltx_avg(tsep))
        elif 'ratio_summ' == m[0] :
            all_res.append(calc_ratio_summ(tsep_list))
        else : raise ValueError(m[0])

    return all_res

"""
# examples
# SEE BELOW
"""
def calc_save_ff_v1(h5_file, h5_kpath,
            gg, op, mlat, latsize, 
            tpol_list, mcgrp_list, tpol_c2_list, psnk_c2_list_list, ir_list, tsep_list, 
            method_list,
            rsplan=('jk', 1) ) :
    
    n_q2 = len(mcgrp_list)
    n_data = None
    n_ff = lhpd.latcorr.get_gff_number(op)
    all_ff = []
    q2_list= []
    for i_q2, (mcgrp, psnk_c2_list) in enumerate(zip(mcgrp_list, psnk_c2_list_list)) :
        mc0 = mcgrp[0]
        q2 = lorentz_mom_transfer_sq(mlat, latsize, mc0[0], mc0[1])
        print(mc0, "Q2=", q2)
        q2_list.append(q2)
        all_ff.append(calc_ff_v1(gg, op, mlat, latsize, 
                              tpol_list, mcgrp, tpol_c2_list, psnk_c2_list, ir_list, tsep_list, 
                              method_list,
                              rsplan = rsplan))

    q2_list = np.asarray(q2_list)
    n_data = all_ff[0][0].shape[0]
    assert (all_ff[0][0].shape[-1] == n_ff)

    def save_ff(dset, i) :
        dset.attrs['q2_list'] = q2_list
        for i_q2, q2 in enumerate(q2_list) :
            # [i_data, i_t, i_q2, i_ff]
            dset[:, :, i_q2, :] = all_ff[i_q2][i]

    cnt = 0
    for m in method_list :
        if   'ratio_pltx' == m[0] :
            for tsep in tsep_list :
                ff_shape = (n_data, 1 + tsep, n_q2, n_ff)
                k = '%s/ratio_pltx/dt%d' % (h5_kpath, tsep)
                h5_ff = h5_file.require_dataset(k, ff_shape, np.float64,
                                                fletcher32=True)
                save_ff(h5_ff, cnt)
                cnt += 1
        elif 'ratio_pltx_avg' == m[0] :
            for tsep in tsep_list :
                ff_shape = (n_data, 1, n_q2, n_ff)
                k = '%s/ratio_pltx_avg/dt%d' % (h5_kpath, tsep)
                h5_ff = h5_file.require_dataset(k, ff_shape, np.float64,
                                                fletcher32=True)
                save_ff(h5_ff, cnt)
                cnt += 1
        elif 'ratio_summ' == m[0] :
            ff_shape = (n_data, 1, n_q2, n_ff)
            k = '%s/ratio_summ' % (h5_kpath)
            h5_ff = h5_file.require_dataset(k, ff_shape, np.float64,
                                            fletcher32=True)
            save_ff(h5_ff, cnt)
            cnt += 1
        elif 'fit_c3d2f' == m[0] :
            ff_shape = (n_data, 1, n_q2, n_ff)
            k = '%s/fit_c3d2f' % (h5_kpath)
            h5_ff = h5_file.require_dataset(k, ff_shape, np.float64,
                                            fletcher32=True)
            save_ff(h5_ff, cnt)
            cnt += 1

"""
# xX
pplist=[([k,0,0],[-3,0,0]) for k in np.r_[-3:5]] ; psnk_c2=[3,0,0] ; mcgrp_list_v0=[[pp] for pp in pplist] ; psnk_c2_list=[[psnk_c2]]*len(pplist)
# xY
pplist=[([k,0,0],[0,-3,0]) for k in np.r_[-3:5]] ; psnk_c2=[3,0,0] ; mcgrp_list_v0=[[pp] for pp in pplist] ; psnk_c2_list=[[psnk_c2]]*len(pplist)

mlat=0.47
tpol_list=['AApSzplus']
op='tensor1'
ir_list=lhpd.latcorr.op_default_ir_list(op)
method_list = [('ratio_pltx',), ('ratio_pltx_avg',), ('ratio_summ',)]
tsep_list=np.r_[8:11]

gg=calc_ff_case_get('ub', 'proton', 'U+D', 'GN2x50', 'bxp10', 'bxm10')
h5f=h5py.File('tmp.h5', 'a')
calc_save_ff(h5f, '/ff', gg, op, 0.47, latsize, tpol_list, mcgrp_list, tpol_list, psnk_c2_list, ir_list, np.r_[8:11], method_list)

runme_calc_save_ff('tmp.ff.xY.v0.h5', 'ub', 'proton','GN2x50','bxp10','bym10', flav_list1, [op], tpol_list, mcgrp_list_v0, tpol_list, psnk_c2_list, tsep_list, mlat, latsize)

"""
def runme_calc_save_ff_v1(h5_fname,ama, had, sm,bsrc,bsnk,
        flav_list, op_list,
        tpol_list, mcgrp_list, tpol_c2_list, psnk_c2_list, tsep_list,
        mlat, latsize,
        method_list = [('ratio_pltx',), ('ratio_pltx_avg',), ('ratio_summ',)],
        rsplan=('jk', 1) ) :
    h5f = h5py.File(h5_fname, 'a') # 'append' mode does not work?

    for flav in flav_list :
        for op in op_list :
            ir_list = op_default_ir_list(op)
            gg      = calc_ff_case_get(ama, had, flav, sm, bsrc, bsnk)
            h5k = '/%s/%s/%s' % (had, op, flav)
            calc_save_ff_v1(h5f, h5k, gg, op, mlat, latsize, 
                         tpol_list, mcgrp_list, tpol_c2_list, psnk_c2_list, ir_list, tsep_list,
                         method_list, rsplan=rsplan)

    h5f.close()


"""
# bxp10, bym10
ssgrp=[ srcsnkpair(dt, 'bxp10', 'bym10', 'GN2x50') for dt in np.r_[8:13] ]
mcgrp=[ mc_case([k,0,0],[0,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]
# bxp10, bxm10
ssgrp=[ srcsnkpair(dt, 'bxp10', 'bxm10', 'GN2x50') for dt in np.r_[8:13] ]
mcgrp=[ mc_case([k,0,0],[-3,0,0], 'AApSzplus') for k in np.r_[-3:5] ]

# bqp10, brm10
ssgrp=[ srcsnkpair(dt, 'bqp10', 'brm10', 'GN2x25') for dt in np.r_[6:11] ]
mcgrp=[ mc_case([k,k,0],[3,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]
# bqp10, bqm10
ssgrp=[ srcsnkpair(dt, 'bqp10', 'bqm10', 'GN2x25') for dt in np.r_[6:11] ]
mcgrp=[ mc_case([k,k,0],[-3,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]

method_list1=[('ratio_pltx',), ('ratio_pltx_avg',)]
flav_list1=['U', 'D', 'U-D', 'U+D', 'P', 'N']
runme_calc_save_ff_v2('tmp.ff.xY.v2.h5', [ [mc] for mc in mcgrp], ssgrp, flav_list1, ['tensor1'], method_list1, mlat=0.47, latsize=latsize, rsplan=('jk',1), ama='ub', had='proton', data_top='.')
"""

execfile('scripts-runme/ff/plot_ff') # moved to lib
