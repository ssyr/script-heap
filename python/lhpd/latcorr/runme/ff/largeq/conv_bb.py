#! /usr/bin/env python
from __future__ import print_function
from past.builtins import execfile
import sys, os

execfile('config_c23pt_boost1_xx_xy.py')
execfile('pproc_largeq.py')

for c in sys.argv[1:] : 
    for ama in ['ex', 'sl'] :
        for sm_tag in sm_list :
            print("# %s %s %s " % (c, ama, sm_tag))
            runme_conv_bb2hdf(c, ama, sm_tag, c3pt_ksrc_tag, c3pt_ksrc_q, baryon_bc_t=ferm_bc[3]**3)
    
