from __future__ import print_function
###############################################################################
# general plotting functions for c3pt and c3pt/c2pt plateaus
# remove the (T,tau)-dependence of the data
#   mc = (p3src, p3snk) OR
#   mc = (p3src, p3snk, p3src_c2, p3src_c2) in case c2 with some p3 are N/A
def plotpltx_c3v2(gg, yval_type, 
            mc, ss, op, ir_name, i_comp, i_reim,
            **kw) :
    """ plot one item """
    mlat    = kw['mlat']
    latsize = kw['latsize']
    rsplan  = kw['rsplan']
    #ipdb.set_trace()
    ax      = kw.get("ax")      or dv.make_std_axes()
    xsh     = kw.get("xsh") or kw.get("xshift") or 0   # example of multiple names for the same param
    label   = kw.get("label")   or str(ss)
    stg     = kw.get("stg")     or dv.style_group_None()

    p3src   = mc.get_p3src()
    esrc    = lhpd.latcorr.lorentz_energy(mlat, latsize, p3src)
    p3snk   = mc.get_p3snk()
    esnk    = lhpd.latcorr.lorentz_energy(mlat, latsize, p3snk)

    c3_cplx = gg.get_threept(mc, ss, op, ir_name)
    if   0 == i_reim : c3 = c3_cplx[:,i_comp].real
    elif 1 == i_reim : c3 = c3_cplx[:,i_comp].imag
    else : raise ValueError
    c3_r    = lhpd.resample(c3, rsplan)
    
    tsep    = ss.get_tsep()
    tr      = np.r_[: tsep + 1]     # TODO this too can be adjusted
    # TODO make flexible the t-dep of denominator model ?
    if   "c3pt" == yval_type :
        t_op_exp= np.exp(-esrc * tr -esnk*(tsep - tr))
        y_r     = c3_r / t_op_exp
    elif "c3d2i" == yval_type :
        t_op_exp= np.exp(-(esnk - esrc) * (tsep - tr))
        c2_src_r= lhpd.resample(gg.get_twopt_src(mc, ss).real, rsplan)
        y_r     = c3_r / c2_src_r[:, tsep : tsep+1] / t_op_exp
    elif "c3d2f" == yval_type :
        t_op_exp= np.exp( (esnk - esrc) * tr)
        c2_snk_r= lhpd.resample(gg.get_twopt_snk(mc, ss).real, rsplan)
        y_r     = c3_r / c2_snk_r[:, tsep : tsep+1] / t_op_exp
    elif "c3d2symm" == yval_type :
        c2_src_r= lhpd.resample(gg.get_twopt_src(mc, ss).real, rsplan)
        c2_snk_r= lhpd.resample(gg.get_twopt_snk(mc, ss).real, rsplan)
        t_op_exp= np.exp(-(esnk - esrc) * (tsep / 2. - tr))
        y_r  = c3_r / np.sqrt(c2_src_r[:, tsep : tsep+1] * c2_snk_r[:, tsep : tsep+1]) / t_op_exp
    elif "c3d2full" == yval_type :
        t_op_exp= 1.
        c2_src_r= lhpd.resample(gg.get_twopt_src(mc, ss).real, rsplan)
        c2_snk_r= lhpd.resample(gg.get_twopt_snk(mc, ss).real, rsplan)
        c2_f2i_r= c2_snk_r / c2_src_r
        y_r  = c3_r / c2_snk_r[:, tsep : tsep+1] * np.sqrt(
                      c2_f2i_r[:, tsep : tsep+1] 
                    * c2_f2i_r[:, tr] / c2_f2i_r[:, tsep - tr]) / t_op_exp
    else : raise ValueError(yval_type)
    
    y_ae = lhpd.calc_avg_err(y_r, rsplan)

    ax.errorbar(xsh + tr, y_ae[0], yerr=y_ae[1], 
                label=label, **stg.edotline())
    return ax

#psnk_c2=[3,0,0]
#ax=plotpltx_c3_list_Iss(gg, np.r_[8:13], 'tensor1', 'H4_T1_d4r1', 1, 0, [3,0,0],psnk_c2, 'AApSzplus', psnk, 'AApSzplus', .47, latsize)
def plotpltx_c3v2_list_Iss(gg, yval_type, 
        mc, ssgrp, op, ir_name, i_comp, i_reim, 
        **kw) :
    """ iterate over ss """
    #ipdb.set_trace()
    ax      = kw.get("ax")      or dv.make_std_axes()
    stg_list= kw.get("stg_list")or it.cycle(dv.style_group_default)
    n_plot  = len(ssgrp)
    xsh     = .6 / max(1., n_plot - 1)
    xsh     = kw.get("xsh") or kw.get("xshift") or xsh

    purge_keys(kw, ["xsh", "label", "ax"])
    for i_ss, ss in enumerate(ssgrp) :
        #print(ss)
        plotpltx_c3v2(gg, yval_type, 
                    mc, ss, op, ir_name, i_comp, i_reim, 
                    label="T=%d" % ss.get_tsep(), xsh=i_ss*xsh, 
                    ax=ax, stg=next(stg_list), **kw)
    
    xlim = ax.get_xlim()
    ax.plot(xlim, [0,0], 'k:')

    purge_keys(kw, ["ax"])
    dv.set_std_axes(ax, **kw)
    ax.legend()
    return ax

#mcgrp=[([k,0,0],[-3,0,0], [k,0,0],[3,0,0]) for k in np.r_[-3:5]]
#gg=calc_ff_case_get('unbias', 'proton', 'U+D', 'GN2x50', 'bxp10', 'bxm10')
#fig=plotpltx_c3_chart_Iss_Mmc(gg, "c3d2full", np.r_[8:11], 'tensor1', 'H4_T1_d4r1', 3, 0, mcgrp, 'AApSzplus', 'AApSzplus', mlat=.47, latsize=latsize)
def plotpltx_c3v2_chart_Iss_Mmc(gg, yval_type,
        mcgrp, ssgrp, op, ir_name, i_comp, i_reim, 
        **kw) : 
    """ iterate over mcgrp """
    #ipdb.set_trace()
    yl_default = {
        "c3pt"      : r'$M_0^{-1}(T,\tau) C_3(T,\tau)$',
        "c3d2i"     : r'$M_0^{-1}(T,\tau)\frac{C_3(T,\tau)}{C_2^i(T)}$',
        "c3d2f"     : r'$M_0^{-1}(T,\tau)\frac{C_3(T,\tau)}{C_2^f(T)}$',
        "c3d2symm"  : r'$M_0^{-1}(T,\tau)\frac{C_3(T,\tau)}{\sqrt{C_2^i(T) C_2^f(T)}}$',
        "c3d2full"  : r'$\frac{C_3(T,\tau)}{C_2^f(T)}\sqrt{\frac{C_2^f(T) C_2^f(\tau) C_2^i(T-\tau)}{C_2^i(T) C_2^i(\tau) C_2^f(T-\tau)}}$',
    }
    mlat    = kw["mlat"]
    latsize = kw["latsize"]
    n_ax    = len(mcgrp)
    ax_ncol = kw.get("ax_ncol") or 2
    ax_nrow = (n_ax + ax_ncol - 1) // ax_ncol
    panelx  = kw.get("panelx") or 6.0
    panely  = kw.get("panely") or 4.5
    figsize = kw.get("figsize")or [panelx * ax_ncol, panely*ax_nrow]
    fig     = plt.figure(figsize=figsize)
    xl      = kw.get("xlabel") or kw.get("xl") or r'$\tau$'
    yl      = kw.get("ylabel") or kw.get("yl") or yl_default.get(yval_type, None)
    closex  = kw.get("closex") or False
    closey  = kw.get("closey") or False
    adj_xlim= kw.get("adj_xlim") or False
    adj_ylim= kw.get("adj_ylim") or False
    ax_list = dv.make_yxsubplot(ax_ncol, ax_nrow, fig=fig, 
            closex=closex, xlabel=xl, 
            closey=closey, ylabel=yl)
    purge_keys(kw, ["figsize", "ax_ncol"])
    for i_ax, mc in enumerate(mcgrp) :
        ax=ax_list[i_ax]
        plotpltx_c3v2_list_Iss(gg, yval_type, 
                mc, ssgrp, op, ir_name, i_comp, i_reim,
                ax=ax, **kw)
        # TODO make std function for mc_str ; XXX q2 depends on mlat, latsize
        q2 = lhpd.latcorr.lorentz_mom_transfer_sq(mlat, latsize, 
                    mc.get_p3src(), mc.get_p3snk())
        ax.text(.5, 0.1, r'%s ($Q^2=%.2f$)' % (dv.latexize(str(mc)), q2), 
                size='x-small', ha='center', va='bottom', transform=ax.transAxes)

    if adj_xlim : dv.adjust_multiplot_xlim(ax_list[:n_ax])
    if adj_ylim : dv.adjust_multiplot_ylim(ax_list[:n_ax])
    return fig

"""
flav_list1=['U', 'D', 'U-D', 'U+D', 'P', 'N']
runme_plotpltx_c3_chart_Iss_Mmc('c3d2f', mcgrp1, ssgrp1, flav_list1, ['tensor1'], mlat=0.47, latsize=latsize, rsplan=('jk',1), out_dir='figs.c3d2f.dt8--10')
"""
def runme_plotpltx_c3v2_chart_Iss_Mmc(
        yval_type, mcgrp, ssgrp,
        flav_list, op_list, 
        **kw) :
    """ iterate over flav, comp, reim 
        (ir_name, ir_comp) list is deduced from op
    """
    out_dir = kw.get("out_dir") or '.'
    rsplan  = kw.get("rsplan")  or ('jk',1)

    purge_keys(kw, ["out_dir", "rsplan"])
    for op, flav in it.product(op_list, flav_list) :
        gg  = ens_data_get(flav=flav, op=op, **kw)
        for ir_name, ir_scale in lhpd.latcorr.op_default_ir_list(op) :
            for i_comp, i_reim in it.product(
                    range(lhpd.pymath.H4_tensor.H4_repr_dim[ir_name]), [0,1]) :
                fig_name = "%s.%d%s_%s" % (gg.str_f(), i_comp, 
                            lhpd.pymath.H4_tensor.H4_repr_comp[ir_name][i_comp],
                            (['r','i'])[i_reim])
                print("%s -> %s/%s" % (yval_type, out_dir, fig_name))
                fig = plotpltx_c3v2_chart_Iss_Mmc(gg, yval_type, 
                        mcgrp, ssgrp, op, ir_name, i_comp, i_reim,
                        rsplan=rsplan, **kw)
                fig.savefig('%s/%s.pdf' % (out_dir, fig_name))
                dv.close_fig(fig)

