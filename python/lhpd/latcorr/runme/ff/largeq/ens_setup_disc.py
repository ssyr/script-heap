from __future__ import print_function
import math
import numpy as np
import lhpd


class srcsnkpair :
    def __init__(self, tsep, ksrc_tag, ksnk_tag, sm_tag) :
        self.tsep       = tsep
        self.ksrc_tag   = ksrc_tag
        self.ksnk_tag   = ksnk_tag
        self.sm_tag     = sm_tag
    def __hash__(self) : 
        return (hash(self.tsep) 
                ^ hash(self.ksrc_tag) 
                ^ hash(self.ksnk_tag) 
                ^ hash(self.sm_tag))
    def __eq__(self, oth) :
        return (self.tsep == oth.tsep 
                and self.ksrc_tag == oth.ksrc_tag
                and self.ksnk_tag == oth.ksnk_tag
                and self.sm_tag == oth.sm_tag)
    def __ne__(self, oth) :
        return not self.__eq__(oth)
    # since srcsnkpair has tsep, using it as key for c2pt map will result in 
    # 2pt functions stored len(tsep_list) times; fix: add special value specifically 
    # for that; when building c2pt map, check if it has already been inserted
    def key2pt(self) : 
        return srcsnkpair(0, self.ksrc_tag, self.ksnk_tag, self.sm_tag)
    def __str__(self) :
        return '%s<%s|%s>(T=%d)' % (self.sm_tag, self.ksnk_tag, self.ksrc_tag, self.tsep)
    def __repr__(self): return self.__str__() 
    # TODO add str repr
    def get_tsep(self)      : return self.tsep

class mc_case :
    tpol_map = {
        '3' : lhpd.latcorr.tpol_map['posSzplus'],
        # TODO add others
    }

    def __init__(self, p3src, p3snk, tpol) :
        self.p3src      = p3src
        self.p3snk      = p3snk
        self.tpol       = tpol
        self.tpol_c2    = 'AApSzplus'       # XXX FIXME TODO hack
    def __hash__(self) :
        return (hash(str(self.tpol)) 
                ^ hash(tuple(self.p3src)) 
                ^ hash(tuple(self.p3snk)))
    def __eq__(self, oth) :
        return (self.tpol == oth.tpol
                and tuple(self.p3src) == tuple(oth.p3src)
                and tuple(self.p3snk) == tuple(oth.p3snk))
    def __ne__(self, oth) :
        return not self.__eq__(oth)
    def __str__(self) :
        return '%s<%s|%s>' % (self.tpol, self.p3snk, self.p3src)
    def __repr__(self): return self.__str__() 
    def get_p3src(self)     : return self.p3src
    def get_p3snk(self)     : return self.p3snk
    def get_tpol(self)      : return self.tpol
    def get_tpol_matr(self) : return lhpd.latcorr.get_tpol_matr(self.get_tpol())
    def get_tpol_c2(self)   : return self.tpol_c2
    def get_tpol_c2_matr(self)   : return lhpd.latcorr.get_tpol_matr(self.get_tpol_c2())


""" examples
gg=ens_data_get(ama='ub', had='proton', flav='U-D', op='tensor1', data_top='.')

# bxp10, bym10
ssgrp=[ srcsnkpair(dt, 'bxp10', 'bym10', 'GN2x50') for dt in np.r_[8:13] ]
mcgrp=[ mc_case([k,0,0],[0,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]
# bxp10, bxm10
ssgrp=[ srcsnkpair(dt, 'bxp10', 'bxm10', 'GN2x50') for dt in np.r_[8:13] ]
mcgrp=[ mc_case([k,0,0],[-3,0,0], 'AApSzplus') for k in np.r_[-3:5] ]

# bqp10, brm10
ssgrp=[ srcsnkpair(dt, 'bqp10', 'brm10', 'GN2x25') for dt in np.r_[6:11] ]
mcgrp=[ mc_case([k,k,0],[3,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]
# bqp10, bqm10
ssgrp=[ srcsnkpair(dt, 'bqp10', 'bqm10', 'GN2x25') for dt in np.r_[6:11] ]
mcgrp=[ mc_case([k,k,0],[-3,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]
"""

class ens_data_get :
    """ 
        ss  = ( tsep[int], ksrc_tag[str], ksnk_tag[str], sm_tag[str] )
        mc  = ( p3src[tuple?], p3snk[tuple?] }
    """
    kbxp10  = np.r_[  1,  0,  0,  0]
    kbxm10  = np.r_[ -1,  0,  0,  0]
    kbyp10  = np.r_[  0,  1,  0,  0]
    kbym10  = np.r_[  0, -1,  0,  0]
    kbzp10  = np.r_[  0,  0,  1,  0]
    kbzm10  = np.r_[  0,  0, -1,  0]
    kbtp10  = np.r_[  0,  0,  0,  1]
    kbtm10  = np.r_[  0,  0,  0, -1]

    kbqp10  = np.r_[  1,  1,  0,  0]
    kbqm10  = np.r_[ -1, -1,  0,  0]
    kbrp10  = np.r_[ -1,  1,  0,  0]
    kbrm10  = np.r_[  1, -1,  0,  0]

    kmom_map= {
        'bqp10' : np.array(kbqp10[:3]),
        'bqm10' : np.array(kbqm10[:3]),
        'brm10' : np.array(kbrm10[:3]),
        'bxp10' : np.array(kbxp10[:3]),
        'bxm10' : np.array(kbxm10[:3]),
        'bym10' : np.array(kbym10[:3]),
        'bzm10' : np.array(kbzm10[:3]),
    }
    def __init__(self, **kw) :
        self.ama        = kw["ama"]
        self.had        = kw["had"]
        self.flav       = kw["flav"]
        self.op         = kw["op"]
        self.data_top   = kw["data_top"]
        self.mlat       = kw.get('mlat')
        self.latsize    = kw.get('latsize', None)
        self.c3pt_contr = kw.get('c3pt_contr') or 'conn'
        self.c3pt_improv= kw.get('c3pt_improv') or '0imp'
        #self.c2fit_file = kw.get("c2fit_file")
        self.c2fit_dgrp = kw.get("c2fit_dgrp")

    def conv_tpol_tag(self, tp) :
        if 'posSzplus' == tp or 'AApSzplus' == tp: return 'AApSzplus'

    def get_threept_conn_flav_(self, flav, ksrc_tag, ksnk_tag, sm_tag, tsep, 
            op, ir_name, p3src, p3snk, tpol) :
        src_boost_mom   = 3 * self.kmom_map[ksrc_tag]
        psrcrel = np.asarray(p3src) - src_boost_mom
        snk_boost_mom   = 3 * self.kmom_map[ksnk_tag]
        psnkrel = np.asarray(p3snk) - snk_boost_mom
        qextrel = psnkrel - psrcrel
        
        bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)
        def get_conn_flav(flav) :
            if self.ama in ['ex', 'sl'] :
                h5fname = get_op_bin_h5_file(self.ama, psnkrel, tsep, 
                        self.conv_tpol_tag(tpol), flav, op, bsm_tag)
            elif 'ub' == self.ama or 'unbias' == self.ama:
                h5fname = get_op_unbias_h5_file(psnkrel, tsep, 
                        self.conv_tpol_tag(tpol), flav, op, bsm_tag)

            h5f = h5py.File(h5fname, 'r')
            h5kpath = '/op/SS/%s_%s/%s/%s/%s/%s_dt%d' % (
                        self.had, self.conv_tpol_tag(tpol), flav,
                        op, ir_name, psnk_str_k(psnkrel), tsep)
            h5d = h5f[h5kpath]
            #print("%s[%s]" % (h5fname, h5kpath))
            assert((h5d.attrs['src_boost_mom'] == src_boost_mom).all())
            assert((h5d.attrs['snk_boost_mom'] == snk_boost_mom).all())
            i_qext  = lhpd.np_find_first(qextrel, h5d.attrs['qext_list'])[0]
            res = h5d[:, :, i_qext]
            h5f.close()
            #print(repr(res.mean(0)[:,tsep//2]))
            return res
            
        return lhpd.latcorr.calc_flav_fromUD(flav, get_conn_flav)

    def get_threept_disc_flav_(self, flav, ksrc_tag, ksnk_tag, sm_tag, tsep,
            op, ir_name, p3src, p3snk, tpol) :
        src_boost_mom   = 3 * self.kmom_map[ksrc_tag]
        psrcrel = np.asarray(p3src) - src_boost_mom
        snk_boost_mom   = 3 * self.kmom_map[ksnk_tag]
        psnkrel = np.asarray(p3snk) - snk_boost_mom
        qextrel = psnkrel - psrcrel
        
        bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)
        def get_conn_flav(flav) :
            h5fname = get_opdisc_all_h5_file(self.ama, psnkrel, op, bsm_tag)
            h5kpath = "/opdisc/SS/%s/%s/%s/%s" % (
                        self.had, op, ir_name, psnk_str_k(psnkrel))
            h5f = h5py.File(h5fname, 'r')
            h5d = h5f[h5kpath]
            n_tpol, n_tsep = h5d.shape[1:3]

            #print("%s[%s]" % (h5fname, h5kpath))
            assert((h5d.attrs['src_boost_mom'] == src_boost_mom).all())
            assert((h5d.attrs['snk_boost_mom'] == snk_boost_mom).all())
            assert(list(h5d.attrs['tsep_list']) == range(n_tsep))
            # check order of tsep and tpol
            assert(16 == n_tpol)
            assert(list(h5d.attrs['tpol_list']) == [ "Tg%d" % g for g in range(16)])

            i_qext  = lhpd.np_find_first(qextrel, h5d.attrs['qext_list'])[0]
            res = lhpd.latcorr.calc_tpol_fromGamma(tpol, lambda g: h5d[:, g, tsep, :, i_qext])
            print("WARNING : dividing data by 64; FIX calc_opdisc and data files")
            return res / 64.

        return lhpd.latcorr.calc_flav_fromUD(flav, get_conn_flav)
        

    def get_threept_flav_(self, flav, ksrc_tag, ksnk_tag, sm_tag, tsep,
            op, ir_name, p3src, p3snk, tpol, c3pt_contr) :
        if 'conn' == c3pt_contr : 
            return self.get_threept_conn_flav_(flav, ksrc_tag, ksnk_tag, sm_tag, 
                    tsep, op, ir_name, p3src, p3snk, tpol)
        elif 'disc' == c3pt_contr :
            return self.get_threept_disc_flav_(flav, ksrc_tag, ksnk_tag, sm_tag, 
                    tsep, op, ir_name, p3src, p3snk, tpol)
        elif 'full' == c3pt_contr :
            return (  self.get_threept_conn_flav_(flav, ksrc_tag, ksnk_tag, sm_tag, 
                                tsep, op, ir_name, p3src, p3snk, tpol)
                    + self.get_threept_disc_flav_(flav, ksrc_tag, ksnk_tag, sm_tag,
                                        tsep, op, ir_name, p3src, p3snk, tpol))
        else : raise ValueError(c3pt_contr)

    def get_threept_1imp_flav_(self, flav, ksrc_tag, ksnk_tag, sm_tag, tsep,
            op, ir_name, p3src, p3snk, tpol, c3pt_contr) :
        p4src   = lhpd.latcorr.lorentz_p4(self.mlat, self.latsize, p3src) * [1,1,1,1j]
        p4snk   = lhpd.latcorr.lorentz_p4(self.mlat, self.latsize, p3snk) * [1,1,1,1j]
        q4ext   = p4snk - p4src
        #print(repr(q4ext))
        res     = None
        if 'tensor1' == op :
            sig_comp= self.get_threept_flav_(flav, ksrc_tag, ksnk_tag, sm_tag, tsep,
                        'sigma2a2', 'H4_T2_d6r1', p3src, p3snk, tpol, c3pt_contr)

            xy,xz,xt,yz,yt,zt = tuple([ sig_comp[:,i] for i in range(6) ])
            n_data  = xy.shape[0]
            sig_munu= [ [  0, xy, xz, xt ], 
                        [-xy,  0, yz, yt ],
                        [-xz,-yz,  0, zt ],
                        [-xt,-yt,-zt,  0 ] ]
            res     = np.zeros((n_data,4) + xy.shape[1:], np.complex128)
            for mu in range(4) :
                for nu in range(4) :
                    res[:,mu] += q4ext[nu]*sig_munu[mu][nu]


            #print(repr(res.mean(0)[:,tsep//2]))
            #res *= 1j ; print("WARNING: correcting OLD T=qbar(-1j*sigma)q = (-1j)T : return New=1j*data")
            print("WARNING: assuming NEW T=qbar(sigma)q in file : NOT correcting")
            return res
        else : raise NotImplementedError
        assert(not None is res)
        #return res


    def get_threept(self, mc, ss, op, ir_name) :
        if   '0imp' == self.c3pt_improv : 
            return self.get_threept_flav_(
                        self.flav, ss.ksrc_tag, ss.ksnk_tag, ss.sm_tag, ss.tsep,
                        op, ir_name, mc.p3src, mc.p3snk, mc.tpol, self.c3pt_contr)
        elif '1imp' == self.c3pt_improv :
            return self.get_threept_1imp_flav_(self.flav, 
                        ss.ksrc_tag, ss.ksnk_tag, ss.sm_tag, ss.tsep,
                        op, ir_name, mc.p3src, mc.p3snk, mc.tpol, self.c3pt_contr)
        elif '01imp' == self.c3pt_improv :
            # TODO need to initialize self.c3pt_improv_coeff[op] map
            return (  self.get_threept_flav_(
                        self.flav, ss.ksrc_tag, ss.ksnk_tag, ss.sm_tag, ss.tsep,
                        op, ir_name, mc.p3src, mc.p3snk, mc.tpol, self.c3pt_contr)
                    + self.c3pt_improv_coeff[op] * self.get_threept_1imp_flav_(
                        self.flav, ss.ksrc_tag, ss.ksnk_tag, ss.sm_tag, ss.tsep,
                        op, ir_name, mc.p3src, mc.p3snk, mc.tpol, self.c3pt_contr))
        else : raise ValueError(self.c3pt_improv)



    # get momenta appropriate for getting symmetrically boost-smeared c2pt
    def get_c2symm_p3src(self, mc, ss) : 
        """ trivial """
        return mc.get_p3src()

    def get_c2symm_p3snk(self, mc, ss) :
        """ return p3snk that should be used to get c2, 
            since with boosted smearing, not all directions of p3snk are available in c2 
        """
        bsrc_q  = self.kmom_map[ss.ksrc_tag]
        bsrc_n  = math.sqrt((bsrc_q**2).sum())
        bsnk_q  = self.kmom_map[ss.ksnk_tag]
        bsnk_n  = math.sqrt((bsnk_q**2).sum())
        #ipdb.set_trace()
        p       = np.asarray(mc.p3snk)
        p3snk   = (p * bsnk_q).sum() * bsrc_q / bsrc_n / bsnk_n
        p3snk   = np.array(p3snk + .5, int)
        # only on-axis for now
        assert ((p3snk**2).sum() == (p**2).sum())
        #print("# snkrel=(%s-%s)=%s -> srcrel=(%s-%s)=%s" % (
            #mc.p3snk, 3*bsnk_q, mc.p3snk - 3*bsnk_q,
            #p3snk, 3*bsrc_q, p3snk - 3*bsrc_q))
        return p3snk

    def get_twopt_symm_mom_tpol_(self, ksrc_tag, ksnk_tag, sm_tag, p3, tpol) :
        p3              = np.asarray(p3)
        src_boost_mom   = 3 * self.kmom_map[ksrc_tag]
        psnkrel = p3 - src_boost_mom    # sic! psnk==psrc for symm
        bsm_tag_symm    = "%s_%s_%s" % (sm_tag, ksrc_tag, ksrc_tag)
        if self.ama in ['ex', 'sl'] :
            h5f = h5py.File(get_hspec_bin_h5_file(self.ama, self.had, bsm_tag_symm), 'r')
        elif 'ub' == self.ama or 'unbias' == self.ama:
            h5f = h5py.File(get_hspec_unbias_h5_file(self.had, bsm_tag_symm), 'r')
        h5kpath = '/hadspec/SS/%s' % (self.had,)
        h5d = h5f[h5kpath]
        assert((h5d.attrs['src_boost_mom'] == src_boost_mom).all())
        assert((h5d.attrs['snk_boost_mom'] == src_boost_mom).all())    # sic! for symm boosting
        i_p = lhpd.np_find_first(psnkrel, h5d.attrs['psnk_list'])[0]
        
        def get_Tg(g) : return h5d[:, g, i_p]
        return lhpd.latcorr.calc_tpol_fromGamma(tpol, get_Tg)

    def get_twopt_src(self, mc, ss) :
        return self.get_twopt_symm_mom_tpol_(ss.ksrc_tag, ss.ksnk_tag, ss.sm_tag, 
                    self.get_c2symm_p3src(mc, ss), mc.tpol_c2)
    def get_twopt_snk(self, mc, ss) :
        return self.get_twopt_symm_mom_tpol_(ss.ksrc_tag, ss.ksnk_tag, ss.sm_tag, 
                    self.get_c2symm_p3snk(mc, ss), mc.tpol_c2)
    #def get_twopt(self, p3, tpol, ss) :
        #return self.get_twopt_mom_tpol_(ss.ksrc_tag, ss.ksnk_tag, ss.sm_tag, p3, tpol)

    def get_twopt_fit_mom_tpol_(self, p3, tpol) :
        snk_str = "px%dpy%dpz%d" % tuple(p3)
        #h5f     = h5py.File(self.c2fit_file, 'r')
        #h5kpath = "/%s_%s/%s" % (self.had, tpol, snk_str)
        #h5d     = h5f[h5kpath]
        h5d     = self.c2fit_dgrp[snk_str]
        assert('c2pt_c0' == h5d.attrs['pname'][0])
        assert('c2pt_e0' == h5d.attrs['pname'][1])
        assert('c2pt_c1' == h5d.attrs['pname'][2])
        assert('c2pt_de1_0' == h5d.attrs['pname'][3])
        res     = h5d['p']
        #h5f.close()
        return res
    def get_twopt_fit_src(self, mc, ss) :
        return self.get_twopt_fit_mom_tpol_(mc.p3src, mc.tpol) 
    def get_twopt_fit_snk(self, mc, ss) :
        return self.get_twopt_fit_mom_tpol_(self.get_c2symm_p3snk(mc, ss), mc.tpol) 

    def str_f(self) :
        return '%s.%s.%s.%s' % (
            self.ama, self.had, self.flav, self.op)
 
