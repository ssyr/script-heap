from past.builtins import execfile
execfile('config_c23pt_boost1_xx_xy.py') ; execfile('ens_setup_disc.py') ; execfile('pproc_largeq.py')
#execfile('fit_c2.py')

latsize=np.r_[32,32,32,64]
mlat=0.47
ainv=2.436
ama='ub'
had='proton'
rsplan=('jk',1)
ZV=1./1.205

ksrc_tag,ksnk_tag,sm_tag='bxp10', 'bxm10', 'GN2x50'# bxp10, bxm10
#ksrc_tag,ksnk_tag,sm_tag='bxp10', 'bym10', 'GN2x50'# bxp10, bym10
flav_list1=['U+D']
tmin,tmax,tskip=8,10,3
tsep_list=np.r_[tmin:tmax+1]
method_list1 = [('ratio_pltx',), ('ratio_pltx_avg',), ('ratio_summ',)]#, ('fit_c3',)]

ssgrp=[ srcsnkpair(dt, ksrc_tag,ksnk_tag,sm_tag) for dt in tsep_list ]
spin_dir="yz"
tpol_list = ['posS%s%s' %(sdir,s) for sdir in spin_dir for s in ['plus', 'minus']] # TODO add Sx,Sy
mcgrp_list=[ [ mc_case([k,0,0],[-3,0,0],tpol) for tpol in tpol_list ] for k in np.r_[-2:4] ]

c2fit_h5fname="c2fit.%s.%s_%s_%s.h5" % (ama,sm_tag,ksrc_tag,ksrc_tag)
ff_h5fname="ff.%s.%s_%s_%s.dt%d--%d.tskip%d.S%s.h5" % (ama,sm_tag,ksrc_tag,ksnk_tag, tmin,tmax, tskip, ''.join(spin_dir))
out_dir="ff-q2dep.%s.%s_%s_%s.dt%d--%d.tskip%d" % (ama,sm_tag,ksrc_tag,ksnk_tag, tmin,tmax, tskip)

"""
ff_param_kw=dict(mlat=mlat, latsize=latsize, rsplan=rsplan, ama='ub', had='proton', data_top='.', tskip=tskip, c2fit_nexp=2, c3pt_contr='disc')
runme_calc_save_ff(ff_h5fname, mcgrp_list, ssgrp, flav_list1, ['tensor1'], method_list1, **ff_param_kw)
#c2fit_dgrp=h5py.File(c2fit_h5fname, 'r')['tr3--15'], 

runme_plot_all_ff(ff_h5fname, mlat, flav_list1, ['F1','F2','Q4F1','Q4F2','Q2F2dF1','GE2GM'], tsep_list, [m[0] for m in method_list1], out_dir=out_dir, ainv=ainv, xr=[0,10])
"""
