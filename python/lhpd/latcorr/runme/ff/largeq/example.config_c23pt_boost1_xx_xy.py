import numpy as np
import lhpd
import itertools as it
import h5py
import aff
import os, sys, time, tempfile
from lhpd.h5_io import h5_meas_dtype

latsize = np.r_[32,32,32,64]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]
sm_list = [ 'GN2x50' ]

had_list = [ 'proton_AApSzplus' ]
c2pt_tpol_list = [ 'Tg%d' % g for g in range(16) ]    # all gammas
c2pt_hslab_len = 16
c3pt_tpol_list = [ 'AApSzplus' ]
c3pt_tsep_list = [ 8, 9, 10, 11, 12]
c3pt_flav_list = ['U', 'D']
#c3pt_op_list   = [ 'tensor1', 'pstensor1' ]
c3pt_op_list   = [ 'tensor0', 'pstensor0', 'tensor1', 'pstensor1', 'sigma2a2' ]
c3pt_lpath_min, c3pt_lpath_max  = 0,1


###############################################################################
# AMA & source coord selection
###############################################################################

cfg_index   = lhpd.read_key_list('list.cfg.all', 'index')
cfg_list    = lhpd.read_key_list('list.cfg', 'samples')

meas_x0 = np.r_[0,0,0,0]
meas_x1 = np.r_[8,8,0,0]
meas_dxc= np.r_[7,11,13,23]

ama_ex = 'ex'
ama_sl = 'sl'
ama_list = [ama_ex, ama_sl]

# create csrcgrp_lists for each config, create meas_list in conversion routines
nsrc_ama_map = { 
    ama_ex : [1,1,1,4], 
    ama_sl : [2,2,2,4] }
def make_csrcgrp_list(ama, cfgkey) :
    if 'ex' == ama : 
        nsrc = [1,1,1,4]
        csrcgrp_list = lhpd.make_src_grid(nsrc, latsize, meas_x0, meas_dxc, 
                1 + cfg_index.index(cfgkey)).reshape(-1, nsrc[t_axis], len(latsize))
    elif 'sl' == ama :
        nsrc = [2,2,2,4]
        csrcgrp_list = np.r_[ 
                lhpd.make_src_grid(nsrc, latsize, meas_x0, meas_dxc, 
                              1 + cfg_index.index(cfgkey)
                             ).reshape(-1, nsrc[t_axis], len(latsize)),
                lhpd.make_src_grid(nsrc, latsize, meas_x1,
                              meas_dxc, 1 + cfg_index.index(cfgkey)
                             ).reshape(-1, nsrc[t_axis], len(latsize))
        ]

    # check that the spatial coord is the same
    assert( (csrcgrp_list[:,:,:t_axis] == csrcgrp_list[:,0:1,:t_axis]).all() )
    return csrcgrp_list


###############################################################################
# momenta selection
###############################################################################

pm1     = [-1, 0, 1]        # [-1;1]
pm2     = [-2,-1, 0, 1, 2]  # [-2;2]
pm6     = range(-6, 1+6)    # [-6;6]

kbxp10  = np.r_[  1,  0,  0,  0]
kbxm10  = np.r_[ -1,  0,  0,  0]
kbym10  = np.r_[  0, -1,  0,  0]
kbzm10  = np.r_[  0,  0, -1,  0]

c3pt_ksrc_tag, c3pt_ksrc_q  = 'bxp10', kbxp10

c2pt_symmboost_psnkrel_list = lhpd.range_prod([pm6, pm1, pm1])
c3pt_psnkrel        = [ 0,  0,  0 ]
c3pt_qextrel_list   = lhpd.range_prod([pm6, pm1, pm1])
c2pt_skewboost_psnkrel_list = lhpd.range_prod([pm1, pm1, pm1])

c3pt_snk_desc_list = [
     #-- p'= (-3, 0, 0)
     #-- p = ( 3, 0, 0) + ([-6;6], [-1;1], [-1;1]) = ([ -3;9], [-1;1], [-1;1])
     #-- q = (-6, 0, 0) + ([-6;6], [-1;1], [-1;1]) = ([-12;0], [-1;1], [-1;1])
     ( 'bxm10', kbxm10, c2pt_skewboost_psnkrel_list, c3pt_psnkrel, c3pt_qextrel_list ),
     #-- p'= ( 0,-3, 0)
     #-- p = ( 3, 0, 0) + ([-6;6], [-1;1], [-1;1]) = ([-3;9], [-1; 1], [-1;1])
     #-- q = (-3,-3, 0) + ([-6;6], [-1;1], [-1;1]) = ([-9;3], [-4;-2], [-1;1])
     ( 'bym10', kbym10, c2pt_skewboost_psnkrel_list, c3pt_psnkrel, c3pt_qextrel_list ),
]

c2pt_srcsnkdesc_list = [
    (c3pt_ksrc_tag, c3pt_ksrc_q, c3pt_ksrc_tag, c3pt_ksrc_q, c2pt_symmboost_psnkrel_list) ] + \
    [ (c3pt_ksrc_tag, c3pt_ksrc_q, t, q, pl) for t,q,pl,x,y in c3pt_snk_desc_list ]

###############################################################################
# file locations 
###############################################################################
# XXX psnk is psnkrel everywhere; otherwise, files will be different with ksnk_tag / ksrc_tag

def csrc_str_f(csrc) : return 'x%dy%dz%dt%d' % tuple(csrc)
def csrc_str_k(csrc) : return 'x%d_y%d_z%d_t%d' % tuple(csrc)
def psnk_str_f(csrc) : return 'PX%dPY%dPZ%d' % tuple(csrc)
def psnk_str_k(csrc) : return 'PX%d_PY%d_PZ%d' % tuple(csrc)
def ama_str(ama) : return ama


data_top='../data.c23pt'

def get_hadspec_aff_file(cfgkey, ama, csrc, bsm_tag) :
    return '%s/hadspec/hadspec.%s.%s.%s.%s.aff' % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrc), bsm_tag)
def get_hadspec_aff_kpath(cfgkey, csrc, tpol) :
    return '/cfg%s/hadspec/SS/proton_%s/%s' % (
            cfgkey, tpol, csrc_str_k(csrc))
def make_hsfilekpath_func(cfgkey, ama, bsm_tag) :
    def hsfilekpath_func(csrc, tpol) :
        return (get_hadspec_aff_file(cfgkey, ama, csrc, bsm_tag),
                get_hadspec_aff_kpath(cfgkey, csrc, tpol))
    return hsfilekpath_func


def get_bb_aff_file(cfgkey, ama, csrc, psnk, tsep, tpol, flav, bsm_tag) :
    return "%s/bb/bb.%s.%s.%s.%sdt%d.%s.%s.%s.aff" % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrc), 
            psnk_str_f(psnk), tsep, tpol, flav, bsm_tag)
def get_bb_aff_kpath(cfgkey, csrc, psnk, tsnk, tpol, flav) :
    return "/cfg%s/bb/SS/proton_%s/%s/%s/%s_T%d" % (
            cfgkey, tpol, flav, csrc_str_k(csrc), psnk_str_k(psnk), tsnk)
def make_bbfilekpath_func(cfgkey, ama, psnk, tsep, tpol, flav, bsm_tag) :
    """ return aff file and kpath generator function to be used with `conv_bb2hdf'
    """
    def bbfilekpath_func(csrc, tsnk) :
        return (
            get_bb_aff_file(cfgkey, ama, csrc, psnk, tsep, tpol, flav, bsm_tag),
            get_bb_aff_kpath(cfgkey, csrc, psnk, tsnk, tpol, flav))
    return bbfilekpath_func


data_out    = '.'

def get_hspec_h5_file(cfgkey, ama, had, bsm_tag) :
    return '%s/hspec/hspec.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, had, bsm_tag)
def get_hspec_bin_h5_file(ama, had, bsm_tag) :
    return '%s/hspec.bin/hspec-bin.%s.%s.%s.h5' % (
            data_out, ama, had, bsm_tag)
def get_hspec_unbias_h5_file(had, bsm_tag) :
    return '%s/hspec.bin/hspec-unbias.%s.%s.h5' % (
            data_out, had, bsm_tag)


def get_bb_h5_file(cfgkey, ama, psnk, tsep, tpol_tag, flav, bsm_tag) :
    return '%s/bb/bb.%s.%s.%sdt%d.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
            tpol_tag, flav, bsm_tag)
def get_bb_bin_h5_file(ama, psnk, tsep, tpol_tag, flav, bsm_tag) :
    return '%s/bb.bin/bb-bin.%s.%sdt%d.%s.%s.%s.h5' % (
            data_out, ama, psnk_str_f(psnk), tsep, 
            tpol_tag, flav, bsm_tag)
def get_bb_unbias_h5_file(psnk, tsep, tpol_tag, flav, bsm_tag) :
    return '%s/bb.bin/bb-unbias.%s.%sdt%d.%s.%s.%s.h5' % (
            data_out, psnk_str_f(psnk), tsep, 
            tpol_tag, flav, bsm_tag)


def get_op_h5_file(cfgkey, ama, psnk, tsep, tpol_tag, flav, op, bsm_tag) :
    return '%s/op/op.%s.%s.%sdt%d.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
            tpol_tag, flav, op, bsm_tag)
def get_op_bin_h5_file(ama, psnk, tsep, tpol_tag, flav, op, bsm_tag) :
    return '%s/op.bin/op.%s.%sdt%d.%s.%s.%s.%s.h5' % (
            data_out, ama, psnk_str_f(psnk), tsep, 
                tpol_tag, flav, op, bsm_tag)
def get_op_unbias_h5_file(psnk, tsep, tpol_tag, flav, op, bsm_tag) :
    return '%s/op.bin/op-unbias.%sdt%d.%s.%s.%s.%s.h5' % (
            data_out, psnk_str_f(psnk), tsep, 
                tpol_tag, flav, op, bsm_tag)

# DISCO stuff
def get_bbqloop_h5_file(cfgkey) :
    return "%s/bb-qloop/bb-qloop.%s.h5" % (
            data_out, cfgkey)

def get_opdisc_h5_file(cfgkey, ama, psnk, op, bsm_tag) : 
    return "%s/op-disc/op.%s.%s.%s.%s.h5" % (
            data_out, cfgkey, ama, psnk_str_f(psnk), op)
