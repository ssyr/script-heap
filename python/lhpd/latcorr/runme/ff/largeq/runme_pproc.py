#!/usr/bin/env python
from __future__ import print_function
from past.builtins import execfile

import os, sys

if '__main__' == __name__ :
    def print_usage(prg) :
        print("""Usage:
%s  <what>  <config>  [<job_id0>, ...]
What :
    conv_hspec
    bin_unbias_hspec
    conv_bb
    calc_op
    bin_unbias_op
    calc_opdisc
    merge_opdisc
""" % (prg,))

    if len(sys.argv) < 3 : 
        print_usage(sys.argv[0] or 'me')
        sys.exit(1)
    conf_py   = sys.argv[2]
    execfile(conf_py)
    execfile('pproc_largeq.py')


##############################################################################
def runme_conv_hspec2hdf(cfgkey, ama, sm_tag, 
                baryon_bc_t=ferm_bc[3]**3,
                hslab_len=c2pt_hslab_len,
                srcsnkdesc_list=c2pt_srcsnkdesc_list,
                tpol_list=c2pt_tpol_list,
                attrs_kw={}) :
    
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)

    for srcsnkdesc in srcsnkdesc_list :
        ksrc_tag, ksrc_mom, ksnk_tag, ksnk_mom, psnkrel_list = srcsnkdesc
        had     = 'proton'
        bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)
        h5file  = get_hspec_h5_file(cfgkey, ama, had, bsm_tag)
        h5kpath = "/cfg%s/hadspec/SS/%s" % (cfgkey, had)
        print("# >>> %s [ %s ]" % (h5file, h5kpath))
        attrs_kw_this = dict(attrs_kw)
        attrs_kw_this.update(
                    source_hadron=had, sink_hadron=had,
                    # boost momenta of 3-quark source and sink
                    src_boost_mom=3*np.asarray(ksrc_mom[:3]), 
                    snk_boost_mom=3*np.asarray(ksnk_mom[:3])
                    )

        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')
        hskf    = make_hsfilekpath_func(cfgkey, ama, bsm_tag)
        conv_hspec2hdf(h5f, h5kpath, latsize, hskf, cfgkey, 
                    csrcgrp_list, psnkrel_list, tpol_list, c2pt_hslab_len, 
                    baryon_bc_t, attrs_kw_this)
        h5f.flush()
        h5f.close()
            

##############################################################################
def runme_bin_unbias_hadspec(cfgkey_list, sm_tag,
                srcsnkdesc_list=c2pt_srcsnkdesc_list,
                VERBOSE=False) :
    # since Tpol is TgN, keep them all together
    for (srcsnkdesc,) in it.product(
            srcsnkdesc_list) :
        ksrc_tag, ksrc_mom, ksnk_tag, ksnk_mom, c2pt_psnk = srcsnkdesc
        bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)

        print("# %s" % (bsm_tag,))

        had     = 'proton'
        h5_kpath  = "/hadspec/SS/%s" % (had,)

        # bin ex
        h5_list_ex  = [ (get_hspec_h5_file(c, ama_ex, had, bsm_tag), 
                        '/cfg%s/%s' % (c, h5_kpath)) 
                        for c in cfgkey_list ]
        h5_file_ex  = get_hspec_bin_h5_file(ama_ex, had, bsm_tag)
        lhpd.mkpath(os.path.dirname(h5_file_ex))
        lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, VERBOSE=VERBOSE)

        # bin sl
        h5_list_sl  = [ (get_hspec_h5_file(c, ama_sl, had, bsm_tag), 
                        '/cfg%s/%s' % (c, h5_kpath)) 
                        for c in cfgkey_list ]
        h5_file_sl  = get_hspec_bin_h5_file(ama_sl, had, bsm_tag)
        lhpd.mkpath(os.path.dirname(h5_file_sl))
        lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, VERBOSE=VERBOSE)
            
        # unbias
        h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
        h5_file_ub  = get_hspec_unbias_h5_file(had, bsm_tag)
        lhpd.mkpath(os.path.dirname(h5_file_ub))
        lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, VERBOSE=VERBOSE)


##############################################################################
def runme_conv_bb2hdf(cfgkey, ama, sm_tag, 
                ksrc_tag, ksrc_mom,
                baryon_bc_t=ferm_bc[3]**3,
                tsep_list=c3pt_tsep_list,
                snk_desc_list=c3pt_snk_desc_list,
                tpol_list=c3pt_tpol_list,
                flav_list=c3pt_flav_list,
                lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                attrs_kw={}) :
    
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

    for (tsep, snk_desc, tpol, flav) in it.product(
            tsep_list, snk_desc_list, tpol_list, flav_list) :
        ksnk_tag, ksnk_mom, c2pt_psnk_list, c3pt_psnk, qext_list = snk_desc
        bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)
        h5file  = get_bb_h5_file(cfgkey, ama, c3pt_psnk, tsep, tpol, flav, bsm_tag)
        had     = 'proton_%s' % tpol
        h5kpath = "/cfg%s/bb/SS/%s/%s/%s_dt%d" % (
                cfgkey, had, flav, psnk_str_k(c3pt_psnk), tsep)
        print("# >>> %s [ %s ]" % (h5file, h5kpath))
        attrs_kw_this = dict(attrs_kw)
        attrs_kw_this.update(
                    source_hadron=had, sink_hadron=had,
                    time_neg=False, sink_mom=c3pt_psnk,
                    # boost momenta of 3-quark source and sink
                    src_boost_mom=3*np.asarray(ksrc_mom[:3]), 
                    snk_boost_mom=3*np.asarray(ksnk_mom[:3])
                    )

        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')
        bbkf    = make_bbfilekpath_func(cfgkey, ama, c3pt_psnk, tsep, tpol, flav, bsm_tag)
        conv_bb2hdf(h5f, h5kpath, latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                    qext_list, lpath_list, baryon_bc_t, attrs_kw_this)
        h5f.flush()
        h5f.close()


##############################################################################
def runme_calc_save_op(cfgkey, ama, sm_tag,
                ksrc_tag, ksrc_mom,
                tsep_list=c3pt_tsep_list,
                snk_desc_list=c3pt_snk_desc_list,
                tpol_list=c3pt_tpol_list,
                flav_list=c3pt_flav_list,
                op_list=c3pt_op_list,
                attrs_kw={}) :
    
    for (tsep, snk_desc, tpol, flav, op) in it.product(
            tsep_list, snk_desc_list, tpol_list, flav_list, op_list) :
        ir_list = lhpd.latcorr.op_default_ir_list(op)
        ksnk_tag, ksnk_mom, c2pt_psnk_list, c3pt_psnk, qext_list = snk_desc
        bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)
        had     = 'proton_%s' % tpol
        
        h5bb_file   = get_bb_h5_file(cfgkey, ama, c3pt_psnk, tsep, tpol, flav, bsm_tag)
        h5bb_kpath  = "/cfg%s/bb/SS/%s/%s/%s_dt%d" % (
                cfgkey, had, flav, psnk_str_k(c3pt_psnk), tsep)
        h5f_bb  = h5py.File(h5bb_file, 'r')

        h5op_file   = get_op_h5_file(cfgkey, ama, c3pt_psnk, tsep, tpol, flav, op, bsm_tag)
        lhpd.mkpath(os.path.dirname(h5op_file))
        h5f_op  = h5py.File(h5op_file, 'a')
        for ir_name, ir_scale in ir_list :
            h5op_kpath  = "/cfg%s/op/SS/%s/%s/%s/%s/%s_dt%d" % (
                    cfgkey, had, flav, op, ir_name, psnk_str_k(c3pt_psnk), tsep)
            print("# >>> %s [ %s ]" % (h5op_file, h5op_kpath))

            calc_save_op(h5f_op, h5op_kpath, latsize, h5f_bb[h5bb_kpath], 
                        c3pt_psnk, tsep, op, ir_name)

        h5f_op.flush()
        h5f_op.close()
        h5f_bb.close()


##############################################################################
def runme_bin_unbias_op(cfgkey_list, sm_tag,
                ksrc_tag, ksrc_mom,
                tsep_list=c3pt_tsep_list,
                snk_desc_list=c3pt_snk_desc_list,
                tpol_list=c3pt_tpol_list,
                flav_list=c3pt_flav_list,
                op_list=c3pt_op_list,
                VERBOSE=False) :

    for (tsep, snk_desc, tpol, flav, op) in it.product(
            tsep_list, snk_desc_list, tpol_list, flav_list, op_list) :
        ksnk_tag, ksnk_mom, c2pt_psnk_list, c3pt_psnk, qext_list = snk_desc
        ir_list = lhpd.latcorr.op_default_ir_list(op)
        for ir_name, ir_scale in ir_list :
            bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)
            had     = 'proton_%s' % tpol

            print("# %s %s dt=%d %s %s %s %s %s" % (
                    had, str(c3pt_psnk), tsep, tpol, flav, op, ir_name, bsm_tag))

            h5_kpath  = "/op/SS/%s/%s/%s/%s/%s_dt%d" % (
                    had, flav, op, ir_name, psnk_str_k(c3pt_psnk), tsep)

            # bin ex
            h5_list_ex  = [ (get_op_h5_file(c, ama_ex, c3pt_psnk, tsep, 
                                            tpol, flav, op, bsm_tag), 
                            '/cfg%s/%s' % (c, h5_kpath)) 
                            for c in cfgkey_list ]
            h5_file_ex  = get_op_bin_h5_file(ama_ex, c3pt_psnk, tsep, tpol, flav, op, bsm_tag)
            lhpd.mkpath(os.path.dirname(h5_file_ex))
            lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, VERBOSE=VERBOSE)

            # bin sl
            h5_list_sl  = [ (get_op_h5_file(c, ama_sl, c3pt_psnk, tsep, 
                                            tpol, flav, op, bsm_tag), 
                            '/cfg%s/%s' % (c, h5_kpath)) 
                            for c in cfgkey_list ]
            h5_file_sl  = get_op_bin_h5_file(ama_sl, c3pt_psnk, tsep, tpol, flav, op, bsm_tag)
            lhpd.mkpath(os.path.dirname(h5_file_sl))
            lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, VERBOSE=VERBOSE)
            
            # unbias
            h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
            h5_file_ub  = get_op_unbias_h5_file(c3pt_psnk, tsep, tpol, flav, op, bsm_tag)
            lhpd.mkpath(os.path.dirname(h5_file_ub))
            lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, VERBOSE=VERBOSE)


##############################################################################
def runme_calc_save_unbias_opdisc(cfgkey, sm_tag, 
                ksrc_tag, ksrc_mom, 
                hslab_len=c2pt_hslab_len,
                snk_desc_list=c3pt_snk_desc_list,
                op_list=c3pt_op_list,
                attrs_kw={}) :
    
    for (snk_desc, op) in it.product(
            snk_desc_list, op_list) :
        ir_list = lhpd.latcorr.op_default_ir_list(op)
        ksnk_tag, ksnk_mom, c2pt_psnk_list, c3pt_psnk, qextrel_list = snk_desc
        bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)
        had     = 'proton' 

        # input c2pt
        c2pt_sl_h5file  = get_hspec_h5_file(cfgkey, 'sl', had, bsm_tag)
        c2pt_sl_h5f     = h5py.File(c2pt_sl_h5file, 'r')
        c2pt_ex_h5file  = get_hspec_h5_file(cfgkey, 'ex', had, bsm_tag)
        c2pt_ex_h5f     = h5py.File(c2pt_ex_h5file, 'r')
        c2pt_kpath      = "/cfg%s/hadspec/SS/%s" % (cfgkey, had)
        # input bb-qloop
        bbql_h5file = get_bbqloop_h5_file(cfgkey)
        bbql_h5f    = h5py.File(bbql_h5file, 'r')
        bbql_kpath  = '/'
        # output
        opdisc_sl_h5file= get_opdisc_bin_h5_file(cfgkey, 'sl', c3pt_psnk, op, bsm_tag)
        lhpd.mkpath(os.path.dirname(opdisc_sl_h5file))
        opdisc_sl_h5f   = h5py.File(opdisc_sl_h5file, 'a')

        opdisc_ub_h5file= get_opdisc_bin_h5_file(cfgkey, 'ub', c3pt_psnk, op, bsm_tag)
        lhpd.mkpath(os.path.dirname(opdisc_ub_h5file))
        opdisc_ub_h5f   = h5py.File(opdisc_ub_h5file, 'a')
        print("# <<< %s[%s]" % (c2pt_ex_h5file, c2pt_kpath))
        print("# <<< %s[%s]" % (c2pt_sl_h5file, c2pt_kpath))
        print("# <<< %s[%s]" % (bbql_h5file, bbql_kpath))

        for ir_name, ir_scale in ir_list :
            opdisc_h5kpath  = "/cfg%s/opdisc/SS/%s/%s/%s/%s" % (
                    cfgkey, had, op, ir_name, psnk_str_k(c3pt_psnk))
            print("# >>> %s[%s]" % (opdisc_sl_h5file, opdisc_h5kpath))
            print("# >>> %s[%s]" % (opdisc_ub_h5file, opdisc_h5kpath))

            calc_save_unbias_opdisc(
                        opdisc_sl_h5f, opdisc_h5kpath,
                        opdisc_ub_h5f, opdisc_h5kpath,
                        latsize,
                        c2pt_sl_h5f[c2pt_kpath], c2pt_ex_h5f[c2pt_kpath],
                        bbql_h5f[bbql_kpath],
                        c3pt_psnk, qextrel_list,
                        hslab_len - 1, hslab_len - 1,           
                        op, ir_name, 
                        attrs_kw=attrs_kw)
        
        opdisc_sl_h5f.flush()
        opdisc_sl_h5f.close()
        
        opdisc_ub_h5f.flush()
        opdisc_ub_h5f.close()


##############################################################################
def runme_merge_opdisc(cfgkey_list, ama, sm_tag, ksrc_tag, 
                snk_desc_list=c3pt_snk_desc_list,
                op_list=c3pt_op_list,
                attrs_kw={}) :

    n_data  = len(cfgkey_list)
    for (snk_desc, op) in it.product(
            snk_desc_list, op_list) :
        ir_list = lhpd.latcorr.op_default_ir_list(op)
        ksnk_tag, ksnk_mom, c2pt_psnk_list, c3pt_psnk, qextrel_list = snk_desc
        bsm_tag = "%s_%s_%s" % (sm_tag, ksrc_tag, ksnk_tag)
        had     = 'proton' 

        out_h5file = get_opdisc_all_h5_file(ama, c3pt_psnk, op, bsm_tag)
        print(out_h5file)
        out_h5f = h5py.File(out_h5file, 'a')
        out_h5d = None
        for ir_name, ir_scale in ir_list :
            h5key   = "opdisc/SS/%s/%s/%s/%s" % (
                        had, op, ir_name, psnk_str_k(c3pt_psnk))
            for i_c, cfgkey in enumerate(cfgkey_list) :
                print("# %s" % cfgkey)
                opdisc_h5file= get_opdisc_bin_h5_file(cfgkey, ama, c3pt_psnk, op, bsm_tag)
                opdisc_h5f  = h5py.File(opdisc_h5file, 'r')
                opdisc_h5d  = opdisc_h5f["/cfg%s/%s" % (cfgkey, h5key)]
                if None is out_h5d :
                    out_h5d = out_h5f.require_dataset(h5key, (n_data,) + opdisc_h5d.shape,
                                opdisc_h5d.dtype, fletcher32=True)
                    lhpd.h5_io.h5_copy_attr(out_h5d, opdisc_h5d, 
                            [ 'tpol_list', 'source_hadron', 'sink_hadron', 'src_boost_mom', 
                              'snk_boost_mom', 'sink_mom', 'qext_list', 'tsep_list', 
                              'tau_list', 'comp_list' ],
                            check_key=True)
                    out_h5d.attrs['dim_spec'] = ['i_data', 'i_tpol', 'i_tsep', 'i_comp', 'i_qext', 'i_tau']

                out_h5d[i_c] = opdisc_h5d
        
        out_h5f.flush()
        out_h5f.close()


##############################################################################
##############################################################################
if '__main__' == __name__ :
    what   = sys.argv[1]
    jobid_list      = sys.argv[3:]

    if   'conv_hspec' == what :
        for c in jobid_list :
            for ama in ['ex', 'sl'] :
                for sm_tag in sm_list :
                    print("# %s %s %s " % (c, ama, sm_tag))
                    runme_conv_hspec2hdf(c, ama, sm_tag)

    elif 'bin_unbias_hspec' == what :
        wl  = list(it.product(c2pt_srcsnkdesc_list))
        print("# len(wl) =", len(wl))

        for j_str in jobid_list : 
            j   = int(j_str)
            assert(0 <= j and j < len(wl))
            srcsnkdesc, = wl[j]
            for sm_tag in sm_list :
                print("# %s %s %s" % (sm_tag, srcsnkdesc[0], srcsnkdesc[2]))
                runme_bin_unbias_hadspec(cfg_list, sm_tag, 
                        srcsnkdesc_list = [srcsnkdesc],
                        VERBOSE=True)

    elif 'conv_bb' == what :
        for c in jobid_list : 
            for ama in ['ex', 'sl'] :
                for sm_tag in sm_list :
                    print("# %s %s %s " % (c, ama, sm_tag))
                    runme_conv_bb2hdf(c, ama, sm_tag, c3pt_ksrc_tag, c3pt_ksrc_q, baryon_bc_t=ferm_bc[3]**3)

    elif 'calc_op' == what :
        for c in jobid_list : 
            for ama in ['ex', 'sl'] :
                for sm_tag in sm_list :
                    print("# %s %s %s " % (c, ama, sm_tag))
                    runme_calc_save_op(c, ama, sm_tag, c3pt_ksrc_tag, c3pt_ksrc_q)

    elif 'bin_unbias_op' == what :
        wl  = list(it.product(c3pt_tsep_list, c3pt_snk_desc_list, c3pt_tpol_list, c3pt_flav_list, c3pt_op_list))
        print("# len(wl) =", len(wl))

        for j_str in jobid_list : 
            j   = int(j_str)
            assert(0 <= j and j < len(wl))
            tsep, snk_desc, tpol, flav, op = wl[j]
            for sm_tag in sm_list :
                print("# dt=%d %s %s %s %s %s" % (tsep, snk_desc[0], tpol, flav, op, sm_tag))
                runme_bin_unbias_op(cfg_list, sm_tag, c3pt_ksrc_tag, c3pt_ksrc_q,
                        tsep_list       = [tsep],
                        snk_desc_list   = [snk_desc],
                        tpol_list       = [tpol],
                        flav_list       = [flav],
                        op_list         = [op],
                        VERBOSE=True)

    elif 'calc_opdisc' == what :
        if len(jobid_list) <= 0 :
            print("no jobid list")
        for jobid in jobid_list : 
            for sm_tag in sm_list :
                runme_calc_save_unbias_opdisc(
                            jobid, sm_tag, c3pt_ksrc_tag, c3pt_ksrc_q)

    elif 'merge_opdisc' == what :
        wl  = list(it.product(c3pt_snk_desc_list, c3pt_op_list))
        print("# len(wl) =", len(wl))

        for j_str in jobid_list : 
            j   = int(j_str)
            j   = int(j_str)
            assert(0 <= j and j < len(wl))
            snk_desc, op = wl[j]
            #for ama in ['sl', 'ub'] :
            for ama in ['ub'] : # only the necessary
                for sm_tag in sm_list :
                    runme_merge_opdisc(cfg_list, ama, sm_tag, c3pt_ksrc_tag,
                            snk_desc_list   = [snk_desc],
                            op_list         = [op],
                            )
    
    else :
        raise ValueError(what)
