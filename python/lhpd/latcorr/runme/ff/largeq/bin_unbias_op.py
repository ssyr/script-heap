#! /usr/bin/env python
from __future__ import print_function
from past.builtins import execfile
import sys, os

execfile('config_c23pt_boost1_xx_xy.py')
execfile('pproc_largeq.py')

wl  = list(it.product(c3pt_tsep_list, c3pt_snk_desc_list, c3pt_tpol_list, c3pt_flav_list, c3pt_op_list))
print("# len(wl) =", len(wl))

for j_str in sys.argv[1:] : 
    j   = int(j_str)
    assert(0 <= j and j < len(wl))
    tsep, snk_desc, tpol, flav, op = wl[j]
    for sm_tag in sm_list :
        print("# dt=%d %s %s %s %s %s" % (tsep, snk_desc[0], tpol, flav, op, sm_tag))
        runme_bin_unbias_op(cfg_list, sm_tag, c3pt_ksrc_tag, c3pt_ksrc_q,
                tsep_list       = [tsep],
                snk_desc_list   = [snk_desc],
                tpol_list       = [tpol],
                flav_list       = [flav],
                op_list         = [op],
                VERBOSE=True)
    
