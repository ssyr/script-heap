#! /usr/bin/env python
from __future__ import print_function
from past.builtins import execfile
import sys, os

execfile('config_c23pt_boost1_xx_xy.py')
execfile('pproc_largeq.py')

wl  = list(it.product(c2pt_srcsnkdesc_list))
print("# len(wl) =", len(wl))

for j_str in sys.argv[1:] : 
    j   = int(j_str)
    assert(0 <= j and j < len(wl))
    srcsnkdesc, = wl[j]
    for sm_tag in sm_list :
        print("# %s %s %s" % (sm_tag, srcsnkdesc[0], srcsnkdesc[2]))
        runme_bin_unbias_hadspec(cfg_list, sm_tag, 
                srcsnkdesc_list = [srcsnkdesc],
                VERBOSE=True)

