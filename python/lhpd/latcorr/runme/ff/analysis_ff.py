from __future__ import print_function
import numpy as np
import h5py
from lhpd.aff_io import aff_key_bb_linkpath_str
from lhpd.pymath.H4_tensor import *
from lhpd.pymath.gamma_matr import gamma_dgr, gamma_id, gamma_dot
from lhpd.pymath.tensor import tensor_matrix_dot, matrix_tensor_dot
import itertools as it
from lhpd.latcorr import make_mom_combo_list

gamma = gamma_dgr

from lhpd.latcorr.ff_solver import *

def bb_linkpath_simplify(lp_, bb_dim=4) :
    """ get linkpath in the list format [ {[0 : 2D)}, ... ] and 
        simplify it by removing adjacent opposite links
    """
    lp  = list(lp_)
    i   = 1
    while i < len(lp) :
        if ((2 * bb_dim - lp[i - 1] + lp[i]) % (2 * bb_dim) == bb_dim) : 
            del lp[i - 1 : i + 1]
            #print("del lp[%d:%d] : %s" % (i-1, i+1, str(lp)))
            i   = max(1, i - 1)
        else : i += 1
    return lp

def calc_qbarDq(func_get_bb, gamma_list, n_deriv, 
                p3src, p3snk, src_snk_dt, 
                latsize, tdir, dir_deriv='symm', bb_dim=4, keep_nan=False) :
    """ compute 
        func_get_bb(gamma, lpath, p3src, p3snk, src_snk_dt)
                    return BB[i_data, t] for gamma, lpath
        return deriv[i_data, t, i_gamma, mu_1, ... mu_N]
                where (i_data, t) axes are "copied" from func_get_bb

        q3ext       3D momentum in lat.units
        keep_nan    mark undefined elements (ususally on border in t) with 'nan'
                    otherwise, these elements are replaced with zeros (default)

        NOTE if generalizing for other shapes returned by func_get_bb,
             make sure that the time axis is the last one

        TODO ? generalize for other than "cutoff" BC in time dir
    """
    q3ext = np.asarray(p3snk) - np.asarray(p3src)

    # qbar is at the origin ; add shifted value to make the derivative symmetric
    def bb_shift_from(y, mu, d, t_axis=-1) :
        """ for d=+1, return y(z+\hat\mu) 
            TODO mark absent values by nan and set them to zero at the end
        """
        if mu == tdir : # coordinate direction : shift along t_axis
            lt  = y.shape[t_axis]
            res = np.zeros_like(y)
            if 1 == d : 
                np_at(res, (t_axis, slice(0, lt-1)))[:] = np_at(y, (t_axis, slice(1, lt)))
                np_at(res, (t_axis, lt-1))[:] = np.nan
            elif -1 == d :
                
                np_at(res, (t_axis, slice(1, lt)))[:] = np_at(y, (t_axis, slice(0, lt-1)))
                np_at(res, (t_axis, 0))[:] = np.nan
            else : raise ValueError(d)
            return res
        else :  # Fourier-transformed direction : mult. by phase
            return x * np.exp(-2.j * np.pi / latsize[mu] * q3ext[mu] * d)
        
    n_gamma = len(gamma_list)
    x_shape = None
    t_axis  = None
    res     = None
    bb      = None
    for lpath in np.ndindex(*([2 * bb_dim] * n_deriv)) :
        lpath_str = aff_key_bb_linkpath_str(bb_linkpath_simplify(
                        lpath, bb_dim=bb_dim))
        muN_slice = tuple([ mu_dir % bb_dim for mu_dir in reversed(lpath) ])
        for i_g, gamma in enumerate(gamma_list) :
            x = func_get_bb(gamma, lpath_str, p3src, p3snk, src_snk_dt)
            if None is res : 
                x_shape = x.shape
                res = np.zeros(x_shape + (n_gamma,) + (bb_dim,) * n_deriv, 
                               dtype=x.dtype)

            sgn = 1
            for i, mu_dir in enumerate(lpath) :
                # qbar is at the origin
                # d is displacement of q wrt qbar in coordinate mu
                mu, d = mu_dir % bb_dim, 2 * (mu_dir / bb_dim) - 1
                assert (0 <= mu and mu < bb_dim)
                assert (-1 == d or 1 == d)
                sgn *= d
                if   'symm' == dir_deriv :
                    x = .5 * (x + bb_shift_from(x, mu, -d)) 
                elif 'right' == dir_deriv :
                    pass
                elif 'left' == dir_deriv :
                    x = -bb_shift_from(x, mu, -d)
                else : raise ValueError(dir_deriv)
                

            res[(Ellipsis, i_g) + muN_slice ] += sgn * x
    
    if not keep_nan : res = np.nan_to_num(res)

    return res / 2**n_deriv



def calc_op_qbarq(func_get_bb, op, ir_list, 
                  p3src, p3snk, src_snk_dt, 
                  latsize, tdir, bb_dim=4, keep_nan=False) :
    """
        ir_list  [(irrep_name, renorm_factor), ... ]
        return [i_data, t, i_comp]
        keep_nan    mark undefined elements (ususally on border in t) with 'nan'
                    otherwise, these elements are replaced with zeros (default)
    """
    res = None
    # no derivative ops
    if   'tensor0' == op :
        assert (1==len(ir_list) and 'H4_T0_d1r1' == ir_list[0][0])
        res = (calc_qbarDq(func_get_bb, [ 0 ], 0, p3src, p3snk, src_snk_dt, 
                            latsize, tdir, bb_dim=bb_dim, keep_nan=True)[..., 0]
                * ir_list[0][1])
    elif 'pstensor0' == op :
        assert (1==len(ir_list) and 'H4_T0_d1r1' == ir_list[0][0])
        res = (calc_qbarDq(func_get_bb, [ 15 ], 0, p3src, p3snk, src_snk_dt, 
                            latsize, tdir, bb_dim=bb_dim, keep_nan=True)[..., 0]
                * ir_list[0][1])
    elif 'tensor1' == op :
        assert (1==len(ir_list) and 'H4_T1_d4r1' == ir_list[0][0])
        res = (calc_qbarDq(func_get_bb, [ 1, 2, 4, 8 ], 0, p3src, p3snk, src_snk_dt, 
                            latsize, tdir, bb_dim=bb_dim, keep_nan=True)
                * ir_list[0][1])
    elif 'pstensor1' == op :
        assert (1==len(ir_list) and 'H4_T1_d4r1' == ir_list[0][0])
        res = (calc_qbarDq(func_get_bb, [ 14, 13, 11, 7 ], 0, p3src, p3snk, src_snk_dt, 
                            latsize, tdir, bb_dim=bb_dim, keep_nan=True)
                * [ 1, -1, 1, -1 ] * ir_list[0][1])
        
    # TODO add tensor charge
    else :
        
        # 1 & 2 deriv operators
        if   op in ['tensor2s2', 'pstensor2s2' ] : n_deriv = 1
        elif op in ['tensor3s3', 'pstensor3s3' ] : n_deriv = 2
        else : raise ValueError(op)

        if   op in ['tensor2s2', 'tensor3s3' ] :    gamma_list = [1, 2, 4, 8]
        elif op in ['pstensor2s2', 'pstensor3s3' ] :gamma_list = [14, 13, 11, 7 ]
        else : raise ValueError(op)
        # TODO add d1/d2

        y = calc_qbarDq(func_get_bb, gamma_list, n_deriv, p3src, p3snk, src_snk_dt, 
                        latsize, tdir, bb_dim=bb_dim, keep_nan=True)
        if op in ['pstensor2s2', 'pstensor3s3' ] :
            np_at(y, (-n_deriv-1, 1))[...] *= -1
            np_at(y, (-n_deriv-1, 3))[...] *= -1

        n_comp  = ir_list_comp_len(ir_list)
        y_dim   = len(y.shape)
        y_dim_d = y_dim - n_deriv - 1 # non-Lorentz indices (i_data, t, etc)
        res     = np.empty(y.shape[:y_dim_d] + (n_comp,), y.dtype)

        # transpose order 
        tr1     = np.r_[y_dim_d : y_dim, : y_dim_d]
        tr2     = np.r_[1 : y_dim_d+1, 0]
        
        sh_comp = 0
        for i, (ir_name, ir_scale) in enumerate(ir_list) :
            ir_dim = H4_repr_dim[ir_name]
            res[..., sh_comp : sh_comp + ir_dim] = (H4_repr_func[ir_name](
                            y.transpose(tr1)).transpose(tr2) * ir_scale)
            sh_comp += ir_dim
        assert(sh_comp == n_comp)

        # set border in t to zeros (although not all of the components are undef)
        for dt in range(n_deriv) :
            np_at(res, (1, dt))[...] = 0.
            np_at(res, (1, -dt-1))[...] = 0.


    if keep_nan : return res
    else : return np.nan_to_num(res)


def test_calc_qbarDq(
            ama_mode= ama_mode_list[0], 
            had     = 'proton_1',
            flav    = 'U',
            p3snk   = [0,0,0],
            tsep    = 10,
            gamma_list = [1,2,4,8],
            n_deriv = 1,
            p3src   = [0,0,1]
            ) :

    h5_bb = get_bb_dset(ama_mode, had, flav, p3snk, tsep) 
    p3src = np.asarray(p3src)
    p3snk = np.asarray(p3snk)
    q3ext = np.asarray(p3snk) - np.asarray(p3src)

    def func_get_bb(gamma, lpath_str, p3src_, p3snk_, src_snk_dt_) :
        assert ((p3src == p3src_).all())
        assert ((p3snk == p3snk_).all())
        assert (tsep  == src_snk_dt_)
        q3ext_ = np.asarray(p3snk_) - np.asarray(p3src_)

        i_l = np_find_first(lpath_str, h5_bb.attrs['linkpath_list'])[0]
        i_g = np_find_first(gamma, h5_bb.attrs['gamma_list'])[0]     
        i_q = np_find_first(q3ext_, h5_bb.attrs['qext_list'])[0]
        return h5_bb[:, i_g, i_l, i_q]
        
    return calc_qbarDq(func_get_bb, gamma_list, n_deriv, p3src, p3snk, tsep, 
                       latsize, t_axis)

def test_calc_op_qbarq(
            had, flav, op, 
            p3src, p3snk, tsep,
            ama_mode= ama_mode_list[0], 
            ) :
    """ run test as
( test_aff_op_qbarq('proton_3', 'U', 'tensor3s3', [1,-1,2], [0,0,0], 10) / 
 test_calc_op_qbarq('proton_3', 'U', 'tensor3s3', [1,-1,2], [0,0,0], 10))[:, 2:-2, :]
( test_aff_op_qbarq('proton_3', 'U', 'pstensor2s2', [1,-1,2], [0,0,0], 10) / 
 test_calc_op_qbarq('proton_3', 'U', 'pstensor2s2', [1,-1,2], [0,0,0], 10))[:, 2:-2, :]
    """

    h5_bb = get_bb_dset(ama_mode, had, flav, p3snk, tsep) 
    p3src = np.asarray(p3src)
    p3snk = np.asarray(p3snk)
    q3ext = np.asarray(p3snk) - np.asarray(p3src)

    def func_get_bb(gamma, lpath_str, p3src_, p3snk_, src_snk_dt_) :
        assert ((p3src == p3src_).all())
        assert ((p3snk == p3snk_).all())
        assert (tsep  == src_snk_dt_)
        q3ext_ = np.asarray(p3snk_) - np.asarray(p3src_)

        i_l = np_find_first(lpath_str, h5_bb.attrs['linkpath_list'])[0]
        i_g = np_find_first(gamma, h5_bb.attrs['gamma_list'])[0]     
        i_q = np_find_first(q3ext_, h5_bb.attrs['qext_list'])[0]
        return h5_bb[:, i_g, i_l, i_q]
        
    return calc_op_qbarq(func_get_bb, op, op_default_ir_list(op), p3src, p3snk, tsep, 
                       latsize, t_axis)
def test_aff_op_qbarq(
            had, flav, op, 
            p3src, p3snk, tsep,
            ir_list = None,
            ama_mode= ama_mode_list[0], 
            meas_spec = [ ('640', [0,0,0,0]) ]
            ) :
    print("TODO : FIX BB SIGNS !!!")
    if None is ir_list : ir_list = op_default_ir_list_aff(op)

    lt      = latsize[3]

    func_op_aff = make_aff_op_file_kpath_func(ama_mode)
    q3ext = np.asarray(p3snk) - np.asarray(p3src)
    
    n_data  = len(meas_spec)
    n_comp  = ir_list_comp_len(ir_list)
    res     = np.empty((n_data, tsep + 1, n_comp,), np.complex128)

    for i_data, (cfg_key, csrc) in enumerate(meas_spec):
        tsrc    = csrc[3]
        tsnk    = (tsrc + tsep) % lt
        aff_file, aff_kpath = func_op_aff(cfg_key, csrc, tsnk, p3snk, had, flav)
        aff_r = aff.Reader(aff_file)
        i_comp = 0
        for i_r, (ir_name, ir_scale) in enumerate(ir_list) :
            for op_comp in H4_repr_comp[ir_name] :
                res[i_data, :, i_comp] = (ir_scale *
                        np.array(aff_r.read('%s/%s' % (
                                    aff_kpath, 
                                    lhpd.aff_io.aff_key_hadron_3pt_tensor(
                                                csrc[3], csrc[:3], tsnk, p3snk, 
                                                 had, flav, q3ext, op, op_comp))))) 
                i_comp += 1

        aff_r.close()

    return res



def make_ff_eqnmat_f2f(gamma_pol, op, ir_list, mass, latsize, mom_combo_list) :
    """ compute equations and
        n_c3pt = len(mom_combo_list) * sum(dim(ir)) * 2
        return eq_mat[i_c3pt, i_ff], precond_mat[i_c3pt, i_eqn]
        NOTE renormalization factors in ir_list are IGNORED (they
            are meant to be applied to the lattice data)
    """
    """ # test as
Tpol_proton1 = gamma_dot((gamma_id + gamma[3])/2., 
                         gamma_id - 1j * gamma_dot(gamma[0], gamma[1]))
op = 'tensor2s2'
mn = .5
mcgrp = make_mom_combo_list(mn, latsize, [[0,0,0],[-1,0,0]], list(lhpd.int_vectors_maxnorm(3, 10)))
make_ff_eqnmat_f2f(Tpol_proton1, op, op_default_ir_list(op), mn, latsize, mcgrp[0.0])
    """
    n_comp = ir_list_comp_len(ir_list)
    n_mc = len(mom_combo_list)
    n_ff = lhpd.latcorr.get_gff_number(op)
    n_c3pt= n_mc * n_comp * 2
    # separate Re/Im
    eq_mat = np.empty((n_mc, n_comp, 2, n_ff), np.float64)
    for i_mc, (p3src, p3snk) in enumerate(mom_combo_list) :
        p4_in   = lorentz_p4(mass, latsize, p3src)
        p4_out  = lorentz_p4(mass, latsize, p3snk)

        mat = lhpd.latcorr.ff_matrix_f2f(mass, p4_in, gamma_pol, mass, p4_out, gamma_pol, op, gamma_pol)
        #print(mat)
        ir_sh = 0
        for (ir_name, ir_scale) in ir_list:
            ir_dim = H4_repr_dim[ir_name]
            mat_ir = H4_repr_func[ir_name](mat)
            #print(ir_name, mat_ir)
            eq_mat[i_mc, ir_sh : ir_sh + ir_dim, 0] = mat_ir.real
            eq_mat[i_mc, ir_sh : ir_sh + ir_dim, 1] = mat_ir.imag
            ir_sh += ir_dim
        assert(ir_sh == n_comp)

    def make_mom_std(p) : 
        p = np.abs(p)
        p.sort()
        return p

    mc_std_list = [ np.r_[ make_mom_std(mc[0]), make_mom_std(mc[1]) ]
                    for mc in mom_combo_list ]
    precond_coeff = []          # [i_eqn][j] -> coeff from eq_mat
    precond_i3pt  = []          # [i_eqn][j] -> i_3pt from eq_mat
    precond_norm2 = []       # [i_eqn][j] -> i_3pt norm
    n_eqn = 0

    for (i_mc, i_comp, i_c) in np.ndindex(n_mc, n_comp, 2) :
        n2_i = (eq_mat[i_mc, i_comp, i_c]**2).sum()
        if n2_i <= NONZERO_TOL : continue # drop zero equations

        coeff = None
        (k_mc, k_comp, k_c) = None, None, None
        for k_eqn in range(n_eqn) :
            assert(0 < len(precond_i3pt[k_eqn]))
            (k_mc, k_comp, k_c) = precond_i3pt[k_eqn][0]
            # pairwise comparison
            if k_c != i_c : continue
            if (mc_std_list[i_mc] != mc_std_list[k_mc]).any() : continue
            xy  = (np.dot(eq_mat[i_mc, i_comp, i_c], eq_mat[k_mc, k_comp, k_c]) 
                        / math.sqrt(n2_i * precond_norm2[k_eqn][0]))

            if (lhpd.rdiff(abs(xy), 1.) < NONZERO_TOL 
                and lhpd.rdiff(n2_i, precond_norm2[k_eqn][0]) < NONZERO_TOL) :
                coeff = xy
                precond_i3pt[k_eqn].append((i_mc, i_comp, i_c))
                precond_coeff[k_eqn].append(coeff)
                precond_norm2[k_eqn].append(n2_i)
            
        if None is coeff :
            precond_i3pt.append([(i_mc, i_comp, i_c)])
            precond_coeff.append([1.])
            precond_norm2.append([n2_i])
            n_eqn += 1

    assert(0 < n_eqn)
    assert( len(precond_i3pt) == n_eqn
        and len(precond_coeff) == n_eqn
        and len(precond_norm2) == n_eqn)

    precond_mat = np.zeros((n_mc, n_comp, 2, n_eqn), np.float64)
    for k_eqn in range(n_eqn) : 
        for j in range(len(precond_coeff[k_eqn])) : 
            precond_mat[precond_i3pt[k_eqn][j]][k_eqn] = precond_coeff[k_eqn][j]

    #return eq_mat.reshape(n_c3pt, n_ff), precond_mat.reshape(n_c3pt, n_eqn)
    return eq_mat, precond_mat


def print_ff_eqnmat_f2f(op, ir_list, mcgrp, eqn_mat, precond_mat) :
    n_mc    = len(mcgrp)
    n_comp  = ir_list_comp_len(ir_list)
    n_ff    = lhpd.latcorr.get_gff_number(op)
    n_eqn   = precond_mat.shape[-1]
    assert eqn_mat.shape == (n_mc, n_comp, 2, n_ff)
    assert precond_mat.shape == (n_mc, n_comp, 2, n_eqn)

    ir_comp_str = []
    for ir_name, ir_scale in ir_list : 
        ir_comp_str.extend(H4_repr_comp[ir_name])

    for (i_mc, i_comp, i_c, i_eqn) in np.asarray(np.where(0 != precond_mat)).T :
        print('[%3d]  %s<%s|%s_%s|%s> = %s*FF' % (
                i_eqn, ['Re', 'Im'][i_c], mom3_str(mcgrp[i_mc][1]), 
                op, ir_comp_str[i_comp], mom3_str(mcgrp[i_mc][0]), 
                str(eqn_mat[i_mc, i_comp, i_c])))

# compute op's and store in an HDF5 file
def calc_save_op(h5_file, h5_kpath, 
                had, flav, op, ir_name, 
                p3snk, tsep,
                latsize, t_axis, h5_bb) :
    """
        h5_file     open HDF5 h5py file
        h5_kpath    kpath relative to root
    """
    p3snk = np.asarray(p3snk)
    assert (p3snk == h5_bb.attrs['sink_mom']).all()
    assert (tsep == h5_bb.attrs['source_sink_dt'])
    assert (had == h5_bb.attrs['source_hadron'])
    assert (had == h5_bb.attrs['sink_hadron'])

    n_data  = h5_bb.shape[0]
    n_qext  = h5_bb.shape[-2]
    n_tau   = h5_bb.shape[-1]
    
    def func_get_bb(gamma, lpath_str, p3src_, p3snk_, src_snk_dt_) :
        assert ((p3snk == p3snk_).all())
        assert (tsep  == src_snk_dt_)
        p3src = np.asarray(p3src_)
        q3ext_ = p3snk - p3src

        i_l = np_find_first(lpath_str, h5_bb.attrs['linkpath_list'])[0]
        i_g = np_find_first(gamma, h5_bb.attrs['gamma_list'])[0]     
        i_q = np_find_first(q3ext_, h5_bb.attrs['qext_list'])[0]
        return h5_bb[:, i_g, i_l, i_q]

    n_comp = H4_repr_dim[ir_name]
    op_shape = (n_data, n_comp, n_qext, n_tau)
    # [i_data, i_comp, i_qext, i_tau]
    h5_op = h5_file.require_dataset(h5_kpath, op_shape, np.complex128, 
                    fletcher32=True)
    
    for i_qext, q3ext in enumerate(h5_bb.attrs['qext_list']) :
        p3src = p3snk - q3ext
        h5_op[:, :, i_qext, :] = calc_op_qbarq(func_get_bb, op, [(ir_name, 1.)], 
                    p3src, p3snk, tsep, latsize, t_axis).transpose((0,2,1))

    lhpd.h5_io.h5_set_datalist(h5_op, lhpd.h5_io.h5_get_datalist(h5_bb))
    h5_op.attrs['dim_spec'] = np.array(['i_data', 'i_comp', 'i_qext', 'i_tau' ],
                                       dtype='S32')
    h5_op.attrs['qext_list'] = h5_bb.attrs['qext_list']
    h5_op.attrs['comp_list'] = H4_repr_comp[ir_name]
    h5_op.attrs['tau_list']  = h5_bb.attrs['tau_list']

    # other parameters
    h5_op.attrs['source_sink_dt']   = h5_bb.attrs['source_sink_dt']
    h5_op.attrs['source_hadron']    = h5_bb.attrs['source_hadron']
    h5_op.attrs['sink_hadron']      = h5_bb.attrs['sink_hadron']
    h5_op.attrs['sink_mom']         = h5_bb.attrs['sink_mom']
    h5_op.attrs['time_neg']         = h5_bb.attrs['time_neg']



def runme_calc_save_op(ama_mode,
                  had_list=had_list_c3pt, 
                  flav_list=flav_list, 
                  psnk_list=psnk_list_c3pt,
                  src_snk_dt_list=src_snk_dt_list,
                  op_list=op_list) :
    """ cycle over correlators """
    for had in had_list :
        for flav in flav_list :
            for op in op_list :
                ir_list = op_default_ir_list(op)

                for tsep in src_snk_dt_list :
                    h5_fname= 'op-%s.%s.%s.%s.dt%d.h5' % (
                                ama_mode_desc(ama_mode), had, op, flav, tsep)
                    h5_file = h5py.File(h5_fname, 'a')

                    for psnk in psnk_list :
                        h5_bb = get_bb_dset(ama_mode, had, flav, psnk, tsep) 

                        for ir_name, ir_scale in ir_list :
                            print('# ', had, flav, psnk, tsep, op, ir_name)
                            h5_kpath= '/%s/%s/%s/PX%dPY%dPZ%d_DT%d/%s' % (
                                        had, op, flav,
                                        psnk[0], psnk[1], psnk[2],
                                        tsep, ir_name)
                            calc_save_op(h5_file, h5_kpath, 
                                         had, flav, op, ir_name, 
                                         psnk, tsep, 
                                         latsize, t_axis, h5_bb)

                    h5_file.close()



"""
merge_list = [( '../run3+4/op-exact.proton_1.tensor1.U.dt10.h5', 
                '/proton_1/tensor1/U/PX0PY0PZ0_DT10/H4_T1_d4r1'),
              ( './op-exact.proton_1.tensor1.U.dt10.h5', 
                '/proton_1/tensor1/U/PX0PY0PZ0_DT10/H4_T1_d4r1')]


"""


def runme_merge_op(ama_mode, dir_list=[],
                  had_list=had_list_c3pt, 
                  flav_list=flav_list, 
                  psnk_list=psnk_list_c3pt,
                  src_snk_dt_list=src_snk_dt_list,
                  op_list=op_list) :
    """ cycle over correlators """
    for had in had_list :
        for flav in flav_list :
            for op in op_list :
                ir_list = op_default_ir_list(op)

                for tsep in src_snk_dt_list :
                    h5_fname= 'op-%s.%s.%s.%s.dt%d.h5' % (
                                ama_mode_desc(ama_mode), had, op, flav, tsep)

                    for psnk in psnk_list :

                        for ir_name, ir_scale in ir_list :
                            print('# ', had, flav, psnk, tsep, op, ir_name)
                            h5_kpath= '/%s/%s/%s/PX%dPY%dPZ%d_DT%d/%s' % (
                                        had, op, flav,
                                        psnk[0], psnk[1], psnk[2],
                                        tsep, ir_name)
                            h5_list = [ ('%s/%s' % (d, h5_fname), h5_kpath) for d in dir_list ]
                            h5file_merge(h5_fname, h5_kpath, h5_list)


def runme_merge_clover(ama_mode, dir_list=[],
                  had_list=had_list_c3pt, 
                  flav_list=flav_list, 
                  psnk_list=psnk_list_c3pt,
                  src_snk_dt_list=src_snk_dt_list) :
    """ cycle over correlators """
    for had in had_list :
        for flav in flav_list :

            for tsep in src_snk_dt_list :
                h5_fname= 'clover3pt-%s.%s.%s.dt%d.h5' % (
                            ama_mode_desc(ama_mode), had, flav, tsep)

                for psnk in psnk_list :

                    print('# ', had, flav, psnk, tsep, )
                    h5_kpath= '/%s/%s/PX%dPY%dPZ%d_DT%d' % (
                                had, flav,
                                psnk[0], psnk[1], psnk[2],
                                tsep)
                    h5_list = [ ('%s/%s' % (d, h5_fname), h5_kpath) for d in dir_list ]
                    h5file_merge(h5_fname, h5_kpath, h5_list)

def runme_merge_hadspec(ama_mode, dir_list=[], had_list=had_list) : 
    for had in had_list :
        h5_fname = ('hadspec-%s.%s.h5' % (ama_mode_desc(ama_mode), had))
        h5_file = h5py.File(h5_fname, 'a')

        print('# ', had)
        h5_kpath= '/%s' %(had,)
        h5_list = [ ('%s/%s' % (d, h5_fname), h5_kpath) for d in dir_list ]

        h5file_merge(h5_fname, h5_kpath, h5_list)

        # finalize
        h5_file.flush()
        h5_file.close()



def calc_ff(had, gamma_pol, flav, op, ir_list, mhad, mcgrp,
            tsep_list, method_list, 
            rsplan = ('jk', 1), apponly=False) :
    """ this is a long ugly function to compute formfactors;
        temporary implementation, will be organized into subfunctions as will be seen fit
        (although likely to stay forever)

        method_list = [ (name, param1, ...), ...]
                    methods to extract the ground state

        func_get_op     (ama_mode, had, flav, op, ir_list, p3src, p3snk, tsep) -> c3pt[i_data,tau,i_comp]
        func_get_c2pt   (ama_mode, had, p3) -> c2pt[i_data, t]

        return formfactors[data][i_method][i_ff]
    """
    h5f_map_ex = {}
    h5f_map_ap = {}
    psnk_list = [ list(x) for x in set([ tuple(mc[1]) for mc in mcgrp ]) ]
    for fl in ['U', 'D'] :
        for tsep in tsep_list :
            for psnk in psnk_list :
                for ir_name, ir_scale in ir_list :
                    h5f_map_ex[(fl, tsep, tuple(psnk), ir_name)] = get_op_dset(
                                    ama_exact, had, fl, op, psnk, tsep, ir_name)
                    h5f_map_ap[(fl, tsep, tuple(psnk), ir_name)] = get_op_dset(
                                    ama_apprx, had, fl, op, psnk, tsep, ir_name)
                    

    
    def func_get_op(ama_mode, had, flav, op, ir_list, p3src, p3snk, tsep) :
        if 'U-D' == flav : 
            return (func_get_op(ama_mode, had, 'U', op, ir_list, p3src, p3snk, tsep)
                  - func_get_op(ama_mode, had, 'D', op, ir_list, p3src, p3snk, tsep))
        if 'U+D' == flav : 
            return (func_get_op(ama_mode, had, 'U', op, ir_list, p3src, p3snk, tsep)
                  + func_get_op(ama_mode, had, 'D', op, ir_list, p3src, p3snk, tsep))

        qext = np.asarray(p3snk) - np.asarray(p3src)
        n_comp = ir_list_comp_len(ir_list)
        n_data = None
        res = None
        sh_comp = 0
        for ir_name, ir_scale in ir_list:
            # h5_op[i_data, i_comp, i_qext, tau]
            if ama_mode == ama_exact : 
                h5_op = h5f_map_ex[(flav, tsep, tuple(p3snk), ir_name)]
            elif ama_mode == ama_apprx : 
                h5_op = h5f_map_ap[(flav, tsep, tuple(p3snk), ir_name)]
            else : raise ValueError(ama_mode)
            i_q = np_find_first(qext, h5_op.attrs['qext_list'])[0]
            if None is res :
                n_data = h5_op.shape[0]
                res = np.empty((n_data, 1+tsep, n_comp), h5_op.dtype)
            ir_dim = H4_repr_dim[ir_name]
            # res[i_data, tau, i_comp]
            res[:, :, sh_comp : sh_comp + ir_dim] = ir_scale * h5_op[:, :, i_q, :].transpose(0, 2, 1)
            sh_comp += ir_dim
        return res
    
    h5d_c2pt_ex = get_c2pt_dset(ama_exact, had)
    h5d_c2pt_ap = get_c2pt_dset(ama_apprx, had)
    def func_get_c2pt(ama_mode, had, p3src) :
        if   ama_mode == ama_exact : h5d = h5d_c2pt_ex
        elif ama_mode == ama_apprx : h5d = h5d_c2pt_ap
        else : raise ValueError(ama_mode)
        i_p = np_find_first(p3src, h5d.attrs['psnk_list'])[0]
        return h5d[:, i_p]

    n_comp  = ir_list_comp_len(ir_list)
    n_mc    = len(mcgrp)
    tmax    = max(tsep_list) + 1            # FIXME if fit c2pt, may need to plug other value
    def get_c3pt_mclist(ama_mode, tsep) :
        n_data = res = None
        for i_mc, (p3src, p3snk) in enumerate(mcgrp) :
            # [i_data, tau, i_comp]
            x = func_get_op(ama_mode, had, flav, op, ir_list, p3src, p3snk, tsep)
            if None is res :
                n_data = x.shape[0]
                # [i_data, tau, i_mc, i_comp, i_c]
                res = np.empty((n_data, 1+tsep, n_mc, n_comp, 2), np.float64)
            res[:, :, i_mc, :, 0] = x.real
            res[:, :, i_mc, :, 1] = x.imag
        return res

    def get_c2pt_src_snk(ama_mode):
        n_data = res_src = res_snk = None
        for i_mc, (p3src, p3snk) in enumerate(mcgrp) :
            # [i_data, t]
            x = func_get_c2pt(ama_mode, had, p3src)
            y = func_get_c2pt(ama_mode, had, p3snk)
            if None is res_src or None is res_snk :
                n_data = x.shape[0]
                res_src = np.empty((n_data, tmax, n_mc), np.float64)
                res_snk = np.empty((n_data, tmax, n_mc), np.float64)
            res_src[:, :, i_mc] = x[:, :tmax].real
            res_snk[:, :, i_mc] = y[:, :tmax].real
        return res_src, res_snk

    def corr_bias_rs(x_ex, x_ap, apponly=apponly) : 
        if apponly : x = x_ap
        else : x = make_ama_dataset_simple(
                        x_ex, n_ex_cfg, x_ap, n_ap_cfg, match_ex_ap)
        return lhpd.resample(x, rsplan)

    def apply_eqn_prec(c2a, c2b, c3ba, prec_mat) :
        """ return preconditioned versions of c2, c3 """
        n_eqn   = prec_mat.shape[-1]
        res_c2a = np.zeros(c2a.shape[0:2] + (n_eqn,), c2a.dtype)
        res_c2b = np.zeros(c2b.shape[0:2] + (n_eqn,), c2b.dtype)
        res_c3ba= dict([ (tsep, np.zeros(c3ba[tsep].shape[0:2] + (n_eqn,), c3ba[tsep].dtype)) 
                         for tsep in tsep_list ])
        for i_mc, i_comp, i_c in np.ndindex(n_mc, n_comp, 2) :
            for i_eqn in range(n_eqn) :
                f   = prec_mat[i_mc, i_comp, i_c, i_eqn]
                if 0. == f : continue
                res_c2a[:, :, i_eqn] += abs(f) * c2a[:, :, i_mc]
                res_c2b[:, :, i_eqn] += abs(f) * c2b[:, :, i_mc]
                for tsep in tsep_list :
                    res_c3ba[tsep][:, :, i_eqn]+= f * c3ba[tsep][:, :, i_mc, i_comp, i_c]
        return res_c2a, res_c2b, res_c3ba


    # [i_mc, n_comp, 2, i_ff], [i_mc, i_comp, 2, i_eqn]
    (eqn_mat, prec_mat) = make_ff_eqnmat_f2f(gamma_pol, op, ir_list, mhad, latsize, mcgrp)
    n_c3pt  = n_mc * n_comp * 2
    n_ff    = eqn_mat.shape[-1]
    n_eqn   = prec_mat.shape[-1]
    

    #### collect all relevant correlator data
    # [i_data, t, i_mc]
    c2pt_src_ap, c2pt_snk_ap = get_c2pt_src_snk(ama_apprx)
    # [i_data, t, i_mc]
    c2pt_src_ex, c2pt_snk_ex = get_c2pt_src_snk(ama_exact)
    # {tsep -> c3pt[i_data, tau, i_mc, i_comp, i_c]
    c3pt_map_ap = dict([ (tsep, get_c3pt_mclist(ama_apprx, tsep))
                         for tsep in tsep_list ])
    c3pt_map_ex = dict([ (tsep, get_c3pt_mclist(ama_exact, tsep))
                         for tsep in tsep_list ])

    #### combine apprx + exact and resample
    c2pt_src_rs = corr_bias_rs(c2pt_src_ex, c2pt_src_ap)
    c2pt_snk_rs = corr_bias_rs(c2pt_snk_ex, c2pt_snk_ap)
    c3pt_map_rs = dict([ 
            (tsep, corr_bias_rs(c3pt_map_ex[tsep], c3pt_map_ap[tsep]))
            for tsep in tsep_list ])

    #### resample apprx-only for covariance calculations later
    c2pt_src_ap_rs = corr_bias_rs(c2pt_src_ex, c2pt_src_ap, apponly=True)
    c2pt_snk_ap_rs = corr_bias_rs(c2pt_snk_ex, c2pt_snk_ap, apponly=True)
    c3pt_map_ap_rs = dict([ 
            (tsep, corr_bias_rs(c3pt_map_ex[tsep], c3pt_map_ap[tsep], apponly=True))
            for tsep in tsep_list ])

    
    ### compute preconditioned c3pt, c2pt for combined and app-only
    c2pt_src_ap_rs_pc, c2pt_snk_ap_rs_pc, c3pt_map_ap_rs_pc = apply_eqn_prec(
                c2pt_src_ap_rs, c2pt_snk_ap_rs, c3pt_map_ap_rs, prec_mat)

    c2pt_src_rs_pc, c2pt_snk_rs_pc, c3pt_map_rs_pc = apply_eqn_prec(
                c2pt_src_rs, c2pt_snk_rs, c3pt_map_rs, prec_mat)


    

    def calc_me_ratio(c2a, c2b, c3ba, tsep) :
        """ ASSUME time axis=1 """
        sqf = np.sqrt(np.abs(  c2b[:, : tsep+1] / c2a[:, : tsep+1]
                               * c2a[:, tsep::-1] / c2b[:, tsep::-1] ))
        return (c3ba / np.sqrt(np.abs(c2b[:, tsep] * c2a[:, tsep]))[:,None] * sqf)
    def calc_me_ratio_all(c2a, c2b, c3ba_map) : 
        return dict([ (tsep, calc_me_ratio(c2a, c2b, c3ba_map[tsep], tsep))
                      for tsep in c3ba_map.keys() ]) 

    # FIXME this is XYINA ; preconditioner applied to 3pt and 2pt, so their ratio should be solved against the same system of equations ; however, below each equation (matrix row) is multiplied by the number of times it occurs congruently in the system
    prec_fact= np.abs(prec_mat.reshape(n_c3pt, n_eqn)).sum(axis=0)
    eqn_prec = np.dot(prec_mat.reshape(n_c3pt, n_eqn).T, 
                      eqn_mat.reshape(n_c3pt, n_ff)) / prec_fact[:,None]
    #print_ff_eqnmat_f3f(op, ir_list, mcgrp, eqn_mat, prec_mat)
    # me[tsep][i_data, tau, i_eqn]
    me_rs   = calc_me_ratio_all(c2pt_src_rs_pc, c2pt_snk_rs_pc, c3pt_map_rs_pc)
    me_ap_rs= calc_me_ratio_all(c2pt_src_ap_rs_pc, c2pt_snk_ap_rs_pc, c3pt_map_ap_rs_pc)
    n_data  = me_rs[tsep_list[0]].shape[0]
    n_data_ap= me_ap_rs[tsep_list[0]].shape[0]

    def calc_ratio_pltx(tsep) :
        """ ff on timeslices
            return ff[n_data, 1+tsep, n_ff] """
        res = np.empty((me_rs[tsep].shape[0], 1+tsep, n_ff), np.float64)
        for tau in range(1+tsep) :
            # solve od: [i_eqn,i_ff] for tau
            cov_ap  = lhpd.calc_cov(me_ap_rs[tsep][:,tau], rsplan)
            cov_inv = lhpd.fitter.cov_invert(cov_ap, rmin=1e-4)
            sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqn_prec, cov_inv)
            res[:,tau,:] = matrix_tensor_dot(sol_mat, me_rs[tsep][:,tau,:], 1)
        return res

    def calc_ratio_pltx_avg(tsep, max_tslice=3) :
        """ ff from ratio pltx center avg
            return ff[n_data, 1, n_ff] """
        assert(tsep in c3pt_map_rs)
        t1, t2 = 0, tsep + 1
        while max_tslice < (t2 - t1) :
            t1 += 1
            t2 -= 1
        # FIXME this is 'regression test'; replace code above with this function
        assert(lhpd.latcorr.pltx_trange(tsep, max_tslice) == (t1, t2))
        
        res     = np.empty((me_rs[tsep].shape[0], 1, n_ff), np.float64)
        # solve od: [i_eqn,i_ff] for tau
        cov_ap  = lhpd.calc_cov(me_ap_rs[tsep][:, t1:t2].mean(axis=1), 
                                         rsplan)
        cov_inv = lhpd.fitter.cov_invert(cov_ap, rmin=1e-4)
        sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqn_prec, cov_inv)
        me_rs_avg = me_rs[tsep][:, t1:t2, :].mean(axis=1)
        res[:, 0, :] = matrix_tensor_dot(sol_mat, me_rs_avg, 1)
        return res

    def calc_ratio_summ(tsep_list, tskip=2) :
        """ ff from summation-extrapolated matrix elements
            return ff[n_data, 1, n_ff] """
        tsep_list   = np.asarray(tsep_list)
        n_tsep      = len(tsep_list)
        
        me_summ     = np.empty((n_data, n_tsep, n_eqn), np.float64)
        me_summ_ap  = np.empty((n_data_ap, n_tsep, n_eqn), np.float64)
        for i_tsep, tsep in enumerate(tsep_list) :
            me_summ[:, i_tsep]      = me_rs[tsep][:, tskip : 1 + tsep - tskip].sum(axis=1)
            me_summ_ap[:, i_tsep]   = me_ap_rs[tsep][:, tskip : 1 + tsep - tskip].sum(axis=1)
        
        # solve od : [i_tsep, {coeff, intercept}]
        me_gs       = np.empty((n_data, n_eqn), np.float64)
        me_gs_ap    = np.empty((n_data_ap, n_eqn), np.float64)
        eqn_summ = np.r_[ [tsep_list], [[1.] * n_tsep] ].T
        for i_eqn in range(n_eqn) :
            me_summ_cov = lhpd.calc_cov(me_summ_ap[:, :, i_eqn], rsplan)
            cov_inv     = lhpd.fitter.cov_invert(me_summ_cov, rmin=1e-4)
            sol_summ, sol_undef = lhpd.fitter.linsolver_od_matr(eqn_summ, cov_inv)
            me_gs[:, i_eqn]     = matrix_tensor_dot(sol_summ, me_summ[:, :, i_eqn], 1)[:, 0]
            me_gs_ap[:, i_eqn]  = matrix_tensor_dot(sol_summ, me_summ_ap[:, :, i_eqn], 1)[:, 0]

        # solve od : [i_eqn, i_ff]
        res         = np.empty((n_data, 1, n_ff), np.float64)
        me_gs_cov   = lhpd.calc_cov(me_gs_ap, rsplan)
        cov_inv     = lhpd.fitter.cov_invert(me_gs_cov, rmin=1e-4)
        sol_ff, sol_undef = lhpd.fitter.linsolver_od_matr(eqn_prec, cov_inv)
        res[:, 0, :]= matrix_tensor_dot(sol_ff, me_gs, 1)
        return res
            
    # TODO calc_gpof
    # TODO calc_exp_fit


    # put plateaus:tsep->x, plateau averages:tsep->x, summation
    all_res = []
    for m in method_list :
        if   'ratio_pltx' == m[0] : 
            for tsep in tsep_list :
                all_res.append(calc_ratio_pltx(tsep))
        elif 'ratio_pltx_avg' == m[0] :
            for tsep in tsep_list :
                all_res.append(calc_ratio_pltx_avg(tsep))
        elif 'ratio_summ' == m[0] :
            all_res.append(calc_ratio_summ(tsep_list))
        else : raise ValueError(m[0])

    return all_res

def calc_save_ff(h5_file, h5_kpath,
            had, gamma_pol, flav, op, ir_list, mhad, mcgrp_list,
            tsep_list, method_list,
            rsplan = ('jk', 1), apponly=False) :
    n_q2 = len(mcgrp_list)
    n_data = None
    n_ff = lhpd.latcorr.get_gff_number(op)
    all_ff = []
    q2_list= []
    for i_q2, mcgrp in enumerate(mcgrp_list) :
        mc0 = mcgrp[0]
        q2 = lorentz_mom_transfer_sq(mhad, latsize, mc0[0], mc0[1])
        print(mc0, "Q2=", q2)
        q2_list.append(q2)
        all_ff.append(calc_ff(had, gamma_pol, flav, op, ir_list, mhad, mcgrp,
                              tsep_list, method_list,
                              rsplan = rsplan, apponly=apponly))

    q2_list = np.asarray(q2_list)
    n_data = all_ff[0][0].shape[0]
    assert (all_ff[0][0].shape[-1] == n_ff)

    def save_ff(dset, i) :
        dset.attrs['q2_list'] = q2_list
        for i_q2, q2 in enumerate(q2_list) :
            # [i_data, i_t, i_q2, i_ff]
            dset[:, :, i_q2, :] = all_ff[i_q2][i]

    cnt = 0
    for m in method_list :
        if   'ratio_pltx' == m[0] :
            for tsep in tsep_list :
                ff_shape = (n_data, 1 + tsep, n_q2, n_ff)
                k = '%s/ratio_pltx/dt%d' % (h5_kpath, tsep)
                h5_ff = h5_file.require_dataset(k, ff_shape, np.float64, 
                                                fletcher32=True)
                save_ff(h5_ff, cnt)
                cnt += 1
        elif 'ratio_pltx_avg' == m[0] :
            for tsep in tsep_list :
                ff_shape = (n_data, 1, n_q2, n_ff)
                k = '%s/ratio_pltx_avg/dt%d' % (h5_kpath, tsep)
                h5_ff = h5_file.require_dataset(k, ff_shape, np.float64, 
                                                fletcher32=True)
                save_ff(h5_ff, cnt)
                cnt += 1
        elif 'ratio_summ' == m[0] :
            ff_shape = (n_data, 1, n_q2, n_ff)
            k = '%s/ratio_summ' % (h5_kpath)
            h5_ff = h5_file.require_dataset(k, ff_shape, np.float64, 
                                            fletcher32=True)
            save_ff(h5_ff, cnt)
            cnt += 1
            
def runme_calc_save_ff(h5_fname,
            had, gamma_pol, mhad, 
            psnk_list=psnk_list_c3pt,
            qext2_max=qext2_max,
            tsep_list=src_snk_dt_list, 
            flav_list=['U-D'], 
            op_list=['tensor1', 'pstensor1', 'tensor2s2'], 
            rsplan = ('jk', 1), apponly=False) :
    """ compute form factors w/o renormalization """
    method_list = [('ratio_pltx',), ('ratio_pltx_avg',), ('ratio_summ',)]
    h5f = h5py.File(h5_fname, 'a') # 'append' mode does not work?
    mc_dict = make_mom_combo_list(mhad, latsize, psnk_list, 
                                  list(lhpd.int_vectors_maxnorm(3, qext2_max)))
    q2_list = sorted(mc_dict.keys())
    mcgrp_list = [ mc_dict[q2] for q2 in q2_list ]

    for flav in flav_list :
        for op in op_list :
            ir_list = op_default_ir_list(op)
            h5k = '/%s/%s/%s' % (had, op, flav)
            calc_save_ff(h5f, h5k, had, gamma_pol, flav, op, ir_list, 
                         mhad, mcgrp_list,
                         tsep_list, method_list,
                         rsplan=rsplan, apponly=apponly)

    h5f.close()



def plot_pltx_cmp(dgrp_ff, i_ff, i_q2, tsep_list, rsplan=('jk',1),
            ylabel=None, yr=None, xlabel=r'$\tau$', xr=None, x_sh=None,
            ax=None, stg_list=dv.style_group_default, stg_summ=dv.style_group_default[-1]) :
    """ quick & dirty plotting of plateaus pltx averages and methods """
    if None is ax : ax = dv.make_std_axes()

    max_tslice  = 3    # FIXME must be a parameter to ratio_pltx_avg, ratio_summ

    n_tsep = len(tsep_list)
    tmax = max(tsep_list)
    # plot ratio_pltx, ratio_pltx_avg
    if None is x_sh : x_sh = .4 / max(1, n_tsep - 1)
    
    # bands
    for i_tsep, tsep in enumerate(tsep_list) :
        y2  = dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)][:, 0, i_q2, i_ff]
        y2_a, y2_e = lhpd.calc_avg_err(y2, rsplan)
        tr2 = np.r_[lhpd.latcorr.pltx_trange(tsep, max_tslice)]
        tr2[-1] -= 1
        ax.fill_between(tr2 + i_tsep * x_sh, [y2_a - y2_e] * 2, [y2_a + y2_e] *2, 
                        **stg_list[i_tsep].band())
        ax.plot(tr2 + i_tsep * x_sh, [y2_a]*2, **stg_list[i_tsep].line())

    # plateaus
    for i_tsep, tsep in enumerate(tsep_list) :
        tr  = np.r_[0 : 1+tsep]
        y   = dgrp_ff['ratio_pltx/dt%d' % (tsep)][:, :, i_q2, i_ff]
        y_a, y_e = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(tr + i_tsep * x_sh, y_a, yerr=y_e, label=(r'$T=%da$' % tsep), 
                    **stg_list[i_tsep].edotline())
    
    # summation res
    ys = dgrp_ff['ratio_summ'][:, 0, i_q2, i_ff]
    ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
    ax.errorbar([-1.], ys_a, yerr=ys_e, label='summ', **stg_summ.edot())
    
    if None is xr : xr = [-1.5, 1+tsep]
    ax.set_xlim(xr)
    ax.set_xlabel(r'$\tau$')
    if not None is  yr : ax.set_ylim(yr)
    if None is ylabel: ylabel = dgrp_ff.name
    ax.set_ylabel(ylabel)

    ax.legend()

    return ax


def plot_ratiopltx_cmp(
            dgrp_ff1, i_ff1, i_q2_1, 
            dgrp_ff2, i_ff2, i_q2_2,
            tsep_list, rsplan=('jk',1),
            ylabel=None, yr=None, 
            xlabel=r'$\tau$', xr=None, x_sh=None,
            ax=None, stg_list=dv.style_group_default, stg_summ=dv.style_group_default[-1]) :
    """ quick & dirty plotting of plateaus pltx averages and methods 
        TODO : universal function plot : (func{x,y,...}, [(grp_x, i_ff_x, i_q2_x), ...], ...
    """
    if None is ax : ax = dv.make_std_axes()

    max_tslice  = 3    # FIXME must be a parameter to ratio_pltx_avg, ratio_summ

    n_tsep = len(tsep_list)
    tmax = max(tsep_list)
    # plot ratio_pltx, ratio_pltx_avg
    if None is x_sh : x_sh = .4 / max(1, n_tsep - 1)
    
    # bands
    for i_tsep, tsep in enumerate(tsep_list) :
        dset1   = dgrp_ff1['ratio_pltx_avg/dt%d' % (tsep)]
        dset2   = dgrp_ff2['ratio_pltx_avg/dt%d' % (tsep)]
        rsp     = h5dset_check_rsplan(dset1, dset2, rsplan=rsplan)
        y2      = dset1[:, 0, i_q2_1, i_ff1] / dset2[:, 0, i_q2_2, i_ff2]
        y2_a, y2_e = lhpd.calc_avg_err(y2, rsp)
        tr2     = np.r_[lhpd.latcorr.pltx_trange(tsep, max_tslice)]
        tr2[-1] -= 1
        ax.fill_between(tr2 + i_tsep * x_sh, [y2_a - y2_e] * 2, [y2_a + y2_e] *2, 
                        **stg_list[i_tsep].band())
        ax.plot(tr2 + i_tsep * x_sh, [y2_a]*2, **stg_list[i_tsep].line())

    # plateaus
    for i_tsep, tsep in enumerate(tsep_list) :
        tr      = np.r_[0 : 1+tsep]
        dset1   = dgrp_ff1['ratio_pltx/dt%d' % (tsep)]
        dset2   = dgrp_ff2['ratio_pltx/dt%d' % (tsep)]
        rsp     = h5dset_check_rsplan(dset1, dset2, rsplan=rsplan)
        y       = dset1[:, :, i_q2_1, i_ff1] / dset2[:, :, i_q2_2, i_ff2]
        y_a, y_e= lhpd.calc_avg_err(y, rsp)
        ax.errorbar(tr + i_tsep * x_sh, y_a, yerr=y_e, label=(r'$T=%da$' % tsep), 
                    **stg_list[i_tsep].edotline())
    
    # summation res
    dset1   = dgrp_ff1['ratio_summ']
    dset2   = dgrp_ff2['ratio_summ']
    rsp     = h5dset_check_rsplan(dset1, dset2, rsplan=rsp)
    ys      = dset1[:, 0, i_q2_1, i_ff1] / dset2[:, 0, i_q2_2, i_ff2]
    ys_a, ys_e = lhpd.calc_avg_err(ys, rsp)
    ax.errorbar([-1.], ys_a, yerr=ys_e, label='summ', **stg_summ.edot())
    
    if None is xr : xr = [-1.5, 1+tmax]
    ax.set_xlim(xr)
    ax.set_xlabel(r'$\tau$')
    if not None is  yr : ax.set_ylim(yr)
    if None is ylabel: ylabel = dgrp_ff.name
    ax.set_ylabel(ylabel)

    ax.legend()

    return ax

def plot_pltx_ffradius_cmp_mom(dgrp_ff, i_ff, i_q2_1, i_q2_2, tsep_list, rsplan=('jk',1),
            ylabel=None, yr=None, xlabel=r'$\tau$', xr=None, x_sh=None,
            ax=None, stg_list=dv.style_group_default, stg_summ=dv.style_group_default[-1]) :
    """ quick & dirty plotting of plateaus pltx averages and methods """
    if None is ax : ax = dv.make_std_axes()

    max_tslice  = 3    # FIXME must be a parameter to ratio_pltx_avg, ratio_summ

    n_tsep = len(tsep_list)
    tmax = max(tsep_list)
    # plot ratio_pltx, ratio_pltx_avg
    if None is x_sh : x_sh = .4 / max(1, n_tsep - 1)
    
    # bands
    for i_tsep, tsep in enumerate(tsep_list) :
        dset= dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)]
        dq2 = dset.attrs['q2_list'][i_q2_1] - dset.attrs['q2_list'][i_q2_2]
        y2  = (1. - dset[:, 0, i_q2_1, i_ff] / dset[:, 0, i_q2_2, i_ff]) / dq2
        y2_a, y2_e = lhpd.calc_avg_err(y2, rsplan)
        tr2 = np.r_[lhpd.latcorr.pltx_trange(tsep, max_tslice)]
        tr2[-1] -= 1
        ax.fill_between(tr2 + i_tsep * x_sh, [y2_a - y2_e] * 2, [y2_a + y2_e] *2, 
                        **stg_list[i_tsep].band())
        ax.plot(tr2 + i_tsep * x_sh, [y2_a]*2, **stg_list[i_tsep].line())

    # plateaus
    for i_tsep, tsep in enumerate(tsep_list) :
        tr  = np.r_[0 : 1+tsep]
        dset= dgrp_ff['ratio_pltx/dt%d' % (tsep)]
        dq2 = dset.attrs['q2_list'][i_q2_1] - dset.attrs['q2_list'][i_q2_2]
        y   = (1. - dset[:, :, i_q2_1, i_ff] / dset[:, :, i_q2_2, i_ff]) / dq2
        y_a, y_e = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(tr + i_tsep * x_sh, y_a, yerr=y_e, label=(r'$T=%da$' % tsep), 
                    **stg_list[i_tsep].edotline())
    
    # summation res
    dset= dgrp_ff['ratio_summ']
    dq2 = dset.attrs['q2_list'][i_q2_1] - dset.attrs['q2_list'][i_q2_2]
    ys  = (1. - dset[:, 0, i_q2_1, i_ff] / dset[:, 0, i_q2_2, i_ff]) / dq2
    ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
    ax.errorbar([-1.], ys_a, yerr=ys_e, label='summ', **stg_summ.edot())
    
    if None is xr : xr = [-1.5, 1+tsep]
    ax.set_xlim(xr)
    ax.set_xlabel(r'$\tau$')
    if not None is  yr : ax.set_ylim(yr)
    if None is ylabel: ylabel = dgrp_ff.name
    ax.set_ylabel(ylabel)

    ax.legend()

    return ax
    
def plot_q2dep_cmp(dgrp_ff, i_ff, tsep_list, q2_slice=slice(None), 
        rsplan=('jk',1), ylabel=None, yr=None, xlabel=r'$Q^2$[lat]', xr=None, x_sh=None,
        ax=None, stg_list=dv.style_group_default, stg_summ=dv.style_group_default[-1]) :
    """ plot q2 dependence of a ff, compare methods
        q2 (x-axis is in lattice units, x=Q2 *a**2
    """
    if None is ax : ax = dv.make_std_axes()
    ls = latsize[0]
    assert (np.asarray(latsize[:3]) == ls).all()
    
    q2r = dgrp_ff['ratio_summ'].attrs['q2_list'][q2_slice]
    #q2r /= (2. * np.pi / ls)**2
    
    n_tsep = len(tsep_list)
    d_xr = (q2r.max() - q2r.min()) / len(q2r)
    if None is x_sh : x_sh = d_xr *.4 / max(1, n_tsep) # +summation


    # plateau avg
    for i_tsep, tsep in enumerate(tsep_list) :
        y   = dgrp_ff['ratio_pltx_avg/dt%d' % (tsep)][:, 0, :, i_ff]
        y   = y[:, q2_slice]
        y_a, y_e = lhpd.calc_avg_err(y, rsplan)
        ax.errorbar(q2r + i_tsep * x_sh, y_a, yerr=y_e, label=(r'$T=%da$' % tsep), 
                    **stg_list[i_tsep].edotline())

    # summation
    ys  = dgrp_ff['ratio_summ'][:, 0, :, i_ff]
    ys  = ys[:, q2_slice]
    ys_a, ys_e = lhpd.calc_avg_err(ys, rsplan)
    ax.errorbar(q2r + n_tsep * x_sh, ys_a, yerr=ys_e, label='summ', 
                **stg_summ.edotline())
    
    if None is xr : xr = [ q2r.min() - .5 * d_xr, q2r.max() + 1. * d_xr]
    ax.set_xlim(xr)
    ax.set_xlabel(r'$Q^2$ [lat]')
    if not None is  yr : ax.set_ylim(yr)
    if None is ylabel: ylabel = dgrp_ff.name
    ax.set_ylabel(ylabel)

    ax.legend()

    return ax

def runme_plot_all_ff(h5_fname, dir_out,
            tsep_list=src_snk_dt_list
                    ) :
    """ plot plateaus for gV, gA, ... """

    h5f = h5py.File(h5_fname, 'r')
    # gV
    name='%s/gV_pltx_%s_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  = plot_pltx_cmp(h5f['/proton_1/tensor1/U-D'], 0, 0, [8,9,10,12], 
                ylabel=r'$g_V^{bare}$', yr=[0,2.5],
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)

    # gA
    name='%s/gA_pltx_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  = plot_pltx_cmp(h5f['/proton_1/pstensor1/U-D'], 0, 0, [8,9,10,12], 
                ylabel=r'$g_A^{bare}$', yr=[0,2.5], 
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)

    # xv
    name='%s/xv_pltx_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax = plot_pltx_cmp(h5f['/proton_1/tensor2s2/U-D'], 0, 0, 
                [8,9,10,12], ylabel=r'$\langle x\rangle_{u-d}^{bare}$', 
                yr=[0,.6], ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)

    # gA/gV
    name='%s/ga2gv_pltx_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  =plot_ratiopltx_cmp(
                h5f['/proton_1/pstensor1/U-D'], 0, 0, 
                h5f['/proton_1/tensor1/U-D'], 0, 0, 
                [8,9,10,12], ylabel=r'$g_A/g_V$', yr=[0,1.5],
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)
    
    # F1(0)/F1(1)
    name='%s/F10dF11_pltx_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  =plot_ratiopltx_cmp(
                h5f['/proton_1/tensor1/U-D'], 0, 0, 
                h5f['/proton_1/tensor1/U-D'], 0, 1,
                [8,9,10,12], ylabel=r'$F1(0)/F1(1)$', yr=[0.5,1.5],
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)

    # GA(0)/GA(1)
    name='%s/GA0dGA1_pltx_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  = plot_ratiopltx_cmp(
                h5f['/proton_1/pstensor1/U-D'], 0, 0, 
                h5f['/proton_1/pstensor1/U-D'], 0, 1,
                [8,9,10,12], ylabel=r'$GA(0)/GA(1)$', yr=[0.5,1.5],
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)


    # F1(Q2)
    name='%s/F1v_q2dep_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  = plot_q2dep_cmp(
                h5f['/proton_1/tensor1/U-D'], 0, [8,9,10,12], 
                ylabel=r'$F_1^{u-d,B}$', yr=None, 
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)
    
    # F2(Q2)
    name='%s/F2v_q2dep_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  = plot_q2dep_cmp(
                h5f['/proton_1/tensor1/U-D'], 1, [8,9,10,12], 
                ylabel=r'$F_2^{u-d,B}$', yr=None, 
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)
    
    # GA(Q2)
    name='%s/GAv_q2dep_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  = plot_q2dep_cmp(
                h5f['/proton_1/pstensor1/U-D'], 0, [8,9,10,12], 
                ylabel=r'$G_A^{u-d,B}$', yr=None, 
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)

    # GP(Q2)
    name='%s/GPv_q2dep_T%s' % (dir_out, '-'.join([str(x) for x in tsep_list]))
    print('# %s' % (name,))
    ax  = plot_q2dep_cmp(
                h5f['/proton_1/pstensor1/U-D'], 1, [8,9,10,12], 
                ylabel=r'$G_A^{u-d,B}$', yr=None, 
                ax=dv.class_logger(dv.make_std_axes()))
    ax.figure.savefig(name+'.pdf')
    dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
    dv.close_fig(ax.figure)
    
"""


# analysis flow (updated 2015/05/21)
for m in ama_mode_list : 
    runme_calc_save_op(m, src_snk_dt_list=[8,9,10,12], op_list=['tensor2s2'])
    runme_calc_save_op(m, src_snk_dt_list=[8,9,10,12], op_list=['pstensor2s2'])

Tpol_proton1 = gamma_dot((gamma_id + gamma[3])/2., 
                         gamma_id - 1j * gamma_dot(gamma[0], gamma[1]))
had='proton_1'
mhad=.537
mc_dict = make_mom_combo_list(.5, latsize, [[0,0,0]], list(lhpd.int_vectors_maxnorm(3, 10)))
q2_list = sorted(mc_dict.keys())
mcgrp = [ mc_dict[q2] for q2 in q2_list ]
flav='U-D'
op='tensor1'
method_list=[('ratio_pltx',), ('ratio_pltx_avg',), ('ratio_summ',)]

#x=calc_ff(had, Tpol_proton1, flav, op, op_default_ir_list(op), mhad, mcgrp[1], src_snk_dt_list, method_list)

#calc_save_ff(h5py.File('tmp.h5', 'a'), '/tmp', had, Tpol_proton1, flav, op, op_default_ir_list(op), mhad, mcgrp[0:3], src_snk_dt_list, method_list)

runme_calc_save_ff('ff.unbias/tmp.ff.proton_1.U-D.h5', had, Tpol_proton1, mhad, flav_list=['U-D'])
runme_calc_save_ff('ff.unbias/tmp.ff.proton_1.U+D.h5', had, Tpol_proton1, mhad, flav_list=['U+D'])

runme_plot_all_ff('ff.unbias/tmp.ff.proton_1.U-D.h5', 'ff.unbias') 

for fl,op 
runme_calc_save_ff('ff.slonly/tmp.ff.proton_1.U-D.h5', had, Tpol_proton1, mhad, flav_list=['U-D'], apponly=True)
runme_calc_save_ff('ff.slonly/tmp.ff.proton_1.U+D.h5', had, Tpol_proton1, mhad, flav_list=['U+D'], apponly=True)

runme_plot_all_ff('ff.slonly/tmp.ff.proton_1.U-D.h5', 'ff.slonly') 



"""



"""
ax=plot_pltx_cmp(h5py.File('tmp_T8-12.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor1/U-D'], 0, 0, [8,9,10,12], ylabel=r'$g_V^B$', yr=[0,2], xr=[-1.5, 13], ax=dv.class_logger(dv.make_std_axes()))
name='gV_pltx_T8-12' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_q2dep_cmp(h5py.File('tmp_T8-12.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor1/U-D'], 0, [8,9,10,12], ylabel=r'$F_1^{u-d,B}$', yr=None, ax=dv.class_logger(dv.make_std_axes()))
name='F1v_q2dep_T9-12' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)
"""


"""
# xv
ax=plot_pltx_cmp(h5py.File('tmp_T8-12.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor2s2/U-D'], 0, 0, [8,9,10,12], ylabel=r'$\langle x\rangle_{u-d}^{bare}$', yr=[0,.6], xr=[-1.5, 13], x_sh=.1333, ax=dv.class_logger(dv.make_std_axes()))
name='xv_pltx_T8-12' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_pltx_cmp(h5py.File('tmp_T8-10.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor2s2/U-D'], 0, 0, [8,9,10], ylabel=r'$\langle x\rangle_{u-d}^{bare}$', yr=[0,.6], xr=[-1.5, 13], x_sh=.1333, ax=dv.class_logger(dv.make_std_axes()))
name='xv_pltx_T8-10' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_pltx_cmp(h5py.File('tmp_T8-9.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor2s2/U-D'], 0, 0, [8,9], ylabel=r'$\langle x\rangle_{u-d}^{bare}$', yr=[0,.6], xr=[-1.5, 13], x_sh=.1333, ax=dv.class_logger(dv.make_std_axes()))
name='xv_pltx_T8-9' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)



# gA
ax=plot_pltx_cmp(h5py.File('tmp_T8-12.ff.proton_1.U-D.h5', 'r')['/proton_1/pstensor1/U-D'], 0, 0, [8,9,10,12], ylabel=r'$g_A^{bare}$', yr=[0,2.5], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='gA_pltx_T8-12' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_pltx_cmp(h5py.File('tmp_T8-10.ff.proton_1.U-D.h5', 'r')['/proton_1/pstensor1/U-D'], 0, 0, [8,9,10], ylabel=r'$g_A^{bare}$', yr=[0,2.5], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='gA_pltx_T8-10' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_pltx_cmp(h5py.File('tmp_T8-9.ff.proton_1.U-D.h5', 'r')['/proton_1/pstensor1/U-D'], 0, 0, [8,9], ylabel=r'$g_A^{bare}$', yr=[0,2.5], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='gA_pltx_T8-9' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)


# gV
ax=plot_pltx_cmp(h5py.File('tmp_T8-12.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor1/U-D'], 0, 0, [8,9,10,12], ylabel=r'$g_V^{bare}$', yr=[0,2], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='gV_pltx_T8-12' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_pltx_cmp(h5py.File('tmp_T8-10.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor1/U-D'], 0, 0, [8,9,10], ylabel=r'$g_V^{bare}$', yr=[0,2], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='gV_pltx_T8-10' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_pltx_cmp(h5py.File('tmp_T8-9.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor1/U-D'], 0, 0, [8,9], ylabel=r'$g_V^{bare}$', yr=[0,2], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='gV_pltx_T8-9' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)


# F1v(1)/F1v(0)
ax=plot_pltx_ffradius_cmp_mom(h5py.File('tmp_T8-12.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor1/U-D'], 0, 1, 0, [8,9,10,12], ylabel=r'$F_1^{u-d}(1) / F_1^{u-d}(0)$', yr=[0,None], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='F1v_radius1to0_pltx_T8-12' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_pltx_ffradius_cmp_mom(h5py.File('tmp_T8-10.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor1/U-D'], 0, 1, 0, [8,9,10], ylabel=r'$F_1^{u-d}(1) / F_1^{u-d}(0)$', yr=[0,None], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='F1v_radius1to0_pltx_T8-10' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

ax=plot_pltx_ffradius_cmp_mom(h5py.File('tmp_T8-9.ff.proton_1.U-D.h5', 'r')['/proton_1/tensor1/U-D'], 0, 1, 0, [8,9], ylabel=r'$F_1^{u-d}(1) / F_1^{u-d}(0)$', yr=[0,None], xr=[-1.5, 13], x_sh=0.1333, ax=dv.class_logger(dv.make_std_axes()))
name='F1v_radius1to0_pltx_T8-9' ; ax.figure.savefig(name+'.pdf') ; dv.save_figdraw(open(name+'.plt.py', 'w'), ax=ax)

"""
