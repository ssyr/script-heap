from past.builtins import execfile
import numpy as np
import lhpd
from lhpd.latcorr import make_linkpath_list
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted
from lhpd.misc.strkey import *

latsize = np.r_[24,24,24,64]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,-1]


cfg_index   = lhpd.read_key_list('list.cfg.all', 'index')
cfg_list    = lhpd.read_key_list('list.cfg.n100', 'samples')

ama_ex      = 'ex'
ama_sl      = 'sl'
ama_list = [ama_ex, ama_sl]

csrc_ngrid = {
    ama_ex : [1,1,1,1],
    ama_sl : [2,2,2,4] }
csrc_meas_dxc    = [7,11,13,23]
csrc_meas_x0     = [0,0,0,0]

def make_csrc_list(ama, cfg_key):
    return lhpd.make_src_grid(csrc_ngrid[ama], latsize, 
                csrc_meas_x0, csrc_meas_dxc, 
                1 + cfg_index.index(cfg_key))

ncoh_src_t      = 1 # number of coherent src in one group
c2pt_hslab_len  = latsize[t_axis] // ncoh_src_t
def make_csrcgrp_list(ama, cfg_key):
    ngrid_t     = csrc_ngrid[ama][t_axis]
    assert(0 == ngrid_t % ncoh_src_t)
    ncoh_grp    = ngrid_t // ncoh_src_t
    # assuming sources are grouped so that csrc[t_axis] changes fastest
    csrc_list   = make_csrc_list(ama, cfg_key)
    ndim        =len(latsize)
    csrcgrp_list= csrc_list.reshape(-1, ncoh_src_t, ncoh_grp, ndim) # split into groups with equal spatial coords
    csrcgrp_list= csrcgrp_list.transpose(0,2,1,3)                   # rearrange to have max. t-separation in each grp
    csrcgrp_list= csrcgrp_list.reshape(-1, ncoh_src_t, ndim)        # flatten (0,1) axes       
    assert((csrcgrp_list[:, :, :-1] == csrcgrp_list[:, 0:1, :-1]).all())
    return csrcgrp_list


meas_list_sl = np.array([(c, csrc) for c in cfg_list for csrc in make_csrc_list('sl', c)], dtype=h5_meas_dtype)
meas_list_ex = np.array([(c, csrc) for c in cfg_list for csrc in make_csrc_list('ex', c)], dtype=h5_meas_dtype)
meas_list = { 
    ama_ex : meas_list_ex,
    ama_sl : meas_list_sl
}


c2pt_psnk_list  = normsorted(list(int_vectors_maxnorm(3, 10)))
#c2pt_tpol_list  = [ 'posSzplus', 'posSzminus' ]
#c2pt_tpol5_list  = [ 'posSzplus', 'posSzminus', 'posSzplus5', 'posSzminus5' ]
c2pt_tpol_list  = [ 'Tg%d' % g for g in range(16) ]
c2pt_tpol5_list = c2pt_tpol_list
hspec_meson_list = [
    'a0_1', 'a0_2', 'a1_x_1', 'a1_y_1', 'a1_z_1',
    'b1_x_1', 'b1_y_1', 'b1_z_1', 
    'pion_1', 'pion_2',
    'rho_x_1', 'rho_x_2', 'rho_y_1', 'rho_y_2', 'rho_z_1', 'rho_z_2',
]
hspec_baryon_list = [
    'delta_1', 'delta_2', 'delta_3',
    'lambda_1', 'lambda_2', 'lambda_3', 'lambda_4', 'lambda_5', 
    'proton_1', 'proton_2', 'proton_3', 'proton_4', 'proton_5', 'proton_6',
    'proton_negpar_3', 
    'proton_np', 'proton_np5', 'proton_pp', 'proton_pp5',
    'xi_1', 'xi_2' 
]

c3pt_tpol_list = ['posSzplus']
c3pt_tpol5_list= c3pt_tpol_list
c3pt_psnk_list = np.asarray([[0,0,0]])
c3pt_qext_list = normsorted(list(int_vectors_maxnorm(3, 10)))
c3pt_tsep_list = [ 8 ]
c3pt_tsnk_full = False

flav_list       = ['U', 'D']
flav_cedm_list  = flav_list
flav_cur_list   = flav_list
gf_cedm_list    = ['orig']
 
c3pt_lpath_min, c3pt_lpath_max=0,1
lpath_list = make_linkpath_list(c3pt_lpath_min, c3pt_lpath_max, dim=4)
lpath_str_list = [ lpath_str(lp) for lp in lpath_list ]

c3pt_op_list = ['tensor1']
#c3pt_op_list = ['pstensor1']
#c3pt_op_list = ['tensor1', 'pstensor1']


data_top='data/bgem+1'
data_out='data_out/bgem+1'
bgem_tag='uEx0y0z-1_dEx0y0z2'
execfile('config_filenames_bgem.py')

