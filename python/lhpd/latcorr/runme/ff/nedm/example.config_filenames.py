import aff
import h5py 
import numpy as np
import math
import itertools as it
import lhpd
from lhpd import make_src_grid


def ama_str(ama) : return ama
# TODO change had_tpol -> had because this is traditional for hadspec
def get_hspec_aff_file(cfgkey, ama, csrc, sm_srcsnk) :
    return '%s/hadspec/hadspec.%s.%s.%s.aff' % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrc))
def get_hspec_aff_kpath(cfgkey, csrc, had, sm_srcsnk) :
    return '/hadspec/%s/%s/%s' % (
            sm_srcsnk, had, csrc_str_k(csrc))
def make_hspec_filekpath_func(cfgkey, ama, had, sm_srcsnk) :
    def hsfilekpath_func(csrc) :
        return (get_hspec_aff_file(cfgkey, ama, csrc, sm_srcsnk),
                get_hspec_aff_kpath(cfgkey, csrc, had, sm_srcsnk))
    return hsfilekpath_func
def get_hspec_h5_file(cfgkey, ama, had, sm_srcsnk, data_out=data_out) :
    return '%s/hspec/hspec.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, had, sm_srcsnk)


def get_c2pt_aff_file(cfgkey, ama, csrcgrp[0], sm_srcsnk) :
    return '%s/c2pt/c2pt.%s.%s.%s.aff' % (
            data_top, cfgkey, ama_str(ama), csrcgrp_str_f(csrcgrp))
def get_c2pt_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk) :
    return '/c2pt/%s/%s_%s' % (
            sm_srcsnk, had, tpol)
def make_c2pt_filekpath_func(cfgkey, ama, had, sm_srcsnk) :
    def hsfilekpath_func(csrcgrp, tpol) :
        return (get_c2pt_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk),
                get_c2pt_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk))
    return hsfilekpath_func
def get_c2pt_h5_file(cfgkey, ama, had, sm_srcsnk, data_out=data_out) :
    return '%s/c2pt/c2pt.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, had, sm_srcsnk)
def get_c2pt_all_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
    return '%s/c2pt.all/c2pt-all.%s.%s.%s.h5' % (
            data_out, ama, had, sm_srcsnk)
def get_c2pt_bin_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
    return '%s/c2pt.bin/c2pt-bin.%s.%s.%s.h5' % (
            data_out, ama, had, sm_srcsnk)
def get_c2pt_unbias_h5_file(had, sm_srcsnk, data_out=data_out) :
    return '%s/c2pt.bin/c2pt-unbias.%s.%s.h5' % (
            data_out, had, sm_srcsnk)


def get_c2pt_volcedm_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk, gf_cedm) :
    return '%s/c2pt_volcedm/c2pt_volcedm.%s.%s.%s.%s.aff' % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrcgrp[0]), gf_cedm)
def get_c2pt_volcedm_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk, flav_cedm, gf_cedm) :
    return '/c2pt_volcedm/%s/%s/%s_%s/%s' % (
            sm_srcsnk, gf_cedm, had, tpol, flav_cedm)
def make_c2pt_volcedm_filekpath_func(cfgkey, ama, had, sm_srcsnk, flav_cedm, gf_cedm) :
    def hsfilekpath_func(csrcgrp, tpol) :
        return (get_c2pt_volcedm_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk, gf_cedm),
                get_c2pt_volcedm_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk, flav_cedm, gf_cedm))
    return hsfilekpath_func
def get_c2pt_volcedm_h5_file(cfgkey, ama, had, sm_srcsnk, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/c2pt_volcedm/c2pt_volcedm.%s.%s.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, had, sm_srcsnk, flav_cedm, gf_cedm)
def get_c2pt_volcedm_bin_h5_file(ama, had, sm_srcsnk, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/c2pt_volcedm.bin/c2pt_volcedm-bin.%s.%s.%s.%s.%s.h5' % (
            data_out, ama, had, sm_srcsnk, flav_cedm, gf_cedm)
def get_c2pt_volcedm_unbias_h5_file(had, sm_srcsnk, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/c2pt_volcedm.bin/c2pt_volcedm-unbias.%s.%s.%s.%s.h5' % (
            data_out, had, sm_srcsnk, flav_cedm, gf_cedm)



def get_bb_aff_file(cfgkey, ama, csrcgrp, psnk, tsep, tpol, flav_cur) :
    return "%s/bb/bb.%s.%s.%s.%sdt%d.%s.%s.aff" % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrcgrp[0]), 
            psnk_str_f(psnk), tsep, tpol, flav_cur)
def get_bb_aff_kpath(cfgkey, csrcgrp, psnk, tsep, sm_srcsnk, had, tpol, flav_cur) :
    return "/bb/%s/%s_%s/%s/%s/%s_dt%d" % (
            sm_srcsnk, had, tpol, flav_cur, csrc_str_k(csrcgrp[0]), psnk_str_k(psnk), tsep)
def make_bb_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, flav_cur) :
    """ return aff file and kpath generator function to be used with `conv_bb2hdf'
    """
    def bbfilekpath_func(csrcgrp, tsep) :
        return (
            get_bb_aff_file(cfgkey, ama, csrcgrp, psnk, tsep, tpol, flav_cur),
            get_bb_aff_kpath(cfgkey, csrcgrp, psnk, tsep, sm_srcsnk, had, tpol, flav_cur))
    return bbfilekpath_func
def get_bb_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, data_out=data_out) :
    return '%s/bb/bb.%s.%s.%sdt%d.%s_%s.%s.%s.h5' % (
            data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur)
def get_bb_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, data_out=data_out) :
    return '%s/bb.bin/bb-bin.%s.%sdt%d.%s_%s.%s.%s.h5' % (
            data_out, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur)
def get_bb_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, data_out=data_out) :
    return '%s/bb.bin/bb-unbias.%s.%sdt%d.%s_%s.%s.%s.h5' % (
            data_out, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur)


def get_op_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
    return '%s/op/op.%s.%s.%sdt%d.%s_%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, op)
def get_op_all_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
    return '%s/op.all/op-all.%s.%sdt%d.%s_%s.%s.%s.%s.h5' % (
            data_out, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, op)
def get_op_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
    return '%s/op.bin/op-bin.%s.%sdt%d.%s_%s.%s.%s.%s.h5' % (
            data_out, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, op)
def get_op_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
    return '%s/op.bin/op-unbias.%sdt%d.%s_%s.%s.%s.%s.h5' % (
            data_out, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, op)



def get_bb_volcedm_aff_file(cfgkey, ama, csrcgrp, psnk, tsep, tpol, flav_cur, flav_cedm, gf_cedm) :
    return "%s/bb_volcedm/bb_volcedm.%s.%s.%s.%sdt%d.%s.%s.%s.%s.aff" % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrcgrp[0]), 
            psnk_str_f(psnk), tsep, tpol, flav_cur, flav_cedm, gf_cedm)
def get_bb_volcedm_aff_kpath(cfgkey, csrcgrp, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm) :
    return "/bb_volcedm/%s/%s/%s_%s/%s/%s/%s/%s_dt%d" % (
            sm_srcsnk, gf_cedm,  had, tpol, flav_cur, flav_cedm, csrc_str_k(csrcgrp[0]), psnk_str_k(psnk), tsep)
def make_bb_volcedm_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm) :
    """ return aff file and kpath generator function to be used with `conv_bb2hdf'
    """
    def bbfilekpath_func(csrcgrp, tsep) :
        return (
            get_bb_volcedm_aff_file(cfgkey, ama, csrcgrp, psnk, tsep, tpol, flav_cur, flav_cedm, gf_cedm),
            get_bb_volcedm_aff_kpath(cfgkey, csrcgrp, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm))
    return bbfilekpath_func
def get_bb_volcedm_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/bb_volcedm/bb_volcedm.%s.%s.%sdt%d.%s_%s.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, flav_cedm, gf_cedm)
def get_bb_volcedm_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/bb_volcedm.bin/bb_volcedm-bin.%s.%sdt%d.%s_%s.%s.%s.%s.%s.h5' % (
            data_out, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, flav_cedm, gf_cedm)
def get_bb_volcedm_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/bb_volcedm.bin/bb_volcedm-unbias.%s.%sdt%d.%s_%s.%s.%s.%s.%s.h5' % (
            data_out, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, flav_cedm, gf_cedm)


def get_op_volcedm_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/op_volcedm/op_volcedm.%s.%s.%sdt%d.%s_%s.%s.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, op, flav_cedm, gf_cedm)
def get_op_volcedm_all_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/op_volcedm.all/op_volcedm-all.%s.%sdt%d.%s_%s.%s.%s.%s.%s.%s.h5' % (
            data_out, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, op, flav_cedm, gf_cedm)
def get_op_volcedm_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/op_volcedm.bin/op_volcedm-bin.%s.%sdt%d.%s_%s.%s.%s.%s.%s.%s.h5' % (
            data_out, ama, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, op, flav_cedm, gf_cedm)
def get_op_volcedm_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=data_out) :
    return '%s/op_volcedm.bin/op_volcedm-unbias.%sdt%d.%s_%s.%s.%s.%s.%s.%s.h5' % (
            data_out, psnk_str_f(psnk), tsep, 
            had, tpol, sm_srcsnk, flav_cur, op, flav_cedm, gf_cedm)


