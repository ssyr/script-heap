import dataview as dv

def plot_statmap_c2pt(gg, mc_list, ss, tr, **kw) :
    xchop   = kw.get('xchop')
    ax      = kw.get('ax') or dv.make_std_axes()
    cmap    = kw.get('cmap') or 'gray'
    c2_list = [ ( stat_dm2madm(gg.get_twopt_src(mc, ss).real[:, tr]), 
                  dv.latexize(str(mc)) )
                for mc in mc_list ]
    m, yt, yl = make_statmap_tile(*c2_list, xchop=xchop, norm=True)
    ax.imshow(m.T, cmap=cmap)
    ax.set_yticks(yt)
    ax.set_yticklabels(yl)
    return ax

def save_statmap_c2pt(fname_out, gg, mc_list, ss, tr, **kw) :
    xchop   = kw.get('xchop')
    cmap    = kw.get('cmap') or 'gray'
    c2_list = [ ( stat_dm2madm(gg.get_twopt_src(mc, ss).real[:, tr]), 
                  dv.latexize(str(mc)) )
                for mc in mc_list ]
    m, yt, yl = make_statmap_tile(*c2_list, xchop=xchop, norm=True)
    mpl.image.imsave(fname_out, m.T, cmap=cmap)
