import numpy as np

#topo_rcut_list = np.r_[4:44+1:4]
qtopo_rcut_list = [ None, 16, 24 ]
qtopo_dtcut_list= [ 2, 4, 8, 12, 20, 48 ]
qtopo_tag_list = [ 'wf0.05x%d' % wf_n for wf_n in [100, 200, 400] ]
qtopo_qop_list = [ 'qtopo_imp5li' ]
