#!/usr/bin/env python
from __future__ import print_function
from past.builtins import execfile
from future.utils import iteritems
import os, sys, re
import traceback
from lhpd import dictnew, dict_prod, dict_prod_auto, np_at_slice, np_equal_bound
from attrdict import AttrDict as adict
from lhpd import strip_prefix

execfile('pproc_nedm_lcdb.py')

if '__main__' == __name__ :
    def print_usage(prg) :
        print("""Usage:
%s  [--sj=i{=0..TOT-1}/TOT]  [--mpi=PMI] [--bin=<binsize>]
        [--data-in=<data_in>]  [--data-out=<data_out>] 
        <what>   <config>  [<job-list>] ++ [<job_id0>] ...
What :
    conv_{c2pt,bb}{,_volcedm,_volpsc}
    calc_op{,_volcedm,_volpsc}
    binx_{c2pt,op}{,_volcedm,_volpsc}
    merge_{c2pt,op}{,_volcedm,_volpsc}
    qtopo_rcut

    ######## OLD
    #conv_hspec          # FIXME : change tpol_list->hspec_list or remove it alltogether from dim_spec
    #conv_c2pt_cpbar

    conv_c2pt
    bin_unbias_c2pt

    conv_c2pt_<vacins>
    bin_unbias_c2pt_<vacins>

    conv_bb
    bin_unbias_op

    conv_bb_<vacins>
    calc_op_<vacins>
    bin_unbias_op_<vacins>

    merge_c2pt
    merge_op
    #merge_op_volcedm

    qtopo_rcut
    unbias_posnpr   # average&unbias all samples, save 1file/cfg 
    collect_posnpr  # transform posnpr into something useful

""" % (prg,))

    argv = list(sys.argv)
    if len(argv) < 1 :
        print_usage('me')
        sys.exit(1)

    sj_this, sj_tot = 0, 1
    binsize     = 1

    opts = adict()
    while 1 < len(argv) and argv[1].startswith('--') :
        if argv[1].startswith('--sj=') :
            sj = re.match('^--sj=(\d+)/(\d+)$', argv[1])
            if not sj : 
                raise ValueError(argv[1])
            sj_this, sj_tot = [ int(x) for x in sj.groups() ]
            if (sj_this < 0 or sj_tot <= sj_this) :
                raise ValueError(("bad sj = %d/%d" % (sj_this, sj_tot)))
            print("sj = ", sj_this, sj_tot)

        elif argv[1].startswith('--data-in=') :
            opts.data_in = re.match('^--data-in=(.+)$', argv[1]).groups()[0]
        elif argv[1].startswith('--data=') :
            opts.data_in = re.match('^--data=(.+)$', argv[1]).groups()[0]
            opts.data_out= opts.data_in
        elif argv[1].startswith('--data-out=') :
            opts.data_out = re.match('^--data-out=(.+)$', argv[1]).groups()[0]

        elif argv[1].startswith('--bin=') :
            opts.binsize = int(re.match('^--bin=(.+)$', argv[1]).groups()[0])
            if opts.binsize <= 0 :
                raise ValueError(("bad binsize = %d" % opts.binsize))

        elif argv[1].startswith('--mpi=') :
            mpitype = re.match('^--mpi=(.+)$', argv[1]).groups()[0]
            if 'PMI' == mpitype :
                sj_this = int(os.environ['PMI_RANK'])
                sj_tot  = int(os.environ['PMI_SIZE'])
            else : raise ValueError(("bad mpi=%s" % mpitype))

            if (sj_this < 0 or sj_tot <= sj_this) :
                raise ValueError(("bad sj[PMI] = %d/%d" % (sj_this, sj_tot)))
            print("sj = ", sj_this, sj_tot)

        else :
            sys.stderr.write("bad arg='%s'\n" % argv[1])
            print_usage(argv[0] or 'me')
            sys.exit(1)
        argv.pop(1)

    if len(argv) < 3 :
        print_usage(argv[0] or 'me')
        sys.exit(1)

    try : isep = argv.index('++')
    except : isep = len(argv)
    #print("len(argv)=%d  isep=%d  argv=%s" % (len(argv), isep, str(argv)))

    if isep < 3 :
        print_usage(argv[0] or 'me')
        sys.exit(1)

    cfglist_cmd_ = argv[isep+1:]
    cfglist_file_ = argv[3:isep]
    #print("joblist=file(%s) + %s" % (str(cfglist_file_), str(cfglist_cmd_)))

    cfgkey_list = list(cfglist_cmd_)
    for f in cfglist_file_ :
        cfgkey_list.extend(lhpd.read_key_list(f, 'samples'))

    if len(cfgkey_list) <= 0 :
        sys.stderr.write('empty list; exit\n')
        sys.exit(1)

    print("Total samples: ", cfgkey_list_str(cfgkey_list))

    # TODO check for repeated keys

    conf_py   = argv[2]
    print("FIXME data_in, data_out overwritten by config")
    execfile(conf_py)
    print("SET opt %s" % str(opts))
    for k, v in iteritems(opts) : globals()[k] = v
    print("data_in  = '%s'" % data_in)
    print("data_out = '%s'" % data_out)


###############################################################################
# c2pt
##############################################################################
def runme_conv_c2pt(
        cfgkey, c2_d,
        ama_list=ama_list, 
        attrs_kw={}) :
    """ convert CP-even 2pt functions 
    """
    had     = c2_d['had']
    bc_t    = get_hadron_bc(had, ferm_bc[t_axis])
    saved_bc_t= c2_d['saved_bc_t']
    print("runme_conv_c2pt: bc_t=%s, saved_bc_t=%s" % (str(bc_t), str(saved_bc_t)))
    for ama in ama_list :
        print("# %s %s %s %s %s " % (cfgkey, ama_str(ama), had, c2_d['tpol_list'], srcsnk_str_f(c2_d)))
        latcorr_conv2hdf(
                dict(data_dir=data_out, cfgkey=cfgkey, ama=ama),        # dat_d
                dict(kind='c2', c2_d=c2_d),
                make_csrcgrp_list(ama, cfgkey),                         # csrcgrp_list
                latsize, t_axis, c2pt_hslab_len, bc_t, 
                dictnew(attrs_kw, source_hadron=had, sink_hadron=had),  # attrs_kw
                data_in=data_in, #data_dir_aff, 
                saved_bc_t=saved_bc_t,
                VERBOSE=True) # attrs_kw

def runme_conv_c2pt_vacins(
        cfgkey, c2_d, vi_d,
        ama_list=ama_list,
        attrs_kw={}) :
    """ convert 2pt+CPodd functions 
    """
    had     = c2_d['had']
    bc_t    = get_hadron_bc(had, ferm_bc[t_axis])
    saved_bc_t= c2_d['saved_bc_t']
    print("runme_conv_c2pt: bc_t=%s, saved_bc_t=%s" % (str(bc_t), str(saved_bc_t)))
    for ama in ama_list :
        print("# %s %s %s %s %s:%s %s " % (
                cfgkey, ama_str(ama), had, c2_d['tpol_list'], 
                vi_d['kind'], vacins_meta0_str_k(vi_d), srcsnk_str_f(c2_d)))
        latcorr_conv2hdf(
                dict(data_dir=data_out, cfgkey=cfgkey, ama=ama),        # dat_d
                dict(kind='c2vi', c2_d=c2_d, vi_d=vi_d),
                make_csrcgrp_list(ama, cfgkey),                         # csrcgrp_list
                latsize, t_axis, c2pt_hslab_len, bc_t, 
                dictnew(attrs_kw, source_hadron=had, sink_hadron=had),  # attrs_kw
                data_in=data_in, #data_dir_aff, 
                saved_bc_t=saved_bc_t,
                VERBOSE=True) # attrs_kw

##############################################################################
# conv_bb
###############################################################################
def runme_conv_c3pt_qbarq(
                cfgkey, c3_d, ins_d,
                ama_list=ama_list,
                attrs_kw={}) :
    had     = c3_d['had']
    bc_t    = get_hadron_bc(had, ferm_bc[t_axis])
    saved_bc_t= c3_d['saved_bc_t']
    print("runme_conv_c3pt_qbarq: bc_t=%s, saved_bc_t=%s" % (str(bc_t), str(saved_bc_t)))
    for ama in ama_list :
        print("# %s %s %s %s %s " % (cfgkey, ama_str(ama), 
                hadron_str(c3_d), srcsnk_str_f(c3_d), c3snk_str_f(c3_d)))

        latcorr_conv2hdf(
                dict(data_dir=data_out, cfgkey=cfgkey, ama=ama),
                dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
                make_csrcgrp_list(ama, cfgkey),
                latsize, t_axis, c3pt_tlen, bc_t,
                dictnew(attrs_kw, source_hadron=had, sink_hadron=had, 
                        tpol=c3_d['tpol'], time_neg=False, sink_mom=c3_d['psnk']),
                data_in=data_in, #data_dir_aff,
                saved_bc_t=saved_bc_t,
                VERBOSE=True)

def runme_conv_c3pt_vacins_qbarq(
                cfgkey, c3_d, ins_d, vi_d,
                ama_list = ama_list,
                attrs_kw={}) :
    had     = c3_d['had']
    bc_t    = get_hadron_bc(had, ferm_bc[t_axis])
    saved_bc_t= c3_d['saved_bc_t']
    print("runme_conv_c3pt_qbarq: bc_t=%s, saved_bc_t=%s" % (str(bc_t), str(saved_bc_t)))
    for ama in ama_list :
        print("# %s %s %s %s:%s %s %s " % (cfgkey, ama_str(ama), 
                hadron_str(c3_d), vi_d['kind'], vacins_meta0_str_k(vi_d),
                srcsnk_str_f(c3_d), c3snk_str_f(c3_d)))

        latcorr_conv2hdf(
                dict(data_dir=data_out, cfgkey=cfgkey, ama=ama),
                dict(kind='c3vi', c3_d=c3_d, ins_d=ins_d, vi_d=vi_d),
                make_csrcgrp_list(ama, cfgkey),
                latsize, t_axis, c3pt_tlen, bc_t,
                dictnew(attrs_kw, source_hadron=had, sink_hadron=had, 
                        tpol=c3_d['tpol'], time_neg=False, sink_mom=c3_d['psnk']),
                data_in=data_in, #data_dir_aff,
                saved_bc_t=saved_bc_t,
                VERBOSE=True)

def runme_calc_save_op(
                cfgkey, c3_d, ins_d,
                ama_list = ama_list,
                attrs_kw={}) :
    
    for ama in ama_list :
        print("# %s %s %s %s %s " % (cfgkey, ama_str(ama), 
                hadron_str(c3_d), srcsnk_str_f(c3_d), c3snk_str_f(c3_d)))

        latcorr_calc_qbarq_op(
                dict(kind='conv', data_dir=data_out, cfgkey=cfgkey, ama=ama),
                dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
                VERBOSE=True)

def runme_calc_save_op_vacins(
                cfgkey, c3_d, ins_d, vi_d,
                ama_list = ama_list,
                attrs_kw={}) :
    
    for ama in ama_list :
        print("# %s %s %s %s:%s %s %s " % (cfgkey, ama_str(ama), 
                hadron_str(c3_d), vi_d['kind'], vacins_meta0_str_k(vi_d),
                srcsnk_str_f(c3_d), c3snk_str_f(c3_d)))

        latcorr_calc_qbarq_op(
                dict(kind='conv', data_dir=data_out, cfgkey=cfgkey, ama=ama),
                dict(kind='c3vi', c3_d=c3_d, ins_d=ins_d, vi_d=vi_d),
                VERBOSE=True)

def weigh_qtopo_dtcut(mspec_list, vispec, dtcut, tsep) :
    """ produce topological charge subsums
        res[mlist, (dtcut+tsep+t).shape] = sum_{tsrc-dtcut <= t <= tsrc+tsep+dtcut} Q(t) 
        dtcut, tsep can be ndarray's: to create tsep-dependent sums for reweighting c2[tsep],
        pass tsep=np.r_[:lt][:,None] (last axis is contracted with q[t])
        

    """
    assert('theta' == vispec['vacins'])
    lt  = latsize[t_axis]
    tr  = np.r_[:lt]
    res = [None] * len(mspec_list)
    # group by ckpoint_id 
    m_c = {}
    for i_m, m in enumerate(mspec_list) : 
        m_c.setdefault(m['ckpoint_id'], []).append((i_m, m))

    rcut    = vispec.get('rcut'), 
    dtcut   = vispec['dtcut']
    for c, mlist in iteritems(m_c) :
        h5file  = get_qtopo_proc_h5_file(c, vispec['gf'])
        h5f = h5py.File(h5file, 'r')
        h5k = '/cfg%s/%s/qtopo_imp5li' % (c, vispec['gf'])
        for i_m, m in mlist :
            if None is rcut : 
                qt  = h5file['%s/xsum' % (h5k,)].value 
            else :
                csrc_sl = list(m['source_coord'])
                csrc_sl[t_axis] = slice(None)
                qt  = h5file['%s/rcut%d' % (h5k, rcut)][tuple(csrc_sl)]
            assert(1 == len(qt.shape) and lt == qt.shape[0])
            # res[t1] = sum_t(tmask[t1, t] * qt[t])
            # where t1=(tsnk-tsrc) for c2 or c3
            tsrc    = m['source_coord'][t_axis]
            tmid    = tsrc  + tsep / 2.
            tlen    = dtcut + tsep / 2.
            tmask0  = np.where((tmid - tr) % lt <= tlen, 1, 0)
            tmask1  = np.where((tr - tmid) % lt <= tlen, 1, 0)
            tmask   = np.where(tmask0 + tmask1, 1, 0)
            res[i_m]= (tmask * qt).sum(-1)

        h5f.close()

    return np.asarray(res)

def weigh_discvacins_mask_trel(mspec_list, mask_trel, dat_d, vi_d, latsize, t_axis) :
    """ produce topological charge subsums
        res[mlist, i_mask, (dtcut+tsep+t).shape] = sum_{t}(mask_trel[i_mask, t-tsrc] * Q[t])
        mask_trel can be ndarray: to create tsep-dependent sums for reweighting c2[tsep],
        pass tsep=np.r_[:lt][:,None] (last axis is contracted with q[t])
    """
    assert('theta' == vi_d['kind'])
    lt  = latsize[t_axis]
    assert mask_trel.shape[-1] == lt
    tr  = np.r_[:lt]
    res = [None] * len(mspec_list)
    # group by ckpoint_id 
    m_c = {}
    for i_m, m in enumerate(mspec_list) : 
        m_c.setdefault(m['ckpoint_id'], []).append((i_m, m))

    rcut    = vi_d.get('rcut')
    for c, mlist in iteritems(m_c) :
        h5file, h5kpath = get_lcdb_loc(
                dictnew(dat_d, cfgkey=c), 
                dict(kind='dvi', vi_d=vi_d))
        h5f     = h5py.File(h5file, 'r')
        for i_m, m in mlist :
            tsrc    = m['source_coord'][t_axis]
            if None is rcut : 
                qt  = h5f['%s/xsum' % (h5kpath,)].value 
            else :
                csrc_sl = list(m['source_coord'])
                csrc_sl[t_axis] = slice(None)
                qt  = h5f['%s/rcut%d' % (h5kpath, rcut)][tuple(csrc_sl)]
            # qt[t]
            assert(1 == len(qt.shape) and lt == qt.shape[0])
            # sum_t mask_trel[i_mask, (t-tsrc)] * qt[t]
            res[i_m]= (mask_trel[..., (tr + lt - tsrc) % lt] * qt).sum(-1)
            #res[i_m]= (mask_trel[(tr + lt - tsrc) % lt] * qt).sum(-1)      # XXX test: must be wrong?
            #res[i_m]= (mask_trel * qt).sum(-1)      # XXX test2: must be wrong? "random" time displacements

        h5f.close()

    return np.array(res)

def make_mask_trel(lt, tr, tsep, dtcut) :
    """ make mask [..., i_t] for time coordinate :
            1, if -dtcut0 <= tr[i_t] <= tsep + dtcut1
            0, otherwise
        time coordinate is wrapped around at lt
        NOTE tsep can be np.array; tsep.shape[1] must agree with tr.shape
    """
    if isinstance(dtcut, list) or isinstance(dtcut, tuple) : 
        dtcut0, dtcut1 = dtcut
    else: 
        dtcut0, dtcut1 = dtcut, dtcut
    mask1   = np.where( tr <= tsep + dtcut1, 1, 0)
    if 0 < dtcut0 :
        mask0   = np.where((lt - tr) % lt <= dtcut0, 1, 0)
        mask_trel = np.where(mask0 + mask1, 1, 0)
    else :
        mask0   = np.where(-dtcut0 <= tr, 1, 0)
        mask_trel = np.where(mask0 * mask1, 1, 0)
    return mask_trel

def runme_binx_c2pt_theta(cfgkey_list, ama_list, c2_d, vi_d, VERBOSE=False) :
    lt      = latsize[t_axis]
    dtcut   = vi_d.get('dtcut', lt)
    tr      = np.r_[:lt]
    # take care only of dtcut: rcut has been done in qtopo files
    tsep    = tr[:, None]       # i_mask == tsep in c2pt[tsep]
    # mask[i_mask==tsep, (t-tsrc)%lt]
    mask_trel = make_mask_trel(lt, tr, tsep, dtcut)
    #mask0   = np.where((lt - tr) % lt <= dtcut, 1, 0)
    #mask1   = np.where( tr <= tsep + dtcut, 1, 0)
    #mask_trel = np.where(mask0 + mask1, 1, 0)   # [t-tsrc, t]
    theta_dat_d = dict(data_dir=data_in)
    def weight_qtopo_tsep(mlist) : 
        return weigh_discvacins_mask_trel(
                mlist, mask_trel, theta_dat_d, vi_d, latsize, t_axis)

    latcorr_bin_unbias(cfgkey_list, ama_list,
        dict(data_dir=data_out),
        dict(kind='c2vi', c2_d=c2_d, vi_d=vi_d),
        lc_d_in=dict(kind='c2', c2_d=c2_d),
        data_in=data_in,
        reweight_func=weight_qtopo_tsep,
        binsize=binsize,
        do_ama=False,
        VERBOSE=VERBOSE)

def runme_binx_c3ts_theta(cfgkey_list, ama_list, c2_d, vi_d, VERBOSE=False) :
    # most of it copied from weigh_qtopo_dtcut
    assert('theta' == vi_d['kind'])
    lt  = latsize[t_axis]
    theta_dat_d = dict(data_dir=data_in)
    def mfunc_qtopo_tslice(d, **kw):
        mspec_list  = kw['mlist']
        # group by ckpoint_id 
        m_c = {}
        for i_m, m in enumerate(mspec_list) : 
            m_c.setdefault(m['ckpoint_id'], []).append((i_m, m))

        res     = np.empty(d.shape + (lt,), d.dtype)
        rcut    = vi_d.get('rcut')
        for c, mlist in iteritems(m_c) :
            h5file, h5kpath = get_lcdb_loc(
                    dictnew(theta_dat_d, cfgkey=c), 
                    dict(kind='dvi', vi_d=vi_d))
            h5f     = h5py.File(h5file, 'r')

            for i_m, m in mlist :
                csrc    = m['source_coord']
                if None is rcut : 
                    qt  = h5f['%s/xsum' % (h5kpath,)].value 
                else :
                    csrc_sl = list(csrc)
                    csrc_sl[t_axis] = slice(None)
                    qt  = h5f['%s/rcut%d' % (h5kpath, rcut)][tuple(csrc_sl)]
                # qt[t]
                assert(1 == len(qt.shape) and lt == qt.shape[0])

                tsrc    = csrc[t_axis]
                res[i_m] = d[i_m][..., None] * lhpd.np_circshift_coord(qt, [tsrc])

            h5f.close()

        return res


    latcorr_bin_unbias(cfgkey_list, ama_list,
        dict(data_dir=data_out),
        dict(kind='c3ts', c2_d=c2_d, vi_d=vi_d),
        lc_d_in=dict(kind='c2', c2_d=c2_d),
        data_in=data_in,
        mfunc=mfunc_qtopo_tslice,
        binsize=binsize,
        do_ama=False,
        VERBOSE=VERBOSE)

    

def runme_binx_op_theta(cfgkey_list, ama_list, c3_d, ins_d, vi_d, VERBOSE=False) :
    # take care only of dtcut: rcut has been done in qtopo files, must be marked in vi_d
    lt      = latsize[t_axis]
    dtcut   = vi_d.get('dtcut', lt)
    tr      = np.r_[:lt]
    tsep    = c3_d['tsep']      # no i_mask : tsep is fixed by c3_d spec
    # mask[(t-tsrc)%lt]
    mask_trel = make_mask_trel(lt, tr, tsep, dtcut)
    #mask0   = np.where((lt - tr) % lt <= dtcut, 1, 0)
    #mask1   = np.where( tr <= tsep + dtcut, 1, 0)
    #mask_trel = np.where(mask0 + mask1, 1, 0)   # [t-tsrc, t]
    theta_dat_d = dict(data_dir=data_in)
    def weight_qtopo_tsep(mlist) : 
        return weigh_discvacins_mask_trel(
                mlist, mask_trel, theta_dat_d, vi_d, latsize, t_axis)

    latcorr_bin_unbias(cfgkey_list, ama_list,
        dict(data_dir=data_out),
        dict(kind='c3vi', c3_d=c3_d, ins_d=ins_d, vi_d=vi_d),
        lc_d_in=dict(kind='c3', c3_d=c3_d, ins_d=ins_d),
        data_in=data_in,
        reweight_func=weight_qtopo_tsep,
        binsize=binsize,
        do_ama=False,
        VERBOSE=VERBOSE)

def runme_qtopo_rcut(cfgkey) :
    for qtopo_tag in qtopo_tag_list :
        h5file_in   = get_qtopo_orig_h5_file(cfgkey, qtopo_tag, data_in=data_in)
        h5f_in      = h5py.File(h5file_in, 'r')
        h5file_out  = get_qtopo_proc_h5_file(cfgkey, qtopo_tag, data_out=data_out)
        lhpd.mkpath(os.path.dirname(h5file_out))
        h5f_out     = h5py.File(h5file_out, 'a')
        h5k         = "/cfg%s/%s/qtopo_imp5li" % (cfgkey, qtopo_tag)
        qtopo       = h5f_in[h5k].value

        rcut_axes  = lhpd.list_del(range(len(latsize)), [t_axis])
        print("# %s -> %s[%s] convo along %s" % (h5file_in, h5file_out, h5k, str(rcut_axes)))

        qtopo_xsum  = qtopo.sum(axis=tuple(rcut_axes))
        h5k_xsum    = "%s/xsum" % (h5k,)
        try : del h5f_out[h5k]
        except : pass
        h5f_out[h5k_xsum]   = qtopo_xsum
        h5f_out[h5k_xsum].attrs['t_axis']   = t_axis
        h5f_out["%s/sum" % (h5k,)]  = qtopo_xsum.sum()
        print("%s[%s] Q=%f" % (h5file_out, h5k, qtopo_xsum.sum()))

        h5_save_sum_rcut(h5f_out, h5k, qtopo, qtopo_rcut_list, axes=rcut_axes)

        h5f_out.close()

def np_calc_corr(a, b) :
    """ compute res[x] = (1/V) \sum_y a(y) b^*(y+x) 
    """
    a   = np.asarray(a)
    b   = np.asarray(b)
    assert a.shape == b.shape
    vol = np.prod(a.shape)
    assert(0 < vol)
    a_f = np.fft.fftn(a)
    b_f = np.fft.fftn(b)
    return np.fft.fftn(a_f * b_f.conj()) / vol**2 #sic!

def runme_qtopo_corr(cfgkey, VERBOSE=False) :

    if VERBOSE : print("# runme_qtopo_corr : ", cfgkey)
    dat_d   = dict(cfgkey=cfgkey)
    for qtopo_gf, qtopo_qop in it.product(qtopo_tag_list, qtopo_qop_list) :
        lc_d        = dict(kind='qtopo', gf=qtopo_gf, qop=qtopo_qop)
        h5file_in, h5kpath_in   = get_lcdb_loc(
                dictnew(dat_d, data_dir=data_in), dictnew(lc_d, dkind='orig'))
        h5f_in      = h5py.File(h5file_in, 'r')
        h5file_out, h5kpath_out  = get_lcdb_loc(
                dictnew(dat_d, data_dir=data_out), dictnew(lc_d, dkind='corr'))
        lhpd.mkpath(os.path.dirname(h5file_out))
        h5f_out     = h5py.File(h5file_out, 'a')
        qtopo       = h5f_in[h5kpath_in].value

        qtopo_corr  = np_calc_corr(qtopo, qtopo)
        a1, a2      = qtopo_corr.real, qtopo_corr.imag
        a1n2        = (a1**2).sum()
        a2n2        = (a2**2).sum()
        if VERBOSE : print("|qtopo_corr|2=%e(re)+%e(im)" % ((a1**2).sum(), (a2**2).sum()))
        assert(np.allclose(a2 / np.sqrt(a1n2), 0))

        lhpd.h5_io.h5_purge_keys(h5f_out, [h5kpath_out])
        h5f_out[h5kpath_out] = a1

        h5f_out.flush()
        h5f_out.close()

def runme_qtopo_corravg(cfglist, qtopo_gf, qtopo_qop, VERBOSE=False) :
    n_data = len(cfglist)

    print("# #cfglist=%d %s %s" % (len(cfglist), qtopo_gf, qtopo_qop))
    lc_d = dict(kind='qtopo', gf=qtopo_gf, qop=qtopo_qop)
    in_list = [ get_lcdb_loc(dict(cfgkey=c, data_dir=data_in), 
                             dictnew(lc_d, dkind='corr'))
                for c in cfglist ]

    h5fname_out, h5kpath_out  = get_lcdb_loc(
            dict(data_dir=data_out), dictnew(lc_d, dkind='corravg'))
    if VERBOSE : print("# >> %s[%s]" % (h5fname_out, h5kpath_out))
    lhpd.mkpath(os.path.dirname(h5fname_out))

    h5_save_r2dep(h5fname_out, "%s/rset" % (h5kpath_out,), in_list, 
            binsize=qtopo_corravg_binsize, rmax=qtopo_corravg_rmax, 
            VERBOSE=VERBOSE)

def runme_unbias_posnpr(cfgkey, VERBOSE=False) : 
    csrclist_ex = np.array(make_csrcgrp_list('ex', cfgkey)).reshape((-1,4))
    csrclist_ap = np.array(make_csrcgrp_list('sl', cfgkey)).reshape((-1,4))
    ml_ex   = np.array([ (cfgkey, c) for c in csrclist_ex ], 
                dtype=lhpd.h5_io.h5_meas_dtype)
    ml_ap   = np.array([ (cfgkey, c) for c in csrclist_ap ], 
                dtype=lhpd.h5_io.h5_meas_dtype)
    m_ap, m_ex = lhpd.h5_io.h5_meas_match(ml_ap, ml_ex)
    m_ap_list = list(m_ap)
    avg     = 0
    n_avg   = 0
    delta   = 0
    n_delta = 0
    
    for posnpr_tag in posnpr_tag_list :
        print("# %s %s" % (cfgkey, posnpr_tag))
        for i_csrc, csrc in enumerate(csrclist_ap) :
            # get sloppy sample
            h5fname_in, h5kpath_in   = get_posnpr_orig_h5_file_kpath(cfgkey, ama_sl, csrc, posnpr_tag)
            if VERBOSE : print("# << %s[%s]" % (h5fname_in, h5kpath_in))
            h5f_in  = h5py.File(h5fname_in, 'a')
            corr    = h5f_in[h5kpath_in].value
            h5f_in.close()
            corr    = lhpd.np_circshift_coord(corr, csrc)
            avg     = avg + corr
            n_avg   += 1

            # ama bias correction?
            i_map   = None
            try : i_map = m_ap_list.index(i_csrc)
            except ValueError : pass
            if not None is i_map :
                h5fname_in_ex, h5kpath_in_ex   = get_posnpr_orig_h5_file_kpath(
                        cfgkey, ama_ex, csrc, posnpr_tag)
                if VERBOSE : print("# << %s[%s]" % (h5fname_in_ex, h5kpath_in_ex))
                h5f_in_ex   = h5py.File(h5fname_in_ex, 'a')
                corr_ex     = h5f_in_ex[h5kpath_in_ex].value
                h5f_in_ex.close()
                corr_ex     = lhpd.np_circshift_coord(corr_ex, csrc)
                delta       = delta + (corr_ex - corr)
                n_delta     += 1

        assert(0 < n_delta)
        avg     /= n_avg
        assert(0 < n_avg)
        delta   /= n_delta
        res = avg + delta
        
        h5fname_out, h5kpath_out  = get_posnpr_unbias_h5_file_kpath(cfgkey, posnpr_tag)
        if VERBOSE : print("# >> %s[%s]" % (h5fname_out, h5kpath_out))
        lhpd.mkpath(os.path.dirname(h5fname_out))
        h5f_out = h5py.File(h5fname_out, 'a')
        lhpd.h5_io.h5_purge_keys(h5f_out, [h5kpath_out])
        h5f_out.create_dataset(h5kpath_out, 
                data=np.array(res, dtype=np.complex64), dtype=np.complex64,
                fletcher32=True)
        h5f_out.flush()
        h5f_out.close()


def list_argsort(l, cmp=cmp) :
    il = [ c for c in enumerate(l) ]
    il.sort(cmp=lambda x,y:cmp(x[1],y[1]))
    return [ c[0] for c in il ]

def list_slice(l, il) : return [ l[i] for i in il ]

equal_bound = np_equal_bound
def list_classify(objlist, cmp=cmp) :
    """ return [ ([idx], classrep), ...], ordered by classrep according to cmp
    """
    isort_objlist = list_argsort(objlist, cmp=cmp)
    objlist_sorted = [objlist[i] for i in isort_objlist ]
    res = [ (isort_objlist[il : iu], v)
            for il, iu, v in equal_bound(objlist_sorted, cmp=cmp) ]
    return res

def list_sort_classify(objlist, cmp=cmp) :
    """ return (isort, [ (ilo, ihi, classrep), ...]), ordered by classrep according to cmp
    """
    isort_objlist = list_argsort(objlist, cmp=cmp)
    objlist_sorted = [ objlist[i] for i in isort_objlist ]
    return isort_objlist, list(equal_bound(objlist_sorted, cmp=cmp))

def np_mgrid_coord(ls) :
    """ return [mu, ix,iy,iz,it] = c[mu] """
    xx  = tuple(np.r_[:lx] for lx in ls)
    return np.array(np.meshgrid(*xx, indexing='ij'))

def np_mgrid_delta(ls, csrc=None) :
    """ return [mu, ix,iy,iz,it] = (c-csrc)[mu], min |c-csrc|"""
    ls  = np.asarray(ls)
    if None is csrc : csrc = np.zeros_like(ls)
    else :            csrc = np.asarray(csrc)
    dx  = tuple((np.r_[:lx] + lx - csrc[i] + lx//2) % lx - lx//2
                for i, lx in enumerate(ls))
    return np.array(np.meshgrid(*dx, indexing='ij'))

def np_mgrid_delta_classify(ls, csrc=None) :
    xlist   = np.abs(np_mgrid_delta(ls, csrc=csrc).reshape(
                len(ls), -1).T)
    xlist.sort(-1)
    xlist   = [ tuple(x) for x in xlist ]
    return list_classify(xlist)
def np_mgrid_delta_sort_classify(ls, csrc=None) :
    xlist   = np.abs(np_mgrid_delta(ls, csrc=csrc).reshape(
                len(ls), -1).T)
    xlist.sort(-1)
    xlist   = [ tuple(x) for x in xlist ]
    return list_sort_classify(xlist)


# TODO move to lib numpy_extra
def h5_save_r2dep(h5fname_out, h5ko, in_list_fn_kp, 
        binsize=1, rmax=None, VERBOSE=False) :
    """ save dependence on r2 """
    assert(0 < len(in_list_fn_kp))
    fn0, kp0 = in_list_fn_kp[0]
    h5f0    = h5py.File(fn0, 'r')
    dset0   = h5f0[kp0]
    ls      = dset0.shape
    dtype   = dset0.dtype
    h5f0.close()

    # subsets of H(4)-equivalent vectors:  
    # isort_rset=argsort_by_class, rset=[ (i_lo, i_hi, classrep), ...]
    isort_rset, rset = np_mgrid_delta_sort_classify(ls)
    # order subsets by r2
    r2list  = (np.array([ r[2] for r in rset ])**2).sum(-1)
    isort_r2= r2list.argsort()
    r2list  = r2list[isort_r2]
    rset    = list_slice(rset, isort_r2)
    # cutoff large r2
    if not None is rmax : 
        i_rset_max = np.searchsorted(r2list, rmax**2, side='right')
        rset    = rset[:i_rset_max]

    n_rset  = len(rset)
    ilist   = [ (r[0], r[1]) for r in rset ]
    rlist   = np.array([ r[2] for r in rset ])
    #print("# rlist = ", rlist[:100])
    #print("# r2list= ", r2list[:100])
    #print("# r2list= ", (rlist**2).sum(-1)[:100])
    #print("# ilist = ", ilist[:100])

    cnt_rset= np.array([ ihi - ilo for ilo, ihi in ilist ])
    assert( 0 < n_rset )
    assert( (0 < cnt_rset).all() )

    n_data  = len(in_list_fn_kp)
    res     = np.empty((n_data, n_rset), dtype=dtype)
    for i_data, (fn, kp) in enumerate(in_list_fn_kp) :
        if VERBOSE: print("# << %s[%s]" % (fn, kp))
        h5f_in  = h5py.File(fn, 'r')
        v       = h5f_in[kp].value
        h5f_in.close()
        srt_vf  = v.flatten()[isort_rset]
        res[i_data] = np.array([ srt_vf[ilo:ihi].sum(0)
                    for ilo, ihi in ilist ]) / cnt_rset
    
    n_bins = n_data // binsize
    if 0 != n_data % binsize : 
        print("WARNING: last %d data are ignored in %d-binning" % (
                n_data % binsize, binsize)
    res = res[ : n_bins * binsize].reshape(n_bins, binsize, n_rset).mean(1))
    if VERBOSE: print("# >> %s[%s]" % (h5fname_out, h5ko))
    lhpd.mkpath(os.path.dirname(h5fname_out))
    h5f     = h5py.File(h5fname_out, 'a')
    lhpd.h5_io.h5_purge_keys(h5f, [h5ko])
    h5d     = h5f.create_dataset("%s/avg" % h5ko,
            data=res, dtype=dtype, fletcher32=True)
    h5d.attrs['latsize']    = ls
    h5f["%s/rset_cnt" % h5ko]   = cnt_rset
    h5f["%s/rset_list" % h5ko]  = np.array(rlist)
    h5f["%s/rset_r2" % h5ko]    = (rlist**2).sum(-1)
    h5f.flush()
    h5f.close()
        

def runme_collect_posnpr(cfglist, VERBOSE=False) : 
    n_data = len(cfglist)

    for posnpr_tag in posnpr_tag_list :
        print("# #cfglist=%d %s" % (len(cfglist), posnpr_tag))

        in_list = [ get_posnpr_unbias_h5_file_kpath(c, posnpr_tag)
                    for c in cfglist ]

        h5fname_out, h5kpath_out  = get_posnpr_collect_h5_file_kpath(posnpr_tag)
        if VERBOSE : print("# >> %s[%s]" % (h5fname_out, h5kpath_out))
        lhpd.mkpath(os.path.dirname(h5fname_out))

        h5_save_r2dep(h5fname_out, "%s/rset" % (h5kpath_out,), 
                in_list, VERBOSE=VERBOSE)    
        # TODO put other collect types here (eg mom.projection)

        
# vvvvvvvvvv XXX OLD STUFF XXX vvvvvvvvvvvvvvv
if False :
    def runme_nanscanAff_c2pt(cfgkey, ama, sm_srcsnk,
                    had='proton',
                    psnk_list=c2pt_psnk_list,
                    tpol_list=c2pt_tpol_list ) :
        """ scan Aff files for NaNs """
        csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)

        hskf    = make_c2pt_filekpath_func(cfgkey, ama, had, sm_srcsnk)
        nanscanAff_c2pt(latsize, hskf, cfgkey, csrcgrp_list, psnk_list, tpol_list)


    def runme_merge_c2pt(cfgkey_list, ama, sm_srcsnk,
                    had='proton',
                    VERBOSE=False) :
        h5_kpath  = "/c2pt/%s/%s" % (sm_srcsnk, had)
        
        h5_list  = [ (get_c2pt_h5_file(c, ama, had, sm_srcsnk),
                        '/cfg%s/%s' % (c, h5_kpath)) 
                        for c in cfgkey_list ]
        h5_file  = get_c2pt_all_h5_file(ama, had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file))
        lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)


    def runme_nanscanAff_c2pt_vacins(cfgkey, ama, sm_srcsnk,
                    baryon_bc_t=ferm_bc[3]**3,
                    had='proton',
                    hslab_len=c2pt_hslab_len,
                    psnk_list=c2pt_psnk_list,
                    tpol_list=c2pt_tpol5_list,
                    vacins_spec_list=[],
                    attrs_kw={}) :
        """ convert CP-even 2pt functions 
        """
        csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
        for vispec in vacins_spec_list :
            vacins  = vispec['vacins']
            hskf    = make_c2pt_vacins_filekpath_func(cfgkey, ama, had, sm_srcsnk, vispec)
            nanscanAff_c2pt(latsize, hskf, cfgkey, csrcgrp_list, psnk_list, tpol_list)

    def runme_merge_c2pt_vacins(cfgkey_list, ama, sm_srcsnk,
                    had='proton',
                    vacins_spec_list=[],
                    VERBOSE=False) :
        # since Tpol is TgN, keep them all together
        for vispec in vacins_spec_list :
            vacins  = vispec['vacins']
            h5_kpath  = "/c2pt_%s/%s/%s/%s" % (vacins, sm_srcsnk, had, vacins_meta_str_k(vispec))
                
            h5_list  = [ (get_c2pt_vacins_h5_file(c, ama, had, sm_srcsnk, vispec),
                            '/cfg%s/%s' % (c, h5_kpath)) 
                            for c in cfgkey_list ]
            h5_file  = get_c2pt_vacins_all_h5_file(ama, had, sm_srcsnk, vispec)

            lhpd.mkpath(os.path.dirname(h5_file))
            lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)

    def runme_nanscanAff_bb(cfgkey, ama, sm_srcsnk, 
                    baryon_bc_t=ferm_bc[3]**3,
                    had='proton',
                    tpol_list=c3pt_tpol_list,
                    psnk_list=c3pt_psnk_list,
                    tsep_list=c3pt_tsep_list,
                    qext_list=c3pt_qext_list,
                    flav_cur_list=flav_cur_list,
                    lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                    tsnk_full=c3pt_tsnk_full, # whether to chop the bbs into coherent hslabs
                    attrs_kw={}) :
        
        csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
        lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

        for (tsep, psnk, tpol, flav_cur) in it.product(
                tsep_list, psnk_list, tpol_list, flav_cur_list) :
            bbkf    = make_bb_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, flav_cur)
            nanscanAff_bb(latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                        qext_list, lpath_list)

    def runme_merge_op(cfgkey_list, ama, sm_srcsnk,
                    had='proton',
                    tpol_list=c3pt_tpol_list,
                    psnk_list=c3pt_psnk_list,
                    tsep_list=c3pt_tsep_list,
                    qext_list=c3pt_qext_list,
                    flav_cur_list=flav_cur_list,
                    op_list=c3pt_op_list,
                    VERBOSE=False,
                    attrs_kw={}) :

        for (tsep, psnk, tpol, flav_cur, op) in it.product(
                tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

            ir_list = lhpd.latcorr.op_default_ir_list(op)
            for ir_name, ir_scale in ir_list :
                print("# %s %s dt=%d %s %s %s %s" % (
                        had, str(psnk), tsep, tpol, flav_cur, op, ir_name))

                h5_kpath    = "/op/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                        sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, op, ir_name)

                h5_list     = [ (get_op_h5_file(c, ama, psnk, tsep, 
                                                sm_srcsnk, had, tpol, flav_cur, op), 
                                '/cfg%s/%s' % (c, h5_kpath)) 
                                for c in cfgkey_list ]
                h5_file     = get_op_all_h5_file(ama, psnk, tsep, 
                                                 sm_srcsnk, had, tpol, flav_cur, op)
                lhpd.mkpath(os.path.dirname(h5_file))
                lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)

    def runme_nanscanAff_bb_vacins(cfgkey, ama, sm_srcsnk, 
                    baryon_bc_t=ferm_bc[3]**3,
                    had='proton',
                    tpol_list=c3pt_tpol5_list,
                    psnk_list=c3pt_psnk_list,
                    tsep_list=c3pt_tsep_list,
                    qext_list=c3pt_qext_list,
                    flav_cur_list=flav_cur_list,
                    lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                    tsnk_full=c3pt_tsnk_full, # whether to chop the bbs into coherent hslabs
                    vacins_spec_list=[],
                    attrs_kw={}) :
        
        csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
        lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

        for vispec in vacins_spec_list :
            vacins  = vispec['vacins']
            for (tsep, psnk, tpol, flav_cur) in it.product(
                    tsep_list, psnk_list, tpol_list, flav_cur_list) :
                bbkf    = make_bb_vacins_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, 
                            flav_cur, vispec)
                nanscanAff_bb(latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                            qext_list, lpath_list)

    def runme_merge_op_vacins(cfgkey_list, ama, sm_srcsnk,
                    had='proton',
                    tpol_list=c3pt_tpol_list,
                    psnk_list=c3pt_psnk_list,
                    tsep_list=c3pt_tsep_list,
                    qext_list=c3pt_qext_list,
                    flav_cur_list=flav_cur_list,
                    op_list=c3pt_op_list,
                    vacins_spec_list=[],
                    ama_list=ama_list,
                    VERBOSE=False,
                    attrs_kw={}) :

        for vispec in vacins_spec_list :
            vacins  = vispec['vacins']
            for (tsep, psnk, tpol, flav_cur, op) in it.product(
                    tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

                ir_list = lhpd.latcorr.op_default_ir_list(op)
                for ir_name, ir_scale in ir_list :
                    print("# %s %s dt=%d %s %s %s %s %s" % (
                            had, str(psnk), tsep, tpol, flav_cur, op, ir_name, str(vispec)))

                    h5_kpath  = "/op_%s/%s/%s_%s/%s_dt%d/%s/%s/%s/%s" % (
                            vacins, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                            flav_cur, op, ir_name, vacins_meta_str_k(vispec))
                    if 'theta'   == vacins : raise NotImplementedError

                    else :

                        h5_list  = [ (get_op_vacins_h5_file(c, ama, psnk, tsep, sm_srcsnk, 
                                                    had, tpol, flav_cur, op, vispec), 
                                        '/cfg%s/%s' % (c, h5_kpath)) 
                                        for c in cfgkey_list ]
                        h5_file  = get_op_vacins_all_h5_file(ama, psnk, tsep, sm_srcsnk, 
                                                         had, tpol, flav_cur, op, vispec)
                        lhpd.mkpath(os.path.dirname(h5_file))
                        lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)


# ^^^^^^^^^^ XXX OLD STUFF XXX ^^^^^^^^^^^^^^^

##############################################################################
##############################################################################

def print_err(what, i_sj, *clist) :
    #def strip_longopt(c) :
        #if isinstance(c, dict) :
            #c = dict(c)
            #purge_keys(c, [''])
        #else : return c
        
    #sys.stderr.write("%s[%d] %s {%s}\n" % (what, i_sj, ', '.join([ str(x) for x in clist] ))
    exc = sys.exc_info()
    sys.stderr.write("ERROR %s[%d]: %s\n" % (what, i_sj, repr(exc[1])))
    if exc[0] in [ 
            AssertionError, SyntaxError, NameError, NotImplementedError, IndentationError, 
            SystemError, TypeError, UnboundLocalError, ValueError, ZeroDivisionError,
            OSError, RuntimeError,
            KeyError, IndexError, AttributeError,
            KeyboardInterrupt, MemoryError ] :
        traceback.print_tb(exc[2])
        raise

if '__main__' == __name__ :
    what   = argv[1]
    jobid_list      = cfgkey_list

    def  subjob_iter(j_iter) :
        j_list  = list(j_iter)
        nj      = len(j_list)
        jj0     = ( sj_this    * nj) // sj_tot
        jj1     = ((sj_this+1) * nj) // sj_tot
        for j in j_list[jj0:jj1] : yield j

    if   'conv_c2pt' == what :
        for i_sj, (c, c2_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, 
                c2pt_spec_list))) :
            try : runme_conv_c2pt(c, c2_d, ama_list=ama_list)
            except : print_err(what, i_sj, (c, c2_d))

    elif what.startswith('conv_c2pt_') :
        s1 = strip_prefix(what, 'conv_c2pt_')
        for i_sj, (c, c2_d, vi_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, 
                c2pt_vi_spec_list, 
                vacins_spec_list_all[s1]))) :
            try : runme_conv_c2pt_vacins(c, c2_d, vi_d, ama_list=ama_list)
            except : print_err(what, i_sj, (c, c2_d, vi_d))

    elif 'conv_bb' == what :
        for i_sj, (c, c3_d, ins_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, 
                c3pt_spec_list, 
                bb_spec_list))) :
            try : runme_conv_c3pt_qbarq(c, c3_d, ins_d, ama_list=ama_list)
            except : print_err(what, i_sj, (c, c3_d, ins_d))

    elif what.startswith('conv_bb_') :
        s1 = strip_prefix(what, 'conv_bb_')
        for i_sj, (c, c3_d, ins_d, vi_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, 
                c3pt_vi_spec_list, 
                bb_vi_spec_list, 
                vacins_spec_list_all[s1]))) :
            try : runme_conv_c3pt_vacins_qbarq(c, c3_d, ins_d, vi_d, ama_list=ama_list)
            except : print_err(what, i_sj, (c, c3_d, ins_d, vi_d))
    
    elif 'calc_op' == what :
        for i_sj, (c, c3_d, ins_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, 
                c3pt_spec_list, 
                op_spec_list))) :
            try : runme_calc_save_op(c, c3_d, ins_d, ama_list=ama_list)
            except : print_err(what, i_sj, (c, c3_d, ins_d))

    elif what.startswith('calc_op_') :
        s1 = strip_prefix(what, 'calc_op_')
        for i_sj, (c, c3_d, ins_d, vi_d) in enumerate(subjob_iter(it.product(
                cfgkey_list, 
                c3pt_vi_spec_list, 
                op_vi_spec_list, 
                vacins_spec_list_all[s1]))) :
            try : runme_calc_save_op_vacins(c, c3_d, ins_d, vi_d, ama_list=ama_list)
            except : print_err(what, i_sj, (c, c3_d, ins_d, vi_d))
    
    ########################################################################
    # XXX binx_* parallelized with --sj
    elif 'binx_c2pt' == what :
        for i_sj, (c2_d,) in enumerate(subjob_iter(
                it.product(c2pt_spec_list))) :
            try : latcorr_bin_unbias(cfgkey_list, ama_list, 
                    dict(data_dir=data_out),
                    dict(kind='c2', c2_d=c2_d),
                    data_in=data_in,
                    binsize=binsize,
                    do_ama=False,
                    VERBOSE=True)
            except : print_err(what, i_sj, (c2_d,))

    elif 'binx_c2pt_theta' == what : 
        s1 = strip_prefix(what, 'binx_c2pt_')
        for i_sj, (c2_d, vi_d) in enumerate(subjob_iter(it.product(
                c2pt_vi_spec_list, 
                vacins_spec_list_all[s1]))) :
            try : runme_binx_c2pt_theta(cfgkey_list, ama_list,
                    c2_d, vi_d, VERBOSE=True)
            except : print_err(what, i_sj, (c2_d, vi_d))
            
    elif what.startswith('binx_c2pt_') :
        s1 = strip_prefix(what, 'binx_c2pt_')
        for i_sj, (c2_d, vi_d) in enumerate(subjob_iter(it.product(
                c2pt_vi_spec_list, 
                vacins_spec_list_all[s1]))) :
            try : latcorr_bin_unbias(cfgkey_list, ama_list, 
                    dict(data_dir=data_out),
                    dict(kind='c2vi', c2_d=c2_d, vi_d=vi_d),
                    data_in=data_in,
                    binsize=binsize,
                    VERBOSE=True)
            except : print_err(what, i_sj, (c2_d, vi_d))

    elif 'binx_c3ts_theta' == what : 
        s1 = strip_prefix(what, 'binx_c3ts_')
        for i_sj, (c2_d, vi_d) in enumerate(subjob_iter(it.product(
                c3ts_c2_spec_list, 
                vacins_spec_list_all[s1]))) :
            try : runme_binx_c3ts_theta(cfgkey_list, ama_list,
                    c2_d, vi_d, VERBOSE=True)
            except : print_err(what, i_sj, (c2_d, vi_d))
            print("*** done i_sj =", i_sj)

    elif 'binx_op' == what :
        for i_sj, (c3_d, ins_d) in enumerate(subjob_iter(it.product(
                c3pt_spec_list, 
                op_spec_list))) :
            for ir in lhpd.latcorr.op_default_ir_list(ins_d['op']) :
                try : latcorr_bin_unbias(cfgkey_list, ama_list, 
                        dict(data_dir=data_out),
                        dict(kind='c3', c3_d=c3_d, ins_d=dictnew(ins_d, ir=ir[0])),
                        data_in=data_in,
                        binsize=binsize,
                        do_ama=False,
                        VERBOSE=True)
                except : print_err(what, i_sj, (c3_d, ins_d))

    elif 'binx_op_theta' == what : 
        s1 = strip_prefix(what, 'binx_op_')
        for i_sj, (c3_d, ins_d, vi_d) in enumerate(subjob_iter(it.product(
                c3pt_vi_spec_list, 
                op_vi_spec_list, 
                vacins_spec_list_all[s1]))) :
            for ir in lhpd.latcorr.op_default_ir_list(ins_d['op']) :
                try : runme_binx_op_theta(cfgkey_list, ama_list,
                        c3_d, dictnew(ins_d, ir=ir[0]), vi_d, 
                        VERBOSE=True)
                except : print_err(what, i_sj, (c3_d, ins_d, vi_d))

    elif what.startswith('binx_op_') :
        s1 = strip_prefix(what, 'binx_op_')
        for i_sj, (c3_d, ins_d, vi_d) in enumerate(subjob_iter(it.product(
                c3pt_vi_spec_list, 
                op_vi_spec_list, 
                vacins_spec_list_all[s1]))) :
            for ir in lhpd.latcorr.op_default_ir_list(ins_d['op']) :
                try : latcorr_bin_unbias(cfgkey_list, ama_list, 
                        dict(data_dir=data_out),
                        dict(kind='c3vi', c3_d=c3_d, ins_d=dictnew(ins_d, ir=ir[0]), vi_d=vi_d),
                        data_in=data_in,
                        binsize=binsize,
                        VERBOSE=True)
                except : print_err(what, i_sj, (c3_d, ins_d, vi_d))

    ########################################################################
    # XXX merg_* parallelized with --sj
    elif 'merge_c2pt' == what :
        for i_sj, (c2_d,) in enumerate(subjob_iter(it.product(
                c2pt_spec_list))) :
            for ama in ama_list :
                try : latcorr_merge(cfgkey_list,
                        dict(data_dir=data_out, ama=ama),
                        dict(kind='c2', c2_d=c2_d),
                        data_in=data_in,
                        VERBOSE=True)
                except : print_err(what, i_sj, (c2_d,))
    elif 'merge_c2pt_theta' == what : raise NotImplementedError
    elif what.startswith('merge_c2pt_') :
        s1 = strip_prefix(what, 'merge_c2pt_')
        for i_sj, (c2_d, vi_d) in enumerate(subjob_iter(it.product(
                c2pt_spec_list,
                vacins_spec_list_all[s1]))) :
            for ama in ama_list :
                try : latcorr_merge(cfgkey_list,
                        dict(data_dir=data_out, ama=ama),
                        dict(kind='c2vi', c2_d=c2_d, vi_d=vi_d),
                        data_in=data_in,
                        VERBOSE=True)
                except : print_err(what, i_sj, (c2_d, vi_d))

    elif 'merge_op' == what :
        for i_sj, (c3_d, ins_d) in enumerate(subjob_iter(it.product(
                c3pt_spec_list,
                op_spec_list))) :
            for ama in ama_list :
                for ir in lhpd.latcorr.op_default_ir_list(ins_d['op']) :
                    try : latcorr_merge(cfgkey_list,
                            dict(data_dir=data_out, ama=ama),
                            dict(kind='c3', c3_d=c3_d, ins_d=dictnew(ins_d, ir=ir[0])),
                            data_in=data_in,
                            VERBOSE=True)
                    except : print_err(what, i_sj, (c3_d, ins_d))
    elif 'merge_op_theta' == what : raise NotImplementedError
    elif what.startswith('merge_op_') :
        s1 = strip_prefix(what, 'merge_op_')
        for i_sj, (c3_d, ins_d, vi_d) in enumerate(subjob_iter(it.product(
                c3pt_spec_list,
                c3pt_vi_spec_list,
                vacins_spec_list_all[s1]))) :
            for ama in ama_list :
                for ir in lhpd.latcorr.op_default_ir_list(ins_d['op']) :
                    try : latcorr_merge(cfgkey_list,
                            dict(data_dir=data_out, ama=ama),
                            dict(kind='c3vi', c3_d=c3_d, ins_d=dictnew(ins_d, ir=ir[0]), vi_d=vi_d),
                            data_in=data_in,
                            VERBOSE=True)
                    except : print_err(what, i_sj, (c3_d, vi_d))


    ########################################################################

    # TODO implement select for different 
    # * lc_d.kind=c3,c2_vi,c3_vi, 
    # * dat_d.kind=conv,bin,ub
    # TODO better command spec? now {cmd=select}_{dat_d.kind=conv}_{lc_d.kind=c2pt}
    elif what.startswith('select_') :
        s1 = strip_prefix(what, 'select_')
        if s1.startswith('conv_') :
            s2 = strip_prefix(s1, 'conv_')
            if 'c2pt' == s2 :
                for i_sj, (c, ama, c2_d) in enumerate(subjob_iter(it.product(
                        cfgkey_list, 
                        ama_list, 
                        c2pt_spec_list))) :
                    try : latcorr_select(
                            dict(data_dir=data_out, cfgkey=c, ama=ama),
                            dict(kind='c2', c2_d=c2_d),
                            data_in=data_in)
                    except : print_err(what, i_sj, (c, ama, c2_d))

    ########################################################################
    elif 'qtopo_rcut' == what :
        for i_sj, (c,) in enumerate(subjob_iter(it.product(
                cfgkey_list))) :
            print("i_sj, c = ", (i_sj, c))
            try : runme_qtopo_rcut(c)
            except : print_err(what, i_sj, (c,))
    elif 'qtopo_corr' == what :
        for i_sj, (c,) in enumerate(subjob_iter(it.product(
                cfgkey_list))) :
            try: runme_qtopo_corr(c, VERBOSE=True)
            except : print_err(what, i_sj, (c,))
    elif 'qtopo_corravg' == what :
        for i_sj, (qtopo_gf, qtopo_qop) in enumerate(subjob_iter(it.product(
                qtopo_tag_list, qtopo_qop_list))) : 
            try: runme_qtopo_corravg(jobid_list, qtopo_gf, qtopo_qop, 
                    VERBOSE=True)
            except : print_err(what, i_sj, (qtopo_gf, qtopo_qop))

    ########################################################################
    elif 'unbias_posnpr' == what :
        for c in jobid_list :
            try: runme_unbias_posnpr(c)
            except : print_err(what, c)
    elif 'collect_posnpr' == what :
        try: runme_collect_posnpr(jobid_list, VERBOSE=True)
        except : print_err(what)

# vvvvvvvvvv XXX OLD STUFF XXX vvvvvvvvvvvvvvv

    elif what.startswith('nanscanAff_') : 
        s1 = strip_prefix(what, 'nanscanAff_')
        if   'c2pt' == s1 : 
            for c, ama, sm_srcsnk in subjob_iter(it.product(
                        cfgkey_list, ama_list, ['SS', 'SP'])) :
                print("# %s %s %s " % (c, ama, sm_srcsnk))
                runme_nanscanAff_c2pt(c, ama, sm_srcsnk)

        elif s1.startswith('c2pt_') :
            s2 = strip_prefix(s1, 'c2pt_')
            for c, ama, sm_srcsnk in subjob_iter(it.product(
                        cfgkey_list, ama_list, ['SS', 'SP'])) :
                print("# %s %s %s %s" % (c, ama, sm_srcsnk, s1))
                runme_nanscanAff_c2pt_vacins(c, ama, sm_srcsnk, 
                        vacins_spec_list=vacins_spec_list_all[s2])

        elif 'bb'   == s1 : 
            for c, ama, sm_srcsnk in subjob_iter(it.product(
                    cfgkey_list, ama_list, ['SS'])) :
                print("# %s %s %s " % (c, ama, sm_srcsnk))
                runme_nanscanAff_bb(c, ama, sm_srcsnk)

        elif s1.startswith('bb_') :
            s2 = strip_prefix(s1, 'bb_')
            for c, ama, sm_srcsnk in subjob_iter(it.product(
                    cfgkey_list, ama_list, ['SS'])) :
                print("# %s %s %s %s" % (c, ama, sm_srcsnk, s1))
                runme_nanscanAff_bb_vacins(c, ama, sm_srcsnk, 
                        vacins_spec_list=vacins_spec_list_all[s2])

    else : raise ValueError(what)
# ^^^^^^^^^^ XXX OLD STUFF XXX ^^^^^^^^^^^^^^^
    
    
    print("DONE sj = ", sj_this, sj_tot)
