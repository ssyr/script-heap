from __future__ import print_function
from past.builtins import basestring
from lhpd.latcorr import *

# XXX moved to here from ~/Results/cedm-*/polariz/polariz.py
# XXX edit HERE

# XXX 2016/09/19 multiply cols0,1 by 'al5hat'
# XXX ALREADY divided by (-1j) !!! 
# XXX   cosistent with writeup signs to match 
# XXX       S_cpviol = S_cpeven + i*d*A_cpbar, 
# XXX       e^{-S_cpviol} = e^{-S_cpeven}(1 - i*d*A_cpbar)
# XXX       <N J Nbar>_{cpviol} = <N J Nbar>_{cpeven} - i*d*<N J Nbar A_cpbar>_{cpeven}
# XXX       ff_coeff = ( I*Tr[{T,gamma5}.S'.Gamma_{cpeven}.S] + Tr[T.S'.Gamma_cpodd.S] ) / (-I)
#
# FIXME split into ff_coeff_cpeven = Tr[{T,gamma5}.S'.\Gamma_even.S] = ff_coeff(tpol={T,gamma5}...) - old tensor1 spinor structure
# FIXME and ff_coeff_cpodd = Tr[T.S'.\Gamma_odd.S] - only this one requires new spinor structure
def get_coeff_cpviol_order1(massN, p4_in, p4_out, tpol, tpol_c2, **kw) :
    """ 1-st order in parity breaking
        [ F1(*al5hat), F2(*al5hat), FAhat, F3hat] (hat==divided by d)
        default flags set correct new coeffs
        user must multiply col 1,2 by al5hat=alfive/d
    """
    # 
    have_tpol_mix   = kw.get('have_tpol_mix', True)
    have_vertex_mix = kw.get('have_vertex_mix', False)

    p4_in   = np.asarray(p4_in)
    p4_out  = np.asarray(p4_out)

    N       = None
    p4      = (p4_out + p4_in) / 2.    # XXX perhaps O(a^?) errors introduced
    p4euc   = p4 * em_contravar
    q4      = (p4_out - p4_in)
    q4euc   = q4 * em_contravar
    #q4_sq   = vec4mink_norm2(q4)
    q4euc_sq= (q4euc**2).sum()

    #ipdb.set_trace()
    ferm_matr_in    = twopt_spinmatrix_ferm(massN, p4_in)
    ferm_matr_out   = twopt_spinmatrix_ferm(massN, p4_out)
    denomsq_in      = np.dot(tpol_c2, ferm_matr_in).trace().real
    denomsq_out     = np.dot(tpol_c2, ferm_matr_out).trace().real
    denom_c2        = math.sqrt(4. * p4_out[-1] * denomsq_out * p4_in[-1] * denomsq_in)

    aux             = np_ddot(ferm_matr_in, tpol, ferm_matr_out)
    tpolmix         = 1j * (np.dot(gamma5, tpol) + np.dot(tpol, gamma5))    # 2017/06/12 move 1j to tpolmix
    auxmix          = np_ddot(ferm_matr_in, tpolmix, ferm_matr_out)

    # F1 : tpolmix
    tr_tpolmix_gamma= np.array([ np.dot(auxmix, gamma[mu]).trace()
                                 for mu in range(4) ])
    tr_gamma_mix    = 0.

    # F2 : tpolmix + vertexmix
    sigmaq          = sigma_dot_mom_euc(q4)
    tr_tpolmix_sigmaq= np.array([ np_ddot(auxmix, sigmaq[mu]).trace()
                                 for mu in range(4) ])
    tr_sigmaq_mix   = 1j*np.array([ np_ddot(aux, sigmaq[mu], 2*gamma5).trace() # 2017/06/12 move 1j to sigmaq_mix
                                     for mu in range(4) ])
    # FA
    #tr_g5           = np.dot(aux, gamma5).trace()
    tr_gamma_g5     = np.array([ np_ddot(aux, gamma[mu], gamma5).trace()
                                 for mu in range(4) ])
    tr_qslash_g5    = np_ddot(aux, vec4_slash_euc(q4euc), gamma5).trace()
    # F3
    sigma5q         = sigma5_dot_mom_euc(q4)
    tr_sigma5q      = np.array([ np_ddot(aux, sigma5q[mu]).trace()
                                 for mu in range(4) ])

    # [mu, i_ff], i_ff = 0:F1, 1:F2, 2:FA, 3:F3
    res = np.zeros(shape=(4, 4), dtype=np.complex128)
    if have_tpol_mix :
        res[:, 0]   += tr_tpolmix_gamma                     # F1 tpol mix
        res[:, 1]   += tr_tpolmix_sigmaq * (0.5 / massN)    # F2 tpol mix
    if have_vertex_mix :
        res[:, 1]   += tr_sigmaq_mix     * (0.5 / massN)    # F2 direct mix (incorrect)

    # sic! minus sign in the 1st term because q4euc**2 = - q4mink**2
    #res[:, 2]       = -2.j / massN *tr_g5 *q4euc - q4_sq / massN**2 *tr_gamma_g5    # FA OLD
    #res[:, 2]       =  -tr_qslash_g5 / massN**2 *q4euc + q4euc_sq / massN**2 *tr_gamma_g5    # FA in Euc 
    # FIXME DEBUG manually set to zero
    res[:, 2]       = 0.
    # sic! fixed conventions (F2 - i gamma5 F_3) -> (F2 + i gamma5 F_3) 2017/06/12
    res[:, 3]       = tr_sigma5q * 1j    * (0.5 / massN)    # F3

    # XXX correcting the overall factor from CPv operators convention
    # 2017/06/12 ?verify? *1j is from operator definition qbar gamma5 sigma, 
    #           and minus sign from correlator expansion with e^{-(S+S_CPv)}
    # S = S_{QCD} + \sum_i {i O_{CPv,i}}
    # O_{CPv,cedm} = \int_{V4} qbar gamma5 (sigma.G) q
    # O_{CPv,theta}= \int_{V4} G.Gdual (Real-valued)
    return res / (-1.j) / denom_c2


def make_ff_eqnmat_precond_f2f_cpviol(op, mlat, latsize, mcgrp, ir_list, **kw) :
    """ generate ff eq.matrix for all tpol, mc=(p3src,p3snk), ir=(ir_name, ir_scale)
        indexing is the direct product : flat([i_tpol, i_mc, i_ir, i_comp, i_reim])
        where i_comp is # of component for ir_list[i_ir], i_reim=0,1 for Real/Imag
            tpol_list   list of spin matrices to include (4x4 cplx; FIXME : extend to use tpol names?)
            mcgrp       list of mc_case objects containing p3src, p3snk, tpol
            ir_list     list of irreps to include ; the ir_scale is ignore
        XXX twisted momenta: twisting angles should be added to mc[0], mc[1]

        NOTE ! preconditioning only over in*out states with equivalent momenta and tpol; 
                otherwise, the 2pt function may be different
    """         
                
    have_space_comp  = kw.get('have_space_comp', True)
    have_time_comp   = kw.get('have_time_comp', True)
    
    n_ff    = 4#get_gff_number(op) 
    n_mc    = len(mcgrp)
    n_comp  = ir_list_comp_len(ir_list)
    # separate Re/Im
    sh_3pt  = (n_mc, n_comp, 2)
    n_c3pt  = n_mc * n_comp * 2
                         
    # momenta
    def make_mom_std(p) : 
        p = np.abs(p) 
        p.sort()
        return p

    mc_std_list = np.array([ ( make_mom_std(mc.get_p3src()), make_mom_std(mc.get_p3snk()))
                             for mc in mcgrp ])
    q2_list     = np.array([ lorentz_mom_transfer_sq(mlat, latsize, mc.get_p3src(), mc.get_p3snk()) 
                             for mc in mcgrp ])
    # sanity check to make sure that all ffs in the system correspond to the same Q2
    assert((np.abs(q2_list - q2_list[0]) < NONZERO_TOL).all())
    
    eq_mat = np.empty(sh_3pt + (n_ff,), np.float64)
    for i_mc, mc in enumerate(mcgrp) :
        p4_in   = lorentz_p4(mlat, latsize, mc.get_p3src())
        p4_out  = lorentz_p4(mlat, latsize, mc.get_p3snk())
        # FIXME pass separate matrix for 2pt, for cases when Tr[tpol]==0
        mat_mc  = get_coeff_cpviol_order1(
                        mlat, p4_in,  p4_out, mc.get_tpol5_matr(), mc.get_tpol_c2_matr(), 
                        **kw)
        mat_mc = np.where(np.abs(mat_mc) < NONZERO_TOL, 0, mat_mc)    # remove non-zero coeffs from roundoff 

        ir_sh = 0
        for (ir_name, ir_scale) in ir_list:
            ir_dim = H4_repr_dim[ir_name]
            mat_mc_ir = H4_repr_func[ir_name](mat_mc)
            #print(ir_name, mat_ir)
            eq_mat[i_mc, ir_sh : ir_sh + ir_dim, 0] = mat_mc_ir.real
            eq_mat[i_mc, ir_sh : ir_sh + ir_dim, 1] = mat_mc_ir.imag
            ir_sh += ir_dim
        assert(ir_sh == n_comp)

    # dyn.create sparse precond matrix : pc[i_row][i_nonzero_elem]
    pc_coeff = []          # [i_eqn][j] -> coeff from eq_mat
    pc_i3pt  = []          # [i_eqn][j] -> idx_3pt from eq_mat
    pc_norm2 = []          # [i_eqn][j] -> norm(eq_mat[i_3pt])

    n_eqn = 0
    for idx_3pt in np.ndindex(n_mc, n_comp, 2) :
        (i_mc, i_comp, i_c) = idx_3pt
        n2_i = (eq_mat[idx_3pt]**2).sum()
        if n2_i <= NONZERO_TOL : continue # drop zero equations
        if i_comp < 3 and not have_space_comp : continue
        if 3 == i_comp and not have_time_comp : continue

        coeff = None
        for k_eqn in range(n_eqn) :
            assert(0 < len(pc_i3pt[k_eqn]))
            idx_3pt_k   = pc_i3pt[k_eqn][0]
            (k_mc, k_comp, k_c) = idx_3pt_k
            xy  = (np.dot(eq_mat[idx_3pt], eq_mat[idx_3pt_k])
                        / math.sqrt(n2_i * pc_norm2[k_eqn][0]))
            # FIXME hardcoded here is the condition for merging equations; how to make it flexible?
            if (    k_c == i_c                                          # both Re or Im
                and (mc_std_list[i_mc] == mc_std_list[k_mc]).all()      # (in,out) momenta are equivalent
                and lhpd.rdiff(abs(xy), 1.) < NONZERO_TOL               # angle is 0 or Pi
                and lhpd.rdiff(n2_i, pc_norm2[k_eqn][0]) < NONZERO_TOL  # norm is the same
                # FIXME does averaging different polarizations make NO sense? 
                #and mcgrp[i_mc].get_tpol() == mcgrp[k_mc].get_tpol()
                    ) :
                coeff = xy
                #print("# old pcmat row idx_3pt=", idx_3pt, " ->", idx_3pt_k, " (x%.1f)" % coeff)
                pc_i3pt[k_eqn].append(idx_3pt)
                pc_coeff[k_eqn].append(coeff)
                pc_norm2[k_eqn].append(n2_i)
                break

        if None is coeff :
            #print("# new pcmat row idx_3pt=", idx_3pt)
            pc_i3pt.append([idx_3pt])
            pc_coeff.append([1.])
            pc_norm2.append([n2_i])
            n_eqn += 1

    assert(0 < n_eqn)
    assert( len(pc_i3pt) == n_eqn
        and len(pc_coeff) == n_eqn
        and len(pc_norm2) == n_eqn)

    pc_mat = np.zeros(sh_3pt + (n_eqn,), np.float64)
    for k_eqn in range(n_eqn) :
        for j in range(len(pc_coeff[k_eqn])) :
            pc_mat[pc_i3pt[k_eqn][j]][k_eqn] = pc_coeff[k_eqn][j]

    # [i_mc, n_comp, 2, i_ff], [i_mc, i_comp, 2, i_eqn]
    return eq_mat, pc_mat

def calc_ff_cpviol(gg, mcgrp, ssgrp, op, ir_list, mlat, latsize,
            method_list, 
            **kw) :
    """ this is a long ugly function to compute formfactors;
        temporary implementation, will be organized into subfunctions as will be seen fit
        (although likely to stay forever)

        method_list = [ (name, param1, ...), ...]
                    methods to extract the ground state

        func_get_op     (ama_mode, had, flav, op, ir_list, p3src, p3snk, tsep) -> c3pt[i_data,tau,i_comp]
        func_get_c2pt   (ama_mode, had, p3) -> c2pt[i_data, t]

        return formfactors[data][i_method][i_ff]
        ----------------------
        making it less ugly
        mc  = class {p3src, p3snk, tpol }
        ss  = class { tsep src_tag, snk_tag, sm_tag }
              ?rename as s4 or s5 or s6: "source&sink smearing&separation specification set"
        
    """
    lcdb    = kw.get('lcdb')
    rsplan  = kw['rsplan']
    tskip   = kw.get('tskip') or None
    c2fit_nexp= kw.get('c2fit_nexp') or None
    

    # TODO rewrite : 
    # # make eqmat, pcmat; apply pcmat
    # # for each ss:
    # #* for all mcgrp : load c3pt (ss, mcgrp, ir_list), resample, apply pcmat
    # #* for all mcgrp : load c2pt for unique ss.key2pt(), [sep. for src, snk?], resample, apply pcmat
    # # for each i_eqn : solve for me (need data for full ssgrp)
    # # solve for ff
    n_ff_cpeven = get_gff_number(op) 
    n_mc    = len(mcgrp)
    n_comp  = ir_list_comp_len(ir_list)
    n_c3pt  = n_mc * n_comp * 2
    tsep_list   = sorted(set([ ss.get_tsep() for ss in ssgrp ]))
    tmax    = max(tsep_list) + 1            # FIXME if fit c2pt, may need to plug other value
    
    list_method_require_c2fit = ['fit_c3']
    
    require_c2fit   = False
    for m in method_list :
        if m[0] in list_method_require_c2fit : 
            require_c2fit = True

    for m in method_list :
        if m[0] in list_method_require_c2fit :
            require_c2fit = True

    def load_twopt_srcsnk(ss):
        n_data = res_src = res_snk = None
        for i_mc, mc in enumerate(mcgrp) :
            # [i_data, t]
            x = gg.get_twopt_src(mc, ss)
            y = gg.get_twopt_snk(mc, ss)
            if None is n_data :
                assert(None is res_src)
                assert(None is res_snk)
                n_data = x.shape[0]
                assert(y.shape[0] == n_data)
                res_src = np.empty((n_data, tmax, n_mc), np.float64)
                res_snk = np.empty((n_data, tmax, n_mc), np.float64)
            # [i_data, t, i_mc]
            res_src[:, :, i_mc] = x[:, :tmax].real
            res_snk[:, :, i_mc] = y[:, :tmax].real
        return res_src, res_snk

    def load_c2fit_srcsnk(ss):
        """ return c2pt restored from fits, normalized -> ==exp(-E_0*t), t->inf, 
            and overlap factors for ground state
            for src and snk

        """
        n_data = n_fitp = None
        c2fitp_src = c2fitp_snk = None
        for i_mc, mc in enumerate(mcgrp) :
            # [i_data, i_p]
            p_src_r  = gg.get_twopt_fit_src(mc, ss)
            p_snk_r  = gg.get_twopt_fit_snk(mc, ss)
            if None is n_data :
                assert(None is c2fitp_src and None is c2fitp_snk)
                n_data, n_fitp = p_src_r.shape[0:2]
                assert(p_snk_r.shape[0] == n_data)
                c2fitp_src= np.empty((n_data, n_fitp, n_mc), np.float64)
                c2fitp_snk= np.empty((n_data, n_fitp, n_mc), np.float64)
            # [i_data, t, i_mc]
            c2fitp_src[:, :, i_mc]     = p_src_r
            c2fitp_snk[:, :, i_mc]     = p_snk_r
        return c2fitp_src, c2fitp_snk

    def load_threept_cpviol(ss) :
        n_data  = res = None
        n_tau   = 1 + ss.get_tsep()
        for i_mc, mc in enumerate(mcgrp) :
            if lcdb : x = gg.get_threept_vacins(mc, ss)
            else :
                # TODO handle iteration over ir_list also in old-style gg
                xl = [ ir_scale * gg.get_threept_vacins(mc, ss, op, ir_name) 
                       for ir_name, ir_scale in ir_list ]
                x = np.concatenate(tuple(xl), axis=1)
            if None is res :
                n_data = x.shape[0]
                # res [i_data, tau, i_mc, i_comp, i_c] real
                res = np.empty((n_data, n_tau, n_mc, n_comp, 2), np.float64)

            res[:, :, i_mc, :, 0] = x[..., :n_tau].real.transpose(0,2,1)
            res[:, :, i_mc, :, 1] = x[..., :n_tau].imag.transpose(0,2,1)

        return res

    def load_ff_cpeven(ss) :
        n_tau   = 1 + ss.get_tsep()
        q2_list     = np.array([ lorentz_mom_transfer_sq(mlat, latsize, mc.get_p3src(), mc.get_p3snk())
                             for mc in mcgrp ])
        assert((np.abs(q2_list - q2_list[0]) < NONZERO_TOL).all())
        if lcdb : x = gg.get_ff(q2_list[0], ss, method='ratio_pltx')
        else :    x = gg.get_ff(q2_list[0], ss, op, method='ratio_pltx')
        # [i_data, tau, i_ff_cpeven]
        assert(x.shape == (n_data, n_tau, n_ff_cpeven))
        return x

    def load_alfive(ss) :
        # [i_data, t]
        x   = gg.get_alfive(ss)
        assert(x.shape[:1] == (n_data,))
        assert(tmax < x.shape[1])
        return x

    def apply_eqn_prec_twopt(c2a, pcmat) :
        """ return preconditioned c2pt[i_data, t, i_eqn] """
        n_eqn   = pcmat.shape[-1]
        res_c2a = np.zeros(c2a.shape[0:2] + (n_eqn,), c2a.dtype)
        for i_eqn, i_mc, i_comp, i_c in np.ndindex(n_eqn, n_mc, n_comp, 2) :
            f   = pcmat[i_mc, i_comp, i_c, i_eqn]
            if 0. == f : continue
            # TODO WARN if variance differs substantially between averaged rows 
            res_c2a[:, :, i_eqn]    += abs(f) * c2a[:, :, i_mc]
        pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
        return res_c2a / pc_norm
    def apply_eqn_prec_fitp(fitp, pcmat) :
        """ return preconditioned c2pt[i_data, i_p, i_eqn] """
        n_eqn   = pcmat.shape[-1]
        res_fitp = np.zeros(fitp.shape[0:2] + (n_eqn,), fitp.dtype)
        for i_eqn, i_mc, i_comp, i_c in np.ndindex(n_eqn, n_mc, n_comp, 2) :
            f   = pcmat[i_mc, i_comp, i_c, i_eqn]
            if 0. == f : continue
            # TODO WARN if variance differs substantially between averaged rows 
            res_fitp[:, :, i_eqn]    += abs(f) * fitp[:, :, i_mc]
        pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
        return res_fitp / pc_norm

    def apply_eqn_prec_threept(c3ba, pcmat) :
        """ return preconditioned c3pt[i_data, tau, i_eqn] """
        n_eqn   = pcmat.shape[-1]
        res_c3ba= np.zeros(c3ba.shape[0:2] + (n_eqn,), c3ba.dtype)
        for i_eqn, i_mc, i_comp, i_c in np.ndindex(n_eqn, n_mc, n_comp, 2) :
            f   = pcmat[i_mc, i_comp, i_c, i_eqn]
            if 0. == f : continue
            # TODO WARN if variance differs substantially between averaged rows 
            res_c3ba[:, :, i_eqn]   += f * c3ba[:, :, i_mc, i_comp, i_c]
        pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
        return res_c3ba / pc_norm
    

    # [i_mc, n_comp, 2, i_ff], [i_mc, i_comp, 2, i_eqn]
    (eqmat, pcmat) = make_ff_eqnmat_precond_f2f_cpviol(
            op, mlat, latsize, mcgrp, ir_list, **kw)
    n_ff    = eqmat.shape[-1]
    n_eqn   = pcmat.shape[-1]
    # preconditioner is applied to both 3pt and 2pt, so their ratio should be solved against 
    # equations without extra factors; therefore, normalize the result of applying pc to eqmat
    # (divide each row by the multiplicity over which pc matrix summates
    pc_norm = np.abs(pcmat.reshape(n_c3pt, n_eqn)).sum(axis=0)
    # [i_eqn, i_ff]
    eqmat_pc= np.dot(pcmat.reshape(n_c3pt, n_eqn).T,
                      eqmat.reshape(n_c3pt, n_ff)) / pc_norm[:,None] 
    
    if kw.get('print_eqmat_pc', False) :   # print(pc'ed eqmat)
        print("XXX required print_eqmat_pc")
        c_list      = ['Re', 'Im']
        comp_list   = ir_list_comp_list(ir_list)
        for i_eqn in range(n_eqn) : 
            mat_ls = []
            for i_mc, i_comp, i_c in it.product(range(n_mc), range(n_comp), [0,1]) : 
                pc_i = pcmat[i_mc, i_comp, i_c, i_eqn]
                if 0. != pc_i :
                    mat_ls.append('%+.1f*%s_%s{%s}' % (pc_i, c_list[i_c], 
                            comp_list[i_comp], mcgrp[i_mc]))
            rhs_sep = '\n    '
            print('%2d\t%s == %s%s' % (i_eqn, eqmat_pc[i_eqn], rhs_sep, rhs_sep.join(mat_ls)))

    # load all data
    c3pt_map_pc_r   = {}
    c2pt_src_pc_r   = {}
    c2pt_snk_pc_r   = {}
    c2fitp_src_pc_r = c2fitp_snk_pc_r = None
    nc2fit_src_pc_r = nc2fit_snk_pc_r = None
    
    # keys for 2pt, alfive
    ssgrp_keyc2pt   = set([ ss.key2pt() for ss in ssgrp ])

    n_data      = None
    for ss in ssgrp :
        c3pt_map_pc_r[ss] = apply_eqn_prec_threept(lhpd.resample(load_threept_cpviol(ss), rsplan), pcmat)
        if None is n_data : n_data = c3pt_map_pc_r[ss].shape[0]
        assert(c3pt_map_pc_r[ss].shape[0] == n_data)
    
    for ss in ssgrp_keyc2pt :
        srcsnk = load_twopt_srcsnk(ss)
        c2pt_src_pc_r[ss] = apply_eqn_prec_twopt(lhpd.resample(srcsnk[0], rsplan), pcmat)
        assert(c2pt_src_pc_r[ss].shape[0] == n_data)
        c2pt_snk_pc_r[ss] = apply_eqn_prec_twopt(lhpd.resample(srcsnk[1], rsplan), pcmat)
        assert(c2pt_snk_pc_r[ss].shape[0] == n_data)
    
    # TODO load c2pt fit params, recreate "c2pt" from them
    if require_c2fit :
        c2fitp_src_pc_r = {}
        c2fitp_snk_pc_r = {}
        nc2fit_src_pc_r = {}
        nc2fit_snk_pc_r = {}
        n_fitp  = None
        c2exp_mf= lhpd.fitter.legacy2modelfunc(lhpd.pyfit.Nexp_ladder_c2pt(c2fit_nexp))
        tr      = np.r_[:tmax]
        for ss in set([ ss.key2pt() ]) :
            srcsnk = load_c2fit_srcsnk(ss)
            # XXX sic! no resampling here because already resampled
            # XXX fits are usually resampled!!! 
            # TODO make sure the rsplan is the same
            # XXX do not apply preconditioner to fitp directly !!! c2fit is not linear in all fitp
            # TODO recover 2pt function here (or in load_c2fit) for each ss
            # TODO apply preconditioner to c2fit
            # XXX how to properly apply precond to e0, de1_0 ???? they typicall have already been averaged over equivs
            c2fitp_src_r, c2fitp_snk_r = srcsnk[0:2]
            c2fitp_src_pc_r[ss] = apply_eqn_prec_fitp(c2fitp_src_r, pcmat)
            c2fitp_snk_pc_r[ss] = apply_eqn_prec_fitp(c2fitp_snk_r, pcmat)
            if None is n_fitp : 
                n_fitp = c2fitp_src_pc_r[ss].shape[1]
            def mk_nc2fit(fitp) :
                return np.array([ [ c2exp_mf.fv(tr, fitp[i_data, :, i_eqn]) 
                                    for i_eqn in range(n_eqn) ] 
                                  for i_data in range(n_data) ]).transpose(0,2,1) 
            
            nc2fit_src_pc_r[ss] = mk_nc2fit(c2fitp_src_pc_r[ss])
            nc2fit_snk_pc_r[ss] = mk_nc2fit(c2fitp_snk_pc_r[ss])

    # TODO methods with c2fit
    
    # [i_data] XXX already resampled
    alfive_r        = dict([ (ss, load_alfive(ss)) for ss in ssgrp_keyc2pt ])

    ff_cpeven_r = {}
    for ss in ssgrp :
        # [i_data, tau, i_ff_cpeven] XXX # already resampled
        ff_cpeven_r[ss] = load_ff_cpeven(ss)
    
    def calc_me_ratio(c2a, c2b, c3ba, tsep) :
        """ ASSUME time axis=1 """
        sqf = np.sqrt(np.abs(  c2b[:, : tsep+1] / c2a[:, : tsep+1]
                               * c2a[:, tsep::-1] / c2b[:, tsep::-1] ))
        return (c3ba / np.sqrt(np.abs(c2b[:, tsep] * c2a[:, tsep]))[:,None] * sqf)
    def calc_me_ratio_all(c2a, c2b, c3ba_map) : 
        return dict([ (ss, calc_me_ratio(c2a[ss.key2pt()], c2b[ss.key2pt()], 
                                         c3ba_map[ss], ss.get_tsep()))
                      for ss in c3ba_map.keys() ]) 
    # me_rs[ss][i_data, tau, i_eqn]
    # TODO solve for me here : ratios, fits, etc ...
    me_rs   = calc_me_ratio_all(c2pt_src_pc_r, c2pt_snk_pc_r, c3pt_map_pc_r)
    

    # XXX hack: split eqmat_pc into cpeven, cpodd, subtract eqmat_cpeven.ff_cpeven from rhs
    # XXX eqmat_cpeven_pc won't have al5hat yet; it will be multiplied in subtraction
    ff_cpeven_sl    = np.s_[0:2]    # F1, F2
    ff_cpviol_sl    = np.s_[2:4]    # FA, F3
    n_ff_cpodd      = 2
    assert(2 == n_ff_cpeven)
    # [i_eqn, i_ff]
    eqmat_cpeven_pc = eqmat_pc[:, ff_cpeven_sl]
    eqmat_pc        = eqmat_pc[:, ff_cpviol_sl]
    #del eqmat_pc    # XXX for debug, to make sure eqmat_pc is not used

    for ss in ssgrp : 
        # subtract eqmat_cpeven_pc.ff_cpeven * alpha[tsep]
        # [i_data, tau, i_eqn]
        rhs_sub_r   = np.dot(ff_cpeven_r[ss], eqmat_cpeven_pc.T)
        # [ss][i_data, tau, i_eqn]
        me_rs[ss]   = me_rs[ss] - rhs_sub_r * alfive_r[ss.key2pt()][:,ss.tsep,None,None]

    # XXX XXX XXX debug print
    if False :
        print(pc_norm)
        print(pcmat)
        print(eqmat_cpeven_pc)
        print(eqmat_cpviol_pc)
        for ss in ssgrp :
            print(ss)
            print(c3pt_map_pc_r[ss].mean(0))
            print(me_rs[ss].mean(0))
        for ss in set([ ss.key2pt() ]) :
            print(ss)
            print(c2pt_src_pc_r[ss].mean(0))
            print(c2pt_snk_pc_r[ss].mean(0))


    def calc_ratio_pltx(ss) :
        """ ff on timeslices
            return ff[n_data, 1+tsep, n_ff] """
        #assert(ss in c3pt_map_pc_r)
        tsep    = ss.get_tsep()
        res     = np.empty((me_rs[ss].shape[0], 1+tsep, n_ff_cpodd), np.float64)
        for tau in range(1+tsep) :
            # solve od: [i_eqn,i_ff] for tau
            cov     = lhpd.calc_cov(me_rs[ss][:,tau], rsplan)
            cov_inv = lhpd.fitter.cov_invert(cov, rmin=1e-4)
            sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
            res[:,tau,:] = matrix_tensor_dot(sol_mat, me_rs[ss][:,tau,:], 1)
        return res

    def calc_ratio_pltx_avg(ss, max_tslice=3) :
        """ ff from ratio pltx center avg
            return ff[n_data, 1, n_ff_cpodd] """
        assert(ss in c3pt_map_pc_r)
        tsep    = ss.get_tsep()
        t1, t2 = lhpd.latcorr.pltx_trange(tsep, max_tslice)
        
        res     = np.empty((me_rs[ss].shape[0], 1, n_ff_cpodd), np.float64)
        # solve od: [i_eqn,i_ff] for tau
        cov     = lhpd.calc_cov(me_rs[ss][:, t1:t2].mean(axis=1), 
                                         rsplan)
        cov_inv = lhpd.fitter.cov_invert(cov, rmin=1e-4)
        sol_mat, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)
        me_rs_avg = me_rs[ss][:, t1:t2, :].mean(axis=1)
        res[:, 0, :] = matrix_tensor_dot(sol_mat, me_rs_avg, 1)
        return res

    def calc_ratio_summ(ss_list, tskip=tskip) :
        """ ff from summation-extrapolated matrix elements
            return ff[n_data, 1, n_ff_cpodd] """
        assert(not None is tskip)
        for ss in ss_list : 
            assert(ss in c3pt_map_pc_r)
        tsep_list   = np.asarray(sorted(set([ ss.get_tsep() for ss in ss_list ])))
        n_tsep      = len(tsep_list)
        assert(len(ss_list) == n_tsep) # TODO implement for general case, e.g. where the same tsep are combined with different smearings
        #raise NotImplementedError
        me_summ     = np.empty((n_data, n_tsep, n_eqn), np.float64)
        for i_ss, ss in enumerate(ss_list) : # FIXME this loop
            tsep    = ss.get_tsep()
            me_summ[:, i_ss]      = me_rs[ss][:, tskip : 1 + tsep - tskip].sum(axis=1)

        # solve od : [i_tsep, {coeff, intercept}]
        me_gs       = np.empty((n_data, n_eqn), np.float64)
        #raise NotImplementedError   # make sure this equation has correct list of tsep for summation
        eqn_summ = np.r_[ [ [ss.get_tsep() for ss in ss_list] ], [[1.] * n_tsep] ].T
        for i_eqn in range(n_eqn) :
            me_summ_cov = lhpd.calc_cov(me_summ[:, :, i_eqn], rsplan)
            cov_inv     = lhpd.fitter.cov_invert(me_summ_cov, rmin=1e-4)        # FIXME regulate cov_inv better
            sol_summ, sol_undef = lhpd.fitter.linsolver_od_matr(eqn_summ, cov_inv)
            me_gs[:, i_eqn]     = matrix_tensor_dot(sol_summ, me_summ[:, :, i_eqn], 1)[:, 0]

        # solve od : [i_eqn, i_ff]
        res         = np.empty((n_data, 1, n_ff), np.float64)
        me_gs_cov   = lhpd.calc_cov(me_gs, rsplan)
        cov_inv     = lhpd.fitter.cov_invert(me_gs_cov, rmin=1e-4)
        sol_ff, sol_undef = lhpd.fitter.linsolver_od_matr(eqmat_pc, cov_inv)    # FIXME regulate cov_inv better
        res[:, 0, :]= matrix_tensor_dot(sol_ff, me_gs, 1)
        return res

    def calc_fit_c3(ss_list, tskip=tskip) :
        """ reproducing Boram's fits here """
        raise NotImplementedError
        ttau    = []  # list of tuples (tsep, tau)
        c3_ttau = []
        for ss in ss_list :
            tsep    = ss.get_tsep()
            ttau.extend([ (ss.get_tsep(), tau) for tau in np.r_[tskip : ss.get_tsep() - tskip + 1]])
            c3_ttau.extend([ c3pt_map_pc_r[ss][:, tskip : ss.get_tsep() - tskip + 1] ])
        # [i_ttau, {tsep,tau}]
        ttau    = np.asarray(ttau)
        n_ttau  = len(ttau)
        # [i_data, i_ttau, i_eqn]
        c3_ttau = np.concatenate(c3_ttau, axis=1)
        #print(ttau, c3_ttau.shape)

        me_gs   = np.empty((n_data, n_eqn), np.float64)
        for i_eqn in range(n_eqn) :
            assert(2 == c2fit_nexp) # TODO any nexp
            # [i_data, i_me, i_ttau]
            c3_ttau_cov     = lhpd.calc_cov(c3_ttau[:, :, i_eqn], rsplan)
            c3_ttau_cov_inv = lhpd.fitter.cov_invert(c3_ttau_cov, rmin=1e-4)    # FIXME regulate cov_inv better
            c3_chi2         = np.empty((n_data,), np.float64)
            n_dof_c3        = n_ttau - 4
            for i_data in range(n_data) :
                assert(1==len(c2fitp_src_pc_r.keys()))  # TODO >1 smearing pairs
                fitp_src = c2fitp_src_pc_r.values()[0][i_data,:,i_eqn]
                assert(1==len(c2fitp_snk_pc_r.keys()))  # TODO >1 smearing pairs
                fitp_snk = c2fitp_snk_pc_r.values()[0][i_data,:,i_eqn]
                # [i_me, i_ttau]
                exp_ttau = np.empty((4, n_ttau), np.float64)
                exp_ttau[0] = np.exp(-fitp_src[1] * ttau[:,1] -fitp_snk[1] * (ttau[:,0]-ttau[:,1]))
                #* np.sqrt(fitp_src[0] * fitp_snk[0]) # <0|0>
                exp_ttau[1] = exp_ttau[0] * np.exp(-fitp_src[3] * ttau[:,1])
                #* np.sqrt(fitp_src[2])  # <0|1>
                exp_ttau[2] = exp_ttau[0] * np.exp(-fitp_snk[3] * (ttau[:,0] - ttau[:,1]))  
                #* np.sqrt(fitp_snk[2])  # <1|0>
                exp_ttau[3] = exp_ttau[0] * np.exp(-fitp_src[3] * ttau[:,1] -fitp_snk[3] * (ttau[:,0] - ttau[:,1])) 
                #* np.sqrt(fitp_src[2]*fitp_snk[2]) # <1|1>
                
                sol_me, undef_me    = lhpd.fitter.linsolver_od_matr(exp_ttau.T, c3_ttau_cov_inv) 
                me_fit              = np.dot(sol_me, c3_ttau[i_data, :, i_eqn])
                me_gs[i_data, i_eqn]= me_fit[0] / np.sqrt(fitp_src[0]*fitp_snk[0])
                # [i_ttau]
                c3_diff             = np.dot(exp_ttau.T, me_fit) -  c3_ttau[i_data, :, i_eqn]
                c3_chi2[i_data]     = np.dot(c3_diff, np.dot(c3_ttau_cov_inv, c3_diff))
                # TODO divide out <0|0> coeff np.sqrt(fitp_src[0] * fitp_snk[0])
            c3_chi2_a, c3_chi2_e = lhpd.calc_avg_err(c3_chi2, rsplan)
            print("# i_eqn=%d done, chi2/dof=%s/%d=%s" % (i_eqn, 
                        lhpd.spprint_ve(c3_chi2_a, c3_chi2_e), n_dof_c3,
                        lhpd.spprint_ve(c3_chi2_a / n_dof_c3, c3_chi2_e / n_dof_c3)))
        # [i_data, i_eqn]
        res             = np.empty((n_data, 1, n_ff), np.float64)
        me_gs_cov       = lhpd.calc_cov(me_gs, rsplan)
        me_gs_cov_inv   = lhpd.fitter.cov_invert(me_gs_cov, rmin=1e-4)
        sol_ff, undef_ff= lhpd.fitter.linsolver_od_matr(eqmat_pc, me_gs_cov_inv)# FIXME regulate cov_inv better
        ff_fit          = matrix_tensor_dot(sol_ff, me_gs, 1)
        res[:, 0, :]    = ff_fit

        # [i_data, i_eqn]
        me_diff         = matrix_tensor_dot(eqmat_pc, ff_fit, 1) - me_gs
        print(me_diff.shape)
        me_chi2         = (me_diff * matrix_tensor_dot(me_gs_cov_inv, me_diff, 1)).sum(1)
        print(me_chi2.shape)
        me_chi2_a, me_chi2_e = lhpd.calc_avg_err(me_chi2, rsplan)
        n_dof_me = n_eqn - n_ff
        print("fit_c3 : me_fit chi2=%s/%d=%s" % (
                    lhpd.spprint_ve(me_chi2_a, me_chi2_e), n_dof_me,
                    lhpd.spprint_ve(me_chi2_a / max(1,n_dof_me), me_chi2_e / max(1,n_dof_me))))
        return res
        # TODO calc_exp_fit continue here

            
    # TODO calc_gpof


    # put plateaus:tsep->x, plateau averages:tsep->x, summation
    all_res = []
    for m in method_list :
        if   'ratio_pltx' == m[0] : 
            for ss in ssgrp :
                all_res.append(calc_ratio_pltx(ss))
                #print(all_res[-1].mean(0))

        elif 'ratio_pltx_avg' == m[0] :
            for ss in ssgrp :
                all_res.append(calc_ratio_pltx_avg(ss))
        elif 'ratio_summ' == m[0] :
            all_res.append(calc_ratio_summ(ssgrp))
        elif 'fit_c3' == m[0] :
            raise NotImplementedError
            all_res.append(calc_fit_c3(ssgrp))
        else : raise ValueError(m[0])

    return all_res


def calc_save_ff_cpviol(
            h5_file, h5_kpath,  # output
            gg,                 # input 
            mcgrp_list, ssgrp, op, ir_list,
            mlat, latsize,
            method_list,
            **kw) :

    n_q2    = len(mcgrp_list)
    n_data  = None
    n_ff    = get_gff_number(op)
    all_ff  = []
    q2_list = []
    for i_q2, mcgrp in enumerate(mcgrp_list) :
        q2grp   = np.array([ lorentz_mom_transfer_sq(mlat, latsize,
                                    mc.get_p3src(), mc.get_p3snk())
                             for mc in mcgrp ])
        q2      = q2grp[0]
        assert(np.allclose(q2, q2grp))
        if 0 < kw.get('verbose', 0) :
            print("# Q2=%f  " % ( q2, ))
        elif 1 < kw.get('verbose', 0) :
            print("# Q2=%f  [ %s ]" % ( q2, ','.join([ str(mc) for mc in mcgrp ]) ))
        q2_list.append(q2)
        all_ff.append(calc_ff_cpviol(gg, mcgrp, ssgrp, op, ir_list, mlat, latsize,
                              method_list, **kw))

    q2_list = np.asarray(q2_list)
    n_data = all_ff[0][0].shape[0]
    assert (all_ff[0][0].shape[-1] == n_ff)

    def save_ff(dset, i) :
        dset.attrs['q2_list'] = q2_list
        dset.attrs['dim_spec'] = ['i_data', 'k_method', 'i_q2', 'i_ff'] # k_method stands for tau in ratio_pltx
        for i_q2, q2 in enumerate(q2_list) :
            # [i_data, i_t, i_q2, i_ff]
            dset[:, :, i_q2, :] = all_ff[i_q2][i]

    cnt = 0
    for m in method_list :
        if   'ratio_pltx' == m[0] :
            tsep_list = []
            for ss in ssgrp :
                tsep    = ss.get_tsep()
                # FIXME make separate records for the same tsep but different smearings
                assert(not tsep in tsep_list)
                tsep_list.append(tsep)
                ff_shape = (n_data, 1 + tsep, n_q2, n_ff)
                k = '%s/ratio_pltx/dt%d' % (h5_kpath, tsep)

                h5_ff = h5_file.require_dataset(k, ff_shape, np.float64,
                                                fletcher32=True)
                save_ff(h5_ff, cnt)
                cnt += 1
        elif 'ratio_pltx_avg' == m[0] :
            tsep_list = []
            for ss in ssgrp :
                tsep    = ss.get_tsep()
                # FIXME make separate records for the same tsep but different smearings
                assert(not tsep in tsep_list)
                tsep_list.append(tsep)
                ff_shape = (n_data, 1, n_q2, n_ff)
                k = '%s/ratio_pltx_avg/dt%d' % (h5_kpath, tsep)
                h5_ff = h5_file.require_dataset(k, ff_shape, np.float64,
                                                fletcher32=True)
                save_ff(h5_ff, cnt)
                cnt += 1

        elif 'ratio_summ' == m[0] :
            ff_shape = (n_data, 1, n_q2, n_ff)
            k = '%s/ratio_summ' % (h5_kpath)
            h5_ff = h5_file.require_dataset(k, ff_shape, np.float64,
                                            fletcher32=True)
            save_ff(h5_ff, cnt)
            cnt += 1
        elif 'fit_c3' == m[0] :
            raise NotImplementedError
            ff_shape = (n_data, 1, n_q2, n_ff)
            k = '%s/fit_c3' % (h5_kpath)
            h5_ff = h5_file.require_dataset(k, ff_shape, np.float64,
                                            fletcher32=True)
            save_ff(h5_ff, cnt)
            cnt += 1

def runme_calc_save_ff_cpviol(
        h5_file,
        mcgrp_list, 
        ssgrp,
        opt_list,
        method_list,
        **kw ) :
    """ 
            opt_list    options to iterate over [ (k, vlist), ...]
            method_list methods to try
    """

    mlat        = kw['mlat']
    latsize     = kw['latsize']
    had         = kw['had']

    if isinstance(h5_file, basestring) :
        h5f = h5py.File(h5_file, 'a')
    elif isinstance(h5_file, h5py.File) or isinstance(h5_file, h5py.Group) :
        h5f = h5_file
    else : raise ValueError(h5_file)

    # iterate over options 
    kk      = [ o[0] for o in opt_list ]
    vvlist  = [ o[1] for o in opt_list ]
    for vv in it.product(*vvlist) :
        kw_opt  = dict(zip(kk, vv))
        h5k_opt = '/'.join(vv)
        print('runme_calc_save_ff_cpviol : ', kw_opt, h5k_opt)

        kw0     = dict(kw) ; kw0.update(kw_opt)
        gg      = ens_data_get(**kw0)
        
        h5k = '/%s/%s' % (had, h5k_opt)
        lhpd.h5_io.h5_purge_keys(h5f, [h5k])
        
        op      = kw_opt['op']
        ir_list = lhpd.latcorr.op_default_ir_list(op)
        kw1     = dict(kw) ; kw1.update(kw_opt)
        lhpd.purge_keys(kw1, ['mlat', 'latsize', 'op']) ; 
        calc_save_ff_cpviol(h5f, h5k, gg, mcgrp_list, ssgrp,
                     op, ir_list, mlat, latsize, method_list, **kw1)

    if isinstance(h5_file, basestring) :
        h5f.close()
    elif isinstance(h5_file, h5py.File) or isinstance(h5_file, h5py.Group) :
        h5f.flush()
    else : raise ValueError(h5_file)


def runme_calc_save_ff_cpviol_lcdb(
        mcgrp_list, ssgrp,
        method_list,
        **kw ) :
    """ 
            opt_list    options to iterate over [ (k, vlist), ...]
            method_list methods to try
    """
    dat_d       = kw['dat_d']
    data_in     = kw.get('data_in', dat_d['data_dir'])
    c2_d        = kw['c2_d']
    c3_d        = kw['c3_d']
    ins_d       = kw['ins_d']
    vi_d        = kw['vi_d']


    print("runme_calc_save_ff_cpviol:", ins_str_desc(ins_d), vacins_str_desc(vi_d))
    assert('op' == ins_d['kind'])
    op      = ins_d['op']
    ir_list = ins_d.get('ir_list', lhpd.latcorr.op_default_ir_list(op))
    ins1_d  = dictnew(ins_d, ir_list=ir_list)
    
    h5file, h5kpath = get_lcdb_loc(dat_d, 
        dict(kind='ffvi', c3_d=dict(kind='c3', had=had),
        ins_d=ins_d, vi_d=vi_d, m_d={}))
    h5f     = h5py.File(h5file, 'a')
    lhpd.h5_io.h5_purge_keys(h5f, [h5kpath])
    
    kw1, (mlat, latsize) = dictnew_pop(kw, ['mlat', 'latsize'])
    gg      = ens_data_get(**dictnew(kw1, ins_d=ins1_d))
    
    lhpd.purge_keys(kw1, ['mlat', 'latsize', 'op']) ; 
    calc_save_ff_cpviol(h5f, h5kpath, gg, mcgrp_list, ssgrp,
                 op, ir_list, mlat, latsize, method_list, **kw1)

    h5f.flush()
    h5f.close()

