from __future__ import print_function
import math, numpy as np, itertools as it
import matplotlib as mpl ; import matplotlib.pyplot as plt
import lhpd
import dataview as dv
from dataview import style_group_default as stg_list

def calc_bgem_c2pt_ratios(gg_p, gg_m, tpol_c2, tpol_c2_spin, sm, flav_cedm, tmax, 
        rsplan=('jk',1), 
        de_factor=1., eflux=None, tr_pltx=None,
        ax=None, stg=stg_list[0]) :

    c2_p_r  = lhpd.resample(gg_p.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2), rsplan)
    c2_m_r  = lhpd.resample(gg_m.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2), rsplan)

    rr1     = c2_p_r.real[:,:tmax+1] / c2_m_r.real[:,:tmax+1]
    rr1_ae  = lhpd.calc_avg_err(rr1, rsplan)
    #print("*** rr1\n", np.array(rr1_ae).T)

    rr1_meff= rr1[:, :-1] / rr1[:,1:]
    rr1_meff_ae = lhpd.calc_avg_err(rr1_meff, rsplan)
    #print("*** rr1_meff\n", np.array(rr1_meff_ae).T)

    c2vc_p_r= lhpd.resample(gg_p.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2_spin, flav_cedm, 'orig'), rsplan)
    c2vc_m_r= lhpd.resample(gg_m.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2_spin, flav_cedm, 'orig'), rsplan)
    de_p_rr = c2vc_p_r.real[:,:tmax] / c2_p_r.real[:,:tmax] * de_factor
    de_m_rr = c2vc_m_r.real[:,:tmax] / c2_m_r.real[:,:tmax] * de_factor
    de_p_rae    = lhpd.calc_avg_err( de_p_rr, rsplan)
    de_m_rae    = lhpd.calc_avg_err(-de_m_rr, rsplan)
    de_pm_rae   = lhpd.calc_avg_err(0.5*(de_p_rr - de_m_rr), rsplan)
    #print("*** de_p\n", np.array(de_p_rae).T)
    #print("*** de_m\n", np.array(de_m_rae).T)
    #print("*** de_pm\n", np.array(de_pm_rae).T)
    if None is eflux : eflux_str = 'e'
    else : eflux_str = '%d' % eflux
    if not None is ax :
        tr  = np.r_[:tmax]
        xsh = 0.2
        #ax.errorbar(0*xsh + tr, de_p_rae[0],  yerr=de_p_rae[1],  label=r'(%s) %s $E=+%s$' % (sm, flav_cedm, eflux_str))
        #ax.errorbar(1*xsh + tr, de_m_rae[0],  yerr=de_m_rae[1],  label=r'(%s) %s $E=-%s$' % (sm, flav_cedm, eflux_str))
        ax.errorbar(2*xsh + tr, de_pm_rae[0], yerr=de_pm_rae[1], 
                    label=r'(%s) %s $E=\pm%s$' % (sm, flav_cedm, eflux_str), 
                    **stg.edotline())
    return ax

def calc_bgem_c2pt_energyshift(gg_p, gg_m, tpol_c2, tpol_c2_spin, sm, flav_cedm, tmax,
        rsplan=('jk',1), de_factor=1., eflux=None,
        ax=None, stg=stg_list[0], xsh=0,
        **kw) :
    label_prefix = kw.get('label_prefix') or ''
    c2_p_r  = lhpd.resample(gg_p.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2), rsplan)
    c2_m_r  = lhpd.resample(gg_m.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2), rsplan)

    c2vc_p_r= lhpd.resample(gg_p.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2_spin, flav_cedm, 'orig'), rsplan)
    c2vc_m_r= lhpd.resample(gg_m.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2_spin, flav_cedm, 'orig'), rsplan)
    
    de_p_rr = c2vc_p_r.real[:,:tmax+1] / c2_p_r.real[:,:tmax+1] * de_factor
    de_m_rr = c2vc_m_r.real[:,:tmax+1] / c2_m_r.real[:,:tmax+1] * de_factor
    #de_pm_rr= 0.5*(de_p_rr - de_m_rr)

    eshift_p_r  = de_p_rr[:,1:] - de_p_rr[:,:-1] 
    eshift_m_r  = de_m_rr[:,1:] - de_m_rr[:,:-1]
    #eshift_pm_r = de_pm_rr[:,1:] - de_pm_rr[:,:-1]

    eshift_p_ae = lhpd.calc_avg_err( eshift_p_r, rsplan)
    eshift_m_ae = lhpd.calc_avg_err(-eshift_m_r, rsplan)
    eshift_pm_ae= lhpd.calc_avg_err(0.5*(eshift_p_r - eshift_m_r), rsplan)
    if None is eflux : eflux_str = 'e'
    else : eflux_str = '%d' % eflux
    if not None is ax :
        tr  = np.r_[:tmax]
        #ax.errorbar(0*xsh + tr, eshift_p_ae[0],  yerr=eshift_p_ae[1],  label=r'(%s) %s $E=+%s$' % (sm, flav_cedm, eflux_str))
        #ax.errorbar(1*xsh + tr, eshift_m_ae[0],  yerr=eshift_m_ae[1],  label=r'(%s) %s $E=-%s$' % (sm, flav_cedm, eflux_str))
        ax.errorbar(xsh + tr, eshift_pm_ae[0], yerr=eshift_pm_ae[1], 
                    label=r'%s (%s) %s $E=\pm%s$' % (label_prefix, sm, flav_cedm, eflux_str),
                    **stg.edotline())
    return ax

def calc_bgem_c2pt_energyshift_m1(gg_m, tpol_c2, tpol_c2_spin, sm, flav_cedm, tmax,
        rsplan=('jk',1), de_factor=1., eflux=None,
        ax=None, stg=stg_list[0],
        **kw) :
    """ TMP HACK """
    label_prefix = kw.get('label_prefix') or ''
    c2_m_r  = lhpd.resample(gg_m.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2), rsplan)

    c2vc_m_r= lhpd.resample(gg_m.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2_spin, flav_cedm, 'orig'), rsplan)
    
    de_m_rr = c2vc_m_r.real[:,:tmax+1] / c2_m_r.real[:,:tmax+1] * de_factor

    eshift_m_r  = de_m_rr[:,1:] - de_m_rr[:,:-1]

    eshift_pm_ae= lhpd.calc_avg_err(-eshift_m_r, rsplan)
    if None is eflux : eflux_str = 'e'
    else : eflux_str = '%d' % eflux
    if not None is ax :
        tr  = np.r_[:tmax]
        xsh = 0.
        ax.errorbar(0*xsh + tr, eshift_pm_ae[0], yerr=eshift_pm_ae[1], 
                    label=r'%s (%s) %s $E=\pm%s$' % (label_prefix, sm, flav_cedm, eflux_str),
                    **stg.edotline())
    return ax

def calc_bgem_c2pt_energyshift_v2(gg_p, gg_m, tpol_c2p, tpol_c2m, tpol_c2p_spin, tpol_c2m_spin, sm, flav_cedm, tmax,
        rsplan=('jk',1), de_factor=1., eflux=None, tr_pltx=None,
        ax=None, stg=stg_list[0], xsh=0., label=None,
        **kw) :
    label_prefix = kw.get('label_prefix') or ''

    # XXX signs _(p|m)(p|m)_ : {\pm Efield} {\pm Sz}
    # cp-even c2
    c2_pp_r  = lhpd.resample(gg_p.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2p), rsplan)
    c2_pm_r  = lhpd.resample(gg_p.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2m), rsplan)
    c2_mp_r  = lhpd.resample(gg_m.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2p), rsplan)
    c2_mm_r  = lhpd.resample(gg_m.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2m), rsplan)
    # cp-odd \delta c2
    c2vc_pp_r= lhpd.resample(gg_p.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2p_spin, 
                    flav_cedm, 'orig'), rsplan)
    c2vc_pm_r= lhpd.resample(gg_p.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2m_spin, 
                    flav_cedm, 'orig'), rsplan)
    c2vc_mp_r= lhpd.resample(gg_m.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2p_spin, 
                    flav_cedm, 'orig'), rsplan)
    c2vc_mm_r= lhpd.resample(gg_m.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2m_spin, 
                    flav_cedm, 'orig'), rsplan)
    # \delta E * t  vs  t
    de_t_pp_rr  = c2vc_pp_r.real[:,:tmax+1] / c2_pp_r.real[:,:tmax+1] * de_factor
    de_t_pm_rr  = c2vc_pm_r.real[:,:tmax+1] / c2_pm_r.real[:,:tmax+1] * de_factor
    de_t_mp_rr  = c2vc_mp_r.real[:,:tmax+1] / c2_mp_r.real[:,:tmax+1] * de_factor
    de_t_mm_rr  = c2vc_mm_r.real[:,:tmax+1] / c2_mm_r.real[:,:tmax+1] * de_factor
    # \delta E  vs  t = 
    eshift_pp_r = de_t_pp_rr[:,1:] - de_t_pp_rr[:,:-1]
    eshift_pm_r = de_t_pm_rr[:,1:] - de_t_pm_rr[:,:-1]
    eshift_mp_r = de_t_mp_rr[:,1:] - de_t_mp_rr[:,:-1]
    eshift_mm_r = de_t_mm_rr[:,1:] - de_t_mm_rr[:,:-1]
    # avg Sz=\pm : remaining (p|m) = {\pm Efield} 
    eshift_p_r  = 0.5 * (eshift_pp_r - eshift_pm_r)
    eshift_m_r  = 0.5 * (eshift_mp_r - eshift_mm_r)
    # avg Efield=\pm
    eshift_r    = 0.5 * (eshift_p_r - eshift_m_r)
    eshift_ae   = lhpd.calc_avg_err(eshift_r, rsplan)

    if not None is tr_pltx :
        tr_pltx = np.asarray(tr_pltx)
        eshift_pltx     = eshift_r[:,tr_pltx]
        # first weigh plateau values with their stdevs,
        #   then average over ensemble (to account for correlatoins)
        eshift_pltx_ae      = lhpd.calc_avg_err(eshift_pltx, rsplan)
        eshift_pltxavg_rs_ae= lhpd.mean_weighted_ve(eshift_pltx, eshift_pltx_ae[1])
        eshift_pltxavg_ae   = lhpd.calc_avg_err(eshift_pltxavg_rs_ae[0], rsplan)
        #eshift_pltx_ae = lhpd.calc_avg_err(eshift_r[:,tr_pltx].mean(-1), rsplan)
        lhpd.pprint_ve([eshift_pltxavg_ae[0]], [eshift_pltxavg_ae[1]], title=['eshift_pltx'])

    if None is eflux : eflux_str = 'e'
    else : eflux_str = '%d' % eflux
    if not None is ax :
        tr  = np.r_[:tmax]
        if None is label : label = r'%s (%s) %s $E=\pm%s$' % (label_prefix, sm, flav_cedm, eflux_str)
        ax.errorbar(xsh + tr, eshift_ae[0], yerr=eshift_ae[1], 
                    label=label, **stg.edotline())
        if not None is tr_pltx :
            eshift_pltx_min = eshift_pltx_ae[0] - eshift_pltx_ae[1]
            eshift_pltx_max = eshift_pltx_ae[0] + eshift_pltx_ae[1]
            dv.plot_ve_band([tr_pltx.min() + xsh-.1, tr_pltx.max() + xsh+.1], 
                    eshift_pltxavg_ae[0], eshift_pltxavg_ae[1], ax, stg.cp(edgecolor='none'))

    return ax

def calc_bgem_c2pt_energyshift_m1_v2(gg_m, tpol_c2p, tpol_c2m, tpol_c2p_spin, tpol_c2m_spin, sm, flav_cedm, tmax,
        rsplan=('jk',1), ax=None, de_factor=1., eflux=None,
        **kw) :
    label_prefix = kw.get('label_prefix') or ''

    c2_mp_r  = lhpd.resample(gg_m.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2p), rsplan)
    c2_mm_r  = lhpd.resample(gg_m.get_twopt_symm_mom_tpol_(sm, [0,0,0], tpol_c2m), rsplan)

    c2vc_mp_r= lhpd.resample(gg_m.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2p_spin, flav_cedm, 'orig'), rsplan)
    c2vc_mm_r= lhpd.resample(gg_m.get_twopt_volcedm_flav_mom_tpol_(sm, [0,0,0], tpol_c2m_spin, flav_cedm, 'orig'), rsplan)
    
    de_mp_rr = c2vc_mp_r.real[:,:tmax+1] / c2_mp_r.real[:,:tmax+1] * de_factor
    de_mm_rr = c2vc_mm_r.real[:,:tmax+1] / c2_mm_r.real[:,:tmax+1] * de_factor
    #de_pm_rr= 0.5*(de_p_rr - de_m_rr)

    eshift_mp_r = (de_mp_rr[:,1:] - de_mp_rr[:,:-1]) /2.
    eshift_mm_r = (de_mm_rr[:,1:] - de_mm_rr[:,:-1]) /2.
    eshift_m_r  = eshift_mp_r - eshift_mm_r

    eshift_m_ae = lhpd.calc_avg_err(-eshift_m_r, rsplan)
    eshift_pm_ae= lhpd.calc_avg_err(-eshift_m_r, rsplan)
    if None is eflux : eflux_str = 'e'
    else : eflux_str = '%d' % eflux
    if not None is ax :
        tr  = np.r_[:tmax]
        xsh = 0.0
        #ax.errorbar(0*xsh + tr, eshift_p_ae[0],  yerr=eshift_p_ae[1],  label=r'(%s) %s $E=+%s$' % (sm, flav_cedm, eflux_str))
        #ax.errorbar(1*xsh + tr, eshift_m_ae[0],  yerr=eshift_m_ae[1],  label=r'(%s) %s $E=-%s$' % (sm, flav_cedm, eflux_str))
        ax.errorbar(2*xsh + tr, eshift_pm_ae[0], yerr=eshift_pm_ae[1], 
                    label=r'%s (%s) %s $E=\pm%s$' % (label_prefix, sm, flav_cedm, eflux_str))
    return ax
