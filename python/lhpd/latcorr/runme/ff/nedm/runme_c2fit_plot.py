from past.builtins import execfile
execfile('config.data.py') ; execfile('ens_setup.py') ; execfile('fit_c2.py')

mlat=0.7
op='tensor1'
ir_name='H4_T1_d4r1'
#tpol='posSzplus'
tpol='pos'
rsplan=('jk',1)
gg=ens_data_get(ama='ub', had='proton', flav='U-D', op=op, data_top=data_out, latsize=latsize, mlat=mlat, twopt_avg_rot=True)

tsep=8
ss_list=[ srcsnkpair(dt, 'SS') for dt in [ tsep ] ]
#mc_list=[ mc_case([k,k,0],[-3,-3,0], 'posSzplus') for k in np.r_[-3:5] ]
qext_list=np.array(list(lhpd.int_vectors_maxnorm(3, 10)))
mc_dict=lhpd.latcorr.make_mom_combo_list(.7, latsize, [[0,0,0]], qext_list)
mcgrp_list = [ [ mc_case(p0, p1, tpol) for p0,p1 in mc_dict[k] ] for k in sorted(mc_dict.keys()) ]
mc_std_list = [ mcgrp[0] for mcgrp in mcgrp_list ]

"""

c2fit_h5f=h5py.File('data_out/c2fit.ub.py')
c2fit_h5g=c2fit_h5f.require_group('/c2fit')
c2fit_kw=dict(rsplan=('jk',1), c2fit_nexp=2, method='nlm', opt_kw=dict(disp=2))

tr0= np.r_[4:12]
runme_fit_save_c2pt(c2fit_h5g, gg, tr0, mc_std_list, ss_list[0], **c2fit_kw)
ax0=runme_plot_fit_c2pt(c2fit_h5g, [tr0], [mc.get_p3src() for mc in mc_std_list ], xlim=[0,16], ylim=[0.,2.0], xl=r'$t$', yl=r'$E_{eff}$',ax=dv.class_logger(dv.make_std_axes()), **c2fit_kw)

name='data_out/c2fit_plot'
dv.save_figdraw(name + '.plt.py', ax=ax0)
ax0.figure.savefig(name + '.pdf')

dslice_no_outliers=np.r_[1,4,5,6,7,8,9,15,16,19,20,21,22,23,24,25,27,29] # XPEHb

"""
