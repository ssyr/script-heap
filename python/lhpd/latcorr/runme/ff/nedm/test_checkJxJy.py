# NEDM/CEDM project
# this script provides examples on how to read data from stripped HDF5 files
# also, it reads matrix el. of spatial vec.current for smallest non-zero momenta
#   and computes SNRs for  them

from __future__ import print_function
from past.builtins import execfile
# load data set parameters
import itertools as it
execfile('config.data.py') ; execfile('ens_setup.py')

# some constants to save keystrokes
mlat=0.7            # lattice value of the nucleon mass # FIXME set to actual value from fits
op='tensor1'        # tag for vector current
ir_name='H4_T1_d4r1'# irrep for vector current (only one)
tpol='posSzplus'    # nucleon polarization matrix
rsplan=('jk',1)     # resampling plan 
tsep=8              # source-sink separation

# the front-end class to the data set
gg=ens_data_get(ama='ub', had='proton', flav='U-D', op=op, data_top=data_out, latsize=latsize, mlat=mlat)

# list of source/sink smearings and separation which are used to extract 
# matrix elements from 2- and 3-point functions
# XXX  only one entry at the moment, but will be extended with additional 
# XXX source-sink separations and (perhaps) other smearing
ssgrp=[ srcsnkpair(dt, 'SS') for dt in [ tsep ] ]

# list of 3-momenta for vertex  momentum projection
qext_list=np.array(list(lhpd.int_vectors_maxnorm(3, 10)))

# dictionary matching Q^2(momentum transfer squared) to a list of pairs of 
# in/out momenta :{ <Q2(float)> : [ (psrc, psnk), ... ], ... }
mc_dict=lhpd.latcorr.make_mom_combo_list(.7, latsize, [[0,0,0]], qext_list)

# list of kinematic points to extract form factors at
# no need to be smart here - the solver will select non-zero equations automatically
mcgrp_list = [ [ mc_case(p0, p1, tpol) for p0,p1 in mc_dict[k] ] for k in sorted(mc_dict.keys()) ]

# list of kinematics for the 2pt fitter
# list of kinematics for the 2pt fitter; only psrc is used
mc_std_list = [ mcgrp[0] for mcgrp in mcgrp_list ]


# compare matrix elements of spatial components of vector current
mcgrp1 = [ mcgrp_list[1][ k ] for k in [0,1,-1,-2] ]    # select x,y mom positive, negative
complist1  = [1, 0, 1, 0]                               # jx, jy, jx, jy
# compute form factor coefficient matrix
eqmat1,pcmat1=lhpd.latcorr.make_ff_eqnmat_precond_f2f(op, mlat, latsize, mcgrp1, [(ir_name,1)])

print("comparing components:")
for i, (mc, comp) in enumerate(zip(mcgrp1, complist1)) : 
    print(i, mc, " == FF.Eq.Matrix[F1/F2] for Re J[%d]" %comp)
    print(eqmat1[i, comp, 0])
    print("SNR of c3pt[t]")
    j_q=gg.get_threept(mc, ssgrp[0], op, ir_name).real[:,comp] ; a,e=lhpd.calc_avg_err(j_q) ; print(a/e)
    print("-------")
"""
jx_qy1=gg.get_threept(mcgrp1[1], ssgrp[0], op, ir_name).real[:,0] ; a,e=lhpd.calc_avg_err(-jx_qy1) ; print(a/e)
jy_qx1=gg.get_threept(mcgrp1[0], ssgrp[0], op, ir_name).real[:,1] ; a,e=lhpd.calc_avg_err(+jy_qx1) ; print(a/e)

mcgrp2=mcgrp_list[1][-1:-3:-1]  # select negative mom in x,y
eqmat2,pcmat2=lhpd.latcorr.make_ff_eqnmat_precond_f2f(op, mlat, latsize, mcgrp2, [(ir_name,1)])
for i_mc, mc in enumerate(mcgrp2) : 
    print(i_mc, mc, " == FF.Eq.Matrix[x/y/z/t, Re/Im, F1/F2]")
    print(eqmat1[i_mc])
    print("-------")
jx_qy2=gg.get_threept(mcgrp2[1], ssgrp[0], op, ir_name).real[:,0] ; a,e=lhpd.calc_avg_err(+jx_qy2) ; print(a/e)
jy_qx2=gg.get_threept(mcgrp2[0], ssgrp[0], op, ir_name).real[:,1] ; a,e=lhpd.calc_avg_err(-jy_qx2) ; print(a/e)
"""

"""
# load also sink 2pt and calc jk-ratio
c2_0=gg.get_twopt_snk(mcgrp1[0], ssgrp[0]).real
c2_0_r=lhpd.resample(c2_0, rsplan)

jx_qy1_r=lhpd.resample(jx_qy1, rsplan)
a,e=lhpd.calc_avg_err(jx_qy1_r/c2_0_r[:,tsep:tsep+1], rsplan) ; print(a, '\n', a/e)

jy_qx1_r=lhpd.resample(jy_qx1, rsplan)
a,e=lhpd.calc_avg_err(jy_qx1_r/c2_0_r[:,tsep:tsep+1], rsplan) ; print(a, '\n', a/e)


c3_gV=gg.get_threept(mcgrp_list[0][0], ssgrp[0], op, ir_name).real[:,3]
c3_gV_r=lhpd.resample(c3_gV, rsplan)
a,e=lhpd.calc_avg_err(c3_gV_r/c2_0_r[:,tsep:tsep+1], rsplan) ; print(a, '\n', a/e)

"""

