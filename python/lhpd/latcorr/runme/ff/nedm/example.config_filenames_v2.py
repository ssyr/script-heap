import aff
import h5py 
import numpy as np
import math
import itertools as it
import lhpd
from lhpd import make_src_grid


def ama_str(ama) : return ama
# TODO change had_tpol -> had because this is traditional for hadspec
def get_hspec_aff_file(cfgkey, ama, csrc, sm_srcsnk) :
    return '%s/hadspec/hadspec.%s.%s.%s.aff' % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrc))
def get_hspec_aff_kpath(cfgkey, csrc, had, sm_srcsnk) :
    return '/hadspec/%s/%s/%s' % (
            sm_srcsnk, had, csrc_str_k(csrc))
def make_hspec_filekpath_func(cfgkey, ama, had, sm_srcsnk) :
    def hsfilekpath_func(csrc) :
        return (get_hspec_aff_file(cfgkey, ama, csrc, sm_srcsnk),
                get_hspec_aff_kpath(cfgkey, csrc, had, sm_srcsnk))
    return hsfilekpath_func
def get_hspec_h5_file(cfgkey, ama, had, sm_srcsnk, data_out=data_out) :
    return '%s/hspec/hspec.%s.%s.%s.%s.h5' % (
            data_out, cfgkey, ama, had, sm_srcsnk)



############### for new-style datafile names ################
def vacins_meta_str_f(vispec) :
    vacins  = vispec['vacins']
    if   'theta' == vacins : 
        rcut_s, dtcut_s = '', ''
        if vispec.get('rcut') : rcut_s  = '.rcut%d'  % vispec['rcut']
        if vispec.get('dtcut'): dtcut_s = '.dtcut%d' % vispec['dtcut']
        return '%s%s%s' % (vispec['gf'], rcut_s, dtcut_s)
    elif 'volcedm'  == vacins : return '%s.%s' % (vispec['flav'], vispec['gf'])
    elif 'volpsc' == vacins :   return '%s' % (vispec['flav'])
    else : raise ValueError(vacins)
def vacins_meta0_str_f(vispec) :
    vacins  = vispec['vacins']
    if   'theta' == vacins : 
        rcut_s, dtcut_s = '', ''
        if vispec.get('rcut') : rcut_s  = '.rcut%d'  % vispec['rcut']
        if vispec.get('dtcut'): dtcut_s = '.dtcut%d' % vispec['dtcut']
        return '%s%s%s' % (vispec['gf'], rcut_s, dtcut_s)
    elif 'volcedm'  == vacins : return '%s' % (vispec['gf'])
    elif 'volpsc' == vacins :   return ''
    else : raise ValueError(vacins)
def vacins_meta_str_k(vispec) :
    vacins  = vispec['vacins']
    if   'theta' == vacins : 
        rcut_s, dtcut_s = '', ''
        if vispec.get('rcut') : rcut_s  = '/rcut%d'  % vispec['rcut']
        if vispec.get('dtcut'): dtcut_s = '/dtcut%d' % vispec['dtcut']
        return '%s%s%s' % (vispec['gf'], rcut_s, dtcut_s)
    elif 'volcedm'  == vacins : return '%s/%s' % (vispec['flav'], vispec['gf'])
    elif 'volpsc' == vacins :   return '%s' % (vispec['flav'])
    else : raise ValueError(vacins)
vacins_str_k = vacins_meta_str_k

def op_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op) :
    return '%sdt%d.%s_%s.%s.%s.%s' % (
            psnk_str_f(psnk), tsep, had, tpol, sm_srcsnk, flav_cur, op)
def bb_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur) :
    return '%sdt%d.%s_%s.%s.%s' % (
            psnk_str_f(psnk), tsep, had, tpol, sm_srcsnk, flav_cur)
def c2pt_meta_str_f(had, sm_srcsnk) :
    return '%s.%s' % (had, sm_srcsnk)
def c2pt_meta0_str_f(csrcgrp, sm_srcsnk) :
    return '%s.%s' % (csrc_str_f(csrcgrp[0]), sm_srcsnk)


# c2pt AFF
def get_c2pt_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk) :
    return '%s/c2pt/c2pt.%s.%s.%s.aff' % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrcgrp[0]))
def get_c2pt_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk) :
    return '/c2pt/%s/%s/%s_%s' % (
            sm_srcsnk, csrcgrp_str_k(csrcgrp), had, tpol)
def make_c2pt_filekpath_func(cfgkey, ama, had, sm_srcsnk) :
    def hsfilekpath_func(csrcgrp, tpol) :
        return (get_c2pt_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk),
                get_c2pt_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk))
    return hsfilekpath_func

# c2pt H5
if False:
    def get_c2pt_h5_file(cfgkey, ama, had, sm_srcsnk, data_out=data_out) :
        return '%s/c2pt/c2pt.%s.%s.%s.%s.h5' % (
                data_out, cfgkey, ama, had, sm_srcsnk)
    def get_c2pt_all_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
        return '%s/c2pt.all/c2pt-all.%s.%s.%s.h5' % (
                data_out, ama, had, sm_srcsnk)
    def get_c2pt_bin_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
        return '%s/c2pt.bin/c2pt-bin.%s.%s.%s.h5' % (
                data_out, ama, had, sm_srcsnk)
    def get_c2pt_unbias_h5_file(had, sm_srcsnk, data_out=data_out) :
        return '%s/c2pt.bin/c2pt-unbias.%s.%s.h5' % (
                data_out, had, sm_srcsnk)
else :
    def get_c2pt_h5_file(cfgkey, ama, had, sm_srcsnk, data_out=data_out) :
        return '%s/c2pt/c2pt.%s.%s.%s.h5' % (
                data_out, cfgkey, ama, c2pt_meta_str_f(had, sm_srcsnk))
    def get_c2pt_dset_h5_file(ama, had, sm_srcsnk, dset, data_out=data_out) :
        n1      = '%s/c2pt.bin/c2pt-%s.%s' % (data_out, dset, ama) 
        return '%s.%s.h5' % (n1, c2pt_meta_str_f(had, sm_srcsnk))
    def get_c2pt_bin_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
        return get_c2pt_dset_h5_file(ama, had, sm_srcsnk, 'bin', data_out=data_out)
    def get_c2pt_unbias_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
        return get_c2pt_dset_h5_file(ama, had, sm_srcsnk, 'unbias', data_out=data_out)
    def get_c2pt_all_h5_file(ama, had, sm_srcsnk, data_out=data_out) :
        return get_c2pt_dset_h5_file(ama, had, sm_srcsnk, 'all', data_out=data_out)


# c2pt vacins AFF
if False :
    def get_c2pt_volcedm_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk, gf_cedm) :
        return '%s/c2pt_volcedm/c2pt_volcedm.%s.%s.%s.%s.aff' % (
                data_top, cfgkey, ama_str(ama), csrc_str_f(csrcgrp[0]), gf_cedm)
    def get_c2pt_volcedm_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk, flav_cedm, gf_cedm) :
        return '/c2pt_volcedm/%s/%s/%s/%s_%s/%s' % (
                sm_srcsnk, csrcgrp_str_k(csrcgrp), gf_cedm, had, tpol, flav_cedm)
    def make_c2pt_volcedm_filekpath_func(cfgkey, ama, had, sm_srcsnk, flav_cedm, gf_cedm) :
        def hsfilekpath_func(csrcgrp, tpol) :
            return (get_c2pt_volcedm_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk, gf_cedm),
                    get_c2pt_volcedm_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk, flav_cedm, gf_cedm))
        return hsfilekpath_func

else :
    def get_c2pt_vacins_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk, vispec) :
        vacins = vispec['vacins']
        vispecx = vacins_meta0_str_f(vispec)
        if '' != vispecx : vispecx = '.' + vispecx
        return '%s/c2pt_%s/c2pt_%s.%s.%s.%s%s.aff' % (
                data_top, vacins, vacins, cfgkey, ama_str(ama), csrc_str_f(csrcgrp[0]), vispecx)
    def get_c2pt_vacins_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk, vispec) :
        vacins = vispec['vacins']
        if   'volcedm' == vacins :
            return '/c2pt_%s/%s/%s/%s/%s_%s/%s' % (
                    vacins, sm_srcsnk, csrcgrp_str_k(csrcgrp), vispec['gf'], had, tpol, vispec['flav'])
        elif 'volpsc' == vacins :
            return '/c2pt_%s/%s/%s/%s/%s_%s/%s' % (
                    vacins, sm_srcsnk, csrcgrp_str_k(csrcgrp), 'orig', had, tpol, vispec['flav'])
        else : raise ValueError(vacins)
    def make_c2pt_vacins_filekpath_func(cfgkey, ama, had, sm_srcsnk, vispec) :
        def hsfilekpath_func(csrcgrp, tpol) :
            return (get_c2pt_vacins_aff_file(cfgkey, ama, csrcgrp, sm_srcsnk, vispec),
                    get_c2pt_vacins_aff_kpath(cfgkey, csrcgrp, had, tpol, sm_srcsnk, vispec))
        return hsfilekpath_func

# c2pt vacins H5
if False:
    def get_c2pt_volcedm_h5_file(cfgkey, ama, had, sm_srcsnk, flav_cedm, gf_cedm, data_out=data_out) :
        return '%s/c2pt_volcedm/c2pt_volcedm.%s.%s.%s.%s.%s.%s.h5' % (
                data_out, cfgkey, ama, had, sm_srcsnk, flav_cedm, gf_cedm)
    def get_c2pt_volcedm_bin_h5_file(ama, had, sm_srcsnk, flav_cedm, gf_cedm, data_out=data_out) :
        return '%s/c2pt_volcedm.bin/c2pt_volcedm-bin.%s.%s.%s.%s.%s.h5' % (
                data_out, ama, had, sm_srcsnk, flav_cedm, gf_cedm)
    def get_c2pt_volcedm_unbias_h5_file(had, sm_srcsnk, flav_cedm, gf_cedm, data_out=data_out) :
        return '%s/c2pt_volcedm.bin/c2pt_volcedm-unbias.%s.%s.%s.%s.h5' % (
                data_out, had, sm_srcsnk, flav_cedm, gf_cedm)

else:
    def get_c2pt_vacins_h5_file(cfgkey, ama, had, sm_srcsnk, vispec, data_out=data_out) :
        vacins  = vispec['vacins']
        return '%s/c2pt_%s/c2pt_%s.%s.%s.%s.%s.h5' % (
                data_out, vacins, vacins, cfgkey, ama, 
                c2pt_meta_str_f(had, sm_srcsnk), vacins_meta_str_f(vispec))
    def get_c2pt_vacins_dset_h5_file(ama, had, sm_srcsnk, vispec, dset, data_out=data_out) :
        vacins  = vispec['vacins']
        n1      = '%s/c2pt_%s.bin/c2pt_%s-%s.%s' % (data_out, vacins, vacins, dset, ama)
        return '%s.%s.%s.h5' % (n1, c2pt_meta_str_f(had, sm_srcsnk), vacins_meta_str_f(vispec))
    def get_c2pt_vacins_bin_h5_file(ama, had, sm_srcsnk, vispec, data_out=data_out) :
        return get_c2pt_vacins_dset_h5_file(ama, had, sm_srcsnk, vispec, 'bin', data_out=data_out)
    def get_c2pt_vacins_unbias_h5_file(ama, had, sm_srcsnk, vispec, data_out=data_out) :
        return get_c2pt_vacins_dset_h5_file(ama, had, sm_srcsnk, vispec, 'unbias', data_out=data_out)
    def get_c2pt_vacins_all_h5_file(ama, had, sm_srcsnk, vispec, data_out=data_out) :
        return get_c2pt_vacins_dset_h5_file(ama, had, sm_srcsnk, vispec, 'all', data_out=data_out)

# bb AFF
def get_bb_aff_file(cfgkey, ama, csrcgrp, psnk, tsep, tpol, flav_cur) :
    return "%s/bb/bb.%s.%s.%s.%sdt%d.%s.%s.aff" % (
            data_top, cfgkey, ama_str(ama), csrc_str_f(csrcgrp[0]), 
            psnk_str_f(psnk), tsep, tpol, flav_cur)
def get_bb_aff_kpath(cfgkey, csrcgrp, psnk, tsep, sm_srcsnk, had, tpol, flav_cur) :
    return "/bb/%s/%s_%s/%s/%s/%s_dt%d" % (
            sm_srcsnk, had, tpol, flav_cur, csrcgrp_str_k(csrcgrp), psnk_str_k(psnk), tsep)
def make_bb_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, flav_cur) :
    """ return aff file and kpath generator function to be used with `conv_bb2hdf'
    """
    def bbfilekpath_func(csrcgrp, tsep) :
        return (
            get_bb_aff_file(cfgkey, ama, csrcgrp, psnk, tsep, tpol, flav_cur),
            get_bb_aff_kpath(cfgkey, csrcgrp, psnk, tsep, sm_srcsnk, had, tpol, flav_cur))
    return bbfilekpath_func

# bb H5
def get_bb_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, data_out=data_out) :
    return '%s/bb/bb.%s.%s.%s.h5' % (data_out, cfgkey, ama, 
            bb_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur))
def get_bb_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, data_out=data_out) :
    return '%s/bb.bin/bb-bin.%s.%s.h5' % (data_out, ama, 
            bb_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur))
def get_bb_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, data_out=data_out) :
    return '%s/bb.bin/bb-unbias.%s.%s.h5' % (data_out, 
            bb_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur))

# op H5
if False:
    def get_op_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
        return '%s/op/op.%s.%s.%sdt%d.%s_%s.%s.%s.%s.h5' % (
                data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
                had, tpol, sm_srcsnk, flav_cur, op)
    def get_op_all_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
        return '%s/op.all/op-all.%s.%sdt%d.%s_%s.%s.%s.%s.h5' % (
                data_out, ama, psnk_str_f(psnk), tsep, 
                had, tpol, sm_srcsnk, flav_cur, op)
    def get_op_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
        return '%s/op.bin/op-bin.%s.%sdt%d.%s_%s.%s.%s.%s.h5' % (
                data_out, ama, psnk_str_f(psnk), tsep, 
                had, tpol, sm_srcsnk, flav_cur, op)
    def get_op_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
        return '%s/op.bin/op-unbias.%sdt%d.%s_%s.%s.%s.%s.h5' % (
                data_out, psnk_str_f(psnk), tsep, 
                had, tpol, sm_srcsnk, flav_cur, op)
else :
    def get_op_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
        return '%s/op/op.%s.%s.%s.h5' % (
                data_out, cfgkey, ama, 
                op_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op))
    def get_op_dset_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, dset, data_out=data_out) :
        n1      = '%s/op.bin/op-%s.%s' % (data_out, dset, ama)
        return '%s.%s.h5' % (n1, 
                op_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op))
    def get_op_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
        return get_op_dset_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, 'bin', data_out=data_out)
    def get_op_unbias_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
        return get_op_dset_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, 'unbias', data_out=data_out)
    def get_op_all_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, data_out=data_out) :
        return get_op_dset_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, 'all', data_out=data_out)


# bb vacins AFF
if False : raise NotImplementedError
else :
    def get_bb_vacins_aff_file(cfgkey, ama, csrcgrp, psnk, tsep, tpol, flav_cur, vispec) :
        vacins  = vispec['vacins']
        return "%s/bb_%s/bb_%s.%s.%s.%s.%sdt%d.%s.%s.%s.aff" % (
                data_top, vacins, vacins, cfgkey, ama_str(ama), csrc_str_f(csrcgrp[0]), 
                psnk_str_f(psnk), tsep, tpol, flav_cur, vacins_meta_str_f(vispec))

    def get_bb_vacins_aff_kpath(cfgkey, csrcgrp, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, vispec) :
        vacins  = vispec['vacins']
        if 'volcedm' == vacins :
            return "/bb_%s/%s/%s/%s_%s/%s/%s/%s/%s_dt%d" % (
                vacins, sm_srcsnk, vispec['gf'], had, tpol, flav_cur, vispec['flav'], csrcgrp_str_k(csrcgrp), psnk_str_k(psnk), tsep)
        elif 'volpsc' == vacins :
            return "/bb_%s/%s/%s/%s_%s/%s/%s/%s/%s_dt%d" % (
                vacins, sm_srcsnk, 'orig',  had, tpol, flav_cur, vispec['flav'], csrcgrp_str_k(csrcgrp), psnk_str_k(psnk), tsep)
        else : raise ValueError(vacins)

    def make_bb_vacins_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, flav_cur, vispec) :
        """ return aff file and kpath generator function to be used with `conv_bb2hdf'
        """
        def bbfilekpath_func(csrcgrp, tsep) :
            return (
                get_bb_vacins_aff_file(cfgkey, ama, csrcgrp, psnk, tsep, tpol, flav_cur, vispec),
                get_bb_vacins_aff_kpath(cfgkey, csrcgrp, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, vispec))
        return bbfilekpath_func

# bb vacins H5
if False : raise NotImplementedError
else :
    def get_bb_vacins_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, vispec, data_out=data_out) :
        vacins  = vispec['vacins']
        return '%s/bb_%s/bb_%s.%s.%s.%s.%s.h5' % (
                data_out, vacins, vacins, cfgkey, ama, 
                bb_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur), 
                vacins_meta_str_f(vispec))
    #def get_bb_volcedm_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm, data_out=data_out) :
        #return '%s/bb_volcedm.bin/bb_volcedm-bin.%s.%sdt%d.%s_%s.%s.%s.%s.%s.h5' % (
                #data_out, ama, psnk_str_f(psnk), tsep, 
                #had, tpol, sm_srcsnk, flav_cur, flav_cedm, gf_cedm)
    #def get_bb_volcedm_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm, data_out=data_out) :
        #return '%s/bb_volcedm.bin/bb_volcedm-unbias.%s.%sdt%d.%s_%s.%s.%s.%s.%s.h5' % (
                #data_out, psnk_str_f(psnk), tsep, 
                #had, tpol, sm_srcsnk, flav_cur, flav_cedm, gf_cedm)

# op vacins H5
if False:
    def get_op_volcedm_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=data_out) :
        return '%s/op_volcedm/op_volcedm.%s.%s.%sdt%d.%s_%s.%s.%s.%s.%s.%s.h5' % (
                data_out, cfgkey, ama, psnk_str_f(psnk), tsep, 
                had, tpol, sm_srcsnk, flav_cur, op, flav_cedm, gf_cedm)
    def get_op_volcedm_all_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=data_out) :
        return '%s/op_volcedm.all/op_volcedm-all.%s.%sdt%d.%s_%s.%s.%s.%s.%s.%s.h5' % (
                data_out, ama, psnk_str_f(psnk), tsep, 
                had, tpol, sm_srcsnk, flav_cur, op, flav_cedm, gf_cedm)
    def get_op_volcedm_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=data_out) :
        return '%s/op_volcedm.bin/op_volcedm-bin.%s.%sdt%d.%s_%s.%s.%s.%s.%s.%s.h5' % (
                data_out, ama, psnk_str_f(psnk), tsep, 
                had, tpol, sm_srcsnk, flav_cur, op, flav_cedm, gf_cedm)
    def get_op_volcedm_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=data_out) :
        return '%s/op_volcedm.bin/op_volcedm-unbias.%sdt%d.%s_%s.%s.%s.%s.%s.%s.h5' % (
                data_out, psnk_str_f(psnk), tsep, 
                had, tpol, sm_srcsnk, flav_cur, op, flav_cedm, gf_cedm)
else :
    def get_op_vacins_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, vispec, data_out=data_out) :
        vacins = vispec['vacins']
        return '%s/op_%s/op_%s.%s.%s.%s.%s.h5' % (
                data_out, vacins, vacins, cfgkey, ama, 
                op_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op), 
                vacins_meta_str_f(vispec))
    def get_op_vacins_dset_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, vispec, dset, data_out=data_out) :
        vacins  = vispec['vacins']
        if '' == ama : amax    = ''
        else : amax = '.' + ama
        n1      = '%s/op_%s.bin/op_%s-%s%s' % (data_out, vacins, vacins, dset, amax)
        return '%s.%s.%s.h5' % (n1, 
                op_meta_str_f(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op), 
                vacins_meta_str_f(vispec))
    def get_op_vacins_all_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, vispec, data_out=data_out) :
        return get_op_vacins_dset_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, vispec, 'all', data_out=data_out)
    def get_op_vacins_bin_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, vispec, data_out=data_out) :
        return get_op_vacins_dset_h5_file(ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, vispec, 'bin', data_out=data_out)
    def get_op_vacins_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, vispec, data_out=data_out) :
        return get_op_vacins_dset_h5_file('', psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op, vispec, 'unbias', data_out=data_out)


# qtopo
def get_qtopo_orig_h5_file(cfgkey, tag) :
    return '%s/qtopo/qtopo.%s.%s.h5' % (data_top, cfgkey, tag)
def get_qtopo_proc_h5_file(cfgkey, tag) :
    return '%s/qtopo/qtopo.%s.%s.h5' % (data_out, cfgkey, tag)

