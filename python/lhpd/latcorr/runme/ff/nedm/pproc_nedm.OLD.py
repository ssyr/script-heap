#import ipdb
from __future__ import print_function
from future.utils import iteritems
import numpy as np
import h5py
import lhpd
from lhpd.misc import np_find_first, np_match, purge_keys
import os, sys, time, tempfile, errno
from lhpd.pymath.H4_tensor import *
#import matplotlib as mpl
#import matplotlib.pyplot as plt
#import dataview as dv
from lhpd.limits import *

###############################################################################
# HADSPEC
###############################################################################

def conv_c2pt2hdf(
            h5g, h5key,             # output
            latsize, 
            hsfilekpath_func,       # input
            cfgkey, csrcgrp_list, psnk_list, tpol_list, hslab_len,
            had_bc_t,
            attrs_kw, 
            t_dir=3, hs_dtype=np.complex128, h5_overwrite=True) :
    """ looping over "coherent" 2pt : compatible with non-coherent
        csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
        
        save data to
        h5g[h5key][n_data, len(psnk_list), hslab_len]

        XXX should also work for hspec_volcedm

        TODO check that hslabs do not overlap
    """
    latsize     = np.asarray(latsize)
    ndim        = len(latsize)
    assert(4 == ndim)
    assert(3 == t_dir)  # smart, eh?
    lt          = latsize[t_dir]
    csrcgrp_list= np.asarray(csrcgrp_list)
    n_grp, grp_size = csrcgrp_list.shape[0:2]
    n_data      = n_grp * grp_size
    assert(4 == csrcgrp_list.shape[-1])
    n_psnk      = len(psnk_list)
    n_tpol      = len(tpol_list)
    
    csrc_list   = []
    hs_res      = np.empty((n_data, n_tpol, n_psnk, hslab_len), hs_dtype)

    i_data = 0
    for i_grp, csrcgrp in enumerate(csrcgrp_list) :
        n_src       = len(csrcgrp)
        # check coord
        for i_src in range(n_src) :
            for mu in range(ndim):
                assert(mu == t_dir or csrcgrp[i_src][mu] == csrcgrp[0][mu])
        print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
        csrc0       = csrcgrp[0]
        tsrc0       = csrc0[t_dir]
        
        for i_tpol, tpol in enumerate(tpol_list) :
            hsfile, hskpath = hsfilekpath_func(csrc0, tpol)
            aff_r   = aff.Reader(hsfile)
            hs      = lhpd.latcorr.aff_read_hadspec_list(aff_r, hskpath, psnk_list)
            aff_r.close()

            for i_c, c in enumerate(csrcgrp) :
                t_sh    = (lt + c[t_dir] - tsrc0) % lt
                # correcting BC[t] applied in [Qlua]save_2pt_list
                if tsrc0 <= c[t_dir] : bc_factor = 1.
                else : bc_factor = 1. / had_bc_t
                hs_res[i_data + i_c, i_tpol] = bc_factor * hs[..., t_sh : t_sh + hslab_len]

        csrc_list.extend(list(csrcgrp))
        i_data += len(csrcgrp)

    assert(n_data == i_data)

    meas_spec_list = np.array([ (cfgkey, c) for c in csrc_list], 
                        dtype=lhpd.h5_io.h5_meas_dtype)
    assert(len(meas_spec_list) == n_data)

    if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5key])
    hs_shape = (n_data, n_tpol, n_psnk, hslab_len)
    h = h5g.require_dataset(h5key, hs_shape, hs_dtype, fletcher32=True)
    h[:] = hs_res

    # "axes"
    h.attrs['dim_spec']      = np.array(['i_data', 'i_tpol', 'i_psnk', 'i_t' ],
                                           dtype='S32')
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(h, meas_spec_list)
    h.attrs['psnk_list']    = np.array(psnk_list, dtype=np.int32)
    h.attrs['tpol_list']    = np.array(tpol_list, dtype='S16')
    h.attrs['t_list']       = np.r_[0 : hslab_len]

    for k, v in iteritems(attrs_kw) :
        h.attrs[k] = v

    h.file.flush()


def conv_bb2hdf(
            h5g, h5key,             # output
            latsize, 
            bbfilekpath_func,       # input
            cfgkey, csrcgrp_list, tsep, qext_list, lpath_list, 
            had_bc_t,
            attrs_kw, 
            tsnk_full=False,
            t_dir=3, bb_dtype=np.complex128, h5_overwrite=True) :
    """
        csrcgrp_list [i_grp, i_csrc, mu] - 2-level grouping 
        
        save data to
        h5g[h5key][n_data, n_gamma, len(lpath_list), len(qext_list), tsep+1]
    """
    """ TEST 
    """
    print("WARNING : APPLYING BC_T FOR WRAP-AROUND BB")
    latsize     = np.asarray(latsize)
    ndim        = len(latsize)
    assert(4 == ndim)
    assert(3 == t_dir)  # smart, eh?
    lt          = latsize[t_dir]
    csrcgrp_list= np.asarray(csrcgrp_list)
    n_grp, grp_size = csrcgrp_list.shape[0:2]
    n_data      = n_grp * grp_size
    assert(4 == csrcgrp_list.shape[-1])

    qext_list   = np.array(qext_list)
    n_qext      = len(qext_list)
    n_gamma     = 16
    n_lpath     = len(lpath_list)

    csrc_list   = []
    bb_res      = np.empty((n_data, n_gamma, n_lpath, n_qext, 1+tsep), bb_dtype)

    i_data = 0
    for i_grp, csrcgrp in enumerate(csrcgrp_list) :
        n_src       = len(csrcgrp)
        # check coord
        for i_src in range(n_src) :
            for mu in range(ndim):
                assert(mu == t_dir or csrcgrp[i_src][mu] == csrcgrp[0][mu])
        print(i_grp, '{', ', '.join( [ str(c) for c in csrcgrp ]), '} # ', time.asctime())
        csrc0       = csrcgrp[0]
        tsrc0       = csrc0[t_dir]
        
        if tsnk_full : tsnk_bb  = (lt + tsrc0 - 1) % lt
        else : tsnk_bb = (lt + tsrc0 + tsep) % lt

        bbfile, bbkpath = bbfilekpath_func(csrc0, tsnk_bb)
        aff_r   = aff.Reader(bbfile)
        bb      = lhpd.latcorr.aff_read_bb_list(aff_r, bbkpath, 
                        range(n_gamma), lpath_list, qext_list)
        if tsnk_full : assert(bb.shape[-1] == lt)
        aff_r.close()

        for i_c, c in enumerate(csrcgrp) :
            # XXX [Qlua] qcd.save_bb DOES NOT apply BC[t]
            # XXX calc_c23pt_cpbar_volcedm.qlua DOES NOT apply BC[t] to seqsrc
            # XXX therefore, NEED TO APPLY BC_FACTOR HERE
            if c[t_dir] + tsep < lt: bc_factor = 1
            else : bc_factor = had_bc_t
            t_sh    = (lt + c[t_dir] - tsrc0) % lt
            bb_res[i_data] = bc_factor * bb[..., t_sh : t_sh + tsep + 1]
            csrc_list.append(c)
            i_data += 1

    assert(n_data == i_data)

    meas_spec_list = np.array([ (cfgkey, c) for c in csrc_list], 
                        dtype=lhpd.h5_io.h5_meas_dtype)
    assert(len(meas_spec_list) == n_data)

    if h5_overwrite : lhpd.h5_io.h5_purge_keys(h5g, [h5key])
    bb_shape = (n_data, n_gamma, len(lpath_list), len(qext_list), tsep + 1)
    b = h5g.require_dataset(h5key, bb_shape, bb_dtype, fletcher32=True)
    b[:] = bb_res

    # "axes"
    b.attrs['dim_spec']      = np.array(['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau' ],
                                           dtype='S32')
    # axes "ticks"
    lhpd.h5_io.h5_set_datalist(b, meas_spec_list)
    b.attrs['gamma_list']    = np.r_[0 : 16]
    b.attrs['linkpath_list'] = np.array(
            [ lhpd.strkey.lpath_str(lp) for lp in lpath_list ],
            dtype='S16')
    b.attrs['qext_list']     = np.array(qext_list, dtype=np.int32)
    b.attrs['tau_list']      = np.r_[0 : tsep+1]
    # other parameters
    b.attrs['source_sink_dt']= int(tsep)

    for k, v in iteritems(attrs_kw) :
        b.attrs[k] = v

    b.file.flush()



# compute op's and store in an HDF5 file
def calc_save_op(h5_file, h5_kpath, 
                latsize, 
                h5_bb,
                p3snk, tsep, op, ir_name ) :
    """
        h5_file     open HDF5 h5py file
        h5_kpath    kpath relative to root
    """
    p3snk = np.asarray(p3snk)
    assert (p3snk == h5_bb.attrs['sink_mom']).all()
    assert (tsep == h5_bb.attrs['source_sink_dt'])

    n_data  = h5_bb.shape[0]
    n_qext  = h5_bb.shape[-2]
    n_tau   = h5_bb.shape[-1]
    
    def func_get_bb(gamma, lpath_str, q3ext) :
        i_l = np_find_first(lpath_str, h5_bb.attrs['linkpath_list'])[0]
        i_g = np_find_first(gamma, h5_bb.attrs['gamma_list'])[0]     
        i_q = np_find_first(q3ext, h5_bb.attrs['qext_list'])[0]
        return h5_bb[:, i_g, i_l, i_q]

    n_comp = H4_repr_dim[ir_name]
    op_shape = (n_data, n_comp, n_qext, n_tau)
    # [i_data, i_comp, i_qext, i_tau]
    h5_op = h5_file.require_dataset(h5_kpath, op_shape, np.complex128, 
                    fletcher32=True)
    
    for i_qext, q3ext in enumerate(h5_bb.attrs['qext_list']) :
        p3src = p3snk - q3ext

        # TODO cleanup: use universal functions (esp.>=1 link ops.) to keep it DRY
        # TODO add tensor0, pstensor0, sigma2a2
        if 'tensor0' == op : 
            assert('H4_T0_d1r1' == ir_name)
            h5_op[:, 0, i_qext, :] =  func_get_bb( 0, 'l0_', q3ext)
        elif 'pstensor0' == op : 
            assert('H4_T0_d1r1' == ir_name)
            h5_op[:, 0, i_qext, :] =  func_get_bb(15, 'l0_', q3ext)
        elif 'tensor1' == op : 
            assert('H4_T1_d4r1' == ir_name)
            h5_op[:, 0, i_qext, :] =  func_get_bb( 1, 'l0_', q3ext)
            h5_op[:, 1, i_qext, :] =  func_get_bb( 2, 'l0_', q3ext)
            h5_op[:, 2, i_qext, :] =  func_get_bb( 4, 'l0_', q3ext)
            h5_op[:, 3, i_qext, :] =  func_get_bb( 8, 'l0_', q3ext)
        elif 'pstensor1' == op :
            assert('H4_T1_d4r1' == ir_name)
            h5_op[:, 0, i_qext, :] =  func_get_bb(14, 'l0_', q3ext)
            h5_op[:, 1, i_qext, :] = -func_get_bb(13, 'l0_', q3ext)
            h5_op[:, 2, i_qext, :] =  func_get_bb(11, 'l0_', q3ext)
            h5_op[:, 3, i_qext, :] = -func_get_bb( 7, 'l0_', q3ext)
        elif 'sigma2a2' == op :
            assert('H4_T2_d6r1' == ir_name)     # antisymmetric
            h5_op[:, 0, i_qext, :] =  1j*func_get_bb( 3, 'l0_', q3ext)
            h5_op[:, 1, i_qext, :] =  1j*func_get_bb( 5, 'l0_', q3ext)
            h5_op[:, 2, i_qext, :] =  1j*func_get_bb( 9, 'l0_', q3ext)
            h5_op[:, 3, i_qext, :] =  1j*func_get_bb( 6, 'l0_', q3ext)
            h5_op[:, 4, i_qext, :] =  1j*func_get_bb(10, 'l0_', q3ext)
            h5_op[:, 5, i_qext, :] =  1j*func_get_bb(12, 'l0_', q3ext)
        else : raise NotImplemented((op, ir_name))

    lhpd.h5_io.h5_copy_attr(h5_op, h5_bb, [
            'sink_mom', 'tau_list', 'qext_list', 'source_sink_dt', 
            'source_hadron', 'sink_hadron', 'tpol'],
            check_key=True)
    lhpd.h5_io.h5_set_datalist(h5_op, lhpd.h5_io.h5_get_datalist(h5_bb))
    h5_op.attrs['dim_spec'] = np.array(['i_data', 'i_comp', 'i_qext', 'i_tau' ],
                                       dtype='S32')
    h5_op.attrs['comp_list'] = H4_repr_comp[ir_name]
