#!/usr/bin/env python
from __future__ import print_function
from past.builtins import execfile

import os, sys

if '__main__' == __name__ :
    def print_usage(prg) :
        print("""Usage:
%s  <what>  <config>  [<job_id0>, ...]
What :
    conv_hspec          # FIXME : change tpol_list->hspec_list or remove it alltogether from dim_spec
    conv_c2pt
    conv_c2pt_volcedm
    #conv_c2pt_cpbar
    conv_bb
    conv_bb_volcedm
    calc_op
    calc_op_volcedm
    bin_unbias_c2pt
    bin_unbias_c2pt_volcedm
    bin_unbias_op
    bin_unbias_op_volcedm
    merge_c2pt
    merge_op
    merge_op_volcedm
    qtopo_r2cut
""" % (prg,))

    if len(sys.argv) < 3 : 
        print_usage(sys.argv[0] or 'me')
        sys.exit(1)
    conf_py   = sys.argv[2]
    execfile(conf_py)
    execfile('pproc_nedm.py')


###############################################################################
# c2pt
##############################################################################
def runme_conv_c2pt(cfgkey, ama, sm_srcsnk,
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                hslab_len=c2pt_hslab_len,
                psnk_list=c2pt_psnk_list,
                tpol_list=c2pt_tpol_list,
                attrs_kw={}) :
    """ convert CP-even 2pt functions 
    """
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)

    h5file  = get_c2pt_h5_file(cfgkey, ama, had, sm_srcsnk)
    h5kpath = "/cfg%s/c2pt/%s/%s" % (cfgkey, sm_srcsnk, had)
    print("# >>> %s [ %s ]" % (h5file, h5kpath))
    attrs_kw_this = dict(attrs_kw)
    attrs_kw_this.update(source_hadron=had, sink_hadron=had)

    lhpd.mkpath(os.path.dirname(h5file))
    h5f     = h5py.File(h5file, 'a')
    hskf    = make_c2pt_filekpath_func(cfgkey, ama, had, sm_srcsnk)
    conv_c2pt2hdf(h5f, h5kpath, latsize, hskf, cfgkey, 
                csrcgrp_list, psnk_list, tpol_list, c2pt_hslab_len, 
                baryon_bc_t, attrs_kw_this)
    h5f.flush()
    h5f.close()

def runme_bin_unbias_c2pt(cfgkey_list, sm_srcsnk,
                had='proton',
                ama_list=ama_list,
                VERBOSE=False) :
    h5_kpath  = "/c2pt/%s/%s" % (sm_srcsnk, had)
    
    # bin ex
    if ama_ex in ama_list : 
        h5_list_ex  = [ (get_c2pt_h5_file(c, ama_ex, had, sm_srcsnk),
                        '/cfg%s/%s' % (c, h5_kpath)) 
                        for c in cfgkey_list ]
        h5_file_ex  = get_c2pt_bin_h5_file(ama_ex, had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_ex))
        lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
                
    # bin sl
    if ama_sl in ama_list :
        h5_list_sl  = [ (get_c2pt_h5_file(c, ama_sl, had, sm_srcsnk),
                        '/cfg%s/%s' % (c, h5_kpath))
                        for c in cfgkey_list ]
        h5_file_sl  = get_c2pt_bin_h5_file(ama_sl, had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_sl))
        lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

    # unbias
    if ama_ex in ama_list and ama_sl in ama_list : 
        h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
        h5_file_ub  = get_c2pt_unbias_h5_file(had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_ub))
        lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

def runme_merge_c2pt(cfgkey_list, ama, sm_srcsnk,
                had='proton',
                VERBOSE=False) :
    h5_kpath  = "/c2pt/%s/%s" % (sm_srcsnk, had)
    
    h5_list  = [ (get_c2pt_h5_file(c, ama, had, sm_srcsnk),
                    '/cfg%s/%s' % (c, h5_kpath)) 
                    for c in cfgkey_list ]
    h5_file  = get_c2pt_all_h5_file(ama, had, sm_srcsnk)
    lhpd.mkpath(os.path.dirname(h5_file))
    lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)


##############################################################################
# c2pt_volcedm
###############################################################################
def runme_conv_c2pt_volcedm(cfgkey, ama, sm_srcsnk,
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                hslab_len=c2pt_hslab_len,
                psnk_list=c2pt_psnk_list,
                tpol_list=c2pt_tpol5_list,
                gf_cedm_list=gf_cedm_list,
                flav_cedm_list=flav_cedm_list,
                attrs_kw={}) :
    """ convert CP-even 2pt functions 
    """
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    for gf_cedm in gf_cedm_list :
        for flav_cedm in flav_cedm_list :
            h5file  = get_c2pt_volcedm_h5_file(cfgkey, ama, had, sm_srcsnk, flav_cedm, gf_cedm)
            h5kpath = "/cfg%s/c2pt_volcedm/%s/%s/%s/%s" % (cfgkey, sm_srcsnk, had, flav_cedm, gf_cedm)
            print("# >>> %s [ %s ]" % (h5file, h5kpath))
            attrs_kw_this = dict(attrs_kw)
            attrs_kw_this.update(source_hadron=had, sink_hadron=had)

            lhpd.mkpath(os.path.dirname(h5file))
            h5f     = h5py.File(h5file, 'a')
            hskf    = make_c2pt_volcedm_filekpath_func(cfgkey, ama, had, sm_srcsnk, flav_cedm, gf_cedm)
            conv_c2pt2hdf(h5f, h5kpath, latsize, hskf, cfgkey, 
                        csrcgrp_list, psnk_list, tpol_list, c2pt_hslab_len, 
                        baryon_bc_t, attrs_kw_this)
            h5f.flush()
            h5f.close()

def runme_bin_unbias_c2pt_volcedm(cfgkey_list, sm_srcsnk,
                had='proton',
                gf_cedm_list=gf_cedm_list,
                flav_cedm_list=flav_cedm_list,
                ama_list=ama_list,
                VERBOSE=False) :
    # since Tpol is TgN, keep them all together
    for gf_cedm, flav_cedm in it.product(gf_cedm_list, flav_cedm_list) :
        h5_kpath  = "/c2pt_volcedm/%s/%s/%s/%s" % (sm_srcsnk, had, flav_cedm, gf_cedm)
            
        # bin ex
        if ama_ex in ama_list : 
            h5_list_ex  = [ (get_c2pt_volcedm_h5_file(c, ama_ex, had, sm_srcsnk, flav_cedm, gf_cedm),
                            '/cfg%s/%s' % (c, h5_kpath)) 
                            for c in cfgkey_list ]
            h5_file_ex  = get_c2pt_volcedm_bin_h5_file(ama_ex, had, sm_srcsnk, flav_cedm, gf_cedm)
            lhpd.mkpath(os.path.dirname(h5_file_ex))
            lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
                        
        # bin sl
        if ama_sl in ama_list :
            h5_list_sl  = [ (get_c2pt_volcedm_h5_file(c, ama_sl, had, sm_srcsnk, flav_cedm, gf_cedm),
                            '/cfg%s/%s' % (c, h5_kpath))
                            for c in cfgkey_list ]
            h5_file_sl  = get_c2pt_volcedm_bin_h5_file(ama_sl, had, sm_srcsnk, flav_cedm, gf_cedm)
            lhpd.mkpath(os.path.dirname(h5_file_sl))
            lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
                
        # unbias
        if ama_ex in ama_list and ama_sl in ama_list : 
            h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
            h5_file_ub  = get_c2pt_volcedm_unbias_h5_file(had, sm_srcsnk, flav_cedm, gf_cedm)
            lhpd.mkpath(os.path.dirname(h5_file_ub))
            lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

###############################################################################
# hspec TODO
###############################################################################
def runme_conv_hspec(cfgkey, ama, sm_srcsnk,
                baryon_bc_t=ferm_bc[3]**3,
                hslab_len=c2pt_hslab_len,
                psnk_list=c2pt_psnk_list,
                baryon_list=hspec_baryon_list,
                meson_list=hspec_meson_list,
                attrs_kw={}) :
    """ convert CP-even 2pt functions 
    """
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    for had in list(baryon_list) + list(meson_list) :
        h5file  = get_hspec_h5_file(cfgkey, ama, had, sm_srcsnk)
        h5kpath = "/cfg%s/c2pt/%s/%s" % (cfgkey, sm_srcsnk, had)
        print("# >>> %s [ %s ]" % (h5file, h5kpath))
        attrs_kw_this = dict(attrs_kw)
        attrs_kw_this.update(source_hadron=had, sink_hadron=had)

        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')
        hskf    = make_hspec_filekpath_func(cfgkey, ama, had, sm_srcsnk)
        conv_c2pt2hdf(h5f, h5kpath, latsize, hskf, cfgkey, 
                    csrcgrp_list, psnk_list, [], #tpol_list
                    c2pt_hslab_len, 
                    baryon_bc_t, attrs_kw_this)
        h5f.flush()
        h5f.close()
            
##############################################################################
# conv_bb
###############################################################################
def runme_conv_bb(cfgkey, ama, sm_srcsnk, 
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                tsnk_full=c3pt_tsnk_full, # whether to chop the bbs into coherent hslabs
                attrs_kw={}) :
    
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

    for (tsep, psnk, tpol, flav_cur) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list) :
        h5file  = get_bb_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur)
        h5kpath = "/cfg%s/bb/%s/%s_%s/%s_dt%d/%s" % (
                cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur)
        print("# >>> %s [ %s ]" % (h5file, h5kpath))
        attrs_kw_this = dict(attrs_kw)
        attrs_kw_this.update(
                    source_hadron=had, sink_hadron=had,
                    tpol=tpol,
                    time_neg=False, sink_mom=psnk)

        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')
        bbkf    = make_bb_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, flav_cur)
        conv_bb2hdf(h5f, h5kpath, latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                    qext_list, lpath_list, baryon_bc_t, attrs_kw_this, tsnk_full=tsnk_full)
        h5f.flush()
        h5f.close()


###############################################################################
# calc_op, bin_unbias_op
###############################################################################
def runme_calc_save_op(cfgkey, ama, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                attrs_kw={}) :
    
    for (tsep, psnk, tpol, flav_cur, op) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :
        ir_list = lhpd.latcorr.op_default_ir_list(op)
        
        h5bb_file   = get_bb_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur)
        h5bb_kpath  = "/cfg%s/bb/%s/%s_%s/%s_dt%d/%s" % (
                cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur)
        h5f_bb  = h5py.File(h5bb_file, 'r')

        h5op_file   = get_op_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op)
        lhpd.mkpath(os.path.dirname(h5op_file))
        h5f_op  = h5py.File(h5op_file, 'a')
        for ir_name, ir_scale in ir_list :
            h5op_kpath  = "/cfg%s/op/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                    cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, op, ir_name)
            print("# >>> %s [ %s ]" % (h5op_file, h5op_kpath))
            calc_save_op(h5f_op, h5op_kpath, latsize, h5f_bb[h5bb_kpath], 
                        psnk, tsep, op, ir_name)

        h5f_op.flush()
        h5f_op.close()

def runme_bin_unbias_op(cfgkey_list, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                ama_list=ama_list,
                VERBOSE=False,
                attrs_kw={}) :

    for (tsep, psnk, tpol, flav_cur, op) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

        ir_list = lhpd.latcorr.op_default_ir_list(op)
        for ir_name, ir_scale in ir_list :
            print("# %s %s dt=%d %s %s %s %s" % (
                    had, str(psnk), tsep, tpol, flav_cur, op, ir_name))

            h5_kpath  = "/op/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                    sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, op, ir_name)

            # bin ex
            if ama_ex in ama_list : 
                h5_list_ex  = [ (get_op_h5_file(c, ama_ex, psnk, tsep, 
                                                sm_srcsnk, had, tpol, flav_cur, op), 
                                '/cfg%s/%s' % (c, h5_kpath)) 
                                for c in cfgkey_list ]
                h5_file_ex  = get_op_bin_h5_file(ama_ex, psnk, tsep, 
                                                 sm_srcsnk, had, tpol, flav_cur, op)
                lhpd.mkpath(os.path.dirname(h5_file_ex))
                lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

            # bin sl
            if ama_sl in ama_list :
                h5_list_sl  = [ (get_op_h5_file(c, ama_sl, psnk, tsep, 
                                                sm_srcsnk, had, tpol, flav_cur, op), 
                                '/cfg%s/%s' % (c, h5_kpath)) 
                                for c in cfgkey_list ]
                h5_file_sl  = get_op_bin_h5_file(ama_sl, psnk, tsep, 
                                                 sm_srcsnk, had, tpol, flav_cur, op)
                lhpd.mkpath(os.path.dirname(h5_file_sl))
                lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
            
            # unbias
            if ama_ex in ama_list and ama_sl in ama_list : 
                h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
                h5_file_ub  = get_op_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op)
                lhpd.mkpath(os.path.dirname(h5_file_ub))
                lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

def runme_merge_op(cfgkey_list, ama, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                VERBOSE=False,
                attrs_kw={}) :

    for (tsep, psnk, tpol, flav_cur, op) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

        ir_list = lhpd.latcorr.op_default_ir_list(op)
        for ir_name, ir_scale in ir_list :
            print("# %s %s dt=%d %s %s %s %s" % (
                    had, str(psnk), tsep, tpol, flav_cur, op, ir_name))

            h5_kpath    = "/op/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                    sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, op, ir_name)

            h5_list     = [ (get_op_h5_file(c, ama, psnk, tsep, 
                                            sm_srcsnk, had, tpol, flav_cur, op), 
                            '/cfg%s/%s' % (c, h5_kpath)) 
                            for c in cfgkey_list ]
            h5_file     = get_op_all_h5_file(ama, psnk, tsep, 
                                             sm_srcsnk, had, tpol, flav_cur, op)
            lhpd.mkpath(os.path.dirname(h5_file))
            lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)



##############################################################################
# conv_bb_volcedm
###############################################################################
def runme_conv_bb_volcedm(cfgkey, ama, sm_srcsnk, 
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                tpol_list=c3pt_tpol5_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                gf_cedm_list=gf_cedm_list,
                flav_cedm_list=flav_cedm_list,
                lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                tsnk_full=c3pt_tsnk_full, # whether to chop the bbs into coherent hslabs
                attrs_kw={}) :
    
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

    for (tsep, psnk, tpol, flav_cur, gf_cedm, flav_cedm) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, gf_cedm_list, flav_cedm_list) :
        h5file  = get_bb_volcedm_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, flav_cedm, gf_cedm)
        h5kpath = "/cfg%s/bb_volcedm/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, flav_cedm, gf_cedm)
        print("# >>> %s [ %s ]" % (h5file, h5kpath))
        attrs_kw_this = dict(attrs_kw)
        attrs_kw_this.update(
                    source_hadron=had, sink_hadron=had,
                    tpol=tpol,
                    time_neg=False, sink_mom=psnk)

        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')
        bbkf    = make_bb_volcedm_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, 
                    flav_cur, flav_cedm, gf_cedm)
        conv_bb2hdf(h5f, h5kpath, latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                    qext_list, lpath_list, baryon_bc_t, attrs_kw_this, tsnk_full=tsnk_full)
        h5f.flush()
        h5f.close()



###############################################################################
# calc_op_volcedm
###############################################################################
def runme_calc_save_op_volcedm(cfgkey, ama, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                gf_cedm_list=gf_cedm_list,
                flav_cedm_list=flav_cedm_list,
                attrs_kw={}) :
    
    for (tsep, psnk, tpol, flav_cur, op, gf_cedm, flav_cedm) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list, gf_cedm_list, flav_cedm_list) :
        ir_list = lhpd.latcorr.op_default_ir_list(op)
        
        h5bb_file   = get_bb_volcedm_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, 
                            flav_cur, flav_cedm, gf_cedm)
        h5bb_kpath  = "/cfg%s/bb_volcedm/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, flav_cedm, gf_cedm)
        h5f_bb  = h5py.File(h5bb_file, 'r')

        h5op_file   = get_op_volcedm_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, 
                            flav_cur, op, flav_cedm, gf_cedm)
        lhpd.mkpath(os.path.dirname(h5op_file))
        h5f_op  = h5py.File(h5op_file, 'a')
        for ir_name, ir_scale in ir_list :
            h5op_kpath  = "/cfg%s/op_volcedm/%s/%s_%s/%s_dt%d/%s/%s/%s/%s/%s" % (
                    cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                    flav_cur, op, ir_name, flav_cedm, gf_cedm)
            print("# >>> %s [ %s ]" % (h5op_file, h5op_kpath))
            calc_save_op(h5f_op, h5op_kpath, latsize, h5f_bb[h5bb_kpath], 
                        psnk, tsep, op, ir_name)

        h5f_op.flush()
        h5f_op.close()

def runme_bin_unbias_op_volcedm(cfgkey_list, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                gf_cedm_list=gf_cedm_list,
                flav_cedm_list=flav_cedm_list,
                ama_list=ama_list,
                VERBOSE=False,
                attrs_kw={}) :

    for (tsep, psnk, tpol, flav_cur, op, gf_cedm, flav_cedm) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list, gf_cedm_list, flav_cedm_list) :

        ir_list = lhpd.latcorr.op_default_ir_list(op)
        for ir_name, ir_scale in ir_list :
            print("# %s %s dt=%d %s %s %s %s %s %s" % (
                    had, str(psnk), tsep, tpol, flav_cur, op, ir_name, flav_cedm, gf_cedm))

            h5_kpath  = "/op_volcedm/%s/%s_%s/%s_dt%d/%s/%s/%s/%s/%s" % (
                    sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                    flav_cur, op, ir_name, flav_cedm, gf_cedm)

            # bin ex
            if ama_ex in ama_list : 
                h5_list_ex  = [ (get_op_volcedm_h5_file(c, ama_ex, psnk, tsep, sm_srcsnk, 
                                            had, tpol, flav_cur, op, flav_cedm, gf_cedm), 
                                '/cfg%s/%s' % (c, h5_kpath)) 
                                for c in cfgkey_list ]
                h5_file_ex  = get_op_volcedm_bin_h5_file(ama_ex, psnk, tsep, sm_srcsnk, 
                                                 had, tpol, flav_cur, op, flav_cedm, gf_cedm)
                lhpd.mkpath(os.path.dirname(h5_file_ex))
                lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

            # bin sl
            if ama_sl in ama_list :
                h5_list_sl  = [ (get_op_volcedm_h5_file(c, ama_sl, psnk, tsep, sm_srcsnk, 
                                                had, tpol, flav_cur, op, flav_cedm, gf_cedm), 
                                '/cfg%s/%s' % (c, h5_kpath)) 
                                for c in cfgkey_list ]
                h5_file_sl  = get_op_volcedm_bin_h5_file(ama_sl, psnk, tsep, sm_srcsnk, 
                                                 had, tpol, flav_cur, op, flav_cedm, gf_cedm)
                lhpd.mkpath(os.path.dirname(h5_file_sl))
                lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
            
            # unbias
            if ama_ex in ama_list and ama_sl in ama_list : 
                h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
                h5_file_ub  = get_op_volcedm_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, 
                                    flav_cur, op, flav_cedm, gf_cedm)
                lhpd.mkpath(os.path.dirname(h5_file_ub))
                lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

def runme_merge_op_volcedm(cfgkey_list, ama, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                gf_cedm_list=gf_cedm_list,
                flav_cedm_list=flav_cedm_list,
                VERBOSE=False,
                attrs_kw={}) :

    for (tsep, psnk, tpol, flav_cur, op, gf_cedm, flav_cedm) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list, gf_cedm_list, flav_cedm_list) :

        ir_list = lhpd.latcorr.op_default_ir_list(op)
        for ir_name, ir_scale in ir_list :
            print("# %s %s dt=%d %s %s %s %s %s %s" % (
                    had, str(psnk), tsep, tpol, flav_cur, op, ir_name, flav_cedm, gf_cedm))

            h5_kpath    = "/op_volcedm/%s/%s_%s/%s_dt%d/%s/%s/%s/%s/%s" % (
                                sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                                flav_cur, op, ir_name, flav_cedm, gf_cedm)

            h5_list     = [ (get_op_volcedm_h5_file(c, ama, psnk, tsep, sm_srcsnk, 
                                            had, tpol, flav_cur, op, flav_cedm, gf_cedm), 
                            '/cfg%s/%s' % (c, h5_kpath)) 
                            for c in cfgkey_list ]
            h5_file     = get_op_volcedm_all_h5_file(ama, psnk, tsep, sm_srcsnk, 
                                             had, tpol, flav_cur, op, flav_cedm, gf_cedm)
            lhpd.mkpath(os.path.dirname(h5_file))
            lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)


def runme_qtopo_r2cut(cfgkey) :
    for qtopo_tag in qtopo_tag_list :
        h5file_in   = get_qtopo_orig_h5_file(cfgkey, qtopo_tag)
        h5f_in      = h5py.File(h5file_in, 'r')
        h5file_out  = get_qtopo_proc_h5_file(cfgkey, qtopo_tag)
        lhpd.mkpath(os.path.dirname(h5file_out))
        h5f_out     = h5py.File(h5file_out, 'a')
        h5k         = "/cfg%s/%s/qtopo_imp5li" % (cfgkey, qtopo_tag)
        qtopo       = h5f_in[h5k].value

        r2cut_axes  = lhpd.list_del(range(len(latsize)), [t_axis])
        print("# %s -> %s[%s] convo along %s" % (h5file_in, h5file_out, h5k, str(r2cut_axes)))

        qtopo_xsum  = qtopo.sum(axis=tuple(r2cut_axes))
        h5k_xsum    = "%s/xsum" % (h5k,)
        try : del h5f_out[h5k]
        except : pass
        h5f_out[h5k_xsum]   = qtopo_xsum
        h5f_out[h5k_xsum].attrs['t_axis']   = t_axis
        h5f_out["%s/sum" % (h5k,)]  = qtopo_xsum.sum()
        print("%s[%s] Q=%f" % (h5file_out, h5k, qtopo_xsum.sum()))

        h5_save_sum_r2cut(h5f_out, h5k, qtopo, qtopo_r2cut_list, axes=r2cut_axes)

        h5f_out.close()

def runme_bin_unbias_op_vacins(cfgkey_list, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                #vacins_spec_list=vacins_spec_list,
                vacins_spec_list=[],
                ama_list=ama_list,
                VERBOSE=False,
                attrs_kw={}) :
    for vispec in vacins_spec_list :
        vacins = vispec['vacins']
        if   'theta' == vacins :
            for (tsep, psnk, tpol, flav_cur, op) in it.product(
                    tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :
                ir_list = lhpd.latcorr.op_default_ir_list(op)
                for ir_name, ir_scale in ir_list :
                    print("# %s %s dt=%d %s %s %s %s %s" % (
                            had, str(psnk), tsep, tpol, flav_cur, op, ir_name, str(vacins)))
                    h5_kpath  = "/op_volcedm/%s/%s_%s/%s_dt%d/%s/%s/%s/%s/%s" % (
                            sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                            flav_cur, op, ir_name, flav_cedm, gf_cedm)

        elif 'volpsc'   == vacins :
            for (tsep, psnk, tpol, flav_cur, op) in it.product(
                    tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

                ir_list = lhpd.latcorr.op_default_ir_list(op)
                for ir_name, ir_scale in ir_list :
                    print("# %s %s dt=%d %s %s %s %s %s" % (
                            had, str(psnk), tsep, tpol, flav_cur, op, ir_name, str(vispec)))

                    h5_kpath  = "/op_%s/%s/%s_%s/%s_dt%d/%s/%s/%s/%s" % (
                            vacins, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                            flav_cur, op, ir_name, vacins_meta_str_k(vispec))

                    # bin ex
                    if ama_ex in ama_list : 
                        h5_list_ex  = [ (get_op_vacins_h5_file(c, ama_ex, psnk, tsep, sm_srcsnk, 
                                                    had, tpol, flav_cur, op, vispec), 
                                        '/cfg%s/%s' % (c, h5_kpath)) 
                                        for c in cfgkey_list ]
                        h5_file_ex  = get_op_vacins_bin_h5_file(ama_ex, psnk, tsep, sm_srcsnk, 
                                                         had, tpol, flav_cur, op, vispec)
                        lhpd.mkpath(os.path.dirname(h5_file_ex))
                        lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                                VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

                    # bin sl
                    if ama_sl in ama_list :
                        h5_list_sl  = [ (get_op_vacins_h5_file(c, ama_sl, psnk, tsep, sm_srcsnk, 
                                                        had, tpol, flav_cur, op, vispec), 
                                        '/cfg%s/%s' % (c, h5_kpath)) 
                                        for c in cfgkey_list ]
                        h5_file_sl  = get_op_vacins_bin_h5_file(ama_sl, psnk, tsep, sm_srcsnk, 
                                                         had, tpol, flav_cur, op, vispec)
                        lhpd.mkpath(os.path.dirname(h5_file_sl))
                        lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                                VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
                    
                    # unbias
                    if ama_ex in ama_list and ama_sl in ama_list : 
                        h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
                        h5_file_ub  = get_op_vacins_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, 
                                            flav_cur, op, vispec)
                        lhpd.mkpath(os.path.dirname(h5_file_ub))
                        lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                                VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
        elif 'volcedm'  == vacins : raise ValueError("use different function")
        else : raise ValueError(vacins)

##############################################################################
##############################################################################
if '__main__' == __name__ :
    what   = sys.argv[1]
    jobid_list      = sys.argv[3:]

    if   'conv_hspec' == what :
        print("FIXME : (1) remove tpol (2) separate conv for 2pt meson, 2pt baryon (different bc)")
        sys.exit(1)
        for c in jobid_list :
            for ama in ama_list :
                for sm_srcsnk in ['SS', 'SP'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_conv_hspec(c, ama, sm_srcsnk)


    elif 'conv_c2pt' == what :
        for c in jobid_list :
            for ama in ama_list :
                for sm_srcsnk in ['SS', 'SP'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_conv_c2pt(c, ama, sm_srcsnk)
    elif 'conv_c2pt_volcedm' == what :
        for c in jobid_list :
            for ama in ama_list :
                for sm_srcsnk in ['SS', 'SP'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_conv_c2pt_volcedm(c, ama, sm_srcsnk)
    elif 'conv_bb' == what :
        for c in jobid_list : 
            for ama in ama_list :
                for sm_srcsnk in ['SS'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_conv_bb(c, ama, sm_srcsnk)
    elif 'conv_bb_volcedm' == what :
        for c in jobid_list : 
            for ama in ama_list :
                for sm_srcsnk in ['SS'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_conv_bb_volcedm(c, ama, sm_srcsnk)
    
    
    elif 'calc_op' == what :
        for c in jobid_list : 
            for ama in ama_list :
                for sm_srcsnk in ['SS'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_calc_save_op(c, ama, sm_srcsnk)
    elif 'calc_op_volcedm' == what :
        for c in jobid_list : 
            for ama in ama_list :
                for sm_srcsnk in ['SS'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_calc_save_op_volcedm(c, ama, sm_srcsnk)
    
    
    elif 'bin_unbias_c2pt' == what :
        for sm_srcsnk in ['SS', 'SP'] :
            print("# nconf=%d %s " % (len(cfg_list), sm_srcsnk))
            runme_bin_unbias_c2pt(cfg_list, sm_srcsnk, ama_list=ama_list)
    elif 'bin_unbias_c2pt_volcedm' == what :
        for sm_srcsnk in ['SS', 'SP'] :
            print("# nconf=%d %s " % (len(cfg_list), sm_srcsnk))
            runme_bin_unbias_c2pt_volcedm(cfg_list, sm_srcsnk, ama_list=ama_list)
    elif 'bin_unbias_op' == what :
        for sm_srcsnk in ['SS'] :
            print("# nconf=%d %s " % (len(cfg_list), sm_srcsnk))
            runme_bin_unbias_op(cfg_list, sm_srcsnk, ama_list=ama_list)
    elif 'bin_unbias_op_volcedm' == what :
        for sm_srcsnk in ['SS'] :
            print("# nconf=%d %s " % (len(cfg_list), sm_srcsnk))
            runme_bin_unbias_op_volcedm(cfg_list, sm_srcsnk, ama_list=ama_list)


    elif 'merge_c2pt' == what :
        for ama in ama_list :
            for sm_srcsnk in ['SS', 'SP'] :
                print("# nconf=%d %s %s" % (len(cfg_list), ama, sm_srcsnk))
                runme_merge_c2pt(cfg_list, ama, sm_srcsnk)
    elif 'merge_op' == what :
        for ama in ama_list :
            for sm_srcsnk in ['SS'] :
                print("# nconf=%d %s %s" % (len(cfg_list), ama, sm_srcsnk))
                runme_merge_op(cfg_list, ama, sm_srcsnk)
    elif 'merge_op_volcedm' == what :
        for ama in ama_list :
            for sm_srcsnk in ['SS'] :
                print("# nconf=%d %s %s" % (len(cfg_list), ama, sm_srcsnk))
                runme_merge_op_volcedm(cfg_list, ama, sm_srcsnk)

    elif 'qtopo_r2cut' == what :
        for c in jobid_list : 
            runme_qtopo_r2cut(c)

    else : raise ValueError(what)

