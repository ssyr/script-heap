# XXX examples
#c2pt/c2pt.1000.sl.x20y28z44t76.aff [/c2pt/SS/x20y28z44t76_x44y4z20t4_x20y28z44t28_x44y4z20t52/proton_Tg0/PX3_PY1_PZ0]
#c2pt/c2pt.1010.sl.proton.SS.h5 [/cfg1010/c2pt/SS/proton]
#c2pt.bin/c2pt-bin.sl.proton.SP.h5 [/c2pt/SS/proton]

#bb/bb.1000.sl.x20y16z20t76.PX0PY0PZ0dt11.posSzplus.U.aff [/bb/SS/proton_posSzplus/U/x20y16z20t76_x44y40z44t4_x20y16z20t28_x44y40z44t52/PX0_PY0_PZ0_dt11/l1_T/g15/qx2_qy0_qz0]
#bb/bb.1000.sl.PX0PY0PZ0dt12.proton_posSzplus.SS.U.h5 [/cfg1000/bb/SS/proton_posSzplus/PX0_PY0_PZ0_dt12/U]
#op/op.1000.sl.PX0PY0PZ0dt10.proton_posSzplus.SS.U.pstensor1.h5 [/cfg1000/op/SS/proton_posSzplus/PX0_PY0_PZ0_dt10/U/pstensor1/H4_T1_d4r1]
#op.bin/op-bin.sl.PX0PY0PZ0dt10.proton_posSzplus.SS.U.tensor1.h5 [/op/SS/proton_posSzplus/PX0_PY0_PZ0_dt10/U/tensor1/H4_T1_d4r1

#c2pt_volcedm/c2pt_volcedm.1000.sl.x32y16z20t76.orig.aff [/c2pt_volcedm/SS/x32y16z20t76_x8y40z44t4_x32y16z20t28_x8y40z44t52/orig/proton_Tg1/D/PX2_PY0_PZ-2]
#c2pt_volcedm/c2pt_volcedm.1010.sl.proton.SS.D.orig.h5 [/cfg1010/c2pt_volcedm/SS/proton/D/orig]
#c2pt_volcedm.bin/c2pt_volcedm-bin.sl.proton.SS.D.orig.h5 [/c2pt_volcedm/SS/proton/D/orig]

#c2pt_volpsc/c2pt_volpsc.1000.sl.x20y4z20t76.aff [/c2pt_volpsc/SS/x20y4z20t76_x44y28z44t4_x20y4z20t28_x44y28z44t52/orig/proton_Tg1/D/PX2_PY0_PZ0]
#c2pt_volpsc/c2pt_volpsc.1010.sl.proton.SS.D.h5 [/cfg1010/c2pt_volpsc/SS/proton/D]
#c2pt_volpsc.bin/c2pt_volpsc-bin.sl.proton.SS.U.h5 [/c2pt_volpsc/SS/proton/U]


#c2pt_theta.bin/c2pt_theta-bin.sl.proton.SP.wf0.05x400.rcut24.dtcut4.h5 [/c2pt_theta/SP/proton/wf0.05x400/rcut24/dtcut4]


#bb_volcedm/bb_volcedm.1000.sl.x20y16z20t76.PX0PY0PZ0dt10.posSzplus.U.U.orig.aff [/bb_volcedm/SS/orig/proton_posSzplus/U/U/x20y16z20t76_x44y40z44t4_x20y16z20t28_x44y40z44t52/PX0_PY0_PZ0_dt10/l1_T/g15/qx2_qy0_qz1]
#bb_volcedm/bb_volcedm.1000.sl.PX0PY0PZ0dt8.proton_posSzplus.SS.D.U.orig.h5 [/cfg1000/bb_volcedm/SS/proton_posSzplus/PX0_PY0_PZ0_dt8/D/U/orig]
#op_volcedm/op_volcedm.1000.sl.PX0PY0PZ0dt10.proton_posSzplus.SS.D.tensor1.U.orig.h5 [/cfg1000/op_volcedm/SS/proton_posSzplus/PX0_PY0_PZ0_dt10/D/tensor1/H4_T1_d4r1/U/orig]
#op_volcedm.bin/op_volcedm-bin.sl.PX0PY0PZ0dt10.proton_posSzplus.SS.U.tensor1.D.orig.h5 [/op_volcedm/SS/proton_posSzplus/PX0_PY0_PZ0_dt10/U/tensor1/H4_T1_d4r1/D/orig]

#bb_volpsc/bb_volpsc.1000.sl.x20y16z20t76.PX0PY0PZ0dt10.posSzplus.U.U.aff [/bb_volpsc/SS/orig/proton_posSzplus/U/U/x20y16z20t76_x44y40z44t4_x20y16z20t28_x44y40z44t52/PX0_PY0_PZ0_dt10/l1_T/g15/qx2_qy1_qz-2]
#bb_volpsc/bb_volpsc.1000.sl.PX0PY0PZ0dt8.proton_posSzplus.SS.U.D.h5 [/cfg1000/bb_volpsc/SS/proton_posSzplus/PX0_PY0_PZ0_dt8/U/D]
#op_volpsc/op_volpsc.1000.sl.PX0PY0PZ0dt10.proton_posSzplus.SS.U.pstensor1.D.h5 [/cfg1000/op_volpsc/SS/proton_posSzplus/PX0_PY0_PZ0_dt10/U/pstensor1/H4_T1_d4r1/D]
#op_volpsc.bin/op_volpsc-bin.sl.PX0PY0PZ0dt10.proton_posSzplus.SS.U.tensor1.D.h5 [/op_volpsc/SS/proton_posSzplus/PX0_PY0_PZ0_dt10/U/tensor1/H4_T1_d4r1/D]
