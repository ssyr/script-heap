#!/usr/bin/env python
from __future__ import print_function
from past.builtins import execfile

import os, sys
import re
import lhpd

if '__main__' == __name__ :
    def print_usage(prg) :
        print("""Usage:
%s  --sj=i{=0..TOT-1}/TOT  <what>   <config>  [<job-list>] ++ [<job_id0>] ...
What :
    #conv_hspec          # FIXME : change tpol_list->hspec_list or remove it alltogether from dim_spec
    #conv_c2pt_cpbar

    conv_c2pt
    bin_unbias_c2pt

    conv_c2pt_<vacins>
    bin_unbias_c2pt_<vacins>

    conv_bb
    calc_op
    bin_unbias_op

    conv_bb_<vacins>
    calc_op_<vacins>
    bin_unbias_op_<vacins>

    merge_c2pt
    merge_op
    #merge_op_volcedm

    qtopo_rcut
""" % (prg,))

    argv = list(sys.argv)    
    if len(argv) < 1 :
        print_usage('me')
        sys.exit(1)

    sj_this, sj_tot = 0, 1

    while 1 < len(argv) and argv[1].startswith('--') :
        if argv[1].startswith('--sj=') :
            sj = re.match('^--sj=(\d+)/(\d+)$', argv[1])
            if not sj : raise ValueError(argv[1])
            sj_this, sj_tot = [ int(x) for x in sj.groups() ]
            print("sj = ", sj_this, sj_tot)
        else : 
            sys.stderr.write("bad arg='%s'\n" % argv[1])
            print_usage(argv[0] or 'me')
            sys.exit(1)
        argv.pop(1)

    if len(argv) < 3 : 
        print_usage(argv[0] or 'me')
        sys.exit(1)

    try : isep = argv.index('++')
    except : isep = len(argv)
    #print("len(argv)=%d  isep=%d  argv=%s" % (len(argv), isep, str(argv)))
    #exit(1)

    if isep < 3 : 
        print_usage(argv[0] or 'me')
        sys.exit(1)

    cfglist_cmd_ = argv[isep+1:]
    cfglist_file_ = argv[3:isep]
    #print("joblist=file(%s) + %s" % (str(cfglist_file_), str(cfglist_cmd_)))

    cfg_list = list(cfglist_cmd_)
    for f in cfglist_file_ : 
        cfg_list.extend(lhpd.read_key_list(f, 'samples'))
    print("Total samples: %d ('%s' ... '%s')" % (
        len(cfg_list), cfg_list[0], cfg_list[-1]))

    if len(cfg_list) <= 0 :
        sys.stderr.write('empty list; exit\n')
        sys.exit(1)

    # TODO check for repeated keys

    conf_py   = argv[2]
    execfile(conf_py)
    execfile('pproc_nedm.py')


###############################################################################
# c2pt
##############################################################################
def runme_conv_c2pt(cfgkey, ama, sm_srcsnk,
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                hslab_len=c2pt_hslab_len,
                psnk_list=c2pt_psnk_list,
                tpol_list=c2pt_tpol_list,
                attrs_kw={}) :
    """ convert CP-even 2pt functions 
    """
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)

    h5file  = get_c2pt_h5_file(cfgkey, ama, had, sm_srcsnk)
    h5kpath = "/cfg%s/c2pt/%s/%s" % (cfgkey, sm_srcsnk, had)
    print("# >>> %s [ %s ]" % (h5file, h5kpath))
    attrs_kw_this = dict(attrs_kw)
    attrs_kw_this.update(source_hadron=had, sink_hadron=had)

    lhpd.mkpath(os.path.dirname(h5file))
    h5f     = h5py.File(h5file, 'a')
    hskf    = make_c2pt_filekpath_func(cfgkey, ama, had, sm_srcsnk)
    conv_c2pt2hdf(h5f, h5kpath, latsize, hskf, cfgkey, 
                csrcgrp_list, psnk_list, tpol_list, c2pt_hslab_len, 
                baryon_bc_t, attrs_kw_this)
    h5f.flush()
    h5f.close()

def runme_nanscanAff_c2pt(cfgkey, ama, sm_srcsnk,
                had='proton',
                psnk_list=c2pt_psnk_list,
                tpol_list=c2pt_tpol_list ) :
    """ scan Aff files for NaNs """
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)

    hskf    = make_c2pt_filekpath_func(cfgkey, ama, had, sm_srcsnk)
    nanscanAff_c2pt(latsize, hskf, cfgkey, csrcgrp_list, psnk_list, tpol_list)

def runme_bin_unbias_c2pt(cfgkey_list, sm_srcsnk,
                had='proton',
                ama_list=ama_list,
                VERBOSE=False) :
    h5_kpath  = "/c2pt/%s/%s" % (sm_srcsnk, had)
    
    # bin ex
    if ama_ex in ama_list : 
        h5_list_ex  = [ (get_c2pt_h5_file(c, ama_ex, had, sm_srcsnk),
                        '/cfg%s/%s' % (c, h5_kpath)) 
                        for c in cfgkey_list ]
        h5_file_ex  = get_c2pt_bin_h5_file(ama_ex, had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_ex))
        lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
                
    # bin sl
    if ama_sl in ama_list :
        h5_list_sl  = [ (get_c2pt_h5_file(c, ama_sl, had, sm_srcsnk),
                        '/cfg%s/%s' % (c, h5_kpath))
                        for c in cfgkey_list ]
        h5_file_sl  = get_c2pt_bin_h5_file(ama_sl, had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_sl))
        lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

    # unbias
    if ama_ex in ama_list and ama_sl in ama_list : 
        h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
        h5_file_ub  = get_c2pt_unbias_h5_file(had, sm_srcsnk)
        lhpd.mkpath(os.path.dirname(h5_file_ub))
        lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

def runme_merge_c2pt(cfgkey_list, ama, sm_srcsnk,
                had='proton',
                VERBOSE=False) :
    h5_kpath  = "/c2pt/%s/%s" % (sm_srcsnk, had)
    
    h5_list  = [ (get_c2pt_h5_file(c, ama, had, sm_srcsnk),
                    '/cfg%s/%s' % (c, h5_kpath)) 
                    for c in cfgkey_list ]
    h5_file  = get_c2pt_all_h5_file(ama, had, sm_srcsnk)
    lhpd.mkpath(os.path.dirname(h5_file))
    lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)


##############################################################################
# c2pt_vacins
###############################################################################
def runme_conv_c2pt_vacins(cfgkey, ama, sm_srcsnk,
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                hslab_len=c2pt_hslab_len,
                psnk_list=c2pt_psnk_list,
                tpol_list=c2pt_tpol5_list,
                vacins_spec_list=[],
                attrs_kw={}) :
    """ convert CP-even 2pt functions 
    """
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        h5file  = get_c2pt_vacins_h5_file(cfgkey, ama, had, sm_srcsnk, vispec)
        h5kpath = "/cfg%s/c2pt_%s/%s/%s/%s" % (cfgkey, vacins, sm_srcsnk, had, vacins_meta_str_k(vispec))
        print("# >>> %s [ %s ]" % (h5file, h5kpath))
        attrs_kw_this = dict(attrs_kw)
        attrs_kw_this.update(source_hadron=had, sink_hadron=had)

        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')
        hskf    = make_c2pt_vacins_filekpath_func(cfgkey, ama, had, sm_srcsnk, vispec)
        conv_c2pt2hdf(h5f, h5kpath, latsize, hskf, cfgkey, 
                    csrcgrp_list, psnk_list, tpol_list, c2pt_hslab_len, 
                    baryon_bc_t, attrs_kw_this)
        h5f.flush()
        h5f.close()
def runme_nanscanAff_c2pt_vacins(cfgkey, ama, sm_srcsnk,
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                hslab_len=c2pt_hslab_len,
                psnk_list=c2pt_psnk_list,
                tpol_list=c2pt_tpol5_list,
                vacins_spec_list=[],
                attrs_kw={}) :
    """ convert CP-even 2pt functions 
    """
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        hskf    = make_c2pt_vacins_filekpath_func(cfgkey, ama, had, sm_srcsnk, vispec)
        nanscanAff_c2pt(latsize, hskf, cfgkey, csrcgrp_list, psnk_list, tpol_list)

def runme_bin_unbias_c2pt_vacins(cfgkey_list, sm_srcsnk,
                had='proton',
                vacins_spec_list=[],
                ama_list=ama_list,
                VERBOSE=False) :
    # since Tpol is TgN, keep them all together
    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        h5_kpath  = "/c2pt_%s/%s/%s/%s" % (vacins, sm_srcsnk, had, vacins_meta_str_k(vispec))
            
        # bin ex
        if ama_ex in ama_list : 
            h5_list_ex  = [ (get_c2pt_vacins_h5_file(c, ama_ex, had, sm_srcsnk, vispec),
                            '/cfg%s/%s' % (c, h5_kpath)) 
                            for c in cfgkey_list ]
            h5_file_ex  = get_c2pt_vacins_bin_h5_file(ama_ex, had, sm_srcsnk, vispec)
            lhpd.mkpath(os.path.dirname(h5_file_ex))
            lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
                        
        # bin sl
        if ama_sl in ama_list :
            h5_list_sl  = [ (get_c2pt_vacins_h5_file(c, ama_sl, had, sm_srcsnk, vispec),
                            '/cfg%s/%s' % (c, h5_kpath))
                            for c in cfgkey_list ]
            h5_file_sl  = get_c2pt_vacins_bin_h5_file(ama_sl, had, sm_srcsnk, vispec)
            lhpd.mkpath(os.path.dirname(h5_file_sl))
            lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
                
        # unbias
        if ama_ex in ama_list and ama_sl in ama_list : 
            h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
            h5_file_ub  = get_c2pt_vacins_unbias_h5_file(had, sm_srcsnk, vispec)
            lhpd.mkpath(os.path.dirname(h5_file_ub))
            lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

def runme_merge_c2pt_vacins(cfgkey_list, ama, sm_srcsnk,
                had='proton',
                vacins_spec_list=[],
                VERBOSE=False) :
    # since Tpol is TgN, keep them all together
    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        h5_kpath  = "/c2pt_%s/%s/%s/%s" % (vacins, sm_srcsnk, had, vacins_meta_str_k(vispec))
            
        h5_list  = [ (get_c2pt_vacins_h5_file(c, ama, had, sm_srcsnk, vispec),
                        '/cfg%s/%s' % (c, h5_kpath)) 
                        for c in cfgkey_list ]
        h5_file  = get_c2pt_vacins_all_h5_file(ama, had, sm_srcsnk, vispec)

        lhpd.mkpath(os.path.dirname(h5_file))
        lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)

###############################################################################
# hspec TODO
###############################################################################
def runme_conv_hspec(cfgkey, ama, sm_srcsnk,
                baryon_bc_t=ferm_bc[3]**3,
                hslab_len=c2pt_hslab_len,
                psnk_list=c2pt_psnk_list,
                baryon_list=hspec_baryon_list,
                meson_list=hspec_meson_list,
                attrs_kw={}) :
    """ convert CP-even 2pt functions 
    """
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    for had in list(baryon_list) + list(meson_list) :
        h5file  = get_hspec_h5_file(cfgkey, ama, had, sm_srcsnk)
        h5kpath = "/cfg%s/c2pt/%s/%s" % (cfgkey, sm_srcsnk, had)
        print("# >>> %s [ %s ]" % (h5file, h5kpath))
        attrs_kw_this = dict(attrs_kw)
        attrs_kw_this.update(source_hadron=had, sink_hadron=had)

        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')
        hskf    = make_hspec_filekpath_func(cfgkey, ama, had, sm_srcsnk)
        conv_c2pt2hdf(h5f, h5kpath, latsize, hskf, cfgkey, 
                    csrcgrp_list, psnk_list, [], #tpol_list
                    c2pt_hslab_len, 
                    baryon_bc_t, attrs_kw_this)
        h5f.flush()
        h5f.close()
            
##############################################################################
# conv_bb
###############################################################################
def runme_conv_bb(cfgkey, ama, sm_srcsnk, 
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                tsnk_full=c3pt_tsnk_full, # whether to chop the bbs into coherent hslabs
                attrs_kw={}) :
    
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

    for (tsep, psnk, tpol, flav_cur) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list) :
        h5file  = get_bb_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur)
        h5kpath = "/cfg%s/bb/%s/%s_%s/%s_dt%d/%s" % (
                cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur)
        print("# >>> %s [ %s ]" % (h5file, h5kpath))
        attrs_kw_this = dict(attrs_kw)
        attrs_kw_this.update(
                    source_hadron=had, sink_hadron=had,
                    tpol=tpol,
                    time_neg=False, sink_mom=psnk)

        lhpd.mkpath(os.path.dirname(h5file))
        h5f     = h5py.File(h5file, 'a')
        bbkf    = make_bb_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, flav_cur)
        conv_bb2hdf(h5f, h5kpath, latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                    qext_list, lpath_list, baryon_bc_t, attrs_kw_this, tsnk_full=tsnk_full)
        h5f.flush()
        h5f.close()

def runme_nanscanAff_bb(cfgkey, ama, sm_srcsnk, 
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                tsnk_full=c3pt_tsnk_full, # whether to chop the bbs into coherent hslabs
                attrs_kw={}) :
    
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

    for (tsep, psnk, tpol, flav_cur) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list) :
        bbkf    = make_bb_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, flav_cur)
        nanscanAff_bb(latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                    qext_list, lpath_list)

###############################################################################
# calc_op, bin_unbias_op
###############################################################################
def runme_calc_save_op(cfgkey, ama, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                attrs_kw={}) :
    
    for (tsep, psnk, tpol, flav_cur, op) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :
        ir_list = lhpd.latcorr.op_default_ir_list(op)
        
        h5bb_file   = get_bb_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur)
        h5bb_kpath  = "/cfg%s/bb/%s/%s_%s/%s_dt%d/%s" % (
                cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur)
        h5f_bb  = h5py.File(h5bb_file, 'r')

        h5op_file   = get_op_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op)
        lhpd.mkpath(os.path.dirname(h5op_file))
        h5f_op  = h5py.File(h5op_file, 'a')
        for ir_name, ir_scale in ir_list :
            h5op_kpath  = "/cfg%s/op/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                    cfgkey, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, op, ir_name)
            print("# >>> %s [ %s ]" % (h5op_file, h5op_kpath))
            calc_save_op(h5f_op, h5op_kpath, latsize, h5f_bb[h5bb_kpath], 
                        psnk, tsep, op, ir_name)

        h5f_op.flush()
        h5f_op.close()

def runme_bin_unbias_op(cfgkey_list, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                ama_list=ama_list,
                VERBOSE=False,
                attrs_kw={}) :

    for (tsep, psnk, tpol, flav_cur, op) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

        ir_list = lhpd.latcorr.op_default_ir_list(op)
        for ir_name, ir_scale in ir_list :
            print("# %s %s dt=%d %s %s %s %s" % (
                    had, str(psnk), tsep, tpol, flav_cur, op, ir_name))

            h5_kpath  = "/op/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                    sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, op, ir_name)

            # bin ex
            if ama_ex in ama_list : 
                h5_list_ex  = [ (get_op_h5_file(c, ama_ex, psnk, tsep, 
                                                sm_srcsnk, had, tpol, flav_cur, op), 
                                '/cfg%s/%s' % (c, h5_kpath)) 
                                for c in cfgkey_list ]
                h5_file_ex  = get_op_bin_h5_file(ama_ex, psnk, tsep, 
                                                 sm_srcsnk, had, tpol, flav_cur, op)
                lhpd.mkpath(os.path.dirname(h5_file_ex))
                lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

            # bin sl
            if ama_sl in ama_list :
                h5_list_sl  = [ (get_op_h5_file(c, ama_sl, psnk, tsep, 
                                                sm_srcsnk, had, tpol, flav_cur, op), 
                                '/cfg%s/%s' % (c, h5_kpath)) 
                                for c in cfgkey_list ]
                h5_file_sl  = get_op_bin_h5_file(ama_sl, psnk, tsep, 
                                                 sm_srcsnk, had, tpol, flav_cur, op)
                lhpd.mkpath(os.path.dirname(h5_file_sl))
                lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
            
            # unbias
            if ama_ex in ama_list and ama_sl in ama_list : 
                h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
                h5_file_ub  = get_op_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, flav_cur, op)
                lhpd.mkpath(os.path.dirname(h5_file_ub))
                lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                        VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

def runme_merge_op(cfgkey_list, ama, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                VERBOSE=False,
                attrs_kw={}) :

    for (tsep, psnk, tpol, flav_cur, op) in it.product(
            tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

        ir_list = lhpd.latcorr.op_default_ir_list(op)
        for ir_name, ir_scale in ir_list :
            print("# %s %s dt=%d %s %s %s %s" % (
                    had, str(psnk), tsep, tpol, flav_cur, op, ir_name))

            h5_kpath    = "/op/%s/%s_%s/%s_dt%d/%s/%s/%s" % (
                    sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, op, ir_name)

            h5_list     = [ (get_op_h5_file(c, ama, psnk, tsep, 
                                            sm_srcsnk, had, tpol, flav_cur, op), 
                            '/cfg%s/%s' % (c, h5_kpath)) 
                            for c in cfgkey_list ]
            h5_file     = get_op_all_h5_file(ama, psnk, tsep, 
                                             sm_srcsnk, had, tpol, flav_cur, op)
            lhpd.mkpath(os.path.dirname(h5_file))
            lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)



##############################################################################
# conv_bb_vacins
###############################################################################
def runme_conv_bb_vacins(cfgkey, ama, sm_srcsnk, 
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                tpol_list=c3pt_tpol5_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                tsnk_full=c3pt_tsnk_full, # whether to chop the bbs into coherent hslabs
                vacins_spec_list=[],
                attrs_kw={}) :
    
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        for (tsep, psnk, tpol, flav_cur) in it.product(
                tsep_list, psnk_list, tpol_list, flav_cur_list) :
            h5file  = get_bb_vacins_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, flav_cur, vispec)
            h5kpath = "/cfg%s/bb_%s/%s/%s_%s/%s_dt%d/%s/%s" % (
                    cfgkey, vacins, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, 
                    vacins_meta_str_k(vispec))
            print("# >>> %s [ %s ]" % (h5file, h5kpath))
            attrs_kw_this = dict(attrs_kw)
            attrs_kw_this.update(
                        source_hadron=had, sink_hadron=had,
                        tpol=tpol,
                        time_neg=False, sink_mom=psnk)

            lhpd.mkpath(os.path.dirname(h5file))
            h5f     = h5py.File(h5file, 'a')
            bbkf    = make_bb_vacins_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, 
                        flav_cur, vispec)
            conv_bb2hdf(h5f, h5kpath, latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                        qext_list, lpath_list, baryon_bc_t, attrs_kw_this, tsnk_full=tsnk_full)
            h5f.flush()
            h5f.close()

def runme_nanscanAff_bb_vacins(cfgkey, ama, sm_srcsnk, 
                baryon_bc_t=ferm_bc[3]**3,
                had='proton',
                tpol_list=c3pt_tpol5_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                lpath_min=c3pt_lpath_min, lpath_max=c3pt_lpath_max,
                tsnk_full=c3pt_tsnk_full, # whether to chop the bbs into coherent hslabs
                vacins_spec_list=[],
                attrs_kw={}) :
    
    csrcgrp_list    = make_csrcgrp_list(ama, cfgkey)
    lpath_list      = lhpd.latcorr.make_linkpath_list(lpath_min, lpath_max)

    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        for (tsep, psnk, tpol, flav_cur) in it.product(
                tsep_list, psnk_list, tpol_list, flav_cur_list) :
            bbkf    = make_bb_vacins_filekpath_func(cfgkey, ama, psnk, sm_srcsnk, had, tpol, 
                        flav_cur, vispec)
            nanscanAff_bb(latsize, bbkf, cfgkey, csrcgrp_list, tsep, 
                        qext_list, lpath_list)


###############################################################################
# calc_op_vacins
###############################################################################
def runme_calc_save_op_vacins(cfgkey, ama, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                vacins_spec_list=[],
                attrs_kw={}) :
    
    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        for (tsep, psnk, tpol, flav_cur, op) in it.product(
                tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :
            ir_list = lhpd.latcorr.op_default_ir_list(op)
            
            h5bb_file   = get_bb_vacins_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, 
                                flav_cur, vispec)
            h5bb_kpath  = "/cfg%s/bb_%s/%s/%s_%s/%s_dt%d/%s/%s" % (
                    cfgkey, vacins, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, flav_cur, 
                    vacins_meta_str_k(vispec))
            h5f_bb  = h5py.File(h5bb_file, 'r')

            h5op_file   = get_op_vacins_h5_file(cfgkey, ama, psnk, tsep, sm_srcsnk, had, tpol, 
                                flav_cur, op, vispec)
            lhpd.mkpath(os.path.dirname(h5op_file))
            h5f_op  = h5py.File(h5op_file, 'a')
            for ir_name, ir_scale in ir_list :
                h5op_kpath  = "/cfg%s/op_%s/%s/%s_%s/%s_dt%d/%s/%s/%s/%s" % (
                        cfgkey, vacins, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                        flav_cur, op, ir_name, vacins_meta_str_k(vispec))
                print("# >>> %s [ %s ]" % (h5op_file, h5op_kpath))
                calc_save_op(h5f_op, h5op_kpath, latsize, h5f_bb[h5bb_kpath], 
                            psnk, tsep, op, ir_name)

            h5f_op.flush()
            h5f_op.close()

def runme_bin_unbias_op_vacins(cfgkey_list, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                vacins_spec_list=[],
                ama_list=ama_list,
                VERBOSE=False,
                attrs_kw={}) :

    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        for (tsep, psnk, tpol, flav_cur, op) in it.product(
                tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

            ir_list = lhpd.latcorr.op_default_ir_list(op)
            for ir_name, ir_scale in ir_list :
                print("# %s %s dt=%d %s %s %s %s %s" % (
                        had, str(psnk), tsep, tpol, flav_cur, op, ir_name, str(vispec)))

                h5_kpath  = "/op_%s/%s/%s_%s/%s_dt%d/%s/%s/%s/%s" % (
                        vacins, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                        flav_cur, op, ir_name, vacins_meta_str_k(vispec))
                if 'theta'   == vacins : raise NotImplementedError

                else :

                    # bin ex
                    if ama_ex in ama_list : 
                        h5_list_ex  = [ (get_op_vacins_h5_file(c, ama_ex, psnk, tsep, sm_srcsnk, 
                                                    had, tpol, flav_cur, op, vispec), 
                                        '/cfg%s/%s' % (c, h5_kpath)) 
                                        for c in cfgkey_list ]
                        h5_file_ex  = get_op_vacins_bin_h5_file(ama_ex, psnk, tsep, sm_srcsnk, 
                                                         had, tpol, flav_cur, op, vispec)
                        lhpd.mkpath(os.path.dirname(h5_file_ex))
                        lhpd.h5_io.h5file_bin(h5_file_ex, h5_kpath, h5_list_ex, 
                                VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

                    # bin sl
                    if ama_sl in ama_list :
                        h5_list_sl  = [ (get_op_vacins_h5_file(c, ama_sl, psnk, tsep, sm_srcsnk, 
                                                        had, tpol, flav_cur, op, vispec), 
                                        '/cfg%s/%s' % (c, h5_kpath)) 
                                        for c in cfgkey_list ]
                        h5_file_sl  = get_op_vacins_bin_h5_file(ama_sl, psnk, tsep, sm_srcsnk, 
                                                         had, tpol, flav_cur, op, vispec)
                        lhpd.mkpath(os.path.dirname(h5_file_sl))
                        lhpd.h5_io.h5file_bin(h5_file_sl, h5_kpath, h5_list_sl, 
                                VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])
                    
                    # unbias
                    if ama_ex in ama_list and ama_sl in ama_list : 
                        h5_list_ub  = [ c1 + c2 for c1,c2 in zip(h5_list_sl, h5_list_ex) ]
                        h5_file_ub  = get_op_vacins_unbias_h5_file(psnk, tsep, sm_srcsnk, had, tpol, 
                                            flav_cur, op, vispec)
                        lhpd.mkpath(os.path.dirname(h5_file_ub))
                        lhpd.h5_io.h5file_bin_unbias(h5_file_ub, h5_kpath, h5_list_ub, 
                                VERBOSE=VERBOSE, keep_datalist=lambda mb: mb[0])

def runme_merge_op_vacins(cfgkey_list, ama, sm_srcsnk,
                had='proton',
                tpol_list=c3pt_tpol_list,
                psnk_list=c3pt_psnk_list,
                tsep_list=c3pt_tsep_list,
                qext_list=c3pt_qext_list,
                flav_cur_list=flav_cur_list,
                op_list=c3pt_op_list,
                vacins_spec_list=[],
                ama_list=ama_list,
                VERBOSE=False,
                attrs_kw={}) :

    for vispec in vacins_spec_list :
        vacins  = vispec['vacins']
        for (tsep, psnk, tpol, flav_cur, op) in it.product(
                tsep_list, psnk_list, tpol_list, flav_cur_list, op_list) :

            ir_list = lhpd.latcorr.op_default_ir_list(op)
            for ir_name, ir_scale in ir_list :
                print("# %s %s dt=%d %s %s %s %s %s" % (
                        had, str(psnk), tsep, tpol, flav_cur, op, ir_name, str(vispec)))

                h5_kpath  = "/op_%s/%s/%s_%s/%s_dt%d/%s/%s/%s/%s" % (
                        vacins, sm_srcsnk, had, tpol, psnk_str_k(psnk), tsep, 
                        flav_cur, op, ir_name, vacins_meta_str_k(vispec))
                if 'theta'   == vacins : raise NotImplementedError

                else :

                    h5_list  = [ (get_op_vacins_h5_file(c, ama, psnk, tsep, sm_srcsnk, 
                                                had, tpol, flav_cur, op, vispec), 
                                    '/cfg%s/%s' % (c, h5_kpath)) 
                                    for c in cfgkey_list ]
                    h5_file  = get_op_vacins_all_h5_file(ama, psnk, tsep, sm_srcsnk, 
                                                     had, tpol, flav_cur, op, vispec)
                    lhpd.mkpath(os.path.dirname(h5_file))
                    lhpd.h5_io.h5file_merge(h5_file, h5_kpath, h5_list, VERBOSE=VERBOSE)


def runme_qtopo_rcut(cfgkey) :
    for qtopo_tag in qtopo_tag_list :
        h5file_in   = get_qtopo_orig_h5_file(cfgkey, qtopo_tag)
        h5f_in      = h5py.File(h5file_in, 'r')
        h5file_out  = get_qtopo_proc_h5_file(cfgkey, qtopo_tag)
        lhpd.mkpath(os.path.dirname(h5file_out))
        h5f_out     = h5py.File(h5file_out, 'a')
        h5k         = "/cfg%s/%s/qtopo_imp5li" % (cfgkey, qtopo_tag)
        qtopo       = h5f_in[h5k].value

        rcut_axes  = lhpd.list_del(range(len(latsize)), [t_axis])
        print("# %s -> %s[%s] convo along %s" % (h5file_in, h5file_out, h5k, str(rcut_axes)))

        qtopo_xsum  = qtopo.sum(axis=tuple(rcut_axes))
        h5k_xsum    = "%s/xsum" % (h5k,)
        try : del h5f_out[h5k]
        except : pass
        h5f_out[h5k_xsum]   = qtopo_xsum
        h5f_out[h5k_xsum].attrs['t_axis']   = t_axis
        h5f_out["%s/sum" % (h5k,)]  = qtopo_xsum.sum()
        print("%s[%s] Q=%f" % (h5file_out, h5k, qtopo_xsum.sum()))

        h5_save_sum_rcut(h5f_out, h5k, qtopo, qtopo_rcut_list, axes=rcut_axes)

        h5f_out.close()

##############################################################################
##############################################################################
def strip_prefix(s, p):
    if s.startswith(p) : return s[len(p):]
    else : return None

if '__main__' == __name__ :
    what   = sys.argv[1]
    #jobid_list      = sys.argv[3:]
    jobid_list      = cfg_list

    def  subjob_iter(j_iter) :
        j_list  = list(j_iter)
        nj      = len(j_list)
        jj0     = ( sj_this    * nj) // sj_tot
        jj1     = ((sj_this+1) * nj) // sj_tot
        for j in j_list[jj0:jj1] : yield j

    if   'conv_hspec' == what :
        print("FIXME : (1) remove tpol (2) separate conv for 2pt meson, 2pt baryon (different bc)")
        sys.exit(1)
        for c in jobid_list :
            for ama in ama_list :
                for sm_srcsnk in ['SS', 'SP'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_conv_hspec(c, ama, sm_srcsnk)


    elif 'conv_c2pt' == what :
        for c in jobid_list :
            for ama in ama_list :
                for sm_srcsnk in ['SS', 'SP'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_conv_c2pt(c, ama, sm_srcsnk)
    elif what.startswith('conv_c2pt_') :
        s1 = strip_prefix(what, 'conv_c2pt_')
        for c in jobid_list :
            for ama in ama_list :
                for sm_srcsnk in ['SS', 'SP'] :
                    print("# %s %s %s %s" % (c, ama, sm_srcsnk, s1))
                    runme_conv_c2pt_vacins(c, ama, sm_srcsnk, 
                            vacins_spec_list=vacins_spec_list_all[s1])
    elif 'conv_bb' == what :
        for c in jobid_list : 
            for ama in ama_list :
                for sm_srcsnk in ['SS'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_conv_bb(c, ama, sm_srcsnk)
    elif what.startswith('conv_bb_') :
        s1 = strip_prefix(what, 'conv_bb_')
        for c in jobid_list : 
            for ama in ama_list :
                for sm_srcsnk in ['SS'] :
                    print("# %s %s %s %s" % (c, ama, sm_srcsnk, s1))
                    runme_conv_bb_vacins(c, ama, sm_srcsnk, 
                            vacins_spec_list=vacins_spec_list_all[s1])
    
    elif 'calc_op' == what :
        for c in jobid_list : 
            for ama in ama_list :
                for sm_srcsnk in ['SS'] :
                    print("# %s %s %s " % (c, ama, sm_srcsnk))
                    runme_calc_save_op(c, ama, sm_srcsnk)
    elif what.startswith('calc_op_') :
        s1 = strip_prefix(what, 'calc_op_')
        for c in jobid_list : 
            for ama in ama_list :
                for sm_srcsnk in ['SS'] :
                    print("# %s %s %s %s" % (c, ama, sm_srcsnk, s1))
                    runme_calc_save_op_vacins(c, ama, sm_srcsnk,
                            vacins_spec_list=vacins_spec_list_all[s1])
    
    
    
    elif 'bin_unbias_c2pt' == what :
        # XXX parallelized with --sj
        for i_j, (sm_srcsnk,) in enumerate(subjob_iter(it.product(
                    ['SS', 'SP']))) :
            print("# %d %s nconf=%d " % (i_j, sm_srcsnk, len(cfg_list)))
            runme_bin_unbias_c2pt(cfg_list, sm_srcsnk, ama_list=ama_list)
    elif what.startswith('bin_unbias_c2pt_') :
        s1 = strip_prefix(what, 'bin_unbias_c2pt_')
        # XXX parallelized with --sj
        for i_j, (sm_srcsnk, vacins) in enumerate(subjob_iter(it.product(
                    ['SS', 'SP'], vacins_spec_list_all[s1] ))) :
            print("# nconf=%d %s %s" % (len(cfg_list), sm_srcsnk, s1))
            runme_bin_unbias_c2pt_vacins(cfg_list, sm_srcsnk, ama_list=ama_list,
                    vacins_spec_list=[vacins])
    elif 'bin_unbias_op' == what :
        for sm_srcsnk in ['SS'] :
            print("# nconf=%d %s " % (len(cfg_list), sm_srcsnk))
            runme_bin_unbias_op(cfg_list, sm_srcsnk, ama_list=ama_list)
    elif what.startswith('bin_unbias_op_') :
        s1 = strip_prefix(what, 'bin_unbias_op_')
        for sm_srcsnk in ['SS'] :
            print("# nconf=%d %s %s" % (len(cfg_list), sm_srcsnk, s1))
            runme_bin_unbias_op_vacins(cfg_list, sm_srcsnk, ama_list=ama_list, 
                    vacins_spec_list=vacins_spec_list_all[s1])


    elif 'merge_c2pt' == what :
        for ama in ama_list :
            for sm_srcsnk in ['SS', 'SP'] :
                print("# nconf=%d %s %s" % (len(cfg_list), ama, sm_srcsnk))
                runme_merge_c2pt(cfg_list, ama, sm_srcsnk)
    elif what.startswith('merge_c2pt_') :
        s1 = strip_prefix(what, 'merge_c2pt_')
        for ama in ama_list :
            for sm_srcsnk in ['SS', 'SP'] :
                print("# nconf=%d %s %s" % (len(cfg_list), ama, sm_srcsnk))
                runme_merge_c2pt_vacins(cfg_list, ama, sm_srcsnk, vacins_spec_list=vacins_spec_list_all[s1])

    elif 'merge_op' == what :
        for ama in ama_list :
            for sm_srcsnk in ['SS'] :
                print("# nconf=%d %s %s" % (len(cfg_list), ama, sm_srcsnk))
                runme_merge_op(cfg_list, ama, sm_srcsnk)
    elif what.startswith('merge_op_') :
        s1 = strip_prefix(what, 'merge_op_')
        for ama in ama_list :
            for sm_srcsnk in ['SS'] :
                print("# nconf=%d %s %s" % (len(cfg_list), ama, sm_srcsnk))
                runme_merge_op_vacins(cfg_list, ama, sm_srcsnk, vacins_spec_list=vacins_spec_list_all[s1])

    elif 'qtopo_rcut' == what :
        for c in jobid_list : 
            runme_qtopo_rcut(c)

    elif what.startswith('nanscanAff_') : 
        s1 = strip_prefix(what, 'nanscanAff_')
        if   'c2pt' == s1 : 
            for c, ama, sm_srcsnk in subjob_iter(it.product(
                        cfg_list, ama_list, ['SS', 'SP'])) :
                print("# %s %s %s " % (c, ama, sm_srcsnk))
                runme_nanscanAff_c2pt(c, ama, sm_srcsnk)

        elif s1.startswith('c2pt_') :
            s2 = strip_prefix(s1, 'c2pt_')
            for c, ama, sm_srcsnk in subjob_iter(it.product(
                        cfg_list, ama_list, ['SS', 'SP'])) :
                print("# %s %s %s %s" % (c, ama, sm_srcsnk, s1))
                runme_nanscanAff_c2pt_vacins(c, ama, sm_srcsnk, 
                        vacins_spec_list=vacins_spec_list_all[s2])

        elif 'bb'   == s1 : 
            for c, ama, sm_srcsnk in subjob_iter(it.product(
                    cfg_list, ama_list, ['SS'])) :
                print("# %s %s %s " % (c, ama, sm_srcsnk))
                runme_nanscanAff_bb(c, ama, sm_srcsnk)

        elif s1.startswith('bb_') :
            s2 = strip_prefix(s1, 'bb_')
            for c, ama, sm_srcsnk in subjob_iter(it.product(
                    cfg_list, ama_list, ['SS'])) :
                print("# %s %s %s %s" % (c, ama, sm_srcsnk, s1))
                runme_nanscanAff_bb_vacins(c, ama, sm_srcsnk, 
                        vacins_spec_list=vacins_spec_list_all[s2])

    else : raise ValueError(what)

