from __future__ import print_function
from past.builtins import execfile
execfile('config.data.py') ; execfile('ens_setup.py')
#TODO
#TODO
#TODO
#TODO

"""
mlat=0.7
op='tensor1'
ir_name='H4_T1_d4r1'
tpol='posSzplus'
rsplan=('jk',1)
gg=ens_data_get(ama='ub', had='proton', flav='U-D', op=op, data_top=data_out, latsize=latsize, mlat=mlat)

tsep=8
ss_list=[ srcsnkpair(dt, 'SS') for dt in [ tsep ] ]
#mc_list=[ mc_case([k,k,0],[-3,-3,0], 'posSzplus') for k in np.r_[-3:5] ]
qext_list=np.array(list(lhpd.int_vectors_maxnorm(3, 10)))
mc_dict=lhpd.latcorr.make_mom_combo_list(.7, latsize, [[0,0,0]], qext_list)
mcgrp_list = [ [ mc_case(p0, p1, tpol) for p0,p1 in mc_dict[k] ] for k in sorted(mc_dict.keys()) ]
mc_std_list = [ mcgrp[0] for mcgrp in mcgrp_list ]
"""


"""
# testing read for 2pt
mc_std_list = [ mcgrp[0] for mcgrp in mcgrp_list ]  # same, from older version
x=gg.get_twopt_src(mc_std_list[0], ssgrp[0]); print(x.shape)

"""


"""

mcgrp1=mcgrp_list[1][0:2] ; print(mcgrp1)
eqmat1=lhpd.latcorr.make_ff_eqnmat_precond_f2f(op, mlat, latsize, mcgrp1, [(ir_name,1)])[0] ; print(eqmat1[(1,0),(0,1)])
jx_qy1=gg.get_threept(mcgrp1[1], ss_list[0], op, ir_name).real[:,0] ; a,e=lhpd.calc_avg_err(jx_qy1) ; print(a/e)
jy_qx1=gg.get_threept(mcgrp1[0], ss_list[0], op, ir_name).real[:,1] ; a,e=lhpd.calc_avg_err(jy_qx1) ; print(a/e)

mcgrp2=mcgrp_list[1][-1:-3:-1] ; print(mcgrp2)
eqmat2=lhpd.latcorr.make_ff_eqnmat_precond_f2f(op, mlat, latsize, mcgrp2, [(ir_name,1)])[0] ; print(eqmat2[(1,0),(0,1)])
jx_qy2=gg.get_threept(mcgrp2[1], ss_list[0], op, ir_name).real[:,0] ; a,e=lhpd.calc_avg_err(jx_qy2) ; print(a/e)
jy_qx2=gg.get_threept(mcgrp2[0], ss_list[0], op, ir_name).real[:,1] ; a,e=lhpd.calc_avg_err(jy_qx2) ; print(a/e)

# load also sink 2pt and calc jk-ratio
c2_0=gg.get_twopt_snk(mcgrp1[0], ss_list[0]).real
c2_0_r=lhpd.resample(c2_0, rsplan)

jx_qy1_r=lhpd.resample(jx_qy1, rsplan)
a,e=lhpd.calc_avg_err(jx_qy1_r/c2_0_r[:,tsep:tsep+1], rsplan) ; print(a, '\n', a/e)

jy_qx1_r=lhpd.resample(jy_qx1, rsplan)
a,e=lhpd.calc_avg_err(jy_qx1_r/c2_0_r[:,tsep:tsep+1], rsplan) ; print(a, '\n', a/e)


c3_gV=gg.get_threept(mcgrp_list[0][0], ss_list[0], op, ir_name).real[:,3]
c3_gV_r=lhpd.resample(c3_gV, rsplan)
a,e=lhpd.calc_avg_err(c3_gV_r/c2_0_r[:,tsep:tsep+1], rsplan) ; print(a, '\n', a/e)

"""

