from __future__ import print_function
import aff
import h5py 
import numpy as np
import math
import itertools as it
import lhpd
from lhpd import make_src_grid
from lhpd.misc import strkey

# XXX general idea
# * modular database (many separate files with internal structure)
# * each dataset is addressed by pair (file, kpath)
# * datasets are standard for c2pt, c3pt, c4pt
# * file, kpath) are created by specific functions:
# * * c2pt: (dat_d, c2_d)
# * * c3pt: (dat_d, c3_d, ins_d)
# * * c4pt: (dat_d, c4_d(=?=c3_d), ins1_d, ins2_d)
# * * c2pt with vac.insertion (dat_d, c2_d, vi_d)
# * * c3pt with vac.insertion (dat_d, c3_d, vi_d, ins_d)
# * dat_d incorporates type(stage:raw,all,bin,ub), cfg?,csrc?,ama? depending on stage
# * XXX_d stands for "description"(dictionary) for XXX
# * preferably use Attrdict?

# XXX pieces of rationale
# Q: which parts of metadata go to file, which go to kpath?
# * most frequently used together -> kpath, the rest -> file
# * separate files are useful for downloading and working on
# *   partial analysis

# XXX examples
# dat_d = dict(kind='bin'|'all'|'cfg', ama='ub'|'sl'|'ex', cfgkey=, csrc=|csrcgrp=)
# c2_d = dict(
#   had='proton', [tpol='posSzplus',]
#   smsrcsnk=, [smtag=,]
#   [bgem='uEx0y0z-2_dEx0y0z4',]
# c2vi_d = TODO
# c3_d = dict(
#   had='proton', tpol='posSzplus',
#   psnk=
#   tsep=
#   smsrcsnk=, [smtag=])
# c3vi_d = TODO
# c3ts_d = dict(
#   had='proton', [tpol='posSzplus',]
#   smsrcsnk=, [smtag=,]
#   [bgem='uEx0y0z-2_dEx0y0z4',]
#   
# ins_d = dict(
#   kind='bb', flav='U'|'D', lpath='xYz' (if empty: all available?) lpath_list=?
#   kind='op', flav='U'|'D', op='tensor1', ir='adfadf'|ir_list=[], flav=
#   kind='qpdf' probably same as bb
#   kind='tmd'
# vi_d = dict(
#   kind='theta'|'volcedm'|'volpsc',
#   ?volcedm,volpsc: flav='U'|'D'
#   ?volcedm: gf=
#   ?theta: rcut=, dtcut=
# ff_d = dict(
#     ...
#   c3_d=(tsep=...
#       had='proton'?
#   )
#   ins_d=(
#       flav='U|D|...'
#       op='tensor1'? ff='F1|F2'
#         ='pstensor1'? 'GA|GP'
#   m_d=(method=..._
#   TODO momenta? method? improv? conn/disc?)
# c2fit_d=dict(method?)

# XXX examples

# hack!
lcdb_opt = globals().get('lcdb_opt', {})


def srcsnk_str_f(cN_d) : return '%s' %(cN_d['smsrcsnk'],)
def srcsnk_str_k(cN_d) : return '%s' %(cN_d['smsrcsnk'],)

def c3snk_str_f(c3_d) : return strkey.snk_str_f(c3_d['psnk'], c3_d['tsep'])
def c3snk_str_k(c3_d) : return strkey.snk_str_k(c3_d['psnk'], c3_d['tsep'])

def vacins_meta0_str_aff_k(vi_d) :
    vi_kind = vi_d['kind']
    if   'theta' == vi_kind : pass
    elif 'volcedm'  == vi_kind : return '%s' % (vi_d['gf'])
    elif 'volpsc' == vi_kind :   return 'orig'      # sic! stupid hack that I have to maintain...
    else : raise ValueError(vi_d)
def vacins_meta0_str_k(vi_d) :
    vi_kind = vi_d['kind']
    if   'theta' == vi_kind : 
        rcut_s, dtcut_s = '', ''
        if vi_d.get('rcut') : rcut_s  = '/rcut%d'  % vi_d['rcut']
        if vi_d.get('dtcut'): dtcut_s = '/dtcut%d' % vi_d['dtcut']
        return '%s%s%s' % (vi_d['gf'], rcut_s, dtcut_s)
    elif 'volcedm'  == vi_kind : return '%s/%s' % (vi_d['flav'], vi_d['gf'])
    elif 'volpsc' == vi_kind :   return '%s' % (vi_d['flav'],)
    else : raise ValueError(vi_d)

def vacins_str_desc(vi_d) : 
    vi_kind = vi_d['kind']
    vi_str = None
    if   'theta' == vi_kind : 
        rcut_s, dtcut_s = '', ''
        if vi_d.get('rcut') : rcut_s  = '.rcut%d'  % vi_d['rcut']
        if vi_d.get('dtcut'): dtcut_s = '.dtcut%d' % vi_d['dtcut']
        vi_str = '%s%s%s' % (vi_d['gf'], rcut_s, dtcut_s)
    elif 'volcedm'  == vi_kind : 
        vi_str = '%s.%s' % (vi_d['gf'], vi_d['flav'])
    elif 'volpsc' == vi_kind : 
        vi_str = '%s' % (vi_d['flav'],)
    else : raise ValueError(vi_d)
    return '%s.%s' % (vi_kind, vi_str)

def ins_meta0_str_k(ins_d) :
    """ keypaths for results """
    ins_kind = ins_d['kind']
    if   'op' == ins_kind :
        s1 = '%s/%s' % (ins_d['flav'], ins_d['op'])
    else : raise NotImplementedError
    return s1

def ins_str_desc(ins_d) :
    ins_kind = ins_d['kind']
    if   'bb' == ins_kind : 
        s = '%s.lp=[%d:%d]' % (ins_d['flav'], ins_d['lmin'], ins_d['lmax'])
    elif 'op' == ins_kind :
        s = '%s.%s' % (ins_d['flav'], ins_d['op'])
    else : raise ValueError(ins_kind)
    return '%s{%s}' % (ins_kind, s)

def hadron_str(cN_d) : 
    had = cN_d['had']
    if 'proton' == had : return "%s_%s" %(had, cN_d['tpol'])
    else : raise ValueError(had)
hadron_str_f = hadron_str
hadron_str_k = hadron_str

def ama_str(ama_s) : return ama_s

def dat_str_f(which, dat_d) :       # only for conv, all, bin, ub
    dat_kind = dat_d['kind']
    if   'conv' == dat_kind : 
        return '%s/%s.%s.%s' % (which, which, 
                dat_d['cfgkey'], ama_str(dat_d['ama']))
    elif 'all'  == dat_kind :
        return '%s.all/%s-all.%s' % (which, which, ama_str(dat_d['ama']))
    elif 'bin'  == dat_kind :
        return '%s.bin/%s-bin.%s' % (which, which, ama_str(dat_d['ama']))
    elif 'ub'   == dat_kind : 
        return '%s.bin/%s-unbias' % (which, which)
    else : raise ValueError(dat_kind)

def obsv_str_f(which, dat_d) :
    dat_kind = dat_d['kind']
    if 'all'  == dat_kind :
        return '%s-all.%s' % (which, ama_str(dat_d['ama']))
    elif 'bin'  == dat_kind :
        return '%s-bin.%s' % (which, ama_str(dat_d['ama']))
    elif 'ub'   == dat_kind : 
        return '%s-unbias' % (which,)

#################### c2pt ########################
def get_c2pt_file(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    which   = 'c2pt'
    if 'bgem' in c2_d : which = '%s_%s' % (which, c2_d['bgem'])
    if   'aff'  == dat_kind :
        return '%s/%s/c2pt.%s.%s.%s.aff' % (
                which, dat_d['cfgkey'], dat_d['cfgkey'], ama_str(dat_d['ama']), 
                strkey.csrc_str_f(dat_d['csrcgrp'][0]))
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
        return '%s.%s.%s.h5' % (
                dat_str_f(which, dat_d), 
                c2_d['had'], c2_d['smsrcsnk'])
    else : raise ValueError(dat_kind)

def get_c2pt_kpath(dat_d, c2_d) :
    dat_kind = dat_d['kind']
    which   = 'c2pt'
    if 'bgem' in c2_d : which = '%s_%s' % (which, c2_d['bgem'])
    if   'aff'  == dat_kind :
        #compatibility switches
        csrc_s = ({ 'csrc0k'    : strkey.csrc_str_k(dat_d['csrcgrp'][0]), 
                    'csrcgrp'   : strkey.csrcgrp_str_f(dat_d['csrcgrp']),
                })[lcdb_opt.get('c2pt_kpath_csrcgrp', 'csrc0k')]
        had_src= ({ 'had_src'   : '%s/%s' % (hadron_str_k(c2_d), csrc_s),
                    'src_had'   : '%s/%s' % (csrc_s, hadron_str_k(c2_d)),
                })[lcdb_opt.get('c2pt_kpath_had_src', 'had_src')]
        return '/%s/%s/%s' % (which, c2_d['smsrcsnk'], had_src)

    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :
        s1 = '/%s/%s/%s' % (which, c2_d['smsrcsnk'], c2_d['had'])
        if 'conv' == dat_kind : return '/cfg%s%s' % (dat_d['cfgkey'], s1)
        else : return s1
    else : raise ValueError(dat_kind)

#################### c2pt_VACINS ########################
def vacins_meta0_str_f(vi_d) :
    vi_kind = vi_d['kind']
    if   'theta' == vi_kind : 
        rcut_s, dtcut_s = '', ''
        if vi_d.get('rcut') : rcut_s  = '.rcut%d'  % vi_d['rcut']
        if vi_d.get('dtcut'): dtcut_s = '.dtcut%d' % vi_d['dtcut']
        return '%s%s%s' % (vi_d['gf'], rcut_s, dtcut_s)
    elif 'volcedm'  == vi_kind : return '%s.%s' % (vi_d['flav'], vi_d['gf'])
    elif 'volpsc' == vi_kind :   return '%s' % (vi_d['flav'],)
    else : raise ValueError(vi_d)
def vacins_meta0_str_aff_f(vi_d) :
    vi_kind = vi_d['kind']
    if   'volcedm'  == vi_kind : return '.%s' % (vi_d['gf'])
    elif 'volpsc' == vi_kind :   return ''
    else : raise ValueError(vi_d)

def vacins_meta0_str_aff_k(vi_d) :
    vi_kind = vi_d['kind']
    if   'theta' == vi_kind : pass
    elif 'volcedm'  == vi_kind : return '%s' % (vi_d['gf'])
    elif 'volpsc' == vi_kind :   return 'orig'      # sic! stupid hack that I have to maintain...
    else : raise ValueError(vi_d)
def vacins_meta0_str_k(vi_d) :
    vi_kind = vi_d['kind']
    if   'theta' == vi_kind : 
        rcut_s, dtcut_s = '', ''
        if vi_d.get('rcut') : rcut_s  = '/rcut%d'  % vi_d['rcut']
        if vi_d.get('dtcut'): dtcut_s = '/dtcut%d' % vi_d['dtcut']
        return '%s%s%s' % (vi_d['gf'], rcut_s, dtcut_s)
    elif 'volcedm'  == vi_kind : return '%s/%s' % (vi_d['flav'], vi_d['gf'])
    elif 'volpsc' == vi_kind :   return '%s' % (vi_d['flav'],)
    else : raise ValueError(vi_d)
def get_c2pt_vacins_file(dat_d, c2_d, vi_d) :
    dat_kind = dat_d['kind']
    vi_kind  = vi_d['kind']
    
    which1 = 'c2pt_%s' % (vi_kind,)
    if 'bgem' in c2_d : which = '%s_%s' % (which1, c2_d['bgem'])
    else : which = which1
    if   'aff'  == dat_kind :
        return '%s/%s/%s.%s.%s.%s%s.aff' % (
                which, dat_d['cfgkey'], which1, dat_d['cfgkey'], ama_str(dat_d['ama']), 
                strkey.csrc_str_f(dat_d['csrcgrp'][0]), vacins_meta0_str_aff_f(vi_d))
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :
        return '%s.%s.%s.%s.h5' % (
                dat_str_f(which, dat_d),
                c2_d['had'], c2_d['smsrcsnk'], vacins_meta0_str_f(vi_d))
    else : raise ValueError(dat_kind)

def get_c2pt_vacins_kpath(dat_d, c2_d, vi_d) :
    dat_kind = dat_d['kind']
    vi_kind  = vi_d['kind']

    which1 = 'c2pt_%s' % (vi_kind,)
    if 'bgem' in c2_d : which = '%s_%s' % (which1, c2_d['bgem'])
    else : which = which1
    if   'aff'  == dat_kind :
        vispecx  = vacins_meta0_str_aff_k(vi_d)
        return '/%s/%s/%s/%s/%s/%s' % (
                which, c2_d['smsrcsnk'], 
                #strkey.csrcgrp_str_k_v1(dat_d['csrcgrp']), vispecx, hadron_str_k(c2_d), vi_d['flav'],
                vispecx, hadron_str_k(c2_d), vi_d['flav'], strkey.csrcgrp_str_k_v1(dat_d['csrcgrp']),
                )
    elif dat_kind in ['conv', 'all', 'bin', 'ub'] :
        vispecx  = vacins_meta0_str_k(vi_d)
        s1 = '/%s/%s/%s/%s' % (which, c2_d['smsrcsnk'], c2_d['had'], vispecx)
        if 'conv' == dat_kind : return '/cfg%s%s' % (dat_d['cfgkey'], s1)
        else : return s1
    else : raise ValueError(dat_kind)


def get_c3ts_file(dat_d, c2_d, vi_d) :
    dat_kind = dat_d['kind']
    vi_kind  = vi_d['kind']
    which    = 'c3ts_%s' % (vi_kind,)
    if 'bgem' in c2_d : which = '%s_%s' % (which, c2_d['bgem'])
    if dat_kind in ['all', 'bin', 'ub'] :
        return '%s.%s.%s.%s.h5' % (
                dat_str_f(which, dat_d),
                c2_d['had'], c2_d['smsrcsnk'], vacins_meta0_str_f(vi_d))
    else : raise ValueError(dat_kind)

def get_c3ts_kpath(dat_d, c2_d, vi_d) :
    dat_kind = dat_d['kind']
    vi_kind  = vi_d['kind']
    which    = 'c3ts_%s' % (vi_kind,)
    if 'bgem' in c2_d : which = '%s_%s' % (which, c2_d['bgem'])
    if dat_kind in ['all', 'bin', 'ub'] :
        vispecx  = vacins_meta0_str_k(vi_d)
        s1 = '/%s/%s/%s/%s' % (which, c2_d['smsrcsnk'], c2_d['had'], vispecx)
        if 'conv' == dat_kind : return '/cfg%s%s' % (dat_d['cfgkey'], s1)
        else : return s1
    else : raise ValueError(dat_kind)

#################### c3pt ########################
def get_c3pt_file(dat_d, c3_d, ins_d) :
    dat_kind = dat_d['kind']
    ins_kind = ins_d['kind']

    if   'bb'   == ins_kind :
        # bb is very generic: also tmd or qpdf, just change 'bb' appropriately
        if   'aff'  == dat_kind :
            return 'bb/%s/bb.%s.%s.%s.%s.%s.%s.aff' % (
                    dat_d['cfgkey'], dat_d['cfgkey'], ama_str(dat_d['ama']), 
                    strkey.csrc_str_f(dat_d['csrcgrp'][0]), c3snk_str_f(c3_d),
                    c3_d['tpol'], ins_d['flav'])
        elif dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            return '%s.%s.%s.%s.%s.h5' % (
                    dat_str_f('bb', dat_d),
                    c3snk_str_f(c3_d), hadron_str_f(c3_d), 
                    c3_d['smsrcsnk'], ins_d['flav'])
        else : raise ValueError((dat_kind, ins_kind))

    elif 'op'   == ins_kind :
        if dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            return '%s.%s.%s.%s.%s.%s.h5' % (
                    dat_str_f('op', dat_d),
                    c3snk_str_f(c3_d), hadron_str_f(c3_d), 
                    c3_d['smsrcsnk'], ins_d['flav'], ins_d['op'])
        else : raise ValueError((dat_kind, ins_kind))
    else : raise ValueError(ins_kind)

def get_c3pt_kpath(dat_d, c3_d, ins_d) :
    # XXX bb is very generic: also tmd or qpdf, just change 'bb' appropriately
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']
    if   'bb'   == ins_kind :
        if   'aff'  == dat_kind :
            kk = lcdb_opt.get('c3pt_kpath_csrcgrp', 'csrc0k')
            if 'csrc0k' == kk : csrc_s = strkey.csrc_str_k(dat_d['csrcgrp'][0])
            elif kk.startswith('csrcgrp') : csrc_s = strkey.csrcgrp_str_f(dat_d['csrcgrp'])
            else : raise ValueError(('c3pt_kpath_csrcgrp', kk))
            #print("here! c3pt_kpath_csrcgrp=", kk)

            return '/bb/%s/%s/%s/%s/%s' % (
                    c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    ins_d['flav'], csrc_s,
                    c3snk_str_k(c3_d))
        elif dat_kind in ['conv', 'all', 'bin', 'ub'] : 
            s1 = '/bb/%s/%s/%s/%s' % (
                    c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_d['flav'])
            if 'conv' == dat_kind : return '/cfg%s%s' % (dat_d['cfgkey'], s1)
            else : return s1
        else : raise ValueError((dat_kind, ins_kind))

    elif 'op'   == ins_kind :
        if dat_kind in ['conv', 'all', 'bin', 'ub'] :
            s1 = '/op/%s/%s/%s/%s/%s/%s' % (
                    c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_d['flav'], ins_d['op'], ins_d['ir'])
            if 'conv' == dat_kind : return '/cfg%s%s' % (dat_d['cfgkey'], s1)
            else : return s1
        else : raise ValueError((dat_kind, ins_kind))

#################### c3pt_VACINS ########################
def get_c3pt_vacins_file(dat_d, c3_d, ins_d, vi_d) :
    ins_kind = ins_d['kind']
    dat_kind = dat_d['kind']
    vi_kind  = vi_d['kind']
    vispecx  = vacins_meta0_str_f(vi_d)

    if   'bb'   == ins_kind :
        # bb is very generic: also tmd or qpdf, just change 'bb' appropriately
        if   'aff'  == dat_kind :
            return 'bb_%s/%s/bb_%s.%s.%s.%s.%s.%s.%s.%s.aff' % (
                    vi_kind, dat_d['cfgkey'], vi_kind, dat_d['cfgkey'], ama_str(dat_d['ama']), 
                    strkey.csrc_str_f(dat_d['csrcgrp'][0]), c3snk_str_f(c3_d),
                    c3_d['tpol'], ins_d['flav'], vispecx)
        elif dat_kind in ['conv', 'all', 'bin', 'ub'] :
            return '%s.%s.%s.%s.%s.%s.h5' % (
                    dat_str_f('bb_%s' % vi_kind, dat_d), 
                    c3snk_str_f(c3_d), hadron_str_f(c3_d), 
                    c3_d['smsrcsnk'], ins_d['flav'], vispecx)
        else : raise ValueError((dat_kind, ins_kind))

    elif 'op'   == ins_kind :
        if dat_kind in ['conv', 'all', 'bin', 'ub'] :       
            return '%s.%s.%s.%s.%s.%s.%s.h5' % (
                    dat_str_f('op_%s' % vi_kind, dat_d),
                    c3snk_str_f(c3_d), hadron_str_f(c3_d), 
                    c3_d['smsrcsnk'], ins_d['flav'], ins_d['op'], vispecx)
        else : raise ValueError((dat_kind, ins_kind))
    else : raise ValueError(ins_kind)

def get_c3pt_vacins_kpath(dat_d, c3_d, ins_d, vi_d) :
    # XXX bb is very generic: also tmd or qpdf, just change 'bb' appropriately
    dat_kind = dat_d['kind']
    ins_kind = ins_d['kind']
    vi_kind  = vi_d['kind']

    if   'bb'   == ins_kind :
        if   'aff'  == dat_kind :
            vispecx  = vacins_meta0_str_aff_k(vi_d)
            return '/bb_%s/%s/%s/%s/%s/%s/%s/%s' % (
                    vi_kind, c3_d['smsrcsnk'], vispecx, hadron_str_k(c3_d),
                    ins_d['flav'], vi_d['flav'], 
                    strkey.csrcgrp_str_k(dat_d['csrcgrp']), c3snk_str_k(c3_d))
        elif dat_kind in ['conv', 'all', 'bin', 'ub'] : 
            vispecx  = vacins_meta0_str_k(vi_d)
            s1 = '/bb_%s/%s/%s/%s/%s/%s' % (
                    vi_kind, c3_d['smsrcsnk'], hadron_str_k(c3_d), 
                    c3snk_str_k(c3_d), ins_d['flav'], vispecx)
            if 'conv' == dat_kind : return '/cfg%s%s' % (dat_d['cfgkey'], s1)
            else : return s1
        else : raise ValueError((dat_kind, ins_kind))

    elif 'op'   == ins_kind :
        if dat_kind in ['conv', 'all', 'bin', 'ub'] :
            vispecx  = vacins_meta0_str_k(vi_d)
            s1 = '/op_%s/%s/%s/%s/%s/%s/%s/%s' % (
                    vi_kind, c3_d['smsrcsnk'], hadron_str_k(c3_d),
                    c3snk_str_k(c3_d), ins_d['flav'], ins_d['op'], ins_d['ir'], vispecx)
            if 'conv' == dat_kind : return '/cfg%s%s' % (dat_d['cfgkey'], s1)
            else : return s1
        else : raise ValueError((dat_kind, ins_kind))

#################### ff ########################
def get_ff_file(dat_d, c3_d, ins_d, m_d) :
    dat_kind = dat_d['kind']
    ins_kind = ins_d['kind']
    assert(dat_kind in ['all', 'bin', 'ub'])
    assert(ins_kind in ['op'])
    return '%s.%s.%s.h5' % (
        obsv_str_f('ff', dat_d), c3_d['had'], ins_d['op'])
def get_ff_kpath(dat_d, c3_d, ins_d, m_d) :
    dat_kind = dat_d['kind']
    ins_kind = ins_d['kind']
    assert(dat_kind in ['all', 'bin', 'ub'])
    assert(ins_kind in ['op'])
    return '/ff/%s/%s/%s' % (c3_d['had'], ins_d['op'], ins_d['flav'])
    
#################### al5 ########################
def get_al5_file(dat_d, c2_d, vi_d) :
    dat_kind = dat_d['kind']
    assert(dat_kind in ['all', 'bin', 'ub'])
    return '%s.%s.%s.h5' % (
        obsv_str_f('al5', dat_d), c2_d['had'], vi_d['kind'])
def get_al5_kpath(dat_d, c2_d, vi_d) :
    dat_kind = dat_d['kind']
    assert(dat_kind in ['all', 'bin', 'ub'])
    return '/alfive/%s/%s' % (c2_d['had'], vacins_meta0_str_k(vi_d))

#################### ff_VACINS ########################
def get_ff_vacins_file(dat_d, c3_d, ins_d, vi_d, m_d) :
    dat_kind = dat_d['kind']
    ins_kind = ins_d['kind']
    assert(dat_kind in ['all', 'bin', 'ub'])
    assert(ins_kind in ['op'])
    return '%s.%s.%s.%s.h5' % (
        obsv_str_f('ffvi', dat_d), c3_d['had'], ins_d['op'], vi_d['kind'])
def get_ff_vacins_kpath(dat_d, c3_d, ins_d, vi_d, m_d) :
    dat_kind = dat_d['kind']
    ins_kind = ins_d['kind']
    assert(dat_kind in ['all', 'bin', 'ub'])
    assert(ins_kind in ['op'])
    return '/ffvi/%s/%s/%s/%s/%s' % (
            c3_d['had'], ins_d['op'], ins_d['flav'], 
            vi_d['kind'], vacins_meta0_str_k(vi_d))

#################### c2pt fits ########################
def get_c2fit_file(dat_d, c2_d, m_d) :
    dat_kind = dat_d['kind']
    assert(dat_kind in ['all', 'bin', 'ub'])
    return '%s.%s.h5' % (
        obsv_str_f('c2fit', dat_d), c2_d['had'])
def get_c2fit_kpath(dat_d, c2_d, m_d) :
    dat_kind    = dat_d['kind']
    assert(dat_kind in ['all', 'bin', 'ub'])
    p3str       = 'px%dpy%dpz%d' % tuple(c2_d['psnk'])
    m_tagkey    = m_d['tagkey']
    return '/c2fit/%s/%s' % (
        m_tagkey, p3str)

############# vacuum insertions ##############
def discvacins_meta0_f(vi_d) :
    vi_kind = vi_d['kind']
    if   'theta' == vi_kind :
        gf_s = ''
        if vi_d.get('gf')  : gf_s  = '.%s' % vi_d['gf']
        vi_str = gf_s
    else :raise ValueError(vi_kind)
    return vi_str

def discvacins_meta0_k(vi_d) :
    vi_kind = vi_d['kind']
    if   'theta' == vi_kind :
        gf_s, qop_s = '', '' 
        if vi_d.get('gf')  : gf_s  = '/%s' % vi_d['gf']
        if vi_d.get('qop') : qop_s = '/qtopo_%s' % vi_d['qop']
        vi_str = '%s%s' % (gf_s, qop_s)
    else :raise ValueError(vi_kind)
    return vi_str

def get_discvacins_file(dat_d, vi_d)  :
    #data.out/data.prod.coh4X/qtopo/qtopo.1340.wf0.05x300.h5
    return 'qtopo/qtopo.%s%s.h5' % (
            dat_d['cfgkey'], discvacins_meta0_f(vi_d))
def get_discvacins_kpath(dat_d, vi_d) :
    #/cfg1340/wf0.05x300/qtopo_imp5li/rcut44
    return '/cfg%s/%s' % (dat_d['cfgkey'], discvacins_meta0_k(vi_d))

def get_qtopo_orig_h5_file(cfgkey, tag,
        data_in=data_in) :
    return '%s/qtopo.h5/qtopo.%s.%s.h5' % (data_in, cfgkey, tag) 
def get_qtopo_proc_h5_file(cfgkey, tag, 
        data_out=data_out) :
    return '%s/qtopo/qtopo.%s.%s.h5' % (data_out, cfgkey, tag) 
def get_qtopo_corr_h5_file(cfgkey, tag, 
        data_out=data_out) :
    return '%s/qtopo_corr/qtopo_corr.%s.%s.h5' % (data_out, cfgkey, tag) 
def get_qtopo_corravg_h5_file(tag, 
        data_out=data_out) :
    return '%s/qtopo_corravg/qtopo_corravg.%s.h5' % (data_out, tag) 

def get_posnpr_orig_h5_file_kpath(cfgkey, ama, csrc, tag,
        data_in=data_in) :
    fn  = '%s/posnpr.h5/posnpr.%s.%s.%s.h5' % (
        data_in, cfgkey, ama, csrc_str_f(csrc))
    kp = '/%s' % (tag)
    return fn, kp
def get_posnpr_unbias_h5_file_kpath(cfgkey, tag,
        data_out=data_out) :
    fn  = '%s/posnpr.h5/posnpr-unbias.%s.h5' % (data_out, cfgkey)
    kp = '/%s' % (tag)
    return fn, kp
def get_posnpr_collect_h5_file_kpath(tag,
        data_out=data_out) :
    fn  = '%s/posnpr-collect.h5/posnpr-collect.h5' % (data_out,)
    kp = '/%s' % (tag)
    return fn, kp



############# common function #############
def get_lcdb_loc(dat_d, lc_d) :
    """ dat_d   = dict(data_dir=,ama=,cfgkey=,csrcgrp=,...)
        lc_d    = dict(kind='c2',   c2_d=,)
                | dict(kind='c2vi', c2_d=, ins_d=, vi_d=)
                | dict(kind='c3',   c3_d=, ins_d=)
                | dict(kind='c3vi', c3_d=, ins_d=, vi_d= } ??? crosscheck with setup @Mira
    """
    ddir    = dat_d['data_dir']
    kind    = lc_d['kind']
    if   'c2'   == kind : 
        c2_d    = lc_d['c2_d']
        return (
            '%s/%s' % (ddir, 
                get_c2pt_file(dat_d, c2_d)), 
                get_c2pt_kpath(dat_d, c2_d))
    elif 'c2vi' == kind : 
        c2_d    = lc_d['c2_d']
        vi_d    = lc_d['vi_d']
        return (
            '%s/%s' % (ddir, 
                get_c2pt_vacins_file(dat_d, c2_d, vi_d)), 
                get_c2pt_vacins_kpath(dat_d, c2_d, vi_d))
    elif 'c3'   == kind :
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        return (
            '%s/%s' % (ddir, get_c3pt_file(dat_d, c3_d, ins_d)), 
            get_c3pt_kpath(dat_d, c3_d, ins_d))
    elif 'c3vi' == kind :
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        vi_d    = lc_d['vi_d']
        return (
            '%s/%s' % (ddir, get_c3pt_vacins_file(dat_d, c3_d, ins_d, vi_d)), 
            get_c3pt_vacins_kpath(dat_d, c3_d, ins_d, vi_d))
    elif 'ff'   == kind :
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        m_d     = lc_d.get('m_d', {})
        return (
            '%s/%s' % (ddir, get_ff_file(dat_d, c3_d, ins_d, m_d)), 
            get_ff_kpath(dat_d, c3_d, ins_d, m_d))
    elif 'ffvi' == kind :
        c3_d    = lc_d['c3_d']
        ins_d   = lc_d['ins_d']
        vi_d    = lc_d['vi_d']
        m_d     = lc_d.get('m_d', {})
        return (
            '%s/%s' % (ddir, get_ff_vacins_file(dat_d, c3_d, ins_d, vi_d, m_d)), 
            get_ff_vacins_kpath(dat_d, c3_d, ins_d, vi_d, m_d))
    elif 'al5'  == kind :
        c2_d    = lc_d['c2_d']
        vi_d    = lc_d['vi_d']
        return (
            '%s/%s' % (ddir, get_al5_file(dat_d, c2_d, vi_d)), 
            get_al5_kpath(dat_d, c2_d, vi_d))
    elif 'c2fit'== kind :
        c2_d    = lc_d['c2_d']
        m_d     = lc_d['m_d']
        return (
            '%s/%s' % (ddir, get_c2fit_file(dat_d, c2_d, m_d)),
            get_c2fit_kpath(dat_d, c2_d, m_d))

    elif 'c3ts' == kind :
        c2_d    = lc_d['c2_d']
        vi_d    = lc_d['vi_d']
        return (
            '%s/%s' % (ddir, 
                get_c3ts_file(dat_d, c2_d, vi_d)), 
                get_c3ts_kpath(dat_d, c2_d, vi_d))


    # FIXME duplication: dvi and qtopo can access the same data
    elif 'dvi'  == kind:
        vi_d    = lc_d['vi_d']
        return (
            '%s/%s' % (ddir, get_discvacins_file(dat_d, vi_d)),
            get_discvacins_kpath(dat_d, vi_d))

    # redundant? also accessed by dvi
    elif 'qtopo' == kind :
        # dat_d = dict(cfgkey=, data_dir=)
        # lc_d=dict(kind='qtopo', dkind=orig|proc|corr|corravg, gf=, qop=)
        dkind = lc_d['dkind']
        if   'orig' == dkind : #  hdf
            return (get_qtopo_orig_h5_file(dat_d['cfgkey'], lc_d['gf'], data_in=ddir),
                    '/cfg%s/%s/qtopo_%s' % (dat_d['cfgkey'], lc_d['gf'], lc_d['qop']))
        elif 'proc' == dkind :
            return (get_qtopo_proc_h5_file(dat_d['cfgkey'], lc_d['gf'], data_out=ddir),
                    '/cfg%s/%s/qtopo_%s' % (dat_d['cfgkey'], lc_d['gf'], lc_d['qop']))
        elif 'corr' == dkind :
            # coord-dependent correlation
            return (get_qtopo_corr_h5_file(dat_d['cfgkey'], lc_d['gf'], data_out=ddir),
                    '/cfg%s/%s/qtopo_%s' % (dat_d['cfgkey'], lc_d['gf'], lc_d['qop']))
        elif 'corravg' == dkind :
            # coord-dependent correlation, averaged over H(5), binned?
            return (get_qtopo_corravg_h5_file(lc_d['gf'], data_out=ddir),
                    '/%s/qtopo_%s' % (lc_d['gf'], lc_d['qop']))
        else : raise ValueError(dkind)
            

    else : raise ValueError(kind)
