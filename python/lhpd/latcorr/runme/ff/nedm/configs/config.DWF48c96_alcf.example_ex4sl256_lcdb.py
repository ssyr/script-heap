from past.builtins import execfile
import numpy as np
import lhpd
from lhpd.latcorr import make_linkpath_list
from lhpd.h5_io import h5_meas_dtype
from lhpd import int_vectors_maxnorm, normsorted
from lhpd.misc.strkey import *


latsize = np.r_[48,48,48,96]
t_axis  = len(latsize) - 1
ferm_bc = np.r_[1,1,1,1]
q_bc_t  = ferm_bc[t_axis]
meson_bc_t = q_bc_t**2
baryon_bc_t= q_bc_t**3


######################
cfg_index   = lhpd.read_key_list('../list.cfg.all.48I', 'index')
ama_ex      = 'ex'
ama_sl      = 'sl'
ama_list = [
    ama_ex, 
    ama_sl,
    ]
def ama_str(ama) : return ama

ncoh_src_t      = 4 # number of coherent src in one group
c2pt_hslab_len  = latsize[t_axis] // ncoh_src_t
c3pt_tlen       = latsize[t_axis] // ncoh_src_t

def make_csrcgrp_list(ama, cfgkey):
    nsrc_ama_map = {
        ama_ex : [1,1,1,4],
        ama_sl : [4,4,4,4] }
    csrc_meas_dxc    = [7,11,13,23]
    csrc_meas_x0     = [0,0,0,0]
    srcgrp_dx_it    = np.array(latsize) / 2 ; srcgrp_dx_it[t_axis] = 0

    return lhpd.make_srcgrp_grid(
            nsrc_ama_map[ama], latsize, csrc_meas_x0, csrc_meas_dxc,
            1 + cfg_index.index(cfgkey), t_axis, ncoh_src_t,
            dx_it=srcgrp_dx_it)


######################
c2pt_psnk_list  = normsorted(list(int_vectors_maxnorm(3, 10)))
c2pt_tpol_list  = [ 'Tg%d' % g for g in range(16) ]

c2pt_spec_list      = [ 
    dict(had='proton', tpol_list=c2pt_tpol_list,
         psnk_list=c2pt_psnk_list, smsrcsnk=smsrcsnk,
         saved_bc_t=baryon_bc_t)
    for smsrcsnk in ['SS', 'SP'] ]


######################
c3pt_tsep_list      = [ 8,9,10,11,12 ]
c3pt_vi_tsep_list   = [ 8,9,10 ]
c3pt_tpol_list      = ['posSzplus']
c3pt_tpol5_list     = c3pt_tpol_list
c3pt_psnk_list = np.asarray([[0,0,0]])

c3pt_spec_list   = [
    dict(had='proton', tpol=tpol, psnk=psnk, tsep=tsep,
         smsrcsnk='SS', saved_bc_t=1)  # sic! bc is not used in c3pt saving
    for (tpol, psnk, tsep) in it.product(
            c3pt_tpol_list, c3pt_psnk_list, c3pt_tsep_list) ]

c3pt_vi_spec_list   = [
    dict(had='proton', tpol=tpol, psnk=psnk, tsep=tsep,
         smsrcsnk='SS', saved_bc_t=1)  # sic! bc is not used in c3pt saving
    for (tpol, psnk, tsep) in it.product(
            c3pt_tpol_list, c3pt_psnk_list, c3pt_vi_tsep_list) ]


######################
flav_ins_list   = ['U', 'D']
c3pt_op_list    = [
    #'pstensor0', 
    #'tensor0',
    'tensor1', 
    'pstensor1',
    #'sigma2a2', 
]
c3pt_qext_list = normsorted(list(int_vectors_maxnorm(3, 10)))

bb_spec_list    = lhpd.dict_prod(
        dict(kind='bb', lmin=0, lmax=1, qext_list=c3pt_qext_list),
        flav=flav_ins_list )

op_spec_list    = lhpd.dict_prod(
        dict(kind='op', qext_list=c3pt_qext_list), 
        flav=flav_ins_list, op=c3pt_op_list)


######################
flav_vi_list    = ['U', 'D']
gf_cedm_list    = ['orig']
execfile('config_qtopo.py')

vacins_spec_list_all = dict(
    theta   = lhpd.dict_prod(dict(kind='theta'), 
                gf=qtopo_tag_list, rcut=qtopo_rcut_list),
    volcedm = lhpd.dict_prod(dict(kind='volcedm'), 
                flav=flav_vi_list, gf=['orig']),
    volpsc  = lhpd.dict_prod(dict(kind='volpsc'), 
                flav=flav_vi_list)
)

data_dir_aff = 'data.in/data.prod.coh4X.all-ln'
data_out    = 'data.out/data.prod.coh4X.ex4sl256'
execfile('config_lcdb.py')
