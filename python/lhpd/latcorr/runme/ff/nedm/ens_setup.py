from __future__ import print_function
import math
import numpy as np
import lhpd


class srcsnkpair :
    def __init__(self, tsep, sm_srcsnk) :
        self.tsep       = tsep
        self.sm_srcsnk     = sm_srcsnk
    def __hash__(self) : 
        return (hash(self.tsep) 
                ^ hash(self.sm_srcsnk))
    def __eq__(self, oth) :
        return (self.tsep == oth.tsep 
                and self.sm_srcsnk == oth.sm_srcsnk)
    def __ne__(self, oth) :
        return not self.__eq__(oth)
    # since srcsnkpair has tsep, using it as key for c2pt map will result in 
    # 2pt functions stored len(tsep_list) times; fix: add special value specifically 
    # for that; when building c2pt map, check if it has already been inserted
    def key2pt(self) : 
        return srcsnkpair(0, self.sm_srcsnk)
    def __str__(self) :
        return '%s(T=%d)' % (self.sm_srcsnk, self.tsep)
    def __repr__(self): return self.__str__() 
    # TODO add str repr
    def get_tsep(self)      : return self.tsep

class mc_case :
    tpol_map = {
        '3' : lhpd.latcorr.tpol_map['posSzplus'],
        # TODO add others
    }

    def __init__(self, p3src, p3snk, tpol, tpol_c2, tpol5, tpol5_c2) :
        self.p3src      = p3src
        self.p3snk      = p3snk
        self.tpol       = tpol
        self.tpol_c2    = tpol_c2
        self.tpol5      = tpol5
        self.tpol5_c2   = tpol5_c2
    def __hash__(self) :
        return (hash(str(self.tpol)) 
                ^ hash(tuple(self.p3src)) 
                ^ hash(tuple(self.p3snk)))
    def __eq__(self, oth) :
        return (self.tpol == oth.tpol
                and tuple(self.p3src) == tuple(oth.p3src)
                and tuple(self.p3snk) == tuple(oth.p3snk))
    def __ne__(self, oth) :
        return not self.__eq__(oth)
    def __str__(self) :
        return '%s<%s|%s>' % (self.tpol, self.p3snk, self.p3src)
    def __repr__(self): return self.__str__() 
    def get_p3src(self)     : return self.p3src
    def get_p3snk(self)     : return self.p3snk
    def get_tpol(self)      : return self.tpol
    def get_tpol_c2(self)   : return self.tpol_c2
    def get_tpol5(self)     : return self.tpol5
    def get_tpol5_c2(self)  : return self.tpol5_c2

    def get_tpol_matr(self)     : return lhpd.latcorr.get_tpol_matr(self.get_tpol())
    def get_tpol_c2_matr(self)  : return lhpd.latcorr.get_tpol_matr(self.get_tpol_c2())
    def get_tpol5_matr(self)    : return lhpd.latcorr.get_tpol_matr(self.get_tpol5())
    def get_tpol5_c2_matr(self) : return lhpd.latcorr.get_tpol_matr(self.get_tpol5_c2())


""" examples
gg=ens_data_get(ama='ub', had='proton', flav='U-D', op='tensor1', data_top='.')

# bxp10, bym10
ssgrp=[ srcsnkpair(dt, 'bxp10', 'bym10', 'GN2x50') for dt in np.r_[8:13] ]
mcgrp=[ mc_case([k,0,0],[0,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]
# bxp10, bxm10
ssgrp=[ srcsnkpair(dt, 'bxp10', 'bxm10', 'GN2x50') for dt in np.r_[8:13] ]
mcgrp=[ mc_case([k,0,0],[-3,0,0], 'AApSzplus') for k in np.r_[-3:5] ]

# bqp10, brm10
ssgrp=[ srcsnkpair(dt, 'bqp10', 'brm10', 'GN2x25') for dt in np.r_[6:11] ]
mcgrp=[ mc_case([k,k,0],[3,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]
# bqp10, bqm10
ssgrp=[ srcsnkpair(dt, 'bqp10', 'bqm10', 'GN2x25') for dt in np.r_[6:11] ]
mcgrp=[ mc_case([k,k,0],[-3,-3,0], 'AApSzplus') for k in np.r_[-3:5] ]
"""

class ens_data_get :
    """ 
        ss  = ( tsep[int], sm_srcsnk[str] )
        mc  = ( p3src[tuple?], p3snk[tuple?] }
    """
    def __init__(self, **kw) :
        self.ama        = kw["ama"]
        self.had        = kw["had"]
        self.flav       = kw["flav"]
        self.op         = kw["op"]
        self.data_top   = kw["data_top"]
        self.mlat       = kw.get('mlat')
        self.latsize    = kw.get('latsize', None)
        self.c3pt_contr = kw.get('c3pt_contr') or 'conn'
        self.c3pt_improv= kw.get('c3pt_improv') or '0imp'
        self.twopt_avg_rot = kw.get('twopt_avg_rot') or False
        self.data_all   = kw.get('data_all') or False   # unbinned?
        #self.c2fit_dgrp = kw.get("c2fit_dgrp")

        self.flav_cedm  = kw.get("flav_cedm")
        self.gf_cedm    = kw.get("gf_cedm")

        self.kw_        = dict(kw)

    ###########################################################################
    # misc
    ###########################################################################
    def str(self) :
        return str(self.kw)
    def str_f(self) : 
        raise NotImplementedError


    ###########################################################################
    # op
    ###########################################################################
    def get_threept_conn_flav_(self, flav, sm_srcsnk, tsep, 
            op, ir_name, p3src, p3snk, tpol) :
        p3src   = np.asarray(p3src)
        p3snk   = np.asarray(p3snk)
        q3ext   = p3snk - p3src
        def get_conn_flav(flav) :
            if self.data_all :
                h5fname = get_op_all_h5_file(self.ama, p3snk, tsep, 
                        sm_srcsnk, self.had, tpol, flav, op, data_out=self.data_top)
            else :
                if self.ama in ['ex', 'sl'] :
                    h5fname = get_op_bin_h5_file(self.ama, p3snk, tsep, 
                            sm_srcsnk, self.had, tpol, flav, op, data_out=self.data_top)
                elif 'ub' == self.ama or 'unbias' == self.ama:
                    h5fname = get_op_unbias_h5_file(p3snk, tsep, 
                            sm_srcsnk, self.had, tpol, flav, op, data_out=self.data_top)
                else : raise ValueError(self.ama)

            h5f = h5py.File(h5fname, 'r')
            h5kpath = '/op/%s/%s_%s/%s_dt%d/%s/%s/%s' % (
                        sm_srcsnk, self.had, tpol, psnk_str_k(p3snk), tsep, 
                        flav, op, ir_name)
            h5d = h5f[h5kpath]
            #print("%s[%s]" % (h5fname, h5kpath))
            i_qext  = lhpd.np_find_first(q3ext, h5d.attrs['qext_list'])[0]
            res = h5d[:, :, i_qext]
            h5f.close()
            #print(repr(res.mean(0)[:,tsep//2]))
            return res
            
        return lhpd.latcorr.calc_flav_fromUD(flav, get_conn_flav)

    """
    def get_threept_disc_flav_(self, flav, sm_srcsnk, tsep,
            op, ir_name, p3src, p3snk, tpol) :
        # TODO update when (if?) have discos 
        p3src   = np.asarray(p3src)
        p3snk   = np.asarray(p3snk)
        q3ext   = p3snk - p3psrc
        def get_conn_flav(flav) :
            h5fname = get_opdisc_all_h5_file(self.ama, p3snk, op, bsm_srcsnk, data_out=self.data_top)
            h5kpath = "/opdisc/SS/%s/%s/%s/%s" % (
                        self.had, op, ir_name, psnk_str_k(p3snk))
            h5f = h5py.File(h5fname, 'r')
            h5d = h5f[h5kpath]
            n_tpol, n_tsep = h5d.shape[1:3]
            
            #print("%s[%s]" % (h5fname, h5kpath))
            assert((h5d.attrs['src_boost_mom'] == src_boost_mom).all())
            assert((h5d.attrs['snk_boost_mom'] == snk_boost_mom).all())
            assert(list(h5d.attrs['tsep_list']) == range(n_tsep))
            # check order of tsep and tpol
            assert(16 == n_tpol)
            assert(list(h5d.attrs['tpol_list']) == [ "Tg%d" % g for g in range(16)])
            
            i_qext  = lhpd.np_find_first(q3ext, h5d.attrs['qext_list'])[0]
            return lhpd.latcorr.calc_tpol_fromGamma(tpol, lambda g: h5d[:, g, tsep, :, i_qext])
        return lhpd.latcorr.calc_flav_fromUD(flav, get_conn_flav)
    """
        

    def get_threept_flav_(self, flav, sm_srcsnk, tsep,
            op, ir_name, p3src, p3snk, tpol, c3pt_contr) :
        if 'conn' == c3pt_contr : 
            return self.get_threept_conn_flav_(flav, sm_srcsnk, 
                    tsep, op, ir_name, p3src, p3snk, tpol)
        elif 'disc' == c3pt_contr :
            return self.get_threept_disc_flav_(flav, sm_srcsnk, 
                    tsep, op, ir_name, p3src, p3snk, tpol)
        elif 'full' == c3pt_contr :
            return (  self.get_threept_conn_flav_(flav, sm_srcsnk, 
                                tsep, op, ir_name, p3src, p3snk, tpol)
                    + self.get_threept_disc_flav_(flav, sm_srcsnk,
                                        tsep, op, ir_name, p3src, p3snk, tpol))
        else : raise ValueError(c3pt_contr)

    def get_threept_1imp_flav_(self, flav, sm_srcsnk, tsep,
            op, ir_name, p3src, p3snk, tpol, c3pt_contr) :
        p4src   = lhpd.latcorr.lorentz_p4(self.mlat, self.latsize, p3src) * [1,1,1,1j]
        p4snk   = lhpd.latcorr.lorentz_p4(self.mlat, self.latsize, p3snk) * [1,1,1,1j]
        q4ext   = p4snk - p4src
        #print(repr(q4ext))
        res     = None
        if 'tensor1' == op :
            sig_comp= self.get_threept_flav_(flav, sm_srcsnk, tsep,
                        'sigma2a2', 'H4_T2_d6r1', p3src, p3snk, tpol, c3pt_contr)

            xy,xz,xt,yz,yt,zt = tuple([ sig_comp[:,i] for i in range(6) ])
            n_data  = xy.shape[0]
            sig_munu= [ [  0, xy, xz, xt ], 
                        [-xy,  0, yz, yt ],
                        [-xz,-yz,  0, zt ],
                        [-xt,-yt,-zt,  0 ] ]
            res     = np.zeros((n_data,4) + xy.shape[1:], np.complex128)
            for mu in range(4) :
                for nu in range(4) :
                    res[:,mu] += q4ext[nu]*sig_munu[mu][nu]


            #print(repr(res.mean(0)[:,tsep//2]))
            #res *= 1j ; print("WARNING: correcting OLD T=qbar(-1j*sigma)q = (-1j)T : return New=1j*data")
            print("WARNING: assuming NEW T=qbar(sigma)q in file : NOT correcting")
            return res
        else : raise NotImplementedError
        assert(not None is res)
        #return res


    def get_threept(self, mc, ss, op, ir_name) :
        if   '0imp' == self.c3pt_improv : 
            return self.get_threept_flav_(
                        self.flav, ss.sm_srcsnk, ss.tsep,
                        op, ir_name, mc.p3src, mc.p3snk, mc.tpol, self.c3pt_contr)
        elif '1imp' == self.c3pt_improv :
            return self.get_threept_1imp_flav_(self.flav, 
                        ss.sm_srcsnk, ss.tsep,
                        op, ir_name, mc.p3src, mc.p3snk, mc.tpol, self.c3pt_contr)
        elif '01imp' == self.c3pt_improv :
            # TODO need to initialize self.c3pt_improv_coeff[op] map
            return (  self.get_threept_flav_(
                        self.flav, ss.sm_srcsnk, ss.tsep,
                        op, ir_name, mc.p3src, mc.p3snk, mc.tpol, self.c3pt_contr)
                    + self.c3pt_improv_coeff[op] * self.get_threept_1imp_flav_(
                        self.flav, ss.sm_srcsnk, ss.tsep,
                        op, ir_name, mc.p3src, mc.p3snk, mc.tpol, self.c3pt_contr))
        else : raise ValueError(self.c3pt_improv)
    
    ###########################################################################
    # op_volcedm
    ###########################################################################
    def get_threept_volcedm_conn_flav_(self, flav_cur, sm_srcsnk, tsep, 
            op, ir_name, p3src, p3snk, tpol, flav_cedm, gf_cedm) :
        p3src   = np.asarray(p3src)
        p3snk   = np.asarray(p3snk)
        q3ext   = p3snk - p3src
        def get_conn_flav_cur(flav_cur) :
            def get_conn_flav_cedm(flav_cedm) :
                if self.ama in ['ex', 'sl'] :
                    h5fname = get_op_volcedm_bin_h5_file(self.ama, p3snk, tsep, 
                            sm_srcsnk, self.had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=self.data_top)
                elif 'ub' == self.ama or 'unbias' == self.ama:
                    h5fname = get_op_volcedm_unbias_h5_file(p3snk, tsep, 
                            sm_srcsnk, self.had, tpol, flav_cur, op, flav_cedm, gf_cedm, data_out=self.data_top)

                h5f = h5py.File(h5fname, 'r')
                h5kpath = '/op_volcedm/%s/%s_%s/%s_dt%d/%s/%s/%s/%s/%s' % (
                            sm_srcsnk, self.had, tpol, psnk_str_k(p3snk), tsep, 
                            flav_cur, op, ir_name, flav_cedm, gf_cedm)
                h5d = h5f[h5kpath]
                #print("  <<< %s[%s]" % (h5fname, h5kpath))
                i_qext  = lhpd.np_find_first(q3ext, h5d.attrs['qext_list'])[0]
                res = h5d[:, :, i_qext]
                h5f.close()
                #print(repr(res.mean(0)[:,tsep//2]))
                return res
            return lhpd.latcorr.calc_flav_fromUD(flav_cedm, get_conn_flav_cedm)
            
        return lhpd.latcorr.calc_flav_fromUD(flav_cur, get_conn_flav_cur)

    def get_threept_volcedm(self, mc, ss, op, ir_name) :
        return self.get_threept_volcedm_conn_flav_(
                    self.flav, ss.sm_srcsnk, ss.tsep,
                    op, ir_name, mc.p3src, mc.p3snk, mc.tpol5, self.flav_cedm, self.gf_cedm)
        

    ###########################################################################
    # c2pt
    ###########################################################################
    def get_c2symm_p3src(self, mc, ss) : 
        """ trivial """
        return mc.p3src

    def get_c2symm_p3snk(self, mc, ss) :
        """ trivial """
        return mc.p3snk

    def get_twopt_symm_mom_tpol_(self, sm_srcsnk, p3, tpol, twopt_avg_rot=False) :
        p3              = np.asarray(p3)
        if self.data_all :
            h5f = h5py.File(get_c2pt_all_h5_file(self.ama, self.had, sm_srcsnk, data_out=self.data_top), 'r')
        else :
            if self.ama in ['ex', 'sl'] :
                h5f = h5py.File(get_c2pt_bin_h5_file(self.ama, self.had, sm_srcsnk, data_out=self.data_top), 'r')
            elif 'ub' == self.ama or 'unbias' == self.ama:
                h5f = h5py.File(get_c2pt_unbias_h5_file(self.had, sm_srcsnk, data_out=self.data_top), 'r')
            else : raise ValueError(self.ama)

        h5kpath = '/c2pt/%s/%s' % (sm_srcsnk, self.had)
        h5d     = h5f[h5kpath]

        psnk_list = h5d.attrs['psnk_list']
        #i_p     = lhpd.np_find_first(p3, h5d.attrs['psnk_list'])[0]
        #i_tpol  = lhpd.np_find_first(tpol, h5d.attrs['tpol_list'])[0]
        #def get_Tg(g) : return h5d[:, g, i_p]
        assert(["Tg%d" % g for g in range(16) ] == list(h5d.attrs['tpol_list']))
        def get_Tg(i_g) : 
            if twopt_avg_rot : 
                p3_std      = np.sort(np.abs(p3))
                p_std_list  = np.sort(np.abs(psnk_list), axis=1)
                i_p_list    = lhpd.np_find_all(p3_std, p_std_list)[0]
                return np.mean([ h5d[:, i_g, i_p] for i_p in i_p_list], axis=0)
            else :
                i_p     = lhpd.np_find_first(p3, h5d.attrs['psnk_list'])[0]
                return h5d[:, i_g, i_p]
        #return get_Tg(i_tpol)
        return lhpd.latcorr.calc_tpol_fromGamma(tpol, get_Tg)

    def get_twopt_src(self, mc, ss) :
        return self.get_twopt_symm_mom_tpol_(ss.sm_srcsnk, 
                    self.get_c2symm_p3src(mc, ss), mc.tpol_c2, 
                    twopt_avg_rot=self.twopt_avg_rot)
    def get_twopt_snk(self, mc, ss) :
        return self.get_twopt_symm_mom_tpol_(ss.sm_srcsnk, 
                    self.get_c2symm_p3snk(mc, ss), mc.tpol_c2,
                    twopt_avg_rot=self.twopt_avg_rot)

    """
    def get_twopt_fit_mom_tpol_(self, p3, tpol) :
        snk_str = "px%dpy%dpz%d" % tuple(p3)
        #h5f     = h5py.File(self.c2fit_file, 'r')
        #h5kpath = "/%s_%s/%s" % (self.had, tpol, snk_str)
        #h5d     = h5f[h5kpath]
        h5d     = self.c2fit_dgrp[snk_str]
        assert('c2pt_c0' == h5d.attrs['pname'][0])
        assert('c2pt_e0' == h5d.attrs['pname'][1])
        assert('c2pt_c1' == h5d.attrs['pname'][2])
        assert('c2pt_de1_0' == h5d.attrs['pname'][3])
        res     = h5d['p']
        #h5f.close()
        return res
    def get_twopt_fit_src(self, mc, ss) :
        return self.get_twopt_fit_mom_tpol_(mc.p3src, mc.tpol) 
    def get_twopt_fit_snk(self, mc, ss) :
        return self.get_twopt_fit_mom_tpol_(self.get_c2symm_p3snk(mc, ss), mc.tpol) 
    """
    ###########################################################################
    # c2pt_volcedm
    ###########################################################################
    def get_twopt_volcedm_flav_mom_tpol_(self, sm_srcsnk, p3, tpol, flav_cedm, gf_cedm) :
        p3      = np.asarray(p3)
        def get_flav_cedm(flav_cedm) :
            if self.ama in ['ex', 'sl'] :
                h5f = h5py.File(get_c2pt_volcedm_bin_h5_file(self.ama, self.had, sm_srcsnk, 
                            flav_cedm, gf_cedm, data_out=self.data_top), 'r')
            elif 'ub' == self.ama or 'unbias' == self.ama:
                h5f = h5py.File(get_c2pt_volcedm_unbias_h5_file(self.had, sm_srcsnk, 
                            flav_cedm, gf_cedm, data_out=self.data_top), 'r')
            h5kpath = '/c2pt_volcedm/%s/%s/%s/%s' % (sm_srcsnk, self.had, flav_cedm, gf_cedm)
            h5d     = h5f[h5kpath]
            i_p     = lhpd.np_find_first(p3, h5d.attrs['psnk_list'])[0]
            def get_Tg(g) : return h5d[:, g, i_p]
            return lhpd.latcorr.calc_tpol_fromGamma(tpol, get_Tg)
        return lhpd.latcorr.calc_flav_fromUD(flav_cedm, get_flav_cedm)

    def get_twopt_volcedm_src(self, mc, ss) :
        return self.get_twopt_volcedm_flav_mom_tpol_(ss.sm_srcsnk, 
                    self.get_c2symm_p3src(mc, ss), mc.tpol5_c2, self.flav_cedm, self.gf_cedm)
    def get_twopt_volcedm_snk(self, mc, ss) :
        return self.get_twopt_volcedm_flav_mom_tpol_(ss.sm_srcsnk, 
                    self.get_c2symm_p3snk(mc, ss), mc.tpol5_c2, self.flav_cedm, self.gf_cedm)


    def get_alfive_flav_(self, sm_srcsnk, flav_cedm) : 
        h5f     = h5py.File('%s/al5.%s.%s.%s.h5' % (self.data_top, self.ama, self.had, sm_srcsnk), 'r')
        def get_flav_cedm(fl) : return h5f['/alfive/%s/%s/%s/alfive' % (sm_srcsnk, self.had, fl)].value
        res = lhpd.latcorr.calc_flav_fromUD(flav_cedm, get_flav_cedm)
        h5f.close()
        return res
    
    def get_alfive(self, ss) :
        return self.get_alfive_flav_(ss.sm_srcsnk, self.flav_cedm)
    
    def get_ff_flav_method_(self, q2, sm_srcsnk, tsep, op, flav, method) :
        if not method in ['ratio_pltx'] : raise NotImplementedError
        h5f     = h5py.File('%s/ff.%s.%s.%s.h5' % (self.data_top, self.ama, self.had, sm_srcsnk), 'r')
        
        def get_flav(flav):
            h5d     = h5f['/%s/%s/%s/%s/dt%d' % (self.had, op, flav, method, tsep)]
            q2_list = h5d.attrs['q2_list'] 
            i_q2_list = np.where(np.abs(q2_list - q2) < 1e-8)[0]
            assert(1 == len(i_q2_list))
            i_q2 = i_q2_list[0]
            return h5d[:,:,i_q2]
        res = lhpd.latcorr.calc_flav_fromUD(flav, get_flav)
        h5f.close()              
        return res  
                                     
    def get_ff(self, q2, ss, op, method='ratio_pltx') :
        return self.get_ff_flav_method_(q2, ss.sm_srcsnk, ss.tsep, op, self.flav, method)
 
