import numpy as np
import lhpd
from lhpd.val_err import val_err

from lhpd.latcorr import gamma_dot_mom_euc, lorentz_p4_mink

def pdecay_N2Pi_ff_coeff(mN, mMes, latsize, p3src, p3snk, tpol) :
    """ compute Tr[ Tpol . (W0,W1-vertex) . NucPol] / (2(eN+mN))
        tpol    polarization (inc. chi2)
    """
    p4src   = lorentz_p4_mink(mN,  latsize, p3src)
    eN      = p4src[-1]
    p4snk   = lorentz_p4_mink(mMes, latsize, p3snk)
    ePi     = p4snk[-1]
    q4ext   = p4src - p4snk # outgoing lepton
    q4slash = gamma_dot_mom_euc(q4ext)
    nuc_pol = (mN*gamma_id - 1j*gamma_dot_mom_euc(p4src)) / 2. / (eN + mN)
    cW0     = lhpd.np_ddot(tpol, nuc_pol).trace()
    cW1     = lhpd.np_ddot(tpol, (-1j/mN)*q4slash, nuc_pol).trace()
    return np.r_[cW0, cW1]


def pdecay_N2Pi_ff_coeff_explicit(mN, mMes, latsize, p3src, p3snk, chiFact) :
    """
        chiFact     R=1, L=1, avg=0 (irrelevant)
        result:  [[cW0_pos,cW1_pos], [cW0_pos_iqg, cW1_pos_iqg]]
    """
    p4src   = lorentz_p4_mink(mN,  latsize, p3src)
    eN      = p4src[-1]
    p4snk   = lorentz_p4_mink(mMes, latsize, p3snk)
    ePi     = p4snk[-1]
    dE      = eN - ePi
    q4ext   = p4src - p4snk # outgoing lepton
    qvec2   = (q4ext[:-1]**2).sum(-1)
    k_dot_q = (p4src[:-1] * q4ext[:-1]).sum(-1)
    #q_x_k   = np.cross(q4ext[:-1], p4src[:-1])
    cW0_pos     = 0.5
    cW1_pos     = dE / (2 * mN) - k_dot_q / (2 * mN * (eN + mN))
    cW0_pos_iqg = k_dot_q / (2 * (eN + mN))
    cW1_pos_iqg = qvec2 / (2 * mN) - dE * k_dot_q / (2 * mN * (eN + mN))
    return np.r_[ [ [ cW0_pos, cW1_pos ], [ cW0_pos_iqg, cW1_pos_iqg ]]]


def pdecay_get_meson(op_X) :
    if   op_X[:2] in [ 'S1', 'S2', 'S3', 'S4' ] : return 'meson_sl'
    elif op_X[:2] in [ 'U0', 'U1' ] : return 'meson_ll'
    elif op_X[:2] in [ 'E0' ] : return 'meson_ss'
    else : raise ValueError(op_X)

    
def get_op_text(op_X, op_chi12) :
    chi12 = (op_chi12[0], op_chi12[1])
    if   'S1' == op_X : return r'$\langle K^0|(us)_%s u_%s|p\rangle$' % chi12
    elif 'S2' == op_X : return r'$\langle K^+|(us)_%s d_%s|p\rangle$' % chi12
    elif 'S3' == op_X : return r'$\langle K^+|(ud)_%s s_%s|p\rangle$' % chi12
    elif 'S4' == op_X : return r'$\langle K^+|(ds)_%s u_%s|p\rangle$' % chi12
    elif 'U1' == op_X : return r'$\langle \pi^+|(ud)_%s d_%s|p\rangle$' % chi12
    elif 'U0' == op_X : return r'$\langle \pi^0|(ud)_%s u_%s|p\rangle$' % chi12
    elif 'E0' == op_X : return r'$\langle \eta |(ud)_%s u_%s|p\rangle$' % chi12
    else : raise ValueError(op_X)


#def pdecay_c3fit_ff_save(
	#h5g_ff, ax_W0_W1, #output
        #gg,
        #mN, mMes_ch,
        #psrc_psnk_list, op_X_chi12_list,
        #tsep_pltx_list,
        #op_Zfact,
        #rsplan,
#
        #c3pt_avg_chi    = False,
        #tskip_c3fit_lo  = None, 
        #tskip_c3fit_hi  = None,
        #t_pltx_r        = None,
        #ncov_shrink     = 0,
        #ncov_tol        = 1e-5,
        #ainv            = None,
        #) :
