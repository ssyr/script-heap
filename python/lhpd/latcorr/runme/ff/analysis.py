from __future__ import print_function
from past.builtins import execfile
import numpy as np
import h5py
import math

import lhpd
from lhpd.misc.numpy_extra import *
import matplotlib as mpl
#mpl.use('Agg')       # don't ask for X
import matplotlib.pyplot as plt
import dataview as dv



###############################################################################
# functions for binning, resampling and, computing averages

def bin_ckpoint(a, sample_desc, cmp=None) :
    """ sort by config number and bin """
    cfg_list = []
    for s in sample_desc :
        if not s['ckpoint_id'] in cfg_list: 
            cfg_list.append(s['ckpoint_id'])

    # sort if asked to ; note that 'ckpoint_id' has type'str' for generality
    # for proper sorting of RBC/LHP project, must use  
    #       cmp = lambda x, y : cmp(int(x), int(y))
    if not None is  cmp : 
        cfg_list.sort(cmp=cmp)

    n_cfg = len(cfg_list)
    res_shape= (n_cfg,) + a.shape[1:]
    res = np.empty(res_shape, a.dtype)
    
    for i_c, c in enumerate(cfg_list) :
        # select indices in sample_desc for config `c' :
        slice_c = np_filter_arg(lambda x : x['ckpoint_id'] == c, sample_desc)
        # ... and put into bin `i_c' :
        res[i_c] = a[slice_c].mean(axis=0)
    return res

def bin_data(a, binsize=1) :
    """ bin adjacent data points """
    assert 0 < binsize
    n_data  = a.shape[0]
    n_bin   = n_data / binsize
    if 0 != n_data % binsize :
        print("Warning: ignore the last %d samples due to binning" % (
                    n_data % binsize,))
    
    res_shape   = (n_bin,) + a.shape[1:]
    res = np.empty(res_shape, a.dtype)
    for i_bin in range(n_bin) : 
        res[i_bin] = a[binsize * i_bin : binsize * (i_bin+1)].mean(axis=0)
    return res
        
def calc_ama_bias(a_exact, sample_desc_exact, 
                  a_apprx, sample_desc_apprx):
    """ bin and correct approx samples 
        sample_desc_exact, sample_desc_apprx
            list of sample descriptions [ (gauge_spec, ...), ... ]
            * bin all samples with the same gauge_spec
            * compute bias correction by comparing exact and apprx
              with _exactly_ the same sample_desc
    
    """
    return a_exact - a_apprx[np_subindex(sample_desc_exact, sample_desc_apprx)]

def make_ama_dataset(
            a_exact, sample_desc_exact,
            a_apprx, sample_desc_apprx) :
    a_bias = bin_ckpoint(
                    calc_ama_bias(
                            a_exact, sample_desc_exact,
                            a_apprx, sample_desc_apprx),
                    sample_desc_exact)
    a_apprx_bin = bin_ckpoint(a_apprx, sample_desc_apprx)
    return a_apprx_bin + a_bias

# XXX use this func if you have troubles with automatic matching exact <-> approx
def make_ama_dataset_simple(a_exact, n_exact, a_apprx, n_apprx, map_exact_apprx) :
    """ assume that a_exact and a_apprx are ordered
        a_exact     array of exact samples
        n_exact     exact samples per config
        a_apprx     array of approx samples
        n_apprx     approx samples per config
        map_exact_apprx
                    for AMA, match a_exact <-> a_apprx[map_exact_apprx]

        E.g. for 5 cfg, 32 apprx + 1 exact samples per cfg, 
            exact samples matched to the first approx samples on each cfg, use
        make_ama_dataset_simple(a_exact, 1, a_apprx, 32, np.r_[0: 5 * 32 : 32])
        
    """
    
    a_apprx_bin = bin_data(a_apprx, n_apprx)
    a_bias      = a_exact - a_apprx[map_exact_apprx]
    a_bias_bin  = bin_data(a_bias, n_exact)
    return a_apprx_bin + a_bias_bin

#def resample_jk(a) :
    #""" jackknife resample along the 0-axis """
    #n_data = a.shape[0]     # number of samples
    #assert (1 < n_data)     
    #a_sum = a.sum(axis=0)        # sum over samples
    ## `a_sum' is broadcast over the first axis of `a'
    #return (a_sum[None, ...] - a) / (n_data - 1) 
#
#def calc_avg(a) :
    #""" compute average over the 0-th axis """
    #return a.mean(axis=0)
#
#def calc_err_jk(a) :
    #""" compute STDEV of 'a' assuming it was jackknife-resampled """
    #n_data = a.shape[0]
    #return a.var(axis=0) * (n_data - 1)





###############################################################################
# DATA import functions


def get_dlist(dset) : 
    """ get data list for dataset `dset' 
        return : array (list) of sample descriptors
    """
    return dset.file[ dset.attrs['data_list'] ].value

###############################################################################
# ANALYSIS functions 
# everything on 1 configuration is binned together 


# global variables : the structure of the AMA ensemble 

def calc_mass_eff(had, rsplan=('jk', 1), approx_only=False) :
    c2pt_ex = get_c2pt_dset(ama_exact, had)
    c2pt_ap = get_c2pt_dset(ama_apprx, had)

    i_p0    = np_find_first([0,0,0], c2pt_ex.attrs['psnk_list'])[0]
    assert (c2pt_ex.attrs['psnk_list'][i_p0] == [0,0,0]).all()

    if approx_only :
        c2pt_x = c2pt_ap[:, i_p0]
    else :
        c2pt_x = make_ama_dataset_simple(c2pt_ex[:, i_p0].real, n_ex_cfg,
                                         c2pt_ap[:, i_p0].real, n_ap_cfg,
                                         match_ex_ap)
    c2pt_rs = lhpd.resample(c2pt_x, rsplan)

    lt = c2pt_rs.shape[-1]
    dt = 1
    trange = np.r_[: lt - dt]
    meff_rs= np.log(np.abs(c2pt_rs[:, trange] / c2pt_rs[:, trange + dt]))

    return lhpd.calc_avg_err(meff_rs, rsplan)

def plot_edotline(plot_param, func, *args, **kwargs) :
    y_avg, y_err = func(*args, **kwargs)
    
    if None is plot_param :
        x_r, ax, stg, label = (None) * 4
    else :
        x_r, ax, stg, label = plot_param

    if None is x_r : x_r = np.r_[ : y_avg.shape[0] ]
    if None is stg: stg = dv.style_group_default[0]
    if None is ax : ax = dv.make_std_axes()
    ax.errorbar(x_r, y_avg, yerr=y_err, label=label, **stg.edotline())

    if not None is  label :
        ax.legend()
    return ax


def calc_energy_eff(had, p2, trange=None, dt=1, rsplan=('jk', 1), approx_only=False) :
    """ more complicated example : averaging over non-zero momenta """

    # get data handlers
    c2pt_ex = get_c2pt_dset(ama_exact, had)
    c2pt_ap = get_c2pt_dset(ama_apprx, had)
    
    # find equivalent momenta
    i_mom_list = np_filter_arg(lambda p : (p**2).sum() == p2, 
                               c2pt_ex.attrs['psnk_list'] )
    
    # NOTE : axes are ['i_data', 'i_psnk', 'i_t'] ( ==c2pt_XX.attrs['dim_spec'] )
    lt      = c2pt_ex.shape[-1]
    if None is trange : trange = np.r_[0: lt - dt]
    trange = np.asarray(trange)
    assert ( trange + dt < lt ).all()

    # read data & average over equivalent momenta
    c2pt_ap_p2 = np.mean([ c2pt_ap[:, i].real for i in i_mom_list ], axis=0)
    if approx_only :
        c2pt_p2 = c2pt_ap_p2
    else:
        c2pt_ex_p2 = np.mean([ c2pt_ex[:, i].real for i in i_mom_list ], axis=0)
        c2pt_p2 = make_ama_dataset_simple(c2pt_ex_p2, n_ex_cfg,
                                          c2pt_ap_p2, n_ap_cfg,
                                          match_ex_ap)
    c2pt_p2_rs = lhpd.resample(c2pt_p2, rsplan)

    # compute Eeff (`abs' is to avoid problems with log(x < 0) )
    eeff_rs = np.log(np.abs(c2pt_p2_rs[:, trange] / c2pt_p2_rs[:, trange + dt]))
    return lhpd.calc_avg_err(eeff_rs, rsplan)


def runme_plot_energy_eff(had, psnk_list, trange, ax=None, approx_only=False) :
    if None is ax : ax = dv.make_std_axes()
    d_tr = .4 / max(len(psnk_list) - 1, 1)
    for i_psnk, psnk in enumerate(psnk_list) :
        plot_edotline( ( d_tr * i_psnk + trange, ax, dv.style_group_default[i_psnk], 
                         'p=%s' % str(psnk) ),
                       calc_energy_eff, had, (np.asarray(psnk)**2).sum(), 
                       trange=trange, approx_only=approx_only)
    ax.set_ylim([0., 1.5])
    ax.set_xlabel(r'$t$')
    ax.set_ylabel(r'$E_{eff}(t)$')

    return ax
""" example
ax=runme_plot_energy_eff('proton_1', [[0,0,0],[0,0,1],[0,1,1],[1,1,1]], np.r_[0:16], ax=dv.class_logger(dv.make_std_axes()), approx_only=True)
"""

# TODO general outline for analysis code
#   * for each ama_mode:
#     + get `data_list' arrays (separately for all records)
#     + compute intersection
#     + sort in a particular order
#   * for each dataset:
#     + find indices to read from HDF5; report error if missing
#     + read data?
#   * match 'data_list' arrays between ama_modes
#   * calc bias_corrected/etc
#   * pass aggregated data to computing functions
def calc_ga2gv_proton1_p000(tsep, rsplan=('jk', 1), approx_only=False):
    """ example : computing gA/gV ratio 
    """
    # gA, gV are determined from these correlators :
    had = 'proton_1'
    psnk= [0,0,0]

    # get 3pt data handlers
    h5_bb_u_ex   = get_bb_dset(ama_exact, had, 'U', psnk, tsep)
    h5_bb_d_ex   = get_bb_dset(ama_exact, had, 'D', psnk, tsep)
    assert(h5_bb_u_ex.shape[0] == n_cfg * n_ex_cfg)
    assert(h5_bb_u_ex.shape == h5_bb_d_ex.shape)
    assert( (get_dlist(h5_bb_u_ex) == get_dlist(h5_bb_d_ex)).all() ) # FIXME find intersection

    h5_bb_u_ap   = get_bb_dset(ama_apprx, had, 'U', psnk, tsep)
    h5_bb_d_ap   = get_bb_dset(ama_apprx, had, 'D', psnk, tsep)
    assert(h5_bb_u_ap.shape[0] == n_cfg * n_ap_cfg)
    assert(h5_bb_u_ap.shape == h5_bb_d_ap.shape)
    assert( (get_dlist(h5_bb_u_ap) == get_dlist(h5_bb_d_ap)).all() ) # FIXME find intersection

    # FIXME `match_ex_ap' should be computed
    assert( (get_dlist(h5_bb_u_ex) == get_dlist(h5_bb_u_ap)[match_ex_ap]).all() )
    
    # getting indices of operators
    # FIXME should be done separately for all datasets
    i_q0    = np_find_first([0,0,0], h5_bb_u_ex.attrs['qext_list'])[0]
    i_l0    = np_find_first('l0_', h5_bb_u_ex.attrs['linkpath_list'])[0]
    i_g_gV  = np_find_first(8, h5_bb_u_ex.attrs['gamma_list'])[0]       # \gamma_4
    i_g_gA  = np_find_first(11, h5_bb_u_ex.attrs['gamma_list'])[0]      # \gamma_3\gamma_5

    # read & resample data
    # NOTE axes are  ['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau'] ( ==bb_XX_YY.attrs['dim_spec'])
    bb_gA_ap= (h5_bb_u_ap[:, i_g_gA, i_l0, i_q0] - h5_bb_d_ap[:, i_g_gA, i_l0, i_q0]).imag
    bb_gV_ap= (h5_bb_u_ap[:, i_g_gV, i_l0, i_q0] - h5_bb_d_ap[:, i_g_gV, i_l0, i_q0]).real
    if approx_only :
        bb_gA_rs= lhpd.resample(bb_gA_ap, rsplan)
        bb_gV_rs= lhpd.resample(bb_gV_ap, rsplan)
    else : 
        bb_gA_ex= (h5_bb_u_ex[:, i_g_gA, i_l0, i_q0] - h5_bb_d_ex[:, i_g_gA, i_l0, i_q0]).imag
        bb_gA_rs= lhpd.resample(make_ama_dataset_simple(bb_gA_ex, n_ex_cfg, 
                                                        bb_gA_ap, n_ap_cfg, 
                                                        match_ex_ap),
                                rsplan)
        bb_gV_ex= (h5_bb_u_ex[:, i_g_gV, i_l0, i_q0] - h5_bb_d_ex[:, i_g_gV, i_l0, i_q0]).real
        bb_gV_rs= lhpd.resample(make_ama_dataset_simple(bb_gV_ex, n_ex_cfg, 
                                                        bb_gV_ap, n_ap_cfg, 
                                                        match_ex_ap), 
                                rsplan)

    # compute gA/gV, average, estimate errors
    ga2gv_rs= bb_gA_rs / bb_gV_rs

    return lhpd.calc_avg_err(ga2gv_rs, rsplan)


def runme_show_ga2gv(tsep_list, ax=None, approx_only=False) :
    if None is ax : ax = dv.make_std_axes()
    d_tr = .4 / max(len(tsep_list) - 1, 1)
    for i, tsep in enumerate(tsep_list) :
        trange = np.r_[ : tsep + 1]
        plot_edotline( ( d_tr * i + trange, ax, dv.style_group_default[i], 
                         'T=%d' % tsep ),
                       calc_ga2gv_proton1_p000, tsep, approx_only=approx_only)
    ax.set_ylim([0., 2.])
    ax.set_xlabel(r'$\tau$')
    ax.set_ylabel(r'$g_A / g_V $')

    return ax
"""
ax=runme_show_ga2gv([8,9,10,12], ax=dv.class_logger(dv.make_std_axes()), approx_only=True)
"""
     
def calc_rEv2_proton1_p000(tsep, rsplan=('jk', 1), approx_only=False) :
    """ compute rEv2[lat.units] with a linear Q2 assumtion
        rEv2 = (6/Q2min) * (1 - <{000}|V4|{001}> / <{000}|V4|{000}>  
        return plateau vs tau
    """
    had     = 'proton_1'
    psnk    = [0,0,0]
    q1_sq   = 1             # off-forward mom.squared to average over


    # get 3pt data handlers
    # get 3pt data handlers
    h5_bb_u_ex   = get_bb_dset(ama_exact, had, 'U', psnk, tsep)
    h5_bb_d_ex   = get_bb_dset(ama_exact, had, 'D', psnk, tsep)
    assert(h5_bb_u_ex.shape[0] == n_cfg * n_ex_cfg)
    assert(h5_bb_u_ex.shape == h5_bb_d_ex.shape)
    assert( (get_dlist(h5_bb_u_ex) == get_dlist(h5_bb_d_ex)).all() ) # FIXME find intersection

    h5_bb_u_ap   = get_bb_dset(ama_apprx, had, 'U', psnk, tsep)
    h5_bb_d_ap   = get_bb_dset(ama_apprx, had, 'D', psnk, tsep)
    assert(h5_bb_u_ap.shape[0] == n_cfg * n_ap_cfg)
    assert(h5_bb_u_ap.shape == h5_bb_d_ap.shape)
    assert( (get_dlist(h5_bb_u_ap) == get_dlist(h5_bb_d_ap)).all() ) # FIXME find intersection

    # FIXME `match_ex_ap' should be computed
    assert( (get_dlist(h5_bb_u_ex) == get_dlist(h5_bb_u_ap)[match_ex_ap]).all() )

    # getting indices of operators
    # FIXME should be done separately for all datasets
    i_l0    = np_find_first('l0_', h5_bb_u_ex.attrs['linkpath_list'])[0]
    i_g_gV4 = np_find_first(8, h5_bb_u_ex.attrs['gamma_list'])[0]       # \gamma_4
    i_q0    = np_find_first([0,0,0], h5_bb_u_ex.attrs['qext_list'])[0]
    # find non-zero qext : all off-forward matr.el with fixed mom.sq
    i_q1_list = np_filter_arg(lambda q : (q**2).sum() == q1_sq,
                               h5_bb_u_ex.attrs['qext_list'] )
    print('q2=%d : %d :\n%s' % (q1_sq, len(i_q1_list), 
                                h5_bb_u_ex.attrs['qext_list'][i_q1_list]))

    # read & resample data
    # NOTE axes are  ['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau'] ( ==bb_XX_YY.attrs['dim_spec'])
    gV4_q0_ap = (  h5_bb_u_ap[:, i_g_gV4, i_l0, i_q0] 
                 - h5_bb_d_ap[:, i_g_gV4, i_l0, i_q0]).real
    gV4_q1_ap = np.mean([ (  h5_bb_u_ap[:, i_g_gV4, i_l0, i_q1] 
                           - h5_bb_d_ap[:, i_g_gV4, i_l0, i_q1]).real
                            for i_q1 in i_q1_list], axis=0)
    if approx_only :
        gV4_q0_x = gV4_q0_ap
        gV4_q1_x = gV4_q1_ap
    else :
        gV4_q0_ex = (  h5_bb_u_ex[:, i_g_gV4, i_l0, i_q0] 
                     - h5_bb_d_ex[:, i_g_gV4, i_l0, i_q0]).real
        gV4_q0_x = make_ama_dataset_simple(gV4_q0_ex, n_ex_cfg,
                                           gV4_q0_ap, n_ap_cfg,
                                           match_ex_ap)

        gV4_q1_ex = np.mean([ (  h5_bb_u_ex[:, i_g_gV4, i_l0, i_q1] 
                               - h5_bb_d_ex[:, i_g_gV4, i_l0, i_q1]).real
                              for i_q1 in i_q1_list], axis=0)
        gV4_q1_x = make_ama_dataset_simple(gV4_q1_ex, n_ex_cfg,
                                           gV4_q1_ap, n_ap_cfg,
                                           match_ex_ap)

    gV4_q0_rs   = lhpd.resample(gV4_q0_x, rsplan)
    gV4_q1_rs   = lhpd.resample(gV4_q1_x, rsplan)
    
    rEv2_rs = (( 6. / q1_sq / (2*math.pi / latsize[0])**2 ) 
                * (1. - gV4_q1_rs / gV4_q0_rs))

    return lhpd.calc_avg_err(rEv2_rs, rsplan)

def runme_show_rEv2(tsep_list, ax=None, approx_only=False) :
    if None is ax : ax = dv.make_std_axes()
    d_tr = .4 / max(len(tsep_list) - 1, 1)
    for i, tsep in enumerate(tsep_list) :
        trange = np.r_[ : tsep + 1]
        plot_edotline( ( d_tr * i + trange, ax, dv.style_group_default[i], 
                         'T=%d' % tsep ),
                       calc_rEv2_proton1_p000, tsep, approx_only=approx_only)
    ax.set_ylim([0., 200.])
    ax.set_xlabel(r'$\tau$')
    ax.set_ylabel(r'$(r_E^2)^{p-n} \, \mathrm{[lat]} $')

    return ax

def get_c2pt_avg(h5_c2pt, p_list, re_im=None) :
    i_p_list    = [ x[0] for x in np_find_first_list(p_list, 
                                h5_c2pt.attrs['psnk_list']) ]
    if not None is  re_im : 
        if 0 == re_im :
            return np.mean([ h5_c2pt[:, i_p].real for i_p in i_p_list ], axis=0)
        elif 1 == re_im :
            return np.mean([ h5_c2pt[:, i_p].imag for i_p in i_p_list ], axis=0)
    else :
        return np.mean([ h5_c2pt[:, i_p] for i_p in i_p_list ], axis=0)

def get_bb_avg(h5_bb, gamma, lpath_str, q_list, re_im=None) :
    i_l = np_find_first(lpath_str, h5_bb.attrs['linkpath_list'])[0]
    i_g = np_find_first(gamma, h5_bb.attrs['gamma_list'])[0]     
    i_q_list= [ x[0] for x in np_find_first_list(q_list, h5_bb.attrs['qext_list']) ]
    # ['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau'] ( ==bb_XX_YY.attrs['dim_spec'])
    if not None is  re_im :
        if 0 == re_im : 
            return np.mean([ h5_bb[:, i_g, i_l, i_q].real for i_q in i_q_list ], axis=0)
        elif 1 == re_im :
            return np.mean([ h5_bb[:, i_g, i_l, i_q].imag for i_q in i_q_list ], axis=0)
        else : raise ValueError
    else :
        return np.mean([ h5_bb[:, i_g, i_l, i_q] for i_q in i_q_list ], axis=0)

    

def calc_general_pltx(tsep, had, psnk, flav, qext_list, lpath_str, 
                     gamma, re_im,
                     rsplan=('jk', 1), approx_only=False) :
    """ compute rEv2[lat.units] with a linear Q2 assumtion
        rEv2 = (6/Q2min) * (1 - <{000}|V4|{001}> / <{000}|V4|{000}>  
        return plateau vs tau
    """
    had     = 'proton_1'
    q1_sq   = 1             # off-forward mom.squared to average over


    h5_c2pt_ex  = get_c2pt_dset(ama_exact, had)
    h5_c2pt_ap  = get_c2pt_dset(ama_apprx, had)
    
    c2_snk_ex   = get_c2pt_avg(h5_c2pt_ex, [psnk], re_im=0)
    c2_snk_ap   = get_c2pt_avg(h5_c2pt_ap, [psnk], re_im=0)
    psrc_list   = np.asarray(psnk) - np.asarray(qext_list) 
    c2_src_ex   = get_c2pt_avg(h5_c2pt_ex, psrc_list, re_im=0)
    c2_src_ap   = get_c2pt_avg(h5_c2pt_ap, psrc_list, re_im=0)


    def get_bb_avg_flav(h5_bb_d, flav, gamma, lpath_str, q_list, re_im=None) :
        if flav in ['U', 'D'] :
            return get_bb_avg_flav(h5_bb_d[flav], gamma, lpath_str, q_list, re_im=re_im)
        elif 'U-D' == flav :
            return ( get_bb_avg(h5_bb_d['U'], gamma, lpath_str, q_list, re_im=re_im)
                    -get_bb_avg(h5_bb_d['D'], gamma, lpath_str, q_list, re_im=re_im))
        elif 'U+D' == flav :
            return ( get_bb_avg(h5_bb_d['U'], gamma, lpath_str, q_list, re_im=re_im)
                    +get_bb_avg(h5_bb_d['D'], gamma, lpath_str, q_list, re_im=re_im))

    h5_bb_ex_d  = { 'U' : get_bb_dset(ama_exact, had, 'U', psnk, tsep),
                    'D' : get_bb_dset(ama_exact, had, 'D', psnk, tsep) }
    h5_bb_ap_d  = { 'U' : get_bb_dset(ama_apprx, had, 'U', psnk, tsep),
                    'D' : get_bb_dset(ama_apprx, had, 'D', psnk, tsep) }

    c3_ex     = get_bb_avg_flav(h5_bb_ex_d, flav, gamma, lpath_str, qext_list, re_im=re_im)
    c3_ap     = get_bb_avg_flav(h5_bb_ap_d, flav, gamma, lpath_str, qext_list, re_im=re_im)

    if approx_only :
        c2_snk_rs = lhpd.resample(c2_snk_ap, rsplan)
        c2_src_rs = lhpd.resample(c2_src_ap, rsplan)
        c3_rs     = lhpd.resample(c3_ap, rsplan)
    else :
        def get_ama_corr(x_ex, x_ap) :
            return make_ama_dataset_simple(x_ex, n_ex_cfg, 
                                x_ap, n_ap_cfg, match_ex_ap)
        c2_snk_rs = lhpd.resample(get_ama_corr(c2_snk_ex, c2_snk_ap), rsplan)
        c2_src_rs = lhpd.resample(get_ama_corr(c2_src_ex, c2_src_ap), rsplan)
        c3_rs     = lhpd.resample(get_ama_corr(c3_ex, c3_ap), rsplan)
    
    print(c2_snk_rs.shape, c2_src_rs.shape, c3_rs.shape)

    if not np.all(0 < c2_snk_rs[:, : tsep + 1]) : print("warning : neg. c2_snk")
    if not np.all(0 < c2_src_rs[:, tsep : : -1]) : print("warning : neg. c2_snk")
    sqf = np.sqrt(np.abs(  c2_snk_rs[:, : tsep+1] / c2_src_rs[:, : tsep+1]
                         * c2_src_rs[:, tsep::-1] / c2_snk_rs[:, tsep::-1] ))
    ratio = (c3_rs / np.sqrt(np.abs(c2_snk_rs[:, tsep] * c2_src_rs[:, tsep]))[:,None]
                   * sqf)
    print(ratio.shape)

    return lhpd.calc_avg_err(ratio, rsplan)

def runme_show_pltx(tsep_list, had, psnk, flav, qext_list, lpath_str,
                     gamma, re_im, ax=None, approx_only=False) :
    if None is ax : ax = dv.make_std_axes()
    d_tr = .4 / max(len(tsep_list) - 1, 1)
    for i, tsep in enumerate(tsep_list) :
        trange = np.r_[ : tsep + 1]
        plot_edotline( ( d_tr * i + trange, ax, dv.style_group_default[i], 
                         'T=%d' % tsep ),
                       calc_general_pltx, tsep, had, psnk, flav, qext_list, 
                       lpath_str, gamma, re_im, approx_only=approx_only)
    #ax.set_ylim([0., 200.])
    ax.set_xlabel(r'$\tau$')
    ax.set_ylabel(r'plateau(%s, $\mathrm{%s}\langle N [\bar{q} \Gamma_{%d} q]\bar{N}\rangle$)' % (flav, 
                {0:'Re', 1:'Im'}[re_im], gamma))

    return ax
"""

ax=dv.class_logger(dv.make_std_axes()) ; ax=runme_show_pltx([8,9,10,12], 'proton_1', [0,0,0], 'U-D', sq1_list, 'l0_', 11, 1, ax=ax, approx_only=True)
"""

"""
h5      = h5py.File('bb-sl-nev500-ncg400.proton_1.U.dt8.h5', 'r')
bb      = h5['proton_1/U/PX0PY0PZ0_DT8'].value
sd      = h5['proton_1/U/PX0PY0PZ0_DT8__DATALIST'].value

h5_ex   = h5py.File('bb-exact.proton_1.U.dt8.h5', 'r')
bb_ex   = h5_ex['proton_1/U/PX0PY0PZ0_DT8'].value
sd_ex   = h5_ex['proton_1/U/PX0PY0PZ0_DT8__DATALIST'].value

bb_ama = make_ama_dataset(bb_ex, sd_ex, bb, sd)




"""


# keep FF analysis separate as it will be merged with the lhpd package
execfile('analysis_ff.py')
