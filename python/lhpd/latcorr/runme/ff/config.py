import numpy as np

latsize         = [ 48, 48, 48, 96 ]
t_axis          = len(latsize) - 1

src_snk_dt_list = [ 8, 9, 10, 12]
op_list         = [
        'tensor1', 'pstensor1',
        'tensor2s2', 'pstensor2s2',
        'tensor3s3', 'pstensor3s3',
        ]
had_list        = [ 'proton_1' ]
had_list_hadspec= [ 'proton_1' ]
had_list_c3pt   = [ 'proton_1' ]
flav_list       = [ 'U', 'D' ]
psnk_list_c3pt  = [[0,0,0]]
qext2_max       = 10
sq1_list        = [ [1,0,0], [-1,0,0],
                    [0,1,0], [0,-1,0],
                    [0,0,1], [0,0,-1] ]


# AMA modes 
ama_exact = {   'approx' : 'exact' }
ama_apprx   = {   
        'approx' : 'sloppy', 
        'nev'   : 500,
        'ncg'   : 400 }
ama_mode_list = [ ama_exact, ama_apprx ]

# (can be deduced from metainfo in HDF5 attributes, hard-coded for simplicity)
n_cfg = 10                       # number of configs 
n_ex_cfg, n_ap_cfg = 1, 32      # number of samples 
# exact samples are regularly spaced wrt approx samples
match_ex_ap = np.r_[ : n_cfg * n_ap_cfg : n_ap_cfg / n_ex_cfg] 

def ama_mode_desc(ama_mode) :
    if 'exact' == ama_mode['approx'] : 
        return 'exact'
    elif 'sloppy' == ama_mode['approx'] :
        return ('sl-nev%d-ncg%d' % (ama_mode['nev'], ama_mode['ncg']))
    else:
        raise ValueError(ama_mode['approx'])

data_top = '../../run3+4'
def get_c2pt_dset(ama_mode, had) :
    """ 
    """
    fname = '%s/hadspec-%s.%s.h5' % (data_top, ama_mode_desc(ama_mode), had)
    kpath = '/%s' % (had,)
    h5f   = h5py.File(fname, 'r')
    return h5f[kpath]

def get_bb_dset(ama_mode, had, flav, psnk, tsep) :
    """ get BB dataset 
        return : HDF5 dataset handler
        the actual data is not (necessarily) loaded to memory until
        a (result).value or (result)[slice] is taken
    """
    fname = '%s/bb-%s.%s.%s.dt%d.h5' % (data_top, ama_mode_desc(ama_mode), had, flav, tsep)
    kpath = '/%s/%s/PX%dPY%dPZ%d_DT%d' % (had, flav, psnk[0],psnk[1],psnk[2], tsep)
    h5f   = h5py.File(fname, 'r')
    return h5f[kpath]
