from past.builtins import basestring
# meas_spec creation from a file 
# <cfg_id>   <parity>
def make_meas_spec(f, latsize) :
    Lx, Ly, Lz, Lt  = latsize
    n_sources   = 3
    src_t0      = 4
    sources     = [
                 [[    0,    0,    0,  (src_t0 ) % Lt ],              # par0
                  [    0, Ly/2, Lz/2,  (src_t0 + Lt / n_sources) % Lt ],
                  [ Lx/2, Ly/2,    0,  (src_t0 + 2 * Lt / n_sources) % Lt ]],
                 [[ Lx/2, Ly/2, Lz/2,  (src_t0) % Lt ],               # par1
                  [ Lx/2,    0,    0,  (src_t0 + Lt / n_sources) % Lt ],
                  [    0,    0, Lz/2,  (src_t0 + 2 * Lt / n_sources) % Lt ]],
                 [[ Lx/2,    0, Lz/2,  (src_t0) % Lt ],               # par2
                  [ Lx/2, Ly/2,    0,  (src_t0 + Lt / n_sources) % Lt ],
                  [    0,    0,    0,  (src_t0 + 2 * Lt / n_sources) % Lt ]],
                 [[    0, Ly/2,    0,  (src_t0) % Lt ],               # par3
                  [    0,    0, Lz/2,  (src_t0 + Lt / n_sources) % Lt ],
                  [ Lx/2, Ly/2, Lz/2,  (src_t0 + 2 * Lt / n_sources) % Lt ]]
    ]

    if isinstance(f, basestring) :
        f = open(f, 'r')
    res = []
    for s in f.readlines() :
        if s[0] == '#': continue
        s_sp = s.split()
        if len(s_sp) <= 0: continue
        if 2 != len(s_sp):
            raise ValueError(s)
        ckpoint_id, par = s_sp[0], int(s_sp[1])
        for i_src in range(n_sources):
            res.append( (ckpoint_id, sources[par][i_src]) )
    return res

