from past.builtins import basestring
from __future__ import print_function
import numpy as np
import h5py
import math
import lhpd
from lhpd.latcorr.bb import *
from lhpd.latcorr.op import *
from lhpd.latcorr.hadspec import *

# ensemble & run-specific
latsize         = [ 48, 48, 48, 48 ]
qext_list2max   = 10
lpath_min, lpath_max = 0, 2

psnk_list       = [ [0,0,0], [-1,0,0] ]
src_snk_dt_list = [ 8, 10, 12 ]
op_list         = [
        'tensor1', 'pstensor1',
        'tensor2s2', 'pstensor2s2',
        'tensor3s3', 'pstensor3s3',
        ]
had_list        = [ 'proton_3', 'proton_negpar_3' ]
psnk_list2max   = 17
flav_list       = [ 'U', 'D' ]
    
smear_tag   = 'GN3x50'
qmass_tag   = 'mq-0.09900'

def make_meas_spec(f) :
    Lx, Ly, Lz, Lt  = latsize
    n_sources   = 3
    src_t0      = 4
    sources     = [
                 [[    0,    0,    0,  (src_t0 ) % Lt ],              # par0
                  [    0, Ly/2, Lz/2,  (src_t0 + Lt / n_sources) % Lt ],
                  [ Lx/2, Ly/2,    0,  (src_t0 + 2 * Lt / n_sources) % Lt ]],
                 [[ Lx/2, Ly/2, Lz/2,  (src_t0) % Lt ],               # par1
                  [ Lx/2,    0,    0,  (src_t0 + Lt / n_sources) % Lt ],
                  [    0,    0, Lz/2,  (src_t0 + 2 * Lt / n_sources) % Lt ]],
                 [[ Lx/2,    0, Lz/2,  (src_t0) % Lt ],               # par2
                  [ Lx/2, Ly/2,    0,  (src_t0 + Lt / n_sources) % Lt ],
                  [    0,    0,    0,  (src_t0 + 2 * Lt / n_sources) % Lt ]],
                 [[    0, Ly/2,    0,  (src_t0) % Lt ],               # par3
                  [    0,    0, Lz/2,  (src_t0 + Lt / n_sources) % Lt ],
                  [ Lx/2, Ly/2, Lz/2,  (src_t0 + 2 * Lt / n_sources) % Lt ]]
    ]

    if isinstance(f, basestring) :
        f = open(f, 'r')
    res = []
    for s in f.readlines() :
        if s[0] == '#': continue
        s_sp = s.split()
        if len(s_sp) <= 0: continue
        if 2 != len(s_sp):
            raise ValueError(s)
        ckpoint_id, par = s_sp[0], int(s_sp[1])
        for i_src in range(n_sources):
            res.append( (ckpoint_id, sources[par][i_src]) )
    return np.array(
                res, 
                dtype=[('ckpoint_id', 'S32'), ('source_coord', np.int32, (4,))])



def aff_bb_file_kpath_func(cfg_key, csrc, tsnk, psnk, had, flav) :
    top_dir     = '../bb' 
    # bb.00_071215_021009.x0y0z24t20.PX-1PY0PZ0T10.proton_negpar_3.D.aff
    return ('%s/bb.%s.x%dy%dz%dt%d.PX%dPY%dPZ%dT%d.%s.%s.aff' % (
                top_dir, cfg_key, 
                csrc[0], csrc[1], csrc[2], csrc[3],
                psnk[0], psnk[1], psnk[2], tsnk,
                had, flav),
            '/cfg%s/bb/%s-%s/%s' % (cfg_key, smear_tag, smear_tag, qmass_tag) )

def aff_op_file_kpath_func(cfg_key, csrc, tsnk, psnk, had, flav, op) :
    top_dir     = './analysis.aff' 
    latsize_t   = latsize[3]
    if (had in ['proton_3']) :
        src_snk_dt = (tsnk - csrc[3] + latsize_t) % latsize_t
    elif (had in ['proton_negpar_3']) :
        src_snk_dt = (csrc[3] - tsnk + latsize_t) % latsize_t
    else: raise ValueError(had)

    return (
            #../op.proton_negpar_3.tensor2s2.U.dt8.aff
            '%s/op.%s.%s.%s.dt%d.aff' % (top_dir, had, op, flav, src_snk_dt),
            #/cfg53_100805_133124/op/GN3x50-GN3x50/mq-0.09900 [+/proton_negpar_3/U/x0_y0_z0_t36/PX-1_PY0_PZ0_T28/tensor2s2/zt/qx2_qy1_qz2]
            '/cfg%s/op/%s-%s/%s' % (cfg_key, smear_tag, smear_tag, qmass_tag) )
def aff_hadspecSS_file_kpath_func(cfg_key, csrc, had):
    top_dir     = './analysis.aff'
    return ('%s/hadspec.SS.nucleon.aff' % (top_dir,),
            '/cfg%s/hspectrum/%s-%s/%s' % (cfg_key, smear_tag, smear_tag, qmass_tag)
           )

def runme_conv_bb(list_meas='list.meas') :
    meas_spec_list = make_meas_spec(list_meas)
    for had in had_list : 
        time_neg = False
        if 'proton_negpar_3' == had : time_neg = True
        for flav in flav_list :
            for src_snk_dt in src_snk_dt_list :
                h5_fname= 'bb.%s.%s.dt%d.h5' % (had, flav, src_snk_dt)
                h5_file = h5py.File(h5_fname, 'a')
                for psnk in psnk_list :
                    print('# ', had, flav, psnk)
                    h5_kpath= '/%s/%s/PX%dPY%dPZ%d_DT%d' %(
                            had, flav,
                            psnk[0], psnk[1], psnk[2], 
                            src_snk_dt)
                    aff_conv_hdf5_bb(
                            h5_file, h5_kpath,
                            meas_spec_list, aff_bb_file_kpath_func, 
                            had, flav, src_snk_dt, psnk, latsize[3], time_neg,
                            qext_list2max, lpath_min, lpath_max)



                # finalize
                h5_file.flush()
                h5_file.close()


def runme_conv_op(list_meas='list.meas') : 
    meas_spec_list = make_meas_spec(list_meas)

    for had in had_list : 
        time_neg = False
        if 'proton_negpar_3' == had : time_neg = True
        for flav in flav_list :
            for op in op_list :
                for src_snk_dt in src_snk_dt_list :
                    
                    h5_fname= 'op.%s.%s.%s.dt%d.h5' % (had, op, flav, src_snk_dt)
                    h5_file = h5py.File(h5_fname, 'a')
                    for psnk in psnk_list :
                        print('# ', had, flav, psnk)
                        h5_kpath= '/%s/%s/%s/PX%dPY%dPZ%d_DT%d' %(
                                had, op, flav,
                                psnk[0], psnk[1], psnk[2], 
                                src_snk_dt)
                        aff_conv_hdf5_op(
                                h5_file, h5_kpath,
                                meas_spec_list, aff_op_file_kpath_func, 
                                had, flav, src_snk_dt, psnk, latsize[3], time_neg,
                                qext_list2max, op)



                    # finalize
                    h5_file.flush()
                    h5_file.close()

def runme_conv_hadspec(list_meas='list.meas') : 
    meas_spec_list = make_meas_spec(list_meas)

    for had in had_list : 
        h5_fname= 'hadspec.%s.SS.h5' % (had,)
        h5_file = h5py.File(h5_fname, 'a')

        print('# ', had)
        h5_kpath= '/%s' %(had,)
        aff_conv_hdf5_hadspec(
                h5_file, h5_kpath,
                meas_spec_list, aff_hadspecSS_file_kpath_func, 
                had, psnk_list2max, latsize[3])

        # finalize
        h5_file.flush()
        h5_file.close()

