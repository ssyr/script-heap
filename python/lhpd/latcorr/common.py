import numpy as np
import math
from lhpd.misc.lat_geom import ldir_inv 
from lhpd.misc.strkey import lpath_str

def make_mom_list(mom2_max):
    """ create a list of dim3=3 momenta so that q^2<=mom2_max
        TODO move to general "geometry" sub-package
    """
    res = []
    pi_max = int(math.ceil(math.sqrt(mom2_max)))
    for px in range(-pi_max, +pi_max+1) : 
        for py in range(-pi_max, +pi_max+1) : 
            for pz in range(-pi_max, +pi_max+1) : 
                if (px*px + py*py + pz*pz) <= mom2_max : 
                    res.append([px,py,pz])
    return res

def make_linkpath_list(lpath_min, lpath_max, dim=4):
    """ create a list of link paths of length lpath_min<=LEN<=lpath_max
        0 <= mu < dim: positive
        dim <= mu < 2*dim: negative
    """
    lp, i_lp, lp_cur  = [ [] ], 0, []
    if lpath_min <= 0 : res = [ [] ]
    else : res = [ ]
    while len(lp_cur) + 1 <= lpath_max :
        for mu in range(2*dim) :
            if 0 != len(lp_cur) and 0 == (dim + mu - lp_cur[-1]) % (2*dim) : continue
            lp.append(lp_cur + [mu])
            if lpath_min <= len(lp_cur) + 1 and len(lp_cur) + 1 <= lpath_max :
                res.append(lp[-1])
        i_lp    += 1
        lp_cur  = lp[i_lp]
    return res
def make_linkpath_list_bb(lpath_min, lpath_max)  :
    return [ lpath_str(lp) for lp in make_linkpath_list(lpath_min, lpath_max) ]

def make_linkpath_list_qpdf(ldir_, lpath_min, lpath_max) :
    """ make lpath keys for lengths [lpath_min, lpath_max]
        negative length means opposite direction 
    """
    res = []
    for l_ in range(lpath_min, lpath_max+1) :
        l, ldir = l_, ldir_
        if l_ < 0 : l, ldir = -l_, ldir_inv[ldir_]
        res.append('l%d_%s' % (l, ldir * l))
    return res

def make_linkpath_list_tmd(*l, **kw) : 
    raise NotImplementedError
