import numpy as np
import aff
from lhpd.aff_io import *
from lhpd.h5_io import *
from lhpd.latcorr.common import *
import math
from ..pymath.H4_tensor import *

def aff_read_op_table(aff_r, aff_kpath, had, flav, csrc, tsnk, psnk, 
        op, comp_list, qext_list):
    """ read operator file from aff for [comp] x [qext]

        TODO perhaps, need to optimize kpath generation, esp. qext->str

        return: op[i_op, i_qext, i_tau]
    """
    print("TODO : FIX BB SIGNS !!!")
    if 0 < len(aff_kpath) and '/' != aff_kpath[-1]:
        aff_kpath = aff_kpath + '/'
    a = None
    nt= None
    for i_comp, op_comp in enumerate(comp_list):
        for i_q, q in enumerate(qext_list):
            x = aff_r.read('%s%s' % (
                    aff_kpath, 
                    aff_key_hadron_3pt_tensor(csrc[3], csrc[0:3], tsnk, psnk, 
                                              had, flav, q, op, op_comp)))
            if None is a:
                nt = len(x)
                a  = np.zeros(
                        shape=(len(comp_list), len(qext_list), nt),
                        dtype=np.complex128)
            a[i_comp, i_q, :] = x[:]
    return a



def aff_conv_hdf5_op(
            h5_file, h5_kpath,
            meas_spec_list, aff_op_file_kpath_func,
            had, flav, src_snk_dt, psnk, latsize_t, time_neg,
            qext_list2max, op) : # TODO (see in Things)
    """ convert OP data from meas-spec-list
        * go through a list of meas_spec
        * assume that operator is sampled all the way from source to sink, inclusively
        * get op_file and op_keypath from external function
        * include all components

        aff_op_file_kpath_func      return op file name and a keypath within
        src_snk_dt                  source-sink sep
        latsize                     lattice size
        time_neg                    whether source->sink time is negative
    """
    print("TODO : FIX BB SIGNS !!!")
    qext_list = make_mom_list(qext_list2max)
    n_data = len(meas_spec_list)
    aff_i = aff_import(8)

    def make_comp_list(rep, comp_list):
        return [ (rep, c) for c in comp_list ]
    # generate list [(rep, rep_comp), ...]
    comp_list = { 'tensor1'     : make_comp_list('H4_T1_d4r1', H4_T1_repr_comp['H4_T1_d4r1']), 
                  'pstensor1'   : make_comp_list('H4_T1_d4r1', H4_T1_repr_comp['H4_T1_d4r1']),
                  'tensor2s2'   : make_comp_list('H4_T2_d3r1', H4_T2_repr_comp['H4_T2_d3r1']) 
                                + make_comp_list('H4_T2_d6r3', H4_T2_repr_comp['H4_T2_d6r3']),
                  'pstensor2s2' : make_comp_list('H4_T2_d3r1', H4_T2_repr_comp['H4_T2_d3r1']) 
                                + make_comp_list('H4_T2_d6r3', H4_T2_repr_comp['H4_T2_d6r3']),
                  'tensor3s3'   : make_comp_list('H4_T3_d8r1_1', H4_T3_repr_comp['H4_T3_d8r1_1']) 
                                + make_comp_list('H4_T3_d4r2', H4_T3_repr_comp['H4_T3_d4r2']),
                  'pstensor3s3' : make_comp_list('H4_T3_d8r1_1', H4_T3_repr_comp['H4_T3_d8r1_1']) 
                                + make_comp_list('H4_T3_d4r2', H4_T3_repr_comp['H4_T3_d4r2']),
                }[op]
    aff_comp_list = [ rc[1] for rc in comp_list ]
    n_comp = len(comp_list)

    op_shape = (n_data, n_comp, len(qext_list), src_snk_dt + 1)
    b = h5_file.create_dataset(
            h5_kpath, shape=op_shape, dtype=np.complex128,
            fletcher32=True)
    for i_ms, ms in enumerate(meas_spec_list):
        ckpoint_id, csrc = ms['ckpoint_id'], ms['source_coord']
        if time_neg : tsnk = (csrc[3] - src_snk_dt + latsize_t) % latsize_t
        else : tsnk = (csrc[3] + src_snk_dt) % latsize_t

        op_file, op_kpath = aff_op_file_kpath_func(
                ckpoint_id, csrc, tsnk, psnk, had, flav, op)
        aff_r   = aff_i.open(op_file)
        b[i_ms] = aff_read_op_table(
                aff_r, op_kpath, had, flav, csrc, 
                tsnk, psnk, op, aff_comp_list, qext_list)

    # "axes"
    b.attrs.dim_spec         = np.array(['i_data', 'i_comp', 'i_qext', 'i_tau' ],
                                           dtype='S32')
    # axes "ticks"
    h5_set_datalist(b, meas_spec_list)
    b.attrs.comp_list       = np.array(comp_list, dtype='S16')
    b.attrs.qext_list       = np.array(qext_list, dtype=np.int32)
    b.attrs.tau_list        = np.r_[0 : src_snk_dt+1]
    # other parameters
    b.attrs.source_sink_dt  = int(src_snk_dt)
    b.attrs.source_hadron   = str(had)
    b.attrs.sink_hadron     = str(had)
    b.attrs.sink_mom        = np.array(psnk, dtype=np.int32)
    b.attrs.time_neg        = int(time_neg)

    return b

