import numpy as np
import aff
from lhpd.aff_io import *
from lhpd.h5_io import *
from lhpd.latcorr.common import *
import math

def aff_read_clover_table(aff_r, aff_kpath, had, flav, csrc, tsnk, psnk, 
        qext_list, clover_ij_gamma_list):
    """ read BuildingBlocks from aff for [qext] x [clover_ij_gamma]

        TODO perhaps, need to optimize kpath generation, esp. qext->str

        return: clover[i_clover_ij_gamma, i_qext, i_tau]
        clover_ij_gamma = [ ( (i, j), iGamma ), ... ]
    """
    print("TODO : FIX BB SIGNS !!!")
    if 0 < len(aff_kpath) and '/' != aff_kpath[-1]:
        aff_kpath = aff_kpath + '/'
    a = None
    nt= None
    for i_c_ijg, c_ijg in enumerate(clover_ij_gamma_list):
        for i_q, q in enumerate(qext_list):
            x = aff_r.read('%s%s' % (
                    aff_kpath, 
                    aff_key_hadron_3pt_clover(csrc[3], csrc[0:3], tsnk, psnk, 
                                had, flav, q, c_ijg[1], c_ijg[0])))
            if None is a:
                nt = len(x)
                a  = np.zeros(
                        shape=(len(clover_ij_gamma_list), len(qext_list), nt),
                        dtype=np.complex128)
            a[i_c_ijg, i_q, :] = x[:]
    return a


def aff_conv_hdf5_clover(
            h5_file, h5_kpath,
            meas_spec_list, aff_clover_file_kpath_func,
            had, flav, src_snk_dt, psnk, latsize_t, time_neg,
            qext_list2max, clover_ij_gamma_list,
            aff_import_max=100):
    """ convert BB data from meas-spec-list
        * go through a list of meas_spec
        * assume that operator is sampled all the way from source to sink, inclusively
        * get clover_file and clover-keypath from external function


        aff_clover_file_kpath_func  return clover file name and a keypath within
        src_snk_dt                  source-sink sep
        latsize                     lattice size
        time_neg                    whether source->sink time is negative
    """
    print("TODO : FIX BB SIGNS !!!")
    qext_list = make_mom_list(qext_list2max)
    n_data = len(meas_spec_list)
    aff_i = aff_import(aff_import_max)
    

    cl_shape = (n_data, len(clover_ij_gamma_list), len(qext_list), src_snk_dt + 1)
    b = h5_file.require_dataset(
            h5_kpath, cl_shape, np.complex128, 
            fletcher32=True)
    for i_ms, ms in enumerate(meas_spec_list):
        ckpoint_id, csrc = ms['ckpoint_id'], ms['source_coord']
        if time_neg : tsnk = (csrc[3] - src_snk_dt + latsize_t) % latsize_t
        else : tsnk = (csrc[3] + src_snk_dt) % latsize_t

        cl_file, cl_kpath = aff_clover_file_kpath_func(
                ckpoint_id, csrc, tsnk, psnk, had, flav)
        aff_r   = aff_i.open(cl_file)
        b[i_ms] = aff_read_clover_table(
                aff_r, cl_kpath, had, flav, csrc, 
                tsnk, psnk, qext_list, clover_ij_gamma_list)

    # "axes"
    b.attrs['dim_spec']      = np.array(['i_data', 'i_clover_ij_gamma', 'i_qext', 'i_tau' ],
                                           dtype='S32')
    # axes "ticks"
    h5_set_datalist(b, meas_spec_list)

    dir_c = 'xyzt'
    b.attrs['clover_ij_gamma_list']    = np.array(
            [ ('%s%s' % (dir_c[c[0][0]], dir_c[c[0][1]]), c[1]) 
              for c in clover_ij_gamma_list ], 
            dtype=[('clover_ij', 'S2'), ('gamma', int) ] )

    b.attrs['qext_list']     = np.array(qext_list, dtype=np.int32)
    b.attrs['tau_list']      = np.r_[0 : src_snk_dt+1]
    # other parameters
    b.attrs['source_sink_dt']= int(src_snk_dt)
    b.attrs['source_hadron'] = str(had)
    b.attrs['sink_hadron']   = str(had)
    b.attrs['sink_mom']      = np.array(psnk, dtype=np.int32)
    b.attrs['time_neg']      = int(time_neg)

    return b

