from future.utils import iteritems
import numpy as np
import aff
from lhpd.aff_io import *
from lhpd.h5_io import *
from lhpd.latcorr.common import *
import math
import re

def aff_read_hadspec_table(aff_r, aff_kpath, had, csrc, psnk_list):
    """ read operator file from aff for [psnk]

        return: hadspec[i_psnk, i_t]
    """
    if 0 < len(aff_kpath) and '/' != aff_kpath[-1]:
        aff_kpath = aff_kpath + '/'
    a = None
    nt= None
    for i_p, p in enumerate(psnk_list):
            #x = aff_r.read(
            x = aff_read_protected(aff_r, '%s%s' % (aff_kpath, 
                            aff_key_hadron_2pt(csrc[3], csrc[0:3], p, had)))
            if None is a:
                nt = len(x)
                a  = np.zeros(
                        shape=(len(psnk_list), nt),
                        dtype=np.complex128)
            a[i_p, :] = x[:]
    return a

# hack... determine template for of momproj tag

def aff_guess_momproj_template(aff_r, aff_kpath) : 
    #klist = aff_r.ls(aff_kpath)
    klist = aff_ls_protected(aff_r, aff_kpath)
    k0 = klist[0]
    rt = {  r'^x[-+\d]+_y'  : "x%d_y%d_z%d",
            r'^PX[-+\d]+_PY': "PX%d_PY%d_PZ%d",
            r'^qx[-+\d]+_qy': "qx%d_qy%d_qz%d" }
    r0,t0 = None, None
    for r,t in iteritems(rt) : 
        if re.match(r, k0) : 
            r0, t0 = r,t
            break
    #for k in klist[1:] : 
        #if None is re.match(r0, k) : 
            #raise KeyError("non-conforming key '%s' for guessed template '%s'" % (k, t0))
    return t0

def aff_read_hadspec_list(aff_r, aff_kpath, psnk_list):
    """ read operator file from aff for [psnk]

        return: hadspec[i_psnk, i_t]
    """
    if 0 < len(aff_kpath) and '/' != aff_kpath[-1]:
        aff_kpath = aff_kpath + '/'
    a = None
    nt= None
    t = aff_guess_momproj_template(aff_r, aff_kpath)
    assert(not None is t)
    for i_p, p in enumerate(psnk_list):
            psnk_str = t % tuple(p)
            k = '%s%s' % (aff_kpath, psnk_str)
            #try : x = aff_r.read(k)
            #except aff.Exception, s : raise KeyError((s, k))
            x = aff_read_protected(aff_r, k)
            if None is a:
                nt = len(x)
                a  = np.zeros(
                        shape=(len(psnk_list), nt),
                        dtype=np.complex128)
            a[i_p, :] = x[:]
    return a


def aff_conv_hdf5_hadspec(
            h5_file, h5_kpath,
            meas_spec_list, aff_hadspec_file_kpath_func,
            had, psnk_list2max, latsize_t,
            aff_import_max=100) : 
    """ convert OP data from meas-spec-list
        * go through a list of meas_spec
        * assume that operator is sampled all the way from source to sink, inclusively
        * get op_file and op_keypath from external function
        * include all components

        aff_op_file_kpath_func      return op file name and a keypath within
        src_snk_dt                  source-sink sep
        latsize                     lattice size
        time_neg                    whether source->sink time is negative
    """
    psnk_list = np.asarray(make_mom_list(psnk_list2max))
    #psnk_list.sort(cmp=lambda x,y: cmp((np.asarray(x)**2).sum(), (np.asarray(y)**2))
    psnk_list_ord= (psnk_list**2).sum(1).argsort()
    psnk_list = psnk_list[psnk_list_ord]


    n_data = len(meas_spec_list)
    aff_i = aff_import(aff_import_max)

    hadspec_shape = (n_data, len(psnk_list), latsize_t)
    b = h5_file.require_dataset(
            h5_kpath, hadspec_shape, np.complex128,
            fletcher32=True)

    for i_ms, ms in enumerate(meas_spec_list):
        ckpoint_id, csrc = ms['ckpoint_id'], ms['source_coord']

        hs_file, hs_kpath = aff_hadspec_file_kpath_func(ckpoint_id, csrc, had)
        aff_r   = aff_i.open(hs_file)
        b[i_ms] = aff_read_hadspec_table(
                aff_r, hs_kpath, had, csrc, psnk_list)
    # "axes"
    b.attrs['dim_spec']     = np.array(['i_data', 'i_psnk', 'i_t' ],
                                           dtype='S32')
    # axes "ticks"
    h5_set_datalist(b, meas_spec_list)
    b.attrs['psnk_list']    = np.array(psnk_list, dtype=np.int32)
    b.attrs['t_list']       =  np.r_[0 : latsize_t ]
    # other parameters
    b.attrs['source_hadron']= str(had)
    b.attrs['sink_hadron']  = str(had)

    return b
