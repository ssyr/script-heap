import numpy as np
from lhpd.h5_io import *
import h5py



def match_data_sanity_check(lists, res, k_comm) :
    """ sanity check for results of 'match_data' """
    for i_X, res_X in enumerate(res) :
        ls_X_dst = []
        k_comm_X = [ None ] * len(k_comm)
        for i_X_f, res_X_f in enumerate(res_X) :
            ls_src, ls_dst = res_X_f
            assert (len(ls_src) == len(ls_dst))
            # check that samples from one file are not repeated
            assert (len(set(ls_src)) == len(ls_src))
            # collect samples for X
            ls_X_dst.extend(ls_dst)
            #
            for j in range(len(ls_src)) :
                k_comm_X[ls_dst[j]]  = lists[i_X][i_X_f][ls_src[j]]

        # check that full k_comm sample is collected for X
        ls_X_dst.sort()
        assert (ls_X_dst == range(len(ls_X_dst)))
        # check that k_comm is reproduced from slices
        assert (k_comm_X == k_comm)

def ndarray_hash(m):
    return hash(m.data)


def merge_data(lists, cmp=None, exclude=set()) :
    """ Utility function to find a sorted matching set of samples, 
        where each sample may have len(lists) variables scattered 
        across a number of data sets; 
        produce a list of slices [[(src_slice, dst_slice), ...], ...] 
        for building up corresponding NumPy arrays;
        Duplicates are eliminated.
    
        input: 
            listA, listB for varA, varB, etc
                each listX contains multiple datalists;
                each datalist lists[X][f] contains sample_key's (opaque, hashable) 
                of samples in some data_X_f data set
            cmp: comparison function for list.sort
            exclude: set of samples to exclude from merge result

            
        output: (k_comm, map_comm)
            k_comm: 
                combined list of keys
            map_comm:
                list [X][f] of tuples of [src/dst]_slices to compose 
                matching datasets of varA,varB,...;
                For example:
                for X in A, B, ... :
                    for f in listX :
                        varX[result[X][f][1]] = data_X_f[result[X][f][0]]

    """
    key_maps = []
    # collect keys->(i_f, i_k) into maps
    for i_X, ls_X in enumerate(lists) :
        key_maps.append({})
        key_map_X = key_maps[-1]
        for i_X_f, ls_X_f in enumerate(ls_X) :
            key_map_X.update([ (k, (i_X_f, i_k)) 
                               for i_k, k in enumerate(ls_X_f) ])
    # find the set of common samples and sort 
    k_comm_set = set.intersection(*[ set(key_map_X.keys()) 
                                     for key_map_X in key_maps ])

    import collections
    if isinstance(exclude, collections.Iterable) :
        k_comm = sorted(k_comm_set.difference(exclude), cmp=cmp)
    elif isinstance(exclude, collections.Callable) :
        k_comm = sorted(filter(exclude, k_comm_set), cmp=cmp)
    else: raise ValueType(exclude)
        
    # produce lists of slice pairs (src, dst)
    # ... (utility function)
    def pairs_to_slices(l) :
        """ convert [(a0,b0), ...] to ([a0, ...], [b0, ...]) """
        return ( [ x[0] for x in l ],
                 [ x[1] for x in l ] )
    # ... (for every X, invert map:{k->(i_X_f, i_X_f_k)} 
    #      to list:{i_X_f->([i_X_f_sample], [i_k_comm])}
    res = [   [   pairs_to_slices([ (key_maps[i_X][k][1], i_k_comm) 
                                    for i_k_comm, k in enumerate(k_comm)
                                    if key_maps[i_X][k][0] == i_X_f ])
                  for i_X_f, ls_X_f in enumerate(ls_X) ]
              for i_X, ls_X in enumerate(lists) ]

    # sanity check
    match_data_sanity_check(lists, res, k_comm)

    return k_comm, res
