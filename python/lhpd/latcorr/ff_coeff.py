from future.utils import iteritems
import lhpd
import numpy as np
import math

# code taken as-is from lhpd-data/calc_ff/ff_coeff.cpp; look there for explanations

# use DeGrand-Rossi Euclidean gamma matrix basis
from lhpd.pymath.gamma_matr import gamma_dgr as gamma
from lhpd.pymath.gamma_matr import Gamma16_dgr as Gamma16
#from lhpd.pymath.gamma_matr import gamma_eucstd as gamma
#from lhpd.pymath.gamma_matr import Gamma16_eucstd as Gamma16
gamma_x, gamma_y, gamma_z, gamma_t = gamma
gamma5  = Gamma16[15]
gamma_id= Gamma16[0]
# FIXME use np.array() instead of np.r_[]
gamma_rot = 1j * np.array([ [ np.dot(gamma[mu], gamma[nu]) 
                              for nu in range(4) ] 
                            for mu in range(4) ] )
gamma_rot[0,0] = gamma_rot[1,1] = gamma_rot[2,2] = gamma_rot[3,3] = 0
gamma_rot_g5   = np.array([ [ np.dot(gamma_rot[mu, nu], gamma5) 
                              for nu in range(4) ] 
                            for mu in range(4) ] )

# H4 representations of Lorentz tensors
from lhpd.pymath.H4_tensor import *





# transition coefficients from Minkowski to Euclidean 
# for time and space components:
# A_euc = coeff_[t/s] * A_mink
# XXX conventions for gamma and derivatives with hep-lat/0304018[Hagler:2003jd]
# agree with derivatives
#em_mom_t    = -1.j
#em_mom_s    = 1.
#em_coord_t  = 1.j
#em_coord_s  = 1.
#em_deriv_t  = 1. / em_coord_t
#em_deriv_s  = 1. / em_coord_s
#em_gamma_t  = 1.
#em_gamma_s  = 1.j
#em_gamma5   = 1.j * em_gamma_t * em_gamma_s * em_gamma_s * em_gamma_s
## 0..2 : space, 3 : time
#em_mom      = np.r_[   em_mom_s,   em_mom_s,   em_mom_s,   em_mom_t ]
#em_coord    = np.r_[ em_coord_s, em_coord_s, em_coord_s, em_coord_t ]
#em_deriv    = np.r_[ em_deriv_s, em_deriv_s, em_deriv_s, em_deriv_t ]
#em_gamma    = np.r_[ em_gamma_s, em_gamma_s, em_gamma_s, em_gamma_t ]
#metric      = np.r_[         -1,         -1,         -1,          1 ]

# Euclidean transformation of contravariant vectors : 
# * coordinates (x1, x2, x3, x4)_E = (x, y, z, I*t)
# * momenta     (p1, p2, p3, p4)_E = (px, py, pz, I*E) where e.g. E = sqrt(m^2+p^2)
# A_euc = coeff_[t|s] * A_mink
em_contravar_s  =  1.
em_contravar_t  =  1.j
# 0..2 : space, 3 : time
em_contravar = np.r_[ em_contravar_s, em_contravar_s, 
                      em_contravar_s, em_contravar_t ]

# XXX all 4-momenta vector variables (unless specified) are defined as real; 
#   use addtional 'em_contravar' factor to compute proper Euclidean contractions 
#   (for example, see e.g. sigma_dot_mom_euc)

# XXX look for conventions and derivations elsewhere
#   (e.g., transition from Minkowski to Euclidean)

########################################################################
# these names for Gamma-matrix contraction functions are very confusing 
# ensure they are properly used
########################################################################
# for all functions: vec4[..., mu] - (array of) Lorentz vectors
# result: (vec4\dot\gamma), shape=vec4.shape[:-1] + (4,4) : last two axes are Dirac indices
# XXX contract Euclidean vec4_euc=(\vec p, i E) with Euclidean gamma-matrices
def vec4_slash_euc(vec4_euc) : 
    return np.tensordot(vec4_euc, gamma, (-1,0))

# XXX convert Minkowski p4mink=(\vec p, E) -> (\vec p, i E) and contract with Euclidean gamma-matrices
def gamma_dot_mom_euc(p4mink) :
    # res[...,a,b] = gamma[mu,a,b] p4[..., mu]
    return np.tensordot(p4mink * em_contravar, gamma, (-1,0))
# XXX convert Minkowski p4mink=(\vec p, E) -> (\vec p, i E) and contract with Euclidean sigma-matrices
def sigma_dot_mom_euc(p4mink):
    # res[...,a,b] = sigma[mu,nu,a,b] p4[..., nu]
    #return (gamma_rot * (p4 * em_contravar)[:,None,None]).sum(1)
    #return np.tensordot(gamma_rot, p4mink * em_contravar, (1,0))
    return np.tensordot(p4mink * em_contravar, gamma_rot, (-1, 1))
# XXX convert Minkowski p4mink=(\vec p, E) -> (\vec p, i E) and contract with Euclidean sigma-matrices *gamma5
def sigma5_dot_mom_euc(p4mink):
    # res[...,a,b] = sigma[mu,nu,a,b] gamma5 p4[..., nu]
    #return (gamma_rot * (p4 * em_contravar)[:,None,None]).sum(1)
    return np.tensordot(p4mink * em_contravar, gamma_rot_g5, (-1,1))


def twopt_spinmatrix_ferm(m, p4mink) : 
    # (m - I*pslash_Euc)
    # FIXME use vec4_slash_euc : 
    # FIXME return m*gamma_id - 1j * vec4_slash_euc(p4 * em_contravar)
    return m * gamma_id - 1j * np.tensordot(p4mink * em_contravar, gamma, (0,0))
    #return m * gamma_id - 1j * (gamma * (p4 * em_contravar)[:,None,None]).sum(0)

def ratio_denominator_f2f(m_in, p4_in, tpol_c2_in, m_out, p4_out, tpol_c2_out) :
    return 2. * math.sqrt(p4_in[3] * p4_out[3] 
            * np.dot(twopt_spinmatrix_ferm( m_in, p4_in ), tpol_c2_in ).trace().real
            * np.dot(twopt_spinmatrix_ferm(m_out, p4_out), tpol_c2_out).trace().real)


# adapted from /Users/syritsyn/Results/BMW/from_JRG/ff.v2/sns-analysis/gff-q2-fits/config.py
gff_list_f2f = {
    'tensor1'       : [ 'F1',   'F2' ],
    'pstensor1'     : [ 'GA',   'GP' ],
    'tensor2s2'     : [ 'A20',  'B20',  'C20' ],
    'pstensor2s2'   : [ 'A20p', 'B20p' ],
    'tensor3s3'     : [ 'A30',  'A32',  'B30',  'B32' ],
    'pstensor3s3'   : [ 'A30p', 'A32p', 'B30p', 'B32p' ],

    # quick hack to get scalar and tensor charges; only for q=0
    'tensor0'       : [ 'S' ],
    'sigma2a2'      : [ 'T' ],  # FIXME add other ffs
}
gff_to_op_f2f = dict([ 
        (ff, op) for op, ff_list in iteritems(gff_list_f2f) 
                 for ff in ff_list ])
gff_to_op_ind_f2f = dict([ 
        (ff, (op, i_ff)) for op, ff_list in iteritems(gff_list_f2f) 
                         for i_ff, ff in enumerate(ff_list) ])
def get_gff_index_f2f(op, gff) :
    return gff_list_f2f[op].index(gff)
def get_gff_number(op) :
    return len(gff_list_f2f[op])
def get_gff_name(op, i_gff) :
    return gff_list_f2f[op][i_gff]
def get_gff_op(gff) : 
    return gff_to_op_ind_f2f[gff][0]
def get_gff_ind(gff) : 
    return gff_to_op_ind_f2f[gff][1]

if False: 
    def gamma_dot_vec(vec4) :
        """ compute contraction gamma[mu]*vec[mu] in Minkowski """
        return ((metric / em_gamma * vec4)[:, None, None] * gamma).sum(0)
    def sigma_dot_vec(vec4) :
        """ compute contraction sigma[mu,nu]*vec[nu] in Minkowski """
        return ((metric / em_gamma * vec4)[:, None, None] * gamma_rot).sum(1) / em_gamma[:, None, None]

    def threept_spinmatrix_f2f(gamma_pol, m_in, p4_in, m_out, p4_out) :
        return np.dot(
                twopt_spinmatrix_ferm(m_in, p4_in),
                np.dot(gamma_pol,
                       twopt_spinmatrix_ferm(m_out, p4_out))) 
                
    def spintrace_vector_f2f(gamma_pol, mass_in, p4_in, mass_out, p4_out, q4) :
        """ return tr[...], tr[... * gamma[mu]], tr[... * sigma*q ] """
        aux = threept_spinmatrix_f2f(gamma_pol, mass_in, p4_in, mass_out, p4_out)
        return (aux.trace(),
                np.r_[ [ np.dot(aux, gamma[mu] / em_gamma[mu]).trace() 
                         for mu in range(4) ] ],
                np.r_[ [ np.dot(aux, ((metric / em_gamma * q4)[:, None, None] 
                                * gamma_rot[mu] / em_gamma[mu]).sum(0)).trace()
                         for mu in range(4) ] ]
        )

    def spintrace_axial_f2f(gamma_pol, mass_in, p4_in, mass_out, p4_out, q4) :
        """ return tr[... gamma5], tr[... * gamma[mu]*gamma5] """
        aux = threept_spinmatrix_f2f(gamma_pol, mass_in, p4_in, mass_out, p4_out)
        return (np.dot(aux, gamma5 / em_gamma5).trace(),
                np.r_[ [ np.dot(aux, np.dot(gamma[mu] / em_gamma[mu], gamma5 / em_gamma5)).trace()
                         for mu in range(4) ] ]
        )


def ff_matrix_f2f(mass_in, p4_in, tpol_c2_in, mass_out, p4_out, tpol_c2_out, op, tpol) :
    """ return (n_comp, n_ff) complex matrix 
        tensors are not symmetrized! One must feed them to functions computing Tn -> H(4) irreps
    """
    p4_in   = np.asarray(p4_in)
    p4_out  = np.asarray(p4_out)

    N   = None
    m_av   = math.sqrt(mass_in * mass_out)    # XXX wild guess, really; perhaps, FIXME?
    p4     = (p4_out + p4_in) / 2.    # XXX perhaps O(a^?) errors introduced
    q4     = (p4_out - p4_in)
    
    ferm_matr_in    = twopt_spinmatrix_ferm(mass_in, p4_in)
    ferm_matr_out   = twopt_spinmatrix_ferm(mass_out, p4_out)
    aux             = np.dot(ferm_matr_in, np.dot(tpol, ferm_matr_out))

    # euclidean tensor components (tensor structure "building blocks")
    p4euc           = p4 * em_contravar
    q4euc           = q4 * em_contravar
    tr_id           = aux.trace()
    tr_g5           = np.dot(aux, gamma5).trace()
    tr_gamma        = np.r_[ [ np.dot(aux, gamma[mu]).trace() 
                               for mu in range(4) ] ]
    tr_gamma_g5     = np.r_[ [ np.dot(aux, np.dot(gamma[mu], gamma5)).trace() 
                               for mu in range(4) ] ]
    sigmaq          = sigma_dot_mom_euc(q4)
    tr_sigmaq       = np.r_[ [ np.dot(aux, sigmaq[mu]).trace() 
                               for mu in range(4) ] ]
    tr_sigma_munu   = np.array([ [ np.dot(aux, gamma_rot[mu, nu]).trace()
                                   for nu in range(4) ]
                                  for mu in range(4)  ])
    
    # cases
    # for explanation of complex factors, look elsewhere
    n_ff    = len(gff_list_f2f[op])
    if   'tensor1' == op :
        res = np.empty(shape=(4, n_ff), dtype=np.complex128)
        res[:, 0] = tr_gamma
        res[:, 1] = tr_sigmaq *.5 /m_av
    
    elif 'tensor2s2' == op :
        res = np.empty(shape=(4, 4, n_ff), dtype=np.complex128)
        res[:,:,0] = 1j *tr_gamma[:,N] *p4euc[N,:]                      # A20
        res[:,:,1] = 1j *tr_sigmaq[:,N] *p4euc[N,:] *.5 /m_av           # B20
        res[:,:,2] = tr_id *q4euc[:,N] *q4euc[N,:] /m_av                # C20
    
    elif 'tensor3s3' == op :
        res = np.empty(shape=(4, 4, 4, n_ff), dtype=np.complex128)
        res[:,:,:,0] = -1 *tr_gamma[:,N,N] *p4euc[N,:,N] *p4euc[N,N,:]              # A30
        res[:,:,:,1] = -1 *tr_gamma[:,N,N] *q4euc[N,:,N] *q4euc[N,N,:]              # A32
        res[:,:,:,2] = -1 *tr_sigmaq[:,N,N] *p4euc[N,:,N] *p4euc[N,N,:] *.5 /m_av   # B30
        res[:,:,:,3] = -1 *tr_sigmaq[:,N,N] *q4euc[N,:,N] *q4euc[N,N,:] *.5 /m_av # B32
    
    elif 'pstensor1' == op :
        res = np.empty(shape=(4, n_ff), dtype=np.complex128)
        res[:, 0] = tr_gamma_g5 
        res[:, 1] = -1j *tr_g5 *q4euc *.5 /m_av
    
    elif 'pstensor2s2' == op :
        res = np.empty(shape=(4, 4, n_ff), dtype=np.complex128)
        res[:,:,0] = 1j *tr_gamma_g5[:,N] *p4euc[N,:]                   # A20p
        res[:,:,1] = tr_g5 * p4euc[:,N] *q4euc[N,:] *.5 /m_av           # B20p
    
    elif 'pstensor3s3' == op :
        res = np.empty(shape=(4, 4, 4, n_ff), dtype=np.complex128)
        res[:,:,:,0] = -1 *tr_gamma_g5[:,N,N] *p4euc[N,:,N] *p4euc[N,N,:]           # A30p
        res[:,:,:,1] = -1 *tr_gamma_g5[:,N,N] *q4euc[N,:,N] *q4euc[N,N,:]           # A32p
        res[:,:,:,2] = 1j *tr_g5 *p4euc[:,N,N] *p4euc[N,:,N] *q4euc[N,N,:] *.5 /m_av# A30p
        res[:,:,:,3] = 1j *tr_g5 *q4euc[:,N,N] *q4euc[N,:,N] *q4euc[N,N,:] *.5 /m_av# A32p
    #elif 'pstensor2a2' == op : #TODO
    #elif 'pstensor3s23s1' == op : #TODO

    # quick hack for S and T matrix el. at zero momentum transfer
    elif 'tensor0' == op :
        assert(np.allclose(q4, 0))  # only for zero vertex momentum 
        res     = np.empty(shape=(n_ff,), dtype=np.complex128)
        res[0]  = tr_id

    elif 'sigma2a2' == op :
        assert(np.allclose(q4, 0))  # only for zero vertex momentum
        res     = np.empty(shape=(4, 4, n_ff), dtype=np.complex128)
        res[:,:,0]  = tr_sigma_munu
        
    else :
        raise ValueError('unknown op="%s"' % op)

    res /= ratio_denominator_f2f(mass_in, p4_in, tpol_c2_in, mass_out, p4_out, tpol_c2_out)
    return res


    
