import numpy as np
import aff
from lhpd.aff_io import *
from lhpd.h5_io import *
from lhpd.latcorr.common import *
import math
from lhpd.misc.strkey import lpath_str

def aff_read_bb_table(aff_r, aff_kpath, had, flav, csrc, tsnk, psnk, 
        qext_list, gamma_list, lpath_list):
    """ read BuildingBlocks from aff for [qext] x [gamma] x [lpath]
        FIXME this one is deprecated because uses lots of irrelevant info 
            (had, flav, csrc, psnk, tsnk) ; replaced with aff_read_bb_list
        return: bb[i_Gamma, i_linkpath, i_qext, i_tau]
    """
    if 0 < len(aff_kpath) and '/' != aff_kpath[-1]:
        aff_kpath = aff_kpath + '/'
    a = None
    nt= None
    for i_lp, lp in enumerate(lpath_list):
        for i_g, g in enumerate(gamma_list):
            for i_q, q in enumerate(qext_list):
                #x = aff_r.read('%s%s' % (
                x = aff_read_protected(aff_r, '%s%s' % (
                        aff_kpath, 
                        aff_key_hadron_3pt_bb(csrc[3], csrc[0:3], tsnk, psnk, had, flav, q, g, lp)))
                if None is a:
                    nt = len(x)
                    a  = np.zeros(
                            shape=(len(gamma_list), len(lpath_list), len(qext_list), nt),
                            dtype=np.complex128)
                a[i_g, i_lp, i_q, :] = np.asarray(x)
    return a

def aff_read_lpathkey_list(aff_r, aff_kpath, gamma_list, lpath_list, qext_list) :
    """ read BuildingBlocks from aff for [qext] x [gamma] x [lpath] [t]
        return: bb[i_Gamma, i_linkpath, i_qext, i_tau]
    """
    if 0 < len(aff_kpath) and '/' != aff_kpath[-1]:
        aff_kpath = aff_kpath + '/'
    a = None
    nt= None
    for i_lp, lp in enumerate(lpath_list) :
        for i_g, g in enumerate(gamma_list):
            for i_q, q in enumerate(qext_list):
                #x   = aff_r.read(
                x   = aff_read_protected(aff_r, '%s%s/g%s/qx%d_qy%d_qz%d' % ((aff_kpath, lp, g) + tuple(q)))
                if None is a:
                    nt = len(x)
                    a  = np.zeros(
                            shape=(len(gamma_list), len(lpath_list), len(qext_list), nt),
                            dtype=np.complex128)
                a[i_g, i_lp, i_q, :] = np.asarray(x)
    return a

def aff_read_bb_list(aff_r, aff_kpath, gamma_list, lpath_list, qext_list) :
    """ read BuildingBlocks from aff for [qext] x [gamma] x [lpath] [t]
        return: bb[i_Gamma, i_linkpath, i_qext, i_tau]
    """
    lp_str_list = [ lpath_str(lp) for lp in lpath_list ]
    return aff_read_lpathkey_list(aff_r, aff_kpath, gamma_list, 
            lp_str_list, qext_list)


def aff_conv_hdf5_bb(
            h5_file, h5_kpath,
            meas_spec_list, aff_bb_file_kpath_func,
            had, flav, src_snk_dt, psnk, latsize_t, time_neg, 
            had_bc_t,  # XXX added had_bc_t; fix callers!
            qext_list2max, lpath_min, lpath_max,
            aff_import_max=100):
    """ convert BB data from meas-spec-list
        * go through a list of meas_spec
        * assume that operator is sampled all the way from source to sink, inclusively
        * get bb_file and bb-keypath from external function
        * include all linkpaths of lengths in interval [lpath_min, lpath_max]


        aff_bb_file_kpath_func      return bb file name and a keypath within
        src_snk_dt                  source-sink sep
        latsize                     lattice size
        time_neg                    whether source->sink time is negative
    """
    qext_list = make_mom_list(qext_list2max)
    lpath_list= make_linkpath_list(lpath_min, lpath_max)
    n_data = len(meas_spec_list)
    n_gamma= 16
    aff_i = aff_import(aff_import_max)
    

    bb_shape = (n_data, n_gamma, len(lpath_list), len(qext_list), src_snk_dt + 1)
    b = h5_file.require_dataset(
            h5_kpath, bb_shape, np.complex128, 
            fletcher32=True)
    for i_ms, ms in enumerate(meas_spec_list):
        ckpoint_id, csrc = ms['ckpoint_id'], ms['source_coord']
        tsrc = csrc[3]
        if time_neg : tsnk = (tsrc - src_snk_dt + latsize_t) % latsize_t
        else : tsnk = (tsrc + src_snk_dt) % latsize_t

        if tsrc <= tsnk : bc_factor = 1
        else : bc_factor = had_bc_t

        bb_file, bb_kpath = aff_bb_file_kpath_func(
                ckpoint_id, csrc, tsnk, psnk, had, flav)
        aff_r   = aff_i.open(bb_file)
        b[i_ms] = bc_factor * aff_read_bb_table(
                aff_r, bb_kpath, had, flav, csrc, 
                tsnk, psnk, qext_list, range(n_gamma), lpath_list)

    # "axes"
    b.attrs['dim_spec']      = np.array(['i_data', 'i_gamma', 'i_linkpath', 'i_qext', 'i_tau' ],
                                           dtype='S32')
    # axes "ticks"
    h5_set_datalist(b, meas_spec_list)
    b.attrs['gamma_list']    = np.r_[0 : 16]
    b.attrs['linkpath_list'] = np.array(
            [ lpath_str(lp) for lp in lpath_list ],
            dtype='S16')
    b.attrs['qext_list']     = np.array(qext_list, dtype=np.int32)
    b.attrs['tau_list']      = np.r_[0 : src_snk_dt+1]
    # other parameters
    b.attrs['source_sink_dt']= int(src_snk_dt)
    b.attrs['source_hadron'] = str(had)
    b.attrs['sink_hadron']   = str(had)
    b.attrs['sink_mom']      = np.array(psnk, dtype=np.int32)
    b.attrs['time_neg']      = int(time_neg)

    return b

