from __future__ import print_function
from past.builtins import basestring
import aff
import sys

def aff_open_reader_protected(aff_f) :
    try : 
        res = aff.Reader(aff_f)
    except AssertionError : raise
    except aff.Exception :
        sys.stderr.write("ERROR %s: %s\n" % (repr(sys.exc_info()[1]), aff_f))
        raise
    return res

def aff_ls_protected(aff_r, aff_k) :
    try : 
        res = aff_r.ls(aff_k)
    except AssertionError : raise
    except aff.Exception :
        sys.stderr.write("ERROR %s: %s[%s]\n" % (repr(sys.exc_info()[1]), aff_r.name(), aff_k))
        raise
    return res

def aff_read_protected(aff_r, aff_k) :
    try : 
        res = aff_r.read(aff_k)
    except AssertionError : raise
    except aff.Exception :
        sys.stderr.write("ERROR %s: %s[%s]\n" % (repr(sys.exc_info()[1]), aff_r.name(), aff_k))
        raise
    return res

class aff_import:
    def __init__(self, max_open, aff_map={}):
        self.max_open = max_open
        self.aff_map = dict(aff_map)        # keep name:(open_count, aff) ; make a copy!
        self.open_count = 0
    def open(self, name):
        if name in self.aff_map:
            r = self.aff_map[name][1]
            self.aff_map[name] = (self.open_count, r)
            self.open_count += 1
            return r
        r = aff.Reader(name)
        if self.max_open <= len(self.aff_map):   # delete LRU entry
            print(('#close one because %d of %d is open' 
                    % (len(self.aff_map), self.max_open)))
            key_lru = None
            count_lru = self.open_count
            for k in self.aff_map.iterkeys():
                if self.aff_map[k][0] < count_lru: 
                    count_lru = self.aff_map[k][0]
                    key_lru = k
            assert (not key_lru is None)
            del(self.aff_map[key_lru])
        self.aff_map[name] = (self.open_count, r)
        self.open_count += 1
        return r

class aff_library(aff_import):
    """ 
        open_rel: open files by name relative to dpath
        read: read data using ((aff_file, kpath_prefix), kpath)
    """
    def __init__(self, dpath, max_open, aff_map={}):
        aff_import.__init__(self, max_open, aff_map)
        if 0 < len(dpath) and dpath[-1:] != '/': dpath += '/'
        self.dpath = dpath
    def open_rel(self, name):
        """ open AFF file, name is relative to dpath """
        return aff_import.open(self, self.dpath + name)
    def read(self, name_kpath, kpath):
        """
            name_kpath = (name, kpath_prefix)
            kpath = kpath relative to kpath_prefix (may also be absolute)
        """
        kpath_prefix = name_kpath[1]
        if 0 < len(kpath_prefix) and kpath_prefix[1][-1:] != '/':
            kpath_prefix += '/'
        return self.open_rel(name_kpath[0]).read(kpath_prefix + kpath)
    def check(self, name=None):
        """ check aff files in aff_import OR open and check 'name' 
            can also be used as a preload
        """
        if None is name:
            for affr in aff_map.values(): affr.check()
        elif isinstance(name, basestring) :
            self.open_rel(name).check()
        elif isinstance(name, tuple) : self.check(name[0])
        elif isinstance(name, list) :
            for n in name: self.check(n)

