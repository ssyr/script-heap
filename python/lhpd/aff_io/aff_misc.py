import aff

def aff_copy_recursive(aff_in, key_in, aff_out, key_out) :
    t = aff_in.type(key_in)
    if list == t : 
        for k in aff_in.ls(key_in) : 
            aff_copy_recursive(
                    aff_in,  "%s/%s" % (key_in,  k), 
                    aff_out, "%s/%s" % (key_out, k))
    elif t in [ str, int, float, complex ] : 
        aff_out.write(key_out, aff_in.read(key_in))
    else : raise ValueError(t)

    
