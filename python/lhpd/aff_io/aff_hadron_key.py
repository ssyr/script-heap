import warnings
class AffKeyError: 
    def __init__(self, errstr): self.errstr = errstr


def aff_key_bb_linkpath_str(lpath):
    """
        xyztXYZT
    """
    print("WARNING[aff_key_bb_linkpath_str] obsolete? look at misc/strkey.py")
    return "l%d_%s" % (len(lpath), ''.join(["xyztXYZT"[c] for c in lpath]))

def aff_key_hadron_3pt_bb(tsrc, csrc, tsnk, psnk, had, flav, qext, gamma, lpath):
    """ 
        example:
        [/cfg00_071215_021009/bb/GN3x50-GN3x50/mq-0.09900/]proton_3/D/x0_y0_z24_t20/
                /PX-1_PY0_PZ0_T30/l3_TTT/g11/qx0_qy0_qz0

        lpath   see help for `aff_key_bb_lpath'
    """
    return "%s/%s/x%d_y%d_z%d_t%d/PX%d_PY%d_PZ%d_T%d/%s/g%d/qx%d_qy%d_qz%d" % \
            (had, flav,
             csrc[0], csrc[1], csrc[2], tsrc,
             psnk[0], psnk[1], psnk[2], tsnk, 
             aff_key_bb_linkpath_str(lpath), gamma,
             qext[0], qext[1], qext[2])

def aff_key_hadron_3pt_clover(tsrc, csrc, tsnk, psnk, had, flav, qext, gamma, clover_ij):
    """ 
        example:
        [/cfg640/clover-3pt/GN0.5625x100-GN0.5625x100/mq0.00078/]
                proton_1/D/x0_y0_z0_t0/PX0_PY0_PZ0_T10/clover_yt/g10/qx0_qy0_qz0

    """
    dir_c = 'xyzt'
    return "%s/%s/x%d_y%d_z%d_t%d/PX%d_PY%d_PZ%d_T%d/clover_%s%s/g%d/qx%d_qy%d_qz%d" % \
            (had, flav,
             csrc[0], csrc[1], csrc[2], tsrc,
             psnk[0], psnk[1], psnk[2], tsnk, 
             dir_c[clover_ij[0]], dir_c[clover_ij[1]], gamma,
             qext[0], qext[1], qext[2])

def aff_key_hadron_3pt_tensor(tsrc, csrc, tsnk, psnk, had, flav, qext, tensor, tensor_comp):
    """ aff_key_hadron_3pt_tensor(tsrc, csrc, tsnk, psnk, had, flav, qext, tensor, tensor_comp) 
        csrc - sequence of spatial x,y,z coordinates
        had = {'proton_3', 'proton_negpar_3'} (Chroma-style names)
        flav = {'U', 'D'} two values allowed
        tensor = {'tensor0', 'pstensor0', 'tensor1', 'pstensor1' 
                  'tensor2s2', 'pstensor2s2', 'tensor3s3', 'pstensor3s3' }
        tensor_comp = {'', 'x|y|z|t', 'xy|xz|xt|yz|yt|zt', 'o1|o2|o3|o4|o5|o6|o7|o8', ...}
    """
    return '%s/%s/x%d_y%d_z%d_t%d/PX%d_PY%d_PZ%d_T%d/%s/%s/qx%d_qy%d_qz%d' % \
            (had, flav,
             csrc[0], csrc[1], csrc[2], tsrc,
             psnk[0], psnk[1], psnk[2], tsnk, 
             tensor, tensor_comp,
             qext[0], qext[1], qext[2])

def aff_key_hadron_2pt(tsrc, csrc, psnk, had):
    """ aff_key_hadron_2pt(tsrc, csrc, psnk, had)
        csrc - sequence of spatial x,y,z coordinates
        had = {'proton_3', 'proton_negpar_3'} (Chroma-style names)
    """
    #if had != 'proton_3' and had != 'proton_negpar_3': raise AffKeyError('unknown hadron name')
    return '%s/x%d_y%d_z%d_t%d/PX%d_PY%d_PZ%d' % \
           (had,
            csrc[0], csrc[1], csrc[2], tsrc,
            psnk[0], psnk[1], psnk[2])


