import math
import numpy as np
import sys

##############################################################################
# stats
##############################################################################
def binit(ens, binsize=1) :
    """ 'cause just bin() is not convincing enough
            binsize     
                int: bin size; 
                tuple: 
                    ('ama', n_apprx, n_exact) bin with bias corrected
                        data in each bin must be in the following form:
                        bin size = n_apprx + 2*n_exact
                        approx samples [0:n_apprx]; approx samples for bias
                        correction[+0: +n_exact]; exact samples [+0; +n_exact]

        bins are computed in a simple loop once over the ensemble 
        must not be a problem: binning is not supposed to be called O(Ndata) times

        ens may be any collection: np.array, list, tuple, etc; useful to make lists 
        of array slices without copying

        return is always a np.array
    """
    if 1 == binsize : 
        
        return np.asarray(ens)

    if isinstance(binsize, (int, long)) : 
        
        if binsize <= 0 :
            raise ValueError('incorrect binsize=%d' % binsize)
        n_data  = len(ens)
        if n_data <= 0 :
            raise ValueError('empty data')
        n_bins  = n_data / binsize
        n_data_bins = n_bins * binsize
        if n_data_bins != n_data :
            import warnings
            warnings.warn('last %d samples are ignored because of binning' % (
                                n_data - n_data_bins), RuntimeWarning)

        # be careful not to create a numpy-copy of the data
        # every element must already be an np.array
        res_sh  = (n_bins,) + ens[0].shape
        res_dt  = np.find_common_type([], [np.float_, ens[0].dtype])
        res     = np.zeros(res_sh, res_dt)
        for i_bin in range(n_bins) :
            for i in range(binsize) : 
                res[i_bin] += ens[i_bin * binsize + i]
        res /= binsize
        return res

    if isinstance(binsize, tuple) :
        if 'ama' == binsize[0] :
            assert(len(binsize) == 3)
            n_apprx, n_exact = binsize[1:3]
            assert(0 < n_apprx)
            assert(0 < n_exact)
            bsize   = n_apprx + 2 * n_exact
            n_data  = len(ens)
            if n_data <= 0 :
                raise ValueError('empty data')
            n_bins  = n_data // bsize
            n_data_bins = n_bins * bsize
            if n_data_bins != n_data :
                raise ValueError('datasize=%d inconsistent with AMA(%d,%d)->%d entries/bin' %(
                                    n_data, n_apprx, n_exact, bsize))
            x_ap = np.zeros_like(ens[0])
            x_ex = np.zeros_like(ens[0])
            x_ap0= np.zeros_like(ens[0])
            res_sh  = (n_bins,) + ens[0].shape
            res_dt  = np.find_common_type([], [np.float_, ens[0].dtype])
            res     = np.empty(res_sh, res_dt)
            for i_bin in range(n_bins) :
                x_ap.fill(0), x_ex.fill(0), x_ap0.fill(0)
                ir  = i_bin * bsize
                for i in range(n_apprx) :   x_ap += ens[ir + i]
                ir += n_apprx
                for i in range(n_exact) :   x_ap0+= ens[ir + i]
                ir += n_exact
                for i in range(n_exact) :   x_ex += ens[ir + i]
                res[i_bin] = x_ap / n_apprx + (x_ex - x_ap0) / n_exact

            return res
            
        else :
            raise ValueError('incorrect bin option "%s"' % binsize[0])
def resample_jk_iter(ens, binsize=1):
    """ 
    iterate over single-eliminated (JK resampling plan) ensemble averages
    return average
    """
    ens = binit(ens, binsize)
    n = len(ens)
    assert(2 <= n)
    sum = ens.sum(0)
    for x in ens : 
        yield (sum - x) / (n - 1)


def resample_bs_iter(ens, binsize=1, samples=100):
    """
    draw bootstrap samples from binned ensemble
    """
    ens = binit(ens, binsize)
    n = len(ens)
    ens_binned = ens
    for i_sample in range(samples) :
        yield ens_binned[np.random.randint(0, n, n)].mean(0)


def resample_iter(ens, rsplan=None):
    """
    iterate over resampled ensemble with specified method
        rsplan       'av'|None   no resampling
                        'jk'        Jackknife
                        'bs'        NOT IMPLEMENTED
        binsize         how to bin data
    """
    if (None is rsplan):  return ens.__iter__()
    assert isinstance(rsplan, tuple)
    if ('av'==rsplan[0]):
        return iter(binit(ens, rsplan[1]))
    elif ('jk' == rsplan[0]):
        return resample_jk_iter(ens, rsplan[1])
    elif ('bs' == rsplan[0]):
        return resample_bs_iter(ens, binsize=rsplan[1], samples=rsplan[2])
    else: raise ValueError('unknown resampling %s' % rsplan[0])

def resample(ens, rsplan=None):
    return np.array([ x for x in resample_iter(ens, rsplan=rsplan) ])
def resample_var_factor(rsplan, n_data):
    """ the factor one has to multiply data variance with to get 
        the true mean uncertainty 
        rsplan  resample plan specification; None is equivalent to ('av',)
        n_data  the number of data points
    """
    if (None is rsplan 
            or 'av' == rsplan[0]
            or 'ama' == rsplan[0]):   
        return 1. / max(1, n_data - 1)
    elif ('jk' == rsplan[0]):               return float(max(1, n_data - 1))
    elif ('bs' == rsplan[0]):               return 1.
    else : raise ValueError(("unknown rsplan='%s'" % rsplan[0]))
def calc_err(a, rsplan=None, axis=0):
    a   = np.asarray(a)
    return np.sqrt(a.var(axis) * resample_var_factor(rsplan, a.shape[axis]))
def calc_avg(a, axis=0):
    a   = np.asarray(a)
    return a.mean(axis)
def calc_avg_err(a, rsplan=None, axis=0):
    return calc_avg(a, axis=axis), calc_err(a, rsplan=rsplan, axis=axis)

##############################################################################
# pretty print
##############################################################################
def spprint_ve(val, err, errmax=100, precmax=8):
    """ DEPRECATED """
    def exp10(pwr):
        abspwr = abs(pwr)
        res = 1
        a = 10
        while 0 < abspwr:
            if abspwr % 2 == 1: res *= a
            abspwr //= 2
            a *= a
        if (0 <= pwr): return res
        else: return 1. / res
    val_norm, pwr_val = 0,0
    assert 0 <= err
    if val != 0:
        pwr_val = int(math.floor(math.log10(abs(val))))
        val_norm= val * exp10(-pwr_val)
        if err != 0:
            pwr_err = int(math.ceil(math.log10(float(err) / errmax)))
            err_norm= int(err * exp10(-pwr_err))
            prec_val= pwr_val - pwr_err
            if prec_val < 0:
                err_norm *= exp10(-prec_val)
                prec_val = 0
            elif precmax < prec_val:
                err_norm=int(err_norm * exp10(precmax - prec_val))
                prec_val = precmax
        else:
            err_norm = 0
            prec_val = precmax
    else:
        val_norm = 0
        if err != 0:
            pwr_err = int(math.ceil(math.log10(float(err) / errmax)))
            err_norm= int(err * exp10(-pwr_err))
            pwr_val = pwr_err
            prec_val= 0
        else: return '0(0)'
    return ('%%.%df(%%d)e%%d' % prec_val) % (val_norm, err_norm, pwr_val)

def pprint_ve_ncov(val, err, ncov, title=None, fo=sys.stdout, prefix=''):
    """ (pretty)print values, errors, normalized covariance matrix
    """
    n_v     = len(val)
    val     = np.asarray(val)
    err     = np.asarray(err)
    if not None is  ncov: ncov    = np.asarray(ncov)
    assert ((title is None or len(title)==n_v) and 
            (err.shape==(n_v,)) and 
            (None is ncov or ncov.shape==(n_v,n_v)))
    for i, v in enumerate(val):
        e = err[i]
        val_str = spprint_ve(v, e, errmax=316)
        if not None is  prefix and '' != prefix: fo.write(prefix)
        if (not title is None): fo.write('%-15s\t' % title[i])
        fo.write('%-31s' % val_str)
        if (not None is  ncov):
            for j in range(i): fo.write('\t%f' % ncov[i,j])
            fo.write('\t1.')
        fo.write('\n')

def pprint_ve(val, err, title=None, fo=sys.stdout, print_cov=True, prefix=''):
    """ (pretty)print values, errors, normalized covariance matrix
    """
    assert (title is None or len(title)==len(val))
    pprint_ve_ncov(val, err, None, title=title, fo=fo, prefix=prefix)

def pprint_ve_cov(val, cov, title=None, fo=sys.stdout, print_cov=True, prefix=''):
    """ (pretty)print values, errors, normalized covariance matrix
    """
    assert (title is None or len(title)==len(val))
    ncov    = np.asarray(cov).copy()
    err     = lhpd.norm_cov_matrix(ncov)
    if not print_cov: ncov = None
    pprint_ve_ncov(val, err, ncov, title, fo, prefix)


def pprint_avg_std_ncov(fo, title, avg, std, ncov, errmax=100, precmax=8):
    """
    DEPRECATED
    """
    raise RuntimeError("deprecated; replace with pprint_ve_ncov")
    if fo is None:
        import sys
        fo = sys.stdout
    assert (title is None or len(title)==len(avg))
    for i in range(len(avg)):
        val_err_str = spprint_ve(avg[i], std[i], errmax, precmax)
        if (not title is None): fo.write('%-15s\t' % title[i])
        fo.write('%-31s' % val_err_str)
        if not ncov is None:
            for j in range(i): 
                fo.write('\t%f' % ncov[i,j])
        fo.write('\t1.')
        fo.write('\n')
