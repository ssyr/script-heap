import numpy
import aff_io.aff_hadron_key
import re
import math


gr_32 = 0x678dde6e

class mom_combo:
    def __init__(self, psrc, psnk, comp):
        self.psrc, self.psnk, self.comp = psrc, psnk, comp
    def __hash__(self):
        return int(0x7fffffff & 
                   (self.psrc.__hash__() + 
                    gr_32 * (self.psnk.__hash__() + 
                             gr_32 * self.comp.__hash__())))

def mom_str(p): return 'x%dy%dz%d' % tuple(p[0:3])

def mom_equiv_std(p):
    """ make normalized form of p for equivalence comparisons
        components of p are positive, in descending order
    """
    p1 = map(abs, p)
    p1.sort(reverse=True)
    return p1

class corr_set:
    def __init__(self, ls_, mc_list=None, mom_list=None):
        ls = numpy.array(ls_, dtype=int)
        assert ls.shape == (4,)
        self.ls = ls
        self.mom_list = None;   self.mom_map = None
        self.mc_list  = None;   self.mc_map  = None
        if not mc_list is None:
            self.mc_list = []
            self.mc_map = {}
            for mc in mc_list:
                if not mc in self.mc_map:
                    self.mc_map[mc] = len(self.mc_list)
                    self.mc_list.append(mc)
                    mom_list.append(mc.psrc)
                    mom_list.append(mc.psnk)
        if not mom_list is None:
            self.mom_list = []
            self.mom_map = {}
            for p in mom_list:
                if not mom_str(p) in self.mom_map :
                    self.mom_map[mom_str(p)] = len(self.mom_list)
                    self.mom_list.append(p)
        self.mom_equiv_map={}            # normalized p -> list of indices
        for i,p in enumerate(self.mom_list):
            p_str = mom_str(mom_equiv_std(p))
            if p_str in self.mom_equiv_map: self.mom_equiv_map[p_str].append(i)
            else: self.mom_equiv_map[p_str] = [i]
    
    def c2pt_index(self, p): return self.mom_map[mom_str(p)]
    def c3pt_index(self, mc): return self.mc_map[mc]
    def c2pt_equiv_index(self, p): return self.mom_equiv_map[mom_str(mom_equiv_std(p))]

def norm_momentum(mom):
    a = map(abs, mom)
    a.sort(reverse=True)
    return a

def time_reverse_c2pt(a):
    ar = a.copy()
    n = a.shape[0]
    for i in range(1, a.shape[0]): ar[i] = -a[n-i]
    return ar

class c2pt_point:
    """ hold data for 2pt points
        can be added/subtracted, scaled with *,/
    """
    def __init__(self, cset, c2pt=None, had=None, csrc=None, aff_r=None, prefix=''):
        """ Read 2pt correlators from aff file
            cset    corr_set object
            had     hadron type
            csrc    [x,y,z,t] coordinates of the source
            aff     aff reader object
            prefix  aff keypath prefix
        """
        self.cset = cset
        if not c2pt is None: 
            self.c2pt = c2pt
            assert len(c2pt) == len(cset.mom_list)
        elif not aff_r is None:
            if had == 'proton_3': need_time_reverse = False
            elif had == 'proton_negpar_3': need_time_reverse = True
            else: raise aff_io.AffKeyError('unknown hadron name')
            self.c2pt = []
            for p in cset.mom_list:
                a = numpy.array(aff_r.read(prefix + '/' + 
                        aff_io.aff_key_hadron_2pt(csrc[3], csrc[0:3], p, had)))
                if need_time_reverse: self.c2pt.append(time_reverse_c2pt(a.real))
                else: self.c2pt.append(a.real)
        else: self.c2pt = [numpy.zeros((cset.ls[3],), dtype=numpy.float64) 
                            for k in range(len(cset.mom_list))]
    
    def __add__(self, other):
        assert self.cset == other.cset
        a = []
        for i in range(len(self.c2pt())): a.append(self.c2pt[i] + other.c2pt[i])
        return c2pt_point(self.cset, a)
    def __sub__(self, other):
        assert self.cset == other.cset
        a = []
        for i in range(len(self.c2pt())): a.append(self.c2pt[i] - other.c2pt[i])
        return c2pt_point(self.cset, a)
    def __mul__(self, other):
        a = []
        for i in range(len(self.c2pt())): a.append(self.c2pt[i] * other)
        return c2pt_point(self.cset, a)
    def __div__(self, other):
        a = []
        for i in range(len(self.c2pt())): a.append(self.c2pt[i] / other)
        return c2pt_point(self.cset, a)
    def get(self, p): return c2pt[self.cset.c2pt_index(mom_str(p))]
    def get_avg_equiv(self, p):
        i_list = self.cset.c2pt_equiv_index(p)
        assert 0 < len(i_list)
        return reduce(lambda x,y: x+y, 
                      [ self.c2pt[i] for i in i_list ], 0) / len(i_list)


def init_corr_set_2pt_mom2_max(ls, mom2_max=0):
    pi_max = int(math.sqrt(mom2_max))
    mom_list = []
    for px in range(-pi_max, pi_max+1):
        for py in range(-pi_max, pi_max+1):
            for pz in range(-pi_max, pi_max+1):
                if px * px + py * py + pz * pz <= mom2_max:
                    mom_list.append(numpy.array([px,py,pz]))
    return corr_set(ls, mom_list=mom_list)

def index2cartesian(k, geom):
    res = [0] * len(geom)
    for i,g in enumerate(geom):
        res[i] = k % g
        k /= g
    return res

# FIXME replace with pymath.combi.permutXXX
def permut_lex_iter(s_):
    s = list(s_)
    s.sort()
    l = len(s)
    assert 0 < l
    yield s
    if l < 1: raise StopIteration
    while True:
        i = l - 2
        while s[i+1] <= s[i]:
            i -= 1
            if (i < 0): raise StopIteration
        j = l - 1
        while s[j] <= s[i]:
            j -= 1
            assert 0 <= j
        s[i], s[j] = s[j], s[i]
        s[i+1:l] = reversed(s[i+1:l])
        yield s

def init_corr_set_2pt_rotations(ls, mom=[0,0,0]):
    # make all comp > 0, non-zeros first
    mom_std = map(abs, mom)
    mom_std.sort()
    mom_std.reverse()
    l = len(mom_std)
    # count non-zeros
    k = 0
    while k < l and 0 < mom_std[k]: k += 1
    # cycle over reflections: +/- for each non-zero component
    mom_sign_list = {}
    mom_list = []
    for i in range(1 << k):
        # add signs to mom
        mom_sign = list(mom_std)
        signs = index2cartesian(i, [2] * l)
        for j in range(l):  mom_sign[j] *= 1 - 2 *signs[j]
        mom_sign.sort()
        # check that this combination has not been used and mark it
        if str(mom_sign) in mom_sign_list :
            continue
        mom_sign_list[str(mom_sign)] = 1
        # cycle over permutations
        for mom_i in permut_lex_iter(mom_sign):
            mom_list.append(numpy.array(mom_i))
    return corr_set(ls, mom_list=mom_list)



def init_ensemble_2pt(cset, f, aff_import, path_prefix='.'):
    """ read point list generated by mk_2pt_data_list and make a list of points 
    """
    ens = []
    for s in f:
        mo = re.match('^(\S+)\s+(\S+)\s+\{(.+),(.+),(.+),(.+)\}\s+(\S+)$', s)
        if mo is None: 
            raise RuntimeError('syntax error in %s: cannot parse \'%s\'' % (f.name, s))
        aff_file, aff_prefix = mo.groups()[0:2]
        csrc = map(int, mo.groups()[2:6])
        had_str = mo.groups()[6]
        if had_str == 'proton_3': had = 'proton_3'
        elif had_str == 'proton_negpar_3': had = 'proton_negpar_3'
        else: 
            raise RuntimeError('syntax error in %s: unknown hadron \'%s\'' % (f.file, had_str))
        aff_r = aff_import.open(path_prefix + '/' + aff_file)
        ens.append(c2pt_point(cset, had=had, csrc=csrc, aff_r=aff_r, prefix=aff_prefix))
    return ens
