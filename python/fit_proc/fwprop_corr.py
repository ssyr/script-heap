from __future__ import print_function
import math
import sys
import numpy
import lhpd
import tsv
import getopt


#import aff

#"""
class Pion_corr_periodic(lhpd.fitter.Model_function_Base):
    def __init__(self, lt):
        assert (0 < lt)
        self.lt = lt
        self.par_names_list = ['mpi', 'loc_axial', 'midpoint', 'ps_shell_point']
    def flatten_range(self, trange, loc_axial, midpoint, ps_shell_point):
        nt = len(trange)
        assert (len(loc_axial) == nt and
                len(midpoint) == nt and 
                len(ps_shell_point) == nt)
        res = numpy.zeros(shape=(nt, 3), dtype=numpy.float64)
        res[:,0] = loc_axial
        res[:,1] = midpoint
        res[:,2] = ps_shell_point
        return res.reshape((nt*3,))
    def flatten(self, loc_axial, midpoint, pscorr):
        return numpy.array([loc_axial, midpoint, pscorr])
    def f_range(self, trange, param_): 
        param = self.select_param(param_)
        nt  = len(trange)
        la  = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        mp  = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        ps  = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        for i,t in enumerate(trange):
            assert (t < self.lt)
            exp1    = math.exp(-param[0] * t)
            exp2    = math.exp(-param[0] * (self.lt - t))
            la[i]   = param[1] * (exp1 - exp2)
            mp[i]   = param[2] * (exp1 + exp2)
            ps[i]   = param[3] * (exp1 + exp2)
        return self.flatten_range(trange, la, mp, ps)
    def df_range(self, trange, param_):
        param = self.select_param(param_)
        nt  = len(trange)
        res = numpy.zeros(shape=(nt, 3, len(param)), dtype=numpy.float64)
        exp1    = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        dexp1   = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        exp2    = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        dexp2   = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        for i,t in enumerate(trange):
            exp1[i] = math.exp(-param[0] * t)
            dexp1[i]= -t * exp1[i]
            exp2[i] = math.exp(-param[0] * (self.lt - t))
            dexp2[i]= -(self.lt - t) * exp2[i]
        res[:,0,0] = param[1] * (dexp1 - dexp2)
        res[:,1,0] = param[2] * (dexp1 + dexp2)
        res[:,2,0] = param[3] * (dexp1 + dexp2)
        res[:,0,1] = exp1 - exp2
        res[:,1,2] = exp1 + exp2
        res[:,2,3] = exp1 + exp2
        return res.reshape((nt*3, 4))

# presume there is no periodic contribution
class Pion_corr_wall(lhpd.fitter.Model_function_Base):
    def __init__(self, lt):
        assert (0 < lt)
        self.lt = lt
        self.par_names_list = ['mpi', 'loc_axial', 'midpoint', 'ps_shell_point']
    def flatten_range(self, trange, loc_axial, midpoint, ps_shell_point):
        nt = len(trange)
        assert (len(loc_axial) == nt and
                len(midpoint) == nt and 
                len(ps_shell_point) == nt)
        res = numpy.zeros(shape=(nt, 3), dtype=numpy.float64)
        res[:,0] = loc_axial
        res[:,1] = midpoint
        res[:,2] = ps_shell_point
        return res.reshape((nt*3,))
    def flatten(self, loc_axial, midpoint, pscorr):
        return numpy.array([loc_axial, midpoint, pscorr])
    def f_range(self, trange, param_): 
        param = self.select_param(param_)
        nt  = len(trange)
        la  = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        mp  = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        ps  = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        for i,t in enumerate(trange):
            assert (t < self.lt)
            exp1    = math.exp(-param[0] * t)
            la[i]   = param[1] * exp1
            mp[i]   = param[2] * exp1
            ps[i]   = param[3] * exp1
        return self.flatten_range(trange, la, mp, ps)
    def df_range(self, trange, param_):
        param = self.select_param(param_)
        nt  = len(trange)
        res = numpy.zeros(shape=(nt, 3, len(param)), dtype=numpy.float64)
        exp1    = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        dexp1   = numpy.zeros(shape=(nt,), dtype=numpy.float64)
        for i,t in enumerate(trange):
            exp1[i] = math.exp(-param[0] * t)
            dexp1[i]= -t * exp1[i]
        res[:,0,0] = param[1] * dexp1
        res[:,1,0] = param[2] * dexp1
        res[:,2,0] = param[3] * dexp1
        res[:,0,1] = exp1
        res[:,1,2] = exp1
        res[:,2,3] = exp1
        return res.reshape((nt*3, 4))



def make_za_plateau(cons_axial, local_axial, lt):
    assert (cons_axial.shape == local_axial.shape and
            lt <= cons_axial.shape[1]) 
    return (cons_axial[:,0:lt-2] + cons_axial[:,1:lt-1]) / local_axial[:,1:lt-1] / 4. + \
            cons_axial[:,1:lt-1] / (local_axial[:,1:lt-1] + local_axial[:,2:lt])


def init_pion_corr_fitter(fwpc_rs, param):
    """
        fwpc_rs     resampled ps correlators (full t extent)
    """
    if (param['method'] == 'pion2pt-periodic'): 
        model_func  = Pion_corr_periodic(param['latsize'][3])
    elif (param['method'] == 'pion2pt-wall'):
        model_func  = Pion_corr_wall(param['latsize'][3])
    else:
        raise ValueError('unknown method %s' % param['method'])

    tr  = param['trange']
    data_rs = [ model_func.flatten_range(tr, a[0,tr,1], a[0,tr,2], a[0,tr,3]) for a in fwpc_rs ]
    data_avg, data_sigma, data_cov = lhpd.calc_avg_err_ncov(data_rs, rsplan=param['rsplan'])
    # use non-correlated fit: forget data_cov
    fitter = lhpd.fitter.Fitter(model_func, tr, data_avg, data_sigma, None)
    return (fitter, data_rs, data_avg, data_sigma, data_cov)

def main_execute(param):
    sys.stderr.write('param = %s\n' % repr(param))
    # read cons, local, midpoint, ps_shell_pt
    fwpc_ens = numpy.array(tsv.read_float(open(param['data'], 'r')))
    fwpc_rs = numpy.array([ a for a in 
                    lhpd.resample_iter(fwpc_ens, rsplan=param['rsplan']) ])
    n_data = fwpc_rs.shape[0]
    assert (fwpc_rs.shape == (n_data, 1, param['latsize'][3], 4))
    # read ps_shell_pt, ps_shell_shell
    pion_ens = numpy.array(tsv.read_float(open(param['data_pion'], 'r')))
    pion_rs = numpy.array([ a for a in 
                    lhpd.resample_iter(pion_ens, rsplan=param['rsplan']) ])
    assert (pion_rs.shape == (n_data, 1, param['latsize'][3], 2))

    # trange
    tr = param['trange']
    # trange for Za is shifted by -1
    tr_za = [ i - 1 for i in tr]

    za_pltx = make_za_plateau(fwpc_rs[:,0,:,0], fwpc_rs[:,0,:,1], param['latsize'][3])[:, tr_za]
    za_pltx_err = lhpd.calc_err(za_pltx, rsplan=param['rsplan'])
    
    zsm_pltx = (pion_rs[:,0,:,0] / pion_rs[:,0,:,1])[:, tr]
    zsm_pltx_err = lhpd.calc_err(zsm_pltx, rsplan=param['rsplan'])[1])


    pcfit = init_pion_corr_fitter(fwpc_rs, param)
    pcfit[0].fit(numpy.array(param['start_val']))

    res_rs          = []
    pcfit_res_rs    = []
    chi2ndf_rs      = []
    for i in range(len(fwpc_rs)):
        za      = lhpd.fitter.fit_average(za_pltx[i], za_pltx_err)[0]
        za_chi2 = (((za_pltx[i] - za)/za_pltx_err)**2).sum() / (len(za_pltx_err) - 1)
        zsm     = lhpd.fitter.fit_average(zsm_pltx[i], zsm_pltx_err)[0]
        zsm_chi2= (((zsm_pltx[i] - zsm)/zsm_pltx_err)**2).sum() / (len(zsm_pltx_err) - 1)
        pcfit_res, pcfit_chi2 = pcfit[0].fit_savestate(pcfit[1][i])
        pcfit_chi2 /= pcfit[0].get_ndf()
        mpi     = pcfit_res[0]
        mres    = pcfit_res[2] / pcfit_res[3]
        mf      = param['mq'] + mres
        fpi_B1  = math.sqrt(zsm * za * -pcfit_res[1] * mf * 4. / mpi**2)
        fpi_B3  = math.sqrt(zsm * pcfit_res[3] * mf**2 * 8. / mpi**3)
        res_rs.append(numpy.array([ za, zsm, mpi, mres, fpi_B1, fpi_B3, mpi/fpi_B1 ]))
        pcfit_res_rs.append(pcfit_res)
        chi2ndf_rs.append(numpy.array([ za_chi2, zsm_chi2, pcfit_chi2]))

    pcfit_avg, pcfit_cov = lhpd.calc_avg_cov(pcfit_res_rs, rsplan=param['rsplan'])
    lhpd.pprint_ve_cov(pcfit_avg, pcfit_cov, title=pcfit[0].func.par_names())

    res_avg, res_cov = lhpd.calc_avg_cov(res_rs, rsplan=param['rsplan'])
    lhpd.pprint_ve_cov(res_avg, res_cov, 
                                    title=['Za', 'Zsm', 'mpi', 'mres', 
                                           'fpi_B1', 'fpi_B3', 'mpi/fpi_B1'])
    chi2_avg, chi2_cov=lhpd.calc_avg_cov(chi2ndf_rs, rsplan=param['rsplan'])
    lhpd.pprint_ve_cov(chi2_avg, chi2_cov, 
                                    title=['Za_chi2', 'Zsm_chi2', 'fwp_chi2'])


"""
    if True:
        # pre-compute average and variation
        avg, cov= lhpd.calc_avg_cov(za_pltx[:, param['trange']], 
                                                      rsplan=param['rsplan'])
        # extract stdev
        err     = numpy.array([ math.sqrt(cov[i,i]) for i in range(len(avg)) ])
        # now find variation from resampled ens.
        avg_rs, cov_rs  = lhpd.calc_avg_cov(
                [ lhpd.fitter.fit_average(a[param['trange']], err)[0] 
                        for a in za_pltx ],
                rsplan=param['rsplan'])
        za, dza = float(avg_rs), math.sqrt(float(cov_rs[0,0]))
    else:
        za, za_cov = lhpd.calc_avg_cov(
                za_pltx[:,param['trange']].sum(1) / len(param['trange']),
                param['rsplan'])
        dza = math.sqrt(za_cov)
    print('%13.7e\t%13.7e\n' % (za, dza))
"""



def print_usage(prg_name):
    import sys
    sys.stderr.write('Usage:\n%s' % prg_name)
    sys.stderr.write(
"""
\t\t--data=<data-file> ...
\t\t--data-pion=<data-pion-file> ...
\t\t--latsize=<Lx>,<Ly>,<Lz>,<Lt>
\t\t--trange=tmin:tmax[,tmin2:tmax2]...
\t\t[--rsplan=<rs-plan>]
\t\t[--start-val=v0,v1,...]

data-fwprop:    tsv[i_data, 0, t, {ps-Acons0, ps-Aloc0, ps-midpoint, ps-ps}] - from DWF inversions
    # read cons, local, midpoint, ps_shell_pt
data-pion:      tsv[i_data, 0, t, {SP, SS}] - pion at rest
"""
        )

from common import *

def parse_options(argv):
    param = {                       #default values
        'latsize'           : [],
        'trange'            : [],
        'data'              : None,
        'data_pion'         : None,
        'rsplan'            : ('jk', 1),
        'start_val'         : [1.,1.,1.,1.],
        'mq'                : None,
        'method'            : 'pion2pt-periodic'
    }
    opt_list, rest_arg = \
         getopt.getopt(argv, '', [ 
                         'latsize=',
                         'trange=',
                         'data=',
                         'data-pion=',
                         'rsplan=',
                         'start-val=',
                         'mq=',
                         'method='
                       ])
    for opt, val in opt_list:
        if   opt == '--latsize':    param['latsize']    = parse_latsize(val)
        elif opt == '--trange':     
            # TODO add sort+uniq
            param['trange'] = parse_int_range(val, min=1)
        elif opt == '--data':       param['data']       = val
        elif opt == '--data-pion':  param['data_pion']  = val
        elif opt == '--rsplan':     param['rsplan']     = parse_rsplan(val)
        elif opt == '--start-val':  param['start_val']  = map(float, val.split(','))
        elif opt == '--mq':         param['mq'] = float(val)
        elif opt == '--method':     param['method'] = val

        else: raise RuntimeError('unknown option %s' % opt)

    # FIXME debug
    import sys
    #sys.stderr.write('argv: %s\n' % argv)
    #sys.stderr.write('getopt: %s\n' % repr(opt_list))
    #sys.stderr.write('param: %s\n' % repr(param))
    
    if not check_param(param, ['latsize', 'trange', 'data', 'data_pion', 
            'rsplan', 'start_val', 'mq', 'method']):
        sys.stderr.write('there are absent parameters\n')
        exit(1)
    return param


if (__name__ == '__main__'):
    import sys
    if (len(sys.argv) <= 1):
        print_usage(sys.argv[0])
        exit(1)
    res = main_execute(parse_options(sys.argv[1:]))
    exit(res)
