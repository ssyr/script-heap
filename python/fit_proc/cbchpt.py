from __future__ import print_function
from math import pi, log, sqrt, acos

#for mpi in [.297, .355, .403]: print(mpi, cbchpt_p2.nucleon_mass(mpi, .0862, 1.2, .883, -1.01, 3.2, -3.4, 1.1))
#for mpi in [.140, .297, .355, .403]: print(mpi, cbchpt_p2.nucleon_mass(mpi, .0862, 1.2, .8726, -1.049, 3.2, -3.4, 0.90))
def nucleon_mass(mpi, fpi, gA, M0, c1, c2, c3, e1r):
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +M0
        -4 * c1 * mpi*mpi
        +3 * gA*gA * mpi * mD4pifpi2 / 2 / sqrt(4 - mDM2) * (-4 + mDM2 + 4 * c1 * mpi**4 / M0**3) 
                * acos(mpi/2/M0)
        -3 * mpi*mpi * mD4pifpi2 / 8 * (
            +(6 * gA*gA / M0 - c2)
            +4 * (gA*gA / M0 - 8 * c1 + c2 + 4 * c3) * log(mpi))
        +4 * e1r * mpi**4 
        -6 * c1 * gA*gA * mpi**2 * mDM2 * mD4pifpi2 * log(mpi/M0))




def A20_umd_tzero_p2(mpi, fpi, gA, M0, av20, d_av20, c8r):
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +av20 
        +av20 * mD4pifpi2 * (
            -(3*gA*gA + 1) * log(mpi) * 2 
            -2*gA*gA 
            +gA*gA * mDM2 * (1 + 3*log(mDM2))
            -0.5*gA*gA * mDM2*mDM2 * log(mDM2) 
            +gA*gA * mpi / sqrt(4*M0*M0 - mpi*mpi) * (14 - 8*mDM2 + mDM2*mDM2) * acos(mpi/2/M0))
        +d_av20 * gA * mD4pifpi2 / 3 * (
            +2*mDM2 * (1 + 3 * log(mDM2))
            -mDM2*mDM2 * log(mDM2)
            +2 * mpi * (4*M0*M0 - mpi*mpi)**1.5 / M0**4 * acos(mpi/2/M0))
        +4*mDM2 * c8r)

def A20_umd_tslope_p2(mpi, fpi, gA, M0, av20, c12, dAt):
    Lchi = 4 * pi * fpi
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +c12 / M0 / M0
        -av20 * gA*gA / 6 * mD4pifpi2 / (4 * M0*M0 - mpi*mpi) * (
            +26
            +8 * log(mDM2)
            -mDM2 * (30 + 32 * log(mDM2))
            +mDM2**2 * (6 + 19.5 * log(mDM2))
            -3 * mDM2**3 * log(mDM2)
            -mpi / sqrt(4 * M0*M0 - mpi*mpi) * (90 - 130 * mDM2 + 51 * mDM2**2 - 6 * mDM2**3) 
                        * acos(mpi/2/M0))
        +dAt * mpi / M0 / Lchi**2)

def A20_upd_tzero_p2(mpi, fpi, gA, M0, as20, c9):
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +as20
        +4 * c9 * mDM2
        -3 * as20 * gA*gA * mD4pifpi2 * (
            +mDM2
            +mDM2 * (2 - mDM2) * log(mpi/M0)
            +mpi / sqrt(4*M0*M0 - mpi**2) * (2 - 4*mDM2 + mDM2**2) * acos(mpi/2/M0)))


    



def B20_umd_tzero_p2(mpi, fpi, gA, M0, MN, av20, bv20):
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +bv20 * MN / M0 
        +av20 * gA*gA * mD4pifpi2 * (
            +(3 + log(mDM2))
            -mDM2 * (2 + 3 * log(mDM2))
            +mDM2*mDM2 * log(mDM2) 
            -2 * mpi / sqrt(4*M0*M0 - mpi*mpi) * (5 - 5*mDM2 + mDM2*mDM2) * acos(mpi/2/M0)))

def B20_umd_tslope_p2(mpi, fpi, gA, M0, MN, av20, dBt):
    Lchi = 4 * pi * fpi
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +av20 * gA*gA / 18 / (4 * pi * fpi)**2 / (4 - mDM2) * (
            +4
            +mDM2 * (83 + 24 * log(mDM2))
            -114 * mDM2**2 * (1 + log(mDM2))
            +mDM2**3 * (24 + 75*log(mDM2))
            -12 * mDM2**4 * log(mDM2)
            -6 * mDM2**1.5 / sqrt(4 - mDM2) * (50 - 80*mDM2 + 33 * mDM2**2 - 4 * mDM2**3)
                        * acos(mpi/2/M0))
        +dBt * MN / M0 / Lchi**2)

def B20_upd_tzero_p2(mpi, fpi, gA, M0, MN, as20, bs20, dB0):
    Lchi = 4 * pi * fpi
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +bs20 * MN / M0
        -3 * as20 * gA*gA * mD4pifpi2 * (
            +(3 + log(mDM2)) 
            -mDM2 * (2 + 3 * log(mDM2))
            +mDM2**2 * log(mDM2) 
            -2 * mpi / sqrt(4*M0*M0 - mpi**2) * (5 - 5 * mDM2 + mDM2*mDM2) * acos(mpi/2/M0))
        +dB0 * mpi**2 * MN / M0 / Lchi**2)
    
def C20_umd_tzero_p2(mpi, fpi, gA, M0, MN, av20, cv20):
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +cv20 * MN / M0
        +av20 * gA*gA * mD4pifpi2 / 12 * (
            -1
            +2 * mDM2 * (1 + log(mDM2))
            -mDM2*mDM2 * log(mDM2) 
            +2 * mpi / sqrt(4*M0*M0 - mpi*mpi) * (2 - 4*mDM2 + mDM2*mDM2) * acos(mpi/2/M0)))

def C20_umd_tslope_p2(mpi, fpi, gA, M0, MN, av20, dCt):
    Lchi = 4 * pi * fpi
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +av20 * gA*gA / 180 / (4 * pi * fpi)**2 / (4 - mDM2) * (
            +4
            -13 * mDM2
            +12 * mDM2**2 * (4 + 3 * log(mDM2))
            -3 * mDM2**3 * (4 + 11 * log(mDM2))
            +6 * mDM2**4 * log(mDM2)
            +6 * mDM2**1.5 / sqrt(4 - mDM2) * (10 - 30 * mDM2 + 15 * mDM2**2 - 2 * mDM2**3)
                        * acos(mpi/2/M0))
        +dCt * MN / M0 / Lchi**2)

def C20_upd_tzero_p2(mpi, fpi, gA, M0, MN, as20, cs20, dC0):
    Lchi = 4 * pi * fpi
    mD4pifpi2 = (mpi / 4 / pi / fpi)**2
    mDM2 = (mpi / M0)**2
    return (
        +cs20 * MN / M0
        -as20 * gA*gA * mD4pifpi2 / 4 * (
            -1
            +2 * mDM2 * (1 + log(mDM2)) 
            -mDM2*mDM2 * log(mDM2)
            +2 * mpi / sqrt(4*M0*M0 - mpi*mpi) * (2 - 4*mDM2 + mDM2**2) * acos(mpi/2/M0))
            +dC0 * mpi * MN / Lchi**2)

