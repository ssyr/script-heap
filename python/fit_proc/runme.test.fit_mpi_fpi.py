import sys
import math
import numpy
import lhpd
from fit_proc import fit_mpi_fpi

param={'data':'fit_data.txt', 'start_val':[0., 0., 1.72, .05], 'n_gauss_samples':100}
fit_mpi_fpi.main(param)


# reload
import my_basic
my_basic.my_reload('lhpd')
reload(sys.modules['fit_proc.fit_mpi_fpi'])

