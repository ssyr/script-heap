from math import * #pi, log, sqrt, acos


def fn_log_d_sq(x):
    """ definition of log(x + sqrt(x*x - 1)) / sqrt(x*x - 1) for all x > 0 """
    assert 0. < x
    if 1. < x:
        return log(x + sqrt(x*x - 1)) / sqrt(x*x - 1) 
    elif x == 1:  
        return 1.
    else:
        return acos(x) / sqrt(1 - x*x)


def vectorff_msradius_dirac_isovec(mpi, Fpi, Delta, gA, cA, B10):
    """ Isovector Dirac m.s. radius in HBChPT (SSE); corrections are O(mpi) 
        mpi     [GeV] pion mass
        Fpi     [GeV] pion decay const, chi.limit; normally Fpi = 0.0862(5) GeV
        Delta   [GeV] N-Delta mass gap; =0.293(1) GeV
        gA      axial charge, chi.limit \approx 1.2
        cA      pi-N-Delta coupling \approx 1.5
        B10     counterterm @1GeV
    """
    return ( -(1. + 7.*gA*gA + (2. + 10.*gA*gA) * log(mpi)) / (4. * pi * Fpi)**2
             -12. * B10 / (4 * pi * Fpi)**2
             +cA*cA / (54. * pi*pi * Fpi*Fpi) * 
                    (26. + 30.*log(mpi) 
                     + 30. * (Delta/mpi) * fn_log_d_sq(Delta / mpi)))


def vectorff_anmagmom_isovec(mpi, Fpi, Delta, mnucleon, gA, cA, cV, kappa0, E1):
    """ Isovector anomalous magnetic moment in HBChPT (SSE); corrections are O(mpi**2)
        mpi     [GeV] pion mass
        Fpi     [GeV] pion decay const, chi.limit; normally Fpi = 0.0862(5) GeV
        Delta   [GeV] N-Delta mass gap; =0.293(1) GeV
        mnucleon[GeV] physical nucleon mass; use 0.94 GeV
        gA      axial charge, chi.limit \approx 1.2
        cA      pi-N-Delta coupling \approx 1.5
        cV      pion-nucleon-Delta-photon? [GeV^-1] from phenomenology ~ (-2.5(4)) GeV^{-1}
        kappa0  chi.limit value of kappa
        E1      counterterm @1GeV
    """
    return kappa0 + \
        ( - gA*gA * mpi / (4. * pi * Fpi*Fpi)
          + 2. * cA*cA * Delta / (9. * pi*pi * Fpi*Fpi) * (
                (Delta / mpi) * (1. - (mpi / Delta)**2) * fn_log_d_sq(Delta/mpi) 
                + log(mpi / (2.*Delta)))
          - 8. * E1 * mpi*mpi 
          + 4. * cA * cV * gA * mpi*mpi / (9 * pi*pi * Fpi*Fpi) * log(2.*Delta)
          + 4. * cA * cV * gA * mpi**3 / (27. * pi * Fpi*Fpi * Delta)
          - 8. * cA * cV * gA * Delta**2 / (27. * pi*pi * Fpi*Fpi) * (
                (Delta / mpi) * (1. - (mpi / Delta)**2)**2 * fn_log_d_sq(Delta/mpi)
                + (1. - 3.*mpi*mpi/(2.*Delta**2)) * log(mpi / (2.*Delta)))
        )*mnucleon


def vectorff_msradiusXanmagmom_isovec(mpi, Fpi, Delta, mnucleon, gA, cA, Core):
    """ Isovector Pauli m.s.radus*anom.mag.moment in HBChPT (SSE);
        corrections are O(mpi); "core" term added
        mpi     [GeV] pion mass
        Fpi     [GeV] pion decay const, chi.limit; normally Fpi = 0.0862(5) GeV
        Delta   [GeV] N-Delta mass gap; =0.293(1) GeV
        mnucleon[GeV] physical nucleon mass; use 0.94 GeV
        gA      axial charge, chi.limit \approx 1.2
        cA      pi-N-Delta coupling \approx 1.45(2)
        Core    "core" counterterm

        Note that kappa should be rescaled to "physical magnetons" by (MN_phys / MN_lat) 
        factor, to agree with mnucleon being physical; in fact, it is (kappa/MN)*r2v2 
        which is fitted
    """
    return (gA*gA / (8. * pi * Fpi*Fpi * mpi)
            + cA*cA / (9. * pi*pi * Fpi*Fpi * mpi) * fn_log_d_sq(Delta/mpi)
            + Core) * mnucleon



def axialff_axialcharge_isovec(mpi, Fpi, Delta, gA, cA, g1, C):
    gamma_SSE   = 1. / (16. * pi*pi * Fpi*Fpi) * (
                    50./81.* cA*cA * g1 
                    - 0.5 * gA 
                    - 2./9. * cA*cA * gA 
                    - gA*gA*gA)
    return (gA - gA**3 * mpi*mpi /(16. * pi*pi * Fpi*Fpi) 
            + 4. * mpi*mpi * (C + cA*cA /(4. * pi*pi * Fpi*Fpi) *(155./972.*g1 - 17./36.*gA)
                              + gamma_SSE * log(mpi))
            + 4. * cA*cA * gA * mpi**3 /(27. * pi * Fpi*Fpi * Delta)
            + 8. * cA*cA * gA * mpi * Delta /(27. * pi*pi * Fpi*Fpi) *(1. - (mpi/Delta)**2) \
                    * fn_log_d_sq(Delta/mpi)
            + cA*cA * Delta**2 /(81. * pi*pi * Fpi*Fpi) *(25.*g1 - 57.*gA) * (
                    log(2.*Delta/mpi) - (1 - (mpi/Delta)**2) * Delta/mpi * fn_log_d_sq(Delta/mpi))

    )
