from __future__ import print_function
import math
import sys
import numpy
import scipy
import lhpd
import getopt
import tsv
from lhpd.fitter import avg_std_ncov, print_avg_std_ncov

import hbchpt
import cbchpt


class fit_tA10_upd(lhpd.fitter.Model_function_Base):
    def __init__(self, fpi, gA):
        self.fpi = fpi
        self.gA  = gA
        self.par_names_list = [ 'tA10_0', 'tA10_mpi' ]

    def f(self, x, p):
        tA10_0, tA10_mpi = p
        return hbchpt.tA10_upd(x, self.fpi, self.gA, tA10_0, tA10_mpi)
    
    def f_range(self, xr, p):
        res = numpy.empty((len(xr),), numpy.float64)
        for i,x in enumerate(xr):
            res[i] = hbchpt.tA10_upd(x, self.fpi, self.gA, p[0], p[1])
        return res
    def make_data_range(self, xr, data):
        return data




def read_data(dlist):
    """
        data_list = [ [mpi1, "file1"], ... ]
        file1=[i_data,0,i_q2, i_ff], i_ff=0 -> tA10
    """
    xr_list      = []
    data_list    = []
    for a in dlist:
        xr_list.append(a[0])
        data_list.append(numpy.array(
                tsv.read_float(open(a[1], 'r')))[:,0,0,0])

    return lhpd.make_sjk([ [x] for x in xr_list], [ a[:,None] for a in data_list ] )

#mpir=numpy.arange(.01, .65, .01) ; tsv.write_format(open('mpi_list', 'w'), mpir[...,None])
#mpi_mn_r=numpy.array([[mpi, cbchpt.nucleon_mass(mpi, .0862, 1.2, .8726, -1.049, 3.2, -3.4, 0.90)] for mpi in mpir])
#fitter=main_fit(dlist, fit_tA10_upd(0.0862, 1.2), [ 1., 1. ], rsplan=rsplan, pr=mpir, save_range='tA10.range')

def main_fit(dlist, m, 
             start_val=None, rsplan=('jk', 1), reparam=None, 
             pr=None, save_range=None):
    xr, data = read_data(dlist)
    data = m.make_data_range(xr, data)
    data_avg, data_std, data_cov = lhpd.calc_avg_err_ncov(data, rsplan)

    if reparam is None:
        reparam = m.reparam_default()
    if start_val is None:
        start_val = m.start_default()

    fitter = lhpd.fitter.Fitter(m, xr, data_avg, data_std, data_cov, 
                   reparam=lhpd.fitter.reparam(*reparam))
    p,c2 = fitter.fit(numpy.array(start_val), method='lma_nodiff')

    res = numpy.empty((len(data), len(p)), numpy.float64)
    chi2= numpy.empty((len(data), 1), numpy.float64)
    funcr=[]
    for i_d,d in enumerate(data):
        res[i_d], chi2[i_d] = fitter.fit_savestate(d, None, 'lma_nodiff')
        print(lhpd.fitter.fit_echo(i_d, res[i_d], chi2[i_d]))
        funcr.append(m.f_range(pr, res[i_d]))

    print_avg_std_ncov(sys.stdout, m.par_names(), *avg_std_ncov(res, rsplan))
    print('ndf = ', fitter.get_ndf())
    print_avg_std_ncov(sys.stdout, ['chi2/ndf'], *avg_std_ncov(chi2/fitter.get_ndf(), rsplan))
    funcr = numpy.array(funcr)
    if (not None is  save_range):
        tsv.write_format(open(save_range, 'w'), 
                     funcr.reshape((funcr.shape[0], 1, len(pr), -1)))
    
    return res, chi2, funcr, fitter

