import sys
import math
import numpy
import lhpd
from fit_proc import fwprop_corr

param={'data':'fwprop_corr.tsv', 'data_pion':'pion1.tsv', 'method':'pion2pt-periodic', 'latsize': [20,20,20,64], 'start_val':[.2,1.,1.,1.], 'trange':range(12,52), 'rsplan':('jk',8), 'mq':0.0138}

#fwprop_corr.main_execute({'data':'fwprop_corr.tsv', 'jkbin':8, 'latsize':[20,20,20,64], 'trange':range(12,52), 'resampled':'jk'})

fwprop_corr.main_execute(param)


# reload
import my_basic
my_basic.my_reload('lhpd')
reload(sys.modules['fit_proc.fwprop_corr'])

