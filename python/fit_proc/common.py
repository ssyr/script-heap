from past.builtins import basestring


def check_param(param, list=[]):
    """ check that param is a dictionary and has non-None entries specified in list 
    """
    if (dict != type(param)): return False
    for k in list:
        if not (k in param) or (None is param[k]) : return False
    return True

def parse_rsplan(val):
    """ format for rsplan:
            av[,B]      B-binned average
            jk[,B]      B-binned Jackknife
            bs[,B[,X]]  B-binned bootstrap, with X MC samples
        returns tuple, [0] in ['av', 'jk', 'bs']
    """
    assert (isinstance(val, basestring))
    vlist = val.split(',')
    if (len(vlist) < 1): raise RuntimeError('incorrect rsplan "%s"' % val)
    if ('av' == vlist[0]):
        if   (1 == len(vlist)): return ('av', 1)
        elif (2 == len(vlist)): return ('av', int(vlist[1]))
        else:  raise RuntimeError('incorrect rsplan "%s"' % val)
    elif ('jk' == vlist[0]):
        if   (1 == len(vlist)): return ('jk', 1)
        elif (2 == len(vlist)): return ('jk', int(vlist[1]))
        else:  raise RuntimeError('incorrect rsplan "%s"' % val)
    elif ('bs' == vlist[0]):
        if   (1 == len(vlist)): return ('bs', 1, 1)
        elif (2 == len(vlist)): return ('bs', int(vlist[1]), 1)
        elif (3 == len(vlist)): return ('bs', int(vlist[1]), int(vlist[2]))
        else:  raise RuntimeError('incorrect rsplan "%s"' % val)
    else:  raise RuntimeError('incorrect rsplan "%s"' % val)


def parse_int_range(val, min=None, max=None):
    rng = []
    for dash in val.split(','):
        i1, i2 = map(int, dash.split(':'))
        if (i2 < i1):
            raise RuntimeError('bad range spec %d:%d' % (i1, i2))
        rng.extend(range(i1, i2+1))
    return filter(lambda i: (not min or min <= i) and (not max or i <= max), rng)

def parse_latsize(val, dim=4):
    ls = map(int, val.split(','))
    if (len(ls) != dim):    raise RuntimeError('bad lattice size spec: %s' % val)
    if (reduce(lambda x,y: x or (y <= 0), ls, False)):
        raise RuntimeError('bad lattice size spec: %s' % val)
    return ls
