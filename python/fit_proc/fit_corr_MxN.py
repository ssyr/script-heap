from __future__ import print_function
from past.builtins import execfile
import numpy
import math
import tsv
import lhpd

"""
execfile("compute_meff_eig.py")
a=numpy.array(tsv.read_float(open("nucleon.all", "r")), dtype=float)
a_ij=get_matrix(a,[0,2])
"""

def get_matrix(a, idx):
    """ create matrix [i_data, i_t, i_smear, j_smear] """
    assert type(idx)==list and len(a.shape)==4 and a.shape[1] == 1
    n = a.shape[0]
    n_t = a.shape[-2]
    n_sm = int(math.sqrt(a.shape[-1]))
    n_idx = len(idx)
    #print(n, n_t, n_sm, n_idx)
    assert n_sm * n_sm == a.shape[-1]
    res = numpy.zeros(shape=(n, n_t, n_idx, n_idx), dtype=numpy.float64)
    for i1, i in enumerate(idx):
        for j1, j in enumerate(idx):
            res[:, :, i1, j1] = a[:, 0, :, n_sm *i + j]
    return res

def find_gen_meff(c1, c2, t1, t2):
    """ solve gen.evalue problem 
            corr[t2] . x = e^{-meff_x*(t2-t1)} . corr[t1] . x
    assumption: 
            corr[t] = Q^H . e^{-Et} Q, 
            where Q_{i,alpha} = <i|alpha>, <i|j> = delta_{ij}
    where E = {E_i} is a diagonal and Q is a square (not necessary unitary) matrix
    doing Cholesky decomp 
            corr[t1] = X1 . X1^H 
            with X1^H = (Z e^{-E*t1/2} Q) and Z.Z^H=1
    one gets 
            A = X1^-1 corr[t2] X1^-1^H = Z e^{-E*(t2-t1)} Z^H. 
    Solving evalue problem A => (Z, e^{-E(t2-t1)}) one can extract E = {E_i} and orthogonal 
    Hilbert vectors 
            Q = e^{E*t1/2} . Z^H . X1^H
    """
    assert t1 < t2
    if False:
        # c1+c1.H = ch . ch^H
        ch = numpy.linalg.cholesky(c1 + c1.T.conj())
        chinv = numpy.linalg.inv(ch)
        A = numpy.dot(chinv, numpy.dot(c2 + c2.T.conj(), chinv.T.conj()))
        evals, evecs = numpy.linalg.eigh(A)
        meff = -numpy.log(evals) / (t2 - t1)
        Q = numpy.exp(meff * .5 * t1)[:,numpy.newaxis] * numpy.dot(ch, evecs).T.conj()
        return meff,Q
    else:
        #c1+c1.H = ch . ch^H
        ch = numpy.linalg.cholesky(c2 + c2.T.conj())
        chinv = numpy.linalg.inv(ch)
        A = numpy.dot(chinv, numpy.dot(c1 + c1.T.conj(), chinv.T.conj()))
        evals, evecs = numpy.linalg.eigh(A)
        meff = numpy.log(evals) / (t2 - t1)
        Q = numpy.exp(meff * .5 * t2)[:,numpy.newaxis] * numpy.dot(ch, evecs).T.conj()
        return meff,Q

def find_gen_meff_avg(a, idx, t1, t2):
    a_mean = get_matrix(a, idx).mean(axis=0)
    meff, Q = find_gen_meff(a_mean[t1], a_mean[t2], t1, t2)
    print(meff)
    invQ = numpy.linalg.inv(Q)
    print(Q[:,0,numpy.newaxis] / Q)
    print(invQ / invQ[0])


import math
import sys
import numpy
import lhpd
import getopt
import tsv


class c2pt_MxN:
    """ n-exponential fit 
            c_2pt_{ab} = (Q^H e^{-Et} Q)_{ab}, 
            where   Q_{ia} = <i|a>, <i|j> = delta_{ij}, i=1..N, a=1..M
                    |a> = N(q_{(a)})|\Omega> : "factorizable" smearing state
        number of degrees of freedom = (M + 1) * (len(trange) * M / 2 - N)
    """
    def __init__(self, M, N):
        self.M = M
        self.N = N
        self.tmp_en = numpy.empty(shape=(N,),  dtype=numpy.float64)
        self.tmp_en_deriv = numpy.empty(shape=(N,N), dtype=numpy.float64)
        self.tmp_Q  = numpy.empty(shape=(N,M), dtype=numpy.float64)
    
    def calc_exp_en(self, dst, src, t):
        """ dst[i] = exp(-t * E_i)
                where E_i = de_0 + ... + de_i
        """
        assert(len(dst.shape) == 1 and dst.shape == src.shape)
        sum = 0
        for i in range(len(src)):
            sum += src[i]
            dst[i] = math.exp(-t * sum)
    def calc_exp_en_deriv(self, dst, dst_der, src, t):
        """ dst_der[i,k] = d exp(-t * E_i) / d de_k, 
                where E_i = de_0 + ... + de_i
            easy to show that 
                dst[i,k] = eE[i] * delta{i>=k} * (-t),
                where   eE[i] = X_0 ... X_i,   
                        X_i = e^{-t * de_i}, 
                        d X_i / d de_k = X_i * (-t) * delta_{i==k}
        """
        ls = len(src)
        assert(len(src.shape) == 1 and 
                dst.shape == src.shape and 
                dst_der.shape == (ls, ls))
        sum = 0
        for i in range(len(src)):
            sum += src[i]
            e   = math.exp(-t * sum)
            dst[i] = e
            dst_der[i, :i+1] = -t * e
            dst_der[i, i+1:] = 0

    def func(self, t, de, Q):
        """ return f[a,b] = \sum_k Q_{ka} Q_{kb} e^{-t*E_k} """
        eE  = self.tmp_en
        self.calc_exp_en(eE, de, t)
        return numpy.dot(Q.T * eE, Q)

    def dfunc(self, t, de, Q):
        """ return 
                df_de[a,b, k] = dC_ab / d de_k = -t e_k Q_{ka} Q_{kb}
                df_Q[a,b, k,c]= dC_ab / dQ_kc = delta_{ac}Q_{kb}e_k + delta_{bc}Q_{ka}e_k
        """
        eE, eE_deriv = self.tmp_en, self.tmp_en_deriv
        self.calc_exp_en_deriv(eE, eE_deriv, de, t)

        df_de   = numpy.zeros(shape=(self.M, self.M, self.N), dtype=numpy.float64)
        df_Q    = numpy.zeros(shape=(self.M, self.M, self.N, self.M), dtype=numpy.float64)

        eE_Q= eE[:,None] * Q      # e^{-E_i*t} * Q[i,alpha]

        for a in range(self.M):
            for b in range(a+1):
                # a,b,c: fit states
                # i,j,k: Hamiltonian orthogonal states
                # e_k = e^{-E_k*t}
                
                # dC_ab / d de_k = deriv_de[k] = -t e_k Q_{ka} Q_{kb}
                df_de[a, b, :] = numpy.dot(Q[:,a] * Q[:,b], eE_deriv)
                if (a != b):
                    df_de[b, a] = df_de[a, b]

                # dC_ab / dQ_kc = deriv_Q[k, c] = delta_{ac}Q_{kb}e_k + delta_{bc}Q_{ka}e_k
                if (a == b):
                    df_Q[a, a, :, a] = 2 * eE_Q[:,a]
                else:
                    df_Q[a, b, :, a] = eE_Q[:,b]
                    df_Q[b, a, :, a] = eE_Q[:,b]
                    df_Q[a, b, :, b] = eE_Q[:,a]
                    df_Q[b, a, :, b] = eE_Q[:,a]
        return df_de, df_Q


class model_c2pt_MxN(lhpd.fitter.Model_function_Base):
    """ 
        take full matrix c2pt_{ab}
        output layout: [{tr} x [M, M]]
    """
    def __init__(self, M, N):
        self.M = M
        self.N = N
        self.c2pt = c2pt_MxN(M, N)

    def size_param(self):   return self.N * (self.M + 1)
    def par_names(self):
        return \
            [ 'de%d' % i for i in range(self.N) ] + \
            [ 'Q_%d_%d' % (i / self.M, i % self.M) for i in range(self.N * self.M) ]
    def par_limits(self):
        return \
            [ ('range', 0.1, 2.0) for i in range(self.N) ] + \
            [ None for i in range(self.N * self.M) ]
    
    def pack_data(self, r):
        """ flatten i_sm, j_sm (last two dims) """
        assert(2 <= len(r.shape))
        assert(r.shape[-2:] == (self.M, self.M))
        return r.reshape(r.shape[:-2] + (self.M * self.M,))
    def unpack_data(self, r):
        """ introduce [i_sm,j_sm] instead of the last dim """
        assert(1 <= len(r.shape))
        assert(r.shape[-1] == self.M * self.M)
        return r.reshape(r.shape[:-1] + (self.M, self.M))

    def pack_flatten_data(self, r):
        """ flatten trange, i_sm, j_sm (3 last dims): [..., trange, M, M] -> [..., :] """
        assert (3 <= len(r.shape) and r.shape[-2:] == (self.M, self.M))
        nt  = r.shape[-3]
        nd  = self.M * self.M
        return r.reshape(r.shape[:-3] + (nt * nd,))

    def calc_data(self, tr, c2pt):
        """ calc and form data to be used for fit 
            input:
                tr      t-range
                c2pt    correlators [..., t, i_sm, j_sm]
            output:
                c2pt    correlators [..., i_t x {[i_sm, j_sm]}]
        """
        assert (3 <= len(c2pt.shape) and 
                c2pt.shape[-2:] == (self.M, self.M))
        return self.pack_flatten_data(c2pt[..., tr, :, :])

    def unpack_param(self, p):
        """ parameter layout:
                p[0:N]      energies
                p[N:N+M*N]  overlaps
        """
        assert p.shape == (self.N * (self.M + 1),)
        return p[:self.N], \
               p[self.N: self.N * (self.M + 1)].reshape((self.N, self.M))

    def f_unpacked(self, t, p):
        return self.c2pt.func(t, *self.unpack_param(p))
    def f(self, t, p):
        return self.pack_data(self.c2pt.func(t, *self.unpack_param(p)))
    def f_range(self, tr, p): 
        nt = len(tr)
        nd = self.M * self.M 
        res = numpy.empty(shape=(nt * nd,), dtype=numpy.float64)
        p_de, p_Q = self.unpack_param(p)
        for it, t in enumerate(tr):
            res[it * nd : (it + 1) * nd] = self.pack_data(self.c2pt.func(t, p_de, p_Q))
        return res

    def df(self, t, p):
        """ return [data_i={[trange, i_sm, j_sm]}] """
        nd = self.M * self.M
        np = len(p)
        # df_de[a,b,k], df_Q[a,b,k,c]
        df_de, df_Q = self.c2pt.dfunc(t, *self.unpack_param(p))
        res = numpy.empty(shape=(nd, np), dtype=df_de.dtype)
        res[:, :self.N] = df_de.reshape((df_de.shape[0] * df_de.shape[1],
                                         df_de.shape[2]))
        res[:, self.N:] = df_Q.reshape((df_Q.shape[0] * df_Q.shape[1], 
                                        df_Q.shape[2] * df_Q.shape[3]))
        return res
    
    def df_range(self, tr, p):
        """ return [data_i={[trange, i_sm, j_sm]}, param_i={de, Q}] """
        nt = len(tr)
        nd = self.M * self.M
        np = len(p)
        res = numpy.empty(shape=(nt * nd, np), dtype=numpy.float64)
        p_de, p_Q = self.unpack_param(p)
        for it, t in enumerate(tr):
            df_de, df_Q = self.c2pt.dfunc(t, p_de, p_Q)
            sh = it * nd
            res[sh : sh + nd, :self.N] = df_de.reshape((df_de.shape[0] * df_de.shape[1],
                                                              df_de.shape[2]))
            res[sh : sh + nd, self.N:] = df_Q.reshape((df_Q.shape[0] * df_Q.shape[1],
                                                             df_Q.shape[2] * df_Q.shape[3]))
        return res



class model_c2pt_MxN_symm(lhpd.fitter.Model_function_Base):
    """ 
        take "L" part of symmetric matrix c2pt_{ab}
        output layout: [{tr} x {"L" [M*(M+1)/2]}]
            where indexing of "L" is [ (0,0), (1,0), (1,1), (2,0), (2,1), (2,2), ... (M-1,M-1) ]
    """
    def __init__(self, M, N):
        self.M = M
        self.N = N
        self.c2pt = c2pt_MxN(M, N)

    def size_param(self):   return self.N * (self.M + 1)
    def par_names(self):
        return \
            [ 'de%d' % i for i in range(self.N) ] + \
            [ 'Q_%d_%d' % (i / self.M, i % self.M) for i in range(self.N * self.M) ]
    def par_limits(self):
        return \
            [ ('range', 0.1, 2.0) for i in range(self.N) ] + \
            [ None for i in range(self.N * self.M) ]
    
    def pack_data(self, r):
        """ convert symmetric matrix MxM -> array[M*(M+1)/2] == L 
            where [M,M] are the last dimensions
        """
        assert(2 <= len(r.shape))
        assert(r.shape[-2:] == (self.M, self.M))
        res = numpy.empty(shape=r.shape[:-2] + (self.M * (self.M + 1) / 2,),
                          dtype=r.dtype)
        shift = 0
        for i in range(self.M):
            res[..., shift: shift+i+1] = r[..., i, 0:i+1]
            shift += i + 1
        return res
    def unpack_data(self, r):
        """ convert array[M*(M+1)/2] == L -> symmetric matrix MxM """
        assert(1 <= len(r.shape))
        assert(r.shape[-1] == self.M * (self.M + 1) / 2)
        res = numpy.empty(shape=r.shape[:-1] + (self.M, self.M),
                          dtype=r.dtype)
        shift = 0
        for i in range(self.M):
            res[..., i, 0: i + 1] = r[..., shift: shift + i + 1]
            res[..., 0: i, i] = r[..., shift: shift + i]
            shift += i + 1
        return res

    def pack_flatten_data(self, r):
        """ convert [..., trange, M, M] -> [..., :] """
        assert (3 <= len(r.shape) and r.shape[-2:] == (self.M, self.M))
        nt  = r.shape[-3]
        nd  = self.M * (self.M + 1) / 2
        res = numpy.empty(shape=r.shape[:-3] + (nt * nd,),
                          dtype=numpy.float64)
        shift = 0
        for i in range(self.M):
            for it in range(nt):
                res[..., it * nd + shift: it * nd + shift + i + 1] = r[...,it, i, 0:i+1]
            shift += i + 1
        return res

    def calc_data(self, tr, c2pt):
        """ calc and form data to be used for fit 
            input:
                tr      t-range
                c2pt    correlators [..., t, i_sm, j_sm]
            output:
                c2pt    correlators [..., i_t x "L"[i_sm >= j_sm]]
        """
        assert (3 <= len(c2pt.shape) and 
                c2pt.shape[-2:] == (self.M, self.M))
        return self.pack_flatten_data(c2pt[..., tr, :, :])

    def unpack_param(self, p):
        """ parameter layout:
                p[0:N]      energies
                p[N:N+M*N]  overlaps
        """
        assert p.shape == (self.N * (self.M + 1),)
        return p[:self.N], \
               p[self.N: self.N * (self.M + 1)].reshape((self.N, self.M))

    def f_unpacked(self, t, p):
        return self.c2pt.func(t, *self.unpack_param(p))
    def f(self, t, p):
        return self.pack_data(self.c2pt.func(t, *self.unpack_param(p)))
    def f_range(self, tr, p): 
        nt = len(tr)
        nd = self.M * (self.M + 1) / 2
        res = numpy.empty(shape=(nt * nd,), dtype=numpy.float64)
        p_de, p_Q = self.unpack_param(p)
        for it, t in enumerate(tr):
            res[it * nd : (it + 1) * nd] = self.pack_data(self.c2pt.func(t, p_de, p_Q))
        return res

    def df(self, t, p):
        nd = self.M * (self.M + 1) / 2
        np = len(p)
        df_de, df_Q = self.c2pt.dfunc(t, *self.unpack_param(p))
        df_Q_r = df_Q.reshape(df_Q.shape[0:2] + (-1,))
        res = numpy.empty(shape=(nd, np), dtype=df_de.dtype)
        shift = 0
        for i in range(self.M):
            res[shift : shift + i + 1, :self.N] = df_de[i, 0 : i+1]
            res[shift : shift + i + 1, self.N:] = df_Q_r[i, 0 : i+1]
            shift += i + 1
        return res
    
    def df_range(self, tr, p):
        nt = len(tr)
        nd = self.M * (self.M + 1) / 2
        np = len(p)
        res = numpy.empty(shape=(nt * nd, np), dtype=numpy.float64)
        p_de, p_Q = self.unpack_param(p)
        for it, t in enumerate(tr):
            df_de, df_Q = self.c2pt.dfunc(t, p_de, p_Q)
            df_Q_r = df_Q.reshape(df_Q.shape[0:2] + (-1,))
            shift = it * nd
            for i in range(self.M):
                res[shift : shift + i + 1, :self.N] = df_de[i, 0 : i+1]
                res[shift : shift + i + 1, self.N:] = df_Q_r[i, 0 : i+1]
                shift += i + 1
        return res





class model_c2pt_MxN_symm_div00t0(lhpd.fitter.Model_function_Base):
    """ 
        take "L" part of symmetric matrix c2pt_{ab} and divide by c2pt_{00}(tr[0])
        output layout: [{tr} x {"L" [M*(M+1)/2]}] 
            where indexing of "L" is [ (0,0) (1,0), (1,1), (2,0), (2,1), (2,2), ... (M-1,M-1) ]
            except for [0 x (0,0)] element (used to normalize all correlators)
    """
    def __init__(self, M, N):
        self.M = M
        self.N = N
        self.c2pt = model_c2pt_MxN(M,N)
        #self.c2pt = model_c2pt_MxN_symm(M,N)
        self.tmp_p = numpy.empty(shape=(self.N * (self.M + 1),),
                                 dtype=numpy.float64)

    def size_param(self):   
        # Q_0_0 == 1 fixed
        return self.N * (self.M + 1) - 1
    def par_names(self):
        # Q_0_0 == 1 by definition
        return \
            [ 'de%d' % i for i in range(self.N) ] + \
            [ 'Q_%d_%d' % (i / self.M, i % self.M) for i in range(1, self.N * self.M) ]
    def par_limits(self):
        return \
            [ ('range', 0.1, 2.0) for i in range(self.N) ] + \
            [ None for i in range(self.M * self.N - 1) ]
    
    def calc_data(self, tr, corr):
        r = self.c2pt.calc_data(tr, corr)
        return r[..., 1:] / r[..., 0:1]

    def int_param(self, p):
        """ restore Q_00 == 1 """
        int_p = numpy.empty(shape=(self.N * (self.M + 1),),
                            dtype=numpy.float64)
        int_p[:self.N]      = p[:self.N]
        int_p[self.N]       = 1.
        int_p[self.N + 1:]  = p[self.N:]
        return int_p

    def f_unpacked(self, t, p):
        r = self.c2pt.f_unpacked(t, self.int_param(p))
        return r / r[0, 0]

    def f(self, t, p): return None      # has no sense for this model
    def df(self, t, p): return None     # has no sense for this model
    def f_range(self, tr, p):
        r = self.c2pt.f_range(tr, self.int_param(p))
        return r[1:] / r[0]
    def df_range(self, tr, p):
        int_p = self.int_param(p)
        r   = self.c2pt.f_range(tr, int_p)
        dr  = self.c2pt.df_range(tr, int_p)

        res = numpy.empty(shape=(len(r) - 1, self.size_param()), dtype=r.dtype)
        
        # remove d / d Q_00
        res[:, :self.N] = dr[1:, :self.N] / r[0] - \
                            numpy.outer(r[1:], dr[0, :self.N] / r[0]**2)
        res[:, self.N:] = dr[1:, self.N + 1:] / r[0] - \
                            numpy.outer(r[1:], dr[0, self.N + 1:] / r[0]**2)
        return res

    

"""
#example
aff_i   = lhpd.aff_io.aff_import(4)
cset=lhpd.init_corr_set_2pt_rotations(numpy.array(param['latsize']), [0,0,0])
data=read_data_aff_AB(cset, aff_i, param['data_list'], param['aff_path'], param['src_smear'], param['snk_smear'], [0,0,0], param['mq_tag'])
"""
def read_data_aff_AB(cset, aff_import, data_list, data_dir, 
                  A_sm_list, B_sm_list, psnk, aff_postfix_=''):
    from numpy import str
    import re
    if data_dir != '': data_dir = data_dir + '/'
    """ return [data][t][i_sm][j_sm] with smearing taken from A_sm_list and B_sm_list """
    # this part reads records from data_list and loads necessary data
    if type(data_list)==file:
        aff_postfix=''
        if (aff_postfix_ != ''): aff_postfix = '/' + aff_postfix_
        ens = []
        for str in data_list:
            mo = re.match('^(\S+)\s+(\S+)\s+\{(.+),(.+),(.+),(.+)\}\s+(\S+)$', str)
            aff_file, aff_prefix = mo.groups()[0:2]
            aff_r = aff_import.open(data_dir + '/' + aff_file)

            csrc = map(int, mo.groups()[2:6])
            had_str = mo.groups()[6]
            elem= None
            nt  = None
            ni  = len(A_sm_list)
            nj  = len(B_sm_list)
            for i in range(ni):
                for j in range(nj):
                    c2pt = lhpd.c2pt_point(cset, had=had_str, csrc=csrc, aff_r=aff_r,
                                      prefix=('%s/%s-%s%s' %(aff_prefix, 
                                              A_sm_list[i], B_sm_list[j], aff_postfix)))\
                            .get_avg_equiv(psnk)

                    if (elem is None) or (nt is None):
                        nt  = len(c2pt)
                        elem= numpy.empty(shape=(nt, ni, nj), dtype=numpy.float64)
                    assert len(c2pt)==nt
                    elem[:,i,j] = c2pt

            ens.append(elem)

        return ens
    # data_list=<filename> wrapper
    elif type(data_list)==str:
        ls_f = open(data_list, 'r')
        ens = read_data_aff_AB(cset, aff_i, ls_f, data_dir, 
                               A_sm_list, B_sm_list, psnk, aff_postfix_)
        ls_f.close()
        return ens
    # data_list=<list-of-filenames> wrapper
    elif type(data_list)==list:
        ens = []
        for name in data_list:
            assert type(name)==str
            ens.extend(read_data_aff_AB(cset, aff_i, name, data_dir, 
                                        A_sm_list, B_sm_list, psnk, aff_postfix_))
        return ens
    else: raise ValueError("type(data_list) = %s" % str(type(data_list)))
            

"""
param = {
    #'model':        'symm',
    'model':        'symm_div00t0',
    'n_states':     3,                  # number of states in fit
    'smear_list':   [0, 2],             # list of smearing indices in TSV
    #'start_val':    [.6, .5, .5, 1., 1., 1., 1., 1.,],     # m=2 n=3 
    #'start_val':    [.6, .5, 1., 1., 1., ],                 # m=2 n=2
    'src_list':     [                   # list of sources in AFF
            'uW0.uW0.dW0',
            'uW1.uW0.dW0',
            'uW0.uW1.dW0',
            'uW1.uW1.dW0',
            'uW0.uW0.dW1',
            'uW1.uW0.dW1',
            'uW0.uW1.dW1',
            'uW1.uW1.dW1' ],
    
    'tsv_datafile':     'data',             # TSV data file
    'n_smear':      5,
    'trange':       range(2,10),
    'cov_diag':     False,
    
    'rsplan':       ('jk', 6)
}
"""

def read_data_tsv(datafile, n_smear, smear_list):
    from numpy import s_, ix_, float64
    #  [i_data,0,t,n_sm*i_sm+j_sm]
    data    = numpy.array(tsv.read_float(open(datafile, 'r')),
                          dtype=float64)
    n_data_raw  = data.shape[0]
    nt      = data.shape[2]
    assert(data.shape[1] == 1 and data.shape[-1] == n_smear**2)
    # select data
    return data.reshape((n_data_raw, nt, n_smear, n_smear)) \
                            [s_[:,:] + ix_(smear_list, smear_list)]


def main_execute(param, start_val):
    from numpy import s_, ix_, float64
    
    # read data
    c2pt_ab     = read_data_tsv(param['tsv_datafile'], param['n_smear'], param['smear_list'])
    
    nt      = c2pt_ab.shape[1]
    n_sm    = param['n_smear']
    smear_list = param['smear_list']
    fit_m   = len(smear_list)
    fit_n   = param['n_states']
    trange  = param['trange']
    model = {
        'symm':         model_c2pt_MxN_symm,
        'symm_div00t0': model_c2pt_MxN_symm_div00t0
        }[param['model']](fit_m, fit_n)

    # resample
    c2pt_rs     = lhpd.resample(model.calc_data(trange, c2pt_ab),
                                             param['rsplan'])
    n_data  = len(c2pt_rs)

    # find avg, var, cov
    c2pt_avg, c2pt_sigma, c2pt_cov \
                = lhpd.calc_avg_err_ncov(c2pt_rs, param['rsplan'])
    if (param['cov_diag']): c2pt_cov = numpy.identity(len(c2pt_sigma), float64)

    # first n: de_i > 0; second m*n Q_ia, any
    fitter  = lhpd.fitter.Fitter(model,
                    trange, c2pt_avg, c2pt_sigma, c2pt_cov, 
                    lhpd.fitter.reparam(*model.par_limits()))

    # fit of average
    p_init  = numpy.array(start_val)
    a = fitter.fit(p_init)
    print(a[0])

    # cycle over ensemble
    res_rs  = numpy.empty(shape=(n_data, model.size_param()), dtype=float64)
    chi2_rs = numpy.empty(shape=(n_data,), dtype=float64)
    ndf = fitter.get_ndf()
    for i in range(len(c2pt_rs)):
        res_rs[i], chi2_rs[i] = fitter.fit_savestate(c2pt_rs[i])
        print(i, "\t", res_rs[i], "\t", chi2_rs[i]/ndf)
        for t in []:
            Cmatr = model.f_unpacked(t, res_rs[i])
            C_eval = numpy.linalg.eigvalsh(Cmatr)
            C_eval.sort()
            print("\t", t, "\t", C_eval, "\t", C_eval[-1]/C_eval[0])

    # print results
    res_avg, res_cov = lhpd.calc_avg_cov(res_rs, param['rsplan'])
    lhpd.pprint_ve_cov(res_avg, res_cov, model.par_names())

    chi2_avg, chi2_cov=lhpd.calc_avg_cov(chi2_rs, param['rsplan'])
    sys.stdout.write('chi2/ndf\t%s\n' %
            lhpd.pprint_ve_cov(chi2_avg / ndf, chi2_cov / ndf**2))
    print("(n_data, n_var, ndf) =", (len(c2pt_avg), model.size_param(), ndf))
    return res_avg, res_cov

