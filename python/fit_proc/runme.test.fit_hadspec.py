import sys
import math
import numpy
import lhpd
from fit_proc import fit_hadspec

cset=lhpd.init_corr_set_2pt([32,32,32,64], 1)
aff_i=lhpd.aff_io.aff_import(4)
ens=fit_hadspec.load_ensemble(aff_i, cset, ['/Users/syritsyn/DATA/rbc32c64_m004/hadspec/hadspec_G7.284-G7.284_nucleon.list'], '/Users/syritsyn/DATA/rbc32c64_m004/hadspec')
fitter=fit_hadspec.init_fitter(cset, ens, numpy.array([0,0,1]), range(2,26))
fitter.fit(numpy.array([0.5,1.,.5,1.,1.]), method='lma')


nucl_mom=numpy.array([0,0,1])
t_range=range(2,26)
c2pt=[ x.get_avg_equiv(nucl_mom) for x in ens ]
meff_jk = [ numpy.array([ math.log(c2[t] / c2 [t+1]) for t in t_range ]) 
            for c2 in lhpd.resample_iter(c2pt, resampled='jk', binsize=16) ]
param=[fitter.fit_savestate(m) for m in meff_jk]

import cProfile
import pstats
cProfile.run('param1=[fitter.fit_savestate(m) for m in meff]', 'fit_savestate_cycle.v2.cProf')
st2=pstats.Stats('fit_savestate_cycle.v2.cProf')
st2.strip_dirs().sort_stats('cumulative').print_callers()


ge=lhpd.fitter.gauss_estimator(resampled='jk')
for p in param:
    ge.add(p)

ge.estimate()


###### new life with G.E.C.K.

import sys
import math
import numpy
import lhpd
from fit_proc import fit_hadspec

# fit
param={'latsize':[32,32,32,64], 'trange':range(2,26), 'data_lists':['/Users/syritsyn/DATA/rbc32c64_m004/hadspec/hadspec_G7.284-G7.284_nucleon.list'], 'data_dir':'/Users/syritsyn/DATA/rbc32c64_m004/hadspec', 'max_aff_files':4, 'rsplan':('jk',16), 'start_val':[.5,1.,.5,1.,1.], 'nucl_mom':[0,0,1], 'cov_diag':False, 'method':'meff_2exp_osc'}
param={'latsize':[28,28,28,64], 'trange':range(2,26), 'data_lists':['/Users/syritsyn/DATA/milc28c64_m010/hadspec/hadspec_list'], 'data_dir':'', 'max_aff_files':4, 'rsplan':('jk',16), 'start_val':[.5,1.,.5,1.,1.], 'nucl_mom':[0,0,1], 'cov_diag':False, 'method':'meff_2exp_osc'}

fit_hadspec.main_execute(param)

# meff
cset    = lhpd.init_corr_set_2pt_rotations(numpy.array(param['latsize']), param['nucl_mom'])
aff_i   = lhpd.aff_io.aff_import(param['max_aff_files'])
ens     = fit_hadspec.load_ensemble(aff_i, cset, param)
lhpd.pprint_ve_cov(*(lhpd.meff_estimate(ens, [0,0,0], range(0, 26), rsplan=('jk',16)) + (None,sys.stdout,False,False)))

# reload
import my_basic
my_basic.my_reload('lhpd')
reload(sys.modules['fit_proc.fit_hadspec'])

def mk_ratio_2nd_factor(ens, p1, p2, T, rsplan=None):
    """
    compute sqrt{c2'(t)c2(T-t)/c2(t)c2'(T-t)}
    """
    assert (0 < T and T < cset.ls[3])
    c1  = [ x for x in lhpd.resample_iter([ x.get_avg_equiv(p1) for x in ens ], rsplan=rsplan) ]
    c2  = [ x for x in lhpd.resample_iter([ x.get_avg_equiv(p2) for x in ens ], rsplan=rsplan) ]
    r2  = [ numpy.array([ math.sqrt(c2[i][t] * c1[i][T-t] / c1[i][t] / c2[i][T-t]) 
                          for t in range(T+1) ])
            for i in range(len(c1)) ]
    return lhpd.calc_avg_cov(r2, rsplan=rsplan)
