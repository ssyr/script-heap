import sys
import math
import numpy
import lhpd
from fit_proc import fit_hadspec

cset=lhpd.init_corr_set_2pt([32,32,32,64], 1)
aff_i=lhpd.aff_io.aff_import(4)
ens=fit_hadspec.load_ensemble(aff_i, cset, ['/Users/syritsyn/DATA/rbc32c64_m004/hadspec/hadspec_G7.284-G7.284_nucleon.list'], '/Users/syritsyn/DATA/rbc32c64_m004/hadspec')
fitter=fit_hadspec.init_fitter(cset, ens, numpy.array([0,0,1]), range(2,26))
fitter.fit(numpy.array([0.5,1.,.5,1.,1.]), method='lma')


nucl_mom=numpy.array([0,0,1])
t_range=range(2,26)
c2pt=[ x.get_avg_equiv(nucl_mom) for x in ens ]
meff_jk = [ numpy.array([ math.log(c2[t] / c2 [t+1]) for t in t_range ]) for c2 in lhpd.resample_iter(c2pt, resampled='jk', binsize=16) ]
param=[fitter.fit_savestate(m) for m in meff_jk]

import cProfile
import pstats
cProfile.run('param1=[fitter.fit_savestate(m) for m in meff]', 'fit_savestate_cycle.v2.cProf')
st2=pstats.Stats('fit_savestate_cycle.v2.cProf')
st2.strip_dirs().sort_stats('cumulative').print_callers()


ge=lhpd.fitter.gauss_estimator(resampled='jk')
for p in param:
    ge.add(p)

ge.estimate()
