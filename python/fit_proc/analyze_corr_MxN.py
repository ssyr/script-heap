from __future__ import print_function
from past.builtins import execfile
import sys
import numpy
import math
import tsv
import lhpd

"""
Extraction of effective masses and vectors
    * by solving generalized EV
    * TODO by fitting
Look for definitions in LABBOOK 240.1
"""

"""
execfile("analyze_corr_MxN.py")
execfile('config.py')
aff_i=ld.aff_io.aff_import(4)
ldata=get_data_aff(aff_i, param['aff_path'], param['data_list'], param['src_smear'], param['snk_smear'], param['mq_tag'], [0,0,0], param['latsize'])
data_rs=ld.resample(ldata.get_data(), param['rsplan'])
res_rs=gen_eff_mass_range(data_rs, 2, [3,4])
tsv.write_val_err(sys.stdout, *estimate(res_rs, param['rsplan']))
"""

def make_vec_std(vec):
    from numpy import array, abs
    vec_std = abs(array(vec, dtype=numpy.int))
    vec_std.sort()
    return vec_std
def make_list_eq_vec_std(vec):
    from numpy import array, abs
    vec_std = make_vec_std(vec)
    vec_list = []
    # temporary solution for len(vec)==3
    assert(len(vec_std) == 3)
    vec_sq = (vec_std**2).sum()
    xmax = int(math.floor(math.sqrt(vec_sq)))
    for x in range(-xmax, +xmax+1):
        ymax = int(math.floor(math.sqrt(vec_sq - x*x)))
        for y in range(-ymax, +ymax+1):
            zmax = int(math.floor(math.sqrt(vec_sq - x*x - y*y)))
            if (x*x + y*y + zmax*zmax == vec_sq):
                vec_list.append([x, y, zmax])
                if zmax != 0: vec_list.append([x, y, -zmax])
    return array(vec_list, dtype=numpy.int)

def time_reverse_c2pt(a):
    ar = numpy.empty_like(a)
    ar[0] = a[0]
    ar[1:]= -a[:0:-1]
    return ar 

class get_data_aff:
    def __init__(self, aff_in, aff_path_prefix, 
                 data_list_file, src_sm_list, snk_sm_list, mq_tag, p3, latsize):
        self.aff_in = aff_in
        if aff_path_prefix is None or aff_path_prefix == '': self.aff_path_prefix = ''
        else: self.aff_path_prefix = aff_path_prefix + '/'
        self.data_list_file = data_list_file
        self.src_smear_list = src_sm_list
        self.snk_smear_list = snk_sm_list
        self.mq_tag = mq_tag
        self.p3std = make_vec_std(p3)
        assert(len(self.p3std) == 3)
        self.p3list= make_list_eq_vec_std(p3)
        self.ls = latsize
        assert(len(self.ls) == 4)

        self.data = None
        self.read_data()

    def read_data(self):
        import re
        from lhpd.aff_io import aff_key_hadron_2pt
        self.data = []
        f = open(self.data_list_file, "r")
        n_src_sm = len(self.src_smear_list)
        n_snk_sm = len(self.snk_smear_list)
        n_mom   = len(self.p3list)
        n_t     = self.ls[-1]
        for str in f:
            mo = re.match('^(\S+)\s+(\S+)\s+\{(.+),(.+),(.+),(.+)\}\s+(\S+)$', str)
            aff_file, aff_prefix = mo.groups()[0:2]
            csrc = map(int, mo.groups()[2:6])
            had = {
                'proton_3'          : 'proton_3', 
                'proton_negpar_3'   : 'proton_negpar_3'
                }[mo.groups()[6]]
            need_time_reverse = {
                'proton_3'          : False, 
                'proton_negpar_3'   : True
                }[had]
                
            aff_r = self.aff_in.open(self.aff_path_prefix + aff_file)
            data = numpy.empty((n_src_sm, n_snk_sm, n_mom, n_t), numpy.complex128)
            for i_src_sm, src_sm in enumerate(self.src_smear_list):
                for i_snk_sm, snk_sm in enumerate(self.src_smear_list):
                    sm = src_sm + '-' + snk_sm
                    for i_p3, p3 in enumerate(self.p3list):
                        input = numpy.array(aff_r.read(aff_prefix + '/' + 
                                    sm + '/' + self.mq_tag + '/' +
                                    aff_key_hadron_2pt(csrc[3], csrc[0:3], p3, had)))
                        assert(len(input) == n_t)
                        # FIXME Should we use complex part as well?
                        if need_time_reverse: input = time_reverse_c2pt(input)
                        data[i_src_sm, i_snk_sm, i_p3] = input
            self.data.append(data)
    
    def get_data(self):
        """ return [i_data, i_sm, t] """
        # self.data[i_data, i_src_sm, i_snk_sm, i_p3, t]
        return numpy.array(self.data, numpy.complex128).mean(axis=3).transpose(
                        (0, 3, 1, 2)).copy()

def calc_gen_eff_mass(c2pt0, c2pt1, t0, t1):
    """ compute effective masses for 1 sample
        input: c2pt{0,1}[i_sm, j_sm] at t0, t1 
        output: Eeff, vec
    """
    from numpy.linalg import cholesky, eigh, inv
    from numpy import dot
    assert (t0 < t1)
    dt = t1 - t0
    A = cholesky((c2pt0 + c2pt0.T.conj()) / 2.)         # c2pt0 = A . A^T
    Ainv = inv(A)
    eval, Z = eigh(dot(dot(Ainv, (c2pt1 + c2pt1.T.conj()) / 2.), Ainv.T.conj())) # ==Z . diag{eval} . Z^T
    Eeff = -numpy.log(eval) / dt
    #A = Q^T . e^{-E*t0/2) . Z^T
    Q = ( dot(A, Z) * numpy.exp(Eeff * t0 / 2.)[None,:] ).T.conj()
    return Eeff, Q


def gen_eff_mass_range(data, t0, trange, project=None):
    """ 
        cycle through data and trange and print effective mass and overlap vectors
        tsv-print data as [i_data, i_state, i_t, {E; Q[i_state,:]} ]
        data is sorted by eff.energy in i_state index
    """
    from numpy import dot
    n_data, tmax, n_sm, n_sm_snk = data.shape
    n_t_range = len(trange)
    if project is None: project = numpy.identity(n_sm)
    assert(n_sm == n_sm_snk)
    n_state = project.shape[1]
    res = numpy.empty((n_data, n_state, n_t_range, n_state + 1), numpy.float64)
    for i_d in range(len(data)):
        for i_t, t in enumerate(trange):
            assert (t0 < t and t < tmax)
            Eeff, Q = calc_gen_eff_mass(
                dot(project.T, dot(data[i_d, t0], project)), 
                dot(project.T, dot(data[i_d, t], project)), 
                t0, t)
            isort = Eeff.argsort()
            dQ = Q / Q[isort[0], :]
            for i, i_s in enumerate(isort):
                res[i_d, i, i_t, 0]  = Eeff[i_s]
                res[i_d, i, i_t, 1:] = dQ[i_s,:]
    return res

def gen_c2pt_geval_range(data, t0, trange, project=None):
    """ 
        cycle through data and trange and print effective mass and overlap vectors
        tsv-print data as [i_data, i_state, i_t, {E; Q[i_state,:]} ]
        data is sorted by eff.energy in i_state index
    """
    from numpy import dot
    def calc_c2pt_geval(c2pt0, c2pt1, t0, t1):
        """ compute effective masses for 1 sample
            input: c2pt{0,1}[i_sm, j_sm] at t0, t1 
            output: Eeff, vec
        """
        from numpy.linalg import cholesky, eigh, inv
        from numpy import dot
        A = cholesky((c2pt0 + c2pt0.T.conj()) / 2.)         # c2pt0 = A . A^T
        Ainv = inv(A)
        eval, Z = eigh(dot(dot(Ainv, (c2pt1 + c2pt1.T.conj()) / 2.), 
                        Ainv.T.conj())) # ==Z . diag{eval} . Z^T
        return eval

    n_data, tmax, n_sm, n_sm_snk = data.shape
    n_t_range = len(trange)
    if project is None: project = numpy.identity(n_sm)
    assert(n_sm == n_sm_snk)
    n_state = project.shape[1]
    res = numpy.empty((n_data, 1, n_t_range, n_state), numpy.float64)
    for i_d in range(len(data)):
        for i_t, t in enumerate(trange):
            eval = calc_c2pt_geval(
                dot(project.T, dot(data[i_d, t0], project)), 
                dot(project.T, dot(data[i_d, t], project)), 
                t0, t)
            eval.sort()
            if t < t0: res[i_d, 0, i_t, :]  = eval
            else: res[i_d, 0, i_t, :]  = eval[::-1]
    return res

def estimate(data, rsplan=None):
    return (data.mean(0),
            data.std(0) * math.sqrt(lhpd.rs_scale_var(rsplan, len(data))))

def print_res(res, rsplan=None):
    import sys, tsv
    #tsv.write_format(sys.stdout, res)
    #tsv.write_val_err(sys.stdout, res.mean(0)[None,...], 
    #                res.var(0)[None,...] * lhpd.rs_scale_var(rsplan, len(res)))
    #tsv.write_format(sys.stdout, res.mean(0)[None,...], '%6e') ; print('\n\n'; tsv.write_format(sys.stdout, res.var(0)[None,...] * lhpd.rs_scale_var(rsplan, len(res)), '%6e'))

# FIXME not needed, remove
def make_smear_list(src_smear_list, snk_smear_list):
    res = []
    for src_sm in src_smear_list:
        for snk_sm in snk_smear_list:
            res.append(src_sm + '-' + snk_sm)
    return res

# FIXME not needed, remove
def make_data(data, n_sm_src, n_sm_snk): 
    """ return [i_data, i_t, i_sm, j_sm] """
    res = data.get_data_avg_equiv_mom()
    n_data, n_sm, n_t = data.shape
    n_sm_src = len(src_smear_list)
    n_sm_snk = len(snk_smear_list)
    assert n_sm
    return res.reshape((n_data, n_))
    return data.get_data_avg_equiv_mom().transpose(axes=(0, 2, 1))\
                .reshape(n_data, n_t, n_sm_src, n_sm_snk).copy()




def calc_avg(a):
    a = numpy.asarray(a)
    return a.mean(0)
def calc_avg_err(a, rsplan=None):
    a = numpy.asarray(a)
    return a.mean(0), numpy.sqrt(a.var(0) * lhpd.resample_var_factor(rsplan, len(a)))
