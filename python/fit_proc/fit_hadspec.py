from __future__ import print_function
# interpreter?
import math
import sys
import numpy
import lhpd
import getopt
import aff
import tsv
from lhpd.pyfit import multiexp
from exceptions import *

def expand_range(r):
    if list == type(r): return r
    elif tuple == type(r): return range(r[0], r[1])
    else: raise ValueError('range=%s' % str(r))

def load_ensemble(aff_i, cset, param):
    if (len(param['data_lists']) <= 0): raise ValueError('no data list files')
    ens = []
    for name in param['data_lists']:
        ls_f = open(name, 'r');
        ens.extend(lhpd.init_ensemble_2pt(cset, ls_f, aff_i, param['data_dir']))
        ls_f.close()
    return ens

def calc_meff(cset, ens, param):
    c2pt = [ x.get_avg_equiv(param['nucl_mom']) for x in ens ]
    tr = expand_range(param['trange'])
    meff_rs = [ numpy.array([ math.log(c2[t] / c2 [t+1]) for t in tr ])
         for c2 in lhpd.resample_iter(c2pt, rsplan=param['rsplan']) ]
    meff_avg, meff_sigma, meff_cov = lhpd.calc_avg_err_ncov(meff_rs, rsplan=param['rsplan'])
    if (param['cov_diag']): meff_cov = numpy.identity(len(meff_sigma))
    return (meff_rs, meff_avg, meff_sigma, meff_cov)

def combine_def_list(a_in, a_def):
    assert(len(a_in) == len(a_def))
    a = list(a_in)
    for i, a_i in enumerate(a):
        if not None is  a_i: a[i] = a_def[i]
    return a

def init_hadspec_fitter(cset, ens, param):
    tr = expand_range(param['trange'])
    if (param['method'] == 'meff_2exp_osc'):
        model_func = multiexp.Meff(multiexp.Nexp_osc_ladder_c2pt(2))
        if 'start_val' in param: 
            assert len(param['start_val']) == len(model_func.par_names())
        meff_rs, meff_avg, meff_sigma, meff_cov = calc_meff(cset, ens, param)
        range_list = [
                ('range', 0., None),
                None,
                ('range', 0., None),
                None,
                ('range', 0., None) ]
        if 'range_val' in param:
            range_list = combine_def_list(param['range_val'], range_list)

        fitter = lhpd.fitter.Fitter(
                model_func,
                tr, meff_avg, meff_sigma, meff_cov,
                lhpd.fitter.reparam(*range_list))
        return (fitter, meff_rs, meff_avg, meff_sigma, meff_cov)
    if (param['method'] == 'meff_2exp'):
        model_func = multiexp.Meff(multiexp.Nexp_ladder_c2pt(2))
        if 'start_val' in param: 
            assert len(param['start_val']) == len(model_func.par_names())
        meff_rs, meff_avg, meff_sigma, meff_cov = calc_meff(cset, ens, param)
        range_list = [
                ('range', 0., None),
                None,
                ('range', 0., None) ]
        if 'range_val' in param:
            range_list = combine_def_list(param['range_val'], range_list)
        fitter = lhpd.fitter.Fitter(
                model_func,
                tr, meff_avg, meff_sigma, meff_cov,
                lhpd.fitter.reparam(*range_list))
        return (fitter, meff_rs, meff_avg, meff_sigma, meff_cov)
    # TODO: add other modes
    if (param['method'] == 'c2pt_2exp'):
        model_func = multiexp.Nexp_ladder_c2pt(2)
        tr = expand_range(param['trange'])
        c2pt_rs     = [ x.get_avg_equiv(param['nucl_mom'])[tr] for x in ens ]
        c2pt_avg, c2pt_sigma, c2pt_cov = lhpd.calc_avg_err_ncov(c2pt_rs, rsplan=param['rsplan'])
        if (param['cov_diag']): c2pt_cov = numpy.identity(len(c2pt_sigma))
        range_list = [
                None,
                ('range', 0., 2.),
                None,
                ('range', 0.01, 2.) ]
        if 'range_val' in param:
            range_list = combine_def_list(param['range_val'], range_list)
        fitter = lhpd.fitter.Fitter(
                model_func,
                tr, c2pt_avg, c2pt_sigma, c2pt_cov,
                lhpd.fitter.reparam(*range_list))
        return fitter, c2pt_rs, c2pt_avg, c2pt_sigma, c2pt_cov

    else: raise RuntimeError('unknown fitting method %s' % method)


def init_accumulators(param, fitter):
    tr = list(expand_range(param['trange']))
    tr.sort()
    if ('show_pts' in param and 0 < param['show_pts']):
        tr_detail = numpy.arange(tr[0], tr[-1], 
                                 float(tr[-1]-tr[0])/param['show_pts'],
                                 dtype=numpy.float64)
    else: tr_detail = tr

    # here, accumulators take tuple (param_array, param_dict)
    tr_str = map(str, tr)
    tr_detail_str = map(str, tr_detail)
    def fitting_func(p):
        return fitter.func.f_range(tr_detail, p[0])

    def exc_contrib(p):
        pdict = p[1]
        return numpy.array([ abs(pdict['c2pt_c1']) * math.exp(-pdict['c2pt_de1_0'] * t)
                             for t in tr], dtype=numpy.float64)
    def exc_contrib_3pt(p):
        pdict = p[1]
        return numpy.array([ math.sqrt(abs(pdict['c2pt_c1'])) * math.exp(-pdict['c2pt_de1_0'] * t)
                             for t in tr], dtype=numpy.float64)
    def osc_contrib(p):
        pdict = p[1]
        return numpy.array([ abs(pdict['c2pt_cosc']) * math.exp(-pdict['c2pt_deosc_0'] * t)
                             for t in tr], dtype=numpy.float64)
    acc_list = [];
    acc_list.append(lhpd.fitter.gauss_estimator(
                fitting_func,
                rsplan=param['rsplan'],
                title=tr_detail_str,
                head='# fitting function'))
    acc_list.append(lhpd.fitter.gauss_estimator(
                exc_contrib,
                rsplan=param['rsplan'],
                title=tr_str,
                head='# 1st excited contribution'))
    acc_list.append(lhpd.fitter.gauss_estimator(
                exc_contrib_3pt,
                rsplan=param['rsplan'],
                title=tr_str,
                head='# 1st excited contribution to 3pt ratio'))
    if (param['method'] == 'meff_2exp_osc'):
        acc_list.append(lhpd.fitter.gauss_estimator(
                    osc_contrib,
                    rsplan=param['rsplan'],
                    title=tr_str,
                    head='# oscillating contribution'))
    return acc_list


def main_execute(param):
    cset    = lhpd.init_corr_set_2pt_mom2_max(
                numpy.array(param['latsize']), 
                (numpy.array(param['nucl_mom'], dtype=int)**2).sum())
    aff_i   = lhpd.aff_io.aff_import(param['max_aff_files'])
    ens     = load_ensemble(aff_i, cset, param)
    hspecfit= init_hadspec_fitter(cset, ens, param)
    a = hspecfit[0].fit(numpy.array(param['start_val']))
    print("initial\t", a[0])
    tr  = expand_range(param['trange'])
    hspecfit_res_rs = []
    chi_fit_rs  = []
    accum   = init_accumulators(param, hspecfit[0])
    ndf     = hspecfit[0].get_ndf()

    # cycle over resampled ensemble, starting from the overall average
    for i_data, meff in enumerate(hspecfit[1]):
        # FIXME chi2 here is computed incorrectly for no resampling
        # because if _no_resampling_is_performed, the correct cov.matrix should be 
        # (N-1) times larger; jk-resampled values "group" around the mean and reproduce
        # the correct chi2 with suppressed variation (due to resampling)
        fit_res, fit_chi2 = hspecfit[0].fit_savestate(meff)
        hspecfit_res_rs.append(fit_res)
        chi_fit_rs.append(numpy.array([ fit_chi2 / ndf ]))
        print(i_data, "\t", fit_res, "\t", fit_chi2 / ndf)
        
        pdict = multiexp.make_param_dict(hspecfit[0].func.par_names(), fit_res)
        for a in accum: a.add((fit_res, pdict))
        
        if ('DEBUG_ECHO' in param): sys.stderr.write('.')


    if ('DEBUG_ECHO' in param): sys.stderr.write('\n')

    hspecfit_avg, hspecfit_cov = lhpd.calc_avg_cov(hspecfit_res_rs, rsplan=param['rsplan'])
    lhpd.pprint_ve_cov(hspecfit_avg, hspecfit_cov, hspecfit[0].func.par_names())

    chi2_avg, chi2_cov = lhpd.calc_avg_cov(chi_fit_rs, rsplan=param['rsplan'])
    lhpd.pprint_ve_cov(chi2_avg, chi2_cov, ['chi2/ndf'])

    sys.stdout.write("# Eeff vs t\n");
    lhpd.pprint_ve_cov(
            *(lhpd.meff_estimate(ens, param['nucl_mom'], 
                                              range(0, 26), rsplan=param['rsplan']) 
                + (None,sys.stdout,False,False)))
    for a in accum:
        a.write(None, False, False)
        sys.stdout.write("\n\n")

    if ('func_range' in param):
        func = []
        save_tr = numpy.array(param['func_range']);
        for p in hspecfit_res_rs:
            func.append(hspecfit[0].func.f_range(save_tr, p))
        f_avg, f_std = lhpd.calc_avg_err(numpy.array(func), param['rsplan'])
        print(save_tr.shape, f_avg.shape, f_std.shape)
        tsv.write_format(open(param['save_range'], 'w'), 
                numpy.array([ save_tr, f_avg, f_std ]).T)




    
def print_usage(prg_name):
    import sys
    sys.stderr.write('Usage:\n%s' % prg_name)
    sys.stderr.write(
"""
\t\t--data=<list-file> ...
\t\t[--data-dir=<top-data-dir>]
\t\t[--max-aff-files=<max-aff-files>]
\t\t--latsize=<Lx>,<Ly>,<Lz>,<Lt>
\t\t--mom=<Px>,<Py>,<Pz>
\t\t--trange=tmin:tmax
\t\t[--jk-bin=<jk-bin>]
\t\t[--cov-diag]
\t\t[--start-val=<param1>,...]
\t\t[--method=?]
"""
        )

from fit_proc.common import *

def parse_options(argv):
    param = {                       #default values
        'latsize'           : None,
        'trange'            : None,
        'data_lists'        : [],
        'data_dir'          : '',
        'max_aff_files'     : 4,
        'rsplan'            : ('jk',1),
        'start_val'         : [ 0.5, 1.0, 0.5, 1.0, 1.0 ],
        'nucl_mom'          : [0,0,0],
        'cov_diag'          : False,
        'method'            : 'meff_2exp_osc'
    }
    opt_list, rest_arg = \
         getopt.getopt(argv, '', 
                       [ 
                         'latsize=',
                         'trange=',
                         'data=',
                         'data-dir=',
                         'max-aff-files=',
                         'rsplan=',
                         'start-val=',
                         'method=',
                         'nucl-mom=',
                         'cov-diag'
                       ])
    import sys
    sys.stderr.write('argv: %s\n' % argv)
    sys.stderr.write('getopt: %s\n' % repr(opt_list))
    for opt, val in opt_list:
        if   opt == '--latsize':    param['latsize']    = parse_latsize(val)
        elif opt == '--trange':     
            # TODO add sort+uniq
            param['trange'] = parse_int_range(val, min=1)
        elif opt == '--data':       param['data_lists'].append(val)
        elif opt == '--data-dir':   param['data_dir'] = val
        elif opt == '--max-aff-files':  
            param['max_aff_files'] = int(val)
            if (max_aff_files <= 0): raise RuntimeError('bad max aff files = %s' % val)
        elif opt == '--rsplan':     param['rsplan']     = parse_rsplan(val)
        elif opt == '--start-val':  param['start_val']  = map(float, val.split(','))
        elif opt == '--method':     param['method'] = val
        elif opt == '--nucl-mom':        
            param['nucl_mom'] = map(int, val.split(','))
            if (len(param['nucl_mom']) != 3): 
                raise RuntimeError('bad lattice mom %s' % val)
        elif opt == '--cov-diag':   param['cov_diag'] = True

        else: raise RuntimeError('unknown option %s' % opt)

    # FIXME debug
    import sys
    sys.stderr.write('argv: %s\n' % argv)
    sys.stderr.write('getopt: %s\n' % repr(opt_list))
    sys.stderr.write('param: %s\n' % repr(param))
    
    check_param(param, ['latsize', 'trange', 'data_lists', 'data_dir', 'max_aff_files',
            'rsplan', 'start_val', 'method', 'nucl_mom', 'cov_diag'])
    return param


#if (__name__ == '__main__'):
#    import sys
#    if (len(sys.argv) <= 1):
#        print_usage(sys.argv[0])
#        exit(1)
#    param   = parse_options(argv[1:])
#    sys.stderr.write('param = %s\n' % repr(param))
#    res = main_execute(sys.argv)
#    exit(res)
#
"""

 XXX what if the estimator does not change states between ensemble points 
  (e.g. not enough precision)? this will underestimate errorbars and cov
"""
