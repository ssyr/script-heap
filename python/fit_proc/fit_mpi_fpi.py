import math
import sys
import numpy
import scipy
import lhpd
import getopt
import tsv


class Pion_mass_decay_SU2(lhpd.fitter.Model_function_Base):
    """ 
        fit pion mass and decay constant at several quark masses 
        m_\pi = \chi (1 + (\chi / 16\pi^2 f^2) ( log(\chi / \Lambda^2) + c_mpi) )
        f_\pi = f    (1 - (\chi / 8\pi^2 f^2)  ( log(\chi / \Lambda^2) + c_fpi) )
        where
        \chi = 2 B0 m_q
        f_\pi(phys) = 130.7(1) MeV
    """

    def __init__(self):
        self.par_names_list = ['cmpi', 'cfpi', 'a_B0', 'a_f']
    def flatten_range(self, mq_range, mpi_list, fpi_list):
        n = len(irange)
        assert (len(mpi_list) == n and 
                len(fpi_list) == n)
        res = numpy.zeros(shape=(n,2), dtype=numpy.float64)
        res[:,0] = mpi_list
        res[:,1] = fpi_list
        return res.reshape((n*2,))
    def flatten(self, mpi, fpi): return numpy.array([mpi, fpi])
    def f_range(self, mq_range, param_):
        param   = self.select_param(param_)
        n       = len(mq_range)
        res     = numpy.zeros(shape=(n,2), dtype=numpy.float64)
        pi_f_sq = (math.pi * param[3]) ** 2
        for i,mq in enumerate(mq_range):
            chi     = 2. * param[2] * mq
            assert (0. < chi)
            log_chi = math.log(chi)
            res[i,0]    = math.sqrt(chi * (1 + chi / 16 / pi_f_sq * (param[0] + log_chi)))
            res[i,1]    = param[3] * (1 - chi / 8 / pi_f_sq * (param[1] + log_chi))
        return res.reshape((2*n,))
    def df_range(self, mq_range, param_):
        param   = self.select_param(param_)
        assert(len(param)==4)
        n       = len(mq_range)
        res     = numpy.zeros(shape=(n, 2, len(param)), dtype=numpy.float64)
        pi_f_sq = (math.pi * param[3]) ** 2
        for i,mq in enumerate(mq_range):
            chi     = 2. * param[2] * mq
            assert (0. < chi)
            log_chi = math.log(chi)
            mpi         = math.sqrt(chi * (1 + chi / 16 / pi_f_sq * (param[0] + log_chi)))
            res[i,0,0]  = chi * chi / 16 / pi_f_sq / 2 / mpi
            res[i,0,1]  = 0.
            res[i,0,2]  = 2 * mq * (1 + chi / 8 / pi_f_sq * (param[0] + 0.5 + log_chi)) / 2 / mpi
            res[i,0,3]  = - chi * chi / 8 / pi_f_sq / param[3] * (param[0] + log_chi) / 2 / mpi
            res[i,1,0]  = 0.
            res[i,1,1]  = - chi / 8 / pi_f_sq * param[3]
            res[i,1,2]  = - mq / 4 / pi_f_sq * param[3] * (param[1] + 1. + log_chi)
            res[i,1,3]  = 1 + chi / 8 / pi_f_sq * (param[1] + log_chi)
        return res.reshape((n*2,4))

def read_data(file):
    """
        data should be in the form
        0.mq  1.L   2,3.mpi,delta   4,5.fpi,delta
    """
    data        = numpy.array(tsv.read_float(open(file, 'r')))
    mq_range    = data[0,0,:,0]         # first column
    lsize       = data[0,0,:,1]
    n           = len(mq_range)
    avg         = numpy.zeros(shape=(n,2), dtype=numpy.float64)
    var         = numpy.zeros(shape=(n,2), dtype=numpy.float64)
    avg[:,0]    = data[0,0,:,2]
    avg[:,1]    = data[0,0,:,4]
    var[:,0]    = data[0,0,:,3]
    var[:,1]    = data[0,0,:,5]
    return mq_range, avg.reshape((2*n,)), var.reshape((2*n,))

def main(param):
    mq_range, avg, var \
                = read_data(param['data'])
    n_data = len(avg)
    model_func  = Pion_mass_decay_SU2()
    fitter      = lhpd.fitter.Fitter(
            model_func, mq_range, avg, var, numpy.identity(n_data))
    par_avg, par_chi2 \
                = fitter.fit(numpy.array(param['start_val']))

    # perform fits with each parameter varied as in SJK ensemble
    mq_track= param['mq_track_range']
    par_rs  = []
    chi2_rs = []
    n_rs    = param['n_gauss_samples']
    avg_rs  = []
    func_rs = []
    rsplan  = ('jk',1)
    for i in range(n_data):
        avg_var_i = avg.copy()
        for a in scipy.random.normal(avg[i], var[i] / math.sqrt(n_rs-1), n_rs):
            avg_var_i[i] = a
            avg_rs.append(avg_var_i.copy())
            par, chi2 = fitter.fit_savestate(avg_var_i)
            par_rs.append(par)
            chi2_rs.append(numpy.array([chi2]))
            func_rs.append(model_func.f_range(mq_track, par))

    avg_avg, avg_cov    = lhpd.calc_avg_cov(avg_rs, rsplan=rsplan)
    lhpd.pprint_ve_cov(avg_avg, avg_cov) 

    par_avg, par_cov    = lhpd.calc_avg_cov(par_rs, rsplan=rsplan)
    lhpd.pprint_ve_cov(par_avg, par_cov, title=['cmpi', 'cfpi', 'a_B0', 'a_f0'])

    chi2_avg, chi2_cov  = lhpd.calc_avg_cov(chi2_rs, rsplan=rsplan)
    lhpd.pprint_ve_cov(chi2_avg, chi2_cov, title=['chi2'])

    func_avg, func_sigma= lhpd.calc_avg_err(func_rs, rsplan=rsplan)
    func_avg    = func_avg.reshape((len(mq_track),2))
    func_sigma  = func_sigma.reshape((len(mq_track),2))
    sys.stdout.write("# mq   mpi  dmpi  fpi dfpi\n")
    for i,mq in enumerate(mq_track):
        sys.stdout.write("%f\t%e\t%e\t%e\t%e\n" % (mq, 
                    func_avg[i,0], func_sigma[i,0],
                    func_avg[i,1], func_sigma[i,1]))
