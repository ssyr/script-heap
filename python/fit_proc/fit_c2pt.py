from __future__ import print_function
from future.utils import iteritems
import math
import sys
import numpy as np
import lhpd
import getopt
import aff
import tsv
from exceptions import *

from config_mpl import *
from dataview.c2pt import *

from lhpd.pyfit import multiexp
from lhpd.pyfit.varpro import varpro_ladder_c2pt
from lhpd.pyfit.meff import calc_meff, calc_meff_ratio
from lhpd import calc_avg, calc_err, calc_avg_err, spprint_ve, pprint_ve_cov


def init_varpro_c2pt(ens, k, mom3, n, rsplan, cov_diag=False):
    c2pt    = get_c2pt(ens, [ k ] , mom3).real
    c2pt    = c2pt[:, 0, :]
    c2pt_r  = lhpd.resample(c2pt, rsplan)
    c2pt_avg, c2pt_sigma, c2pt_ncov = lhpd.calc_avg_err_ncov(c2pt_r, rsplan)
    if cov_diag: c2pt_ncov  = np.identity(len(c2pt_avg), np.float64)
    return varpro_ladder_c2pt(n, c2pt_avg, c2pt_sigma, c2pt_ncov)


def solve_varpro(nstate, tr, c2_avg, c2_err, c2_ncov, start_val=None, verbose=0):
    """ 
        nstate      number of states to include
        tr          range of 't's to include 
        c2_avg, c2_err, c2_ncov data
    """
    varpro_func = varpro_ladder_c2pt(n, c2_avg, c2pt_err, c2pt_ncov)
    n_param     = varpro_func.n_param()
    reparam     = lhpd.fitter.reparam(*([ None ] + [ ('range', 0., None) ] * (n_param - 1)))

    if None is start_val: start_val = np.zeros(n_param)
    fitter      = lhpd.fitter.Fitter(varpro_func, tr, np.zeros(len(tr)), 
                    c2pt_err[tr], c2pt_ncov[tr,:][:,tr], reparam)
    p, chi2 = fitter.fit(start_val, method='lma_nodiff')
    return p, chi2

"""
def solve_varpro_jkiter(nstate, tr, c2, ncov_threshold):
    #TODO
    c2pt_avg, c2pt_sigma, c2pt_ncov = lhpd.calc_avg_err_ncov(c2pt_rs, rsplan)
"""

def fit_varpro_c2pt(c2pt_rs, tr, n, start_val, rsplan, cov_diag=False, prange=None,
        plot_meff=None,
        plot_dr10=None, 
        plot_param_bin=None, 
        xstep=.1, xshift=0.15, dt=1, plt_index=0, label=None):
    """

        return: (E[i_data, i_state], chi2[i_data], ndf)
    """
    tr          = np.asarray(tr)
    start_val   = np.asarray(start_val)

    
    #c2pt    = get_c2pt(ens, [ k ] , mom3).real
    #c2pt    = c2pt[:, 0, :]
    #c2pt_r  = lhpd.resample(c2pt, rsplan)
    c2pt_avg, c2pt_sigma, c2pt_ncov = lhpd.calc_avg_err_ncov(c2pt_rs, rsplan)
    c2pt_ncov_evals = np.linalg.eigvalsh(c2pt_ncov[tr,:][:,tr]) #; c2pt_ncov_evals.sort()
    print("c2pt_ncov_evals: [ %e, %e ]" % (c2pt_ncov_evals.min(), c2pt_ncov_evals.max()))
    c2pt_ncov_evals.sort(); print(c2pt_ncov_evals)

    if cov_diag: c2pt_ncov  = np.identity(len(c2pt_avg), np.float64)

    vpro    = varpro_ladder_c2pt(n, c2pt_avg, c2pt_sigma, c2pt_ncov)

    if None is prange: prange  = [ None for x in range(n) ]
    fit_vpro = lhpd.fitter.Fitter(
            vpro,
            tr, np.zeros(len(tr), np.float64), 
            c2pt_sigma[tr], c2pt_ncov[tr,:][:,tr],
            lhpd.fitter.reparam(*prange))
    p0, chi2 = fit_vpro.fit(start_val, 'lma_nodiff')
    print(p0, chi2)
    
    # cycle over ensemble
    p_rs    = np.empty((len(c2pt_rs), n), np.float64)
    chi2_rs = np.empty((len(c2pt_rs), 1), np.float64)
    pb = lhpd.progbar(total=len(c2pt_rs), prefix='fit_varpro: ')
    for i_data, c2 in enumerate(c2pt_rs):
        p_rs[i_data], chi2_rs[i_data] = \
            lhpd.fitter.Fitter(varpro_ladder_c2pt(n, c2, c2pt_sigma, c2pt_ncov),
                    tr, np.zeros(len(tr), np.float64), 
                    c2pt_sigma[tr], c2pt_ncov[tr,:][:,tr],
                    lhpd.fitter.reparam(*prange)).fit(
                            start_val, method='lma_nodiff')
        #p_rs[i_data], chi2_rs[i_data] = \
        #        fit_vpro.fit_savestate(c2[tr], method='lma_nodiff')
        pb.step(); pb.redraw()
    pb.finish()
    
    # report
    ndf = fit_vpro.get_ndf()
    p_avg, p_cov = lhpd.calc_avg_cov(p_rs, rsplan)
    lhpd.pprint_ve_cov(p_avg, p_cov, fit_vpro.func.par_names())
    chi2_avg, chi2_cov = lhpd.calc_avg_cov(chi2_rs, rsplan)
    lhpd.pprint_ve_cov(chi2_avg / ndf, chi2_cov / ndf**2, ['chi2/ndf'])
    
    # plot
    tr_f    = np.arange(min(tr) - xstep, max(tr) + 2*xstep, xstep)

    def get_fmt(fmt, i):
        if dict == type(fmt): return fmt
        elif list == type(fmt): return fmt[ i % len(fmt) ]
        else: raise ValueError('cannot interpret format')

    def sprint_param(p_avg, p_err, chi2_avg, chi2_err):
        p_ve_str = ''
        for i in range(len(p_avg)): 
            p_ve_str += spprint_ve(p_avg[i], p_err[i], precmax=5) + '  '
        p_ve_str = '[ ' + p_ve_str + ']  ' + spprint_ve(chi2_avg[0], chi2_err[0], precmax=5)
        return p_ve_str

    # meff: TODO
    if not None is  plot_meff:
        ax, fmt, fmt_shade = plot_meff[0:3]
        # data
        meff_rs = np.log(c2pt_rs[:,tr] / c2pt_rs[:,tr+dt]) / dt
        meff_avg, meff_err = lhpd.calc_avg_err(meff_rs, rsplan)
        for i in range(len(meff_avg)):
            if meff_avg[i] < meff_err[i]: meff_err[i] = float('nan')
        ax.errorbar(tr + xshift * plt_index, 
                    meff_avg, meff_err, **get_fmt(fmt, plt_index))
        # band
        meff_f_rs   = np.empty((len(p_rs), len(tr_f)), np.float64)
        for i_data, p in enumerate(p_rs):
            vpro_sol    = vpro.varpro_solve(tr, p)
            meff_f_rs[i_data] = np.log(np.dot(vpro.sysmatrix(tr_f, p), vpro_sol) /
                    np.dot(vpro.sysmatrix(tr_f + dt, p), vpro_sol)) / dt
        meff_f_avg, meff_f_err = lhpd.calc_avg_err(meff_f_rs, rsplan)
        meff_f_lo, meff_f_hi = meff_f_avg - meff_f_err, meff_f_avg + meff_f_err
        ax.fill_between(tr_f + xshift * plt_index, 
                meff_f_lo, meff_f_avg, **get_fmt(fmt_shade, plt_index))
        ax.fill_between(tr_f + xshift * plt_index, 
                meff_f_avg, meff_f_hi, **get_fmt(fmt_shade, plt_index))
        # text
        p_avg, p_err = lhpd.calc_avg_err(p_rs, rsplan)
        chi2_avg, chi2_err = lhpd.calc_avg_err(chi2_rs / ndf, rsplan)
        p_ve_str = sprint_param(p_avg, p_err, chi2_avg, chi2_err)
        ax.text(.0, .1*plt_index, p_ve_str, ha='left', va='bottom', 
                transform = ax.transAxes, fontsize='xx-small', 
                color=get_fmt(fmt, plt_index)['c'])

    # plot dr10: TODO
    if not None is  plot_dr10:
        ax, fmt, fmt_shade = plot_dr10
        dr10_min = 1.01e-4
        # band
        dr10_f_rs = np.empty((len(p_rs), len(tr_f)), np.float64)
        for i_data, p in enumerate(p_rs):
            # vpro_terms = [ e^{-t_i*E_j} * sqrt(Z_j) ]
            vpro_terms  = vpro.sysmatrix(tr_f, p) * np.sqrt(vpro.varpro_solve(tr, p))
            dr10_f_rs[i_data] = vpro_terms[:, 1:].sum(1) / vpro_terms[:, 0]
        dr10_f_avg, dr10_f_err = lhpd.calc_avg_err(dr10_f_rs, rsplan)
        dr10_f_lo, dr10_f_hi = dr10_f_avg - dr10_f_err, dr10_f_avg + dr10_f_err
        dr10_f_lo = np.where(dr10_f_lo < dr10_min, dr10_min, dr10_f_lo)
        dr10_f_avg= np.where(dr10_f_avg< dr10_min, dr10_min, dr10_f_avg)
        dr10_f_hi = np.where(dr10_f_hi < dr10_min, dr10_min, dr10_f_hi)
        ax.fill_between(tr_f + xstep * plt_index, 
                dr10_f_lo, dr10_f_avg, **get_fmt(fmt_shade, plt_index))
        ax.fill_between(tr_f + xstep * plt_index, 
                dr10_f_avg, dr10_f_hi, **get_fmt(fmt_shade, plt_index))

    # plot param bins
    if not None is  plot_param_bin:
        ax_list, fmt = plot_param_bin
        for i in range(n):
            ax = ax_list
            if list == type(ax): ax = ax_list[i]
            ax.hist(p_rs[:,i], bins=p_rs.shape[0]/10, **get_fmt(fmt, plt_index))
            ax.set_xlabel(fit_vpro.func.par_names()[i])
            #ax.hist(p_rs[:,i], *get_fmt(fmt, plt_index))
            #ax.hist(p_rs[:,i], bins=p_rs.shape[0]/10)
            
    return p_rs, chi2, ndf
    """ Usage:
    fit_varpro_c2pt(ens, key_list[0], mom3, range(10, 60), 2, [.2,.1], rsplan=('jk', 1), cov_diag=False, prange=[None , None])
    """

def show_fit_varpro_c2pt(ens, k, mom3, tr, n, start_val, rsplan=('jk',1), 
            cov_diag=False, prange=None, dt=1,
            fmt=fmt_marker_robust, fmt_shade=fmt_shade_robust):

    mom3        = np.asarray(mom3)
    tr          = np.asarray(tr)
    start_val   = np.asarray(start_val)


    c2pt    = get_c2pt(ens, [ k ] , mom3).real
    c2pt    = c2pt[:, 0, :]
    c2pt_rs = lhpd.resample(c2pt, rsplan)

    fig     = plt.figure(figsize=[10., 8.])

    ax_meff = fig.add_subplot(2, 2, 1)
    ax_meff.set_xlabel(r'$t$')
    ax_meff.set_ylabel(r'$m^{eff}$')
    #ax_meff.set_ylim(.2, .6)
    
    ax_dr10 = fig.add_subplot(2, 2, 2)
    ax_dr10.set_xlabel(r'$t$')
    ax_dr10.set_ylabel(r'$\delta R_{10}$')
    ax_dr10.set_yscale('log')

    ax_param= [fig.add_subplot(2, 2, 3), fig.add_subplot(2, 2, 4)]
    
    p_rs, chi2_rs, ndf = fit_varpro_c2pt(c2pt_rs, tr, n, start_val, 
                    rsplan=rsplan, cov_diag=cov_diag, prange=prange,
                    plot_meff=(ax_meff, fmt, fmt_shade), dt=dt,
                    plot_dr10=(ax_dr10, fmt, fmt_shade),
                    plot_param_bin=(ax_param, fmt_shade))

    if cov_diag: cov_str = 'ucorr'
    else: cov_str = 'corr'
    fig.text(.5, .99, 
             ('Fit %s[%d,%d,%d]: %dexp in [%d,%d] %s %s' % 
                (k, 
                 mom3[0],mom3[1],mom3[2],
                 n, tr.min(), tr.max(),
                 cov_str, str(rsplan))), 
             ha='center', va='top')

    return fig 
    """ Usage:
    show_fit_varpro_c2pt(ens, key_list[0], [0,0,0], range(4,20), 2, [.4,.4], ('jk',4), cov_diag=False, prange=[('range', .0, 5.), ('range', .001, 5.)], dt=1)
    """
    
class C2pt_helper:
    def __init__(self, latsize, mom2_max, aff_in=lhpd.aff_io.aff_import(10)):
        self.aff_in     = aff_in
        self.latsize    = np.asarray(latsize)
        self.mom2_max   = mom2_max
        self.cset       = lhpd.init_corr_set_2pt_mom2_max(
                            self.latsize, mom2_max)
    def import_data(self, data_map):
        n_data = None
        self.ens = {}
        for k,v in iteritems(data_map):
            data = []
            for dl in v:
                data.extend(lhpd.init_ensemble_2pt(
                    self.cset, open(dl[0]), self.aff_in, dl[1]))
            self.ens[k] = data
            
            if None is n_data: 
                n_data = len(data)
            else:
                if n_data != len(data):
                    raise ValueError('unmatching number of measurements in %s:%s' % 
                                (str(k), str(v)))

    def get_c2pt(self, ens, key_list, mom3):
        """ return c2pt[i_data, i_key, t] 
        """
        n_key = len(key_list)
        lt = latsize[3]
        res = np.empty((len(ens.values()[0]), n_key, lt), np.complex128)
        for i_k, k in enumerate(key_list):
            for i_x, x in enumerate(ens[k]):
                res[i_x, i_k, :] = x.get_avg_equiv(mom3)
        return res


    def show_c2pt_range(self, k, mom3,
                  ax=None, fmt=fmt_line_robust, fmt_shade=fmt_shade_robust,
                  c2pt_min=None, c2pt_max=None, c2pt_linthresh=None, lin_order=10**5):
        
        
        tr = np.arange(0, self.latsize[-1])

        mom3    = np.asarray(mom3)
        c2pt    = get_c2pt(self.ens, [ k ] , mom3).real[:, 0, :]
        c2pt_s  = np.sort(c2pt, axis=0)
        #c2pt_s_i= np.argsort(c2pt, axis=0)
        
        if None is ax:
            ax = plt.figure().add_subplot(1,1,1)
        
        plot_stat_2d(c2pt_s, tr, quant=(.0015, .0230, .1587), is_sorted=True,
              ax=ax, fmt=fmt, fmt_shade=fmt_shade)
        
        ax.set_xlabel(r'$t$')
        ax.set_ylabel(r'$C_\text{2pt}$')

        # set tics
        if None is c2pt_min: c2pt_min = c2pt.min()
        if None is c2pt_max: c2pt_max = c2pt.max()
        if None is c2pt_linthresh:
            c2pt_abs_rmax   = max(math.fabs(c2pt_min), math.fabs(c2pt_max))
            c2pt_linthresh  = c2pt_abs_rmax / lin_order
        ax.set_ylim(c2pt_min, c2pt_max)
        ax.set_yscale('symlog', linthreshy=c2pt_linthresh)
        ax.grid(True)
        
        ax.set_yticks(make_ticks_symlog(c2pt_min, c2pt_max, base=10, step=1, 
                    linthresh=c2pt_linthresh))

        return ax

    def show_c2pt(self, k, mom3, tr=None, 
                  ax=None, fmt=fmt_marker_robust, fmt_shade=fmt_shade_robust,
                  c2pt_min=None, c2pt_max=None, c2pt_linthresh=None, lin_order=10**5):

        mom3    = np.asarray(mom3)
        c2pt    = self.get_c2pt(self.ens, [ k ] , mom3).real[:, 0, :]

        if None is tr: tr = np.arange(0, self.latsize[-1])
        else: tr = np.asarray(tr)

        if None is ax:
            ax = plt.figure().add_subplot(1,1,1)

        for i, c in enumerate(c2pt): 
            ax.plot(tr, c)
        

        ax.set_xlabel(r'$t$')
        ax.set_ylabel(r'$C_\text{2pt}$')

        # set tics
        if None is c2pt_min: c2pt_min = c2pt.min()
        if None is c2pt_max: c2pt_max = c2pt.max()
        if None is c2pt_linthresh:
            c2pt_abs_rmax   = max(math.fabs(c2pt_min), math.fabs(c2pt_max))
            c2pt_linthresh  = c2pt_abs_rmax / lin_order
        ax.set_ylim(c2pt_min, c2pt_max)
        ax.set_yscale('symlog', linthreshy=c2pt_linthresh)
        ax.grid(True)

        ax.set_yticks(make_ticks_symlog(c2pt_min, c2pt_max, base=10, step=1, 
                    linthresh=c2pt_linthresh))

        return ax


def init_hadspec_fitter(tr, y_avg, y_sigma, y_ncov, method, start_val):
    if (method == 'meff_2exp_osc'):
        model_func = multiexp.Meff(multiexp.Nexp_osc_ladder_c2pt(2))
        range_list = [
                ('range', 0., 10.),
                ('range', 0., 1e5),
                ('range', 0., 10.),
                None,
                ('range', 0., None) ]
    elif (method == 'meff_2exp'):
        model_func = multiexp.Meff(multiexp.Nexp_ladder_c2pt(2))
        range_list = [
                ('range', 0., 10.),
                ('range', 0., 1e5),
                ('range', 0., 10.) ]
    elif (method == 'c2pt_2exp'):
        model_func = multiexp.Nexp_ladder_c2pt(2)
        range_list = [
                ('range', 0., 1e5),
                ('range', 0., 10.),
                ('range', 0., 1e5),
                ('range', 0.01, 10.) ]
    else: raise RuntimeError('unknown fitting method %s' % method)

    fitter = lhpd.fitter.Fitter(
            model_func,
            tr, y_avg, y_sigma, y_ncov,
            lhpd.fitting.reparam(*range_list))
    return fitter


def init_accumulators(param, fitter):
    tr = list(expand_range(param['trange']))
    tr.sort()
    if ('show_pts' in param and 0 < param['show_pts']) :
        tr_detail = numpy.arange(tr[0], tr[-1],
                                 float(tr[-1]-tr[0])/param['show_pts'],
                                 dtype=numpy.float64)
    else: tr_detail = tr

    # here, accumulators take tuple (param_array, param_dict)
    tr_str = map(str, tr)
    tr_detail_str = map(str, tr_detail)
    def fitting_func(p):
        return fitter.func.f_range(tr_detail, p[0])

    def exc_contrib(p):
        pdict = p[1]
        return np.array([ abs(pdict['c2pt_c1']) * math.exp(-pdict['c2pt_de1_0'] * t)
                             for t in tr], dtype=np.float64)
    def exc_contrib_3pt(p):
        pdict = p[1]
        return np.array([ math.sqrt(abs(pdict['c2pt_c1'])) * math.exp(-pdict['c2pt_de1_0'] * t)
                             for t in tr], dtype=np.float64)
    def osc_contrib(p):
        pdict = p[1]
        return np.array([ abs(pdict['c2pt_cosc']) * math.exp(-pdict['c2pt_deosc_0'] * t)
                             for t in tr], dtype=np.float64)
    acc_list = [];
    acc_list.append(lhpd.fitter.gauss_estimator(
                fitting_func,
                rsplan=param['rsplan'],
                title=tr_detail_str,
                head='# fitting function'))
    acc_list.append(lhpd.fitter.gauss_estimator(
                exc_contrib,
                rsplan=param['rsplan'],
                title=tr_str,
                head='# 1st excited contribution'))
    acc_list.append(lhpd.fitter.gauss_estimator(
                exc_contrib_3pt,
                rsplan=param['rsplan'],
                title=tr_str,
                head='# 1st excited contribution to 3pt ratio'))
    if (param['method'] == 'meff_2exp_osc'):
        acc_list.append(lhpd.fitter.gauss_estimator(
                    osc_contrib,
                    rsplan=param['rsplan'],
                    title=tr_str,
                    head='# oscillating contribution'))
    return acc_list


def fit_meff(ens, k, mom3, tr, cov_diag=False, rsplan=('jk',1), 
             plot_meff=None, plot_dr10=None, xstep=.1, xshift=0.15, plt_index=0):
    """
        plot_meff   (axes, fmt_marker, fmt_shade)
        xshift      
    """
    # XXX real part, first key of c2pt is taken here
    c2pt    = get_c2pt(ens, [ k ] , mom3).real
    c2pt    = c2pt[:, 0, :]

    c2pt_r  = lhpd.resample(c2pt, rsplan)
    meff_r  = calc_meff(c2pt_r, tr)
    meff_avg, meff_sigma, meff_ncov = lhpd.calc_avg_err_ncov(meff_r, rsplan)
    if cov_diag == True: meff_ncov = np.identity(len(meff_sigma), np.float64)
    start_val = np.array([.6, 1., .6])
    fit_meff= init_hadspec_fitter(tr, meff_avg, meff_sigma, meff_ncov, 'meff_2exp', start_val)
    
    a = fit_meff.fit(start_val)


    #print("initial\t", a[0])
    p_rs = []
    chi_fit_rs  = []
    #accum   = init_accumulators(param, fit_meff)
    ndf     = fit_meff.get_ndf()

    # cycle over resampled ensemble, starting from the overall average
    for i_data, meff in enumerate(meff_r):
        # FIXME chi2 here is computed incorrectly for no resampling
        # because if _no_resampling_is_performed, the correct cov.matrix should be 
        # (N-1) times larger; jk-resampled values "group" around the mean and reproduce
        # the correct chi2 with suppressed variation (due to resampling)
        fit_res, fit_chi2 = fit_meff.fit_savestate(meff)
        p_rs.append(fit_res)
        chi_fit_rs.append(np.array([ fit_chi2 / ndf ]))
        pdict = multiexp.make_param_dict(fit_meff.func.par_names(), fit_res)


    p_avg, p_cov = lhpd.calc_avg_cov(p_rs, rsplan)
    lhpd.pprint_ve_cov(p_avg, p_cov, fit_meff.func.par_names())
    chi2_avg, chi2_cov = lhpd.calc_avg_cov(chi_fit_rs, rsplan)
    lhpd.pprint_ve_cov(chi2_avg, chi2_cov, ['chi2/ndf'])

    def get_fmt(fmt, i):
        if dict == type(fmt): return fmt
        elif list == type(fmt): return fmt[ i % len(fmt) ]
        else: raise ValueError('cannot interpret format')
    if not None is  plot_meff:
        ax, fmt_marker, fmt_shade = plot_meff

        tr_f = np.arange(min(tr) - xstep, max(tr) + 2*xstep, xstep)
        f_avg, f_err = lhpd.calc_avg_err(
                np.array([ fit_meff.func.f_range(tr_f, p) for p in p_rs]), 
                rsplan)
        f_l, f_h = f_avg - f_err, f_avg + f_err 
        ax.fill_between(tr_f + xstep * plt_index, f_l, f_avg, **get_fmt(fmt_shade, plt_index))
        ax.fill_between(tr_f + xstep * plt_index, f_h, f_avg, **get_fmt(fmt_shade, plt_index))
        
        p_avg, p_err = lhpd.calc_avg_err(np.array(p_rs), rsplan)
        p0, p0_l, p0_h = p_avg[0], p_avg[0] - p_err[0], p_avg[0] + p_err[0]
        
        meff_label = '%s[%d,%d,%d]: $m_0$=%s $c_1/c_0$=%s $\delta m_{10}$=%s $\chi^2/dof$=%s' % \
               (k, mom3[0], mom3[1], mom3[2],
                spprint_ve(p_avg[0], p_err[0]), 
                spprint_ve(p_avg[1], p_err[1]),
                spprint_ve(p_avg[2], p_err[2]), 
                spprint_ve(chi2_avg, math.sqrt(chi2_cov[0,0])))
        ax.errorbar(tr + xstep * plt_index, meff_avg, meff_sigma, label=meff_label,
                    **get_fmt(fmt_marker, plt_index))

    if not None is  plot_dr10:
        def dr10_range(xr, p):
            """ p = [ E0, c1/c0, dE10 ] """
            return np.array([ math.sqrt(p[1])*math.exp(-x * p[2]) for x in xr ])
        def make_max(a,b):
            assert a.shape == b.shape
            assert len(a.shape) == 1 # stub for 1d arrays
            c = a.copy()
            for i in range(len(c)): 
                if c[i] < b[i]: c[i] = b[i]
            return c
        ax, fmt_marker, fmt_shade = plot_dr10
        # "integer" t
        fi_avg, fi_err = lhpd.calc_avg_err(
                np.array([ dr10_range(tr, p) for p in p_rs]), 
                rsplan)
        min_log = fi_avg.min() / 10.
        fi_l, fi_h = fi_avg - fi_err, fi_avg + fi_err 
        fi_l = make_max(fi_l, min_log * np.ones_like(fi_l))
        ax.errorbar(tr, fi_avg, fi_err, label=k,
                    **get_fmt(fmt_marker, 0))
        print(fi_l.min(), fi_l.max(), fi_h.min(), fi_h.max())

        # "continuous" t 
        tr_f = np.arange(min(tr) - xstep, max(tr) + 2*xstep, xstep)
        f_avg, f_err = lhpd.calc_avg_err(
                np.array([ dr10_range(tr_f, p) for p in p_rs]), 
                rsplan)
        f_l, f_h = f_avg - f_err, f_avg + f_err 
        f_l = make_max(f_l, min_log * np.ones_like(f_l))
        ax.fill_between(tr_f, f_l, f_avg, **get_fmt(fmt_shade, 0))
        ax.fill_between(tr_f, f_h, f_avg, **get_fmt(fmt_shade, 0))
        print(f_l.min(), f_l.max(), f_h.min(), f_h.max())
        #ax.set_yscale('log')




def show_fit_meff(ens, key_list, mom3, tr, cov_diag=False, rsplan=('jk',1),
             xstep=.1, xshift=0.15, 
             fmt=fmt_marker_robust, fmt_shade=fmt_shade_robust):
    
    tr      = np.array(tr)
    mom3    = np.array(mom3)
    fig     = plt.figure(figsize=[10., 8.])
    
    ax_meff = fig.add_subplot(2, 1, 1)
    ax_meff.set_xlabel(r'$t$')
    ax_meff.set_ylabel(r'$m^{eff}$')
    
    ax_dr10 = fig.add_subplot(2, 1, 2)
    ax_dr10.set_xlabel(r'$t$')
    ax_dr10.set_ylabel(r'$\delta R_{10}$')
    for i_k, k in enumerate(key_list):
        fit_meff(ens, k, mom3, tr, cov_diag=False, rsplan=('jk',1),
                 plot_meff=(ax_meff, fmt, fmt_shade), 
                 plot_dr10=(ax_dr10, fmt, fmt_shade), 
                 xstep=xstep, xshift=xshift, plt_index=i_k)
        print(k)

    ax_meff.legend()
    ax_dr10.legend()
    



# analysis-specific plotting functions
# TODO might be useful to have a library of these too

def show_meff(ax, ens, key_list, mom3, tr, dt=1, rsplan=('jk',1), 
            xshift=0, fmt=fmt_marker_default, fmt_line=None): 
    """
        ens         dict: key->ens
        key_list    list of keys to show
        mom3        momentum
        tr          t-range
        dt          eff.mass interval
        xshift      shift each group in x direction for readability
        fmt         formats of markers
        fmt_line    formats for tracing lines

    """
    # XXX real part of c2pt is taken here
    c2pt    = get_c2pt(ens, key_list, mom3).real   
    c2pt_r  = lhpd.resample(c2pt, rsplan)
    meff_r  = calc_meff(c2pt_r, tr)
    meff_avg  = lhpd.calc_avg(meff_r)
    meff_err  = lhpd.calc_err(meff_r, rsplan)

    for i_k, k in enumerate(key_list):
        plot_data2D(ax, i_k, np.array(tr) + i_k * xshift,
                    meff_avg[i_k], 
                    meff_err[i_k], 
                    label=str(k),
                    fmt=fmt, fmt_line=fmt_line)
    #ax.set_xlim(xmin=-.5, xmax=np.array(tr).max()+1.5)
    #ax.set_ylim(ymin=0., ymax=2.)
    ax.set_xlabel('$t$')
    ax.set_ylabel(r'$m^{eff}$')
    #ax.set_xticks(tr)
    ax.legend()
    #try:
    #    plt.show()
    #except KeyboardInterrupt: pass
    #return fig

def show_c2pt_ratio(ax, ens, key_list, mom3, tr, t0, k0, rsplan=('jk',1), 
            xshift=0, fmt=fmt_marker_default, fmt_line=None):
    """
        calc ratio (c2[k,t] / c2[k0,t]) / (c2[k,t0] / c2[k0,t0])
        xshift      shift each group in x direction for readability
        fmt         formats of markers
    """
    # XXX real part of c2pt is taken here
    c2pt    = get_c2pt(ens, key_list, mom3).real   
    c2pt_r  = lhpd.resample(c2pt, rsplan)
    i_k0    = key_list.index(k0)
    r_r     = c2pt_r[:, :, :] / c2pt_r[:, i_k0:i_k0+1, :] 
    r_r     = r_r[:, :, :] / r_r[:, :, t0:t0+1]
    r_r     = r_r[..., tr]
    ratio_avg  = lhpd.calc_avg(r_r)
    ratio_err  = lhpd.calc_err(r_r, rsplan)

    for i_k, k in enumerate(key_list):
        plot_data2D(ax, i_k, np.array(tr) + i_k * xshift,
                    ratio_avg[i_k], 
                    ratio_err[i_k], 
                    label=str(k),
                    fmt=fmt, fmt_line=fmt_line)
    #ax.set_ylim(ymin=0., ymax=1.5)
    #ax.set_xlim(xmin=-.5, xmax=np.array(tr).max()+1.5)
    ax.set_xlabel(r'$t$')
    ax.set_ylabel(r'$\frac{C_2(k,t)}{C_2(k_0,t)} / \frac{C_2(k,t_0)}{C_2(k_0,t_0)}$')
    #ax.set_xticks(tr)
    ax.legend(loc=1)
    ax.plot([ min(tr), max(tr) ], [1., 1.], 'k--')
    #try:
    #    plt.show()
    #except KeyboardInterrupt: pass
    #return fig

def show_c2pt_comp(ens, key_list, mom3, tr, t0, k0, dt=1, rsplan=('jk',1), xshift=0.15,
        fmt=modify_format(fmt_marker_robust, ls='', lw=1, ms=4, mew=.5), 
        fmt_line=modify_format(fmt_marker_robust, ls='', lw=1, ms=4, mew=.5)):
    tr      = np.array(tr)
    mom3    = np.array(mom3)
    fig     = plt.figure(figsize=[10., 8.])
    
    ax_meff = fig.add_subplot(2, 1, 1)
    ax_meff.text(.5, .95, 
                 ("$M_{eff}$: dt=%d" % (dt)), 
                 ha='center', va='top', transform = ax_meff.transAxes)
    show_meff(ax_meff, ens, key_list, mom3, tr, xshift=xshift, 
              fmt=fmt, fmt_line=fmt_line);  

    ax_c2r  = fig.add_subplot(2, 1, 2)
    ax_c2r.text(.5, .95, 
                 ("$C_{2pt}$ ratio: $t_0$=%d, $k_0$='%s'" % (t0, k0)),
                 ha='center', va='top', transform = ax_c2r.transAxes)
    show_c2pt_ratio(ax_c2r, ens, key_list, mom3, tr, t0, k0, xshift=xshift, 
                    fmt=fmt, fmt_line=fmt_line)

    return fig



"""
cur_fmt=modify_format(fmt_marker_robust, ls='', lw=1, ms=4, mew=.5)
fig=plt.figure(figsize=[10.,8.]);  ax=fig.add_axes([.1,.1,.8,.8]);  show_meff(ax, ens, key_list, np.array([0,0,0]), range(1,21), xshift=.08, fmt=cur_fmt);  ax.set_ylim(0., .6) ; ax.set_xlim(-.5, None)
fig=plt.figure(figsize=[10.,8.]);  ax=fig.add_axes([.1,.1,.8,.8]);  show_c2pt_ratio(ax, ens, key_list, np.array([0,0,0]), range(1,21), t0=16, k0='GN3x220-GN3x220', xshift=.08, fmt=cur_fmt);  ax.set_ylim(0., 1.8) ; ax.set_xlim(-.5, None)
"""


"""
fmt=fmt_marker_robust
fmt_shade=fmt_shade_robust
fig=plt.figure(figsize=[10.,8.]);  ax=fig.add_axes([.1,.1,.8,.8]);  fit_meff(ens, ['GN3x155-GN3x155'], mom3, range(1,21), plot_meff=(ax,fmt,fmt_shade));  ax.set_ylim(0., 1.2) ; ax.set_xlim(-.5, None)
"""


"""
# TODO
init_model
    IN: model parameters (n-exp, +osc?, meff?
    OUT: 
        class
    function(xr, y, y_sigma, y_cov) -> param
    
fitter: 
    IN: ax, fmt_marker, fmt_shade, xstep
"""
