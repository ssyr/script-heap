from numpy.linalg import *
from numpy import *

def hankel_matr(p, q, y):
    """
        p,q     Hankel matrix size
        y       array of correlator values (p+q-1) (real)
    """
    assert y.shape == (p+q-1,)
    H = empty((p, q), y.dtype)
    for j in range(q): H[:,j] = y[j:j+p]
    return H

def sys_kung(p, q, y, n):
    """ Computation of the system matrix by Kung's method
        from a (p x q) Hankel matrix
        p,q     Hankel matrix size
        y       array of correlator values (p+q-1) (real)
        n       number of exponents to approximate
    """
    assert p >= q
    assert y.shape == (p+q,)
    H = hankel_matr(p+1, q, y)
    # compute Kung's system matrix
    u,s,vH = svd(H, full_matrices=0, compute_uv=1)
    d = u[-1,:n]
    assert vdot(d,d) < 1
    # q x q pseudoinverse component, q <= p-1
    u2xu2T_inv = outer(solve(identity(n, d.dtype) - outer(d, d.conj()), d), 
                       d.conj()) + identity(n, d.dtype)
    M = dot(u2xu2T_inv, 
            dot(u[:-1, :n].T.conj(), u[1:, :n]))
    return M

def sys_zeigen(p, q, y, n):
    """ Computation of the system matrix by Zeigen&McEwen's method
        from a (p x q) Hankel matrix
        p,q     Hankel matrix size
        y       array of correlator values (p+q-1) (real)
        n       number of exponents to approximate
    """
    assert p >= q
    assert y.shape == (p+q,)
    H = hankel_matr(p, q, y[:p+q-1])
    Hbar = hankel_matr(p, q, y[1:p+q])
    u,s,vH = svd(H, full_matrices=0, compute_uv=1)
    s1 = 1. / sqrt(s[:n])
    M = dot(u[:,:n].T.conj(), dot(Hbar, vH[:n,:].T.conj())) \
            * s1 * s1[:,None]
    return M
    
def test_corr(expM, c, m):
    n = expM.size
    assert expM.shape == (n,) and c.shape == (n,)
    z = ones((n,), dtype=c.dtype)
    y = empty((m,), dtype=c.dtype)
    for i in range(m):
        y[i] = dot(c, z)
        z *= expM
    return y

def sys_coeff(expM, y, sigma, cov = None):
    """ Determine coeffs of the exponents in the correlator
        expM    exponents [n]
        y       correlator values [m]
        sigma   error estimates of y [m]
        cov     covariance matrix (normalized) [m,m]
    """
    m = y.size
    n = expM.size
    assert n <= m
    assert sigma.shape == (m,)
    if cov is None: cov = identity(m, y.dtype)
    assert cov.shape == (m,m)
    # make system matrix A for leastsq problem y = A.x
    # and combine it with the errorbars: sA := sigma^-1 . A
    z = ones(n, y.dtype)
    sA = empty((m, n), y.dtype)
    for i in range(m):
        sA[i] = z / sigma[i]
        z *= expM
    # solve
    icov    = pinv(cov, rcond=1e-8)
    sys     = dot(sA.T, dot(icov, sA))
    isys    = pinv(sys, rcond=1e-8)
    rhs     = dot(sA.T, dot(icov, y / sigma))
    x       = dot(isys, rhs)

    # chi2
    dy      = dot(sA, x) - y / sigma
    chi2    = dot(dy, dot(icov, dy))
    ndf     = m - n

    return x, chi2, ndf
