from __future__ import print_function
import math
import sys
import numpy
import scipy
import lhpd
import getopt
import tsv
import cbchpt


class mom2_umd_Cbchpt(lhpd.fitter.Model_function_Base):

    def __init__(self, fpi, gA, M0):
        self.fpi    = fpi
        self.gA     = gA
        self.M0     = M0
        self.par_names_list = ['av20', 'd_av20', 'c8r', 'bv20', 'cv20']

    def f(self, x, p):
        """ xr = [[mpi1, MN1], ... ] """
        av20, d_av20, c8r, bv20, cv20 = p
        fpi, gA, M0 = self.fpi, self.gA, self.M0
        return numpy.array([
            cbchpt.A20_umd_tzero_p2(x[0], fpi, gA, M0, av20, d_av20, c8r),
            cbchpt.B20_umd_tzero_p2(x[0], fpi, gA, M0, x[1], av20, bv20),
            cbchpt.C20_umd_tzero_p2(x[0], fpi, gA, M0, x[1], av20, cv20), ])

    def f_range(self, xr, p):
        """ xr = [[mpi1, MN1], ... ] """
        av20, d_av20, c8r, bv20, cv20 = p
        fpi, gA, M0 = self.fpi, self.gA, self.M0
        xr = numpy.array(xr)
        assert (len(xr.shape) == 2 and xr.shape[1] == 2)
        res = numpy.empty((xr.shape[0], 3), numpy.float64)
        for i,x in enumerate(xr):
            res[i, 0] = cbchpt.A20_umd_tzero_p2(x[0], fpi, gA, M0, av20, d_av20, c8r)
            res[i, 1] = cbchpt.B20_umd_tzero_p2(x[0], fpi, gA, M0, x[1], av20, bv20)
            res[i, 2] = cbchpt.C20_umd_tzero_p2(x[0], fpi, gA, M0, x[1], av20, cv20)

        return res.flatten()

    def make_data_range(self, xr, data):
        assert (len(xr.shape) == 2 and xr.shape[1] == 2)
        assert (len(data.shape) == 4 and data.shape[1:] == (len(xr), 2, 3))
        return data[...,0,:].reshape((data.shape[0], -1))


class mom2_tzero_tslope_umd_Cbchpt(lhpd.fitter.Model_function_Base):
    """ 
        fit 
        gA = HBChPT value 
    """

    def __init__(self, fpi, gA, M0):
        self.fpi    = fpi
        self.gA     = gA
        self.M0     = M0
        self.par_names_list = [ 'av20', 'd_av20', 'c8r', 'bv20', 'cv20', 
                                'c12', 'dAt', 'dBt', 'dCt' ]

    def f(self, x, p):
        """ xr = [[mpi1, MN1], ... ] """
        av20, d_av20, c8r, bv20, cv20, c12, dAt, dBt, dCt = p
        fpi, gA, M0 = self.fpi, self.gA, self.M0
        return numpy.array([
            [ cbchpt.A20_umd_tzero_p2(x[0], fpi, gA, M0, av20, d_av20, c8r),
              cbchpt.B20_umd_tzero_p2(x[0], fpi, gA, M0, x[1], av20, bv20),
              cbchpt.C20_umd_tzero_p2(x[0], fpi, gA, M0, x[1], av20, cv20) ],
            [ cbchpt.A20_umd_tslope_p2(x[0], fpi, gA, M0, av20, c12, dAt),
              cbchpt.B20_umd_tslope_p2(x[0], fpi, gA, M0, x[1], av20, dBt),
              cbchpt.C20_umd_tslope_p2(x[0], fpi, gA, M0, x[1], av20, dCt) ]]).flatten()

    def f_range(self, xr, p):
        """ xr = [[mpi1, MN1], ... ] """
        av20, d_av20, c8r, bv20, cv20, c12, dAt, dBt, dCt = p
        fpi, gA, M0 = self.fpi, self.gA, self.M0
        xr = numpy.array(xr)
        assert (len(xr.shape) == 2 and xr.shape[1] == 2)
        res = numpy.empty((xr.shape[0], 2, 3), numpy.float64)
        for i,x in enumerate(xr):
            res[i,0,0] = cbchpt.A20_umd_tzero_p2(x[0], fpi, gA, M0, av20, d_av20, c8r)
            res[i,0,1] = cbchpt.B20_umd_tzero_p2(x[0], fpi, gA, M0, x[1], av20, bv20)
            res[i,0,2] = cbchpt.C20_umd_tzero_p2(x[0], fpi, gA, M0, x[1], av20, cv20)
            res[i,1,0] = cbchpt.A20_umd_tslope_p2(x[0], fpi, gA, M0, av20, c12, dAt)
            res[i,1,1] = cbchpt.B20_umd_tslope_p2(x[0], fpi, gA, M0, x[1], av20, dBt)
            res[i,1,2] = cbchpt.C20_umd_tslope_p2(x[0], fpi, gA, M0, x[1], av20, dCt)

        return res.flatten()

    def make_data_range(self, xr, data):
        assert (len(xr.shape) == 2 and xr.shape[1] == 2)
        assert (len(data.shape) == 4 and data.shape[1:] == (len(xr), 2, 3))
        return data[...,0:2,:].reshape((data.shape[0], -1))


def read_data(data_list):
    """
        data_list = [ [mpi1, MN1, "file1"], ... ]
        file1=[:,0,0,{A20, B20, C20}]
    """
    xr      = []
    data    = []
    n_data  = 0
    for a in data_list:
        xr.append([ a[0], a[1] ])
        d = numpy.array(tsv.read_float(open(a[2], 'r')))[:,0,0:2,0:3]
        data.append(d)
        n_data += len(d)

    # make SJK
    n_mpi = len(xr)
    res = numpy.empty(shape=(n_data, n_mpi, 2, 3), dtype=numpy.float64)
    for i_a, a in enumerate(data_list): # cycle over range
        avg = data[i_a].mean(axis=0)
        i_data = 0
        for i_b, b in enumerate(data_list): # cycle over data
            n_data_cur = len(data[i_b])
            if i_a == i_b: res[i_data: i_data+n_data_cur, i_a] = data[i_a]
            else: res[i_data: i_data+n_data_cur, i_a] = numpy.array(avg)[None]
            i_data += n_data_cur
    assert i_data == n_data
    return numpy.array(xr), res


#fix_Dav=[None, ('fixed', 0.165)] + [ None for i in range(7) ]
#main_fit(dlist, mom2_umd_Cbchpt(0.0862, 1.2, 0.889), [0.21, 0.1, 0.1, 0.1, 0.1], rsplan, reparam=None, mpir=numpy.arange(.140,.65, .01), save_range='range.dat')
#main_fit(dlist, mom2_tzero_tslope_umd_Cbchpt(0.0862, 1.2, 0.889), [.21, .1, .1, .1, .1, 0., 0., 0., 0.], rsplan=rsplan, reparam=None, mpir=numpy.arange(.14,.65, .01), save_range='range.dat')


def fit_echo(cnt, p, chi2):
    str = ('%+.4e' % p[0])
    for pi in p[1:]: str += (' %+.4e' % pi)
    return ('%05d' % cnt) + ' [ ' + str + ' ] ' + ('%.5f' % chi2)

def main_fit(datalist, m, start_val, rsplan=('jk', 1), reparam=None, mpir=None, save_range=None):
    from lhpd.fitter import avg_std_ncov, print_avg_std_ncov
    xr, data = read_data(datalist)
    data = m.make_data_range(xr, data)
    data_avg, data_std, data_cov = lhpd.calc_avg_err_ncov(data, rsplan)
    if reparam is None:
        reparam = m.reparam_default()

    fitter = lhpd.fitter.Fitter(m, xr, data_avg, data_std, data_cov, 
                   reparam=lhpd.fitter.reparam(*reparam))
    p,c2 = fitter.fit(numpy.array(start_val), method='lma_nodiff')

    res = numpy.empty((len(data), len(p)), numpy.float64)
    chi2= numpy.empty((len(data), 1), numpy.float64)
    funcr=[]
    mpi_mn_r = numpy.array( [ [ mpi, cbchpt.nucleon_mass(mpi, .0862, 1.2, .8726, 
                                                         -1.049, 3.2, -3.4, 0.90) ] 
                                for mpi in mpir ] )
    for i_d,d in enumerate(data):
        res[i_d], chi2[i_d] = fitter.fit_savestate(d, None, 'lma_nodiff')
        print(fit_echo(i_d, res[i_d], chi2[i_d]))
        funcr.append(m.f_range(mpi_mn_r, res[i_d]))

    print_avg_std_ncov(sys.stdout, m.par_names(), *avg_std_ncov(res, rsplan))
    print('ndf = ', fitter.get_ndf())
    print_avg_std_ncov(sys.stdout, ['chi2/ndf'], *avg_std_ncov(chi2/fitter.get_ndf(), rsplan))
    funcr = numpy.array(funcr)
    tsv.write_format(open(save_range, 'w'), 
                     funcr.reshape((funcr.shape[0], 1, len(mpi_mn_r), -1)))
    
    return res, chi2, funcr, fitter

