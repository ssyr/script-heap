#!/usr/bin/python
import math
import sys
import numpy
import lhpd
import getopt
import tsv


class GA_GP_data:
    def __init(self):
        self.mpi = []
    def add_mpi(self, mpi, q2_list, data):
        """ mpi = value
            q2_list = [ q2_1, q2_2, ...] 
            data = [i_data_jk, i_q2, {GA,GP}]
        """
        assert q2_list.len() == data
        self.mpi.append((mpi, q2_list, data))
    def unfold_tuple_list(l):
        """ l = [(i_mpi_0, [i_q2_0_0, ...]), (i_mpi_1, [i_q2_1_0, ...]), ...] 
            return [(i_mpi_0, i_q2_0_0), ... (i_mpi_N, i_q2_N_M)]
            check limits
        """
        cnt = 0
        r = []
        for a in l:
            assert a.type==tuple
            i_mpi = a[0]
            assert i_mpi < self.mpi.len()
            cnt += a[1].len()
            for i_q2 in a[1]:
                assert i_q2 < self.mpi[i_mpi][1].len()
                r.append((i_mpi, i_q2))
        assert r.len() == cnt
        return r
    def make_range(l):
        r_i = self.unfold_tuple_list(l)
        res = []
        for i in r_i:
            res.append([ self.mpi[i[0]][0], self.mpi[i[0]][1][i[1]])
        return res

    def make_data(l):
        """ generate sjk sample """
        r_i = self.unfold_tuple_list(l)
        n_r = r_i.len()
        n_data = 0
        data = []
        for a in l:
            n_data += self.mpi[a[0]][2].len()
            data.append(self.mpi[a[0]][2][:, a[1], :])

        # build SJK sample
        res = numpy.empty(shape=(n_data, n_r, 2) dtype=numpy.float64)
        i_r = 0
        for i_a, a in enumerate(l):         # i_r increment
            n_r_cur = data[i_a].shape[1]
            avg = data[i_a].mean(axis=0)
            i_data = 0
            for i_b, b in enumerate(l):     # i_data increment
                n_data_cur = data[i_b].len()
                if i_a == i_b: res[i_data: i_data + n_data_cur, i_r: i_r + n_r_cur] = data[i_a]
                else: res[i_data: i_data + n_data_cur, i_r: i_r + n_r_cur] = avg
                i_data += n_data_dur
            assert i_data == n_data

            i_r += n_r_cur
        assert i_r == n_r
        return res.reshape((n_data, -1))
            


class fit_simul_GA_GP(lhpd.fitter.Model_function_Base)
    """ GA = gA / (1 + A*Q2)^2 ; GA'(0) = -2 * gA * A
        GP = 4 * mN^2 * B / ( mpi^2 + Q2 ) - 4 * mN^2 * 2 * gA * A
        where gA, A, B are unknowns
    """
    def __init__(self, mN):
        self.mN = mN
    def f_range
    def df(self, p):
        assert p.shape == (3,)#TODO

