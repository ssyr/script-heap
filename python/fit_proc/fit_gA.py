import math
import sys
import numpy
import scipy
import lhpd
import getopt
import tsv

#import hbchpt

# TODO move to hbchpt module as a function
class gA_Hbchpt(lhpd.fitter.Model_function_Base):
    """ 
        fit 
        gA = HBChPT value 
    """

    def __init__(self, fpi, Delta, cA):
        self.fpi = fpi
        self.Delta = Delta
        self.cA = cA
        self.par_names_list = ['gA0', 'g1', 'CT']

    def sqrtXlogR(self, Dl, mpi):
        from math import sqrt, log, acos
        if (mpi < Dl): return sqrt(Dl**2 - mpi**2) * log(Dl / mpi + sqrt((Dl / mpi)**2 - 1))
        else: return -sqrt(mpi**2 - Dl**2) * acos(Dl/mpi)


    def f(self, mpi, p):
        """ p = [ gA0, g1, cterm ] """
        from math import sqrt, pi, log
        gA0, g1, cterm = p[0], p[1], p[2]
        cA = self.cA
        Dl = self.Delta
        fpi = self.fpi
        pi_fpi = fpi * pi
        gA = gA0 - gA0**3 * mpi**2 / (16 * pi_fpi**2) \
            + 4*mpi**2 \
                * ( cterm \
                    + cA**2 / (4 * pi_fpi**2) * ( 155./972.*g1 - 17./36.*gA0) \
                    + log(mpi) / (16 * pi_fpi**2) \
                        * (50./81. * cA**2 * g1 - gA0/2. - 2./9. * cA**2 * gA0 - gA0**3)) \
            + 4 * cA**2 * gA0 * mpi**3 / (27. * pi * fpi**2 * Dl) \
            + 8 * cA**2 * gA0 * mpi**2 / (27. * pi_fpi**2) * self.sqrtXlogR(Dl, mpi) / Dl \
            + cA**2 * Dl**2 / (81 * pi_fpi**2) * (25*g1 - 57*gA0) \
                * (log(2 * Dl / mpi) - self.sqrtXlogR(Dl, mpi) / Dl)
        return gA
        

class gA_Hbchpt_fix_g1(lhpd.fitter.Model_function_Base):
    """ 
        fit 
        gA = HBChPT value 
    """

    def __init__(self, fpi, Delta, cA, g1):
        self.fpi = fpi
        self.Delta = Delta
        self.cA = cA
        self.g1 = g1
        self.par_names_list = ['gA0', 'CT']

    def sqrtXlogR(self, Dl, mpi):
        from math import sqrt, log, acos
        if (mpi < Dl): return sqrt(Dl**2 - mpi**2) * log(Dl / mpi + sqrt((Dl / mpi)**2 - 1))
        else: return -sqrt(mpi**2 - Dl**2) * acos(Dl/mpi)


    def f(self, mpi, p):
        """ p = [ gA0, g1, cterm ] """
        from math import sqrt, pi, log
        gA0, cterm = p[0], p[1]
        cA = self.cA
        g1 = self.g1
        Dl = self.Delta
        fpi = self.fpi
        pi_fpi = fpi * pi
        gA = gA0 - gA0**3 * mpi**2 / (16 * pi_fpi**2) \
            + 4*mpi**2 \
                * ( cterm \
                    + cA**2 / (4 * pi_fpi**2) * ( 155./972.*g1 - 17./36.*gA0) \
                    + log(mpi) / (16 * pi_fpi**2) \
                        * (50./81. * cA**2 * g1 - gA0/2. - 2./9. * cA**2 * gA0 - gA0**3)) \
            + 4 * cA**2 * gA0 * mpi**3 / (27. * pi * fpi**2 * Dl) \
            + 8 * cA**2 * gA0 * mpi**2 / (27. * pi_fpi**2) * self.sqrtXlogR(Dl, mpi) / Dl \
            + cA**2 * Dl**2 / (81 * pi_fpi**2) * (25*g1 - 57*gA0) \
                * (log(2 * Dl / mpi) - self.sqrtXlogR(Dl, mpi) / Dl)
        return gA

def read_data(data_list):
    """
        data_list = [ [mpi1, "file1"], ... ]
        file1=g_A[:,0,0,0]
    """
    mpi_range = []
    data = []
    n_data = 0
    for a in data_list:
        mpi_range.append(a[0])
        d = numpy.array(tsv.read_float(open(a[1], 'r')))[:,0,0,0]
        data.append(d)
        n_data += len(d)

    # make SJK
    n_mpi = len(mpi_range)
    res = numpy.empty(shape=(n_data, n_mpi), dtype=numpy.float64)
    for i_a, a in enumerate(data_list): # cycle over range
        avg = data[i_a].mean(axis=0)
        i_data = 0
        for i_b, b in enumerate(data_list): # cycle over data
            n_data_cur = len(data[i_b])
            if i_a == i_b: res[i_data: i_data+n_data_cur, i_a] = data[i_a]
            else: res[i_data: i_data+n_data_cur, i_a] = numpy.array(avg)[None]
            i_data += n_data_cur
    assert i_data == n_data
    return numpy.array(mpi_range), res

#fitter=main_fit(dlist, gA_Hbchpt(0.0862, 0.293, 1.5), [1.22, 2.5, 4.], rsplan=rsplan, reparam=(None, ('fixed', 2.5), None), pr=numpy.arange(.01,.65,.01), save_range='gA.range')

def main_fit(datalist, m, start_val, rsplan=('jk', 1), 
             reparam=None, pr = None, save_range=None):
    xr, data = read_data(datalist)
    data_avg, data_cov = gaussian_estimate(data, rsplan)
    data_std = norm_cov_matrix(data_cov)
    
    if reparam is None:
        reparam = m.reparam_default()
    if start_val is None:
        start_val = m.start_default()


    fitter = lhpd.fitter.Fitter(m, xr, data_avg, data_std, data_cov,
                   reparam=lhpd.fitter.reparam(*reparam))
    p, c2 = fitter.fit(numpy.array(start_val), 'lma_nodiff')

    res = []
    chi2= []
    func= []
    gA_phys = []
    mpi_phys = 0.140
    for d in data:
        px, chi2x = fitter.fit_savestate(d, method='lma_nodiff')
        sys.stderr.write('%s\t%s\n' % (str(px), chi2x))
        chi2.append([chi2x])
        res.append(px)
        func.append(m.f_range(pr, px))
        gA_phys.append([ m.f(mpi_phys, px) ])
    func_avg, func_std = lhpd.calc_avg_err(func, rsplan)
    if not None is  save_range:
        tsv.write_format(open(save_range, 'w'), 
                     numpy.array(func)[:,None,:,None])

    lhpd.pprint_ve_cov(*(lhpd.calc_avg_cov(res, rsplan) + (m.par_names(),)))
    lhpd.pprint_ve_cov(*(lhpd.calc_avg_cov(numpy.array(gA_phys), rsplan) + 
                    ([ 'gA(mpi=%.3f)' % mpi_phys ],)))
    lhpd.pprint_ve_cov(*(lhpd.calc_avg_cov(numpy.array(chi2), rsplan) + (['chi2'],)))
    return p, c2


"""
def main(param):
    mq_range, avg, var \
                = read_data(param['data'])
    n_data = len(avg)
    model_func  = Pion_mass_decay_SU2()
    fitter      = lhpd.fitter.Fitter(
            model_func, mq_range, avg, var, numpy.identity(n_data))
    par_avg, par_chi2 \
                = fitter.fit(numpy.array(param['start_val']))

    # perform fits with each parameter varied as in SJK ensemble
    mq_track= param['mq_track_range']
    par_rs  = []
    chi2_rs = []
    n_rs    = param['n_gauss_samples']
    avg_rs  = []
    func_rs = []
    rsplan  = ('jk',1)
    for i in range(n_data):
        avg_var_i = avg.copy()
        for a in scipy.random.normal(avg[i], var[i] / math.sqrt(n_rs-1), n_rs):
            avg_var_i[i] = a
            avg_rs.append(avg_var_i.copy())
            par, chi2 = fitter.fit_savestate(avg_var_i)
            par_rs.append(par)
            chi2_rs.append(numpy.array([chi2]))
            func_rs.append(model_func.f_range(mq_track, par))

    avg_avg, avg_cov    = lhpd.calc_avg_cov(avg_rs, rsplan=rsplan)
    lhpd.pprint_ve_cov(avg_avg, avg_cov) 

    par_avg, par_cov    = lhpd.calc_avg_cov(par_rs, rsplan=rsplan)
    lhpd.pprint_ve_cov(par_avg, par_cov, 
                                    title=['cmpi', 'cfpi', 'a_B0', 'a_f0'])

    chi2_avg, chi2_cov  = lhpd.calc_avg_cov(chi2_rs, rsplan=rsplan)
    lhpd.pprint_ve_cov(chi2_avg, chi2_cov, title=['chi2'])

    func_avg, func_sigma  = lhpd.calc_avg_err(func_rs, rsplan=rsplan)
    func_avg    = func_avg.reshape((len(mq_track),2))
    func_sigma  = func_sigma.reshape((len(mq_track),2))
    sys.stdout.write("# mq   mpi  dmpi  fpi dfpi\n")
    for i,mq in enumerate(mq_track):
        sys.stdout.write("%f\t%e\t%e\t%e\t%e\n" % (mq, 
                    func_avg[i,0], func_sigma[i,0],
                    func_avg[i,1], func_sigma[i,1]))
"""
