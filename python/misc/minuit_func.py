from __future__ import print_function
from future.utils import iteritems
import os
import sys

import math
from numpy.numarray import *
import minuit
import aff
import fit_func 

def prepare_2pt_minuit(ens, func, p, t_range, diag=False, **param):
    a = fit_func.prepare_2pt_chi2_data(ens, p, t_range, diag)
    scale_factor = a[0][0]
    print(scale_factor)
    m    = minuit.Minuit(func(a[0] / scale_factor, 
                              a[1] / scale_factor, 
                              a[3], 
                              t_range), 
                         **param)
    m.tol = 0.1
    # m.printMode = 1
    return m

def print_data(x, f=sys.stdout):
    assert type(x) == tuple and len(x) == 3
    for t in range(len(x[0])): f.write('%d\t%13.7e\t%13.7e\n' % (t, x[0][t], x[1][t]))
    
def print_minuit(m, f=sys.stdout):
    assert type(m) == minuit.Minuit
    for k,v in iteritems(m.values):
        f.write('%s\t= %13.7e\t(%13.7e)\n' % (k, v, m.errors[k]))
        
def print_minuit_gp(m, f=sys.stdout):
    assert type(m) == minuit.Minuit
    for k,v in iteritems(m.values):
        f.write('%s\t= %13.7e\n' % (k, v))
        f.write('d%s\t= %13.7e\n' % (k, m.errors[k]))

def trial_fit(ens, p, c0, e0, r1=range(8,25), r2=range(4,25), r3=range(1,25), diag=False):
    """
    strategy:
    1) fit to 1exp in range([8:]) -> c0,e0
    2) fit to 2exp in range([4:]), start with c0,e0, c1=c0, e1=2*e0 -> c0,e0,c1, e1
    3) fit to 2exp_neg in range([2:]), start with c0,e0, c1,e1, cneg=c1, eneg=e1
    """
    try:
        try:
            c0a, e0a = abs(c0), abs(e0)
            a2pt= fit_func.prepare_2pt_chi2_data(ens, p, r1, diag)
            sc1 = a2pt[0][0]
            m1  = minuit.Minuit(fit_func.fit_2pt_1exp(a2pt[0] / sc1, a2pt[1] / sc1, a2pt[3], r1), 
                                c0=c0a / sc1, e0=e0a)
            m = m1
            m1.scan(('c0', 10, 0., 2.*c0a / sc1),
                    ('e0', 10, 0., 2.*e0a))
            print('STEP 1(scan): ', m.values)
            m1.migrad()
        except minuit.MinuitError, me: print(repr(me))
        print("scale=%e\n" % (sc1,))
        print_minuit(m1)
        print('chi2 = ', m1.fval)
        #
        try:
            c0, e0 = m1.values['c0'] * sc1, m1.values['e0']
            c0a, e0a = abs(c0), abs(e0)
            a2pt= fit_func.prepare_2pt_chi2_data(ens, p, r2, diag)
            sc2 = a2pt[0][0]
            m2 = minuit.Minuit(fit_func.fit_2pt_2exp(a2pt[0] / sc2, a2pt[1] / sc2, a2pt[3], r2), 
                               c0=c0 / sc2, e0=e0, 
                               c1=c0 / sc2, e1=2. * e0)
            m = m2
            m2.scan(('c1', 10, 0., 2.*c0a / sc2),
                    ('e1', 10, e0a, 3.*e0a))
            print('STEP 2(scan):', m.values)
            m2.migrad()
        except minuit.MinuitError, me: print(repr(me))
        print("scale=%s\n" % (sc2,))
        print_minuit(m2)
        print('chi2 = ', m2.fval)
        #
        try:
            c0, e0 = m2.values['c0'] * sc2, m2.values['e0']
            c1, e1 = m2.values['c1'] * sc2, m2.values['e1']
            c1a, e1a = abs(c1), abs(e1)
            a2pt= fit_func.prepare_2pt_chi2_data(ens, p, r3, diag)
            sc3 = a2pt[0][0]
            m3 = minuit.Minuit(fit_func.fit_2pt_2exp_neg(a2pt[0] / sc3, a2pt[1] / sc3, 
                                                         a2pt[3], r3),
                                c0=c0 / sc3, e0=e0, 
                                c1=c1 / sc3, e1=e1,
                                cneg=c1 / sc3, eneg=e1)
            m = m3
            m3.scan(('cneg', 10, -3.*c1a / sc3, 3.*c1a / sc3),
                    ('eneg', 10, 0., 2.*e1a))
            print('STEP 3(scan):', m.values)
            m3.migrad()
        except minuit.MinuitError, me: print(repr(me))
        print("scale=%s\n" % (sc3,))
        print_minuit(m3)
        print('chi2 = ', m3.fval)
        #
        return m3
    except: 
        print('EXCEPTION: ', repr(sys.exc_info()[1]))
        print(m.values)
        return m
    else: return m3

