import math
from numpy import *
from numpy.numarray import array
from cov_matr import *

def cov_elem(y): return outerproduct(y,y)

def prepare_2pt_chi2_data(ens, p, t_range, diag=False):
    c2pt = [ x.get_avg_equiv(p) for x in ens ]
    c2pt_avg = reduce(lambda x,y: x + y, c2pt, 0) / len(ens)
    cov = reduce(lambda x,y: x + cov_elem(y - c2pt_avg), c2pt, 0) \
                    / len(ens) / (len(ens) - 1)
    if diag:
        for i in range(cov.shape[0]):
            for j in range(i):
                cov[i,j] = 0.
                cov[j,i] = 0.
    cov_norm = array([[ cov[t1,t2] for t1 in t_range] for t2 in t_range ])
    sigma = norm_cov_matrix(cov_norm)
    rcov_norm = recip_cov_matrix(cov_norm)
    return (array([ c2pt_avg[i] for i in t_range ]),
            sigma,
            cov_norm,
            rcov_norm)
    

"""
functions produce the following chi2:
chi2 = 0.5 * \sum_{a,b} (f(t[a]) - avg[a]) / sigma[a] * 
                        (f(t[b]) - avg[b]) / sigma[b] * 
                        rcov[a,b]
"""

def fit_2pt_2exp_neg(avg, sigma, rcov, t_range):
    """ make callable object that returns chi^2
        t_range[N]  list of specific 't'
        avg[N]      average values at specific 't'
        sigma[N]    sigmas for 'avg' 
        rcov[N,N]   inverse normalized covariance matrix for 'avg'
    """
    def func_2exp_neg(t,c0,e0,c1,e1,cneg,eneg):
        try:
            a = c0 * math.exp(-t * e0) + \
                c1 * math.exp(-t * e1) + \
                cneg * (-1)**t * math.exp(-t * eneg)
        except OverflowError: return float('inf')
        else: return a
    def chi2(c0,e0,c1,e1,cneg,eneg):
        dy = (array([ func_2exp_neg(t,c0,e0,c1,e1,cneg,eneg) 
                                     for t in t_range ]) 
              - avg ) / sigma
        return 0.5 * innerproduct(dy, dot(rcov, dy))
    return chi2

def fit_2pt_1exp_neg(avg, sigma, rcov, t_range):
    """ make callable object that returns chi^2
        t_range[N]  list of specific 't' 
        avg[N]      average values at specific 't' (NB: N !=t )
        sigma[N]    sigmas for 'avg' (NB: N !=t )
        rcov[N,N]   inverse normalized covariance matrix for 'avg' (NB: N !=t )
    """
    def func_1exp_neg(t,c0,e0,cneg,eneg):
        try:
            a = c0 * math.exp(-t * e0) + \
                cneg * (-1)**t * math.exp(-t * eneg)
        except OverflowError: return float('inf')
        else: return a
    def chi2(c0,e0,cneg,eneg):
        dy = (array([ func_1exp_neg(t,c0,e0,cneg,eneg) 
                                     for t in t_range ]) 
              - avg ) / sigma
        return 0.5 * innerproduct(dy, dot(rcov, dy))
    return chi2

def fit_2pt_2exp(avg, sigma, rcov, t_range):
    """ make callable object that returns chi^2
        t_range[N]  list of specific 't'
        avg[N]      average values at specific 't'
        sigma[N]    sigmas for 'avg' 
        rcov[N,N]   inverse normalized covariance matrix for 'avg'
    """
    def func_2exp(t,c0,e0,c1,e1):
        try:
            a = c0 * math.exp(-t * e0) + \
                c1 * math.exp(-t * e1)
        except OverflowError: return float('inf')
        else: return a
    def chi2(c0,e0,c1,e1):
        dy = (array([ func_2exp(t,c0,e0,c1,e1) 
                                     for t in t_range ]) 
              - avg ) / sigma
        return 0.5 * innerproduct(dy, dot(rcov, dy))
    return chi2
    
def fit_2pt_1exp(avg, sigma, rcov, t_range):
    """ make callable object that returns chi^2
        t_range[N]  list of specific 't'
        avg[N]      average values at specific 't'
        sigma[N]    sigmas for 'avg' 
        rcov[N,N]   inverse normalized covariance matrix for 'avg'
    """
    def func_1exp(t,c0,e0):
        try:
            a = c0 * math.exp(- t * e0) 
        except OverflowError: return float('inf')
        else: return a
    def chi2(c0,e0):
        dy = (array([ func_1exp(t,c0,e0) 
                                     for t in t_range ]) 
              - avg ) / sigma
        return 0.5 * innerproduct(dy, dot(rcov, dy))
    return chi2


def deriv_exp(c,e,t):
    """ calculate f=c*exp(-e*t) together with its derivatives; 
        return tuple (f, df/dc, df/de, d2f/dc2(==0), d2f/dcde, d2f/de2)
    """
    exp_te = math.exp(-e*t)
    return (c*exp_te, exp_te, -c*t*exp_te,
            0, -t*exp_te, c*t*t*exp_te)



    

    
def Nexp_ladder_f(t, param):
    """ sum of exponents with ascending energies
        f(t) = X_0(c_0 + X_1(c_1 + ... X_N c_N)...),
        where X_i = e^{-e^{l_i}t}, or
        f(t) = c_0 e^{-E_0 t} + c_1 e^{-E_1 t} + ... + c_N e^{-E_N t},
        where E_0 = e^{l_0}, 
              E_1 = e^{l_0} + e^{l_1},
              ...
              E_N = e^{l_0} + ... + e^{l_N}
        t       time
        param   (c0, l0, c1, l1, ... cN, lN)
        return: value of function
    """
    assert(len(param) % 2 == 0)
    Np1 = len(param) / 2
    func = 0
    for i in reversed(range(Np1)):
        func = math.exp(-t * math.exp(param[2*i+1])) * (param[2*i] + func)
    return func
def Nexp_ladder_f_df(t, param):
    """ return: (func, dfunc)
        func    value of function
        dfunc   array of derivatives of func w.r.t. corresp. parameters
    """
    assert(len(param) % 2 == 0)
    Np1 = len(param) / 2
    dEarr = array([ math.exp(param[2*i+1])
                                    for i in range(Np1) ])
    Xarr = array([ math.exp(-t * dE) 
                                    for dE in dEarr ])
    Carr = array([ param[2*i]
                                    for i in range(Np1) ])
    dfunc = zeros(shape=(2*Np1,), type=Float64)
    func = 0
    for i in reversed(range(Np1)):
        dfunc[2*i : 2*(i+1)] = ( Xarr[i], 
                               Carr[i] + func )
        dfunc[2*(i+1) : 2*Np1] *= Xarr[i]
        func = Xarr[i] * (Carr[i] + func)
    for i in range(Np1): 
        dfunc[2*i+1] *= dEarr[i] * Xarr[i] * (-t)
    return (func, dfunc)
def Nexp_ladder_f_df_ddf(t, param):
    """ return: (func, dfunc, ddfunc)
        func    value of function
        dfunc   array of derivatives of func w.r.t. corresp. parameters
        ddfunc  hessian
    """
    assert(len(param) % 2 == 0)
    Np1 = len(param) / 2
    dEarr = array([ math.exp(param[2*i+1])
                                    for i in range(Np1) ])
    Xarr    = array([ math.exp(-t * dE) 
                                    for dE in dEarr ])
    Carr    = array([ param[2*i]
                                    for i in range(Np1) ])
    dfunc   = zeros(shape=(2*Np1,), type=Float64)
    ddfunc  = zeros(shape=(2*Np1, 2*Np1), type=Float64)
    func = 0
    for i in reversed(range(Np1)):
        ddfunc[2*i:2*(i+1), 2*i:2*(i+1)] = [[0,1],[1,0]]
        for j in range(i+1,Np1):
            ddfunc[2*i,   2*j : 2*(j+1)] = [0,0]                    # row d2/dc_i ...
            ddfunc[2*i+1, 2*j : 2*(j+1)] = dfunc[2*j : 2*(j+1)]     # row d2/dX_i ...
            ddfunc[2*j : 2*(j+1), 2*i  ] = [0,0]                    # col d2/dc_i ...
            ddfunc[2*j : 2*(j+1), 2*i+1] = dfunc[2*j : 2*(j+1)]     # col d2/dX_i ...
        ddfunc[2*(i+1) : , 2*(i+1) : ] *= Xarr[i]                   # update previous ddfunc
        dfunc[2*i : 2*(i+1)] = [ Xarr[i], Carr[i] + func ]          # new dfunc
        dfunc[2*(i+1) : ] *= Xarr[i]                                # update previous dfunc
        func = Xarr[i] * (Carr[i] + func)
    
    fact = array([ [ 1, dEarr[i] * Xarr[i] * (-t) ] for i in range(Np1) ], 
                                  type=Float64, shape=(2*Np1,))
    ddfunc = ddfunc * fact.reshape((1,2*Np1)) * fact.reshape((2*Np1,1))
    for i in range(Np1):
        ddfunc[2*i+1, 2*i+1] += dEarr[i] * t * (dEarr[i] * t - 1) * Xarr[i] * dfunc[2*i+1]
    dfunc *= fact
    return (func, dfunc, ddfunc)
def Nexp_ladder_decipher(param):
    assert(len(param) % 2 == 0)
    Np1 = len(param) / 2
    Earr = zeros(type=Float64, shape=(Np1,))
    for i in range(Np1): Earr[i:] += math.exp(param[2*i + 1])
    res = zeros(type=Float64, shape=(2*Np1,))
    for i in range(Np1):
        res[2*i  ] = param[2*i]
        res[2*i+1] = Earr[i]
    return res


def Nexp_ladder_osc_f(t, param):
    assert 2 <= len(param)
    return Nexp_ladder_f(t, param[0:-2]) + param[-2] * math.exp(-t * param[-1]) * (-1)**(t%2)
def Nexp_ladder_osc_f_df(t, param):
    assert 2 <= len(param)
    df = zeros(type=Float64, shape=(len(param),))
    f, df[0:-2] = Nexp_ladder_f_df(t, param[0:-2])
    c_osc   = param[-2]
    exp_osc = math.exp(-t * param[-1]) * (-1)**(t%2)
    f += c_osc * exp_osc
    df[-2] = exp_osc
    df[-1] = c_osc * exp_osc * (-t)
    return (f, df)
def Nexp_ladder_osc_f_df_ddf(t, param):
    assert 2 <= len(param)
    df  = zeros(type=Float64, shape=(len(param),))
    ddf = zeros(type=Float64, shape=(len(param),len(param)))
    f, df[0:-2], ddf[0:-2, 0:-2] = Nexp_ladder_f_df_ddf(t, param[0:-2])
    c_osc   = param[-2]
    exp_osc = math.exp(-t * param[-1]) * (-1)**(t%2)
    f += c_osc * exp_osc
    df[-2] = exp_osc
    df[-1] = c_osc * exp_osc * (-t)
    ddf[-2,-2] = 0.
    ddf[-2,-1] = ddf[-1,-2] = exp_osc * (-t)
    ddf[-1,-1] = c_osc * exp_osc * t * t
    return (f, df, ddf)
def Nexp_ladder_osc_decipher(param):
    assert 2 <= len(param)
    res = zeros(type=Float64, shape=(len(param),))
    res[0:-2]   = Nexp_ladder_decipher(param[0:-2])
    res[-2:]    = param[-2:]
    return res



"""
from numpy.numarray import *
class exp_sum:
    def f(t, param):
        assert len(param) % 2 == 0
        func = 0.
        for i in range(len(param) / 2): func += param[2*i] * math.exp(-t * param[2*i+1])
        return func
    def df(t, param):
        np = len(param)
        assert np % 2 == 0
        dfunc = zeros(type=Float64, shape(np,))
        for i in range(np/2): 
            texp = math.exp(-t * param[2*i+1])
            dfunc[2*i : 2*(i+1)] = texp, -t * param[2*i] * texp
        return dfunc
    def f_df(t, param):
        np = len(param)
        assert np % 2 == 0
        func = 0.
        dfunc = zeros(type=Float64, shape(np,))
        for i in range(np/2): 
            texp = math.exp(-t * param[2*i+1])
            func += param[2*i] * texp
            dfunc[2*i : 2*(i+1)] = texp, -t * param[2*i] * texp
        return (func, dfunc)
    def ddf(t, param):
        np = len(param)
        assert np % 2 == 0
        ddfunc = zeros(type=Float64, shape(np,))
        for i in range(np/2):
            texp = math.exp(-t * param[2*i+1])
            ddfunc[2*i, 2*i] = 0.
            ddfunc[2*i, 2*i+1] = ddfunc[2*i+1, 2*i] = -t * texp
            ddfunc[2*i+1, 2*i+1] = t * t * texp
        return ddfunc
    def f_df_ddf(t,param):
        np = len(param)
        assert np % 2 == 0
        func = 0.
        dfunc = zeros(type=Float64, shape(np,))
        ddfunc = zeros(type=Float64, shape(np,))
        for i in range(np/2):
            texp = math.exp(-t * param[2*i+1])
            func += param[2*i] * texp
            dfunc[2*i : 2*(i+1)] = texp, -t * param[2*i] * texp
            ddfunc[2*i, 2*i] = 0.
            ddfunc[2*i, 2*i+1] = ddfunc[2*i+1, 2*i] = -t * texp
            ddfunc[2*i+1, 2*i+1] = t * t * texp
        return (func, dfunc, ddfunc)
    """
    
def fit_chi2(avg, sigma, rcov, arg_range, func, dfunc, ddfunc):
    """ prepare functions for fmin_ncg
        return tuple (chi2, grad(chi2), hess(chi2))
        order of parameters (c0,e0,c1,e1,c2,e2 ... cN, eN)
    """
    def chi2(arg):
        f = array([ (func(t, arg) - avg[a]) / sigma[a] 
                                   for a,t in enumerate(arg_range) ])
        return dot(f, dot(rcov, f))
    def chi2_grad(arg):
        np = len(arg)
        nt = len(avg)
        f   = zeros(type=Float64, shape=(nt,))
        df  = zeros(type=Float64, shape=(nt,np))
        for a,t in enumerate(arg_range):
            f[a], df[a] = dfunc(t, arg)
        f   -= avg
        f   /= sigma
        df  /= sigma.reshape((nt,1))  # divide each row by corresponding sigma
        return 2. * dot(f, dot(rcov, df))
    def chi2_hess(arg):
        np = len(arg)
        nt = len(avg)
        f   = zeros(type=Float64, shape=(nt,))
        df  = zeros(type=Float64, shape=(nt,np))
        ddf = zeros(type=Float64, shape=(nt,np,np))
        for a,t in enumerate(arg_range):
            f[a], df[a], ddf[a] = ddfunc(t, arg)
        f   -= avg
        f   /= sigma
        df  /= sigma.reshape((nt,1))
        ddf /= sigma.reshape((nt,1,1))
        return 2. * dot(df.transpose(), dot(rcov, df)) # + 2. * tensordot(dot(f,rcov), ddf, axes=(0,0))
    return (chi2, chi2_grad, chi2_hess)

def fit_chi2_Nexp_ladder(a, s, rc, tr): 
    return fit_chi2(a, s, rc, tr, 
                    Nexp_ladder_f, 
                    Nexp_ladder_f_df, 
                    Nexp_ladder_f_df_ddf)

def fit_chi2_Nexp_ladder_osc(a, s, rc, tr):
    return fit_chi2(a, s, rc, tr, 
                    Nexp_ladder_osc_f, 
                    Nexp_ladder_osc_f_df, 
                    Nexp_ladder_osc_f_df_ddf)



def meff_func(c2pt_f, c2pt_f_df, c2pt_f_df_ddf):
    def meff_f(t, param):
        x = array((1.,) + tuple(param))     # c0==1
        c2pt_f0     = c2pt_f  (t,   x)
        c2pt_f1     = c2pt_f  (t+1, x)
        return math.log(abs(c2pt_f0)) - math.log(abs(c2pt_f1))
    def meff_f_df(t, param):
        x = array((1.,) + tuple(param))     # c0==1
        c2pt_f0, c2pt_df0   = c2pt_f_df (t,   x)
        c2pt_f1, c2pt_df1   = c2pt_f_df (t+1, x)
        return (math.log(abs(c2pt_f0)) - math.log(abs(c2pt_f1)),
                c2pt_df0[1:] / c2pt_f0 - c2pt_df1[1:] / c2pt_f1)
    def meff_f_df_ddf(t, param):
        x = array((1.,) + tuple(param))     # c0==1
        c2pt_f0, c2pt_df0, c2pt_ddf0    = c2pt_f_df_ddf(t,   x)
        c2pt_f1, c2pt_df1, c2pt_ddf1    = c2pt_f_df_ddf(t+1, x)
        return (math.log(abs(c2pt_f0)) - math.log(abs(c2pt_f1)),
                c2pt_df0[1:] / c2pt_f0 - c2pt_df1[1:] / c2pt_f1,
                - outerproduct(c2pt_df0[1:], c2pt_df0[1:]) / c2pt_f0**2 \
                + c2pt_ddf0[1:,1:] / c2pt_f0 \
                + outerproduct(c2pt_df1[1:], c2pt_df1[1:]) / c2pt_f1**2 \
                - c2pt_ddf1[1:,1:] / c2pt_f1)
    return (meff_f, meff_f_df, meff_f_df_ddf)

def fit_chi2_meff_Nexp_ladder(a,s,rc,tr):
    return fit_chi2(a,s,rc,tr,
                    *meff_func(Nexp_ladder_f, 
                               Nexp_ladder_f_df, 
                               Nexp_ladder_f_df_ddf))
def fit_chi2_meff_Nexp_ladder_osc(a,s,rc,tr):
    return fit_chi2(a,s,rc,tr,
                    *meff_func(Nexp_ladder_osc_f, 
                               Nexp_ladder_osc_f_df, 
                               Nexp_ladder_osc_f_df_ddf))

    
from scipy import linalg
def fit_lma(avg, sigma, cov, arg_range, func, dfunc):
    def f_vec(param):
        return dot(Bcr.transpose(), 
                   array([ (func(x, param) - avg[a]) / sigma[a] 
                           for a,x in enumerate(arg_range) ]))
    def Df_matr(param):
        df = zeros(type=Float64, shape=(len(arg_range), len(param)))
        for a,x in enumerate(arg_range):
            df[a] = dfunc(x, param)[1] / sigma[a]
        return dot(Bcr.transpose(), df)
    # inverse Cholesky factor of the covariance matrix
    Bcr = linalg.inv(linalg.cholesky(cov))
    return (f_vec, Df_matr)

def fit_lma_meff_Nexp_ladder(a,s,c,tr):
    return fit_lma(a,s,c,tr,
                   *meff_func(Nexp_ladder_f, 
                              Nexp_ladder_f_df, 
                              Nexp_ladder_f_df_ddf)[0:2])
def fit_lma_meff_Nexp_ladder_osc(a,s,c,tr):
    return fit_lma(a,s,c,tr,
                   *meff_func(Nexp_ladder_osc_f, 
                              Nexp_ladder_osc_f_df, 
                              Nexp_ladder_osc_f_df_ddf)[0:2])
    
