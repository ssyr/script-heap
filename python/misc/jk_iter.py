def jk_subset(list, func=lambda x: x, binsize=1):
    """
    jk_subset(list, func=<id>, binsize=1)
    Resample list with Jackknife plan, with blocking;
    func is applied to each sample separately (not blocks); this is meaningful in 
    the case of covariance matrix estimation.
        binsize is ignored so far
    """
    if len(list) <= 1 or binsize < 1: raise StopIteration
    n = len(list)
    n_bins = n / binsize
    if n_bins <= 1: raise StopIteration
    n = n_bins * binsize
    sum = func(list[0])
    for i in range(1,n): sum += func(list[i])
    for i in range(n_bins): 
        a = sum;
        for j in range(binsize):
            a -= func(list[i*binsize+j])
        yield a / binsize / (n_bins - 1)

