from fit_func import *
from numpy.numarray import *
import math

def fit_ensemble(ens, binsize):
    if len(ens) <= 2 or binsize < 1: raise StopIteration
    n = len(ens)
    n_bins = n / binsize
    if n_bins <= 1: raise StopIteration
    n = n_bins * binsize
    sum = reduce(lambda x,y: x+y, ens, 0)
    avg = sum / n
    sum_cov = reduce(lambda x,y: x + cov_elem(y - avg), ens, 0)
    n_sub_ens = (n_bins - 1) * binsize
    for i_bin in range(n_bins):
        sub_ens = ens[binsize*i_bin:binsize*(i_bin+1)]
        avg_sample = (sum - reduce(lambda x,y: x+y, sub_ens, 0)) / n_sub_ens
        cov_sample = (sum_cov - reduce(lambda x,y: x+cov_elem(y-avg), sub_ens,0)) \
                        / n_sub_ens / math.sqrt(n_bins - 2)
        yield (avg_sample, cov_sample)
