import os
import sys

import math
import numpy.linalg
from scipy.optimize.optimize import *
from numpy.numarray import *
import minuit
import aff
import aff_import

from jk_iter import *
from cov_matr import *
from minuit_func import *
from fit_func import *

geometry=[20,20,20,64]
cset = init_corr_set_2pt(array(geometry), 0)
aff_i = aff_import.aff_import(4)

head_dir = '32c64_m004_m030.v2.full/'
list_files = (head_dir + 'list/hadspec.proton3',
              head_dir + 'list/hadspec.protonNegpar3')
ens = []
for name in list_files:
    ls_f = open(name, 'r');
    ens.extend(init_ensemble_2pt(cset, ls_f, aff_i, head_dir))
    close(ls_f)
