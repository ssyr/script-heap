from future.utils import iteritems
import re
import types
import sys

def my_reload(patt):
    res = []
    for k,v in iteritems(sys.modules):
        if (not None is re.match(patt, k) and type(v) == types.ModuleType):
            res.append(reload(v))
    return res

