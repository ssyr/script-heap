# minuit part



# CG/NCG part
m   = trial_fit(ens,array([0,0,0]), c0=7.4594592e-09, e0=4.7549218e-01, 
                r1=range(16,26), r2=range(2,26), r3=range(2,26))
x0  = array([ m.values['c0'], math.log(m.values['e0']), 
              m.values['c0'], math.log(m.values['e0']) ], type=Float64)
a   = prepare_2pt_chi2_data(ens, array([0,0,0]), range(2,26))
    
scale_factor = a[0][0]
ff  = fit_Nexp_ladder(a[0] / scale_factor, a[1] / scale_factor, a[3], range(2,26))
    
x1  = x0
x1  = fmin_cg(ff[0], x1, ff[1])
x1  = x0
x1  = fmin_ncg(ff[0], x1, ff[1], hess=ff[2])



import scipy.optimize
t_range=range(2,26)
scale=1e-9
x0=array([  6.85678683e+00,  -2.26665633e-01,   6.99999632e+01, 3.12295425e+00])
c2pt=get_c2pt(ens, array([0,0,0]),t_range)
(avg_tot, cov_tot) = gaussian_estimate(c2pt)
sigma_tot = norm_cov_matrix(cov_tot)
flma=fit_func.fit_lma(avg_tot/scale, sigma_tot/scale, cov_tot, t_range, fit_func.Nexp_ladder_f, fit_func.Nexp_ladder_df)
scipy.optimize.leastsq(flma[0], x0, Dfun=flma[1], full_output=1)


c2pt=get_c2pt(ens, array([0,0,0]),t_range)
(avg_tot, cov_tot) = gaussian_estimate(c2pt)
sigma_tot = norm_cov_matrix(cov_tot)
rcov_tot=recip_cov_matrix(cov_tot)


avg_test=array([ 1.*exp(-t) + 2.*exp(-2*t) for t in t_range])
sigma_test=array([ .1*exp(-t) for t in t_range ])
cov_test=zeros(type=Float64, shape=(len(t_range),len(t_range)))
for i in range(len(t_range)): cov_test[i,i] = 1.

rcov_test=recip_cov_matrix(cov_test)
f_tot=fit_func.fit_Nexp_ladder(avg_test, sigma_test, rcov_test, t_range)
fmin_ncg(f_tot[0],x0.copy(), f_tot[1], fhess=f_tot[2], disp=1)

flma=fit_func.fit_lma(avg_test, sigma_test, cov_test, t_range, 
                      fit_func.Nexp_ladder_f, fit_func.Nexp_ladder_df)
scipy.optimize.leastsq(flma[0],x0.copy(), Dfun=flma[1])


def meff_estimate(ens,p, t_range):
    c2pt = [ x.get_avg_equiv(p) for x in ens ]
    meff = [ array([ math.log(c2[t] / c2 [t+1]) for t in t_range ]) 
                for c2,v2 in jk_gaussian_estimate(c2pt) ]
    meff_avg, meff_cov = gaussian_estimate(meff)
    return meff_avg, meff_cov * (len(ens) - 1)**2


meff_avg, meff_cov = meff_estimate(ens, array([0,0,0]), t_range)
meff_sigma = norm_cov_matrix(meff_cov)
for a,t in enumerate(t_range):
    sys.stdout.write('%d\t%14.7e\t%14.7e\n' %(t, meff_avg[a], meff_sigma[a]))

