from __future__ import print_function
import math
import fit_func
#import fit_2pt
from cov_matr import *
from corr_set import *

from scipy.optimize.optimize import *
from numpy.numarray import *

def cov_elem(vec): return outerproduct(vec, vec)

def jk_gaussian_estimate(ens, binsize=1, diag=False):
    """ 
    a cycle over JK ensemble of resampled values
    return (avg, cov)
    """
    if len(ens) <= 2 or binsize < 1: raise StopIteration
    n = len(ens)
    n_bins = n / binsize
    if n_bins <= 1: raise StopIteration
    n = n_bins * binsize
    sum = reduce(lambda x,y: x + y, ens, 0)
    avg = sum / n
    sum_cov = reduce(lambda x,y: x + cov_elem(y - avg), ens, 0)
    n_sub_ens = (n_bins - 1) * binsize
    for i_bin in range(n_bins):
        sub_ens = ens[binsize*i_bin:binsize*(i_bin+1)]
        avg_sample = (sum - reduce(lambda x,y: x + y, sub_ens, 0)) / n_sub_ens
        # this estimates the variance of avg_sample; change to estimator of the variance of average.
#        cov_sample = (reduce(lambda x,y: x + cov_elem(y - avg_sample), ens, 0) - 
#                        reduce(lambda x,y: x + cov_elem(y - avg_sample), sub_ens, 0)) / n_sub_ens \
#                        * binsize / n / (n_sub_ens - 1)
        # with this method of evaluating the covariance, 
        # E(1st line) = (1-1/n)Dx
        # D(avg_sample) = 1/(n-binsize)Dx
        cov_sample = (sum_cov - reduce(lambda x,y: x + cov_elem(y - avg), sub_ens,0)) / n_sub_ens \
                            * n / n_sub_ens / (n - 1)
        if diag:
            for i in range(cov_sample.shape[0]):
                for j in range(i): 
                    cov_sample[i,j] = 0.
                    cov_sample[j,i] = 0.
        yield (avg_sample, cov_sample)

def gaussian_estimate(ens, diag=False):
    """
    estimate average and its covariance matrix
    ens     vector of samples
    returns:    (avg, cov)
    avg     average
    cov     covariance of average estimate
    """
    n = len(ens)
    if n <= 1: return None
    avg = reduce(lambda x,y: x + y, ens, 0) / n
    cov = reduce(lambda x,y: x + cov_elem(y - avg), ens, 0) / n / (n - 1)
    if diag:
        for i in range(cov.shape[0]):
            for j in range(i):
                cov[i,j] = 0.
                cov[j,i] = 0.
    return (avg, cov)
    
def make_chi2_2pt(avg, cov, t_range=None, diag=False, f_2pt=None, **argd):
    """ 
    take average values and covariance of average estimate, return function(s) to fit
    avg     vector of average estimates
    cov     matrix of covariance estimates
    t_range list of 't' argument values corresponding to entries of 'avg' and 'cov'
    diag    whether to make 'cov' diagonal
    f_2pt   function which takes avg, sigma, rcov, t_range, **argd and returns functions to fit
    argd    additional arguments to 'f_2pt'
    """
    if (t_range is None or f_2pt is None): return None
    cov_copy = cov.copy();
    sigma = norm_cov_matrix(cov_copy)
    rcov_norm = recip_cov_matrix(cov_copy)
    return f_2pt(avg, sigma, rcov_norm, t_range, **argd)
    
def jk_fit_cg(ens, x0, func_gen, func_decipher, binsize=1, scale=None, diag=False,**argd):
    if len(ens) % binsize != 0: 
        sys.stderr.write('Discard last %d elements\n' % (len(ens) % binsize,))
    n   = len(ens) / binsize
    len_ens = n * binsize
    (avg_tot, cov_tot) = gaussian_estimate(ens[0:len_ens], diag)
    if scale is None: scale = math.sqrt((avg_tot * avg_tot).sum())
    assert 0 < scale
    sigma_tot = norm_cov_matrix(cov_tot)
    rcov_tot = recip_cov_matrix(cov_tot)
    f_tot = func_gen(avg_tot / scale, sigma_tot / scale, rcov_tot, **argd)
    a_tot = fmin_cg(f_tot[0], x0, f_tot[1], disp=0)
    print(2.*f_tot[0](a_tot))
    res = []
    chi2= []
    for avg, cov in jk_gaussian_estimate(ens[0:len_ens], binsize, diag):
        # XXX use covariance matrix estimate over the total ensemble
        f = func_gen(avg / scale, sigma_tot / scale, rcov_tot, **argd)
        a = fmin_cg(f[0], a_tot, f[1], disp=0)
        res.append(func_decipher(a))
        chi2.append(2. * f[0](a))
        sys.stderr.write('.')
    sys.stderr.write('\n')
    res_tot = func_decipher(a_tot)
    (res_jk_avg, res_jk_cov) = gaussian_estimate(res)
    res_corrected   = n * res_tot - (n - 1) * res_jk_avg
    chi2_tot = 2. * f_tot[0](a_tot)
    (chi2_jk_avg, chi2_jk_cov) = gaussian_estimate(chi2)
    chi2_corrected  = n * chi2_tot - (n - 1) * chi2_jk_avg
    return (res_tot, res_jk_cov * (n-1)**2, chi2_tot, chi2_jk_cov[0,0] * (n-1)**2);
    #    return (res_tot, res_jk_avg, res_corrected, res_jk_cov * (n-1)**2, 
    #            chi2_tot, chi2_jk_avg, chi2_corrected, chi2_jk_cov[0,0] * (n-1)**2)
    
def fit_cg(func_gen, ens, x0, scale=None, diag=False, **argd):
    n   = len(ens)
    (avg_tot, cov_tot) = gaussian_estimate(ens, diag);
    if scale is None: scale = math.sqrt((avg_tot * avg_tot).sum())
    assert 0 < scale
    sigma_tot = norm_cov_matrix(cov_tot)
    rcov_tot = recip_cov_matrix(cov_tot)
    f_tot = func_gen(avg_tot / scale, sigma_tot / scale, rcov_tot, **argd)
    #    f_tot = make_chi2(avg_tot / scale, cov_tot / scale / scale, **argd)
    a   = fmin_cg(f_tot[0], x0, f_tot[1], disp=1)
    return (a, 2.*f_tot[0](a))

def get_c2pt(ens, p, t_range):
    c2pt = [ x.get_avg_equiv(p) for x in ens ]
    return [ array([ c[t] for t in t_range]) for c in c2pt ]

def cycle_trange_fit_cg(ens, p, x0, 
                         tmin0, tmin1, tmax,
                         func_gen, func_decipher,
                         binsize=1, param_dim=None,
                         **argd):
    if x0 is None: 
        x0c = zeros(type=Float64, shape=(param_dim,))
        assert 0 < param_dim and 0 == param_dim % 2
        for i in range(param_dim / 2): x0c[2*i] = 1.
    else: 
        x0c = x0.copy();
        param_dim = len(x0)
    for tmin in range(tmin0, tmin1+1):
        t_range = range(tmin, tmax+1)
        ndf = tmax - tmin + 1 - param_dim
        c2pt = get_c2pt(ens, p, t_range)
        (res, res_cov, chi2,chi2_cov) = jk_fit_cg(c2pt, x0c, 
                                                   func_gen, func_decipher, binsize, 
                                                   tr=t_range, **argd)
        sys.stdout.write('%d\t%d\t' % (tmin, tmax))
        for i in range(len(res)): sys.stdout.write('%14.7e\t' % res[i])
        sys.stdout.write('%13.7e\t' % (chi2/ndf,))
        for i in range(len(res)): sys.stdout.write('%14.7e\t' % math.sqrt(res_cov[i,i]))
        sys.stdout.write('%14.7e\n' % (math.sqrt(chi2_cov)/ndf,))
    
