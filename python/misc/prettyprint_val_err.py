def exp10(pwr):
    abspwr = abs(pwr)
    res = 1
    a = 10
    while 0 < abspwr:
	if abspwr % 2 == 1: res *= a
	abspwr /= 2
	a *= a
    if (0 <= pwr): return res
    else: return 1. / res

def prettyprint_val_err(val, err, errmax=100):
    assert 0 < err
    pwr_err = int(math.ceil(math.log10(float(err) / errmax)))
    err_norm= int(err * exp10(-pwr_err))
    pwr_val = int(math.floor(math.log10(abs(val))))
    val_norm= val * exp10(-pwr_val)
    prec_val = pwr_val - pwr_err
    if (prec_val < 0): prec_val = 0
    return ('%%.%df(%%d)e%%d' % prec_val) % (val_norm, err_norm, pwr_val)
