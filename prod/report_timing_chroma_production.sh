#!/bin/bash
for dir in forward/2500 backward/2500 ; do
  echo "============= dir: $dir =================="
  for f in $dir/*.output ; do
    echo "-------------- file: $f ----------------"
    grep 'machine' $f
    grep 'QDP:FlopCount:MDWFQpropT_Single_Prec: Performance/CPU:\|QDP:FlopCount:MDWFQpropT_Double_Prec: Performance/CPU\|PROPAGATOR: total time = ' $f
    grep 'CHROMA: total time = ' $f
    echo ; echo
  done
done
