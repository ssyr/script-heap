#!/bin/bash


[ $# -ge 1 ] || {
  cat >&2 <<EOF
Create subdirs and move files to them
Usage:
$(basename $0)  
  [--src-dir=<src-dir>]
  [--dst-dir=<dst-dir>]
  c1 c2 ...

EOF
  exit 1
}
src_dir=.
dst_dir=.

while [[ $1 == --* ]] ; do
  case "$1" in 
  --src-dir=*)    src_dir=$(echo $1 |sed -e 's?--src-dir=??') ;; 
  --dst-dir=*)    dst_dir=$(echo $1 |sed -e 's?--dst-dir=??') ;; 
  esac
  shift 1
done

flist=$(mktemp)
ls $src_dir >$flist

for c in $* ; do
  d="$dst_dir/$c"
  echo $d
  mkdir -p "$d"
  grep '\.'$c'\.' $flist \
    |gawk '{print "'$src_dir'/"$1 }' \
    |xargs mv -n -t "$d"
done
rm -f $flist
