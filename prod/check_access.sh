#!/bin/bash

# <perm=[rwx]+>  <file> ...
function check_perm {
  if [ $# -lt 2 ] ; then 
    echo >&2 "check_perm: error in parameters" 
    exit 1 
  fi
  local perm=$1
  shift 1
  local f
  for f in $@ ; do
    if [[ "$perm" =~ 'r' ]] && [ ! -r "$f" ] ; then
      echo >&2 "$f: cannot read"
      return 1
    fi
    if [[ "$perm" =~ 'w' ]] && [ ! -w "$f" ] ; then
      echo >&2 "$f: cannot write"
      return 1
    fi
    if [[ "$perm" =~ 'x' ]] && [ ! -x "$f" ] ; then
      echo >&2 "$f: cannot list/execute"
      return 1
    fi
  done
}

check_perm  rx   /home/syritsyn/data/BMW/3.31/48c48_ml-0.09900_mh-0.04/gauge.nersc
check_perm  r   /home/syritsyn/data/BMW/3.31/48c48_ml-0.09900_mh-0.04/gauge.nersc/00_071203_154902.conf
check_perm  r    /home/syritsyn/data/BMW/3.31/48c48_ml-0.09900_mh-0.04/gauge.nersc/juelich/run0/lat.48_48.3_071203_154902

check_perm  rx   /home/syritsyn/data/BMW/3.31/48c48_ml-0.09900_mh-0.04/c3pt.run3
check_perm  rwx  /home/syritsyn/data/BMW/3.31/48c48_ml-0.09900_mh-0.04/c3pt.run3/{hadspec,bb}

check_perm  rx   bin bin/*
check_perm  r    *.qlua

check_perm  rwx  . logs/*
