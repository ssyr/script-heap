#!/bin/bash

function print_usage(){
  cat >&2 <<EOF
Usage:
$(basename $0)  [-t <delay>] [-n <count>] <jobid> 

EOF
}

[ $# -ge 1 ] || {
  print_usage
  exit 1
}

delay=60
count=1000000
todo=log_top_mem.sh
dokill=

while [[ $1 == -* ]] ; do
  case $1 in
    -t) delay=$2 ; shift 2 ;;
    -n) count=$2 ; shift 2 ;;
    -k) dokill=yes ; shift 1 ;;
    --) break ;;
    *) 
      print_usage
      exit 1
  esac
done

jobid=$1
for h in $(fqstat $jobid |grep 'ds\|bc\|pi' ) ; do
  logf=/lqcdproj/lhpc/sns-prod/log_top_mem/$jobid/$h
  if [ "x$dokill" == "xyes" ] ; then
    rsh -n $h killall $(basename $todo) </dev/null >/dev/null 2>&1 &
  else 
    rsh -n $h $todo $logf $count $delay </dev/null >/dev/null 2>&1 &
  fi
done
