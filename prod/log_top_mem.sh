#!/bin/bash

[ $# -eq 3 ] || { echo >&2 "usage: <logfile> <count> <delay>" ; exit 1 ; }
logf="$1"
count=$2
delay=$3

mkdir -p $(dirname $logf)
while [[ $count -ne 0 ]] ; do
  echo "***********"
  date
  top -bmn1 |head -n 5
  top -bmn1 |grep $USER
  sleep $delay
  count=$((count - 1))
done >>$logf

