#!/bin/bash

watch -n 300 '
  echo -ne "\n  ------ WAITING ------\n"
  llq -x |grep $USER |grep " I " |gawk '\''{printf("%s %s\n", $0, NR)}'\''
  
  echo -ne "\n  ------ RUNNING ------\n"
  llq -x |grep " R " |grep $USER
  
  echo -ne "\n  ------ RUNNING(other) ------\n"
  llq -x |grep " R " |grep -v $USER
  
  #echo -ne "\n  ------ WAITING(other) ------\n"
  #cqstat -f |grep -v running |grep -v $USER'

