#!/usr/bin/env python
from __future__ import print_function
import os, sys, re
import traceback
import numpy as np, h5py
import math, cmath

def print_usage(prg) :
    sys.stderr.write("""Usage:
%s  
    [--verbose|-v]                      # verbose output
    [--recursive|-r]                    # recursive comparison
    [--files-from=<filelist>|-]         # get files/keys to compare from a file
    <h5fileA1>  [-k <h5kpathA1>|--key=<h5kpathA1>]  <h5fileB1>  [-k <h5kpathB1>|--key=<h5kpathB1>]
    ...

If filelist option is used, each line must contain 4 space-separated names
<h5fileA1>   <h5kpathA1>  <h5fileB1>   <h5kpathB1>
...
""" % prg)

# TODO separate silent mode (no printing normRatio,cosAngle), only match/mismatch
# TODO scaled comparison
#    [--scale=<scale>]                   # use scale
# TODO comparison up to rel/abs precision overall or by component

def print_err(s) :
    exc = sys.exc_info()
    sys.stderr.write("ERROR %s: %s\n" % (s, repr(exc[1])))

def get_val(s, prefix) :
    if not s.startswith(prefix) : return None
    else : return s[len(prefix):]


argv    = list(sys.argv)
prg     = argv[0]

if len(argv) < 2 :
    print_usage(argv[0])
    sys.exit(1)

scal    = None
flist   = None
do_recursive = False
VERBOSE     = 0
while 1 < len(argv) and argv[1].startswith('-') :
    if   argv[1].startswith('--scal=') :
        scal_str = re.match('^--scal=(.+)$', argv[1]).groups()[0]
        try :
            scal = eval("0+(%s)" % scal_str)     # some protection from mistakes
        except :
            raise
            print_err(scal_str)
            sys.exit(1)
        print("scal = ", scal)

    elif argv[1].startswith('--files-from=') :
        flist = re.match('^--files-from=(.+)$', argv[1]).groups()[0]

    elif '--verbose' == argv[1] or '-v' == argv[1] :
        VERBOSE = 1

    elif '--recursive' == argv[1] or '-r' == argv[1] :
        do_recursive = True

    else :
        print_usage(prg)
        sys.exit(1)

    argv.pop(1)


def np_cmp_str(x, y) :
    try : 
        x_n2   = (np.abs(x)**2).sum()
        y_n2   = (np.abs(y)**2).sum()
        if  0. == x_n2 :
            if 0. == y_n2 : return '0/0'
            else : return '0/1'
        else :
            if 0. == y_n2 : return '1/0'
                
        cxy     = (x.conj() * y).sum() / math.sqrt(x_n2 * y_n2)
        cxy_r, cxy_ph = cmath.polar(cxy)
        return "%21.16e (%21.16e,%21.16e)" % (math.sqrt(x_n2 / y_n2), cxy_r, cxy_ph)
    except : 
        sys.stdout.write("%s\n" % str(sys.exc_info()))
        return "(error)"
        



def cmp_file(h5fileA, h5kpathA, h5fileB, h5kpathB) :
    tit = "%s[%s] <> %s[%s]" % (h5fileA, h5kpathA, h5fileB, h5kpathB)

    def cmp_obj(h5dA, h5dB) :
        titA = "%s[%s]" % (h5dA.file.filename, h5dA.name)
        titB = "%s[%s]" % (h5dB.file.filename, h5dB.name)
        if isinstance(h5dA, h5py.Group) and isinstance(h5dB, h5py.Group) :
            if do_recursive :
                klistA  = h5dA.keys()
                klistB  = h5dB.keys()
                klist   = list(set(klistA + klistB))
                for k in sorted(klist) :
                    if   not k in klistA : sys.stdout.write("%s : only in %s\n" % (k, titB))
                    elif not k in klistB : sys.stdout.write("%s : only in %s\n" % (k, titA))
                    else : cmp_obj(h5dA[k], h5dB[k])
            
            else : 
                sys.stdout.write("%s: use '--recursive' for Group comparison\n")

        elif isinstance(h5dA, h5py.Dataset) and isinstance(h5dB, h5py.Dataset) :
            sys.stdout.write("%s  <>  %s : %s\n" % (
                    titA, titB, np_cmp_str(h5dA[()], h5dB[()])))

        else : 
            sys.stdout.write("type(%s)='%s'  <>  type(%s)='%s'\n" % (
                    titA, type(h5dA), titB, type(h5dB)))

    try : 
        if 0 < VERBOSE :
            sys.stdout.write("CMP %s\n" % (tit,))

        h5fA, h5fB  = None, None

        try : h5fA    = h5py.File(h5fileA, 'r')
        except IOError : sys.stdout.write("%s\n" % str(sys.exc_info()))
        
        try : h5fB    = h5py.File(h5fileB, 'r')
        except IOError : sys.stdout.write("%s\n" % str(sys.exc_info()))

        if None is h5fA or None is h5fB : return


        h5dA, h5dB = None, None

        try : h5dA    = h5fA[h5kpathA]
        except KeyError : sys.stderr.write("%s[%s] : does not exist\n" % (h5fileA, h5kpathA))

        try : h5dB    = h5fB[h5kpathB]
        except KeyError : sys.stderr.write("%s[%s] : does not exist\n" % (h5fileB, h5kpathB))

        if None is h5dA or None is h5dB : return


        cmp_obj(h5dA, h5dB)

        h5fA.close()
        h5fB.close()

    except: 
        raise
        #print_err(tit)


#if 0 != (len(argv) - 1) % 4 :
    #print(argv)

cmdline   = list(argv[1:])
while 0 < len(cmdline) :
    try : 
        h5fA    = cmdline.pop(0)
        h5kA    = '/'
        if 0 < len(cmdline) :
            if cmdline[0].startswith('--key==') : 
                h5kA    = get_val(cmdline.pop(0), '--key=')
            elif '-k' == cmdline[0] : 
                cmdline.pop(0)
                h5kA    = cmdline.pop(0)

        h5fB    = cmdline.pop(0)
        h5kB    = '/'
        if 0 < len(cmdline) :
            if cmdline[0].startswith('--key==') : 
                h5kB = get_val(cmdline.pop(0), '--key=')
            elif '-k' == cmdline[0] : 
                cmdline.pop(0)
                h5kB = cmdline.pop(0)

    except IndexError :
        sys.stderr.write("error in file/kpath list in cmdlin\n")
        print_usage(prg)
        sys.exit(1)
        
    if VERBOSE: sys.stdout.write("CMP %s[%s] <> %s[%s]\n" % (h5fA, h5kA, h5fB, h5kB))

    cmp_file(h5fA, h5kA, h5fB, h5kB)

if not None is flist :
    if '-' == flist : flist_f = sys.stdin
    else : flist_f = open(flist, 'r')

    for i_l, l in enumerate(flist_f.readlines()) :
        if re.match('^\s*$', l) : 
            continue
        l_s = l.split()
        if 4 != len(l_s) :
            sys.stderr.write("SYN.ERROR IN '%s':%d '%s'\n" % (flist, i_l, l))
            continue
        h5fileA, h5kpathA, h5fileB, h5kpathB = l_s[:4]
        cmp_file(h5fileA, h5kpathA, h5fileB, h5kpathB)

    if '-' != flist : 
        flist_f.close()
