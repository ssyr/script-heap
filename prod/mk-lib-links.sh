#!/bin/bash

# create search list
dir_list=''
for d in $@ ; do
  dir_list="$dir_list  ../$d"
done

if [ "x$dir_list" == "x" ] ; then
  dir_list='..'
fi

echo "dir_list='$dir_list'"

# search libs
mkdir -p all-lib
( cd all-lib || exit 1
  rm -f *
  for f in $(find $dir_list -name \*.so\* -or -name \*.dylib
      |grep -v all-lib |grep -v python |grep -v '/work-') ; do
    [ -d $f ] && continue
    echo ln -s $f .
    ln -s $f .
  done
)

# search bins
mkdir -p all-bin
( cd all-bin || exit 1
  rm -f *
  for f in $(find $dir_list -type f |grep '/bin/' |grep -v all-bin |grep -v '/work-') ; do
    [ -x $f ] && [ ! -d $f ] || continue
    ln_f=`basename $f`
    if [ -e $ln_f ] ; then
      # ambiguity; put a stub
      if grep "Conflicting binaries for $ln_f" $ln_f ; then
        echo "echo -e >&2 \"$f\"" >>$ln_f
      else
        old_f=$(ls -l $ln_f |sed -e 's?^.*-> ??')
        rm $ln_f
        cat >$ln_f <<EOF
#!/bin/bash
echo -e >&2 "Conflicting binaries for '$ln_f'; use a full path:"
echo -e >&2 "$old_f"
echo -e >&2 "$f"
EOF
      fi
      chmod u+x $ln_f
    else
      echo ln -s $f .
      ln -s $f .
    fi
  done
)
echo THIS FILE
