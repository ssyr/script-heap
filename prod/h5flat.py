#!/usr/bin/env python
from __future__ import print_function
import sys, h5py
import numpy as np

def h5_cp_flat(h5f_src, ksrc, h5f_dst, kdst, key_shape=True) :
    dsrc    = h5f_src[ksrc]
    dt_src  = dsrc.dtype
    xdst    = None
    if str(dt_src).startswith('complex') :
        xsrc    = dsrc.value
        xdst    = np.r_['-1', xsrc.real[..., None], xsrc.imag[..., None]]
    else : 
        xdst    = dsrc.value

    if key_shape : 
        sh_str = '__%s' % 'x'.join([ '%d' % i for i in xdst.shape ])
    else : sh_str = ''

    kdst_full   = '%s%s' % (kdst, sh_str)
    try : del h5f_dst[kdst_full]
    except : pass
    h5f_dst[kdst_full] = xdst.flat
    h5f_dst.flush()

def print_usage() : 
    print('Usage:\n%s  <h5f-src>  <h5f-dst>  <k1> [...]' % sys.argv[0])

if len(sys.argv) < 3 :
    print_usage()
    sys.exit(1)

h5f_src = h5py.File(sys.argv[1], 'r')
h5f_dst = h5py.File(sys.argv[2])


# feed keys to the file using
# h5ls -r src.h5 |grep Dataset |grep -v DATALIST |sed -e 's? Dataset.*$??' |xargs h5flat.py src.h5 dst.h5
for k in sys.argv[3:] :
    h5_cp_flat(h5f_src, k, h5f_dst, k, key_shape=True)

h5f_src.close()
h5f_dst.close()
