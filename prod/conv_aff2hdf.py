#!/usr/bin/env python2 
from __future__ import print_function
import h5py
import aff
import sys

def conv_aff2hdf_grp(h5g, aff_r, aff_kpath) :
    for k in aff_r.ls(aff_kpath) :
        aff_kpath_new = "%s/%s" % (aff_kpath, k)
        t = aff_r.type(aff_kpath_new)
        if list is t : 
            print("GROUP\t%s" % aff_kpath_new)
            h5g_new = h5g.require_group(k)
            conv_aff2hdf_grp(h5g_new, aff_r, aff_kpath_new)
        else :
            print("DATA\t%s" % aff_kpath_new)
            h5g[k] = aff_r.read(aff_kpath_new)


if 3 != len(sys.argv) :
    sys.stderr.write("""Usage:
%s  <src-aff-file>  <dst-hdf-file>
""" % sys.argv[0])
    sys.exit(1)

aff_fname, h5_fname = sys.argv[1:]

aff_r   = aff.Reader(aff_fname)
h5_w    = h5py.File(h5_fname, 'w')
conv_aff2hdf_grp(h5_w, aff_r, '/')
h5_w.flush()
h5_w.close()
