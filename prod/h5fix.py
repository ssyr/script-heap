#!/usr/bin/env python
from __future__ import print_function
import os, sys, re
import traceback
import numpy as np, h5py

def print_usage(prg) :
    sys.stderr.write("""Usage:
%s  
    [--scal=<factor>]  
    [--files-from=<filelist>|-]
    [--verbose]
    <h5file1> <h5kpath1>

""" % prg)

def print_err(s) :
    exc = sys.exc_info()
    sys.stderr.write("ERROR %s: %s\n" % (s, repr(exc[1])))


argv = list(sys.argv)
if len(argv) < 2 :
    print_usage(argv[0])
    sys.exit(1)

scal    = None
flist   = None
VERBOSE     = 0
while 1 < len(argv) and argv[1].startswith('--') :
    if   argv[1].startswith('--scal=') :
        scal_str = re.match('^--scal=(.+)$', argv[1]).groups()[0]
        try :
            scal = eval("0+(%s)" % scal_str)     # some protection from mistakes
        except :
            print_err(scal_str)
            sys.exit(1)
        print("scal = ", scal)

    elif argv[1].startswith('--files-from=') :
        flist = re.match('^--files-from=(.+)$', argv[1]).groups()[0]

    elif '--verbose' == argv[1] :
        VERBOSE = 1

    argv.pop(1)

def fix_file(h5file, h5kpath) :
    h5f = h5py.File(h5file)

    d = h5f[h5kpath].value
    if not None is scal :
        d = scal * d
    
    h5f[h5kpath][:] = d


if 0 != (len(argv) - 1) % 2 :
    sys.stderr.write("unpaired file/kpath in cmdline '%s'", argv[1:])
    sys.exit(1)

while 1 < len(argv) :
    h5file  = argv.pop(1)
    h5kpath = argv.pop(1)
    if 0 < VERBOSE :
        sys.stdout.write("FIX  %s[%s]\n" % (h5file, h5kpath))
    try : 
        fix_file(h5file, h5kpath)
    except :
        print_err("%s[%s]" % (h5file, h5kpath))

if not None is flist :
    if '-' == flist : flist_f = sys.stdin
    else : flist_f = open(flist, 'r')

    for i_l, l in enumerate(flist_f.readlines()) :
        if re.match('^\s*$', l) : 
            continue

        l_s = l.split()
        if 2 != len(l_s) :
            sys.stderr.write("SYN.ERROR IN '%s':%d '%s'" % (flist, i_l, l))
            continue
        h5file, h5kpath = l_s[:2]

        if 0 < VERBOSE :
            sys.stdout.write("FIX  %s[%s]\n" % (h5file, h5kpath))
        try : 
            fix_file(h5file, h5kpath)
        except :
            print_err("%s[%s]" % (h5file, h5kpath))

    if '-' != flist : 
        flist_f.close()
