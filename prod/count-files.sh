#!/bin/bash
[ $# -lt 2 ] && {
  cat >&2 <<EOF
Usage:
$(basename $0)  
    [--clist=<clist1>] ...
    [--flist=<list1>] ...
    <grep-pattern>  <dir1>  <dir2>  ...  --  c1  c2  ...

Search and count files in directories (and optional file lists).
Dir-list is supplied in command line
Cfgkey-list is supplied in command line or in clist option(s).
Options:
  --clist=<list1> 
        filename with list of cfgkeys; repeated opts are concatenated
  --flist=<list1>
        filename with list of files to scan; repeated opts are concatenated
EOF
  exit 1
}
#echo "$*"

dlist=()
flist=()
clist=()
while [[ $1 == --* ]] ; do
  case "$1" in 
  --flist=*)       
    f1=$(echo $1 |sed -e 's?--flist=??') 
    flist=("${flist[@]}"  "$f1")
    dlist=("${dlist[@]}"  "<$f1>")
    ;;
  --clist=*)
    cfglist=$(echo $1 |sed -e 's?--clist=??')
    clist=("${clist[@]}" $(< $cfglist) )
    ;;
  --) break ;;
  *) echo >&2 "*** Unknown option '$1'" ; print_usage ; exit 1 ;;
  esac
  shift
done

patt="$1"
shift

flist_rm=
while [ "$1" != "--" ] && [ $# -gt 0 ] ; do
  d1="$1"
  dlist=("${dlist[@]}"  "$d1")
  f1=$(mktemp)
  ls $d1 >$f1
  echo >&2 -ne "added\t$d1\t$f1\t" ; cat "$f1" |wc -l >&2
  flist=("${flist[@]}"  "$f1")
  flist_rm="$flist_rm  $f1"
  shift
done
nf=${#flist[@]}

shift
clist=("${clist[@]}" $* )

# XXX debug
echo -e "flist\t='" "${flist[@]}" "'" >&2
echo -e "dlist\t='" "${dlist[@]}" "'" >&2
echo -e "clist\t='" "${clist[@]}" "'" >&2
echo -e "patt\t='$patt'"              >&2
#for c in "${clist[@]}" ; do echo $c ; done

#exit 1

#echo >&2 $flist_rm
#echo >&2 "'$patt'  '$*'"
#exit 1 #FIXME debug

for c in "${clist[@]}" ; do
#  for d in $dlist ; do
  for i in $(seq 0 $(( nf - 1)) ) ; do
    d1="${dlist[$i]}"
    f1="${flist[$i]}"
    echo -ne "$c\t$d1\t"
    grep -P '(?<!\w)'$c'(?!\w)' "$f1" |grep "$patt" |wc -l
  done
done

rm -f $flist_rm
