#!/usr/bin/env python
import math
import re
import sys
import numpy
from exceptions import *


stage_keys = [
    'init',
    'gauge_load',
    'gauge_smear',
    'solver_init',
    'frw_prop_source',
    'frw_prop_inv',
    'frw_prop_smear',
    'hadspec',
    'bkw_prop_source',
    'bkw_prop_inv',
    'bb_012links_save',
    'bb_3links_save',
#    '1source==2meas'  
    ]


def parse_log_TIME(file):
    stages = {}
    invert = []
    total = None

    fi = open(file, "r")
    for l in fi:
        if 'TIME("' != l[0:6]: continue

        mo  = re.match('TIME\("([^"]+)","([^"]+)"\) = (\d+\.\d+) secs', l)
        if None == mo:
            sys.stderr.write("bad TIME string: '%s'\n", l)
            continue

        if mo.groups()[0:2] == ('total', 'end'): 
            total = float(mo.groups()[2])
            continue

        key = mo.groups()[0]
        time= float(mo.groups()[2])
        if key in stage_keys:
            if key in stages: stages[key].append(time)
            else: stages[key] = [ time ]
            continue

        mo1 = re.match('\[(\d+)\]dirac_inv\(QDP:(\w+)\(', key)
        if None != mo1:
            inv_cnt = int(mo1.groups()[0])
            field   = mo1.groups()[1]
            if field == 'DiracPropagator': invert.append([time, 12, inv_cnt])
            elif field == 'DiracFermion' : invert.append([time,  1, inv_cnt])
            else: raise ValueError('%s:unknown field in inverter' %(field))
            continue
    return total, stages, invert

def sum_list_float(l):
    sum = 0.0
    for f in l: sum += f
    return sum


def analyze_invert(invert, cnt_eigcg):
    """
    invert: list of eigcg iterations returned by analyze_TIME
    cnt_eigcg: >= number of RHS while constructing espace
    return: cost of one "deflated" CG solution, cost of espace construction
    """
    sum_eigcg, rhs_eigcg = 0, 0
    sum_cg, rhs_cg = 0, 0
    cnt = 0
    for i,inv in enumerate(invert):
        time, rhs, inv_cnt = inv
        if cnt < cnt_eigcg: 
            sum_eigcg += time
            rhs_eigcg += rhs
        else: 
            sum_cg += time
            rhs_cg += rhs
        cnt += rhs

    # "interpolate" CG performance to EigCG to find espace cost
    if (0 < rhs_cg):
        sum_cg /= rhs_cg
        sum_eigcg -= sum_cg * rhs_eigcg
    return sum_cg, sum_eigcg

def print_TIME_multi(list_logs, cnt_eigcg):
    n = len(list_logs)

    total   = []
    stages  = []
    frac    = []
    inv_cg  = []
    eigen   = []
    cnt     = None
    inv_cg  = []
    eigen   = []
    eigen_frac = []
    for l in list_logs:
        t, s, invert = parse_log_TIME(l)
        if None == t:
            sys.stderr.write("skip incomplete log '%s'\n" % l)
            continue

        sum = 0
        this_cnt = []
        this_time = []
        this_frac = []
        for key in stage_keys:
            this_cnt.append(len(s[key]))
            this_time.append(sum_list_float(s[key]))
            this_frac.append(this_time[-1] / t)

        if None == cnt: cnt = this_cnt
        elif cnt != this_cnt:
            sys.stderr.write("skip inconsistent log '%s'\n" % l)
            continue

        total.append(t)
        stages.append(this_time)
        frac.append(this_frac)
        
        # EigCG analysis
        this_inv_cg, this_eigen = analyze_invert(invert, cnt_eigcg)
        inv_cg.append(this_inv_cg)
        eigen.append(this_eigen)
        eigen_frac.append(this_eigen / t)


    total   = numpy.array(total)
    stages  = numpy.array(stages)
    frac    = numpy.array(frac)
    sys.stdout.write('Total = %10.2f(%.2f) over %d log files\n' % \
                     (total.mean(), math.sqrt(total.var()), len(total)))
    sys.stdout.write('%-24s  %44s  %4s  %10s  %11s\n' % \
                     ('Stage', 'Time: min, avg(std), max', 'Cnt', 'Time/Cnt', 'Frac(std)'))
    for i,key in enumerate(stage_keys):
        t = stages[:,i]
        f = frac[:,i]
        sys.stdout.write('%-24s  %10.2f %10.2f(%10.2f) %10.2f  %4d  %10.2f  %7.4f(%.4f)\n' % \
                         (key, 
                         t.min(), t.mean(), math.sqrt(t.var()), t.max(),
                         cnt[i], t.mean() / cnt[i],
                         f.mean(), math.sqrt(f.var())))


    sum         = stages.sum(axis=1)
    frac_sum    = frac.sum(axis=1)
    sys.stdout.write('----\n%-24s  %10.2f %10.2f(%10.2f) %10.2f  %4d  %10.2f  %7.4f(%.4f)\n' % \
                     ('SUM(check)', 
                      sum.min(), sum.mean(), math.sqrt(sum.var()), sum.max(),
                      1, sum.mean(),
                      frac_sum.mean(), math.sqrt(frac_sum.var())))
    
    sys.stdout.write('====\n%-24s  %44s  %7s\n' % \
                     ('', 'Time: min, avg(std), max', 'Frac'))
    eigen   = numpy.array(eigen)
    eigen_frac = numpy.array(eigen_frac)
    sys.stdout.write('%-24s  %10.2f %10.2f(%10.2f) %10.2f  %7.4f\n' % \
                     ('Building espace',
                      eigen.min(), eigen.mean(), math.sqrt(eigen.var()), eigen.max(),
                      eigen_frac.mean()))
    inv_cg  = numpy.array(inv_cg)
    sys.stdout.write('%-24s  %10.2f %10.2f(%10.2f) %10.2f\n' % \
                     ('Time per RHS, deflated',
                      inv_cg.min(), inv_cg.mean(), math.sqrt(inv_cg.var()), inv_cg.max()))


def print_TIME(file, cnt_eigcg):
    total, stages, invert = parse_log_TIME(file)
    if None != total: sys.stdout.write("Total = %10.f\n" % ( total))
    else: sys.stdout.write("Total = <no info>\n")
    
    sys.stdout.write('%-24s %10s %10s %10s %10s\n' % \
                     ('Stage', 'Time', 'Cnt', 'Time/Cnt', 'Frac'))
    sum = 0
    for key in stage_keys:
        cnt = len(stages[key])
        time= sum_list_float(stages[key])
        if None != total: frac_str = '%10.4f' % (time / total)
        else: frac_str = 'N/A'
        sys.stdout.write('%-24s %10.2f %10d %10.2f %10s\n' % \
                            (key, time, cnt, time / cnt, frac_str))
        sum += time
    sys.stdout.write('----\n')
    sys.stdout.write('%-24s %10.2f %10d %10.2f %10.4f\n' % \
                     ('SUM', sum, 1, sum, sum / total))

    if False:
        sys.stdout.write('%-5s %-3s\t%10s %10s\n' % \
                            ('#inv', 'rhs', 'time', 'time/rhs'))
        for i,inv in enumerate(invert):
            time, rhs, inv_cnt = inv
            sys.stdout.write('%-5d %3d\t%10.2f %10.2f\n' % \
                                (inv_cnt, rhs, time, time/rhs))

    inv_cg, eigen = analyze_invert(invert, cnt_eigcg)
    sys.stdout.write('Building espace = %10.2f  (frac=%10.4f)\n' % \
                     (eigen, eigen / total))
    sys.stdout.write('Time per RHS, deflated = %10.2f\n' % \
                     (inv_cg))

        


# MAIN
if (len(sys.argv) < 2):
    sys.stderr.write("Usage:\n%s  <log-stdout>\n" %(sys.argv[0]));
    exit(1)

cnt_eigcg = 34; sys.stderr.write("[W] use cnt_eigcg=%d\n" % cnt_eigcg)

if (len(sys.argv) == 2): print_TIME(sys.argv[1], cnt_eigcg)
else: print_TIME_multi(sys.argv[1:], cnt_eigcg)

