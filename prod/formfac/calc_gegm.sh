#!/bin/bash

#. ff_functions.sh
if [ $# -lt 2 ] ; then
  echo >&2 -ne "Usage:\n$0  <config>  <out-dir>\n"
  exit 1
fi

config=$1
. ${config} || exit 1
out_dir=$2
op='tensor1'

shift 2
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done


Nblock=$(TSVstat.pl -H ${out_dir}/${op}/U-pltx|gawk '{print $1}')
if [ "$Nblock" -lt 2 ] ; then
  echo 2>&1 "data has not enough blocks"
  exit 1
fi
q2xN_list=$(mktemp -p . q2l_XXXXXX)
etab -r '[0-'$((Nblock-1))'][][*][]' -e '$[0][0][%1][0]' <"${out_dir}/q2_list" >"$q2xN_list"

# compute GE/GM
for flav in $(echo $FLAV_LIST) ; do 
  TSVpaste.pl 3 "$q2xN_list" "${out_dir}/${op}/${flav}-pltx" \
    |etab -r '[]' -e '$[1]-$[2]*$[0]/4./'$MN'/'$MN' ; $[1] + $[2]'  \
    |tee "${out_dir}/${op}/${flav}-pltx-GEGM" \
    |estimate.pl --avg --var-jk  >"${out_dir}/${op}/${flav}-pltx-GEGM-av"
  etab -r '[]' -e '$[0]/$[1]' <"${out_dir}/${op}/${flav}-pltx-GEGM" \
    |estimate.pl --avg --var-jk >"${out_dir}/${op}/${flav}-pltx-GEdGM-av"
  TSVpaste.pl 3 "${out_dir}/q2_list" "${out_dir}/${op}/${flav}-pltx-GEGM-av" >"${out_dir}/${op}/${flav}-pltx-GEGM-av-q2"
  TSVpaste.pl 3 "${out_dir}/q2_list" "${out_dir}/${op}/${flav}-pltx-GEdGM-av" >"${out_dir}/${op}/${flav}-pltx-GEdGM-av-q2"
done

rm -f "$q2xN_list"
