#!/bin/bash

out_dir='.'
if [ $# -ge 1 ] ; then
  out_dir=$1
  shift
fi

mkdir -p $out_dir

cat >$out_dir/mom1 <<EOF
# op1_t op1_x
 -1   0   0	 -1   0   0
# op1_t
  0   0   0	  0   0   0
EOF


cat >$out_dir/mom2 <<EOF
# op1_t op1_x op1_y
 -2   0   0	 -1   0   0
EOF


cat >$out_dir/mom3 <<EOF
# op1_t op1_z
  0   0  -1	  0   0   0
# op1_t op1_x op1_y
  0  -1   0	  0   0   0
# op1_t op1_x op1_y
 -1   0   0	  0   0   0
# op1_t op1_x op1_y
  0   0   0	 -1   0   0
# op1_t op1_x op1_y
  1   0   0	  0   0   0
# op1_t op1_x op1_y
  0   1   0	  0   0   0
# op1_t op1_z
  0   0   1	  0   0   0
EOF


cat >$out_dir/mom4 <<EOF
# op1_t op1_x op1_y op1_z
 -1   0  -1	 -1   0   0
# op1_t op1_x op1_y
 -1  -1   0	 -1   0   0
# op1_t op1_x op1_y
 -1   1   0	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   0   1	 -1   0   0
EOF


cat >$out_dir/mom5 <<EOF
# op1_t op1_x op1_y op1_z
 -2   0  -1	 -1   0   0
# op1_t op1_x op1_y
 -2  -1   0	 -1   0   0
# op1_t op1_x op1_y
 -2   1   0	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2   0   1	 -1   0   0
EOF


cat >$out_dir/mom6 <<EOF
# op1_t op1_x op1_y op1_z
  0  -1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   0  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  1   0  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  0   1  -1	  0   0   0
# op1_t op1_x op1_y
 -1  -1   0	  0   0   0
# op1_t op1_x op1_y
  1  -1   0	  0   0   0
# op1_t op1_x op1_y
 -1   1   0	  0   0   0
# op1_t op1_x op1_y
  1   1   0	  0   0   0
# op1_t op1_x op1_y op1_z
  0  -1   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   0   1	  0   0   0
# op1_t op1_x op1_y op1_z
  1   0   1	  0   0   0
# op1_t op1_x op1_y op1_z
  0   1   1	  0   0   0
EOF


cat >$out_dir/mom7 <<EOF
# op1_t op1_x op1_y op1_z
 -1  -1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1  -1   1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   1   1	 -1   0   0
EOF


cat >$out_dir/mom8 <<EOF
# op1_t op1_x op1_y op1_z
  0   0  -1	 -1   0   0
# op1_t op1_x op1_y
  0  -1   0	 -1   0   0
# op1_t op1_x op1_y
  0   1   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   0   1	 -1   0   0
EOF


cat >$out_dir/mom9 <<EOF
# op1_t op1_x op1_y op1_z
 -2  -1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2   1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2  -1   1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2   1   1	 -1   0   0
EOF


cat >$out_dir/mom10 <<EOF
# op1_t op1_x op1_y op1_z
 -1  -1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  1   1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1  -1   1	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -1   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   1   1	  0   0   0
# op1_t op1_x op1_y op1_z
  1   1   1	  0   0   0
EOF


cat >$out_dir/mom11 <<EOF
# op1_t op1_x op1_y op1_z
  0  -1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -1   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1   1	 -1   0   0
EOF


cat >$out_dir/mom12 <<EOF
# op1_t op1_x op1_y
 -3   0   0	 -1   0   0
EOF


cat >$out_dir/mom13 <<EOF
# op1_t op1_z
  0   0  -2	  0   0   0
# op1_t op1_x op1_y
  0  -2   0	  0   0   0
# op1_t op1_x op1_y
 -2   0   0	  0   0   0
# op1_t op1_x op1_y
  2   0   0	  0   0   0
# op1_t op1_x op1_y
  0   2   0	  0   0   0
# op1_t op1_z
  0   0   2	  0   0   0
EOF


cat >$out_dir/mom14 <<EOF
# op1_t op1_x op1_y op1_z
 -1   0  -2	 -1   0   0
# op1_t op1_x op1_y
 -1  -2   0	 -1   0   0
# op1_t op1_x op1_y
 -1   2   0	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   0   2	 -1   0   0
EOF


cat >$out_dir/mom15 <<EOF
# op1_t op1_y
  1   0   0	 -1   0   0
EOF


cat >$out_dir/mom16 <<EOF
# op1_t op1_x op1_y op1_z
 -3   0  -1	 -1   0   0
# op1_t op1_x op1_y
 -3  -1   0	 -1   0   0
# op1_t op1_x op1_y
 -3   1   0	 -1   0   0
# op1_t op1_x op1_y op1_z
 -3   0   1	 -1   0   0
EOF


cat >$out_dir/mom17 <<EOF
# op1_t op1_x op1_y op1_z
 -2   0  -2	 -1   0   0
# op1_t op1_x op1_y
 -2  -2   0	 -1   0   0
# op1_t op1_x op1_y
 -2   2   0	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2   0   2	 -1   0   0
EOF


cat >$out_dir/mom18 <<EOF
# op1_t op1_x op1_y op1_z
  0  -1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   0  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  1   0  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  0   1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  0  -2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   0  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  2   0  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  0   2  -1	  0   0   0
# op1_t op1_x op1_y
 -1  -2   0	  0   0   0
# op1_t op1_x op1_y
  1  -2   0	  0   0   0
# op1_t op1_x op1_y
 -2  -1   0	  0   0   0
# op1_t op1_x op1_y
  2  -1   0	  0   0   0
# op1_t op1_x op1_y
 -2   1   0	  0   0   0
# op1_t op1_x op1_y
  2   1   0	  0   0   0
# op1_t op1_x op1_y
 -1   2   0	  0   0   0
# op1_t op1_x op1_y
  1   2   0	  0   0   0
# op1_t op1_x op1_y op1_z
  0  -2   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   0   1	  0   0   0
# op1_t op1_x op1_y op1_z
  2   0   1	  0   0   0
# op1_t op1_x op1_y op1_z
  0   2   1	  0   0   0
# op1_t op1_x op1_y op1_z
  0  -1   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   0   2	  0   0   0
# op1_t op1_x op1_y op1_z
  1   0   2	  0   0   0
# op1_t op1_x op1_y op1_z
  0   1   2	  0   0   0
EOF


cat >$out_dir/mom19 <<EOF
# op1_t op1_x op1_y op1_z
 -1  -1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1  -2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1  -2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1  -1   2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   1   2	 -1   0   0
EOF


cat >$out_dir/mom20 <<EOF
# op1_t op1_x op1_y op1_z
  0   0  -2	 -1   0   0
# op1_t op1_x op1_y
  0  -2   0	 -1   0   0
# op1_t op1_x op1_y
  0   2   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   0   2	 -1   0   0
EOF


cat >$out_dir/mom21 <<EOF
# op1_t op1_x op1_y op1_z
  1   0  -1	 -1   0   0
# op1_t op1_x op1_y
  1  -1   0	 -1   0   0
# op1_t op1_x op1_y
  1   1   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   0   1	 -1   0   0
EOF


cat >$out_dir/mom22 <<EOF
# op1_t op1_x op1_y op1_z
 -2  -1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2   1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2  -2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2   2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2  -2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2   2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2  -1   2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -2   1   2	 -1   0   0
EOF


cat >$out_dir/mom23 <<EOF
# op1_t op1_x op1_y op1_z
 -1  -1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  1   1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1  -2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  2   1  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  1   2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1  -2   1	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -2   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -1   1	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -1   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   1   1	  0   0   0
# op1_t op1_x op1_y op1_z
  2   1   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   2   1	  0   0   0
# op1_t op1_x op1_y op1_z
  1   2   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1  -1   2	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -1   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   1   2	  0   0   0
# op1_t op1_x op1_y op1_z
  1   1   2	  0   0   0
EOF


cat >$out_dir/mom24 <<EOF
# op1_t op1_x op1_y op1_z
  0  -1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -1   2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1   2	 -1   0   0
EOF


cat >$out_dir/mom25 <<EOF
# op1_t op1_x op1_y op1_z
  0  -1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -1   2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1   2	 -1   0   0
EOF


cat >$out_dir/mom26 <<EOF
# op1_t op1_x op1_y op1_z
  1  -1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1  -1   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   1   1	 -1   0   0
EOF


cat >$out_dir/mom27 <<EOF
# op1_t op1_x op1_y op1_z
  0  -2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   0  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  2   0  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  0   2  -2	  0   0   0
# op1_t op1_x op1_y
 -2  -2   0	  0   0   0
# op1_t op1_x op1_y
  2  -2   0	  0   0   0
# op1_t op1_x op1_y
 -2   2   0	  0   0   0
# op1_t op1_x op1_y
  2   2   0	  0   0   0
# op1_t op1_x op1_y op1_z
  0  -2   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   0   2	  0   0   0
# op1_t op1_x op1_y op1_z
  2   0   2	  0   0   0
# op1_t op1_x op1_y op1_z
  0   2   2	  0   0   0
EOF


cat >$out_dir/mom28 <<EOF
# op1_t op1_x op1_y op1_z
 -1  -2  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   2  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1  -2   2	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   2   2	 -1   0   0
EOF


cat >$out_dir/mom29 <<EOF
# op1_t op1_x op1_y op1_z
  1   0  -2	 -1   0   0
# op1_t op1_x op1_y
  1  -2   0	 -1   0   0
# op1_t op1_x op1_y
  1   2   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   0   2	 -1   0   0
EOF


cat >$out_dir/mom30 <<EOF
# op1_t op1_z
  0   0  -3	  0   0   0
# op1_t op1_x op1_y op1_z
 -1  -2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  2   1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  1   2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  2   2  -1	  0   0   0
# op1_t op1_x op1_y
  0  -3   0	  0   0   0
# op1_t op1_x op1_y
 -3   0   0	  0   0   0
# op1_t op1_x op1_y
  3   0   0	  0   0   0
# op1_t op1_x op1_y
  0   3   0	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -2   1	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -2   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   2   1	  0   0   0
# op1_t op1_x op1_y op1_z
  2   2   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1  -2   2	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -2   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -1   2	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -1   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   1   2	  0   0   0
# op1_t op1_x op1_y op1_z
  2   1   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   2   2	  0   0   0
# op1_t op1_x op1_y op1_z
  1   2   2	  0   0   0
# op1_t op1_z
  0   0   3	  0   0   0
EOF


cat >$out_dir/mom31 <<EOF
# op1_t op1_z
  0   0  -3	  0   0   0
# op1_t op1_x op1_y op1_z
 -1  -2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  2   1  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
  1   2  -2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   2  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  2   2  -1	  0   0   0
# op1_t op1_x op1_y
  0  -3   0	  0   0   0
# op1_t op1_x op1_y
 -3   0   0	  0   0   0
# op1_t op1_x op1_y
  3   0   0	  0   0   0
# op1_t op1_x op1_y
  0   3   0	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -2   1	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -2   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   2   1	  0   0   0
# op1_t op1_x op1_y op1_z
  2   2   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -1  -2   2	  0   0   0
# op1_t op1_x op1_y op1_z
  1  -2   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2  -1   2	  0   0   0
# op1_t op1_x op1_y op1_z
  2  -1   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -2   1   2	  0   0   0
# op1_t op1_x op1_y op1_z
  2   1   2	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   2   2	  0   0   0
# op1_t op1_x op1_y op1_z
  1   2   2	  0   0   0
# op1_t op1_z
  0   0   3	  0   0   0
EOF


cat >$out_dir/mom32 <<EOF
# op1_t op1_x op1_y op1_z
 -1   0  -3	 -1   0   0
# op1_t op1_x op1_y
 -1  -3   0	 -1   0   0
# op1_t op1_x op1_y
 -1   3   0	 -1   0   0
# op1_t op1_x op1_y op1_z
 -1   0   3	 -1   0   0
EOF


cat >$out_dir/mom33 <<EOF
# op1_t op1_x op1_y op1_z
  0  -2  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   2  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -2   2	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   2   2	 -1   0   0
EOF


cat >$out_dir/mom34 <<EOF
# op1_t op1_x op1_y op1_z
  1  -1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1  -2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1  -2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1  -1   2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   1   2	 -1   0   0
EOF


cat >$out_dir/mom35 <<EOF
# op1_t op1_x op1_y op1_z
  1  -1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1  -2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1  -2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  1  -1   2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   1   2	 -1   0   0
EOF


cat >$out_dir/mom36 <<EOF
# op1_t op1_x op1_y
  2   0   0	 -1   0   0
EOF


cat >$out_dir/mom37 <<EOF
# op1_t op1_x op1_y op1_z
  0  -1  -3	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   0  -3	  0   0   0
# op1_t op1_x op1_y op1_z
  1   0  -3	  0   0   0
# op1_t op1_x op1_y op1_z
  0   1  -3	  0   0   0
# op1_t op1_x op1_y op1_z
  0  -3  -1	  0   0   0
# op1_t op1_x op1_y op1_z
 -3   0  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  3   0  -1	  0   0   0
# op1_t op1_x op1_y op1_z
  0   3  -1	  0   0   0
# op1_t op1_x op1_y
 -1  -3   0	  0   0   0
# op1_t op1_x op1_y
  1  -3   0	  0   0   0
# op1_t op1_x op1_y
 -3  -1   0	  0   0   0
# op1_t op1_x op1_y
  3  -1   0	  0   0   0
# op1_t op1_x op1_y
 -3   1   0	  0   0   0
# op1_t op1_x op1_y
  3   1   0	  0   0   0
# op1_t op1_x op1_y
 -1   3   0	  0   0   0
# op1_t op1_x op1_y
  1   3   0	  0   0   0
# op1_t op1_x op1_y op1_z
  0  -3   1	  0   0   0
# op1_t op1_x op1_y op1_z
 -3   0   1	  0   0   0
# op1_t op1_x op1_y op1_z
  3   0   1	  0   0   0
# op1_t op1_x op1_y op1_z
  0   3   1	  0   0   0
# op1_t op1_x op1_y op1_z
  0  -1   3	  0   0   0
# op1_t op1_x op1_y op1_z
 -1   0   3	  0   0   0
# op1_t op1_x op1_y op1_z
  1   0   3	  0   0   0
# op1_t op1_x op1_y op1_z
  0   1   3	  0   0   0
EOF


cat >$out_dir/mom38 <<EOF
# op1_t op1_x op1_y op1_z
  0   0  -3	 -1   0   0
# op1_t op1_x op1_y
  0  -3   0	 -1   0   0
# op1_t op1_x op1_y
  0   3   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   0   3	 -1   0   0
EOF


cat >$out_dir/mom39 <<EOF
# op1_t op1_x op1_y op1_z
  2   0  -1	 -1   0   0
# op1_t op1_x op1_y
  2  -1   0	 -1   0   0
# op1_t op1_x op1_y
  2   1   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  2   0   1	 -1   0   0
EOF


cat >$out_dir/mom40 <<EOF
# op1_t op1_x op1_y op1_z
  0  -1  -3	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1  -3	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -3  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   3  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -3   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   3   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -1   3	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1   3	 -1   0   0
EOF


cat >$out_dir/mom41 <<EOF
# op1_t op1_x op1_y op1_z
  0  -1  -3	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1  -3	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -3  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   3  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -3   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   3   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  0  -1   3	 -1   0   0
# op1_t op1_x op1_y op1_z
  0   1   3	 -1   0   0
EOF


cat >$out_dir/mom42 <<EOF
# op1_t op1_x op1_y op1_z
  2  -1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  2   1  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  2  -1   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  2   1   1	 -1   0   0
EOF


cat >$out_dir/mom43 <<EOF
# op1_t op1_x op1_y op1_z
  1  -2  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   2  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1  -2   2	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   2   2	 -1   0   0
EOF


cat >$out_dir/mom44 <<EOF
# op1_t op1_x op1_y op1_z
  1   0  -3	 -1   0   0
# op1_t op1_x op1_y
  1  -3   0	 -1   0   0
# op1_t op1_x op1_y
  1   3   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  1   0   3	 -1   0   0
EOF


cat >$out_dir/mom45 <<EOF
# op1_t op1_x op1_y op1_z
  2   0  -2	 -1   0   0
# op1_t op1_x op1_y
  2  -2   0	 -1   0   0
# op1_t op1_x op1_y
  2   2   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  2   0   2	 -1   0   0
EOF


cat >$out_dir/mom46 <<EOF
# op1_t op1_x op1_y op1_z
  2  -1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  2   1  -2	 -1   0   0
# op1_t op1_x op1_y op1_z
  2  -2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  2   2  -1	 -1   0   0
# op1_t op1_x op1_y op1_z
  2  -2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  2   2   1	 -1   0   0
# op1_t op1_x op1_y op1_z
  2  -1   2	 -1   0   0
# op1_t op1_x op1_y op1_z
  2   1   2	 -1   0   0
EOF


cat >$out_dir/mom47 <<EOF
# op1_t op1_x op1_y
  3   0   0	 -1   0   0
EOF


cat >$out_dir/mom48 <<EOF
# op1_t op1_x op1_y op1_z
  3   0  -1	 -1   0   0
# op1_t op1_x op1_y
  3  -1   0	 -1   0   0
# op1_t op1_x op1_y
  3   1   0	 -1   0   0
# op1_t op1_x op1_y op1_z
  3   0   1	 -1   0   0
EOF


