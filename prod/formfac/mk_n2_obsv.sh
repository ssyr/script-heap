#!/bin/bash

export PYTHONPATH="$HOME/usr/lib64/python2.5/site-packages:$HOME/src/python-work-current:$PYTHONPATH"
FIT_NPOLE="python $HOME/src/python-work-current/fit_proc/fit_npole.py"

if [ $# -lt 2 ] ; then 
  echo -e "Usage:\n$(basename $0)  <config>  <out_dir>  [<PARAM1>=<val1> ...]"
  exit 1
fi
config=$1
out_dir=$2

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

if [ ! -d ${out_dir}/tensor2s2 ] || [ ! -d ${out_dir}/pstensor1 ] ; then
  echo >&2 "operators not found: tensor2s2 and pstensor1 are required"
  exit 1
fi

for op in {,ps}tensor2s2 ; do
for flav in U D U-D ; do
    $FIT_NPOLE --fit-func=dipole ${out_dir}/q2_list ${out_dir}/${op}/${flav}-pltx \
      |pytsv -pe 'show(_[:,:,[0],:])' >${out_dir}/${op}/${flav}-pltx-t0 ||exit 1
  done
  # U+D contrib is small; should be fitted with linear instead of dipole
  $FIT_NPOLE --fit-func=linear ${out_dir}/q2_list ${out_dir}/${op}/U+D-pltx \
    |pytsv -pe 'show(_[:,:,[0],:])' >${out_dir}/${op}/U+D-pltx-t0 ||exit 1
done

for flav in U D U-D U+D ; do
  data="${out_dir}/tensor2s2/${flav}-pltx-t0 ${out_dir}/pstensor2s2/${flav}-pltx-t0 ${out_dir}/pstensor1/${flav}-pltx"
  pytsv -e '
X=a[:,0:1,0:1,0:1]; print "x['$flav']\t", pretty_val_err(avg(X),var_jk(X))
DX=b[:,0:1,0:1,0:1]; print "dx['$flav']\t", pretty_val_err(avg(DX),var_jk(DX))
J=(X+a[:,0:1,0:1,1:2])/2; print "J['$flav']\t", pretty_val_err(avg(J),var_jk(J))
S=c[:,0:1,0:1,0:1]/2;  print "S['$flav']\t", pretty_val_err(avg(S),var_jk(S))
L=J-S;  print "L['$flav']\t", pretty_val_err(avg(L),var_jk(L))
' $data || exit 1
done
