#!/bin/bash


# <data_dir>
function count_bb() {
  [ $# -eq 1 ] || { echo "$FUNCNAME: bad parameters" ; exit 1 ; }
  local patt='s?^bb\.?? ; s?\.x.*$?? '
  ls "$1" |sed -e "$patt" |sort |uniq -c
}
# <data_dir>
function count_bb1() {
  [ $# -eq 1 ] || { echo "$FUNCNAME: bad parameters" ; exit 1 ; }
  for n in $(ls "$data_dir") ; do
    echo -e $(ls "$data_dir/$n" |grep $n |wc -l) "\t$n"
  done
}
# <data_dir>
function count_hadspec() {
  [ $# -eq 1 ] || { echo "$FUNCNAME: bad parameters" ; exit 1 ; }
  local patt='s?^hadspec\.?? ; s?\.x.*$?? '
  ls "$1" |sed -e "$patt" |sort |uniq -c
}
# <data_dir>
function count_op() {
  [ $# -eq 1 ] || { echo "$FUNCNAME: bad parameters" ; exit 1 ; }
  for n in $(ls "$data_dir") ; do
    echo -e $(ls "$data_dir/$n/split" |grep $n |wc -l) "\t$n"
  done
}

# <nfiles>
function print_summary() {
  [ $# -eq 1 ] || { echo "$FUNCNAME: bad parameters" ; exit 1 ; }
  local nfiles=$1
  gawk 'BEGIN {num_full=0 ; num_part=0}
        $1 == '$nfiles' {num_full+=1} 
        $1 != '$nfiles' {num_part+=1} 
        END { 
          print "full(=='$nfiles')\t = " num_full
          print "part(!='$nfiles')\t = " num_part
          print "total = " num_full+num_part } '
}
# <nfiles>
function print_list_full() {
  [ $# -eq 1 ] || { echo "$FUNCNAME: bad parameters" ; exit 1 ; }
  local nfiles=$1
  gawk '$1 =='$nfiles' {print $2 }'
}
# <nfiles>
function print_list_part() {
  [ $# -eq 1 ] || { echo "$FUNCNAME: bad parameters" ; exit 1 ; }
  local nfiles=$1
  gawk '$1 !='$nfiles' {print $2 }'
}
# <nfiles>
function print_list_part_v() {
  [ $# -eq 1 ] || { echo "$FUNCNAME: bad parameters" ; exit 1 ; }
  local nfiles=$1
  gawk '$1 !='$nfiles' {print $0 }'
}


# MAIN BLOCK

if [ $# -lt 3 ] ; then
  echo >&2 -e "Count files in data directories; script must be adjusted for particular naming schemes.
Usage:
$(basename $0)  [--full]  {bb|bb1|hadspec|op}  <dir>  <expected #files>
    --summary print summary: numbers of total, full part (default)
    --full    print a list of cfg names with full sets of files
    --part    print a list of cfg names with partial sets of files
    --part-v  print a list of cfg names with partial sets of files 
              with file counts
Data directories:
  * bb:         <dir>/bb.<cfg>.*.aff
  * bb1:        <dir>/<cfg>/bb.<cfg>.*.aff
  * hadspec:    <dir>/hadspec.<cfg>.*.aff
  * op:         <dir>/<cfg>/split/op.<cfg>.*.aff
"
  exit 1
fi

do_func=print_summary
while [ $# -gt 3 ] ; do
  if   [[ "$1" == '--full' ]] ; then do_func=print_list_full ; shift ; continue
  elif [[ "$1" == '--part' ]] ; then do_func=print_list_part ; shift ; continue
  elif [[ "$1" == '--part-v' ]] ; then do_func=print_list_part_v ; shift ; continue
  elif [[ "$1" == '--summary' ]] ; then do_func=print_summary ; shift ; continue
  else echo >&2 "bad option '$1'" ; exit 1
  fi
done

what=$1
data_dir=$2
nfiles=$3
if   [[ "$what" == bb ]]      ; then count_func=count_bb
elif [[ "$what" == bb1 ]]     ; then count_func=count_bb1
elif [[ "$what" == hadspec ]] ; then count_func=count_hadspec
elif [[ "$what" == op ]]      ; then count_func=count_op
else echo >&2 "unknown data '$what'" ; exit 1
fi

$count_func $data_dir | $do_func $nfiles
