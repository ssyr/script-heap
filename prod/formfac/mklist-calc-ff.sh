#!/bin/bash
export PATH="$(pwd)/../bin:$PATH"

geom="24,24,24,24"
#data="data"
ens_list='run0'
dt=10

FLAV_LIST='U D'
HAD_LIST='proton_3 proton_negpar_3'
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'



mkdir -p list
list_dir=$(pwd)/list
pushd ..
for op in $(echo $OP_LIST) ; do 
  for flav in $(echo $FLAV_LIST) ; do
    for ens in $(echo $ens_list) ; do
      for had in $(echo $HAD_LIST) ; do 
        mklist_calc_ff.pl \
          data/${ens}/hadspec.SS.nucleon.aff \
          data/${ens}/op.${had}.${op}.${flav}.dt${dt}.aff
      done
    done \
    |sortlist_calc_ff.pl \
        -G $geom --cfg-id-compare-stringwise \
    >${list_dir}/${op}.${flav}
  done
done
popd
