#!/bin/bash
# default settings
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'
PLOT_LIST=''
FLAV_LIST="U D"

if [ $# -lt 3 ] ; then 
  echo -e "Usage:\n$(basename $0)  <config>  <out_dir>  <out-file-prefix>  [<PARAM1>=<val1> ...]"
  exit 1
fi
config=$1
out_dir=$2
out_file_prefix=$3

#if [ "$out_file_prefix" != "" ] ; 
#  out_file_prefix="${out_file_prefix}."
#fi

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

# command line settings override config and default
shift 3
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done

. ff_functions.sh

#usage: <op> <flav> <use> [<dir> <title>] ...
function gen_plot_list() {
  [ $# -gt 3 ] && [ $((($#-3)%2)) -eq 0 ] || exit 1
  local op=$1
  local flav=$2
  local use=$3
  shift 3
  local plot_list=''
  while [ $# -gt 0 ] ; do
    plot_list="$plot_list  $1/${op}/${flav}-pltx-av-q2 $use $2"
    shift 2
  done
  echo "$plot_list"
}

#usage: <out-prefix> <flav> [<dir> <title>] ...
function gen_plot_ff-tensor1() {
  local op='tensor1'
  [ $# -ge 2 ] || exit 1
  local out_prefix=$1
  local flav=$2
  shift 2
  plot_ff "${out_prefix}F1.${flav}.ps" \
          "$ENS_TITLE, F@_1^{${flav}}(Q^2)"  'Q^2 [lat]'  "F@_1^{${flav}}" \
          $(gen_plot_list $op $flav '2:4' "$@") 
  plot_ff "${out_prefix}F2.${flav}.ps" \
          "$ENS_TITLE, F@_2^{${flav}}(Q^2)"  'Q^2 [lat]'  "F@_2^{${flav}}" \
          $(gen_plot_list $op $flav '3:5' "$@")
}
# usage: <out-prefix> <flav> [<dir> <title>] ...  
function gen_plot_ff-pstensor1() {
  local op='pstensor1'
  [ $# -ge 2 ] || exit 1
  local out_prefix=$1
  local flav=$2
  shift 2
  plot_ff "${out_prefix}GA.${flav}.ps" \
          "$ENS_TITLE, G@_A^{${flav}}(Q^2)"  'Q^2 [lat]'  "G@_A^{${flav}}" \
          $(gen_plot_list $op $flav '2:4' "$@") || return 1
  plot_ff "${out_prefix}GP.${flav}.ps" \
          "$ENS_TITLE, G@_P^{${flav}}(Q^2)"  'Q^2 [lat]'  "G@_P^{${flav}}" \
          $(gen_plot_list $op $flav '3:5' "$@") || return 1
}
# usage: <out-prefix> <flav> [<dir> <title>] ...
function gen_plot_ff-tensor2s2() {
  local op='tensor2s2'
  [ $# -ge 2 ] || exit 1
  local out_prefix=$1
  local flav=$2
  shift 2
  plot_ff "${out_prefix}A20.${flav}.ps" \
          "$ENS_TITLE, A@_{20}^{${flav}}(Q^2)"  'Q^2 [lat]'  "A@_{20}^{${flav}}" \
          $(gen_plot_list $op $flav '2:5' "$@") || return 1
  plot_ff "${out_prefix}B20.${flav}.ps" \
          "$ENS_TITLE, B@_{20}^{${flav}}(Q^2)"  'Q^2 [lat]'  "B@_{20}^{${flav}}" \
          $(gen_plot_list $op $flav '3:6' "$@") || return 1
  plot_ff "${out_prefix}C20.${flav}.ps" \
          "$ENS_TITLE, C@_{20}^{${flav}}(Q^2)"  'Q^2 [lat]'  "C@_{20}^{${flav}}" \
          $(gen_plot_list $op $flav '4:7' "$@") || return 1
}
# usage: <out-prefix> <flav> [<dir> <title>] ...
function gen_plot_ff-pstensor2s2() {
  local op='pstensor2s2'
  [ $# -ge 2 ] || exit 1
  local out_prefix=$1
  local flav=$2
  shift 2
  plot_ff "${out_prefix}TA20.${flav}.ps" \
          "$ENS_TITLE, \\~A@_{20}^{${flav}}(Q^2)"  'Q^2 [lat]'  "\\~A@_{20}^{${flav}}" \
          $(gen_plot_list $op $flav '2:4' "$@") || return 1
  plot_ff "${out_prefix}TB20.${flav}.ps" \
          "$ENS_TITLE, \\~B@_{20}^{${flav}}(Q^2)"  'Q^2 [lat]'  "\\~B@_{20}^{${flav}}" \
          $(gen_plot_list $op $flav '3:5' "$@") || return 1
}
# usage: <out-prefix> <flav> [<dir> <title>] ...
function gen_plot_ff-tensor3s3() {
  local op='tensor3s3'
  [ $# -ge 2 ] || exit 1
  local out_prefix=$1
  local flav=$2
  shift 2
  plot_ff "${out_prefix}A30.${flav}.ps" \
          "$ENS_TITLE, A@_{30}^{${flav}}(Q^2)"  'Q^2 [lat]'  "A@_{30}^{${flav}}" \
          $(gen_plot_list $op $flav '2:6' "$@") || return 1
  plot_ff "${out_prefix}A32.${flav}.ps" \
          "$ENS_TITLE, A@_{32}^{${flav}}(Q^2)"  'Q^2 [lat]'  "A@_{32}^{${flav}}" \
          $(gen_plot_list $op $flav '3:7' "$@") || return 1
  plot_ff "${out_prefix}B30.${flav}.ps" \
          "$ENS_TITLE, B@_{30}^{${flav}}(Q^2)"  'Q^2 [lat]'  "B@_{30}^{${flav}}" \
          $(gen_plot_list $op $flav '4:8' "$@") || return 1
  plot_ff "${out_prefix}B32.${flav}.ps" \
          "$ENS_TITLE, B@_{32}^{${flav}}(Q^2)"  'Q^2 [lat]'  "B@_{32}^{${flav}}" \
          $(gen_plot_list $op $flav '5:9' "$@") || return 1
}
# usage: <out-prefix> <flav> [<dir> <title>] ...
function gen_plot_ff-pstensor3s3() {
  local op='pstensor3s3'
  [ $# -ge 2 ] || exit 1
  local out_prefix=$1
  local flav=$2
  shift 2
  plot_ff "${out_prefix}TA30.${flav}.ps" \
          "$ENS_TITLE, \\~A@_{30}^{${flav}}(Q^2)"  'Q^2 [lat]'  "\\~A@_{30}^{${flav}}" \
          $(gen_plot_list $op $flav '2:6' "$@") || return 1
  plot_ff "${out_prefix}TA32.${flav}.ps" \
          "$ENS_TITLE, \\~A@_{32}^{${flav}}(Q^2)"  'Q^2 [lat]'  "\\~A@_{32}^{${flav}}" \
          $(gen_plot_list $op $flav '3:7' "$@") || return 1
  plot_ff "${out_prefix}TB30.${flav}.ps" \
          "$ENS_TITLE, \\~B@_{30}^{${flav}}(Q^2)"  'Q^2 [lat]'  "\\~B@_{30}^{${flav}}" \
          $(gen_plot_list $op $flav '4:8' "$@") || return 1
  plot_ff "${out_prefix}TB32.${flav}.ps" \
          "$ENS_TITLE, \\~B@_{32}^{${flav}}(Q^2)"  'Q^2 [lat]'  "\\~B@_{32}^{${flav}}" \
          $(gen_plot_list $op $flav '5:9' "$@") || return 1
}


for op in $(echo $OP_LIST) ; do
  for flav in $(echo $FLAV_LIST) ; do 
    echo -e "$op\t$flav"
    mkdir -p "${out_dir}/${op}" || exit 1
    gen_plot_ff-$op "${out_dir}/${op}/${out_file_prefix}" \
      $flav $PLOT_LIST || exit 1
  done
done
