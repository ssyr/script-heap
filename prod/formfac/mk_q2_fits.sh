#!/bin/bash

# default settings
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'

FLAV_LIST='
U
D
U-D
U+D
P
N'

if [ $# -lt 2 ] ; then 
  echo -e "Usage:\n$(basename $0)  <config>  <out_dir>  [<PARAM1>=<val1> ...]"
  exit 1
fi
config=$1
out_dir=$2

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

# command line settings override config and default
shift 2
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done

export PYTHONPATH="$HOME/usr/lib64/python2.5/site-packages:$HOME/src/python-work-current:$PYTHONPATH"
FIT_NPOLE="python $HOME/src/python-work-current/fit_proc/fit_npole.py"

for op in $(echo $OP_LIST) ; do
  for flav in $(echo $FLAV_LIST) ; do
    $FIT_NPOLE --fit-func=dipole --with-chi2=reduced \
        ${out_dir}/q2_list ${out_dir}/${op}/${flav}-pltx \
      >${out_dir}/${op}/${flav}-pltx-npole \
      ||exit 1
  done
done

#. ff_functions.sh
#
#flav='U-D'
#out_file=${out_dir}/${ENS_NAME}.${SET_NAME}.dt${SRC_SNK_DT}.mn${MN}.obsv
#mkdir -p $(dirname $out_file)
#tmpfile=$(mktemp ${out_dir}/tmp.XXXXXX)
#TSVpaste.pl 3 ${out_dir}/tensor1/${flav} ${out_dir}/pstensor1/${flav} \
#  |plateau_average $PLATEAU_3PT \
#  |etab -r '[][]' -e '$[0][0] ; $[0][2] ; $[0][2]/$[0][0] ; 12*(sqrt($[0][0]/$['$R2_Q2_INDEX'][0])-1)/'$R2_DQ2'' \
#  |estimate.pl --avg --var-jk >$tmpfile
#echo "# g_V   g_A   g_A/g_V   <r^2_v1>^{${flav}}" >$out_file
#etab -r '[0-3][0-1]' -e '$[0][4*%0+%1]' <$tmpfile |sed -e 's?^?# ?' >>$out_file
#cat $tmpfile >>$out_file
#rm $tmpfile
