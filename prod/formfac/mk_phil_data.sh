#!/bin/bash

OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'
FLAV_LIST="U-D U+D"

if [ $# -ne 3 ] ; then
  echo >&2 -ne "Usage:\n$0  <config>  <in-dir>  <out-dir>\n"
  exit 1
fi
config="$1"
in_dir="$2"
out_dir="$3"

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

# command line settings override config and default
shift 3
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done

# usage: <flav>
function flavor_phil() {
  [ $# -eq 1 ] || exit 1
  case "$1" in 
    U-D)
      echo 'umd' ;;
    U+D)
      echo 'upd' ;;
    U)
      echo 'u' ;;
    D)
      echo 'd' ;;
    *)
      echo >&2 "unknown flavor '$1'" ;;
  esac
}

# usage: <op>
function op_phil() {
  [ $# -eq 1 ] || exit 1
  case "$1" in 
    tensor1)
      echo 'n1_v' ;;
    pstensor1)
      echo 'n1_a' ;;
    tensor2s2)
      echo 'n2_v' ;;
    pstensor2s2)
      echo 'n2_a' ;;
    tensor3s3)
      echo 'n3_v' ;;
    pstensor3s3)
      echo 'n3_a' ;;
    *)
      echo >&2 "unknown op '$1'" ;;
  esac
}

#usage <i_ff>  <q2-list>  <in-file>  <out-file>
function mk_phil_data() {
  echo $*
  [ $# -eq 4 ] || exit 1
  local i_ff=$1
  local q2_list=$2
  local in_file=$3
  local out_file=$4
  pytsv -e 'show(join(3, a, b[:,0,:,'$i_ff'].swapaxes(0,1)[None,None]))' \
    "$q2_list"  "$in_file"  >"$out_file"
}

for op in $(echo "$OP_LIST") ; do
  out="$out_dir/$(op_phil $op)"
  mkdir -p "$out"
  for fl in $(echo "$FLAV_LIST") ; do
    pfl=$(flavor_phil $fl)
    case $op in 
      tensor1)
        mk_phil_data  0  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/A10_${pfl}" || exit 1
        mk_phil_data  1  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/B10_${pfl}" || exit 1
        ;;
      pstensor1)
        mk_phil_data  0  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/A10_${pfl}" || exit 1
        mk_phil_data  1  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/B10_${pfl}" || exit 1
        ;;
      tensor2s2)
        mk_phil_data  0  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/A20_${pfl}" || exit 1
        mk_phil_data  1  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/B20_${pfl}" || exit 1
        mk_phil_data  2  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/C20_${pfl}" || exit 1
        ;;
      pstensor2s2)
        mk_phil_data  0  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/A20_${pfl}" || exit 1
        mk_phil_data  1  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/B20_${pfl}" || exit 1
        ;;
      tensor3s3)
        mk_phil_data  0  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/A30_${pfl}" || exit 1
        mk_phil_data  1  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/A32_${pfl}" || exit 1
        mk_phil_data  2  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/B30_${pfl}" || exit 1
        mk_phil_data  3  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/B32_${pfl}" || exit 1
        ;;
      pstensor3s3)
        mk_phil_data  0  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/A30_${pfl}" || exit 1
        mk_phil_data  1  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/A32_${pfl}" || exit 1
        mk_phil_data  2  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/B30_${pfl}" || exit 1
        mk_phil_data  3  "$in_dir/q2_list"  "$in_dir/$op/${fl}-pltx"  "$out/B32_${pfl}" || exit 1
        ;;
      *)
        echo >&2 "unknown op '$op'"
        exit 1
        ;;
    esac
  done
done
