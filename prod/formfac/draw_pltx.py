import lhpd
import tsv
import numpy as np
import scipy as sp
from lhpd.fitting import gaussian_estimate as gaus_est
import math

import matplotlib as mpl
import matplotlib.pyplot as plt
import dataview
from dataview.config_mpl import style_group
from lhpd import pprint_ve

import pdb


mom_psnk000 = [
    # <i_mom>  <tag>  <label>
    ( 0, 'mom1',  '(000;000)' ),
    ( 1, 'mom3',  '(000;110)' ),
    ( 2, 'mom6',  '(000;100)' ),
    ( 3, 'mom10', '(000;111)' ),
]
mom_pxyz01 = [
    # (i_mom)  (tag)  (label)
    ( 0, 'mom1',  '(000;000)' ),
    ( 1, 'mom3',  '(000;110)' ),
    ( 2, 'mom4',  '(100;110)' ),
    ( 3, 'mom6',  '(000;100)' ),
    ( 4, 'mom7',  '(100;111)' ),
    ( 5, 'mom8',  '(100;010)' ),
    ( 6, 'mom10', '(000;111)' ),
    ( 7, 'mom11', '(100;011)' ),
    ( 8, 'mom15', '(100;-100)'),
    ( 9, 'mom21', '(100;-110)')
]

"""
stg = [
    style_group((.7,0,0),  '-', 's'),
    style_group((0,.4,0),  '-', 'd'),
    style_group((0,0,1),   '-', 'o'),
    style_group((0,.7,.7), '-', 'x'),
    style_group('#808000', '-', '+'),
    style_group('#e000d0', '-', 'h'),

    style_group('#ff6600', '-', 'v'),
    style_group('#7d26cd', '-', '^'),
    style_group('#b7410e', '-', '>'),
    style_group('#990066', '-', '<'),

    style_group('#000000', '-', '*', alpha_shade=.2)    # default black star
]
"""

ff_names = { 
    # <op> -> [ <ff1-name>, ...]
    'tensor1'       : [ 'F1', 'F2' ],
    'pstensor1'     : [ 'GA', 'GV' ],
    'tensor2s2'     : [ 'A20', 'B20', 'C20' ],
    'pstensor2s2'   : [ 'tA20', 'tB20' ],
    'tensor3s3'     : [ 'A30', 'A32', 'B30', 'B32' ],
    'pstensor3s3'   : [ 'tA30', 'tA32', 'tB30', 'tB32' ],
}
def get_ff_cnt(op): return len(ff_names[op])
def get_ff_name(op, i_ff): return ff_names[op][i_ff]
def get_ff_col(op, i_ff): 
    """ translate op and i_ff into val_col and err_col in an X-av-q2 file """
    return (1 + i_ff, 1 + i_ff + get_ff_cnt(op))

def draw_pltx_ff_compare_sep(data_list, op, i_ff, flav, i_mom, 
        stg_list, ax, xshift=None, xlim=None, ylim=None, do_label='dt'):
    """
        data_list     ff data specification [ (<dt>, <path>), ... ]
        op          (str) op-tag
        i_ff        (int) formfactor index
        flav        (str) flav-tag
        i_mom       (int) mom index
        ax          axes to draw in
        stg_list    
        do_label    None|'dt'
    """
    n_dt = len(data_list)
    if None==xshift: xshift = .3 / max(1, n_dt-1)
    
    # t range
    if None==xlim: tmin, tmax = None, None
    else: tmin, tmax = xlim
    if None==tmin: tmin = -.3
    if None==tmax: tmax = .3 + max(dt[0] for dt in data_list)

    for i_dt in range(n_dt):
        dt, p_  = data_list[i_dt]
        d_      = np.array(tsv.read_float(open('%s/%s/%s-av' % (p_, op, flav))))[0]
        n_ff    = get_ff_cnt(op)
        ff_a    = d_[i_mom,:,i_ff]
        ff_e    = d_[i_mom,:,i_ff+n_ff]
        t_r     = np.array(range(0, dt+1))
        if None == do_label:    label = ''
        elif 'dt' == do_label:  label = (r'$\Delta t=%d$' % dt)
        else: label = ''
        ax.errorbar(i_dt*xshift + t_r, ff_a, yerr=ff_e, label=label, 
                    **stg_list[i_dt].edotline())

    ax.set_xlim([tmin, tmax])
    if None != ylim: ax.set_ylim(ylim)
    if None != do_label: ax.legend()
    return ax


def drawlist_mom_pltx_ff_compare_sep(data_list, op, i_ff, flav, mom_list, 
        stg_list, ax_list, xshift=None, xlim=None, ylim=None):
    """ Draw a series of plateau plots comparing different separations
        Advantage: can set common ymin,ymax
        mom_list    [(mom_ind, mom_tag, mom_label), ...]
            mom_ind     (int) mom index in data files
            mom_tag     (str) tag 'mom#'
            mom_label   (str) label
    """
    pass    # TODO
    n_mom   = len(mom_list)
    ylim_l = []
    for i_mom, m in enumerate(mom_list):
        draw_pltx_ff_compare_sep(data_list, op, i_ff, flav, m[0], 
                                 stg_list, ax_list[i_mom], 
                                 xshift=xshift, xlim=xlim, ylim=None, do_label=None)
        ylim_l.append(ax_list[i_mom].get_ylim())
    ylim_ax = [min(yl[0] for yl in ylim_l), max(yl[1] for yl in ylim_l)]
    if None != ylim:
        if None != ylim[0]: ylim_ax[0] = min(ylim_ax[0], ylim[0])
        if None != ylim[1]: ylim_ax[1] = max(ylim_ax[1], ylim[1])
    for i_mom in range(n_mom):
        ax_list[i_mom].set_ylim(ylim_ax)
        ax_list[i_mom].text(0.01, 0.01, ('state=%s' % mom_list[i_mom][2]), 
                ha='left', va='bottom',
                transform = ax_list[i_mom].transAxes, fontsize='xx-small')
    return ax_list

def draw_pltx_ff_compare_mom(data, op, i_ff, flav, mom_list, stg_list, ax, 
        xshift=None, xlim=None, ylim=None, do_label='mom'): 
    """ Draw different mom.states on the same axis
        data        [(dt, path), ...]
            dt          (int) src-snk sep
            path        data directory
        mom_list    [(mom_ind, mom_tag, mom_label), ...]
            mom_ind     (int) mom index in data files
            mom_tag     (str) tag 'mom#'
            mom_label   (str) label
    
    """
    n_mom = len(mom_list)
    if None==xshift: xshift = .3 / max(1, n_mom-1)
    dt, p_ = data[0:2]
    # t range
    if None==xlim: tmin, tmax = None, None
    else: tmin, tmax = xlim
    if None==tmin: tmin = -.3
    if None==tmax: tmax = .3 + dt
    
    for i_mom in range(n_mom):
        i_data_mom, mom_tag, mom_label = mom_list[i_mom][0:3]
        d_      = np.array(tsv.read_float(open('%s/%s/%s-av' % (p_, op, flav))))[0]
        n_ff    = get_ff_cnt(op)
        ff_a    = d_[i_mom,:,i_ff]
        ff_e    = d_[i_mom,:,i_ff+n_ff]
        t_r     = np.array(range(0, dt+1))
        if None == do_label:    label = ''
        elif 'mom' == do_label: label = mom_label
        else: label = ''
        ax.errorbar(i_mom*xshift + t_r, ff_a, yerr=ff_e, label=label, 
                    **stg_list[i_mom].edotline())
        
    ax.set_xlim([tmin, tmax])
    if None != ylim: ax.set_ylim(ylim)
    if None != do_label: ax.legend()
    return ax

def drawlist_op_pltx_ff_compare_mom(data, op_ff_flav_list, mom_list, stg_list, ax_list,
        xshift=None, xlim=None, ylim=None): 
    """ TODO 
        data        [(dt, path), ...]
            dt          (int) src-snk sep
            path        data directory
        mom_list    [(mom_ind, mom_tag, mom_label), ...]
            mom_ind     (int) mom index in data files
            mom_tag     (str) tag 'mom#'
            mom_label   (str) label
        op_ff_list  [(op, i_ff)]
    """

    n_op    = len(op_ff_flav_list)
    for i_op, op_ff_flav in enumerate(op_ff_flav_list):
        op, i_ff, flav = op_ff_flav[0:3]
        draw_pltx_ff_compare_mom(data, op, i_ff, flav, mom_list, 
                                 stg_list, ax_list[i_op],
                                 xshift=xshift, xlim=xlim, ylim=None, do_label=None)
        ax_list[i_op].text(0.01, 0.01, ('FF=%s flav=%s' % (get_ff_name(op, i_ff), flav)),
                ha='left', va='bottom',
                transform = ax_list[i_op].transAxes, fontsize='xx-small')
    return ax_list
