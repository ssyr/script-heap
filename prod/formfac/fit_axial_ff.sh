#!/bin/bash

FIT_NPOLE="python $HOME/usr/lib/python/fit_proc/fit_npole.py"

. ./fit_axial_ff.config.sh || exit 1

#fit_GA=$(mktemp -p .)
#subtr_GP=$(mktemp -p .)
fit_GA=./tmp.fit_GA
subtr_GP=./tmp.subtr_GP


echo "# fit GA ~ a/(1+bx)**2"
echo "# a~gA"
echo "# b ; b~(rA^2) [lat] ; b~(rA^2) [fm^2] ; b~(rA) [fm]"
echo "# GA'(0)=-2*a*b"
echo "# chi2"
$FIT_NPOLE --fit-func=dipole --with-chi2=reduced --xslice="$xslice_GA"  q2_list  "$ff_file" \
  |tee $fit_GA \
  |pytsv -pe '
gA  = _[...,0:1,0:1]
B   = _[...,1:2,0:1]
chi2= _[...,2:3,0:1]
X = gA ; show_val_err(avg(X), var_jk(X))
X = join(3, B, 12 * B, 12 * B * (.1973/'$AINV')**2, sqrt(12*B)*.1973/'$AINV') ; show_val_err(avg(X), var_jk(X))
X = -2*gA*B ; show_val_err(avg(X), var_jk(X))
X = chi2 ; show_val_err(avg(X), var_jk(X))'

echo "#"
echo "# 4 * mN^2 * (gA - <GTD>) / (mpi^2 + Q^2) - 4 mN^2 GA'(0)"
echo "#"

echo "# fit GP to a/(1+bx) + c"
echo "# a ; a~(gA_pole) ; a~(gA/gA_pole)"
echo "# b ; b~(m_pi[lat]) ; b~(m_pi[GeV])"
echo "# c ; c~(-GA'(0)[lat]) ; c~(rA^2[fm^2])"
echo "# chi2/ndf"
pytsv -e 'show(a[...,1:2])'  "$ff_file" \
  |$FIT_NPOLE --fit-func='monopole+c' --with-chi2=reduced --xslice="$xslice_GP"  \
      q2_list  - \
  |pytsv -pe '
A=_[...,0:1,0:1]
B=_[...,1:2,0:1]
C=_[...,2:3,0:1]
chi2=_[...,3:4,0:1]
gA=a[...,0:1,0:1]
X=join(2, join(3, A, A*('$MPI'/'$MN'/2)**2, gA / (A*('$MPI'/'$MN'/2)**2)),
          join(3, B, 1./sqrt(B), '$AINV'/sqrt(B)),
          join(3, C, -C*1.5/'$MN'**2 / gA, -C*1.5/'$MN'**2 / gA * (.1973/'$AINV')**2),
          join(3, chi2, chi2, chi2))
show_val_err(avg(X),var_jk(X))'  "$fit_GA"


echo "#"
echo "# fit GP + 4*mN^2*(-GA'(0))  to  a/(1+b*x)"
echo "# a ; a~(gA_pole) ; a~(gA/gA_pole)"
echo "# b ; b~(m_pi[lat]) ; b~(m_pi[GeV])"
echo "# chi2/ndf"
pytsv -e 'show(b[...,1:2] + a[...,0:1,0:1] * a[...,1:2,0:1] * 8 *'$MN'**2) '  \
    "$fit_GA"  "$ff_file" >"$subtr_GP"
$FIT_NPOLE --fit-func=monopole --with-chi2=reduced --xslice="$xslice_GP"  q2_list  "$subtr_GP" \
    |pytsv -pe '
A=_[...,0:1,0:1]
B=_[...,1:2,0:1]
chi2=_[...,2:3,0:1]
gA=a[...,0:1,0:1]
X=join(2, join(3, A, A*('$MPI'/'$MN'/2)**2, gA / (A*('$MPI'/'$MN'/2)**2)),
          join(3, B, 1./sqrt(B), '$AINV'/sqrt(B)),
          join(3, chi2, chi2, chi2))
show_val_err(avg(X),var_jk(X))'  "$fit_GA"


echo "#"
echo "# fit (1 + Q^2 / mpi^2) GP = (a+c) + c * Q^2/mpi^2"
echo "# a ; a~(gA_pole) ; a~(gA/gA_pole)"
echo "# c ; c~(-GA'(0)[lat]) ; c~(rA^2[fm^2])"
echo "# chi2/ndf"
pytsv -e 'show(b[...,:,1:2]*(a / '$MPI'**2 + 1))'  q2_list  "$ff_file" \
  |$FIT_NPOLE --fit-func=linear --with-chi2=reduced --xslice="$xslice_GP" q2_list - \
  |pytsv -pe '
C=_[...,1:2,0:1] * '$MPI'**2
A=_[...,0:1,0:1] - C
chi2=_[...,2:3,0:1]
gA=a[...,0:1,0:1]
X=join(2, join(3, A, A*('$MPI'/'$MN'/2)**2, gA / (A*('$MPI'/'$MN'/2)**2)),
          join(3, C, -C*1.5/'$MN'**2 / gA, -C*1.5/'$MN'**2 / gA * (.1973/'$AINV')**2),
          join(3, chi2, chi2, chi2))
show_val_err(avg(X), var_jk(X))'  "$fit_GA"


echo "#"
echo "# fit (1 + Q^2 / mpi^2) GP / GA  to  A + B*x ; then A = (2mN/mpi)^2 (gApole/gA - rA^2 mpi^2/6)"
echo "# A ; A*(mpi/2mN)^2 ~ (gA_pole/gA - rA^2 mpi^2/6); A~(gA/gA_pole)"
echo "# B ; B*(mpi/2mN)^2 ; B*(mpi/2mN) [GeV^2]"
echo "# chi2/ndf"
pytsv -e 'show(b[...,1:2]/b[...,0:1]*(a / '$MPI'**2 + 1))'  q2_list  "$ff_file" \
  |$FIT_NPOLE --fit-func=linear --with-chi2=reduced --xslice="$xslice_GP" q2_list - \
  |pytsv -pe '
A=_[...,0:1,0:1]
B=_[...,1:2,0:1]
chi2=_[...,2:3,0:1]
mdGA0=2*a[...,1:2,0:1]
X=join(3, A, A * ('$MPI'/'$MN'/2.)**2, 1 / (A * ('$MPI'/'$MN'/2.)**2 + mdGA0 * '$MPI'**2))
show_val_err(avg(X), var_jk(X))
X=join(3, B, B * ('$MPI'/'$MN'/2.)**2, B * ('$MPI'/'$MN'/2.)**2 * '$AINV'**2)
show_val_err(avg(X), var_jk(X))
X=chi2; show_val_err(avg(X), var_jk(X))'  "$fit_GA"
#rm -f  "$fit_GA"  "$subtr_GP" 
