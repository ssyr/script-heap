#!/bin/bash

# default settings
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'

if [ $# -lt 2 ] ; then 
  echo -e "Usage:\n$(basename $0)  <config>  <out_dir>  [<PARAM1>=<val1> ...]"
  exit 1
fi
config=$1
out_dir=$2

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

# command line settings override config and default
shift 2
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done

function conv_to_comma_list() {
  [ $# -ge 1 ] || return 0
  local list=$1
  shift
  while [ $# -gt 0 ] ; do
    list="$list,$1"
    shift
  done
  echo "$list"
}

pltx_list=$(conv_to_comma_list $PLATEAU_3PT)


flav='U-D'

out_file=${out_dir}/${ENS_NAME}.${SET_NAME}.dt${SRC_SNK_DT}.mn${MN}.obsv
mkdir -p $(dirname $out_file)
tmpfile=$(mktemp ${out_dir}/tmp.XXXXXX)
echo "# g_V   g_A   g_A/g_V   <r^2_v1>^{${flav}}" >$out_file
pytsv -e '
plt=['$pltx_list'];
gv=a[:,0,plt,0].sum(axis=1)[...,None] / len(plt)
f1qnz=a[:,'$R2_Q2_INDEX',plt,0].sum(axis=1)[...,None] / len(plt)
ga=b[:,0,plt,0].sum(axis=1)[...,None] / len(plt)
r2=12*(sqrt(gv / f1qnz)-1)/'$R2_DQ2'
res=join(1, gv, ga, ga/gv, r2)[:,None,None,:]
show_val_err(avg(res), var_jk(res))' \
  ${out_dir}/tensor1/${flav} ${out_dir}/pstensor1/${flav} \
  >>$out_file
  
