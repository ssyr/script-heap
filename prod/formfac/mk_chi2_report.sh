
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'
FLAV_LIST="U D"

GSL_FUNC="$HOME/src/lhpc-data-trunk-new/utils/gsl_func"
function analyze_chi2() {
  etab -r '[]' -e '($[1]==0 ? 0. : $[0]) ; $[1]' \
  |$GSL_FUNC -f - cdf_chisq_Q \
  |l2b.pl |estimate.pl --med --min
}


if [ $# -lt 2 ] ; then 
  echo -e "Usage:\n$(basename $0)  <config>  <out_dir>  [<PARAM1>=<val1> ...]"
  exit 1
fi
config=$1
out_dir=$2

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

# command line settings override config and default
shift 2
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done

. ff_functions.sh
for op in $(echo $OP_LIST) ; do
  for flav in $(echo $FLAV_LIST) ; do
    chi2_file=${out_dir}/${op}/chi2.${flav}
    echo '# chi2 ME' >${chi2_file}
    estimate.pl --avg <${out_dir}/${op}/chi2_me.${flav} \
      |analyze_chi2 >>${chi2_file}
    echo '# chi2 pltx' >>${chi2_file}
    estimate.pl --avg <${out_dir}/${op}/chi2_pltx.${flav} \
      |analyze_chi2 >>${chi2_file}
  done
done
