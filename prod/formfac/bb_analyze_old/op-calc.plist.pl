#!/usr/bin/perl -w

package main;
use strict;
 
use File::Basename;
push(@INC, "/home/syritsyn/usr/share/perlmod");
require Bbconv;

my $FAST_CP     = 'my-cp';
my $LHPC_AFF    = 'lhpc-aff';
my $CALC_OP     = 'calc_op';
my $process_id = basename($0) . ".$$";

if ($#ARGV+1 != 3) {
    print STDERR "Usage:\nme <config.pl> <#-list> <q-list>\n";
    exit 1;
}

# read parameters
my %config      = ();
if (!-f $ARGV[0]) {
    print STDERR "$ARGV[0]: file not found\n";
    exit 1;
}
eval `cat $ARGV[0]`; die $@ if ($@);
if (have_unset_keys(\%config, qw(
                                 id latsize qmass smear
                                 src_snk_dt n_sources
                                 scratch log_dir bb_dir op_dir
                                 hadron_list psnk_list op_list flav_list
                                 ))) {
    print STDERR "have unset parameters\n";
    exit 1;
}
debug_print("config=(", hash_to_string(\%config), ")");
my    ($LOG_DIR, $BB_DIR, $OP_DIR, $SCRATCH, $ID) = @config{
     qw(log_dir   bb_dir   op_dir   scratch   id)};

# read config list
open(N_LIST, "<$ARGV[1]") 
    or die "$ARGV[1]: cannot open file for read";
my @n_list = map { chomp ; [ split(/\s+/) ] } <N_LIST>;
#foreach (@n_list) { print "@$_\n" } ; die "checkpoint";
close(N_LIST);

# get path to qlist file
my $qlist = $ARGV[2];
if (substr($qlist, 0, 1) ne '/') { $qlist = getcwd() . "/" . $qlist }
if (!-f $qlist) {
    print STDERR "$ARGV[2]: file not found\n";
    exit 1;
}

# set up log of finished cfg_key's
my $list_done = basename($0).".done.list";
invoc_shell("touch $list_done");

foreach my $meas_spec (@n_list) {
    my ($cfg_key, $parity) = @$meas_spec;
    # check list_log
    my @list_done = grep({$_ eq $cfg_key} split(/\n+/, invoc_shell("cat $list_done")));
    if (scalar(@list_done)) { 
        debug_print("skipping $cfg_key: done");
        next 
    }
    
    # make directories to store result
    invoc_shell("mkdir -p $LOG_DIR/$cfg_key");
    invoc_shell("mkdir -p $OP_DIR/$cfg_key $OP_DIR/$cfg_key/split");
    
    # make scratch dir and cd to it
    my $scratch = "$SCRATCH/$ID.$cfg_key.$process_id";
    debug_print('scratch=', $scratch);
    invoc_shell("mkdir -p $scratch");
    !pushd($scratch) or die;
    invoc_shell("mkdir -p bb op op/split");
    
    # cycle over cases
    for my $hadron (@{$config{'hadron_list'}}) {
        for my $psnk (@{$config{'psnk_list'}}) {
            for my $i_src (0 .. $config{'n_sources'}-1) {
                # TODO? also cycle over src_snk_dt
                my $csrc = get_src_coord($parity, $i_src);
                my $tsnk = calc_sink_pos($csrc->[3], $config{'src_snk_dt'}, 
                                         $config{'latsize'}->[3], $hadron);

                # FIXME this is a stupid part due to calc_op iface; 
                #       calc_op should be rewritten and code below fixed
                my $aff_bbfile = "bb/all_flav.tmp.aff";
                open(AFF_MERGE, "|$LHPC_AFF join -o $aff_bbfile -f -")
                    or die "cannot run $LHPC_AFF";
                foreach my $flav (@{$config{'flav_list'}}) {
                    my $f = bb_name_prod($cfg_key, $csrc, $tsnk, $psnk, $hadron, $flav);
                    die "missing file: $BB_DIR/$f"
                        if (!-f "$BB_DIR/$f");
                    invoc_shell("rm -f bb/$f");
                    invoc_shell("$FAST_CP $BB_DIR/$f bb/$f");
                    print AFF_MERGE "/ bb/$f /\n";
                }
                close AFF_MERGE
                    or die "cannot close $LHPC_AFF";

                # logging
                my $out_log = "$LOG_DIR/$cfg_key/out.$process_id";
                my $err_log = "$LOG_DIR/$cfg_key/err.$process_id";

                my $aff_opfile = "op/all_flav_op.tmp.aff";
                my $bb_kpath = sprintf('/cfg%s/bb/%s/%s', $cfg_key, @config{qw(smear qmass)});
                my $op_kpath = sprintf('/cfg%s/op/%s/%s', $cfg_key, @config{qw(smear qmass)});
                invoc_shell("$CALC_OP $config{'calc_op_arg'} -q $qlist " .
                            " -123 -O tensor3s23s1 -O tensor2a2 " .
                            sprintf(' -G %d,%d,%d,%d ', @{$config{'latsize'}}) .
                            sprintf(' -c %d,%d,%d,%d ', @$csrc) .
                            sprintf(' -p %d,%d,%d,%d ', @$psnk, $tsnk) .
                            " -b $hadron " .
                            " -i $aff_bbfile $bb_kpath  -o $aff_opfile $op_kpath" .
                            " >>$out_log 2>>$err_log");
                
                
                # split operators
                my @klist = map({ s/:.*$//; $_}
                                split(/\n+/, 
                                      invoc_shell("$LHPC_AFF ls -kR $aff_opfile")));
                my $aff_split_cmd = "| $LHPC_AFF extract -f - $aff_opfile " .
                                    " >>$out_log 2>>$err_log";
                debug_print($aff_split_cmd);
                open(AFF_SPLIT_STREAM, $aff_split_cmd)
                    or die "$LHPC_AFF: cannot start job";
                
                foreach my $op (@{$config{'op_list'}}) {
                    foreach my $flav (@{$config{'flav_list'}}) {
                        my $f = "op/split/" . 
                                op_name_prod($cfg_key, $csrc, $tsnk, $psnk,
                                             $hadron, $flav, $op);
                        my @klist_cur = grep(/\/$op$/ && /\/$flav\//, @klist); 
                        die "internal error"
                            if (1+$#klist_cur != 1);
                        my $k = $klist_cur[0];
                        print AFF_SPLIT_STREAM "$k $f $k\n";
                        die "$LHPC_AFF: error $?" 
                            if ($?);
                    }
                }
                close(AFF_SPLIT_STREAM)
                    or die "$LHPC_AFF: error finishing job";

                 # copy split operators
                foreach my $op (@{$config{'op_list'}}) {
                    foreach my $flav (@{$config{'flav_list'}}) {
                        my $f = op_name_prod($cfg_key, $csrc, $tsnk, $psnk,
                                             $hadron, $flav, $op);
                        invoc_shell("rm -f $OP_DIR/$cfg_key/split/$f");
                        invoc_shell("$FAST_CP op/split/$f $OP_DIR/$cfg_key/split/$f");
                        invoc_shell("rm -f op/split/$f");
                    }
                }

                # clean up
                invoc_shell("rm -f $aff_bbfile $aff_opfile");
                # TODO clean up "bb/"
            }
        }
    }
    
    # clean
    !popd() or die;
    invoc_shell("rm -r $scratch");

    # mark as done
    invoc_shell("echo '$cfg_key' >>$list_done");
}
