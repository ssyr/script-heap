#!/usr/bin/perl -w

package main;
use strict;
 
use File::Basename;
push(@INC, "/home/syritsyn/usr/share/perlmod");
require Bbconv;

our ($int_pattern, $uint_pattern, $id_pattern, $CALC_OP);
my ($int_p, $uint_p) = ($int_pattern, $uint_pattern);

my $PVFS_CP = 'my-cp';
my $LHPC_AFF = 'lhpc-aff';

sub bmw_aff_op_filename_parse($$);

my $process_id = basename($0) . ".$$";
if ($#ARGV+1 != 2) {
    print STDERR "Usage:\nme <config.pl> <#-list>\n";
    exit 1;
}

# read parameters
my %config      = ();
if (!-f $ARGV[0]) {
    print STDERR "$ARGV[0]: file not found\n";
    exit 1;
}
eval `cat $ARGV[0]`; die $@ if ($@);
if (have_unset_keys(\%config, qw(
                                 id latsize qmass smear
                                 src_snk_dt n_sources
                                 scratch log_dir bb_dir op_dir
                                 hadron_list psnk_list op_list flav_list
                                 ))) {
    print STDERR "have unset parameters\n";
    exit 1;
}
debug_print("config=(", hash_to_string(\%config), ")");
my    ($LOG_DIR, $BB_DIR, $OP_DIR, $SCRATCH, $ID) = @config{
     qw(log_dir   bb_dir   op_dir   scratch   id)};

# read config list
open(N_LIST, "<$ARGV[1]") 
    or die "$ARGV[1]: cannot open file for read";
my @n_list = map { chomp ; [ split(/\s+/) ] } <N_LIST>;
#foreach (@n_list) { print "@$_\n" } ; die "checkpoint";
close(N_LIST);


# cycle over cases
for my $hadron (@{$config{'hadron_list'}}) {
    for my $op (@{$config{'op_list'}}) {
        for my $flav (@{$config{'flav_list'}}) {
            my $aff_opfile = "op.${hadron}.${op}.${flav}.dt$config{'src_snk_dt'}.aff" ;
            my $aff_join_cmd = "| $LHPC_AFF join -f - -o $aff_opfile ";
            debug_print($aff_join_cmd);
            open(AFF_JOIN_STREAM, $aff_join_cmd)
                or die "$LHPC_AFF: cannot start job";
            
            # make scratch dir 
            my $scratch = "$SCRATCH/$ID.$process_id.$hadron.$op";
            debug_print('scratch=', $scratch);
            invoc_shell("mkdir -p $scratch");

            foreach my $meas_spec (@n_list) {
                my ($cfg_key, $parity) = @$meas_spec;
                for my $i_src (0 .. $config{'n_sources'}-1) {
                    # TODO? also cycle over src_snk_dt
                    my $csrc = get_src_coord($parity, $i_src);
                    my $tsnk = calc_sink_pos($csrc->[3], $config{'src_snk_dt'},
                                    $config{'latsize'}->[3], $hadron);
                    
                    for my $psnk (@{$config{'psnk_list'}}) {
                        # get op file
                        my $aff_opfile_source = op_name_prod($cfg_key, $csrc,
                                        $tsnk, $psnk, $hadron, $flav, $op);

                        my $f = "$scratch/$aff_opfile_source";
                        invoc_shell("rm -f $f");
                        invoc_shell("$PVFS_CP $OP_DIR/$cfg_key/split/$aff_opfile_source $f");
                        print AFF_JOIN_STREAM "/ $f /\n";
                        die "$LHPC_AFF: error $?" 
                           if ($?);
                    }
                }
            }
            close(AFF_JOIN_STREAM)
                or die "$LHPC_AFF: error finishing job";
            die "$LHPC_AFF: error $?" 
               if ($?);
                            
            invoc_shell("rm -r $scratch");
        }
    }

}

