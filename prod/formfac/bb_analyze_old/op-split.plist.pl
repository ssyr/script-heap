#!/usr/bin/perl -w

package main;
use strict;
 
use File::Basename;
push(@INC, "/home/syritsyn/usr/share/perlmod");
require Bbconv;

our ($int_pattern, $uint_pattern, $id_pattern, $CALC_OP);
my ($int_p, $uint_p) = ($int_pattern, $uint_pattern);

my $FAST_CP = 'my-cp';
my $LHPC_AFF = 'lhpc-aff';

sub bmw_aff_op_filename_parse($$);

my $process_id = basename($0) . ".$$";
if ($#ARGV+1 != 2) {
    print STDERR "Usage:\nme <config.pl> <#-list>\n";
    exit 1;
}

# read parameters
my %config      = ();
if (!-f $ARGV[0]) {
    print STDERR "$ARGV[0]: file not found\n";
    exit 1;
}
eval `cat $ARGV[0]`; die $@ if ($@);
if (have_unset_keys(\%config, qw(
                                 id latsize qmass smear
                                 src_snk_dt n_sources
                                 scratch log_dir bb_dir op_dir
                                 hadron_list psnk_list op_list flav_list
                                 ))) {
    print STDERR "have unset parameters\n";
    exit 1;
}
debug_print("config=(", hash_to_string(\%config), ")");
my    ($LOG_DIR, $BB_DIR, $OP_DIR, $SCRATCH, $ID) = @config{
     qw(log_dir   bb_dir   op_dir   scratch   id)};

# read config list
open(N_LIST, "<$ARGV[1]") 
    or die "$ARGV[1]: cannot open file for read";
my @n_list = map { chomp ; [ split(/\s+/) ] } <N_LIST>;
#foreach (@n_list) { print "@$_\n" } ; die "checkpoint";
close(N_LIST);

my $list_done = basename($0).".done.list";
invoc_shell("touch $list_done");

foreach my $meas_spec (@n_list) {
    my ($cfg_key, $parity) = @$meas_spec;
    # check list_log
    my @list_done = grep({$_ eq $cfg_key} split(/\n+/, invoc_shell("cat $list_done")));
    if (scalar(@list_done)) { 
        debug_print("skipping $cfg_key: done");
        next 
    }
    
    # make directories to store result
    invoc_shell("mkdir -p $LOG_DIR/$cfg_key");
    invoc_shell("mkdir -p $OP_DIR/$cfg_key $OP_DIR/$cfg_key/split");
    
    # make scratch dir and cd to it
    my $scratch = "$SCRATCH/$ID.$cfg_key.$process_id";
    debug_print('scratch=', $scratch);
    invoc_shell("mkdir -p $scratch");
    !pushd($scratch) or die;
    invoc_shell("mkdir -p bb op op/split");
    
    # cycle over cases
    for my $hadron (@{$config{'hadron_list'}}) {
        for my $psnk (@{$config{'psnk_list'}}) {
            for my $i_src (0 .. $config{'n_sources'}-1) {
                # TODO? also cycle over src_snk_dt
                my $csrc = get_src_coord($parity, $i_src);
                my $tsnk = calc_sink_pos($csrc->[3], $config{'src_snk_dt'}, 
                                         $config{'latsize'}->[3], $hadron);

                my $aff_opfile = op_name_oldprod($cfg_key, $csrc, $tsnk, $psnk, $hadron,
                                        $config{'smear'});

                
                # logging
                my $out_log = "$LOG_DIR/$cfg_key/out.$process_id";
                my $err_log = "$LOG_DIR/$cfg_key/err.$process_id";

                # get op file
                my $aff_opfile_source = "$OP_DIR/$cfg_key/$aff_opfile";
                invoc_shell("rm -f op/$aff_opfile");
                invoc_shell("$FAST_CP $aff_opfile_source op/$aff_opfile");
                
                # split operators
                my @klist = map({ s/:.*$//; $_}
                                split(/\n+/, 
                                      invoc_shell("$LHPC_AFF ls -kR op/$aff_opfile")));
                my $aff_split_cmd = "| $LHPC_AFF extract -f - op/$aff_opfile " .
                                    " >>$out_log 2>>$err_log";
                debug_print($aff_split_cmd);
                open(AFF_SPLIT_STREAM, $aff_split_cmd)
                    or die "$LHPC_AFF: cannot start job";
                
                for my $op (@{$config{'op_list'}}) {
                    for my $flav (@{$config{'flav_list'}}) {
                        my $f = "op/split/" . 
                                op_name_prod($cfg_key, $csrc, $tsnk, $psnk,
                                             $hadron, $flav, $op);
                        my @klist_cur = grep(/\/$op$/ && /\/$flav\//, @klist); 
                        die "internal error"
                            if (1+$#klist_cur != 1);
                        my $k = $klist_cur[0];
                        print AFF_SPLIT_STREAM "$k $f $k\n";
                        die "$LHPC_AFF: error $?" 
                            if ($?);
                    }
                }
                close(AFF_SPLIT_STREAM)
                    or die "$LHPC_AFF: error finishing job";
                
                # copy split operators
                for my $op (@{$config{'op_list'}}) {
                    for my $flav (@{$config{'flav_list'}}) {
                        my $f = op_name_prod($cfg_key, $csrc, $tsnk, $psnk,
                                             $hadron, $flav, $op);
                        invoc_shell("rm -f $OP_DIR/$cfg_key/split/$f");
                        invoc_shell("$FAST_CP op/split/$f $OP_DIR/$cfg_key/split/$f");
                        invoc_shell("rm -f op/split/$f");
                    }
                }
                # clean aff_op
                invoc_shell("rm -f op/$aff_opfile");
            }
        }
    }
    
    # clean
    !popd() or die;
    invoc_shell("rm -r $scratch");

    # mark as done
    invoc_shell("echo '$cfg_key' >>$list_done");
}

