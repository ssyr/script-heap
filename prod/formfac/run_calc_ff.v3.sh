#!/bin/bash

# default settings
NO_AFF_CHECK=""
DATA_BASE_DIR='..'
MAX_AFF_FILES=8
CALC_FF_OPTIONS='-o reduce_zeros'
STATE_COMB_PATH='../mom/mom'
JK_BINSIZE=1
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'
FLAV_LIST="U D"
function get_specific_options() { true; }

if [ $# -lt 2 ] ; then 
  echo -e "Usage:\n$(basename $0)  <config>  <out_dir>  [<PARAM1>=<val1> ...]"
  exit 1
fi
config=$1
out_dir=$2

SOLVER="time calc_ff"

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

# command line settings override config and default
shift 2
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done

mkdir -p $out_dir
$SOLVER -Z analysis=desc_mom \
  -G ${GEOMETRY} -O tensor1 \
  -m ${MN} \
  -D ${SRC_SNK_DT} \
  $(for n in $(echo $STATE_COMB_LIST) ; do echo "-s ${STATE_COMB_PATH}$n " ; done) \
  ${CALC_FF_OPTIONS} \
   |gawk '{print $NF }' >${out_dir}/q2_list || exit 1

for op in $(echo $OP_LIST) ; do
  mkdir -p "${out_dir}/${op}"
  for flav in $(echo $FLAV_LIST) ; do
    list_name="list/${op}.${flav}"
    if [ "$NO_AFF_CHECK" == "" ] ; then
      for f in $(gawk '{print $1}' <$list_name |grep -v 'hadspec' |sort |uniq) \
                 $(gawk '{print $3}' <$list_name |grep -v 'hadspec' |sort |uniq) ; do
        echo "lhpc-aff check ${DATA_BASE_DIR}/$f"
        lhpc-aff check ${DATA_BASE_DIR}/$f || exit 1
      done
    fi
    $SOLVER -Z analysis=ff \
           -G ${GEOMETRY} \
           -F ${flav} \
           -O ${op} \
           -m ${MN} \
           -D ${SRC_SNK_DT} \
           -i $list_name \
           $(for n in $(echo $STATE_COMB_LIST) ; do echo "-s ${STATE_COMB_PATH}$n " ; done) \
           -b $DATA_BASE_DIR \
           -I $MAX_AFF_FILES \
           -Z resample=jk:${JK_BINSIZE} \
           -Z chi2_pltx_file=${out_dir}/${op}/chi2_pltx.${flav} \
           -Z chi2_me_file=${out_dir}/${op}/chi2_me.${flav} \
           $(get_specific_options $op $flav) \
           ${CALC_FF_OPTIONS} \
            >${out_dir}/${op}/${flav} || exit 1
  done
done
