#!/bin/bash

if [ $# -lt 3 ] ; then
  echo >&2 -ne "Usage:\n$0  <scale>  <in-dir>  <out-dir>\n"
  exit 1
fi

scale=$1
in_dir=$2
out_dir=$3
shift 3

# default lists
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'
# setting lists from cmdline
FLAV_LIST="U D U-D U+D P N"
for opt in "$@" ; do
  echo "Setting '$opt'"
  eval "$opt" || exit 1
done

for op in $(echo $OP_LIST) ; do
  mkdir -p "$out_dir/$op" || exit 1
  for flav in $(echo $FLAV_LIST) ; do
    pytsv -e 'show('$scale'*a)' "$in_dir/$op/$flav"  > "$out_dir/$op/$flav"
  done
done

cp  "$in_dir/q2_list"  "$out_dir/q2_list"
cp  "$in_dir/config"  "$out_dir/config.orig"
cat >"$out_dir/README.scaled" <<EOF
Scaled by scale=$scale
OP_LIST="$OP_LIST"
FLAV_LIST="$FLAV_LIST"

CONFIG:
---------
EOF
cat "$in_dir/config"  >>"$out_dir/README.scaled"
