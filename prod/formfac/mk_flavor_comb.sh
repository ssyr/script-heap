#!/bin/bash

# default settings
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'

if [ $# -lt 2 ] ; then
  echo -e "Usage:\n$(basename $0)  <config>  <out-dir>"
  exit 1
fi
config=$1
out_dir=$2

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

# command line overrides all above
shift 2
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done


for op in $(echo $OP_LIST) ; do
  if [ ! -d ${out_dir}/${op} ] ; then
    echo >&2 "${out_dir}/${op} is absent"
    exit 1
  fi
  pytsv -se 'show(a-b)' ${out_dir}/${op}/{U,D} >${out_dir}/${op}/U-D
  pytsv -se 'show(a+b)' ${out_dir}/${op}/{U,D} >${out_dir}/${op}/U+D
  pytsv -se 'show((2*a+b)/3.)' ${out_dir}/${op}/{U,D} >${out_dir}/${op}/P
  pytsv -se 'show((a+2*b)/3.)' ${out_dir}/${op}/{U,D} >${out_dir}/${op}/N
done
