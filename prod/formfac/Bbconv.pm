#!/usr/bin/perl -w
package main;
use strict;
use File::Basename;
use Cwd;

our $LHPC_AFF = 'lhpc-aff';
#our $CALC_OP = '/home/syritsyn/src/lhpc-data-trunk/calc_op/calc_op';
#our $STRIP_BB = '/home/syritsyn/src/lhpc-data-trunk/strippers/strip_bb';
our $CALC_OP = 'calc_op';
our $STRIP_BB = 'strip_bb';

# TODO make constants
our $id_pattern = '[A-Za-z_][0-9A-Za-z_]*';
our $uint_pattern = '[0-9]+';
our $int_pattern = '[+-]?' . $uint_pattern;
# short aliases
my ($int_p, $uint_p) = ($int_pattern, $uint_pattern);

my $VERBOSE=1;

sub debug_pause {
    print @_, ' :', (caller(1))[2], ': pause>';
    my $a = <STDIN>;
}

sub debug_print(@) {
    my $caller = (caller(1))[3];
    if (!defined($caller)) { $caller = '<top>'}
    if ($VERBOSE) { 
        print sprintf('[%02d:%02d:%02d] (%s) ', (localtime)[2,1,0], $caller),
              @_, "\n" ;
    }
}
sub hash_to_string($) {
    my $hash = shift;
    return join(', ', map({"'$_'=>'$hash->{$_}'"} 
                          sort({$a cmp $b} 
                               keys %{$hash})));
}
sub array_to_string($) {
    my $arr = shift;
    return join(', ', @{$arr});
}
sub ref_as_list($) {
    my $a = $_[0];
    if (ref($a) eq '') { return $a }
    elsif (ref($a) eq 'SCALAR' ) { return (${$a}) }
    elsif (ref($a) eq 'ARRAY') { return (@{$a}) }
    elsif (ref($a) eq 'HASH' ) { return values(%{$a}) }
    else { die "cannot make list from bad ref = '", ref($a), "'" };
    return ()
}

my @dirstack=();
# push into dir
# usage: $0  dirname
sub pushd($) {
    my $dir = shift;
    debug_print("$dir");
    if (-d $dir) {
        push @dirstack, getcwd();
        if (!chdir($dir)) {
            print STDERR "$dir: cannot chdir\n";
            return 1;
        }
        return 0;
    } else {
        print STDERR "$dir: no such directory\n";
        return 1
    }
}
# pop from dir
# usage: $0
sub popd {
    my $depth = 1;
    if (0 <= $#_) { 
        $depth = shift;
        if ($depth <= 0) {
            print STDERR "$depth: bad depth\n";
            return 1;
        }
    }
    for (my $i = $depth; $i-- ;) {
        if ($#dirstack < 0) { 
            print STDERR "dirstack is empty\n";
            return 1 
        }
        my $dir = pop @dirstack;
        debug_print("to $dir");
        if (!chdir($dir)) {
            print STDERR "$dir: cannot chdir\n";
            return 1;
        }
    }
    return 0;
}

# shell command invocation
# usage: $0  cmd
sub invoc_shell($) {
    my $cmd = shift;
    debug_print("$cmd");
    my $res = `$cmd`; 
    die "$cmd: shell invoc error $?" if ($?);
    chomp($res);
    return $res;
}

# compare the values for the keys that exist in both hashes
# usage: $0 \%hash1 \%hash2
sub compare_existing_keys($$) {
    my ($hash1, $hash2) = @_;
    foreach (keys %{$hash1}) {
        if (exists($hash2->{$_}) && 
            ($hash1->{$_} ne $hash2->{$_})) {
            debug_print("$_: value mismatch: '$hash1->{$_}' != '$hash2->{$_}'");
            return 1;
        }
    }
}

# check that the keys exist in the hash and the values are defined
# usage $0 \%hash key ...
sub have_unset_keys($@) {
    my $hash = shift;
    my $have_unset = 0;
    foreach (@_) {
        if (!exists(${$hash}{$_})) {
            debug_print("{$_}: do not exist");
            $have_unset = 1;
        } elsif (!defined(${$hash}{$_})) {
            debug_print("{$_}: unset");
            $have_unset = 1;
        }
    }
    return $have_unset;
}



# hadron names used in AFF keypaths in hspectrum, bb, op
# usage:  <hadron>
sub aff_hadron_name($) { return $_[0] }
# parse hadron name: key -> $param{'hadron'}
# usage:  <name>
sub aff_hadron_name_parse($) { return $_[0] }


# hadron names used in AFF file names
# usage:  <hadron>
sub aff_hadron_name_f($) {
    my $hadron = shift;
    if ($hadron eq 'proton_3')              { return 'proton3' }
    elsif ($hadron eq 'proton_negpar_3')    { return 'protonNegpar3' }
    elsif ($hadron eq 'pion_1')             { return 'pion1' }
    else { die "unknown \$hadron=$hadron" }
}
# parse hadron name fron AFF file name: name -> $param{'hadron'}
# usage:  <name>
sub aff_hadron_name_f_parse($) { 
    my $name = shift;
    if ($name eq 'proton3')                 { return 'proton_3' }
    elsif ($name eq 'protonNegpar3')        { return 'proton_negpar_3' }
    elsif ($name eq 'pion1')                { return 'pion_1' }
    else { die "unknown \$hadron=$name" }
}

# check if the correlator with 'hadron' state needs time reverse
# usage:  <hadron>
sub need_time_reverse($) {
    my $hadron = shift;
    if ($hadron eq 'proton_3')              { return 0 }
    if ($hadron eq 'pion_1')                { return 0 }
    elsif ($hadron eq 'proton_negpar_3')    { return 1 }
    else { die "unknown \$hadron=$hadron" }
}

# calculate the sink position from source position, separation and hadron type
# usage:  $src_t  $src_snk_dt  $latsize_t  $hadron
sub calc_sink_pos($$$$) {
    my ($src_t, $dt, $latsize_t, $hadron) = @_;
    if (need_time_reverse($hadron))     { 
        return ($latsize_t + $src_t - $dt) % $latsize_t 
    } else {
        return ($src_t + $dt) % $latsize_t 
    }
}


# compose AFF BB file name from parameters
# usage:  \%param
sub aff_bb_filename($) {
    my $param = shift;
    !have_unset_keys($param, qw(hadron cfg_n smear 
                                snk_px snk_py snk_pz snk_t
                                src_x src_y src_z src_t))
        or die "have unset keys";
    return sprintf('cfg%s_bb_%s_%s_x%dy%dz%dt%d_PX%dPY%dPZ%dT%d.aff',
        @{$param}{qw(cfg_n smear)}, aff_hadron_name_f(${$param}{'hadron'}),
        @{$param}{qw(src_x src_y src_z src_t)}, 
        @{$param}{qw(snk_px snk_py snk_pz snk_t)});
}

# parse AFF BB file name
# usage:  $name \%param
sub aff_bb_filename_parse($$) {
    my ($name, $param) = @_;
    if ($name =~ /cfg([0-9_]+)_bb_([^_]+)_([^_]+)_x($uint_p)y($uint_p)z($uint_p)t($uint_p)_PX($int_p)PY($int_p)PZ($int_p)T($uint_p).aff/) {
        @{$param}{qw(cfg_n smear hadron)} = ($1, $2, aff_hadron_name_f_parse($3));
        @{$param}{qw(src_x src_y src_z src_t)} = map({sprintf('%d', $_)} ($4, $5, $6, $7));
        @{$param}{qw(snk_px snk_py snk_pz snk_t)} = map({sprintf('%d', $_)} ($8, $9, $10, $11));
        return 0;
    } else { return 1 }
}

# compose AFF BB keypath from parameters
# usage:  \%param
sub aff_bb_keypath($) {
    my ($param) = @_;
    !have_unset_keys($param, qw(cfg_n smear qmass)) 
        or die "have unset keys";
    return sprintf('/cfg%s/bb/%s/%s',
        @{$param}{qw(cfg_n smear qmass)});
}

# generate log file name
# usage:  \%param
sub log_name($) {
    my $param = shift;
    return aff_bb_filename($param);
}

# compose AFF OP file name from parameters
#   one cfg
#   many op_spec
#   one hadron
#   one source-sink pair
# usage:  \%param
sub aff_op_filename($) {
    my ($param) = (@_);
    !have_unset_keys($param, qw(hadron cfg_n smear 
                                snk_px snk_py snk_pz snk_t
                                src_x src_y src_z src_t))
        or die "have unset keys";
    return sprintf('cfg%s_op_%s_%s_x%dy%dz%dt%d_PX%dPY%dPZ%dT%d.aff', 
        @{$param}{qw(cfg_n smear)}, aff_hadron_name_f(${$param}{'hadron'}),
        @{$param}{qw(src_x src_y src_z src_t)}, 
        @{$param}{qw(snk_px snk_py snk_pz snk_t)});
}

# parse AFF op file name
# usage:  $name \%param
sub aff_op_filename_parse($) {
    my ($name, $param) = @_;
    if ($name =~ /cfg([0-9_]+)_op_([^_]+)_([^_]+)_x($uint_p)y($uint_p)z($uint_p)t($uint_p)_PX($int_p)PY($int_p)PZ($int_p)T($uint_p).aff/) {
        @{$param}{qw(cfg_n smear hadron)} = ($1, $2, aff_hadron_name_f_parse($3));
        @{$param}{qw(src_x src_y src_z src_t)} = map({sprintf('%d', $_)} ($4, $5, $6, $7));
        @{$param}{qw(snk_px snk_py snk_pz snk_t)} = map({sprintf('%d', $_)} ($8, $9, $10, $11));
        return 0;
    } else { return 1 }
}
        

# compose AFF op filename: 
#   one cfg
#   one op_spec
#   one hadron
#   many source-sink pairs with the same separation
# usage:  \%param
sub aff_opspec_filename($) {
    my ($param) = @_;
    !have_unset_keys($param, qw(op_spec cfg_n smear hadron src_snk_dt))
        or die 'have unset keys';
    return sprintf('cfg%s_op_%s_%s_dt%d_%s.aff',
        @{$param}{qw(cfg_n smear)}, aff_hadron_name_f($param->{'hadron'}),
        @{$param}{qw(src_snk_dt op_spec)});
}


# parse AFF op filename: 
#   one cfg
#   one op_spec
#   one hadron
#   many source-sink pairs with the same separation
# usage:  $name, \%param
sub aff_opspec_filename_parse($) {
    my ($name, $param) = @_;
    if ($name =~ /cfg([0-9_]+)_op_([^_]+)_([^_]+)_dt($uint_p)_([^_]+).aff/) {
        @{$param}{qw(cfg_n smear hadron)} = ($1, $2, aff_hadron_name_f_parse($3));
        @{$param}{qw(src_snk_dt op_spec)} = (sprintf('%d', $4), $5);
        return 0;
    } else { return 1 }
}


# compose AFF op filename: 
#   many cfgs
#   one op_spec
#   one hadron
#   many source-sink pairs with the same separation
# usage:  \%param
sub aff_opspec_ens_filename($) {
    my ($param) = @_;
    !have_unset_keys($param, qw(op_spec smear hadron src_snk_dt))
        or die 'have unset keys';
    return sprintf('op_%s_%s_dt%d_%s.aff',
        @{$param}{qw(smear)}, aff_hadron_name_f($param->{'hadron'}),
        @{$param}{qw(src_snk_dt op_spec)});
}

# parse AFF op filename: 
#   many cfgs
#   one op_spec
#   one hadron
#   many source-sink pairs with the same separation
# usage:  \%param
sub aff_opspec_ens_filename_parse($$) {
    my ($name, $param) = @_;
    if ($name =~ /op_([^_]+)_([^_]+)_dt($uint_p)_([^_]+).aff/) {
        @{$param}{qw(smear hadron)} = ($1, aff_hadron_name_f_parse($2));
        @{$param}{qw(src_snk_dt op_spec)} = (sprintf('%d', $3), $4);
        return 0;
    } else { return 1 }
}

# compose AFF op keypath from parameters
# usage:  \%param
sub aff_op_keypath($) {
    my ($param) = @_;
    !have_unset_keys($param, qw(cfg_n smear qmass)) 
        or die "have unset keys";
    return sprintf('/cfg%s/op/%s/%s',
        @{$param}{qw(cfg_n smear qmass)});
}

# convert a list of BB files to 
# usage: $out-file  \@bbfile_list  \%param  \&parse_bbfile_name $strip_bb-param
sub convert_bbfile_list($$$$@) {
    my ($aff_bbfile, $bbfile_list, $param, $parse_bbfile_name, @cmdline_arg) = @_;
    my $cmdline_arg = join(' ', @cmdline_arg);
    debug_print("$aff_bbfile '$cmdline_arg'");
    !have_unset_keys($param, qw(out_log err_log
                                latsize_x latsize_y latsize_z latsize_t
                                src_x src_y src_z src_t
                                snk_px snk_py snk_pz snk_t
                                hadron))
        or die 'have unset keys';
    my ($out_log, $err_log) = @{$param}{qw(out_log err_log)};
    foreach my $bbfile (@{$bbfile_list}) {
        my %param = %{$param};  # private copy of param
        {   my %save_param = %param;
            $parse_bbfile_name->(basename($bbfile), \%param);
            die "mismatch in parsed parameters"
                if (compare_existing_keys(\%save_param, \%param));
        }
    }
    my $bbconv_cmd = "| $STRIP_BB $cmdline_arg -iv -f - -o $aff_bbfile " .
                     sprintf('-G %d,%d,%d,%d -c %d,%d,%d,%d -p %d,%d,%d,%d -b %s ',
                             @{$param}{qw(latsize_x latsize_y latsize_z latsize_t)},
                             @{$param}{qw(src_x src_y src_z src_t)},
                             @{$param}{qw(snk_px snk_py snk_pz snk_t)},
                             aff_hadron_name($param->{'hadron'})) .
                     " >>$out_log 2>>$err_log";
    debug_print($bbconv_cmd);
    open(bbconv_stream, $bbconv_cmd)
        or die "$STRIP_BB: cannot start job";
    foreach my $bbfile (@{$bbfile_list}) {
        my $aff_bbkey = aff_bb_keypath($param);
        print bbconv_stream "$aff_bbkey $bbfile\n";
        die "$STRIP_BB: error $?" 
            if ($?);
    }
    close(bbconv_stream)
        or die "$STRIP_BB: error finishing job";
}

# calculate operators from AFF BB file, store to AFF OP file
#   one cfg
#   many op_spec
#   one hadron
#   one source-sink pair
# currently, only twist 2 up to rank 3 operators are supported
# usage:  aff-op-file  aff-bb-file  \%param  q-list-file  $calc_op_param
sub aff_calc_operators($$$$@) {
    my ($aff_opfile, $aff_bbfile, $param, $q_list, @cmdline_arg) = @_;
    my $cmdline_arg = join(' ', @cmdline_arg);
    debug_print("$aff_bbfile  $aff_opfile  $q_list '$cmdline_arg'");
    !have_unset_keys($param, qw(cfg_n smear qmass 
                                latsize_x latsize_y latsize_z latsize_t
                                src_x src_y src_z src_t 
                                snk_px snk_py snk_pz snk_t
                                out_log err_log))
        or die 'have unset keys';
    invoc_shell("$CALC_OP $cmdline_arg -123 -q $q_list " .
                sprintf(' -G %d,%d,%d,%d -c %d,%d,%d,%d -p %d,%d,%d,%d -b %s ', 
                        @{$param}{qw(latsize_x latsize_y latsize_z latsize_t)},
                        @{$param}{qw(src_x src_y src_z src_t)},
                        @{$param}{qw(snk_px snk_py snk_pz snk_t)},
                        aff_hadron_name($param->{'hadron'})) .
                sprintf(' -i %s %s -o %s %s ',
                        $aff_bbfile, aff_bb_keypath($param),
                        $aff_opfile, aff_op_keypath($param)) .
                sprintf(' >>%s 2>>%s', @{$param}{qw(out_log err_log)})
               );
}

# split an AFF OP file by operator type
#   many op_spec -> one op_spec
# currently, only twist 2, up to rank 3, both helicity operators are supported
# usage:  $in_op_file  $out_dir  \%param 
sub aff_split_operators_opspec($$$) {
    my ($in_op_file, $out_dir, $param) = @_;
    debug_print("$in_op_file -> $out_dir");
    !have_unset_keys($param, qw(out_log err_log cfg_n smear op_spec))
        or die 'have unset keys';
    my $smear_key = $param->{'smear'};
    my $op_key = $param->{'op_spec'};
    
    my @key_list = map({ s/:.*$//; $_}
                       split(/\n+/, 
                             invoc_shell("$LHPC_AFF ls -kR $in_op_file")));
        
    my ($out_log, $err_log) = @{$param}{qw(out_log err_log)};
    
    my $out_op_file = "$out_dir/$param->{'op_spec'}." . basename($in_op_file);
    my $aff_cp_cmd = "| $LHPC_AFF cp -Rf - $in_op_file $out_op_file >>$out_log 2>>$err_log";
    debug_print($aff_cp_cmd);
    open(aff_cp_stream, $aff_cp_cmd)
        or die "$LHPC_AFF: cannot start job";
    foreach my $key (grep(/\/$smear_key\//, 
                          grep(/\/$op_key$/,
                               @key_list))) {
        print aff_cp_stream "$key $key\n";
        die "$LHPC_AFF: error $?" 
            if ($?);
    }
    close(aff_cp_stream)
        or die "$LHPC_AFF: error finishing job";
}

# join all like AFF OP files in the directory, put output to other dir
# currently support twist 2, rank up to 3, both helicity operators
# usage:  in-dir  out_op_file  \%param  \@op_spec_list
sub aff_join_operators_opspec($$$) {
    my ($in_dir, $out_op_file, $param) = @_;
    debug_print("$in_dir -> $out_op_file");
    !have_unset_keys($param, qw(out_log err_log hadron op_spec))
        or die 'have unset keys';
    my ($out_log, $err_log) = @{$param}{qw(out_log err_log)};
    my $op_spec = $param->{'op_spec'};
    my $had_patt = sprintf('_%s', aff_hadron_name_f($param->{'hadron'}));
    my @file_list = split(/\n+/, 
                          invoc_shell("ls $in_dir"));
    my $aff_merge_cmd = "|$LHPC_AFF insert -f - -o $out_op_file >>$out_log 2>>$err_log";
    debug_print($aff_merge_cmd);
    open(aff_merge_stream, $aff_merge_cmd)
        or die "$LHPC_AFF: cannot start job";
    foreach my $file (grep(/^$op_spec\./ && /$had_patt/,
                           @file_list)) {
        print aff_merge_stream "/ $in_dir/$file /\n";
        die "$LHPC_AFF: error $?"
            if ($?)
    }
    close(aff_merge_stream)
        or die "$LHPC_AFF: error finishing job";
}

sub aff_hspec_filename($) {
    my ($param) = @_;
    !have_unset_keys($param, qw(cfg_n src_x src_y src_z src_t))
        or die 'have unset keys';
    return sprintf('cfg%s_hadspec_x%dy%dz%dt%d.aff', 
                   @{$param}{qw(cfg_n)},
                   @{$param}{qw(src_x src_y src_z src_t)});
}
sub aff_hspec_hadron_filename($) {
    my ($param) = @_;
    !have_unset_keys($param, qw(cfg_n smear hadron))
        or die 'have unset keys';
    return sprintf('cfg%s_hadspec_%s_%s.aff', 
                   @{$param}{qw(cfg_n smear)},
                   aff_hadron_name_f($param->{'hadron'}));
}
sub aff_hspec_hadron_ens_filename($) {
    my ($param) = @_;
    !have_unset_keys($param, qw(smear hadron))
        or die 'have unset keys';
    return sprintf('hadspec_%s_%s.aff', 
                   @{$param}{qw(smear)},
                   aff_hadron_name_f($param->{'hadron'}));
}

# split AFF hadron file
#   many hadrons -> one hadron
#   many source sink pairs
# usage:  $in_hspec_file  $out_dir  \%param
sub aff_split_hadspec_nucleon($$$) {
    my ($in_hspec_file, $out_dir, $param) = @_;
    debug_print("$in_hspec_file -> $out_dir");
    !have_unset_keys($param, qw(hadron smear out_log err_log))
        or die 'have unset keys';
    
    my @key_list = map({ s/:.*$//; $_ }
                       split(/\n+/, 
                             invoc_shell("$LHPC_AFF ls -kR $in_hspec_file")));
    
    my ($out_log, $err_log) = @{$param}{qw(out_log err_log)};
    my $had_key = aff_hadron_name($param->{'hadron'});
    my $smear_key = $param->{'smear'};
    
    my $out_hspec_file = "$out_dir/$param->{'hadron'}.$param->{'smear'}." . 
                         basename($in_hspec_file);
    my $aff_cp_cmd = 
        "| $LHPC_AFF cp -Rf - $in_hspec_file $out_hspec_file >>$out_log 2>>$err_log";
    debug_print($aff_cp_cmd);
    open(aff_cp_stream, $aff_cp_cmd)
        or die "$LHPC_AFF: cannot start job";
    foreach my $key (grep(/\/$smear_key\//, 
                          grep(/\/$had_key$/, 
                               @key_list))) {   # select hadron
        print aff_cp_stream "$key $key\n";
        die "$LHPC_AFF: error $?"
            if ($?);
    }
    close(aff_cp_stream)
        or die "$LHPC_AFF: error finishing job";
}

# join AFF hadron file
#   many source sink pairs
# usage:  $in_dir  $out_hspec_file  \%param
sub aff_join_hadspec_nucleon($$$) {
    my ($in_dir, $out_hspec_file, $param) = @_;
    debug_print("$in_dir -> $out_hspec_file");
    !have_unset_keys($param, qw(hadron smear out_log err_log))
        or die 'have unset keys';
    my @file_list = split(/\n+/, invoc_shell("ls $in_dir"));
    my $in_hspec_pattern = sprintf('^%s\.%s\.', @{$param}{qw(hadron smear)});
    
    my ($out_log, $err_log) = @{$param}{qw(out_log err_log)};
    my $aff_merge_cmd = "| $LHPC_AFF insert -f - -o $out_hspec_file >>$out_log 2>>$err_log";
    debug_print($aff_merge_cmd);
    open(aff_merge_stream, $aff_merge_cmd)
        or die "$LHPC_AFF: cannot start job";
    foreach my $in_hspec_file (grep(/$in_hspec_pattern/, @file_list)) {
        print aff_merge_stream "/ $in_dir/$in_hspec_file /\n";
        die "$LHPC_AFF: error $?"
            if ($?);
    }
    close(aff_merge_stream)
        or die "$LHPC_AFF: error finishing job";
}
    
# read hash from file in the form
#<key>=value
# ...
# usage: $0  config_file_name   \%config
# return: %config
sub read_hash_from_file($$) {
    my ($cfg_file_name, $config) = @_;
    open(cfg_file, "<$cfg_file_name") 
        or die "$cfg_file_name: cannot open\n";
    while (<cfg_file>) {
        chomp;
        if (/^#/) { next }
        if (/^\s*($id_pattern)="([^"]*)"/) { $config->{$1}=$2; next }
        if (/^\s*($id_pattern)='([^']*)'/) { $config->{$1}=$2; next }
        if (/^\s*($id_pattern)=(\S*)/) { $config->{$1}=$2; next }
        die "$cfg_file_name: syntax error in $_";
    }
    close cfg_file;
    return $config;
}

# read list from file; elements are separated by any whitespace
sub read_list_from_file($) {
    my $file = shift;
    open(FI, $file) || die "cannot open $file";
    local $/=undef;
    my @res = split(/\s+/, <FI>);
    close(FI);
    return @res;
}

1;
