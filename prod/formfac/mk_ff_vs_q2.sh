#!/bin/bash

# default settings
OP_LIST='
tensor1
pstensor1
tensor2s2
pstensor2s2
tensor3s3
pstensor3s3'
FLAV_LIST="U D U-D U+D P N"

if [ $# -lt 2 ] ; then 
  echo -e "Usage:\n$(basename $0)  <config>  <out_dir>  [<PARAM1>=<val1> ...]"
  exit 1
fi
config=$1
out_dir=$2

#load config
. $config || { echo "cannot source config file '$config'" >&2 ; exit 1 ; }

# command line settings override config and default
shift 2
for param in "$@" ; do
  echo "Setting '$param'"
  eval $param || exit 1
done

function conv_to_comma_list() {
  [ $# -ge 1 ] || return 0
  local list=$1
  shift
  while [ $# -gt 0 ] ; do 
    list="$list,$1"
    shift
  done
  echo "$list"
}

pltx_list=$(conv_to_comma_list $PLATEAU_3PT)

for op in $(echo $OP_LIST) ; do
  for flav in $(echo $FLAV_LIST) ; do
    fname=${out_dir}/${op}/${flav}
    pytsv -se 'plt=['$pltx_list']; show(_[:,:,plt].sum(axis=2)[:,newaxis]/len(plt))' <${fname} \
      |tee ${fname}-pltx \
      |pytsv -pse 'show(join(3,avg(_),var_jk(_)))' \
      |tee ${fname}-pltx-av \
      |pytsv -pse 'show(join(3,a,_))' ${out_dir}/q2_list \
      >${fname}-pltx-av-q2 \
      || exit 1
    pytsv -se 'show(join(3,avg(_),var_jk(_)))' <$fname >${fname}-av \
      || exit 1
  done
done  
