#!/bin/bash
cnt=0
if [ $# -lt 1 ] ; then
  while read f ; do
    printf '%s\t%s\n' "$f" $(ls $f|wc -l)
    cnt=$((cnt+1))
  done
else
  for f in $@ ; do
    printf '%s\t%s\n' "$f" $(ls $f|wc -l)
  done
fi

if [ "$cnt" -eq 0 ] ; then
  echo >&2 -ne "Usage:\n$(basename $0)  <dir1>  ...\necho <dir> ... |$(basename $0)\n"
  exit 1
fi
