#!/bin/bash

if [ $# -ne 1 ] ; then
  cat >&2 <<EOF
Usage:
$(basename $0)  <logdir>

EOF
  exit 1
fi

logdir=$1
all_done=""
cnt_tot=0
cnt_fail=0
for f in $(ls $logdir/stdout.j*) ; do
  cnt_tot=$((cnt_tot + 1))
  grep -q 'DONE GANGJOB' $f && continue
  cnt_fail=$((cnt_fail + 1))
  echo $f
  all_done=1
done

echo "$cnt_tot checked, $cnt_fail not finished"

[ "x$all_done" == "x" ]
