#!/bin/bash

do_overwrite=""
do_dryrun=""
do_cksum=""
file_list=""
file_list_rel=""
while [[ $1 == --* ]] ; do
  case "$1" in
  --ow)   do_overwrite="yes" ;;
  --dry-run) do_dryrun="yes" ;;
  --cksum) do_cksum="yes" ;;
  --list=*) 
    file_list=$(echo $1 |sed -e 's?--list=??') ;;
  --list-rel=*) 
    file_list=$(echo $1 |sed -e 's?--list-rel=??')
    file_list_rel="yes" ;;
  *) echo >&2 "'$1': unknown option" ; exit 1 ;; 
  esac
  shift
done

if [ $# -lt 2 ] ; then 
  cat >&2 <<EOF 
Usage:
$(basename $0)  
    [--ow]  [--dry-run]  [--cksum]
    [--list=<file-list> | --list-rel=<file-list-rel>]
    <in-dir>  <out-dir>
EOF
  exit 1
fi

src_dir="$1"
if [ ! -d "$src_dir" ] ; then 
  echo >&2 "$src_dir: not a directory"
  exit 1
fi

dst_dir="$2"
mkdir -p $dst_dir || {
  echo >&2 "$dst_dir: cannot mkdir"
  exit 1
}
#if [ ! -d "$dst_dir" ] ; then 
#  echo >&2 "$dst_dir: not a directory"
#  exit 1
#fi
echo "DCCP-R  '$src_dir'  ->  '$dst_dir'"

#debug_prefix="echo "
if [ "x$do_dryrun" == "xyes" ] ; then
  debug_prefix="echo "
fi

cksum_suffix="__CRC_ADLER32"
function make_list() {
  if [ "x$file_list" != "x" ] ; then
    if [ ! -f "$file_list" ] ; then
      echo >&2 "$file_list: not found"
      return 1
    fi
    local prefix=""  
    if [ "x$file_list_rel" == "xyes" ] ; then 
      prefix="$src_dir/"
    fi
    cat $file_list |grep -v "$cksum_suffix"'$' |sed -e 's?^?'"$prefix"'?'
  else
    find $src_dir -type f |grep -v "$cksum_suffix"'$'
  fi
}

for src_file in $(make_list) ; do
  src_file_rel=${src_file##$src_dir}
  if [ ! -f $src_file ] ; then
    echo "$src_file_rel: missing at source; skip"
    continue
  fi
  if [ "x$do_cksum" == "xyes" ] ; then
    $debug_prefix ecrc $src_file |sed -e 's?^CRC ??' >$src_file_crc &
  fi
  dst_file="$dst_dir/$src_file_rel"
  src_file_crc="${src_file}__CRC_ADLER32"
  mkdir -p $(dirname $dst_file)
  if [ -f $dst_file ] ; then
    if [ "x$do_overwrite" == "xyes" ] ; then
      echo "$dst_file: already exists ; overwriting"
      $debug_prefix rm -f $dst_file
    else 
      echo "$dst_file: already exists ; skipping"
      continue
    fi
  fi
  echo "DCCP  ${src_file_rel#/}  ->  $(dirname $dst_file)/"
  $debug_prefix dccp $src_file $dst_file
done
