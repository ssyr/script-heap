#!/bin/bash
function print_usage() {
  cat >&2 <<EOF
Usage:
$(basename $0)  <Nvol>[:<stride>]
(read input from stdin)
EOF
}

if [ $# -lt 1 ] ; then
  print_usage
  exit 1
fi

while [[ $1 == --* ]] ; do
  case "$1" in 
  *) echo >&2 "*** Unknown option '$1'" ; print_usage ; exit 1 ;;
  esac
  shift
done

nvol=$1
nstride=1
if [[ "$nvol" == *:* ]] ; then
  IFS=": " read nvol nstride <<<"${nvol}"
fi
echo "TEST $nvol:$nstride"

nvol=$1
if [ "$nvol" -lt 1 ] ; then
  echo >&2 "nvol must be positive"
  exit 1
fi

while read f ; do
  for n in $(seq 0  $nstride  "$((nvol-1))" ) ; do
    v=$(printf "vol%04d" $n)
    echo $(dirname $f)/$v/$(basename $f)
  done
done
