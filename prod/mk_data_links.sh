#!/bin/bash

[ $# -eq 3 ] || {
  cat >&2 <<EOF
$(basename $0) <src-dir>  <dst-dir>  <cfg-list>
    src-dir   path RELATIVE to <dst-dir>/CFG
    dst-dir   make links in <dst-dir>/CFG
    cfg-list  list of CFG

EOF
  exit 1
}

srcdir=$1
dstdir=$2
cfglist=$3
V=

# make file list
flist=$(readlink -f $(mktemp))
dc0="$dstdir/$(head -n1 $cfglist)"
(mkdir -p $dc0 && cd $dc0 && ls $srcdir) >$flist
echo $flist
for c in $(cat $cfglist) ; do 
  dc="$dstdir/$c"
  ( mkdir $V -p $dc && cd $dc && pwd && \
    for f in $(cat $flist|grep '\.'"$c"'\.') ; do
      ln $V -s "$srcdir/$f" .
    done )
done

rm $flist
