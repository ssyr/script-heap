#!/bin/bash
function print_usage {
  cat >&2 <<EOF
Usage:
$1  <dir1> ...
EOF
}
echo $#
if [[ $# -le 0 ]] ; then
  print_usage $1
  exit 1
fi

for bd in $* ; do 
  echo "-----------------------------------"
  for d in $(find $bd -type d |grep anal |grep -v bin) ; do 
    echo $d
    du -sh $d
    ls -lhrS $d |grep -v total |grep '^-' |head -n5
    ls -lhS $d  |grep -v total |grep '^-' |head -n5
    echo
  done
done
