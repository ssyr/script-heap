#!/bin/bash


function print_usage() {
  cat <<EOF
Usage:
$(basename $0)  
    [--stride=<n>]  
    [--log-src=<log-src>  --log-dst=<log-dst>]   
    <nvol>  <src-dir>  <dst-dir>
EOF
}

# 1:ivol
function volstr() { printf "vol%04d" $1 ; }

use_log=no
srclog=
dstlog=
volstride=1
while [[ $1 == --* ]] ; do
  case "$1" in 
  --stride=*)     volstride=$(echo $1 |sed -e 's?--stride=??') ;;
  --srclog=*)     srclog=$(echo $1 |sed -e 's?--srclog=??')  ; use_log=yes ;;
  --dstlog=*)     dstlog=$(echo $1 |sed -e 's?--dstlog=??')  ; use_log=yes ;;
  *) echo >&2 "*** Unknown option '$1'" ; print_usage ; exit 1 ;;
  esac
  shift
done

[ $# -ge 3 ] || {
  print_usage >&2
  exit 1
}

nvol=$1
srcdir=$2
dstdir=$3
echo "vol=$nvol[$volstride]"
echo "srcdir='$srcdir'  dstdir='$dstdir'"
echo "srclog='$srclog'  dstlog='$dstlog'  use_log='$use_log'"


if [ "x$use_log" == "xyes" ] ; then
  # check logs
  if [ "x$srclog" == "x" ] || [ "x$dstlog" == "x" ] ; then
    echo >&2 "both <srclog> and <dstlog> have to be specified"
    print_usage >&2
    exit 1
  fi

  if [ ! -f "$srclog" ] ; then 
    echo >&2 "ERROR empty srclog='$srclog'"
    exit 1
  fi
  if [ -f "$dstlog" ] ; then 
    if [ ! -w "$dstlog" ] ; then
      echo "ERROR need write perm for dstlog='$dstlog'"
      exit 1
    fi
  else 
    touch "$dstlog" || {
      echo "ERROR cannot create dstlog='$dstlog'"
      exit 1
    }
  fi
fi

#check dstdir
for n in $(seq 0 $volstride $((nvol-1)) ) ; do
  d="$dstdir/$(volstr $n)"
  if [ ! -d "$d" ] ; then
    mkdir -p "$d" || {
      echo >&2 "ERROR cannot create dstdir/v='$d'"
      exit 1
    }
  elif [ ! -w "$d" ] ; then
    echo >&2 "ERROR need write perm for dstdir/v='$d'"
    exit 1
  fi
done

# 1:basename
function mv_vol_pdir() {
  bn=$1
  for n in $(seq 0 $volstride $((nvol-1)) ) ; do
    v=$(volstr $n)
    mv -n "$srcdir/$v/$bn"  "$dstdir/$v/$bn"
  done
}

umask 0002

V=echo 
if [ "x$use_log" == "xyes" ] ; then

  for bn in $(cat "$srclog") ; do
    if ! grep -qx "$bn" "$dstlog" ; then
      $V "mv $bn"
      mv_vol_pdir "$bn"
      echo "$bn" >>"$dstlog"
      sed -i '/^'"$bn"'$/d' "$srclog"
    fi
  done

else
  for bn in $(cd "$srclog/vol0000" ; ls ) ; do
    $V "mv $bn"
    mv_vol_pdir "$bn"
  done
fi
