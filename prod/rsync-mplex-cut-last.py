#!/usr/bin/env python
from __future__ import print_function

import re
import sys

def print_usage(prg) :
    print("""Usage:
%s  <flist1>  ...

For each key, print all values except the last from <flist*>:
<key>: <value>
(used to filter `rsync-mplex*' output
""" % (prg,))


if len(sys.argv) <= 1 :
    print_usage(sys.argv[0] or 'prg')
    exit(1)

# parse 
# for each print 
last_kv = {}
for f in sys.argv[1:] :
    fi = open(f, 'r')
    for s in fi.readlines() :
        s1 = s.rstrip()
        # copy comments
        if '#' == s1[0] : 
            print(s1)
        # iterate through key:value pairs
        k, v = re.match('^([^:]+)[: ]+(.+)$', s1).groups()
        if k in last_kv :
            print("%s: %s" % (k, last_kv[k]))
        last_kv[k] = v

