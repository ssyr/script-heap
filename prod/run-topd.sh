#!/bin/bash

# usage: <cmd>  <out>  <interval>  <count>  <remote-cmd>
# example: 
# $ run_topd 'date ; top -bn1 |head -n 20' 'top.dump'  30  -1  'ssh nodeX'
function run_topd() {
  local cmd="${1:-date ; top -bn1 |head -n 20}"
  local out="${2:-$$.dump}"
  local interval="${3:-10}"
  local cnt=${4:--1}
  local remote_cmd=$5
  echo "cmd='$cmd'   out='$out'   wait='$interval'  #='$count'  remote='$remote_cmd'"
  full_cmd='cnt='"$cnt"' ; while [[ $cnt -ne 0 ]] ; do cnt=$((cnt-1)) ; '"$cmd"' ; sleep '"$interval"' ; done >'"$out"' 2>&1 </dev/null'
  echo $full_cmd
  if [[ "x$remote_cmd" == 'x' ]] ; then bash -c "$full_cmd" &
  else $remote_cmd "bash -c '$full_cmd & disown' " ; fi
}

# usage in a job startup script that runs on a headnode (must have a file with the node list)
# walltime=$WALLTIME # walltime in seconds
# interval=30 # interval between snapshots in seconds
# for node in $(cat $PBS_NODEFILE |sort |uniq) ; do
#   run_topd 'echo "# ------" ; date ; top -bn1 |head -n 30' \
#     "$work_dir/top.logs/${PBS_JOBID}.${node}.top"  $interval  $((walltile/interval))  "rsh $node"
# done
#
# cleanup of background jobs:
# for node in $(cat $PBS_NODEFILE |sort |uniq |grep -v $(hostname)); do 
#   rsh $node killall -9 bash
# done
# killall -9 bash
