#!/bin/env python
from __future__ import print_function
import sys, os
import re


def print_usage(prgname) :
    sys.stdout.write("""%s  [-h] [-c]
    -h      print this help and exit
    -c      print accumulated at the end
Expect stdin:
<Elapsed(DD-HH:MM:SS)>  <AllocNodes>  <AllocCPU>  <REST>

Print stdout:
<node*hr>  <core*hr>  # <REST>
""" %(prgname,))
    

#print(repr(sys.argv))
prgname =sys.argv[0]
if 1 < len(sys.argv) :  my_argv = list(sys.argv[1:])
else : my_argv = []

print_tot   = False
while 0 < len(my_argv) and '-' == my_argv[0][0] :
    if '-h' == my_argv[0] or '--help' == my_argv[0] : 
        print_usage(prgname)
        sys.exit(1)
    if '-c' == my_argv[0] : 
        print_tot = True
    print(my_argv[0])
    my_argv.pop(0)

def timefmt2sec(dd_hms):
    dd_hms = dd_hms.split('-')
    #print(repr(dd_hms))
    if   1 == len(dd_hms) : dd, hms = 0, dd_hms[0]
    elif 2 == len(dd_hms) : dd, hms = int(dd_hms[0]), dd_hms[1]
    else : raise ValueError(dd_hms)
    hms = hms.split(':')
    hms.reverse()
    ss, mm, hh = 0, 0, 0
    if len(hms) <= 0 or 3 < len(hms) : raise ValueError(hms)
    if 1 <= len(hms) : ss = float(hms[0])
    if 2 <= len(hms) : mm = int(hms[1])
    if 3 <= len(hms) : hh = int(hms[2])
    return ss + 60 * (mm + 60 * (hh + 24 * dd))

tot_elap_nodeh  = 0
tot_elap_coreh  = 0
for l_in in sys.stdin.readlines() : 
    l_tok   = l_in.split()
    #print(repr(l_tok))
    if len(l_tok) < 3 : raise ValueError(l_in)
    elap_h  = timefmt2sec(l_tok[0]) / 3600.
    nnodes  = int(l_tok[1])
    ncores  = int(l_tok[2])
    tot_elap_nodeh += elap_h * nnodes
    tot_elap_coreh += elap_h * ncores
    print('%13.2f\t%13.2f\t# c/n=%.2f %s' % (
            elap_h * nnodes, elap_h * ncores, 
            float(ncores)/nnodes, ' '.join(l_tok[3:])))
if print_tot :
    print("---TOTAL\n%13.2f\t%13.2f" % (tot_elap_nodeh, tot_elap_coreh))
