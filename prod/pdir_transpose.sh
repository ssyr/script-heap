#!/bin/bash

prgname=$(basename $0)
function print_usage()
{
  cat >&2  <<EOF
Usage:
$prgname  [-f]  <dir> VX VY VZ VT  [dir1] ...
  volume geometry is universal
  dir lex2rlex: current enumeration is lex., make it rev.lex.
  dir rlex2lex: current enumeration is rev.lex., make it lex.
EOF
}

if [ $# -lt 5 ] ; then
  print_usage
  exit 1
fi

# opt
force=no
VERBOSE=no
while [[ $1 == -* ]] ; do
  case "$1" in 
  -f)             force=yes ;;
  -v)             VERBOSE=yes ;;
  *)  echo >&2 "*** Unknown option '$1'" ; print_usage ; exit 1 ;;
  esac
  shift
done
echo $*

# params
transp_dir=$1
if [[ "x$transp_dir" != "xLex2Rlex" ]] && [[ "x$transp_dir" != "xRlex2Lex" ]] ; then
  print_usage
  exit 1
fi
VX=$2
VY=$3
VZ=$4
VT=$5
shift 5


# <ivol>
# rlex: nvol = (ix + VX*(iy + VY*(iz + VZ * it)))
# rev.lexicographic
f_rl_x=1
f_rl_y=$((VX))
f_rl_z=$((VX * VY))
f_rl_t=$((VX * VY * VZ))
# lex:  nvol = (((ix * VY + iy)*VZ + iz)*VT + it)
# dir.lexicographic
f_dl_x=$((VT * VZ * VY))
f_dl_y=$((VT * VZ))
f_dl_z=$((VT))
f_dl_t=1
function transpose_volname() {
  local ivol=$1
  local ix iy iz it
  if   [[ "x$transp_dir" == "xLex2Rlex" ]] ; then
    ix=$(( (ivol / f_dl_x) % VX ))
    iy=$(( (ivol / f_dl_y) % VY ))
    iz=$(( (ivol / f_dl_z) % VZ ))
    it=$(( (ivol / f_dl_t) % VT ))
    echo $(( ix * f_rl_x + iy * f_rl_y + iz * f_rl_z + it * f_rl_t ))
  elif [[ "x$transp_dir" == "xRlex2Lex" ]] ; then
    ix=$(( (ivol / f_rl_x) % VX ))
    iy=$(( (ivol / f_rl_y) % VY ))
    iz=$(( (ivol / f_rl_z) % VZ ))
    it=$(( (ivol / f_rl_t) % VT ))
    echo $(( ix * f_dl_x + iy * f_dl_y + iz * f_dl_z + it * f_dl_t ))
  else 
    print_usage
    exit 1
  fi
}

function volname()
{
  printf "vol%04d" $1
}

nvol=$(( VX * VY * VZ * VT ))
echo "NVOL=$nvol"

mvcmd="mv -v"
for d in $* ; do
  if [[ "x$VERBOSE" == "xyes" ]] ; then echo "dir=$d" ; fi
  vol_bad=no
  for ivol in $(seq 0 $((nvol-1)) ) ; do
    vn=$(volname "$ivol" )
    if [ ! -d "$d/$vn" ] ; then
      echo "$d: no volume $vn ; SKIP"
      vol_bad=yes
      break
    fi
  done
  vn=$(volname "$nvol" ) 
  if [[ "x$force" == "x" ]] && [ -d "$d/$vn" ] ; then
    echo "$d: extra volume $vn ; SKIP (use -f to force)"
    vol_bad=yes
  fi
  if [[ "x$vol_bad" != "xyes" ]] ; then 
    dtmp=$(mktemp -d -p "$d")
    if [ ! -d "$dtmp" ] ; then 
      echo>&2 "$d: cannot create tempdir" 
      continue
    fi
    for ivol in $(seq $((nvol-1)) ) ; do
      ivol2=$(transpose_volname $ivol)
      vn=$(volname $ivol)
      vn2=$(volname $ivol2)
      if [[ "x$VERBOSE" == "xyes" ]] ; then echo "$vn <> $vn2" ; fi
      d1="$d/$vn"
      d2p="$dtmp/$vn2"
      $mvcmd "$d1" "$d2p"
    done
    for ivol in $(seq $((nvol-1)) ) ; do
        vn=$(volname $ivol)
        $mvcmd  "$dtmp/$vn"  "$d/$vn"
    done
    if ! rmdir "$dtmp" ; then 
      echo "$dtmp: cannot remove temp dir"
    fi
  fi
  done
