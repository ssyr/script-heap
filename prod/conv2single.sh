#!/bin/bash

for f in $* ; do
  rm -f $f
  ./qio-build/examples/qio-convert-mesh-pfs 2 $f <layout.4448
done
