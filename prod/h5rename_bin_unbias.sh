for f in $* ; do 
  [[ "$f" =~ '-unbias.' ]] || continue
  f2=$(echo $f |sed -e 's?-unbias.?-bin.ub.?')
  mv -v $f $f2
done
