#!/bin/bash

function print_usage() {
  cat >&2 <<EOF 
Usage:
$(basename $0)  [cp|check|cksum]
    [-v | --verbose]   [--cksum | --cksum-recalc]
    [--list=<file-list> | --list-rel=<file-list-rel>]
    [--ow] [--dry-run]
    <in-dir>  <out-dir>
---
Copy or check recursively that all files in <in-dir> are present in 
<out-dir> and their versions in <in-dir> <= <out-dir> by modif.date. 
Note that the arg.order is the same as for copying to tape storage.
---
Commands:
  cksum   (re)compute checksums and print then to stdout with -v
          useful for speeding up backups
  check   compare two directories: file size, modification date,
          checksum
  cp      copy files; overwrite if size/date/checksum mismatch
Parameters:
  --cksum 
          compute/verify checksums (saved in \`\$\{file\}__CRC_ADLER32\'
          for regular files or taken from enstore for PNFS)
  --cksum-recalc
          force recomputing checksums, even if they are available
  --ow    
          force overwriting existing target files if they have different
          size, checksum, or earlier timestamp
  --dry-run
          print commands but do not do anything (works with \`cp\')
  --list=<list-of-files>
          process files from the file list
  --list-rel=<list-of-files>
          process files from the file list (paths are relative to <in-dir>)
  -v, --verbose
          print extensive information

EOF
}

#################### parse parameters ##########################
if [ $# -lt 2 ] ; then 
  print_usage
  exit 1
fi
cmd_what="$1"
shift

do_overwrite=""
do_dryrun=""
do_verbose=""
do_cksum=""
do_cksum_recalc=""
file_list=""
file_list_rel=""
while [[ $1 == -* ]] ; do
  case "$1" in
  --ow)   do_overwrite="yes" ;;
  --dry-run) do_dryrun="yes" ;;
  -v|--verbose)     do_verbose="yes" ;;
  --cksum)          do_cksum="yes" ;;
  --cksum-recalc)
    do_cksum="yes" 
    do_cksum_recalc="yes" ;;
  --list=*) 
    file_list=$(echo $1 |sed -e 's?--list=??') ;;
  --list-rel=*) 
    file_list=$(echo $1 |sed -e 's?--list-rel=??')
    file_list_rel="yes" ;;
  *) 
    echo >&2 "'$1': unknown option"
    print_usage
    exit 1 ;; 
  esac
  shift
done


#################### utility constants & functions #############
cksum_suffix="__CRC_ADLER32"
pnfs_prefix="/pnfs"

function make_list() {
  if [ "x$file_list" != "x" ] ; then
    if [ ! -f "$file_list" ] ; then
      echo >&2 "$file_list: not found"
      return 1
    fi
    local prefix=""  
    if [ "x$file_list_rel" == "xyes" ] ; then 
      prefix="$src_dir/"
    fi
    cat $file_list |grep -v "$cksum_suffix"'$' |sed -e 's?^?'"$prefix"'?'
  else
    find $src_dir -type f |grep -v "$cksum_suffix"'$'
  fi
}
# check if a file on PNFS
function is_pnfs() {
  [[ "$1" == "$pnfs_prefix"/* ]]
}
# return 0(true) if creation_date($1) <= creation_date($2)
# both files must exist
function is_not_newer() {
  [ $(stat -c '%Y' "$1") -le $(stat -c '%Y' "$src_file") ]
}
# calc CRC from a file
function calc_crc() {
  local f=$1
  [ -f $f ] || return 1
  if is_pnfs $f ; then
    enstore pnfs --xref $f |grep '^crc' |sed -e 's?crc: ??'
  else
    ecrc $f |grep '^CRC' |sed -e 's?CRC ??'
  fi
}
# get CRC from file if it has been computed, or compute it and save ; 
# if do_crc_recalc, recompute and save CRC if NOT on pnfs
function get_crc() {
  local f=$1
  [ -f $f ] || return 1
  if is_pnfs $f ; then calc_crc "$f"
  else
    local f_crc="${f}${cksum_suffix}"
    if [ "x$do_cksum_recalc" == "x" ] \
          && [ -f "$f_crc" ] && is_not_newer "$f" "$f_crc"; then
      cat $f_crc
    else
      local cksum=$(calc_crc $f)
      if [ -f $f_crc ] && is_not_newer "$f" "$f_crc" ; then
        if [ "x$cksum" != "x$(cat $f_crc)" ] ; then
          echo >&2 "$f: LOCAL checksum mismatch: stored != recomputed"
          # do not write anything to files; the user must investigate; report new checksum
          echo $cksum
          return 1
        else
          echo $cksum
          return 0
        fi
      else
        echo $cksum >$f_crc
        echo $cksum
        return 0
      fi
    fi
  fi
}
# return crc if stored in a file; otherwise, return ""
function ask_crc(){
  local f=$1
  [ -f $f ] || return 1
  if is_pnfs $f ; then return 1
  else
    local f_crc="${f}${cksum_suffix}"
    if [ -f "$f_crc" ] && is_not_newer "$f" "$f_crc" ; then
      cat $f_crc
      return 0
    else
      return 1
    fi
  fi
}



#################### MAIN ######################################
src_dir="$1"
if [ ! -d "$src_dir" ] ; then 
  echo >&2 "$src_dir: not a directory"
  exit 1
fi

case $cmd_what in
cp|check)
  dst_dir="$2"
  if [ ! -d "$dst_dir" ] ; then
    echo >&2 "$dst_dir: not a directory"
  exit 1
  fi ;;
  *) ;;
esac




case $cmd_what in 
cksum)
  [ "$do_verbose" ] && echo >&2 "CKSUM-R '$src_dir'"
  for src_file in $(make_list) ; do
    src_file_rel=${src_file##$src_dir}
    if [ ! -f $src_file ] ; then
      echo "$src_file_rel: missing at source; skip"
      continue
    fi
    src_cksum="$(get_crc $src_file)"
    [ $do_verbose ] && echo >&2 "$src_file_rel: CRC=$src_cksum"
  done
  ;;

check)
  [ "$do_verbose" ] && echo >&2 "CHECK-R  '$src_dir'  ->  '$dst_dir'"
  cnt_miss=0
  cnt_size=0
  cnt_date=0
  cnt_cksum=0
  for src_file in $(make_list) ; do
    src_file_rel=${src_file##$src_dir}
    if [ ! -f $src_file ] ; then
      echo "$src_file_rel: missing at source; skip"
      continue
    fi
    dst_file="$dst_dir/$src_file_rel"
    [ $do_verbose ] && echo >&2 "CMP  $src_file  <=>  $dst_file"
    src_file_crc="${src_file}${cksum_suffix}"
    if [ ! -f $dst_file ] ; then 
      echo "$src_file_rel: missing at the destination"
      cnt_miss=$((cnt_miss+1))
    elif [ $(stat -c '%s' "$dst_file") -ne $(stat -c '%s' "$src_file") ] ; then
      echo "$src_file_rel: size mismatch"
      cnt_size=$((cnt_size+1))
    elif ! is_not_newer "$src_file" "$dst_file" ; then # [ $(stat -c '%Y' "$dst_file") -lt $(stat -c '%Y' "$src_file") ] ; then
      echo "$src_file_rel: newer at the source"
      cnt_date=$((cnt_date+1))
    elif [ "x$do_cksum" == "xyes" ] ; then
      src_cksum="$(get_crc $src_file)"
      dst_cksum="$(get_crc $dst_file)"
      if [ "x$src_cksum" != "x$dst_cksum" ] ; then
        echo "$src_file_rel: cksum mismatch"
        cnt_cksum=$((cnt_cksum+1))
      else
        [ $do_verbose ] && echo >&2 "$src_file_rel: CRC=$src_cksum OK"
      fi
    fi
  done
  if [ $cnt_miss -ne 0 ] || [ $cnt_size -ne 0 ] || [ $cnt_date -ne 0 ] ; then
    if [ "x$do_verbose" == "xyes" ] ; then
      echo "missing:    $cnt_miss"
      echo "diff.size:  $cnt_size"
      echo "newer src:  $cnt_date"
      echo "bad cksum:  $cnt_cksum"
    fi
    exit 1
  else
    [ "x$do_verbose" == "xyes" ] && echo "COPY OK: $src_dir <= $dst_dir"
    exit 0
  fi
  ;;

cp)
  [ $do_verbose ] && echo >&2 "DCCP-R  '$src_dir'  ->  '$dst_dir'"
  for src_file in $(make_list) ; do
    src_file_rel=${src_file##$src_dir}
    if [ ! -f $src_file ] ; then
      echo "$src_file_rel: missing at source; skipping"
      continue
    fi
    [ "x$do_cksum" == "xyes" ] && src_cksum=$(get_crc $src_file)
    dst_file="$dst_dir/$src_file_rel"
    mkdir -p $(dirname $dst_file)
    if [ -f $dst_file ] ; then
      dst_cksum=$(get_crc $dst_file)
      if [ $do_overwrite ] ; then  str_do="OVERWRITING"
      else str_do="SKIPPING" ; fi
      if ! is_not_newer "$src_file"  "$dst_file" ; then
        echo "$src_file: destination exists: older than source ; $str_do"
        if [ $do_overwrite ] ; then  $debug_prefix rm -f $dst_file
        else continue ; fi
      elif [ "$src_cksum" != "$dst_cksum" ] ; then
        echo "$dst_file: destination exists: checksum mismatch $src_cksum != $dst_cksum; $str_do"
        if [ $do_overwrite ] ; then  $debug_prefix rm -f $dst_file
        else continue ; fi
      else
        echo "$dst_file: destination already exists ; skipping"
        [ $do_verbose ] && echo "$src_file_rel: CRC=$src_cksum OK"
        continue
      fi
    fi
    [ $do_verbose ] && echo >&2 "DCCP  ${src_file_rel#/}  ->  $(dirname $dst_file)/"
    $debug_prefix dccp $src_file $dst_file
  done
  ;;
*) 
  echo >&2 "$cmd_what: unknown command"
  print_usage
  exit 1
  ;;
esac
