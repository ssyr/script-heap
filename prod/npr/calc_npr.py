#
# analysys of forward vertices q=0
from __future__ import print_function

import lhpd
import lhpd.npr as npr
from lhpd.fitting import resample
import tsv
import numpy as np
from lhpd.npr import LDIM, NCOLOR, NSPIN, dirprop_shape
from lhpd.math.gamma_dgr import Gamma16_dgr
from lhpd.math.H4_tensor import *
from lhpd.misc import progbar
import types
import math


def read_prop(aff_i, mom_frw, mom_bkw, latsize, list_key, prop_key):
    n_data  = len(list_key)
    prop    = np.empty((n_data,) + dirprop_shape, np.complex128)
    
    pbar    = progbar(total=n_data, prefix='read prop')
    for i_k, k in enumerate(list_key):
        prop[i_k] = npr.aff_read_npr_prop(
                            aff_i.open(get_npr_file(k)),
                            get_npr_kpath(k, mom_frw, mom_bkw) + '/' + prop_key)
        pbar.step() ; pbar.redraw()
    pbar.finish()
    return prop
    
def read_q2vertex_deriv0(aff_i, mom_frw, mom_bkw, latsize, list_key):
    n_data  = len(list_key)
    v       = np.empty((n_data, NSPIN*NSPIN) + dirprop_shape, np.complex128)
    
    pbar    = progbar(total=n_data, prefix=('R v_d0(%s->%s)' % (str(mom_frw), str(mom_bkw))))
    for i_k, k in enumerate(list_key):
        v[i_k] = npr.aff_read_npr_2qvertex(
                        aff_i.open(get_npr_file(k)), 
                        get_npr_kpath(k, mom_frw, mom_bkw), 
                        0)
        pbar.step() ; pbar.redraw()
    pbar.finish()
    return v


def read_q2vertex_deriv1(aff_i, mom_frw, mom_bkw, latsize, list_key):
    n_data  = len(list_key)
    v       = np.empty((n_data, NSPIN*NSPIN, LDIM) + dirprop_shape, np.complex128)
    
    pbar    = progbar(total=n_data, prefix=('R v_d1(%s->%s)' % (str(mom_frw), str(mom_bkw))))
    mom     = np.asarray(mom_frw) - np.asarray(mom_bkw)
    for i_k, k in enumerate(list_key):
        v[i_k] = npr.calc_bb2dsymm_momproj(
                        npr.aff_read_npr_2qvertex(
                                aff_i.open(get_npr_file(k)), 
                                get_npr_kpath(k, mom_frw, mom_bkw), 1),
                        (1,), mom, latsize)
        pbar.step() ; pbar.redraw()
    pbar.finish()
    return v
    
def read_q2vertex_deriv2(aff_i, mom_frw, mom_bkw, latsize, list_key):
    n_data  = len(list_key)
    v       = np.empty((n_data, NSPIN*NSPIN, LDIM, LDIM) + dirprop_shape, np.complex128)
    
    pbar    = progbar(total=n_data, prefix=('R v_d2(%s->%s)' % (str(mom_frw), str(mom_bkw))))
    mom     = np.asarray(mom_frw) - np.asarray(mom_bkw)
    for i_k, k in enumerate(list_key):
        v[i_k] = npr.calc_bb2dsymm_momproj(
                        npr.aff_read_npr_2qvertex(
                                aff_i.open(get_npr_file(k)), 
                                get_npr_kpath(k, mom_frw, mom_bkw), 2),
                        (1,2), mom, latsize)
        pbar.step() ; pbar.redraw()
    pbar.finish()
    return v
    

def calc_npr_val(rhs, sigma, ret=(0,)):
    """ take the same parameters as lhpd.npr.tensor_linsolver 
        return only a subset of solutions + norm2(rhs) + norm2(resid wrt the subset)
        return: (x[ret[0]], ..., x[ret[-1]], norm2(rhs), norm2(sigma.x[ret] - rhs))
    """
    rhs     = np.asarray(rhs)
    sigma   = np.asarray(sigma)
    x, resid= npr.tensor_linsolver(rhs, sigma)
    x_ret   = x[ret,...]
    resid   = rhs - np.tensordot(x_ret, sigma[ret,...], (0,0))
    resid_abs2 = (resid*resid.conj()).sum().real
    rhs2    = (rhs * rhs.conj()).sum().real
    return tuple(x_ret) + (rhs2, resid_abs2)
def calc_npr_val2(rhs, sigma, ret=(0,)):
    """ take the same parameters as lhpd.npr.tensor_linsolver 
        return only a subset of solutions + norm2(rhs) + norm2(resid wrt the subset)
        return: (x[ret[0]], ..., x[ret[-1]], norm2(rhs), norm2(sigma.x[ret] - rhs))
    """
    rhs     = np.asarray(rhs)
    sigma   = np.asarray(sigma)
    x, resid= npr.vec_avg_linsolver_arr(rhs, sigma)
    x_ret   = x[ret,...]
    resid   = rhs - np.tensordot(x_ret, sigma[ret,...], (0,0))
    resid_abs2 = (resid*resid.conj()).sum().real
    rhs2    = (rhs * rhs.conj()).sum().real
    return tuple(x_ret) + (rhs2, resid_abs2)

    
def cplx2real(v, axis=-1):
    return np.rollaxis(np.concatenate((v.real[...,None], v.imag[...,None]), -1), -1, axis)
def cplx2real_flat(v, axis=-1):
    return np.concatenate((v.real, v.imag), axis)

def calc_npr_all(list_key, list_mom_pairs, latsize,
                 do_deriv0=True, do_cons=True, do_deriv1=True, do_deriv2=True):
    assert(np.allclose(list_mom_frw, list_mom_bkw)) # FIXME lift limitation
    N     = None
    n_mom = len(list_mom_frw)
    n_data= len(list_key)

    # [i_mom, i_ens, i_G]
    tr_invp_gamma = np.empty((n_mom, n_data, NSPIN*NSPIN), np.complex128)
    def mk_npr_vars(n_mom, n_data):
        return (np.empty((n_mom, n_data), np.complex128),
                np.empty((n_mom, n_data), np.float64),
                np.empty((n_mom, n_data), np.float64))
    z_scal, z_scal2, z_scal_r2    = mk_npr_vars(n_mom, n_data)
    z_pssc, z_pssc2, z_pssc_r2    = mk_npr_vars(n_mom, n_data)
    z_vec, z_vec2, z_vec_r2       = mk_npr_vars(n_mom, n_data)
    z_axvec, z_axvec2, z_axvec_r2 = mk_npr_vars(n_mom, n_data)
    z_sigma, z_sigma2, z_sigma_r2 = mk_npr_vars(n_mom, n_data)
    z_cvec,  z_cvec2,  z_cvec_r2  = mk_npr_vars(n_mom, n_data)
    z_caxvec,z_caxvec2,z_caxvec_r2= mk_npr_vars(n_mom, n_data)
    z_t2s2_r3d1,  z_t2s2_r3d1_2,  z_t2s2_r3d1_r2    = mk_npr_vars(n_mom, n_data)
    z_t2s2_r6d3,  z_t2s2_r6d3_2,  z_t2s2_r6d3_r2    = mk_npr_vars(n_mom, n_data)
    
    aff_i = lhpd.aff_io.aff_import(100)
    for i_mom, mp in enumerate(list_mom_pairs):
        mom_frw, mom_bkw = mp
        p_frw, p_bkw= np.asarray(mp[0]), np.asarray(mp[1])
        exp_ip_frw  = np.exp(1j * npr.mom_linear(p_frw, latsize))
        exp_ip_bkw  = np.exp(1j * npr.mom_linear(p_bkw, latsize))
        
        qmom        = p_frw - p_bkw                 # for shifts
        qmom_lin    = npr.mom_linear(qmom, latsize)
        qmom_lin_sq = (qmom_lin**2).sum()
        exp_iq      = np.exp(1j*qmom_lin)
        qmom_sine   = npr.mom_sine(qmom, latsize)
        
        pmom        = (p_frw + p_bkw) / 2.
        pmom_lin    = npr.mom_linear(pmom, latsize)
        pmom_lin_sq = (pmom_lin**2).sum()
        pmom_sine   = (+npr.mom_sine(p_frw, latsize) 
                       +npr.mom_sine(p_bkw, latsize)) / 2. # for Dsymm
        pmom_sine_sq= (pmom_sine**2).sum()
        
        dirprop_shape = (NCOLOR, NSPIN, NCOLOR, NSPIN)

        # propagators
        frw_prop = resample(read_prop(aff_i, mom_frw, mom_bkw, latsize, 
                                      list_key, prop_key=frw_prop_key), 
                            rsplan)
        bkw_prop = resample(read_prop(aff_i, mom_frw, mom_bkw, latsize, 
                                      list_key, prop_key=bkw_prop_key),
                            rsplan)
        # S^{-1}
        inv_frw_prop = np.array([ npr.inv_dirprop(p) for p in frw_prop ])
        inv_bkw_prop = np.array([ npr.inv_dirprop(p) for p in bkw_prop ])
        # tr_spin(tr_color(S^-1) . Gamma16) [i_data, i_Gamma]
        tr_invp_gamma[i_mom] = [ [ np.tensordot(p.trace(axis1=0, axis2=2), g, ((0,1), (1,0))) / 12.
                                      for g in Gamma16_dgr ]
                                  for p in inv_frw_prop ]
        if do_deriv0:    # switch
            v0 = resample(read_q2vertex_deriv0(aff_i, mom_frw, mom_bkw, latsize, list_key), rsplan)
            for i_k, k in enumerate(list_key):
                x = npr.amp_q2vertex_useinv(v0[i_k], inv_frw_prop[i_k], 
                                            inv_bkw_prop[i_k]
                                           ).trace(axis1=-4, axis2=-2) / NCOLOR
                                           
                z_scal[i_mom][i_k], z_scal2[i_mom][i_k], z_scal_r2[i_mom][i_k] = \
                    calc_npr_val(npr.G16_to_scalar(x), 
                                 [npr.G16_to_scalar(Gamma16_dgr)])
                            
                z_pssc[i_mom][i_k], z_pssc2[i_mom][i_k], z_pssc_r2[i_mom][i_k] = \
                    calc_npr_val(npr.G16_to_pseudoscalar(x), 
                                 [npr.G16_to_pseudoscalar(Gamma16_dgr)])
                            
                z_vec[i_mom][i_k], z_vec2[i_mom][i_k], z_vec_r2[i_mom][i_k] = \
                    calc_npr_val(npr.G16_to_vec(x), 
                                 [npr.G16_to_vec(Gamma16_dgr)])
                            
                z_axvec[i_mom][i_k], z_axvec2[i_mom][i_k], z_axvec_r2[i_mom][i_k] = \
                    calc_npr_val(npr.G16_to_axialvec(x), 
                                 [npr.G16_to_axialvec(Gamma16_dgr)]) 
                            
                z_sigma[i_mom][i_k], z_sigma2[i_mom][i_k], z_sigma_r2[i_mom][i_k] = \
                    calc_npr_val(npr.G16_to_sigma(x), 
                                 [npr.G16_to_sigma(Gamma16_dgr)]) 
        

        if do_cons:
            v_cvec  = np.empty((n_data, LDIM) + dirprop_shape, np.complex128)
            v_caxvec= np.empty((n_data, LDIM) + dirprop_shape, np.complex128)
            
            pbar    = progbar(total=n_data, prefix=('R v_d1(%s->%s)' % (str(mom_frw), str(mom_bkw))))
            for i_k, k in enumerate(list_key):
                # [iGamma, mu/mu_inv, ...]
                l1      = npr.aff_read_npr_2qvertex(
                                        aff_i.open(get_npr_file(k)), 
                                        get_npr_kpath(k, mom_frw, mom_bkw), 1)
                for mu in range(LDIM):
                    v_cvec[i_k][mu]     = (
                            +(l1[0, mu] + l1[1<<mu, mu])/2. * exp_iq[mu]
                            -(l1[0, LDIM+mu] - l1[1<<mu, LDIM+mu])/2. )
                    v_caxvec[i_k][mu]   = (-1)**mu * (
                            +(l1[15, mu] + l1[15 - (1<<mu), mu])/2. * exp_iq[mu]
                            -(l1[15, LDIM+mu] - l1[15 - (1<<mu), LDIM+mu])/2. )
                pbar.step() ; pbar.redraw()
            pbar.finish()

            v_cvec  = resample(v_cvec)
            v_caxvec= resample(v_caxvec)
            
            a_cvec  = (
                +(Gamma16_dgr[0] + npr.G16_to_vec(Gamma16_dgr))/2. * exp_ip_bkw.conj()[:,N,N]
                -(Gamma16_dgr[0] - npr.G16_to_vec(Gamma16_dgr))/2. * exp_ip_frw[:,N,N] )
            a_caxvec= (
                +(Gamma16_dgr[15] + npr.G16_to_axialvec(Gamma16_dgr))/2. * exp_ip_bkw.conj()[:,N,N]
                -(Gamma16_dgr[15] - npr.G16_to_axialvec(Gamma16_dgr))/2. * exp_ip_frw[:,N,N] )
            
            for i_k, k in enumerate(list_key):
                x_cvec  = npr.amp_q2vertex_useinv(v_cvec[i_k], inv_frw_prop[i_k], 
                                            inv_bkw_prop[i_k]
                                           ).trace(axis1=-4, axis2=-2) / NCOLOR
                z_cvec[i_mom][i_k], z_cvec2[i_mom][i_k], z_cvec_r2[i_mom][i_k] = \
                    calc_npr_val(x_cvec, [a_cvec])
                
                x_caxvec= npr.amp_q2vertex_useinv(v_caxvec[i_k], inv_frw_prop[i_k], 
                                            inv_bkw_prop[i_k]
                                           ).trace(axis1=-4, axis2=-2) / NCOLOR
                z_caxvec[i_mom][i_k], z_caxvec2[i_mom][i_k], z_caxvec_r2[i_mom][i_k] = \
                    calc_npr_val(x_caxvec, [a_caxvec])
            
            
        if do_deriv1:
            v1 = resample(read_q2vertex_deriv1(aff_i, mom_frw, mom_bkw, latsize, list_key), rsplan)
            #p1  = (pmom_sine + pmom_lin) / 2.
            p1  = pmom_sine
            p2  = p1
            p3  = p1
            p2p3= (p1*p2).sum()
            mom_gamma = np.tensordot(npr.G16_to_vec(Gamma16_dgr), p1, (0,0))
            a_t2s2 = [ npr.G16_to_vec(Gamma16_dgr)[:,N,:,:] * 1j*p1[N,:,N,N], 
                       #mom_gamma[N,N,:,:] * p2[:,N,N,N] * 1j*p3[N,:,N,N] / p2p3
                     ]
            for i_k, k in enumerate(list_key):
                x = npr.amp_q2vertex_useinv(v1[i_k], inv_frw_prop[i_k], 
                                            inv_bkw_prop[i_k]
                                           ).trace(axis1=-4, axis2=-2) / NCOLOR
                z_t2s2_r3d1[i_mom][i_k], z_t2s2_r3d1_2[i_mom][i_k], z_t2s2_r3d1_r2[i_mom][i_k] =\
                    calc_npr_val2(H4_T2_d3r1(npr.G16_to_vec(x)), 
                                 [ H4_T2_d3r1(a) for a in a_t2s2] )
                z_t2s2_r6d3[i_mom][i_k], z_t2s2_r6d3_2[i_mom][i_k], z_t2s2_r6d3_r2[i_mom][i_k] =\
                    calc_npr_val2(H4_T2_d6r3(npr.G16_to_vec(x)), 
                                 [ H4_T2_d6r3(a) for a in a_t2s2] )

        if do_deriv2:
            v2 = resample(read_q2vertex_deriv2(aff_i, mom_frw, mom_bkw, latsize, list_key), rsplan)
            for i_k, k in enumerate(list_key):
                x = npr.amp_q2vertex_useinv(v2[i_k], inv_frw_prop[i_k], 
                                            inv_bkw_prop[i_k]
                                           ).trace(axis1=-4, axis2=-2) / NCOLOR
            
        continue # TODO implement the rest


    # saving all
    tsv.write_format(open('zprop', 'w'), 
                     cplx2real_flat(tr_invp_gamma.transpose(1,0,2))[:,N,:,:])
    def write_npr_v(z, z_file, z2, zr2,  zr_file):
        print(z.shape, z2.shape, zr2.shape)
        tsv.write_format(open(z_file, 'w'), cplx2real(z.transpose(1,0))[:,N,:,:])
        tsv.write_format(open(zr_file, 'w'), 
                         np.concatenate((z2[...,N], zr2[...,N]),-1
                                       ).transpose(1,0,2)[:,N,:,:])
    # write vertices
    if do_deriv0:
        write_npr_v(z_scal,  'z_scal',  z_scal2,  z_scal_r2,  'zr_scal')
        write_npr_v(z_pssc,  'z_pssc',  z_pssc2,  z_pssc_r2,  'zr_pssc')
        write_npr_v(z_vec,   'z_vec',   z_vec2,   z_vec_r2,   'zr_vec')
        write_npr_v(z_axvec, 'z_axvec', z_axvec2, z_axvec_r2, 'zr_axvec')
        write_npr_v(z_sigma, 'z_sigma', z_sigma2, z_sigma_r2, 'zr_sigma')
        
    if do_cons:
        write_npr_v(z_cvec,  'z_cvec',  z_cvec2,  z_cvec_r2,  'zr_cvec')
        write_npr_v(z_caxvec,'z_caxvec',z_caxvec2,z_caxvec_r2,'zr_caxvec')
        
    
    if do_deriv1:
        write_npr_v(z_t2s2_r3d1, 'z_t2s2r3d1', z_t2s2_r3d1_2, z_t2s2_r3d1_r2, 'zr_t2s2r3d1')
        write_npr_v(z_t2s2_r6d3, 'z_t2s2r6d3', z_t2s2_r6d3_2, z_t2s2_r6d3_r2, 'zr_t2s2r6d3')

    if do_deriv2:
        pass
   
    
def write_list_mom(list_mom_pairs, latsize, f):
    """ write mom list : 
        p2_linear  p2_sine   kx ky kz kt  px py pz pt
    """
    list_mom = [ np.array(mp[0]) for mp in list_mom_pairs ]
    n_mom = len(list_mom)
    tsv_mom = np.empty((n_mom,10), np.float64)
    tsv_mom[:,0]    = [ npr.momsq_linear(m, latsize) for m in list_mom ]
    tsv_mom[:,1]    = [ npr.momsq_sine(m, latsize) for m in list_mom ]
    tsv_mom[:,2:6]  = list_mom
    tsv_mom[:,6:10] = [ npr.mom_linear(m, latsize) for m in list_mom ]
    tsv.write_format(open(f, 'w'), tsv_mom[None,None])

if __name__ == '__main__':
    assert(np.array(latsize).shape == (LDIM,)
       and np.array(list_mom_frw).shape[1:] == (LDIM,)
       and np.array(list_mom_bkw).shape[1:] == (LDIM,)
       and len(list_mom_frw) == len(list_mom_bkw)
       and type(list_key) == types.ListType
       and type(get_npr_file) == types.FunctionType 
       and type(get_npr_kpath) == types.FunctionType 
       and type(frw_prop_key) == types.StringType
       and type(bkw_prop_key) == types.StringType
       and type(rsplan) == types.TupleType
       )
    write_list_mom(list_mom_pairs, latsize, 'list_mom')
    calc_npr_all(list_key, list_mom_pairs, latsize)
