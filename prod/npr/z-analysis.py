from __future__ import print_function
import lhpd
import tsv
import numpy as np
import scipy as sp
import math

import matplotlib as mpl
import matplotlib.pyplot as plt
from dataview import *
from dataview.config_mpl import style_group
from lhpd import spprint_ve

import pdb

rsplan      = ('jk',1)
zpert_dir   = '/Users/syritsyn/Mathematica/QCD/renorm_RIp'

#pi_vec      = np.array(tsv.read_float(open('z_vec')))[:,0,:,0]
#pi_t2s2r3d1 = np.array(tsv.read_float(open('z_t2s2r3d1')))[:,0,:,0]
#pi_t2s2r6d3 = np.array(tsv.read_float(open('z_t2s2r6d3')))[:,0,:,0]

stg  = [
    style_group((0,0,1),   '-', 'o'),
    style_group((.7,0,0),  '-', 's'),
    style_group((0,.4,0),  '-', 'd'),
    style_group((0,.7,.7), '-', 'x'),
    style_group('#808000', '-', '+'),
    style_group('#e000d0', '-', 'h'),

    style_group('#ff6600', '-', 'v'),
    style_group('#7d26cd', '-', '^'),
    style_group('#b7410e', '-', '>'),
    style_group('#990066', '-', '<'),

    style_group('#000000', '-', '*', alpha_shade=.2)    # default black star
    ]

def get_zx(q2_name, zvec_name, zx_name, zx_name_pert, ainv, rsplan):
    q2list  = np.array(tsv.read_float(open(q2_name)))[0,0,:,0]
    pi_vec  = np.array(tsv.read_float(open(zvec_name)))[:,0,:,0]
    pi_x    = np.array(tsv.read_float(open(zx_name)))[:,0,:,0]
    zx      = pi_vec / pi_x
    zx_pert_func = np.array(tsv.read_float(open(zx_name_pert)))[0,0]
    zx_pert = np.interp(np.sqrt(q2list)*ainv, zx_pert_func[:,0], zx_pert_func[:,1])
    zx_si   = zx / zx_pert
    zx_si_avg, zx_si_err = lhpd.calc_avg_err(zx_si, rsplan)
    return q2list * ainv**2, zx_si_avg, zx_si_err

def show_zx(q2_name, zvec_name, zx_name, zx_name_pert, ainv, rsplan, stg):
    q2list, zx_si_avg, zx_si_err = get_zx(q2_name, zvec_name, zx_name, zx_name_pert, ainv, rsplan)
    fig     = plt.figure(figsize=[10.,8.])
    ax      = fig.add_axes([.1,.1,.8,.8])
    print(zx_si_avg.shape, zx_si_err.shape, stg.dot())
    ax.errorbar(q2list, zx_si_avg, yerr=zx_si_err, **stg.dot())
    return fig

"""
fig = show_zx('list_mom', 'z_vec', 'z_sigma', zpert_dir + '/zp_ZWilsonTC', ainv, rsplan, stg[0])

"""

def get_zname_pert(zname):
    return {
        'z_t2s2r3d1'    : 'zp_ZWilsonT2N2',
        'z_t2s2r6d3'    : 'zp_ZWilsonT2N2',
        'z_sigma'       : 'zp_ZWilsonTC',
        } [zname]

def show_comp(ens_list, zname, ainv,
        zpert_dir   = '/Users/syritsyn/Mathematica/QCD/renorm_RIp',
        rsplan      = ('jk', 1),
        ax          = None):
    """
        ens_list    [(path, label, stg), ]
        zname       z_sigma/z_t2s2r3d1/z_t2s2r6d3
        assumes rsplan=('jk', 1) and preset zpert_dir
    """
    if None == ax:
        fig = plt.figure(figsize=[10.,8.])
        ax  = fig.add_axes([.1,.1,.8,.8])
    for e in ens_list:
        path, label, stg = e
        if 0 < len(path): path = path + '/'
        q2, z_a, z_e = get_zx(path + 'list_mom', path + 'z_vec', 
                path + zname, zpert_dir + '/' + get_zname_pert(zname), ainv, rsplan)
        ax.errorbar(q2, z_a, yerr=z_e, label=label, **stg.dot())

    ax.legend()
    ax.set_xlim([0, None])
    ax.set_xlabel(r'$\mu^2$')
    ax.set_ylabel(r'$Z_\text{SI}$')

    return ax
    

def linreg(a, y, yerr=None, ycov=None):
    """ solve for x: a.x = y 
        a       matrix
        yerr    y errorbars
        ycov    y covariance (only one of yerr,ycov must be given)
        return: x, xcov:
            x = (aT . ycov^-1 . a)^-1 . (aT . ycov^-1) . y
            xcov = (aT . ycov^-1 . a)^-1
        if neither yerr,ycov is given, compute xcov so that chi^2 is normalized to #dof
    """
    a       = np.asarray(a)
    y       = np.asarray(y)

    m,n = a.shape
    assert m == y.shape[0]
    assert n <= m
    assert yerr == None or ycov == None

    no_err      = False
    if yerr != None:    ycov_inv = np.diag(1. / np.asarray(yerr)**2)
    elif ycov != None:  ycov_inv = np.linalg.inv(np.asarray(ycov))
    else: 
        no_err  = True
        ycov_inv= np.identity(m, int)

    aTcI    = np.dot(a.T, ycov_inv)
    sysm    = np.dot(aTcI, a)
    sysm_inv= np.linalg.inv(sysm)
    
    x       = np.dot(sysm_inv, np.dot(aTcI, y))
    resid   = y - np.dot(a, x)
    chi2    = np.dot(np.dot(y, ycov_inv), resid)

    xcov    = sysm_inv
    if no_err and n < m:
        n_dof = m - n
        assert 0 < chi2
        xcov *= chi2 / n_dof

    return x, xcov

def linreg_maxdiff(a, xcov):
    return np.sqrt(np.array([ np.dot(a_i, np.dot(xcov, a_i)) for a_i in a ]))


def show_extrap_mq0(ens_list, zname, ainv,
        zpert_dir   = '/Users/syritsyn/Mathematica/QCD/renorm_RIp',
        rsplan      = ('jk', 1),
        ax          = None,
        show_mq     = True,
        q2_range    = None,
        stg_mq0     = stg[-1]):
    """
        ens_list    [(path, label, stg, const?*mq), ]
        zname       z_sigma/z_t2s2r3d1/z_t2s2r6d3
        assumes rsplan=('jk', 1) and preset zpert_dir
    """
    if None == ax:
        fig = plt.figure(figsize=[10.,8.])
        ax  = fig.add_axes([.1,.1,.8,.8])

    n_ens   = len(ens_list)
    #pdb.set_trace()

    # collect mq, z, zerr from ensembles
    mq_list, q2_list, z_a_list, z_e_list = [], [], [], []
    for e in ens_list:
        path, label, stg, mq = e
        if 0 < len(path): path = path + '/'
        q2, z_a, z_e = get_zx(path + 'list_mom', path + 'z_vec', 
                path + zname, zpert_dir + '/' + get_zname_pert(zname), ainv, rsplan)
        q2_list.append(q2)
        z_a_list.append(z_a)
        z_e_list.append(z_e)
        mq_list.append(mq)

        if show_mq: 
            ax.errorbar(q2, z_a, yerr=z_e, label=label, **stg.dot())
    
    # check that q2 are the same
    q2      = q2_list[0]
    n_q2    = len(q2)
    for q2_i in q2_list[1:]:
        assert np.allclose(q2, q2_i)

    # extrapolate mq->0 for every i_q2
    z0_a    = np.empty((n_q2,), np.float64)
    z0_e    = np.empty((n_q2,), np.float64)
    for i_q2 in range(n_q2):
        x, xcov = linreg([ [1., mq_list[i]] for i in range(n_ens) ], 
                         np.asarray(z_a_list)[:,i_q2],
                         yerr=np.asarray(z_e_list)[:,i_q2])
        z0_a[i_q2]  = x[0]
        z0_e[i_q2]  = math.sqrt(xcov[0,0])

    if None == q2_range: i_q2_sel = range(n_q2)
    else:
        i_q2_sel = []
        for i_q2, q in enumerate(q2):
            if q2_range[0] <= q and q <= q2_range[1]: i_q2_sel.append(i_q2)


    # extrapolate q2->0
    x, xcov         = linreg([ [1., q2_i] for q2_i in q2[i_q2_sel] ], 
                             z0_a[i_q2_sel])
    z00_a           = x[0]
    z00_e           = math.sqrt(xcov[0,0])

    # output
    print(spprint_ve(z00_a, z00_e))

    # plot
    ax.errorbar(q2, z0_a, yerr=z0_e, label=r'$m_q=0$', **stg_mq0.dot())
    
    # plot band
    q2band          = np.r_[min(q2[i_q2_sel]):max(q2[i_q2_sel]):101j]
    q2b_lin_matr    = np.array([ [1., q2b ] for q2b in q2band ])
    z0_a_band       = np.dot(q2b_lin_matr, x)
    z0_e_band       = linreg_maxdiff(q2b_lin_matr, xcov)
    ax.fill_between(q2band, z0_a_band-z0_e_band, z0_a_band, **stg_mq0.band())
    ax.fill_between(q2band, z0_a_band+z0_e_band, z0_a_band, **stg_mq0.band())

    ax.legend()
    ax.set_xlim([0, None])
    ax.set_xlabel(r'$\mu^2$')
    ax.set_ylabel(r'$Z_\text{SI}$')

    return ax

"""
#comparison
p200=''
q2list_1, t2s2d3r1_200MeV_a,t2s2d3r1_200MeV_e = get_zx(p200+'list_mom', p200+'z_vec', p200+'z_t2s2r3d1', zpert_dir + '/zp_ZWilsonT2N2', ainv, rsplan)
p250='../../../32c48_m-0.09530_mh-0.04/NPR/pynpr.mar2012/'
q2list_2, t2s2d3r1_250MeV_a,t2s2d3r1_250MeV_e = get_zx(p250+'list_mom', p250+'z_vec', p250+'z_t2s2r3d1', zpert_dir + '/zp_ZWilsonT2N2', ainv, rsplan)

fig=plt.figure(figsize=[10.,8.]) ; ax=fig.add_axes([.1,.1,.8,.8]); 
ax.errorbar(q2list, t2s2d3r1_200MeV_a, yerr=t2s2d3r1_200MeV_e, label='m_\pi=200 MeV', **stg[0].dot())
ax.errorbar(q2list+.07, t2s2d3r1_250MeV_a, yerr=t2s2d3r1_250MeV_e, label='m_\pi=250 MeV', **stg[1].dot())
ax.legend()
ax.set_xlim([0,None])
ax.set_xlabel(r'$\mu^2$')
ax.set_ylabel(r'$Z_\text{SI}$')
fig.savefig('figs-z/comp_200_250_t2s2d3r1.pdf')
fig.savefig('figs-z/comp_200_250_t2s2d3r1.png')
"""
