# scal
mkdir -p figs
PLTDIR=$HOME/usr/plt
gnuplot <<EOF
  load '$PLTDIR/lc'
  load '$PLTDIR/pt'
  load '$PLTDIR/ps_ls.plt'
  set output 'figs/zprop_trG0.eps'
  plot [0:] \\
        'zprop-av-q2' u 1:11:43 tit 'Re[S.G0]' w ye, \\
        'zprop-av-q2' u 1:27:59 tit 'Im[S.G0]' w ye
  set output
EOF
gnuplot <<EOF
  load '$PLTDIR/lc'
  load '$PLTDIR/pt'
  load '$PLTDIR/ps_ls.plt'
  set output 'figs/zprop_trGi_vs_ki.eps'
  plot [0:] x notit, sin(x) notit,\\
        'zprop-av-q2' u 7:28:40 tit 'Im[S.G1] vs k_x' w ye, \\
        'zprop-av-q2' u 8:29:61 tit 'Im[S.G2] vs k_y' w ye, \\
        'zprop-av-q2' u 9:31:63 tit 'Im[S.G4] vs k_z' w ye, \\
        'zprop-av-q2' u 10:35:67 tit 'Im[S.G8] vs k_t' w ye
  set output
EOF
gnuplot <<EOF
  load '$PLTDIR/lc'
  load '$PLTDIR/pt'
  load '$PLTDIR/ps_ls.plt'
  set output 'figs/z_scal_pssc.eps'
  plot \\
       'z_scal-av-q2' u 1:11:13 tit 'Re(scal)' w ye ls 1, \\
       'z_scal-av-q2' u 1:12:14 tit 'Im(scal)' w ye ls 2, \\
       'zr_scal-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(scal)' w ye ls 3, \\
       'z_pssc-av-q2' u 1:11:13 tit 'Re(pssc)' w ye ls 4, \\
       'z_pssc-av-q2' u 1:12:14 tit 'Im(pssc)' w ye ls 5,\\
       'zr_pssc-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(pssc)' w ye ls 6
  set output
EOF
gnuplot <<EOF
  load '$PLTDIR/lc'
  load '$PLTDIR/pt'
  load '$PLTDIR/ps_ls.plt'
  set output 'figs/z_vec_axvec.eps'
  plot \\
       'z_vec-av-q2' u 1:11:13 tit 'Re(vec)' w ye ls 1, \\
       'z_vec-av-q2' u 1:12:14 tit 'Im(vec)' w ye ls 2, \\
       'zr_vec-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(vec)' w ye ls 3, \\
       'z_axvec-av-q2' u 1:11:13 tit 'Re(axvec)' w ye ls 4, \\
       'z_axvec-av-q2' u 1:12:14 tit 'Im(axvec)' w ye ls 5,\\
       'zr_axvec-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(axvec)' w ye ls 6
  set output
EOF
gnuplot <<EOF
  load '$PLTDIR/lc'
  load '$PLTDIR/pt'
  load '$PLTDIR/ps_ls.plt'
  set output 'figs/z_cvec_caxvec.eps'
  plot \\
       'z_cvec-av-q2' u 1:11:13 tit 'Re(cvec)' w ye ls 1, \\
       'z_cvec-av-q2' u 1:12:14 tit 'Im(cvec)' w ye ls 2, \\
       'zr_cvec-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(cvec)' w ye ls 3,\\
       'z_caxvec-av-q2' u 1:11:13 tit 'Re(caxvec)' w ye ls 4, \\
       'z_caxvec-av-q2' u 1:12:14 tit 'Im(caxvec)' w ye ls 5,\\
       'zr_caxvec-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(caxvec)' w ye ls 6
  set output
EOF
gnuplot <<EOF
  load '$PLTDIR/lc'
  load '$PLTDIR/pt'
  load '$PLTDIR/ps_ls.plt'
  set output 'figs/z_sigma.eps'
  plot \\
       'z_sigma-av-q2' u 1:11:13 tit 'Re(sigma)' w ye ls 1, \\
       'z_sigma-av-q2' u 1:12:14 tit 'Im(sigma)' w ye ls 2, \\
       'zr_sigma-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(sigma)' w ye ls 3
  set output
EOF
gnuplot <<EOF
  load '$PLTDIR/lc'
  load '$PLTDIR/pt'
  load '$PLTDIR/ps_ls.plt'
  set output 'figs/z_t2s2.eps'
  plot \\
       'z_t2s2r3d1-av-q2' u 1:11:13 tit 'Re(t2s2r3d1)' w ye ls 1, \\
       'z_t2s2r3d1-av-q2' u 1:12:14 tit 'Im(t2s2r3d1)' w ye ls 2, \\
       'zr_t2s2r3d1-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(t2s2r3d1)' w ye ls 3, \\
       'z_t2s2r6d3-av-q2' u 1:11:13 tit 'Re(t2s2r6d3)' w ye ls 4, \\
       'z_t2s2r6d3-av-q2' u 1:12:14 tit 'Im(t2s2r6d3)' w ye ls 5,\\
       'zr_t2s2r6d3-av-q2' u 1:(\$12/\$11):(\$14/\$11) tit 'Res(t2s2r6d3)' w ye ls 6
  set output
EOF
