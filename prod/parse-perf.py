#!/usr/bin/python
from __future__ import print_function

import getopt
import re
import sys
import numpy

#MDWF eigCG solver: status = 0, eps = 9.9016e-17, iters = 1901, time = 615.818 sec, perf = 696.23 MFlops/sec

def print_usage():
    print("""Usage:
%s
    [-n <n_sol>] 
    [-p <patt>] 
    log1 log2 ...""" % sys.argv[0])
    print("""
Parse and print statistics for solver performance from an stdout log;
"-p <patt>" selects relevant lines; "-n <n_sol>" computes stat.average 
over adjacent n_sol output records
""")

try:
    optlist, flist = getopt.getopt(sys.argv[1:], "n:p:")
except getopt.GetoptError, err:
    print("Error: ", str(err))
    print_usage()
    sys.exit(1)

if len(flist) <= 0 : 
    print_usage()
    sys.exit(1)


n = 0
patt = ''
for opt, val in optlist:
    if   '-n' == opt: n = int(val)
    elif '-p' == opt: patt = val
    else: raise ValueError((opt, val))

l_perf = []
for f in flist:
    if '-' == f:    fd = sys.stdin
    else:           fd = open(f, 'r')
    l1 = filter(None, 
                [ re.match(r'.+eps = ([\d\.eE-]+), iters = (\d+), time = ([\d\.]+) sec, perf = ([\d\.]+) MFlops/sec', s)
                  for s in filter(lambda x: re.match(patt, x), 
                                  fd.readlines()) ])
    l2 =     [ (int(g[1]), float(g[2]), float(g[3]), float(g[0]))
               for g in [ r.groups() for r in filter(None, l1) ] ]

    l_perf.extend(l2)


def print_stat(l):
    l_s = sorted(l, cmp=lambda x1,x2: cmp(x1[0], x2[0]))
    print("min: %d\t%.2f\t%.2f\t%.1e" % tuple(l_s[0]))
    mid = len(l_s) / 2
    if 0 != len(l_s) % 2: 
        print("med: %d\t%.2f\t%.2f\t%.1e" % tuple(l_s[mid]))
    else:
        ls_mid = tuple([(l_s[mid-1][i] + l_s[mid][i])/2 for i in [0,1,2,3] ])
        print("med: %d\t%.2f\t%.2f\t%.1e" % ls_mid)
    print("avg: %.0f\t%.2f\t%.2f\t%.1e" % tuple(numpy.array(l_s, float).mean(0)))
    print("max: %d\t%.2f\t%.2f\t%.1e" % tuple(l_s[-1]))

if 0 != n:
    if 0 != len(l_perf) % n:
        print("warning: the number of matching lines is not a multiple of n=%d" % n)
    for i in range(len(l_perf) / n):
        print("### %d .. %d: iter, time, perf, eps" % (i*n + 1, (i+1)*n))
        print_stat(l_perf[i * n : (i + 1) * n])
else:
    print("### %d records: iter, time, perf, eps" % len(l_perf))
    print_stat(l_perf)
