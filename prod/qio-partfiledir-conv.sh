#!/bin/bash

echo >&2 "THIS DOES NOT WORK : QIO MASTERFILE HAS PARTFILE/PARTFILE_DIR IN HEADER?"
echo >&2 "THIS DOES NOT WORK : QIO MASTERFILE HAS PARTFILE/PARTFILE_DIR IN HEADER?"
echo >&2 "THIS DOES NOT WORK : QIO MASTERFILE HAS PARTFILE/PARTFILE_DIR IN HEADER?"
echo >&2 "THIS DOES NOT WORK : QIO MASTERFILE HAS PARTFILE/PARTFILE_DIR IN HEADER?"

function print_usage() {
  cat >&2 <<EOF
Usage:
\$ $(basename $0)   {pf2pfdir|pfdir2pf}   <vol-number>
      <src-file>  [<dst-file>]
---
    <vol-number>
              number of volumes ; if <=0, autodetect

EOF
}

if [ $# -lt 3 ] ; then 
  print_usage
  exit 1
fi

cmd=$1
n_vol=$2
shift 2

# TODO parse addl options here

if [ $# -lt 1 ] ; then
  print_usage
  exit 1
elif [ $# -lt 2 ] ; then
  src_file=$1
  dst_file=$1
elif [ $# -eq 2 ] ; then
  src_file=$1
  dst_file=$2
else
  shift 2
  echo "trailing options : $*"
  print_usage
  exit 1
fi

# make list of partfile volume files
# n_vol src_file
function pf_list() {
  [ $# -eq 2 ] || return 1
  local nvol=$1
  local src_file=$2
  if [ $nvol -gt 0 ] ; then
    for n in $(seq 0 $((nvol-1)) ) ; do
      local v=$(printf 'vol%04d' $n)
      echo ${src_file}.$v
    done
  else
    ls "$src_file".vol????
  fi
}
#transform partfile name (particular volume) to partfile_dir name
# src_name_pf_vol dst_name_pfdir
function pfdir_name() {
  [ $# -eq 2 ] || return 1
  local v=$(echo "$1" |sed -e 's?^.*\.\(vol[0-9]\+\)$?\1?')
  echo $(dirname "$2")/${v}/$(basename "$2")
}

# make list of partfile_dir volume files
# n_vol src_file
function pfdir_list() {
  [ $# -eq 2 ] || return 1
  local nvol=$1
  local src_file="$2"
  if [ $nvol -gt 0 ] ; then
    for n in $(seq 0 $((nvol-1)) ) ; do
      local v=$(printf 'vol%04d' $n)
      echo $(dirname "$src_file")/$v/$(basename "$src_file")
    done
  else
    ls $(dirname "$src_file")/vol????/$(basename "$src_file")
  fi
}
#transform partfile_dir name (particular volume) to partfile name
# src_name_pfdir_vol dst_name_pf
function pf_name() {
  [ $# -eq 2 ] || return 1
  local v=$(basename $(dirname "$1"))
  [[ $v = vol???? ]] || return 1
  echo "$2".${v}
}
#debug_prefix=echo

case $cmd in
pf2pfdir)
  for f in $(pf_list $n_vol "$src_file") ; do
    d=$(pfdir_name  "$f"  "$dst_file")
    $debug_prefix mkdir -p $(dirname "$d")
    $debug_prefix mv -v  "$f"  "$d"
  done
  ;;
pfdir2pf)
  for f in $(pfdir_list $n_vol "$src_file") ; do
    $debug_prefix mv -v "$f"  $(pf_name  "$f"  "$dst_file")
    $debug_prefix rmdir -v $(dirname "$f") 2>/dev/null
  done
  ;;
*)
  echo >&2 "$cmd: unknown command"
  print_usage
  exit 1
esac


#    [--dst-dir=<dst-dir>]
