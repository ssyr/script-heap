#!/bin/bash

do_verbose=""
while [[ $1 == -* ]] ; do
  case "$1" in
  -v|--verbose) do_vebose="yes" ;;
  *) echo >&2 "'$1': unknown option" ; exit 1 ;; 
  esac
  shift
done

if [ $# -lt 2 ] ; then 
  cat >&2 <<EOF 
Usage:
$(basename $0)  
    <in-dir>  <out-dir>
---
Check recursively that all files in <in-dir> are present in <out-dir>
and their versions in <in-dir> <= <out-dir> by modif.date. 
Note that the arg.order is the same as for copying to tape storage.
EOF
  exit 1
fi

src_dir="$1"
if [ ! -d "$src_dir" ] ; then 
  echo >&2 "$src_dir: not a directory"
  exit 1
fi

dst_dir="$2"
if [ ! -d "$dst_dir" ] ; then
  echo >&2 "$dst_dir: not a directory"
  exit 1
fi

[ "x$do_verbose" == "xyes" ] && echo "CHECK-R  '$src_dir'  ->  '$dst_dir'"

cnt_miss=0
cnt_size=0
cnt_date=0
for src_file in $(find $src_dir -type f |sort) ; do
  src_file_rel=${src_file##$src_dir}
  dst_file="$dst_dir/$src_file_rel"
  if [ ! -f $dst_file ] ; then 
    echo "$src_file_rel: missing at the destination"
    cnt_miss=$((cnt_miss+1))
  elif [ $(stat -c '%s' "$dst_file") -ne $(stat -c '%s' "$src_file") ] ; then
    echo "$src_file_rel: size mismatch"
    cnt_size=$((cnt_size+1))
  elif [ $(stat -c '%Y' "$dst_file") -lt $(stat -c '%Y' "$src_file") ] ; then
    echo "$src_file_rel: older at the source"
    cnt_date=$((cnt_date+1))
  fi
done

if [ $cnt_miss -ne 0 ] || [ $cnt_size -ne 0 ] || [ $cnt_date -ne 0 ] ; then
  if [ "x$do_verbose" == "xyes" ] ; then
    echo "missing:    $cnt_miss"
    echo "diff.size:  $cnt_size"
    echo "newer src:  $cnt_date"
  fi
  exit 1
else
  [ "x$do_verbose" == "xyes" ] && echo "COPY OK: $src_dir <= $dst_dir"
fi
