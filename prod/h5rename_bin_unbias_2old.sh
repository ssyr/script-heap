for f in $* ; do 
  [[ "$f" =~ '-bin.ub.' ]] || continue
  f2=$(echo $f |sed -e 's?-bin.ub.?-unbias.?')
  mv -v $f $f2
done
