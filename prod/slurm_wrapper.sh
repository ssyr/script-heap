#!/bin/bash
[ $# -ge 5 ] || {
  cat >&2 <<EOF
Usage:
$(basename $0)  <account>  <queue>  <walltime>  <nodes>  <procs>   [<cmd> ...]

account   active account name
queue     target queue
walltime  hh:mm:ss
nodes     total nodes
prods     total processes

The processes have env.vars PMI_RANK/PMI_SIZE 

EOF
  exit 1
}
ACCT=$1
QUEUE=$2
WALLTIME=$3
NODES=$4
PROCS=$5
PROCPERNODE=$(( (PROCS + NODES - 1) / NODES ))
shift 5

nodespec="-N $NODES --ntasks-per-node $PROCPERNODE"

submit_job_param_str=$(for p in "$@" ; do echo -n "  '$p'" ; done)
echo "CMD  : $submit_job_param_str"
echo "SLURM: ${PROCS}p/${NODES}n -> $PROCPERNODE : 'sbatch $nodespec'"

script=$(mktemp -p .  slurmbatch-XXXXX.sh)
cat >$script <<EOF
#!/bin/bash
#SBATCH -J $(basename $script)             #job name
#SBATCH $nodespec
#SBATCH -e ${script}.err      #Errors file name
#SBATCH -o ${script}.out      #Output file name
#SBATCH --qos=normal

srun $submit_job_param_str 
EOF
chmod u+x $script

echo $script
sbatch  -A "$ACCT"  -p "$QUEUE"  -t "$WALLTIME"   $script 
#rm $script
