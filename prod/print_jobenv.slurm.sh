#!/bin/bash

rank=$SLURM_PROCID
rank=${SLURM_PROCID:-$PMI_RANK}
fname=stdout.$(basename $0).${SLURM_JOBID}.$rank
uname -a >$fname
hostname >>$fname
echo "======================" >>$fname
set |grep CUDA >>$fname
set |grep OMP >>$fname
set |grep SLURM >>$fname
echo "======================" >>$fname
set |grep -v 'CUDA\|OMP\|SLURM' >>$fname
