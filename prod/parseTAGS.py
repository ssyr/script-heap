#!/usr/bin/env python
from __future__ import print_function
from future.utils import iteritems
import math
import re
import sys
from exceptions import *
import itertools as it
import numpy as np
import h5py


# parse 
def parse_TAG(fname, tag_list, dtype=np.float64) :
    res = {}
    fi  = open(fname, "r")
    for l in fi:
        li = l.split()
        if len(li) < 1 : continue
        for tag in tag_list :
            if li[0] == tag :
                res.setdefault(tag, []).append([ dtype(x) for x in li[1:] ])
    return res
    fi.close()

def dictlist_update_append(dlist, d, check_keys=False) :
    if 0 == len(dlist.keys()) :
        for k in d.keys() : dlist[k] = []
    if check_keys :
        for k in dlist.keys() :
            if not k in d :
                raise KeyError(k)
    for k, v in iteritems(d) :
        if check_keys :
            if not k in dlist :
                raise KeyError(k)
        dlist.setdefault(k, []).append(v)


def print_usage(s) :
    sys.stderr.write("""Usage:
%s  -o <out.h5>
    [--list=<flist>|-]  [-v]
    [--tags=tag:key]
""" % s)

def print_err(tag) :
    exc = sys.exc_info()
    sys.stderr.write("ERROR in tag %s: %s\n" % (tag, repr(exc[1])))

argv        = list(sys.argv)
progname    = argv[0]

VERBOSE     = 0
in_flist    = None
h5file_out  = None
tag_key_list    = []
if (len(argv) < 2):
    print_usage(progname)
    exit(1)
else :
    while 1 < len(argv) and argv[1].startswith('-') :
        if '--' == argv[1] : 
            argv.pop(1)
            break
        elif argv[1].startswith('--keys=') :
            kk = re.match('^--keys=(.+)$', argv[1])
            if not kk :
                raise ValueError(argv[1])
            kk_s = kk.groups()[0]
            print("kk_s=", kk_s)
            time_keys = [ tuple(x.split(':')) for x in kk_s.split(',') ]
            print("time_keys = ", time_keys)
        elif argv[1].startswith('--list=') :
            kk = re.match('^--list=(.+)$', argv[1])
            if not kk : raise ValueError(argv[1])
            in_flist = kk.groups()[0]
            if VERBOSE :
                print("in_flist='%s'" % str(in_flist))
        elif argv[1].startswith('--tags=') :
            kk = re.match('^--tags=(.+)$', argv[1])
            if not kk : raise ValueError(argv[1])
            tk_list_str = kk.groups()[0]
            for tk_str in tk_list_str.split(',') :
                if len(tk_str) < 2 : continue
                tk  = tk_str.split(':')
                if 1 == len(tk) :
                    tk.append(tk[0])
                elif 2 < len(tk) :
                    print("bad tk='%s'", tk_str)
                    exit(1)
                if 0 == len(tk[0]) : 
                    print("bad tk='%s'", tk_str)
                    exit(1)
                if 0 == len(tk[1]) : tk[1] = tk[0]
                tag_key_list.append(tuple(tk))

        elif '-v' == argv[1] :
            VERBOSE=1
        elif '-o' == argv[1] : 
            h5file_out = argv[2]
            argv.pop(2)
        else :
            sys.stderr.write("bad arg='%s'\n" % argv[1])
            print_usage(argv[0] or 'me')
            sys.exit(1)
        argv.pop(1)

if None is h5file_out:
    sys.stderr.write("No output hdf5 file\n\n")
    print_usage(progname)
    exit(1)

if VERBOSE :
    for tag, k in tag_key_list : print("tag:key = %s:%s" % (tag, k))

log_list = argv[1:]
if not None is in_flist :
    if '-' == in_flist :
        if VERBOSE : print("flist=STDIN")
        fi = sys.stdin
    else :
        if VERBOSE : print("flist=", in_flist)
        fi = open(in_flist, 'r')
    log_list.extend([ x.rstrip() for x in fi.readlines() ])

dlist = dict([ (tag, []) for tag, k in tag_key_list ])
tag_list = [ tag for tag, k in tag_key_list ]
for f in log_list :
    if VERBOSE: print("read '%s'" % f)
    dictlist_update_append(dlist, parse_TAG(f, tag_list))

if 0 == len(log_list) :
    sys.stderr.write("No input files\n\n")
    print_usage(progname)
    exit(1)

if not None is h5file_out :
    h5f = h5py.File(h5file_out)
    for tag, k in tag_key_list :
        v0  = dlist.get(tag)
        if None is v0 or 0 == len(v0) :
            if VERBOSE: print("EMPTY tag='%s'" % tag)
            continue
        try: v = np.array(v0)
        except: 
            print_err(tag)
            continue
        try: del h5f[k]
        except : pass
        h5f[k] = v

    h5f.flush()
    h5f.close()
