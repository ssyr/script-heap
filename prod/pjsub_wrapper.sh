#!/bin/bash
[ $# -ge 4 ] || {
  cat >&2 <<EOF
Usage:
$(basename $0)  <rscunit>  <walltime>  <nodes>  <procs>   [<cmd> ...]

rscunit   bwmpc|gwmpc(not tested)
walltime  hh:mm:ss
nodes     total notes
prods     total processes

The processes have env.vars PMI_RANK/PMI_SIZE 

EOF
  exit 1
}
RSCUNIT=$1
WALLTIME=$2
NODES=$3
PROCS=$4
PROCPERNODE=$(( (PROCS + NODES - 1) / NODES ))
shift 4
nodespec=''
case $RSCUNIT in
  bwmpc)
  nodespec="vnode=$NODES,vnode-core=40"
  ;;
  gwmpc)
  echo >&2 "gwmpc NOT TESTED!!!"
  nodespec="node=$NODES"
  ;;
esac

submit_job_param_str=$(for p in "$@" ; do echo -n "  '$p'" ; done)
echo "CMD: $submit_job_param_str"
echo "PJM@$RSCUNIT: ${PROCS}p/${NODES}n -> $PROCPERNODE : 'pjsub -L $nodespec'"

script=$(mktemp -p .  pjmbatch-XXXXX.sh)
cat >$script <<EOF
#!/bin/bash
#PJM -L rscgrp=batch
#PJM -L rscunit=$RSCUNIT
#PJM -L elapse=$WALLTIME
mpirun -np $PROCS -ppn $PROCPERNODE $submit_job_param_str 
EOF
chmod u+x $script

echo $script
pjsub --mpi proc=$PROCS -L "$nodespec" -g G19022 $script 
#rm $script
