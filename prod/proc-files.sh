#!/bin/bash

# filter files and feed list of files to pipe
# descendant of count-files.sh

[ $# -lt 2 ] && {
  cat >&2 <<EOF
Usage:
$(basename $0)  [--cmd=<cmd>]  [--sep-dir]
    [--list=<list1>]  [--list=<list2>] ...
    [--patt=<grep-pattern>]
    <dir1>  <dir2>  ...  --  c1  c2  ...

Notes:
* Default cmd is wc -l. 
* Configs from \`--list' are processed first.
* In \`cmd', \`\$c' is substituted. 
* All files from <dir1>, <dir2>, ... are grouped by config (cN). 
* If grouping separately by dir is desired, use \`--sepdir' ; 
  in \`cmd', \`\$d' will be substituded with the dir name.
EOF
  exit 1
}

# opt.args
patt=""
cmd="wc -l"
list_lists=""
sepdir=
while [[ $1 == --* ]] ; do
  case "$1" in
  --cmd=*)      cmd=$(echo $1 |sed -e 's?--cmd=??') ;;
  --sep-dir)    sepdir=yes ;;
  --list=*)     l1=$(echo $1 |sed -e 's?--list=??') ; list_lists="$list_lists $l1" ;;
  --patt=*)     patt=$(echo $1 |sed -e 's?--patt=??') ;;
  esac
  shift 1
done

# dirs
dlist=""
while [ "$1" != "--" ] && [ $# -gt 0 ] ; do
  dlist="$dlist $1"
#  echo "add '$1'"
  shift
done
shift
echo "'$patt'   '$dlist'   '$list_lists'   '$*'"

# <1.$c>
# group files from all dirs
function proc_groupdir() {
  local c="$1"
  local cmd1=$(echo $cmd |sed -e 's?\$c?'"$c"'?')
  echo -ne "$c\n"
  for d in $dlist ; do
    ls $d |grep -P '(?<!\w)'"$c"'(?!\w)' |grep "$patt" |sed -e 's?^?'"$d/"'?' 
  done \
  |$cmd1
}
# proc files from dirs separately
function proc_sepdir() {
  local c="$1"
  for d in $dlist ; do
    local cmd1=$(echo $cmd |sed -e 's?\$c?'"$c"'? ; s?\$d?'"$d"'?')
    echo -ne "$c\t$d\n"
    ls $d |grep -P '(?<!\w)'"$c"'(?!\w)' |grep "$patt" |sed -e 's?^?'"$d/"'?' \
    |$cmd1
  done
}

if [[ "x$sepdir" == xyes ]] ; 
  then procfunc=proc_sepdir
  else procfunc=proc_groupdir
fi

for ll in $list_lists ; do
  for c in $(<$ll) ; do $procfunc $c ; done
done
for c in $* ; do $procfunc $c ; done
