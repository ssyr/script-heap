#!/bin/bash

[ $# -ge 5 ] || {
  cat <<EOF
Usage:
$(basename $0)  nvol  stride  dstdir  dstlog  bn1 ...
EOF
  exit 1
}
nvol=$1
volstride=$2
dstdir=$3
dstlog=$4
shift 4

# 1:ivol
function volstr() { printf "vol%04d" $1 ; }

mkdir -p $dstdir || exit 1
for bn in $* ; do
  for n in $(seq 0 $volstride $((nvol-1)) ) ; do
    d="$dstdir/$(volstr $n)"
    mkdir -p $d || exit 1
    touch $d/$bn
  done
  echo $bn >> "$dstlog"
done
