#!/bin/bash
# vim: syn=sh

if [ $# -ne 5 ] ; then
  echo 1>&2 -e "Usage:"
  echo 1>&2 -e "$(basename $0)  <job>  <nodes>  <queue>  <time>  <project>"
  echo 1>&2 -e "Params:"
  echo 1>&2 -e "    <nodes>='2x1x1:TXYZ:4,4,4,8' means" 
  echo 1>&2 -e "        bg_shape=2x1x1" 
  echo 1>&2 -e "        mapfile=TXYZ" 
  echo 1>&2 -e "        qmp_geom='4 4 4 8'"
  echo 1>&2 -e "    <time>='HH:MM:SS'" 
  exit 1
fi

conf="$1"
nodes="$2"
queue="$3"
time="$4"
project="$5"

# parse nodes
bg_shape=$(echo $nodes |sed -e 's?\(.*\):\(.*\):\(.*\)?\1? ; s? ??g')
mapfile=$(echo $nodes |sed -e 's?\(.*\):\(.*\):\(.*\)?\2? ; s? ??g')
qmp_geom=$(echo $nodes |sed -e 's?\(.*\):\(.*\):\(.*\)?\3? ; s? ??g ; s?,? ?g' )
#echo "bg_shape=$bg_shape  mapfile=$mapfile  qmp_geom=$qmp_geom"

hracks_cnt=$(echo $bg_shape |sed -e 's?x?\*?g' |bc)
cores_cnt=$(echo $qmp_geom |sed -e 's? ?\*?g' |bc)
cores_per_hrack=2048
#echo "hracks_cnt=$hracks_cnt  cores_cnt=$cores_cnt"

if [ $((hracks_cnt * cores_per_hrack)) -ne $((cores_cnt)) ] ; then
  echo 1>&2 -e "mismatch between #hracks=$hracks_cnt and #nodes=$cores_cnt"
  exit 1
fi

# TODO verify correctness

wkdir=$(pwd)
logdir="logs/$conf"
logpath="$logdir/$conf"
mkdir -p "$logdir"

cat >"$logpath.ll" <<EOF
# @ job_name = $conf
# @ comment = "BMW nucleon structure"
# @ error = $logpath.error
# @ output = $logpath.output
# @ environment = DCMF_EAGER=65536;
# @ environment = COPY_ALL;
# @ environment = LL_JOB=1;
# @ wall_clock_limit = $time
# @ notification = error
# @ notify_user = ssyrisyn@lbl.gov
# @ job_type = bluegene
# @ bg_shape = $bg_shape
# @ bg_connection = TORUS
# @ initialdir = $wkdir
# @ queue
mpirun -mode VN -mapfile $mapfile -exe $wkdir/bin/qlua-bkend -cwd $wkdir -args "-qmp-geom $qmp_geom $logpath.job.qlua config.qlua main.qlua" -verbose 2 -label
EOF

llsubmit "$logpath.ll" >$logdir/submitted
