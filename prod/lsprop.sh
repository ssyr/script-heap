#!/bin/bash
function print_usage() {
  cat >&2 <<EOF 
Usage:
$(basename $0)  
    [--vol=<n>]
    <dir>  <cfg1>  ....
EOF
}

[ $# -ge 2 ] || {
  print_usage
  exit 1
}
vol=0
while [[ $1 == --* ]] ; do
  case "$1" in 
  --stride=*)     vol=$(echo $1 |sed -e 's?--vol=??') ;;
  *) echo >&2 "*** Unknown option '$1'" ; print_usage ; exit 1 ;;
  esac
  shift
done
volstr=$(printf 'vol%04d' $vol)

propdir=$1
shift

for c in $* ; do
  for f in $(
      cd $propdir/$volstr
      ls |grep '\.'"$c"'\.'
      ) ; do
    echo $propdir/$f
  done
done
