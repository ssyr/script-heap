#!/bin/bash


function print_usage() {
  cat >&2 <<EOF
Delete prop, erase ref from prop_log
Usage:
$(basename $0)  <prop1> ...

EOF
}

# match <fname> ~ <prefix>.<c>.<rest>, return <c>
# 1.fname 2.prefix
function get_cfgkey() {
  local pfx="$2"
  echo $1 |sed -e 's/^'"$pfx"'\.// ; s/\..*$//' 
}

# 1.fname
function get_prop_log(){
  local f=$1
  local bn=$(basename "$f")
  local dn=$(dirname "$f")
  local sdn=$(basename "$dn")
  local ddn=$(dirname "$dn")
  echo >&2 "get_prop_log[$f] : ddn='$ddn'  sdn='$sdn'  bn='$bn'"
  local kk=''
  local pfx=''
  case "$sdn" in 
    frw_prop) kk='prop_F' ; pfx='frw_prop' ;;
    bkw_prop) kk='prop_B' ; pfx='bkw_prop' ;;
    bkw_prop) kk='prop_B' ; pfx='bkw_prop\(_grp\)*' ;;
    *) echo >&2 "bad sdn='$sdn'"
  esac
  local c=$(get_cfgkey "$bn" "$pfx")
  echo >&2 "get_prop_log[$f] : c='$c'"
  echo "$ddn/prop_log/${c}.${kk}"
}

[ $# -ge 1 ] || {
  print_usage
  exit 1
}

V=''
#V=echo #XXX debug

for f in $* ; do
  dn="$(dirname $f)"
  bn="$(basename $f)"
  if [ ! -d "$dn" ] || [ ! -w "$dn" ] ; then
    echo >&2 "ERROR dirname not found / writable for: '$f'"
    continue
  fi
  echo "del $f"
  (cd $dn ; for d in $(ls |grep '^vol') ; do echo $d/$bn ; done |parallel -j 8 -n 8 $V rm) 
  plog="$(get_prop_log $f)"
  echo "erase $f from '$plog'"
  if [ -e "$plog" ] ; then
    if [ -w "$plog" ] ; then
      $V sed -i '/'"$bn"'/d' "$plog"
    else 
    echo >&2 "WARN prop_log not writable: '$plog'"
    fi
  else
    echo >&2 "WARN prop_log not found: '$plog'"
  fi
done
