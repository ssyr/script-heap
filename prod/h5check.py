#!/usr/bin/env python
from __future__ import print_function
from future.utils import iteritems

import os, sys, re, traceback
import h5py
import numpy as np

def print_usage(prg) :
    sys.stderr.write("""Usage:
%s  
    [--verbose|-v]      verbose output
    [--nan-ok|-n]       ignore NaN/inf (do not trigger check Fail)
    [--zero-ok|-z]      ignore exact zero records (typically mean lost data)
    [--lim-ok|-l]       ignore violation of limits (min-,max-abs)
    [--min-abs=<MIN>]   minimal value for sqrt(norm2(data[site]))
    [--max-abs=<MAX>]   maximal value for sqrt(norm2(data[site]))
    [--dscan|-d]        print stat info ONLY - don't analyze data
    <file1>  [-k <key1>| --key=<key1>]  ...

By default, Fail is triggered if any entry is NaN/inf or all entries are zero.
Inf and NaN are treated the same.
""" % prg)

def print_err(s) :
    exc = sys.exc_info()
    sys.stderr.write("ERROR %s: %s\n" % (s, repr(exc[1])))


VERBOSE = 0     # TODO make param
STATONLY= 0
nanOK   = 0
zeroOK  = 0
limOK   = 0
min_abs = None
max_abs = None
argv = list(sys.argv)
def get_val(s, prefix) :
    if not s.startswith(prefix) : return None
    else : return s[len(prefix):]

while 1 < len(argv) and argv[1].startswith('-') :
    if   '--verbose' == argv[1] or '-v' == argv[1] :
        VERBOSE = 1
    elif '--dscan' == argv[1] or '-d' == argv[1] :
        STATONLY = 1
    elif '--nan-ok' == argv[1] or '-n' == argv[1] :
        if VERBOSE : print("OPT: nanOK")
        nanOK   = 1
    elif '--zero-ok' == argv[1] or '-z' == argv[1] :
        zeroOK  = 1
        if VERBOSE : print("OPT: zeroOK")
    elif '--lim-ok' == argv[1] or '-l' == argv[1] :
        if VERBOSE : print("OPT: limOK")
        limOK  = 1

    elif argv[1].startswith('--min-abs=') :
        min_abs = get_val(argv[1], '--min-abs=')
        try : min_abs = eval("0+(%s)" % min_abs)     # some protection from mistakes/injection
        except :
            print_err(min_abs)
            sys.exit(1)
        if VERBOSE : print("OPT: min_abs =", min_abs)

    elif argv[1].startswith('--max-abs=') :
        max_abs = get_val(argv[1], '--max-abs=')
        try : max_abs = eval("0+(%s)" % max_abs)     # some protection from mistakes/injection
        except :
            print_err(max_abs)
            sys.exit(1)
        if VERBOSE : print("OPT: max_abs =", max_abs)

    else :
        print_usage(prg)
        sys.exit(1)

    argv.pop(1)

def h5dscan_recursive(h5g):
    def h5dscan_obj(h5gk) :
        is_ok = True
        if   isinstance(h5gk, h5py.Group)   : 
            what = 'Group'
        elif isinstance(h5gk, h5py.Datatype): 
            what = 'Datatype'
        elif isinstance(h5gk, h5py.Dataset) and not h5gk.name.endswith('__DATALIST') :
            type_str= str(h5gk.dtype)

            h5gk_dimspec = h5gk.attrs.get('dim_spec')
            if not None is h5gk_dimspec : 
                shape_str = ', '.join([ '%s:%d' % (h5gk_dimspec[i], a) 
                                        for i,a in enumerate(h5gk.shape) ])
            else : 
                shape_str  = ', '.join([str(d) for d in h5gk.shape])

            attrs_str = ', '.join([ 
                    '%s=%s[%s]' % (k, v.dtype, ','.join([ str(b) for b in v.shape ]) )
                    for k,v in iteritems(h5gk.attrs) ])

            sys.stdout.write("%s[%s]:\n\t%s[%s]\n\t{%s}\n" % ( 
                    h5g.file.filename, h5g.name, 
                    type_str, shape_str, attrs_str))
                
        else : pass

    isok = h5dscan_obj(h5g) 
    if isinstance(h5g, h5py.Group) :
        klist = h5g.keys()
        klist.sort()
        for k in klist : h5dscan_recursive(h5g[k]) 

def h5check_recursive(h5g):
    def h5checkobj(h5gk) :
        is_ok = True
        if   isinstance(h5gk, h5py.Group)   : 
            what = 'Group'
        elif isinstance(h5gk, h5py.Datatype): 
            what = 'Datatype'
        elif isinstance(h5gk, h5py.Dataset) and not h5gk.name.endswith('__DATALIST') :
            what    = "Dataset {%s}" % ', '.join([str(d) for d in h5gk.shape])
            v       = h5gk[()]
            n_tot   = v.size

            n_nan   = n_tot - np.count_nonzero(np.isfinite(v))
            is_ok   = is_ok and (nanOK or (0 == n_nan))
            if VERBOSE and 0 < n_nan : 
                sys.stdout.write("%s[%s]: %d/%d NaN/inf\n" % (h5g.file.filename, h5g.name, n_nan, n_tot))

            n_zero  = np.count_nonzero(0 == v)
            is_ok   = is_ok and (zeroOK or (n_zero < n_tot))
            if VERBOSE and 0 < n_zero : 
                sys.stdout.write("%s[%s]: %d/%d zeros\n" % (h5g.file.filename, h5g.name, n_zero, n_tot))

            #  compute abs only once
            if not None is min_abs or not None is max_abs : v_abs = np.abs(v)
            if not None is min_abs :
                n_min_v = np.count_nonzero(v_abs < min_abs)
                is_ok   = is_ok and (limOK or (0 == n_min_v))
                if VERBOSE and 0 < n_min_v : 
                    sys.stdout.write("%s[%s]: %d/%d below MIN\n" % (h5g.file.filename, h5g.name, n_min_v, n_tot))
            if not None is max_abs :
                n_max_v = np.count_nonzero(max_abs < np.abs(v))
                is_ok   = is_ok and (limOK or (0 == n_max_v))
                if VERBOSE and 0 < n_max_v : 
                    sys.stdout.write("%s[%s]: %d/%d above MAX\n" % (h5g.file.filename, h5g.name, n_max_v, n_tot))


        else : what = '???'

        if is_ok : sys.stdout.write("%s[%s]: %s: OK\n" % ( h5g.file.filename, h5g.name, what))
        else : sys.stdout.write("%s[%s]: %s: Fail\n" % ( h5g.file.filename, h5g.name, what))


        return is_ok

    isok = h5checkobj(h5g) 
    if isinstance(h5g, h5py.Group) :
        klist = h5g.keys()
        klist.sort()
        isok = isok and np.array([ h5check_recursive(h5g[k]) for k in klist ]).all()
    return isok


if len(argv) <= 1 :
    print_usage(argv[0])
    exit(1)

flist = list(argv[1:])
cnt_ok, cnt_fail = 0, 0
while 0 < len(flist) :
    f = flist.pop(0)
    k = '/'
    if 0 < len(flist) :
        if flist[0].startswith('--key==') : 
            k = get_val(flist.pop(0), '--key=')
        elif '-k' == flist[0] : 
            flist.pop(0)
            k = flist.pop(0)
    if VERBOSE: sys.stdout.write("READ %s[%s]\n" % (f, k))

    isok = True
    try : 
        h5f = h5py.File(f, 'r')
        if STATONLY : h5dscan_recursive(h5f[k])
        else :        isok = h5check_recursive(h5f[k])
        h5f.close()
    except :
        isok = False
        e = sys.exc_info()
        sys.stderr.write(str(e[1]))
        traceback.print_tb(e[2])

    if isok :
        sys.stdout.write("OK   %s\n" % f)
        cnt_ok += 1
    else :
        sys.stdout.write("Fail %s\n" % f)
        cnt_fail += 1

if 0 < cnt_ok :     sys.stdout.write("SUMMARY: OK %d\n" % (cnt_ok,))

if 0 < cnt_fail :   
    sys.stdout.write("SUMMARY: Fail %d\n" % (cnt_fail,))
    sys.exit(1)
else : 
    sys.exit(0)
