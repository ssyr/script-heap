#!/usr/bin/env python
from __future__ import print_function
import os, sys, re
import lhpd, h5py, numpy as np

def select_dslice_obj(h5g_out, h5g_in, h5kpath, dslice) :
    h5o_in          = h5g_in[h5kpath]
    if lhpd.h5_io.h5_is_dset_datalist(h5o_in) or lhpd.h5_io.h5_is_dset_hugeattr(h5o_in) : 
        return # will copy datalists,hugeattrs to each h5o if not orphaned
    if 1 <= VERBOSE: 
        print("%s << %s [kpath=%s]" % (h5g_out.file.filename, h5g_in.file.filename, h5g_in.name))

    if   isinstance(h5o_in, h5py.Dataset) :
        # copy data
        lhpd.h5_io.h5_purge_keys(h5g_out, h5kpath)
        h5g_out[h5kpath]  = h5o_in [()] [dslice] # XXX read all in memory, then slice
    elif isinstance(h5o_in, h5py.Group) :
        h5g_out.require_group(h5kpath)

    h5o_out         = h5g_out[h5kpath]
    # handle datalist separately
    # handle hugeattr separately
    attrs           = h5o_in.attrs.keys()
    attrs_cp       = []
    for a in attrs :
        if   lhpd.h5_io.h5_is_attr_datalist(h5o_in, a) :
            if 1 <= VERBOSE :
                print("copy datalist %s[%s][%s]" % (h5g_in.file.filename, h5g_in.name, a))
            lhpd.h5_io.h5_set_datalist(h5o_out, 
                    lhpd.h5_io.h5_get_datalist(h5o_in)[dslice])
        elif lhpd.h5_io.h5_is_attr_hugeattr(h5o_in, a) :
            if 1 <= VERBOSE :
                print("copy hugeattr %s[%s][%s]" % (h5g_in.file.filename, h5g_in.name, a))
            lhpd.h5_io.h5_set_hugeattr(h5o_out, a, 
                    lhpd.h5_io.h5_get_hugeattr(h5o_in, a))
        else : 
            attrs_cp.append(a)

    lhpd.h5_io.h5_copy_attr(h5o_out, h5o_in, attrs_cp)

def select_dslice_obj_recur(h5g_out, h5g_in, dslice) :
    if 1 <= VERBOSE : print("%s[%s] << %s[%s]" % (
            h5g_out.file.filename, h5g_out.name, 
            h5g_in.file.filename, h5g_in.name))
    if isinstance(h5g_in, h5py.Group) :
        for k in h5g_in.keys() :
            select_dslice_obj(h5g_out, h5g_in, k, dslice) 
            if isinstance(h5g_in[k], h5py.Group) :
                select_dslice_obj_recur(h5g_out[k], h5g_in[k], dslice)

        # FIXME copy datalist, hugeattr for Groups AND Datasets?
            

dslice      = None
h5fname_in  = None
h5fname_out = None
do_recur    = False
VERBOSE     = 0

if '__main__' == __name__ :

    argv = list(sys.argv)
    print(repr(argv))
    if len(argv) < 1 :
        print_usage('me')
        exit(1)

    while 1 < len(argv) and argv[1].startswith('-') :
        if argv[1].startswith('--dslice=') :
            dslice_str = re.match('^--dslice=(.+)$', argv[1]).groups()[0]
            try : dslice = eval("np.r_[%s]" % dslice_str)
            except : 
                sys.stderr.write("bad dslice='%s'\n" % dslice_str)
                exit(1)
        elif '-r' == argv[1] :
            do_recur = True
        elif '-v' == argv[1] :
            VERBOSE = 1
        elif argv[1].startswith('--file-in=') :
            h5fname_in = re.match('^--file-in=(.+)$', argv[1]).groups()[0]
        elif argv[1].startswith('--file-out=') :
            h5fname_out = re.match('^--file-out=(.+)$', argv[1]).groups()[0]
        else : 
            sys.stderr.write("bad option '%s'\n" % argv[1])
            exit(1)
        
        argv.pop(1)


if 1 <= VERBOSE: 
    print("%s << %s (recur=%s) dslice(%d)=%s" % (
        h5fname_out, h5fname_in, str(do_recur), len(dslice), str(dslice)))

if (None is h5fname_in) or (None is h5fname_out) or (None is dslice) :
    sys.stderr.write('insufficient parameters\n')
    exit(1)

h5f_in  = h5py.File(h5fname_in, 'r')
h5f_out = h5py.File(h5fname_out, 'a')

if do_recur :
    if 1 < len(argv) : 
        sys.stderr.write("Recursive(-r) set: ignore cmdline keylist\n%s\n" % '\n'.join(argv[1:]))
    select_dslice_obj_recur(h5f_out['/'], h5f_in['/'], dslice)
else : 
    for k in argv[1:] : 
        select_dslice_obj(h5f_out, h5f_in, k, dslice)

h5f_in.close()
h5f_out.flush()
h5f_out.close()
