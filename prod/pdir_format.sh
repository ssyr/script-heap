#!/bin/bash
if [ $# -lt 1 ] ; then
  cat >&2  <<EOF
Usage:
$(basename $0) <Nvol>[:<stride>]  [dir1] ...
EOF
  exit 1
fi

nvol=$1
nstride=1
if [[ "$nvol" == *:* ]] ; then
  IFS=": " read nvol nstride <<<"${nvol}"
fi
echo "TEST $nvol:$nstride"

shift
if [ $# -lt 1 ] ; then
  dir_list='.'
fi
echo $nvol
echo $*
for n in $(seq 0 $nstride $((nvol-1)) ) ; do
  v=$(printf 'vol%04d' $n)
#  echo mkdir -pv $(for d in $(echo "$@") ; do echo $d/$v ; done )
  mkdir -pv $(for d in $(echo "$@") ; do echo $d/$v ; done )
done
