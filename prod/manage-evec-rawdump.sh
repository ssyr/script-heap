#!/bin/bash

function print_usage() {
  cat >&2 <<EOF
Usage:
$(basename $0) <Nrank>  <stride>  [list|ll|check|rm|tar|untar]  <file1>  ...
$(basename $0) <Nrank>  <stride>  [cp|mv]  <src>  <dest>
TODO add -v for verbose

EOF
}

if [ $# -lt 3 ] ; then
  print_usage
  exit 1
fi

nrank=$1
stride=$2
cmd=$3
shift 3

if [ $# -le 0 ] ; then
  echo >&2 "Error: no files"
  print_usage
  exit 1
fi

nrankM1=$(( nrank - 1 ))
strideM1=$(( stride - 1 ))
ngroup=$(( nrank / stride ))
ngroupM1=$(( ngroup - 1 ))

# file i_f i_g
function mk_fname_part() {
  printf '%s/%04d/%s.%07d\n' $(dirname $1) $3 $(basename $1) $2
}
# file
function mk_fname_crc32() {
  echo "$1.CRC32"
}
# file
function mk_fname_evals() {
  echo "$1.evals"
}

#file
function list_files() {
  local f=$1
  for i_f in $(seq 0 $nrankM1) ; do
    local i_g=$(( i_f / stride ))
    mk_fname_part $f $i_f $i_g
  done
  mk_fname_evals $f
  mk_fname_crc32 $f
}


# file
function do_check() {
  for f in $(list_files $1) ; do
    [ -x $f ] || echo $f missing
  done
}
# file
function do_ll() {
  list_files $1 |xargs ls -lh
}
# file
function do_rm() {
  list_files $1 |xargs rm -v
}


for f in $* ; do
  case $cmd in
    list)   list_files $f ;;
    check)  do_check $f ;;
    ll)     do_ll $f ;;
    tar)    echo >&2  "$cmd: Not Implemented" ;;
    untar)  echo >&2  "$cmd: Not Implemented" ;;
    cp)     echo >&2  "$cmd: Not Implemented" ;;
    mv)     echo >&2  "$cmd: Not Implemented" ;;
    rm)     do_rm $f ;;
    *)      echo >&2 "$cmd : unknown"
  esac
done

