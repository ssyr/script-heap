#!/usr/bin/env python
from __future__ import print_function
from future.utils import iteritems
import math
import re
import sys
import numpy as np
from exceptions import *


def parse_log_TIME(fname):
    res     = {}
    fi = open(fname, "r")
    for l in fi:
        if not l.startswith('TIME('): continue

        mo  = re.match('TIME\("([^"]+)","([^"]+)"\) = (\d+\.\d+) secs', l)
        if None == mo:
            sys.stderr.write("%s: bad TIME string: '%s'\n", fname, l)
            continue
        k = tuple(mo.groups()[0:2])
        res.setdefault(k, []).append(float(mo.groups()[2]))
    return res
    fi.close()

def dictlist_update(dlist, d, check_keys=False) :
    if 0 == len(dlist.keys()) :
        for k in d.keys() : dlist[k] = []
    if check_keys :
        for k in dlist.keys() :
            if not k in d :
                raise KeyError(k)
    for k, v in iteritems(d) :
        if check_keys :
            if not k in dlist :
                raise KeyError(k)
        dlist.setdefault(k, []).extend(v)
def stat_str(a, fmt='%.2f') :
    """ print stats of an array avg{min:med:max} """
    a   = np.array(a)
    a.sort()
    #return ('%s(%s/%s/%s)' % ((fmt,)*4)) % (
        #np.mean(a), np.min(a), np.median(a), np.max(a))
    return ('%s (%s < %s < %s > %s > %s)' % ((fmt,)*6)) % (
        np.mean(a), np.min(a), np.percentile(a, 25), 
        np.median(a), np.percentile(a, 75), np.max(a))


def print_usage(nn):
    sys.stderr.write("Usage:\n%s  [--list=<log-list] [--keys=<keys>]  <log-stdout>\n" %(nn));

time_keys=[('total', 'done')] # default
in_flist = None
VERBOSE = 0

argv = list(sys.argv)

if (len(argv) < 2):
    print_usage(argv[0])
    exit(1)
else :
    while 1 < len(argv) and argv[1].startswith('-') :
        if argv[1].startswith('--keys=') :
            kk = re.match('^--keys=(.+)$', argv[1])
            if not kk :
                raise ValueError(argv[1])
            kk_s = kk.groups()[0]
            print("kk_s=", kk_s)
            time_keys = [ tuple(x.split(':')) for x in kk_s.split(',') ]
            print("time_keys = ", time_keys)
        elif argv[1].startswith('--list=') :
            kk = re.match('^--list=(.+)$', argv[1])
            if not kk : raise ValueError(argv[1])
            in_flist = kk.groups()[0]
            if VERBOSE : 
                print("in_flist='%s'" % str(in_flist))

        elif '-v' == argv[1] : 
            VERBOSE=1
        else :
            sys.stderr.write("bad arg='%s'\n" % argv[1])
            print_usage(argv[0] or 'me')
            sys.exit(1)
        argv.pop(1)


log_list = argv[1:]
if not None is in_flist :
    if '-' == in_flist : 
        print("flist=STDIN")
        fi = sys.stdin
    else : 
        print("flist=", in_flist)
        fi = open(in_flist, 'r')
    log_list.extend([ x.rstrip() for x in fi.readlines() ])

dlist = {}
for f in log_list :
    if VERBOSE: print("read '%s'" % f)
    dictlist_update(dlist, parse_log_TIME(f))

if VERBOSE :
    print("allkeys=", dlist.keys())

for k in time_keys :
    print(k, "=", stat_str(dlist[k]))
