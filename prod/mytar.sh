#!/bin/bash
[ $# -eq 1 ] || { 
  cat <<EOF
Usage:
$(basename $0)  <out-tar-file>
EOF
  exit 1
}

tarfile="$1"
tar --remove-files -T - -cvf "$tarfile"
dn="$(dirname $tarfile)"
md5sum "$tarfile" >>"$dn/MD5SUM"
