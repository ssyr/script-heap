#!/bin/bash


prg_tag=$(basename $0)
dir_prefix=${PWD#$HOME/}

function home_path () {
  local d
  d="$1"
  if [ "${d:0:1}" == "/" ] ; then echo ${d#$HOME/}
  else echo ${dir_prefix}/$d ; fi
}

function arch_htar () {
  local d full_d full_d_up base_d
  d=$(echo $1 |sed -e 's?/\+$??')
  home_d=$(home_path "$d")
  full_d="$HOME/$home_d"
  full_d_up=$(dirname $full_d)
  base_d=$(basename $d)
  if [ ! -d "$full_d" ] ; then
    echo >&2 "$prg_tag: $full_d: not a directory"
    continue
  fi
  pushd >/dev/null $full_d_up
  htar -cf "$home_d.tar" -P $base_d && \
    touch "${base_d}__ARCHIVED__" && \
    echo $d OK
  popd >/dev/null
}

function arch_huntar () {
  local d full_d full_d_up 
  d=$(echo $1 |sed -e 's?/\+$?? ; s?__ARCHIVED__$??')
  home_d=$(home_path "$d")
  full_d="$HOME/$home_d"
  full_d_up="$(dirname $full_d)"
  if [ ! -d "$full_d_up" ] ; then
    echo >&2 "$prg_tag: $full_d_up: not a directory"
    continue
  fi
  pushd >/dev/null $full_d_up
  htar -xf "$home_d.tar" && \
    echo $d OK
  popd >/dev/null
}
function arch_hlist () {
  local d full_d full_d_up 
  d=$(echo $1 |sed -e 's?/\+$?? ; s?__ARCHIVED__??')
  home_d=$(home_path "$d")
  echo $d
  full_d="$HOME/$home_d"
  full_d_up="$(dirname $full_d)"
  if [ ! -d "$full_d_up" ] ; then
    echo >&2 "$prg_tag: $full_d_up: not a directory"
    continue
  fi
  pushd >/dev/null $full_d_up
  htar -tf "$home_d.tar" && \
    echo $d OK
  popd >/dev/null
}

if [ $# -le 1 ] ; then
  cat >&2 <<EOF
Put to archive/get from archive/list archive
Archive TAR files locations mirror archived directories
For each archived directory, <dir>__ARCHIVED__ file is created
  
Usage:
$prg_tag  <cmd>  <dir1> ...

Commands:
    htar    create(overwrite) a TAR file in HPSS
            a new TAR overwrites an old TAR
            a mark "<dir>__ARCHIVED__" is created
    huntar  extract files from a TAR file from HPSS
            files are overwritten by the archived versions
    hlist   list files in a TAR file in HPSS

EOF
  exit 1
fi


cmd="$1"
shift 1
case $cmd in
  htar)
    for d in "$@" ; do arch_htar "$d" ; done ;;
  huntar)
    for d in "$@" ; do arch_huntar "$d" ; done ;;
  hlist)
    for d in "$@" ; do arch_hlist "$d" ; done ;;
  *)
    echo >&2 "$prg_tag: $cmd: unknown command"
    exit 1
esac
