from past.builtins import execfile
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

import dataview as dv


fig = plt.figure(figsize=[10,8])
x0, x1  = 0., 6.

ax1 = dv.axes_log(fig.add_axes([.1, .55, .8, .4]))
ax1.plot(np.r_[x0:x1:100j], np.sin(np.r_[x0:x1:100j]), label='sin(x)')
ax1.legend()

ax2 = dv.axes_log(fig.add_axes([.1, .05, .8, .4]))
ax2.plot(np.r_[x0:x1:100j], np.cos(np.r_[x0:x1:100j]), label='cos(x)')
ax2.legend()

fig.savefig('sine_ver1.pdf')

dv.save_figdraw(fo=open('sine_ver1.py', 'w'), fig_name='fig_sine', 
                # all axes in the figure should be listed below
                sine=ax1, cos=ax2)

### some time later

execfile('sine_ver1.py')
