#!/usr/bin/perl -w
use strict;

#/cfg081010_143023/hspectrum/GN25-GN25/mq-0.0953/proton_3/x0_y0_z12_t36/PX2_PY2_PZ2

if (1+$#ARGV < 2) {
    printf STDERR "Usage:\n$0  -f (<file-list>|-) \n" .
                  "\t\t[-p < pattern>] ...\n" .
                  "\t\t[--] [<file1>] ...";
    exit 1;
}
sub read_list($)
{ 
    my @list = ();
    if ($_[0] eq '-') {
        while(<STDIN>) {
            chomp;
            if (/^$/ || /^#/) { next };
            push @list, $_;
        }
    } else {
        open(FP, "$_[0]") 
            or die "cannot open $_[0]";
        while(<FP>) {
            chomp;
            if (/^$/ || /^#/) { next };
            push @list, $_;
        }
        close(FP);
    }
    return @list;
}

my $list = undef;
my $out_file = undef;
my @patt_list = ();
while ($_ = shift @ARGV) {
    if ($_ eq '--') { last }
    elsif ($_ !~ /^-/) { unshift(@ARGV, $_) ; last }
    elsif ($_ eq '-f') { $list = shift @ARGV ; next }
    elsif ($_ eq '-p') { push @patt_list, shift @ARGV ; next }
    else { die "unknown parameter '$_'"; }
}
die "file list not given"
    if (!defined($list));
my @file_list = (read_list($list), @ARGV);



my $LHPC_AFF='lhpc-aff';
foreach my $file (@file_list) {
    my @keys = split(/\n/, `$LHPC_AFF ls -kR $file`);
    foreach (@keys) { $_ =~ s/:.*$// }

    foreach my $patt (@patt_list) {
        @keys = grep (/$patt/, @keys)
    }
    open(AFF_CAT, "|$LHPC_AFF cat -cgnf - $file ")
        or die "cannot open $file for reading";
    print "### $file\n";
    foreach my $key (@keys) {
        print AFF_CAT "$key\n";
    }
    close(AFF_CAT)
        or die "cannot finish $LHPC_AFF\n";
    print "\n\n"
}

