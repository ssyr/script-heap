#!/bin/bash

if [ $# -ne 4 ] ; then
  echo >&2 -ne "Usage:\n$(basename $0)  <file-in>  <key-in>  <file-out>  <key-out>\n"
  exit 1
fi
aff_in="$1"
key_in="$2"
aff_out="$3"
key_out="$4"

lhpc-aff ls -kR "$aff_in" \
    |sed -e 's?:.*$??' \
    |grep "${key_in}"'$' \
    |while read k ; do echo "$k $(echo $k |sed -e "s?${key_in}?${key_out}?")" ; done \
    |lhpc-aff cp -Rf -   "$aff_in"  "$aff_out"
