#!/usr/bin/perl -w
use strict;


if (1+$#ARGV < 5) {
    printf STDERR "Usage:\n" .
                  "$0  -f (<file-list>|-)  -o <out-aff-file>\n" .
                  "        [-p < pattern>] ...\n" .
                  "        [--]  [<file1>] ...\n";
    exit 1;
}
sub read_list($)
{ 
    my @list = ();
    if ($_[0] eq '-') {
        while(<STDIN>) {
            chomp;
            if (/^$/ || /^#/) { next };
            push @list, $_;
        }
    } else {
        open(FP, "$_[0]") 
            or die "cannot open $_[0]";
        while(<FP>) {
            chomp;
            if (/^$/ || /^#/) { next };
            push @list, $_;
        }
        close(FP);
    }
    return @list;
}

my $list = undef;
my $out_file = undef;
my @patt_list = ();
my $verbose = 1;
while ($_ = shift @ARGV) {
    if ($_ eq '--') { last }
    elsif ($_ !~ /^-/) { unshift(@ARGV, $_); last }
    if ($_ eq '-f') { $list = shift @ARGV ; next }
    if ($_ eq '-o') { $out_file = shift @ARGV ; next }
    if ($_ eq '-p') { push @patt_list, shift @ARGV ; next }
    if ($_ eq '-v') { $verbose = 1 ; next }
    if ($_ eq '-vv') { $verbose = 2 ; next }
    if ($_ eq '-V') { $verbose = 0 ; next }
    die "unknown parameter '$_'";
}
die "output file not given"
    if (!defined($out_file));
my @file_list = (@ARGV);
if (defined($list)) { @file_list = (read_list($list), @file_list) };




my $LHPC_AFF='lhpc-aff';
my @tmp_list = ();
my $key_cnt = 0;
foreach my $file (@file_list) {
    my @keys = split(/\n/, `$LHPC_AFF ls -kR $file`);
    foreach (@keys) { $_ =~ s/:.*$// }

    foreach my $patt (@patt_list) {
        @keys = grep (/$patt/, @keys)
    }
    my $tmp_file = `mktemp -p . $out_file.XXXXXXXX`;
    chomp $tmp_file;
    `rm $tmp_file`;
    open(AFF_CP, "|$LHPC_AFF cp -Rf - $file $tmp_file")
        or die "cannot open $tmp_file for writing";
    foreach my $key (@keys) {
        $key_cnt += 1;
        if (2 <= $verbose) { print STDERR "$file [$key]\n" }
        print AFF_CP "$key $key\n";
    }
    close(AFF_CP)
        or die "cannot finish $LHPC_AFF\n";
    push @tmp_list, $tmp_file;
}

open(AFF_JOIN, "|$LHPC_AFF join -o $out_file -f -")
    or die "cannot open $out_file for writing";
foreach my $file (@tmp_list) {  print AFF_JOIN "/ $file /\n" }
close(AFF_JOIN)
    or die "cannot finish $LHPC_AFF\n";
foreach my $file (@tmp_list) {  `rm -f $file` }

if (1 <= $verbose) { print STDERR "selected $key_cnt keys\n" }
