#!/usr/bin/perl -w
use strict;
use File::Basename;

if (1+$#ARGV < 4) {
    print STDERR "Usage:\n$0  <src_dir>  <dst_dir>  <main-text>  <text-map>  <fig-map>\n";
    exit(1)
}

my ($src_dir, $dst_dir, $maintext_file, $textmap_file, $figmap_file) = @ARGV;
print "$src_dir\t$dst_dir\t$maintext_file\t$textmap_file\t$figmap_file\n";

(-d $src_dir) or die "$src_dir is not a directory";

if (!-d $dst_dir) {
    (!-e $dst_dir) 
        or die "$dst_dir is not a directory";
    mkdir($dst_dir) 
        or die "cannot create $dst_dir";
} 

sub read_file_text($) {
    my $FH;
    open($FH, $_[0])
        or die "$_[0] is not readable";
    local $/ = undef; 
    my $text = <$FH>;
    close($FH);
    return $text;
}

# TODO
my $maintext = read_file_text($maintext_file);

my $fm_fh;
open($fm_fh, $figmap_file)
    or die "$figmap_file is not readable";
my @figmap = ();
while (<$fm_fh>) {
    chomp;
    if (/^#/ || /^$/) { next }
    push @figmap, [ split(/\s+/, $_) ];
}
close($fm_fh);

open($fm_fh, $textmap_file)
    or die "$textmap_file is not readable";
my @textmap = ();
while (<$fm_fh>) {
    chomp;
    if (/^#/) { next }
    push @textmap, [ split(/\s+/, $_) ];
}
close($fm_fh);


foreach (@textmap) {
    my ($from) = @$_;
    my $from_text = read_file_text("$src_dir/$from");
    $maintext =~ s/\\input\{$from(.tex)?\}/$from_text/g;
}
foreach (@figmap) {
    my ($from, $to) = @$_;
    print "$from -> $to\n";
    `cp "$src_dir/$from" "$dst_dir/$to"`;
    my $from_b = $from; $from_b =~ s/\.[^.]+$//;
    my $to_b = $to; $to_b =~ s/\.[^.]+$//;
    print "subst '$from_b' -> '$to_b'\n";
    $maintext =~ s/$from_b/$to_b/g;
}
if ($maintext =~ /[^%]*\\bibliography\{(.+)\}/) {
    my $bib=$1 ;
    $bib =~ s/.bib$// ;
    $bib = "$bib.bib" ;
    print "$src_dir/$bib -> $dst_dir/$bib\n" ;
    `cp  "$src_dir/$bib"  "$dst_dir/$bib" ` ;
}
if (-e "$src_dir/Makefile") { `cp "$src_dir/Makefile" "$dst_dir/Makefile"` }
if (-e "$src_dir/make.tex.inc") { `cp "$src_dir/make.tex.inc" "$dst_dir/make.tex.inc"` }

my $dst_maintext_file = "$dst_dir/" . basename($maintext_file);
print STDERR "write to '$dst_maintext_file'\n";
open($fm_fh, ">$dst_maintext_file");
print $fm_fh $maintext;
close($fm_fh);

