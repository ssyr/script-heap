#!/usr/bin/env python
from __future__ import print_function

import re
import sys
import os, shutil, tempfile
import getopt

def print_usage(name) :
    print("""Usage:
%s  <out-dir>  <latex-file> 
    --def-extn=<.extn>
""" % name)

def has_extn(name, extn_list) :
    for e in extn_list :
        if name.endswith(e) : return e
    return None

def noextname(fname, delim='.') :
    c       = fname.rfind(delim)
    if c < 0 : return fname
    dname   = os.path.dirname(fname)
    res = fname[:c]
    if len(res) < len(dname) :
        return fname
    else : return res

if len(sys.argv) < 3:
    print_usage(sys.argv[0])
    sys.exit(1)

cmdopt, args = getopt.getopt(sys.argv[1:], [], ['def-extn='])
extn_default = '.pdf'
for o, v in cmdopt :
    if '--def-extn' == o : extn_default = v
    else : 
        print_usage(sys.argv[0])
        raise ValueError

if 2 != len(args) :
    print_usage(sys.argv[0])

out_dir, tex_fname = args

print(tex_fname, '->', out_dir, extn_default)


if '' != os.path.dirname(tex_fname) :
    print("RUN IN THE SOURCE DIRECTORY")
    sys.exit(1)

fig_cnt = 0
subfig_cnt = 0

tex_noextname = noextname(tex_fname)
bbl_fname = '%s.bbl' % (tex_noextname)
print(bbl_fname)
out_fd, out_fname = tempfile.mkstemp()
fig_rename = [ 
    (out_fname, '%s/%s' % (out_dir, tex_fname)),
    (bbl_fname, '%s/%s' % (out_dir, bbl_fname)) ]


out_file = os.fdopen(out_fd, 'w')
for l in open(tex_fname) : 
    l1 = str(l)
    ss_cnt = 0
    for ss in [r'\begin{figure}', r'\includegraphics'] :
        ss_cnt += l1.count(ss)
    if 1 < ss_cnt : 
        print("TOO MANY TOKENS PER STRING\n", l1)
        sys.exit(1)

    if re.match(r'[^%]*\\begin\{figure\}', l1) :
        fig_cnt += 1
        subfig_cnt = 0

    x = re.match(r'[^%]*\\includegraphics\[.*\]\{(.+)\}', l1)
    if x :
        subfig_cnt  += 1
        fig_fname_orig= x.groups()[0]
        extn = has_extn(fig_fname_orig, ['.pdf', '.png', '.ps'])
        if None is extn :
            fig_fname = '%s%s' % (fig_fname_orig, extn_default)
            extn = extn_default
        else : fig_fname = fig_fname_orig
        fig_dst_bname = 'fig%d%s%s' % (fig_cnt, 'abcdefghijkl'[subfig_cnt-1], extn)
        l1 = l1.replace(fig_fname_orig, fig_dst_bname)
        fig_dst = '%s/%s' % (out_dir, fig_dst_bname)
        fig_rename.append((fig_fname, fig_dst))
        #print(fig_cnt, subfig_cnt, fig_fname_orig, fig_dst_bname)
    
    x = re.match(r'[^%]*\\bibliography\{(.+)\}', l1)
    if x :
        bib_fname = x.groups()[0]
        fig_rename.append(('%s.bib' % bib_fname, '%s/%s.bib' % (out_dir, bib_fname)))
    out_file.write(l1)

out_file.flush() ; out_file.close()
print(fig_rename)

#sys.exit(1)
os.makedirs(out_dir)

for src, dst in fig_rename : 
    if os.path.isfile(src) : 
        print(src, '->', dst)
        shutil.copy(src, dst)
    else : 
        print(src, ": not found")

