#!/usr/bin/perl -w

local $/=undef;

my $text = <>;

# remove all unnecessary \n's
# remove \n unless the next line starts with \s: it's a new paragraph
$text =~ s/\s*\n(?!\s)/ /g;
#remove spaces in front of a new paragraph
$text =~ s/\n\s+/\n/g;
#remove empty lines
$text =~ s/^\s+$//g;

print $text, "\n";
