#!/usr/bin/perl -w
use strict;
sub rotate {
    my ($r, @p) = @_;
    if ($r eq "") { return @p }
    if ($r eq "L") { return ( -$p[1],  $p[0] ) }
    if ($r eq "R") { return (  $p[1], -$p[0] ) }
    if ($r eq "U") { return ( -$p[0], -$p[1] ) }
    die "unknown rotation $r";
}

sub print_usage() {
    print "Usage:\n" .
          "$0\n" .
          "        [--in-paper-size=<W>:<H>]  [--out-paper-size=<W>:<H>]\n" .
          "        [--in-fields=<L>:<D>:<R>:<U>]  [--out-fields=<L>:<D>:<R>:<U>]\n" .
          "        [--out-rotate=L|R|U]  [--bind-width=<bind>]\n" .
          "        <in-psfile> <out-psfile>\n";
}
sub generate_pagespecs($$$$$);


# portrait letter
my @out_paper_size = (612,792);
my @in_paper_size = (612,792);
my $out_rotate = "L";


# text fields on input: left, down, right, up
my @in_fields_ldru = (72,24,90,58);

# binding width, applied to Y coordinate
my $out_bind_width=16;
# text fields on output: left, down, right, up
my @out_fields_ldru=(32,32,32,32);

# 1 x 2 pages on the output
my @out_nup_xy = (1, 2); 
# where to put bind location on the 1st page of output 
my $out_bind_location = 'D'; 

if (1+$#ARGV < 2) {
    print_usage();
    exit 1
}
while (@ARGV) {
    my $cmd = $ARGV[0];
    
    if (!defined($cmd) || $cmd eq '--' || $cmd !~ /^--/) { last }
    else { shift @ARGV; }

    if ($cmd eq '--help') {
        print_usage();
        exit 1
    } elsif ($cmd =~ /^--in-paper-size=/) {
        @in_paper_size = map { sprintf('%d', $_) } split(':', $');
        my $correct = 1;
        foreach (@in_paper_size) { $correct &&= (0 < $_) }
        if (!$correct || 2 != 1+$#in_paper_size) {
            print STDERR "incorrect value for --in-paper-size='$''\n";
            exit 1;
        }
    } elsif ($cmd =~ /^--out-paper-size=/) {
        @out_paper_size = map { sprintf('%d', $_) } split(':', $');
        my $correct = 1;
        foreach (@out_paper_size) { $correct &&= (0 < $_) }
        if (!$correct || 2 != 1+$#out_paper_size) {
            print STDERR "incorrect value for --out-paper-size='$''\n";
            exit 1;
        }
    } elsif ($cmd =~ /^--in-fields=/) {
        @in_fields_ldru = map { sprintf('%d', $_) } split(':', $');
        my $correct = 1;
        foreach (@in_fields_ldru) { $correct &&= (0 <= $_) }
        if (!$correct || 4 != 1+$#in_fields_ldru) {
            print STDERR "incorrect value for --in-fields='$''\n";
            exit 1;
        }
    } elsif ($cmd =~ /^--out-fields=/) {
        @out_fields_ldru = map { sprintf('%d', $_) } split(':', $');
        my $correct = 1;
        foreach (@out_fields_ldru) { $correct &&= (0 <= $_) }
        if (!$correct || 4 != 1+$#out_fields_ldru) {
            print STDERR "incorrect value for --out-fields='$''\n";
            exit 1;
        }
    } elsif ($cmd =~ /^--out-rotate=/) {
        $out_rotate = $';
        if (index('LRU', $out_rotate) < 0) {
            print STDERR "incorrect value for --out-rotate='$''\n";
            exit 1;
        }
    } elsif ($cmd =~ /^--bind-width=/) {
        $out_bind_width = sprintf('%s', $');
        if ($out_bind_width < 0) {
            print STDERR "incorrect value for --bind-width='$''\n";
            exit 1;
        }
    } else {
        print STDERR "unknown option '$cmd'\n";
        print_usage();
        exit 1
    }
}

die "incorrect bind location '$out_bind_location'" 
    if (index('LDRU', $out_bind_location) < 0);

my @out_bind_ldru  = (0,0,0,0);
my @out_bind2_ldru = (0,0,0,0);
if ($out_bind_location eq 'L') { 
    @out_bind_ldru  = ($out_bind_width, 0, 0, 0);
    @out_bind2_ldru = (0, 0, $out_bind_width, 0);
} elsif ($out_bind_location eq 'R') {
    @out_bind_ldru  = (0, 0, $out_bind_width, 0);
    @out_bind2_ldru = ($out_bind_width, 0, 0, 0);
} elsif ($out_bind_location eq 'D') {
    @out_bind_ldru  = (0, $out_bind_width, 0, 0);
    @out_bind2_ldru = (0, 0, 0, $out_bind_width);
} elsif ($out_bind_location eq 'U') {
    @out_bind_ldru  = (0, 0, 0, $out_bind_width);
    @out_bind2_ldru = (0, $out_bind_width, 0, 0);
}

my @in_text_size = (
    $in_paper_size[0] - $in_fields_ldru[0] - $in_fields_ldru[2],
    $in_paper_size[1] - $in_fields_ldru[1] - $in_fields_ldru[3]
);
die "incorrect input specification" 
    if ($in_text_size[0] <= 0 || $in_text_size[1] <= 0);

# text will be rotated 'L'
my @rt_text_size = map(abs, rotate($out_rotate, @in_text_size));
my $rt_text_ratio_xy = $rt_text_size[0] / $rt_text_size[1];
print "rt_text_size=(@rt_text_size), ratio_xy=$rt_text_ratio_xy\n";


my @out_max_text_size = ( 
    ($out_paper_size[0] - ($out_bind_ldru[0] + $out_bind_ldru[2])) / $out_nup_xy[0] 
                - ($out_fields_ldru[0] + $out_fields_ldru[2]),
    ($out_paper_size[1] - ($out_bind_ldru[1] + $out_bind_ldru[3])) / $out_nup_xy[1] 
                - ($out_fields_ldru[1] + $out_fields_ldru[3])
);
die "incorrect input specification" 
    if ($out_max_text_size[0] <= 0 || $out_max_text_size[1] <= 0);
my $out_max_text_ratio_xy = $out_max_text_size[0] / $out_max_text_size[1];
print "out_max_text_size=(@out_max_text_size), ratio_xy=$out_max_text_ratio_xy\n";


#fit rectangle @rt_text_ratio_xy into @out_max_text_ratio_xy
my @out_text_size = (0, 0);
my $out_scale=0;
if ($rt_text_ratio_xy >= $out_max_text_ratio_xy) {
    $out_scale = $out_max_text_size[0] / $rt_text_size[0];
    $out_text_size[0] = $out_max_text_size[0];
    $out_text_size[1] = $out_text_size[0] / $rt_text_ratio_xy;
} else {
    $out_scale = $out_max_text_size[1] / $rt_text_size[1];
    $out_text_size[1] = $out_max_text_size[1];
    $out_text_size[0] = $out_text_size[1] * $rt_text_ratio_xy;
}
print "out_text_size=(@out_text_size), ratio_xy=", 
        $out_text_size[0] / $out_text_size[1], "\n";
print "scale=$out_scale\n";


#determine shift by page center
my @rt_page_center = (rotate($out_rotate, 
    $in_fields_ldru[0] + $in_text_size[0] / 2.,
    $in_fields_ldru[1] + $in_text_size[1] / 2. ));
my @out_max_page_center = (
    $out_fields_ldru[0] + $out_max_text_size[0] / 2.,
    $out_fields_ldru[1] + $out_max_text_size[1] / 2.);
my @out_max_page_size = (
    $out_fields_ldru[0] + $out_max_text_size[0] + $out_fields_ldru[2],
    $out_fields_ldru[1] + $out_max_text_size[1] + $out_fields_ldru[3]);
    

# shift without $out_bind_ldru applied! it depends on page being even/odd
my @shifts = ();
for my $i_nup_x (0 .. $out_nup_xy[0] - 1) {
    for my $i_nup_y (0 .. $out_nup_xy[1] - 1) {
        push @shifts, 
                [
                +$out_max_page_size[0] * $i_nup_x
                -$rt_page_center[0] * $out_scale + $out_max_page_center[0],

                +$out_max_page_size[1] * $i_nup_y
                -$rt_page_center[1] * $out_scale + $out_max_page_center[1]
                ]
    }
}
my $pagespecs;
$pagespecs = sprintf('%d:', 2*(1+$#shifts)) .
    generate_pagespecs(\@shifts, \@out_bind_ldru,  $out_scale, $out_rotate, 0) . ',' . 
    generate_pagespecs(\@shifts, \@out_bind2_ldru, $out_scale, $out_rotate, 1+$#shifts);
print $pagespecs, "\n";
`pstops '$pagespecs' $ARGV[0] $ARGV[1]`;



my $shift_x = -$rt_page_center[0] * $out_scale
              + $out_fields_ldru[0] + $out_max_text_size[0] / 2.;
my $shift_y0= -$rt_page_center[1] * $out_scale
              + $out_fields_ldru[1] + $out_max_text_size[1] / 2.;
my $shift_y1= $shift_y0
              + $out_fields_ldru[1] + $out_max_text_size[1] + $out_fields_ldru[3];
my $shifts = [ [ $shift_x, $shift_y0 + $out_bind_width ],
               [ $shift_x, $shift_y1 + $out_bind_width ],
               [ $shift_x, $shift_y0 ],
               [ $shift_x, $shift_y1 ] ];
sub print_pagespec($$$$) {
    my ($shift_x, $shift_y, $scale, $rotate) = @_;
    return sprintf('%s@%f(%f,%f)', $rotate, $scale, $shift_x, $shift_y);
}
#my $pagespecs = 
#    "4:0${out_rotate}@" . "${out_scale}($shifts->[0][0],$shifts->[0][1])" . 
#     "+1${out_rotate}@" . "${out_scale}($shifts->[1][0],$shifts->[1][1])" .
#     ",2${out_rotate}@" . "${out_scale}($shifts->[2][0],$shifts->[2][1])" .
#     "+3${out_rotate}@" . "${out_scale}($shifts->[3][0],$shifts->[3][1])" ;
$pagespecs = 
    '4:0' . print_pagespec($shifts->[0][0], $shifts->[0][1], $out_scale, $out_rotate) . 
     '+1' . print_pagespec($shifts->[1][0], $shifts->[1][1], $out_scale, $out_rotate) . 
     ',2' . print_pagespec($shifts->[2][0], $shifts->[2][1], $out_scale, $out_rotate) . 
     '+3' . print_pagespec($shifts->[3][0], $shifts->[3][1], $out_scale, $out_rotate);


print $pagespecs, "\n";
#`pstops '$pagespecs' $ARGV[0] $ARGV[1]`;


sub generate_pagespecs($$$$$) {
    my ($shifts, $bind_ldru, $scale, $rotate, $pagecnt_base) = @_;
    return 
        join('+', 
             map { sprintf('%d%s@%f(%f,%f)', 
                           $pagecnt_base + $_, $rotate, $scale, 
                           $shifts->[$_][0] + $bind_ldru->[0],
                           $shifts->[$_][1] + $bind_ldru->[1]) } 
                 (0 .. $#$shifts));

}
