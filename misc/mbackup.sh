#!/bin/bash

BACKUP_DIR_LIST="/Users/syritsyn"
prg_name=$(basename $0)

# get list of files to backup
# usage: <dir> [<inc-mark>]
function get_file_list() {
  if [ $# -lt 1 ] ; then 
    echo -ne "Usage:\nget_file_list <dir> [<inc-mark>]\n" >&2
    return 1
  fi
  local dir="$1"
  if [ ! -r "$dir" ] ; then
    echo "$prg_name: path '$dir' does not exist" >&2
    return 1
  fi
  
  local inc_command=''
  if [ $# -eq 2 ] ; then
    if [ ! -f "$2" ] ; then
      echo "$prg_name: inc-mark '$2' does not exist" >&2
      return 1
    fi
    inc_command="-newer $2"
  fi

  if [ -f "$dir" ] ; then 
    find "$dir" -type f $inc_command -print
    return 0
  elif [ -d "$dir" ] ; then
    pushd "$dir" >/dev/null || return 1
    { 
      if [ -r ".mbackup_include" ] ; then
        for f in $(< .mbackup_include) ; do
          find "$f" -type f $inc_command -print
        done
      else
        find . -type f $inc_command -print 
      fi
    } \
    | { 
      if [ -r ".mbackup_exclude" ] ; then
        grep -vf .mbackup_exclude \
          |sed -e 's?^?'"$dir/"'? ; s?/\./?/?g'
      else
        sed -e 's?^?'"$dir/"'? ; s?/\./?/?g' 
      fi
    }
    popd >/dev/null || return 1
  fi
}

#usage: <backup-archive> <dst>
function copy_to_storage() {
  time rsync -P $1 $2 || return 1
}

INC_MARK=''
inc_mark=''
if [ $# -ge 1 ] ; then
  INC_MARK="$1"
  if [ -r "$INC_MARK" ] ; then
    inc_mark="$INC_MARK"
  fi
fi

if [ "$inc_mark" == "" ] ; then
  backup_name='backup_'$(date +'%Y-%m-%d')'_full_'$(uuidgen)
else
  backup_name='backup_'$(date +'%Y-%m-%d')'_incr_'$(uuidgen)
fi


ARCHIVE_CMD="tar -czT -"
ENCRYPT_CMD="gpg -er backup@nowhere.else"
DST_REMOTE="bgl-backup:/data/P7/syritsyn/Archive/macbook-backup"

pushd / >/dev/null

tmp_dir=$(mktemp -d /tmp/backup.XXXXXX)
backup_archive=$tmp_dir/$backup_name
time {
  for d in $BACKUP_DIR_LIST ; do 
    get_file_list "$d" $inc_mark
  done
} \
  |sed -e 's?^/??' \
  |$ARCHIVE_CMD |$ENCRYPT_CMD >$backup_archive || exit 1
echo "backup size $(ls -l $backup_archive |awk '{print $5}')"

copy_to_storage $backup_archive $DST_REMOTE || exit 1
rm -rf $(dirname $backup_archive)

popd >/dev/null

if [ "$INC_MARK" != "" ] ; then
  mkdir -p $(dirname $INC_MARK)
  touch "$INC_MARK"
fi
