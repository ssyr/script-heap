prefix	?= $(HOME)/usr
bindir  ?= $(prefix)/bin
libdir  ?= $(prefix)/lib
perldir ?= $(prefix)/share/perlmod
pythonlibdir ?= $(prefix)/lib/python

PYTHON	?= /usr/bin/python

export prefix PYTHON bindir libdir perldir pythonlibdir

subdirs = \
	aff \
	prod \
	python \

install: do.local do.install

do.install:
	@for d in $(subdirs) ; do \
		$(MAKE) -C $$d do.install ;\
	done

do.local:
	@for d in $(subdirs) ; do \
		$(MAKE) -C $$d do.local ;\
	done

clean:
	@for d in $(subdirs) ; do \
		$(MAKE) -C $$d clean ;\
	done

.PHONY: install do.local do.install clean
