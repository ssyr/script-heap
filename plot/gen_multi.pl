#!/usr/bin/perl -w
use strict;

my $xw=1;
my $yw=.5;
my $xlw=.2;
my $n=3;

sub make_multi {
    my ($n_x, $n_y, 
        $x_shift, $y_shift, 
        $x_width, $y_width, 

        $x_labels, $y_labels,
        $x_tics_cmds, $y_tics_cmds,
        $x_tics_in_cmds, $y_tics_in_cmds,
        $x_label_width, $y_label_width, 

        $x2_labels, $y2_labels,
        $x2_tics_cmds, $y2_tics_cmds,
        $x2_tics_in_cmds, $y2_tics_in_cmds,
        $x2_label_width, $y2_label_width, 

        $preamble,
        $list,
        $order,
        ) = @_;
    my $str = '';
    $str .= "$preamble";
    $str .= "set multiplot \n";
    $str .= "set multi\n";
    $str .= "set tmar 0\nset bmar 0\nset rmar 0\nset lmar 0\n";
    my $x_total = $n_x * $x_shift + $x_label_width + $x2_label_width;
    my $y_total = $n_y * $y_shift + $y_label_width + $y2_label_width;
    my $x_sh_rel = $x_shift * 1.0 / $x_total;
    my $y_sh_rel = $y_shift * 1.0 / $y_total;
    if ($order eq 'dr') {
        my $cnt = 0;
        for (my ($ix, $x_cnt) = (0, 0) ; $ix < $n_x ; $ix++, $x_cnt++) {
            for (my ($iy, $y_cnt) = ($n_y, 0) ; $iy-- ; $y_cnt++) {
                # set x axis
                if (0 == $iy) {
                    $str .= "set xl '$x_labels->[$x_cnt]'\n";
                    $str .= "set xtics $x_tics_cmds->[$x_cnt]\n";
                    $str .= "set x2l ''\n";
                    $str .= "set x2tics $x2_tics_in_cmds->[$x_cnt]\n";
                } elsif ($n_y - 1 == $iy) {
                    $str .= "set xl ''\n";
                    $str .= "set xtics $x_tics_in_cmds->[$x_cnt]\n";
                    $str .= "set x2l '$x2_labels->[$x_cnt]'\n";
                    $str .= "set x2tics $x2_tics_cmds->[$x_cnt]\n";
                } else {
                    $str .= "set xl ''\n";
                    $str .= "set xtics $x_tics_in_cmds->[$x_cnt]\n";
                    $str .= "set x2l ''\n";
                    $str .= "set x2tics $x2_tics_in_cmds->[$x_cnt]\n";
                }
                # set y axis
                if (0 == $ix) {
                    $str .= "set yl '$y_labels->[$y_cnt]'\n";
                    $str .= "set ytics $y_tics_cmds->[$y_cnt]\n";
                    $str .= "set y2l ''\n";
                    $str .= "set y2tics $y2_tics_in_cmds->[$y_cnt]\n";
                } elsif ($n_x - 1 == $ix) {
                    $str .= "set yl ''\n";
                    $str .= "set ytics $y_tics_in_cmds->[$y_cnt]\n";
                    $str .= "set y2l '$y2_labels->[$y_cnt]'\n";
                    $str .= "set y2tics $y2_tics_cmds->[$y_cnt]\n";
                } else {
                    $str .= "set yl ''\n";
                    $str .= "set ytics $y_tics_in_cmds->[$y_cnt]\n";
                    $str .= "set y2l ''\n";
                    $str .= "set y2tics $y2_tics_in_cmds->[$y_cnt]\n";
                }

                my $x_orig_rel = ($x_label_width + $x_shift * $ix) * 1.0 / $x_total;
                my $y_orig_rel = ($y_label_width + $y_shift * $iy) * 1.0 / $y_total;
                $str .= "set size $x_sh_rel,$y_sh_rel\n";
                $str .= "set origin $x_orig_rel,$y_orig_rel\n";
                $str .= "$list->[$cnt]\n";

                $cnt++;
            }
        }
    }
    #elsif ($order eq 'rd' ) {
    #}
    else {
        print STDERR "unknown order='$order'";
        exit 1
    }
    $str .= "unset multiplot";
    return $str;

}

#testing
open(FI, 'test.plt') or die "cannot open file";
my @list = <FI>;
foreach my $x (@list) { print "$x"; }
print make_multi(
        2, 3,                           # $n_x, $n_y, 
        .5, .25,                        # $x_shift, $y_shift, 
        .5, .25,                        # $x_width, $y_width,

        ['x_1', 'x_2'], ['y_1', 'y_2', 'y_3'], # $x_labels, $y_labels,
        ['.2 format "%.1f"', '.2 format "%.1f"'], 
        ['.5 format "%.1f"', '.5 format "%.1f"', '.5 format "%.1f"'], # $x_tics_cmds, $y_tics_cmds,
        ['format ""', 'format ""'], 
        ['format ""', 'format ""', 'format ""'],     # $x_tics_in_cmds, $y_tics_in_cmds,
        .1, .1,                         # $x_label_width, $y_label_width, 

        ['x@^2_1', 'x@^2_2'], 
        ['y@^2_1', 'y@^2_2', 'y@^2_3'],     # $x2_labels, $y2_labels,
        ['.1 format "%.1f"', '.1 format "%.1f"'], 
        ['.25 format "%.1f"', '.25 format "%.1f"', '.25 format "%.1f"' ],   #$x2_tics_cmds, $y2_tics_cmds,
        ['format ""', 'format ""'], 
        ['format ""', 'format ""', 'format ""'],     #$x2_tics_in_cmds, $y2_tics_in_cmds,
        .2, .2,     #$x2_label_width, $y2_label_width, 

        "",         # preamble
        \@list,      # $list
        'dr',       #  $order,
        );
